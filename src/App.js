// import './App.scss';
import React from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from './redux/index';
import AppRouter from './routers/index';
import { PersistGate } from 'redux-persist/integration/react';

function App() {
  return (
    <div className="App">
      <Provider className="App" store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppRouter />
        </PersistGate>
      </Provider>
    </div>
  );
}

export default App;
