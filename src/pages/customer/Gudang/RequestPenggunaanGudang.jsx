import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './styles.css';
import DataGudang from './components/DataGudang';
import TipePenyimpanan from './components/TipePenyimpanan';
import TableSKUProduk from './components/TableSKUProduk';
import Button from '../../../components/elements/Button';
import BackButton from '../../../components/elements/BackButton';
import ModalKirimPenggunaanGudang from './components/ModalKirimPenggunaanGudang';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
const RequestPenggunaanGudang = () => {
  const history = useHistory();
  const [state, setState] = useState({ showModal: false });
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Request Penggunaan Gudang"
              onClick={() => history.push('/gudang/request-gudang')}
            />
            <Button
              styleType="blueFill"
              text="Kirim  Penggunaan Gudang"
              onClick={() => {
                setState({ ...state, showModal: true });
              }}
            />
          </div>
        )}
      >
        <ContentItem col="col-12" spaceBottom={3}>
          <DataGudang page="RequestPenggunaanGudang" />
        </ContentItem>
        <ContentItem className="p-3" col="col-12" spaceBottom={3}>
          <TipePenyimpanan />
        </ContentItem>
        <ContentItem className="p-3" col="col-12" spaceBottom={3}>
          <TableSKUProduk />
        </ContentItem>
      </ContentContainer>
      <ModalKirimPenggunaanGudang
        show={state.showModal}
        onHide={() => setState({ ...state, showModal: false })}
      />
    </>
  );
};

export default RequestPenggunaanGudang;
