import actionType from './reduxConstant';

const initialState = {
  requestGudang: {},
  skuProduct: [
    {
      id: 1,
      sku: 'SKU123',
      product: 'Baju Bagus',
      size: '40x20x10',
      qty: 0,
    },
    {
      id: 2,
      sku: 'SKUABC',
      product: 'Baju New Edition',
      size: '40x20x10',
      qty: 0,
    },
  ],
  listWarehouse: {},
  detailWarehouse: {},
  listCoordinateWarehouse: [],
};

const gudangReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.SET_DATA_REQUEST_GUDANG:
      return {
        ...prevState,
        requestGudang: action.payload,
      };
    case actionType.CHANGE_QUANTITY_SKU_PRODUCT: {
      let newSKUProduct = prevState.skuProduct.length
        ? prevState.skuProduct.map((item) => {
            if (item.id === action.payload.id) {
              return {
                ...item,
                qty: action.payload.qty,
              };
            } else {
              return item;
            }
          })
        : [];
      return {
        ...prevState,
        skuProduct: newSKUProduct,
      };
    }
    case actionType.LIST_WAREHOUSE_CUSTOMER: {
      const listCoordinate =
        action.payload?.data?.map((item) => ({
          lng: item.longitude,
          lat: item.latitude,
        })) || [];
      return {
        ...prevState,
        listWarehouse: action.payload,
        listCoordinateWarehouse: listCoordinate,
      };
    }
    case actionType.DETAIL_WAREHOUSE_CUSTOMER: {
      return {
        ...prevState,
        detailWarehouse: action.payload,
      };
    }
    default:
      return prevState;
  }
};

export default gudangReducer;
