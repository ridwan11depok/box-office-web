import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

export const setDataRequestGudang = (data) => {
  return {
    type: actionType.SET_DATA_REQUEST_GUDANG,
    payload: data,
  };
};

export const changeQuantitySKUProduct = (data) => {
  return {
    type: actionType.CHANGE_QUANTITY_SKU_PRODUCT,
    payload: data,
  };
};

export const getListWarehouse =
  (params = {}, warehouseId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store?.[0]?.id;
      let data = {};
      let headers = { token: token };
      let response;
      if (warehouseId) {
        response = await consume.getWithParams(
          'detailWarehouseCustomer',
          data,
          headers,
          { ...params, store_id: storeId },
          warehouseId
        );
        dispatch(detailWarehouse(response.result));
      } else {
        response = await consume.getWithParams(
          'listWarehouseCustomer',
          data,
          headers,
          { ...params, store_id: storeId }
        );
        dispatch(listWarehouse(response.result));
      }

      dispatch(setLoading(false));
    } catch (err) {
      dispatch(listWarehouse({}));
      dispatch(setLoading(false));
    }
  };

const listWarehouse = (data) => {
  return {
    type: actionType.LIST_WAREHOUSE_CUSTOMER,
    payload: data,
  };
};

const detailWarehouse = (data) => {
  return {
    type: actionType.DETAIL_WAREHOUSE_CUSTOMER,
    payload: data,
  };
};

export const requestGudang = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store?.[0]?.id;
    let data = { ...payload, store_id: storeId };
    let headers = { token: token };

    const response = await consume.post(
      'requestWarehouseCustomer',
      data,
      headers
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil mengirim request gudang.',
        type: 'success',
      })
    );
    return Promise.resolve(response);
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal mengirim request gudang.',
        type: 'error',
      })
    );
    return Promise.reject(err);
  }
};
