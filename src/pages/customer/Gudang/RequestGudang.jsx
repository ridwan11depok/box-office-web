import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import './styles.css';
import { getListWarehouse } from './reduxAction';
import DataGudang from './components/DataGudang';
import FotoGudang from './components/FotoGudang';
import CustomerGudang from './components/CustomerGudang';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import BackButton from '../../../components/elements/BackButton';
import { openInNewTab } from '../../../utils/helper';
import { useSelector } from 'react-redux';
import ConfirmRequestGudang from './components/ConfirmRequestGudang';
import StatusGudang from './components/StatusGudang';
import DeliveryDay from './components/DeliveryDay';
import Banner from '../../../components/elements/Banner';

const useStyles = makeStyles({
  gudangName: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },

  btnBlueOutline: {
    marginRight: '5px',
  },
  btnBlueNoFill: {
    marginRight: 0,
    paddingLeft: 0,
  },
  label: {
    fontWeight: '600',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#1C1C1C',
  },
  cycleContainer: {
    width: '40px',
    height: '40px',
    backgroundColor: '#EBEBFF',
    color: '#192A55',
  },
  valueBold: {
    fontWeight: '700',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    color: '#1C1C1C',
  },
  totalReview: {
    fontFamily: 'Open Sans',
    fontSize: '12px',
    color: '#192A55',
    fontWeight: '500',
  },
});

const RequestGudang = () => {
  const history = useHistory();
  const classes = useStyles()
  const params = useParams();
  const [state, setState] = useState({
    showRequest: false,
  });
  const { detailWarehouse } = useSelector((state) => state.gudang);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListWarehouse({}, params?.id));
  }, []);

  return (
    <div className='container-fluid'>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row flex-wrap justify-content-between">
            <div className='d-flex flex-row  align-items-center'>
              <BackButton
                label="Request Gudang"
                onClick={() => history.push('/gudang')}
              />
              <div className='ml-2'>
                <StatusGudang status={detailWarehouse?.status_approval || 0} />
              </div>
            </div>
            <div className="d-flex flex-row pr-2">
              <Button
                styleType="blueOutline"
                className={classes.btnBlueOutline}
                text="Whatsapp"
                startIcon={() => <WhatsAppIcon />}
                onClick={() => {
                  let removeZeroFromPhoneNumber = detailWarehouse?.phone_number?.[0] === '0' ? detailWarehouse?.phone_number?.slice(1) : detailWarehouse?.phone_number?.[0]
                  openInNewTab(`https://wa.me/62${removeZeroFromPhoneNumber}`)
                }}
              />
              {
                !detailWarehouse?.storage || detailWarehouse?.storage?.status === 2  ?
                <Button
                  onClick={() => setState({ ...state, showRequest: true })}
                  styleType="blueFill"
                  className={classes.btnBlueFill}
                  text="Request Penggunaan Gudang"
                />
                :
                ''
              }
            </div>
          </div>
        )}
      >
        {detailWarehouse?.storage?.status === 0 &&
          <ContentItem col="col-12" spaceBottom={3} border={false}>
            <Banner
              className="w-100 d-flex flex-column flex-lg-row justify-content-between align-items-lg-center"
              styleType={detailWarehouse?.storage?.status === 0 ? 'yellow' : 'red'}
            >
              <h6 className={classes.textBanner}>
                {detailWarehouse?.storage?.status === 0 ?
                 'Store sedang menunggu dalam persetujuan oleh pihak Administrator.'
                : 'Store dalam keadaan rejected dan sedang menunggu persetujuan pihak administrator'}
              </h6>

            </Banner>
          </ContentItem>
        }
        <ContentItem
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}>
          <DataGudang />
        </ContentItem>
        <ContentItem
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}>
          <DeliveryDay />
        </ContentItem>
      </ContentContainer>
      <ContentItem className="p-3" spaceBottom={3}>
        <FotoGudang />
      </ContentItem>
      <ContentItem className="p-3" spaceBottom={3}>
        <CustomerGudang />
      </ContentItem>
      <ConfirmRequestGudang
        show={state.showRequest}
        onHide={() => setState({ ...state, showRequest: false })}
      />
    </div>
  );
};

export default RequestGudang;
