import React, { useEffect } from 'react';
import '../System Settings/styles.css';
import ListGudang from './components/ListGudang';
import FilterAndSearch from './components/FilterAndSearch';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import Map from '../../../components/elements/Map';
import { useDispatch, useSelector } from 'react-redux';
import { getListWarehouse } from './reduxAction';
// import LeafletMap from '../../../components/elements/Map/Leaflet';

const Gudang = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListWarehouse());
  }, []);
  const { listCoordinateWarehouse, listWarehouse } = useSelector((state) => state.gudang);

  return (
    <>
      <ContentContainer title="Gudang">
        <ContentItem spaceBottom={4}>
          <FilterAndSearch />
        </ContentItem>
        <ContentItem
          col="col-12 col-md-6"
          spaceRight="0 pr-md-2"
          spaceBottom={3}
          style={{ overflow: 'hidden' }}
        >
          {/* <LeafletMap /> */}
          <Map className="h-100" isMarkerShown mode='viewer' listCoordinate={listCoordinateWarehouse} data={listWarehouse?.data} />
        </ContentItem>
        <ContentItem
          col="col-12 col-md-6"
          spaceLeft="0 pl-md-2"
          spaceBottom={3}
        >
          <ListGudang />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default Gudang;
