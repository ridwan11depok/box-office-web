import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

const optionsPenyimpanan = [
  {
    label: 'Tanpa Barcode',
    description:
      'Tidak menggunakan sistem barcode. Pengecekan dilakukan secara manual, dan tidak menggunakan barcode scanner.',
  },
  {
    label: 'Barcode Oleh Seller',
    description:
      'Anda harus melakukan barcode labelling sebelum mengirimkan inventory ke gudang. Format barcode yang diberikan dapat di-print menggunakan label Tom&Jerry No.107 (50x20mm)',
    note: 'Disarankan dengan produk SKU lebih dari 10',
  },
  {
    label: 'Barcode Oleh Seller',
    description:
      'Gudang akan melakukan barcode labelling untuk anda dengan biaya Rp.250 per label.',
    note: 'Disarankan dengan produk SKU lebih dari 10',
  },
];
const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  note: {
    fontSize: '12px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
  },
});

function TipePenyimpanan() {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState('0');

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };
  return (
    <>
      <h6 className={classes.label}>Tipe Penyimpanan</h6>
      <h6 className={classes.description}>
        Pilih sistem penyimpanan stock sesuai dengan keperluanmu.
      </h6>
      <div
        className="d-flex flex-column mt-1"
        // style={{ flexFlow: 'row wrap' }}
      >
        <FormControl component="fieldset">
          <RadioGroup
            row
            aria-label="position"
            name="position"
            defaultValue="top"
          >
            {optionsPenyimpanan.map((option, idx) => (
              <div
                className="d-flex flex-column p-1 rounded pl-2 w-100 mb-2 control-tipe-penyimpanan"
                style={{ border: '1px solid #E8E8E8' }}
              >
                <div className="d-flex flex-row align-items-start">
                  <Radio
                    checked={selectedValue === idx.toString()}
                    onChange={handleChange}
                    value={idx.toString()}
                    name="radio-button"
                    inputProps={{ 'aria-label': idx.toString() }}
                    style={{ color: '#192A55' }}
                  />
                  <div className="ml-0">
                    <h6
                      className={`${classes.label} mb-0`}
                      style={{ marginTop: '11px' }}
                    >
                      {option.label}
                    </h6>
                    {option.note && (
                      <h6 className={`${classes.note} mt-0`}>{option.note}</h6>
                    )}
                  </div>
                </div>
                <h6 className={classes.description}>{option.description}</h6>
              </div>
            ))}
          </RadioGroup>
        </FormControl>
      </div>
    </>
  );
}

export default TipePenyimpanan;
