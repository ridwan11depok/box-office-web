import { makeStyles } from '@material-ui/core';
import React from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx'

const useStyles = makeStyles({
  label: {
    fontWeight: '400',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#4F4F4F',
    marginLeft: '14px'
  },
  labelTotal: {
    fontWeight: '400',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#4F4F4F',
  },
  dataValue: {
    fontWeight: '600',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#363636',
  },
});

const BoxSvg = () => {
  return (
    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="40" height="40" rx="20" fill="#DBE3F5" />
      <path fill-rule="evenodd" clip-rule="evenodd" d="M20 10.9453L30 14.2786V14.5751V24.9994V26.4515L20 30.0626L10 26.4515V24.4994V14.5751V14.2786L20 10.9453ZM20 13.0535L27.3911 15.5172L25.6198 16.1568L18.337 13.6078L20 13.0535ZM15.236 14.6415L12.6089 15.5172L20 18.1862L22.64 17.2329L15.236 14.6415ZM24 18.8681L21 19.9515V27.5751L28 25.0473V24.9994V17.4237L26 18.1459V19.9994L24 20.9994V18.8681ZM12 24.4994V17.4237L19 19.9515V27.5751L12 25.0473V24.4994Z" fill="#182953" />
    </svg>
  )
}


export default function DeliveryDay(props) {
  const history = useHistory();
  const classes = useStyles()
  const { detailWarehouse } = useSelector((state) => state.gudang);
  console.log(`detailWarehouse`, detailWarehouse)
  const listDeliveryDay = detailWarehouse?.delivery_day
    ? JSON.parse(detailWarehouse.delivery_day).map((item) => ({
      day: item[0].charAt(0).toUpperCase() + item[0].slice(1),
      hour: `${item[1]} - ${item[2]}`,
    }))
    : [];


  console.log(`listDeliveryDay`, listDeliveryDay)
  return (
    <div className='container'>
      <div className='pb-4 pt-2'>
      <span className={clsx(classes.label, 'mt-1')}>Hari Pengiriman</span>
      {listDeliveryDay?.length ?
        listDeliveryDay.map((item, index) => {
          return (
            <div className='row col-12 mt-2' key={index}>
              <div className={clsx(classes.dataValue, 'col-6')}>{item?.day}</div>
              <div className={clsx(classes.dataValue, 'col-6')}>{item?.hour}</div>
            </div>
          )
        })
        :
        ''
      }
      </div>
      <div className='row border-top'>
        <div className='col-6 border-right'>
          <div className='d-flex w-100 px-2 py-3'>
            <BoxSvg />
            <div className='d-flex flex-col pl-2'>
              <span className={clsx(classes.labelTotal)}>Total Outbound</span>
              <span className={clsx(classes.dataValue)}>{detailWarehouse?.total_outbound}</span>
            </div>
          </div>
        </div>
        <div className='col-6'>
          <div className='d-flex flex-row w-100 px-2 py-3'>
            <BoxSvg />
            <div className='d-flex flex-col pl-2'>
              <span className={clsx(classes.labelTotal)}>Total Inbound</span>
              <span className={clsx(classes.dataValue)}>{detailWarehouse?.total_inbound}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
