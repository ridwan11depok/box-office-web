import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  storeName: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  address: {
    fontSize: '12px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

function CustomerGudang() {
  const classes = useStyles();
  const { detailWarehouse } = useSelector((state) => state.gudang);
  return (
    <>
      <h6 className={classes.label}>Customer</h6>
      <div
        className="d-flex flex-md-row flex-column justify-content-between mt-1"
        style={{ flexFlow: 'row wrap' }}
      >
        { detailWarehouse?.store_list?.length ?
        detailWarehouse?.store_list?.map((item) => {
          return (
            <div style={{ width: '49%', marginBottom: '7px' }}>
              <div
                className="d-flex flex-row align-items-center p-1 pl-2"
              >
                <img src={item?.url} alt={item?.name} height='48px' width="48px" className='rounded'/>
                <div className="ml-2">
                  <h6 className={classes.storeName}>
                    {item?.name || 'No Data'}
                  </h6>
                  <h6 className={classes.address}>
                    {`Kecamatan ${item?.district}, ${item?.city}, ${item?.province}`}
                  </h6>
                </div>
              </div>
            </div>
          );
        })
        : 
        <div className='text-center w-100'>Gudang belum memiliki customer</div>
      }
      </div>
    </>
  );
}

export default CustomerGudang;
