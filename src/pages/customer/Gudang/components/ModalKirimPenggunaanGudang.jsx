import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';

const ModalKirimPenggunaanGudang = (props) => {
  const { skuProduct } = useSelector((state) => state.gudang);
  const checkSKUProduct = () => {
    if (skuProduct.filter((item) => item.qty > 0).length) {
      return true;
    } else {
      return false;
    }
  };
  return (
    <>
      <Modal
        titleModal={`Kirim Penggunaan Gudang ${
          checkSKUProduct() ? '' : '(Dropship)'
        }`}
        bodyClassName="pt-3 pb-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) =>
          checkSKUProduct() ? (
            <p className="text-left">
              Apakah anda yakin akan melakukan request gudang “Gudang A” sebagai
              tempat penyimpanan anda?
            </p>
          ) : (
            <p className="text-left">
              Anda tidak memasukkan stock yang akan dikirim ke gudang. Apakah
              anda akan melanjutkan sebagai{' '}
              <span style={{ color: '#DA101A' }}>'Dropshipper'</span>?
            </p>
          )
        }
        data={props.data}
        footer={(data) => (
          <>
            <Button
              onClick={props.onHide}
              styleType="lightBlueFill"
              text="Batal"
              className="mr-3"
            />
            <Button
              styleType="blueFill"
              text={
                checkSKUProduct()
                  ? 'Kirim Penggunaan Gudang'
                  : 'Request Sebagai Dropshipper'
              }
            />
          </>
        )}
      />
    </>
  );
};

export default ModalKirimPenggunaanGudang;
