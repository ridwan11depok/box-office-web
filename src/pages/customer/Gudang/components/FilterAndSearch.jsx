import React, { useEffect, useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import {
  SelectSearch,
  SearchField,
  ReactSelect,
} from '../../../../components/elements/InputField';
import { useDispatch, useSelector } from 'react-redux';
import { getListWarehouse } from '../reduxAction';
import { getArea } from '../../../administrator/Gudang/reduxAction';

function FilterAndSearch() {
  const { listProvince, listCity } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const dispatch = useDispatch();
  const [state, setState] = useState({
    province_id: null,
    city_id: null,
    keyword: '',
  });
  const onSearch = (e) => {
    setState({ ...state, keyword: e.target.value });
    dispatch(getListWarehouse({ search: e.target.value }));
  };

  const handleSelectProvince = (e) => {   
   let selectedId = Number(e.target.value) === 0 ? null : Number(e.target.value)

    setState({ ...state, province_id: selectedId });
    dispatch(
      getListWarehouse({
        search: state.keyword,
        province_id: selectedId,
        city_id: state.city_id,
      })
    );
    dispatch(getArea(selectedId));
  };

  const handleSelectCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(
      getListWarehouse({
        search: state.keyword,
        province_id: state.province_id,
        city_id: Number(e.target.value),
      })
    );
  };
  useEffect(() => {
    dispatch(getArea());
  }, []);
  return (
    <div className="w-100 p-3 d-flex flex-row justify-content-between align-items-center">
      <ReactSelect
        onChange={handleSelectProvince}
        label="Provinsi"
        options={listProvince}
        // name="province_id"
        // formik={formik}
        // withValidate
        className="w-100 mr-1"
        placeholder="Pilih Provinsi"
      />
      <ReactSelect
        onChange={handleSelectCity}
        label="Kabupaten/Kota"
        options={listCity}
        // name="province_id"
        // formik={formik}
        // withValidate
        className="w-100  mr-1"
        placeholder="Pilih Kab/Kota"
      />
      {/* <Form.Group controlId="formBasicProvinsi" className="mr-1 mb-0">
        <Form.Label as={'h6'} className="input-label">
          Provinsi
        </Form.Label>
        
        <SelectSearch
          placeholderSearch="Search"
          placeholderSelect="All"
          options={listProvince}
          onChange={handleSelectProvince}
        />
      </Form.Group> */}
      {/* <Form.Group controlId="formBasicKabouatenKota" className="mr-1 mb-0">
        <Form.Label as={'h6'} className="input-label">
          Kota/Kabupaten
        </Form.Label>
        <SelectSearch
          placeholderSearch="Search"
          placeholderSelect="All"
          options={listProvince.map((item) => ({
            key: item.value,
            text: item.label,
          }))}
        />
      </Form.Group> */}
      <SearchField
        className="w-100"
        label="Cari Gudang"
        placeholder="All"
        iconPosition="end"
        onChange={onSearch}
      />
    </div>
  );
}

export default FilterAndSearch;
