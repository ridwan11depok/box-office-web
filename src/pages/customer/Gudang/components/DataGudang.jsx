import React, { useState } from 'react';
import gudang from '../../../../assets/icons/profilegudang.svg';
import { useHistory } from 'react-router-dom';
import StatusGudang from './StatusGudang';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../../../components/elements/Button';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import Avatar from '@material-ui/core/Avatar';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import StarIcon from '@material-ui/icons/Star';
import { useSelector } from 'react-redux';
import MapModal from '../../../../components/elements/Map/MapModal';
import ConfirmRequestGudang from './ConfirmRequestGudang';
import { openInNewTab } from '../../../../utils/helper';

const useStyles = makeStyles({
  gudangName: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },

  btnBlueOutline: {
    marginRight: '5px',
  },
  btnBlueNoFill: {
    marginRight: 0,
    paddingLeft: 0,
  },
  label: {
    fontWeight: '400',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#4F4F4F',
  },
  dataValue: {
    fontWeight: '600',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#363636',
  },
  cycleContainer: {
    width: '40px',
    height: '40px',
    backgroundColor: '#EBEBFF',
    color: '#192A55',
  },
  valueBold: {
    fontWeight: '700',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    color: '#1C1C1C',
  },
  totalReview: {
    fontFamily: 'Open Sans',
    fontSize: '12px',
    color: '#192A55',
    fontWeight: '500',
  },
});
function DataGudang(props) {
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = useState({
    showMap: false,
    showRequest: false,
  });
  const { detailWarehouse } = useSelector((state) => state.gudang);
  const listDeliveryDay = detailWarehouse?.delivery_day
    ? JSON.parse(detailWarehouse.delivery_day).map((item) => ({
      day: item[0].charAt(0).toUpperCase() + item[0].slice(1),
      hour: `${item[1]} - ${item[2]}`,
    }))
    : [];
  return (
    <>
      <div className="d-flex flex-row justify-content-between p-3 flex-wrap">
        <div className="d-flex flex-row w-50">
          {detailWarehouse?.logo_full_url ?
            <img style={{ width: '104px', height: '104px' }} src={detailWarehouse?.logo_full_url} alt={detailWarehouse?.logo} />
            :
            <img style={{ width: '104px', height: '104px' }} src={'https://my-goodlife.com/img.php?imgsrc=&size=400x400'} alt='Image not availabe' />

          } 
          <div className="ml-2">
            <h6 className={classes.label}>Nama Gudang</h6>
            <h6 className={classes.dataValue}>
              {detailWarehouse?.name || 'No Data'}
            </h6>
            <div style={{ flex: 0.5 }} className="pl-0">
              <h6 className={classes.label}>Nomor Telepon</h6>
              <h6 className={classes.dataValue}>
                {detailWarehouse?.phone_number || 'No Data'}
              </h6>
            </div>
          </div>
        </div>
      </div>
      <div className="d-flex flex-row justify-content-between mt-1 p-3">

        <div style={{ flex: 0.5 }}>
          <h6 className={classes.label}>Email</h6>
          <h6 className={classes.dataValue}>
            {detailWarehouse?.email || 'No Data'}
          </h6>
          <h6 className={classes.label}>Alamat</h6>
          <h6 className={classes.dataValue}>
            {detailWarehouse?.address || 'No Data'}
          </h6>
          <Button
            styleType="blueNoFill"
            className={classes.btnBlueNoFill}
            text="Lihat Lokasi"
            startIcon={() => <MapOutlinedIcon />}
            onClick={() => setState({ ...state, showMap: true })}
          />
        </div>
      </div>
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        initialCoordinate={{
          lng: detailWarehouse?.longitude,
          lat: detailWarehouse?.latitude,
        }}
        mode="viewer"
      />
      <ConfirmRequestGudang
        show={state.showRequest}
        onHide={() => setState({ ...state, showRequest: false })}
      />
    </>
  );
}

export default DataGudang;
