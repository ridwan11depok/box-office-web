import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { requestGudang } from '../reduxAction';
import { useHistory } from 'react-router-dom';
import { Form } from 'react-bootstrap';

const ConfirmRequestGudang = (props) => {
  const { detailWarehouse } = useSelector((state) => state.gudang);
  const { isLoading } = useSelector((state) => state.loading);
  const dispatch = useDispatch();
  const history = useHistory();
  const [state, setState] = useState({ error: '' });

  const handleRequest = async () => {
    setState({ ...state, error: '' });
    try {
      await dispatch(requestGudang({ warehouse_id: detailWarehouse?.id }));
      props.onHide();
      history.push('/gudang');
    } catch (err) {
      setState({
        ...state,
        error:
          err?.error?.message ||
          'Terjadi kesalahan dalam mengirim request gudang.',
      });
    }
  };
  useEffect(() => {
    if (!props.show) {
      setState({ ...state, error: '' });
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Kirim Request Penggunaan Gudang"
        bodyClassName="pt-3 pb-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <>
            <p className="text-left">
              Apakah anda yakin akan melakukan request gudang “
              {detailWarehouse?.name}” sebagai tempat penyimpanan anda?
            </p>
            {state.error && (
              <Form.Text className="text-input-error mt-3">
                {state.error}
              </Form.Text>
            )}
          </>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              onClick={props.onHide}
              styleType="lightBlueFill"
              text="Batal"
              className="mr-3"
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Request'}
              onClick={handleRequest}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ConfirmRequestGudang;
