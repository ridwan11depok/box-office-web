import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setDataRequestGudang } from '../reduxAction';
import StatusGudang from './StatusGudang';
import Badge from '../../../../components/elements/Badge';

const useStyles = makeStyles({
  root: {
    padding: '15px',
  },
  containerList: {
    height: '65vh',
    overflowY: 'auto',
  },
  card: {
    width: '100%',
    border: 'none',
    boxShadow: 'none',
    backgroundColor: 'transparent',
    marginTop: '15px',
  },
  media: {
    height: 240,
    borderRadius: '4px',
  },
  titleList: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    color: '#000000',
    fontSize: '18px',
    paddingBottom: '10px',
  },
  warehouseName: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    color: '#000000',
    fontSize: '14px',
  },
  warehouseAddress: {
    fontFamily: 'Open Sans',
    fontWeight: '400',
    color: '#000000',
    fontSize: '14px',
  },
});

export default function ListGudang() {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const onClickCard = (data) => {
    dispatch(setDataRequestGudang(data));
    history.push(`/gudang/request-gudang/${data?.id}`);
  };
  const { listWarehouse } = useSelector((state) => state.gudang);
  return (
    <div className={classes.root}>
      <Typography className={classes.titleList} variant="h5" component="h5">
        List Gudang
      </Typography>
      <div className={classes.containerList}>
        {listWarehouse?.data?.length ?
          listWarehouse?.data?.map((item) => (
            <Card key={item} className={classes.card}>
              <CardActionArea onClick={() => onClickCard(item)}>
                <CardMedia
                  className={classes.media}
                  image={item.logo_full_url}
                  title="Klik untuk Request Gudang"
                />
                <CardContent style={{ margin: '0px', padding: '10px 0 10px 0' }}>
                  <div
                    style={{
                      display: 'flex',
                      justfyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}
                  >
                    <Typography
                      className={classes.warehouseName}
                      variant="h6"
                      component="h6"
                    >
                      {item.name}
                    </Typography>
                    <StatusGudang status={item?.status_approval} />
                  </div>
                  <Typography
                    className={classes.warehouseAddress}
                    variant="body2"
                    component="p"
                  >
                    {item.address}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          )) :
          <div className='d-flex h-100 justify-content-center align-items-center'>
            <span>Tidak ada gudang</span>
          </div>
          
        }
      </div>
    </div>
  );
}
