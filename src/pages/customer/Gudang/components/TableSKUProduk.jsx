import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import { changeQuantitySKUProduct } from '../reduxAction';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '../../../../components/elements/Button';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router-dom';
import Counter from '../../../../components/elements/Counter';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const QtyField = ({ data }) => {
  const dispatch = useDispatch();
  const handleChangeQty = (type) => {
    let newData = { ...data };
    if (data.qty >= 1 && type === 'decrease') {
      newData.qty = data.qty - 1;
      dispatch(changeQuantitySKUProduct(newData));
    } else if (data.qty >= 0 && type === 'increase') {
      newData.qty = data.qty + 1;
      dispatch(changeQuantitySKUProduct(newData));
    }
  };

  return (
    <Counter
      onDecrease={() => handleChangeQty('decrease')}
      onIncrease={() => handleChangeQty('increase')}
      value={data.qty}
    />
  );
};

const EmptySkuProduct = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Upload produk di Master SKU untuk melakukan request gudang dengan
        produk.
      </h6>
      <Button
        onClick={() => history.push('/system-settings/ecommerce')}
        text="Master SKU"
        styleType="blueOutline"
      />
    </div>
  );
};

const HeaderTable = () => {
  const classes = useStyles();
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2">
      <h5 className={classes.titleTable}>SKU Produk</h5>
      <Input
        id="input-with-icon-adornment"
        className={classes.inputSearch}
        placeholder="Cari Product, SKU"
        startAdornment={
          <InputAdornment position="start">
            <SearchIcon style={{ marginLeft: '10px' }} />
          </InputAdornment>
        }
        disableUnderline
      />
    </div>
  );
};
function TableSKUProduk() {
  const dispatch = useDispatch();
  const { skuProduct } = useSelector((state) => state.gudang);
  const renderQtyField = (data) => <QtyField data={data} />;
  return (
    <div>
      <HeaderTable />
      <DataTable
        column={[
          {
            heading: 'SKU',
            key: 'sku',
          },
          {
            heading: 'Produk',
            key: 'product',
          },
          {
            heading: 'Ukuran',
            key: 'size',
          },
          {
            heading: 'Qty',
            render: renderQtyField,
          },
        ]}
        data={skuProduct}
        withNumber={false}
        renderEmptyData={() => <EmptySkuProduct />}
      />
    </div>
  );
}

export default TableSKUProduk;
