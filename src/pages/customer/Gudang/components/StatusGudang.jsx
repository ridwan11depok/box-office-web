import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Badge from '../../../../components/elements/Badge';

const useStyles = makeStyles({
  wareHouseStatusContainer: {
    // backgroundColor: '#EDF7EE',
    // padding: '4px',
    // borderRadius: '32px',
    marginLeft: 'auto',
    // paddingLeft: '8px',
    // paddingRight: '8px',
    // display: 'flex',
    // justifyContent: 'center',
    // alignItems: 'center',
    // margin: 0,
  },
  wareHouseStatus: {
    color: '#1B5E20',
    fontFamily: 'Open Sans',
    fontWeight: '600',
    fontSize: '10px',
    textAlign: 'center',
  },
});

export default function StatusGudang({ status = 0 }) {
  const classes = useStyles();
  return (
    <div className={classes.wareHouseStatusContainer}>
      <Badge
        label={
          status === 0 ? 'Waiting' : status === 1 ? 'Verified' : 'Rejected'
        }
        styleType={status === 0 ? 'yellow' : status === 1 ? 'green' : 'red'}
      />
    </div>
  );
}
