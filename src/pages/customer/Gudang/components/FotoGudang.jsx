import React from 'react';
import Slider from 'react-slick';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  arrowCarousel: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#192A55',
    padding: '5px',
    color: '#FFFFFF',
    borderRadius: '4px',
    fontSize: '12px',
    cursor: 'pointer',
  },
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
});

function ArrowCarousel(props) {
  const { direction, onClick } = props;
  const classes = useStyles();
  const icons = {
    left: () => <NavigateBeforeIcon />,
    right: () => <NavigateNextIcon />,
  };
  return (
    <div onClick={onClick} className={classes.arrowCarousel} onClick={onClick}>
      {icons[direction]()}
    </div>
  );
}

function FotoGudang() {
  const classes = useStyles();
  const carouselRef = React.createRef();
  const settings = {
    infinite: true,
    speed: 500,
    // slidesToShow: 4,
    slidesToScroll: 4,
    variableWidth: true,
    arrows: false,
  };
  const { detailWarehouse } = useSelector((state) => state.gudang);
  if(detailWarehouse?.file_documents?.length){
    return (
      <>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {detailWarehouse?.file_documents?.length ? (
            <ArrowCarousel
              direction="left"
              onClick={() => carouselRef.current.slickPrev()}
            />
          ) : null}
  
          <div style={{ width: '64vw' }}>
            <Slider {...settings} ref={(e) => (carouselRef.current = e)}>
              {detailWarehouse?.file_documents?.map((item) => (
                <div style={{ width: '16vw' }}>
                  <img style={{ width: '16vw', padding: '5px' }} src={item?.url} alt={item?.logo} />
                </div>
              ))}
            </Slider>
          </div>
          {detailWarehouse?.file_documents?.length ? (
            <ArrowCarousel
              direction="right"
              onClick={() => carouselRef.current.slickNext()}
            />
          ) : null}
        </div>
      </>
    );
  }
  else {
    return <div className='text-center'>Foto gudang tidak tersedia</div>
  }
}

export default FotoGudang;
