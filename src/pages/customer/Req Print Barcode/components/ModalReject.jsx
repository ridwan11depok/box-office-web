import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';

const ModalReject = (props) => {
  return (
    <>
      <Modal
        titleModal="Tolak Print"
        titleClassName="title-modal"
        bodyClassName="body-modal  pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <p className="text-left">
            {`Apakah anda yakin untuk menolak proses pembayaran request print barcode?`}
          </p>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#DA101A',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Reject
            </button>
            <button
              onClick={props.onAgree}
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              Batal
            </button>
          </>
        )}
      />
    </>
  );
};

export default ModalReject;
