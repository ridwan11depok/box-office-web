import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import PinInput from 'react-pin-input';

const ModalBayar = (props) => {
  return (
    <>
      <Modal
        titleModal="Masukkan Pin"
        titleClassName="title-modal"
        bodyClassName="body-modal  pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <center>
            <PinInput
              length={6}
              initialValue=""
              secret
              onChange={(value, index) => { }}
              type="numeric"
              inputMode="number"
              style={{ padding: '10px' }}
              inputStyle={{ borderColor: '#192A55' }}
              inputFocusStyle={{ borderColor: 'blue' }}
              onComplete={(value, index) => { }}
              autoSelect={true}
              regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
            />
          </center>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#192A55',
                width: '100%',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Bayar
            </button>
          </>
        )}
      />
    </>
  );
};

export default ModalBayar;
