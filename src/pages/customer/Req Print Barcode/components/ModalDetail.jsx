import React, { useState } from 'react';
import ModalReject from './ModalReject';
import ModalBayar from './ModalBayar';
import Modal from '../../../../components/elements/Modal/Modal';
import Gudang from '../../../../assets/icons/profilegudang.svg';

const ModalDetail = (props) => {
  const [stateOne, setStateOne] = useState({
    showDelete: false,
    dataDelete: '',
    showChangePassword: false,
    dataChangePassword: {},
    showAddPersonil: false,
  });

  const [stateTwo, setStateTwo] = useState({
    showDelete: false,
    dataDelete: '',
    showChangePassword: false,
    dataChangePassword: {},
    showAddPersonil: false,
  });

  const handleHideModalReject = () => {
    setStateOne({ ...stateOne, showDelete: false });
  };

  const handleAgreeModalReject = () => {
    setStateOne({ ...stateOne, showDelete: false });
  };

  const handleHideModalBayar = () => {
    setStateTwo({ ...stateTwo, showDelete: false });
  };

  const handleAgreeModalBayar = () => {
    setStateTwo({ ...stateTwo, showDelete: false });
  };

  return (
    <>
      <Modal
        titleModal="Request Print"
        titleClassName="title-modal"
        bodyClassName="body-modal  pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <div>
            <div
              style={{
                height: '50px',
                display: 'flex',
                paddingLeft: '10px',
              }}
            >
              <div>
                <img
                  src={Gudang}
                  style={{
                    marginLeft: '-12px',
                    marginTop: '3px',
                  }}
                />
              </div>
              <div className="jarak-gudang">
                <span className="gudang-a">{data.gudang}</span>
                <br></br>
                <span className="lokasi-gudang">Bandung, Jawa Barat</span>
              </div>
            </div>
            <p className="text-left">
              {`Anda memiliki request print barcode dari gudang. 
                Silahkan melakukan pembayaran untuk melakukan print barcode.`}
            </p>
            <div className="table-responsive">
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th className="th">SKU</th>
                    <th className="th">Produk</th>
                    <th className="th">Qty Print</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>SKU-12345678991023</td>
                    <td>Shoes ABC</td>
                    <td>2</td>
                  </tr>
                  <tr>
                    <td>SKU-12345678991023</td>
                    <td>Bag 123</td>
                    <td>2</td>
                  </tr>
                  <tr>
                    <td>SKU-12345678991023</td>
                    <td>Sandal ABC</td>
                    <td>3</td>
                  </tr>
                  <tr>
                    <td colSpan={2} style={{ textAlign: 'end' }}>
                      Total Item
                    </td>
                    <td>7</td>
                  </tr>
                  <tr>
                    <td
                      colSpan={2}
                      style={{
                        textAlign: 'end',
                        backgroundColor: '#F2F2F2',
                      }}
                    >
                      Harga Label
                    </td>
                    <td style={{ backgroundColor: '#F2F2F2' }}>Rp. 250</td>
                  </tr>
                  <tr>
                    <td
                      colSpan={2}
                      style={{
                        textAlign: 'end',
                        backgroundColor: '#F2F2F2',
                      }}
                    >
                      Total Pembayaran
                    </td>
                    <td style={{ backgroundColor: '#F2F2F2' }}>Rp. 1.750</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <button
              // onClick={props.onAgree}
              onClick={() => setStateOne({ ...stateOne, showDelete: true })}
              style={{
                backgroundColor: '#FDE7E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#DA101A',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              Reject
            </button>
            <ModalReject
              show={stateOne.showDelete}
              onHide={handleHideModalReject}
              data={stateOne.dataDelete}
              onAgree={handleAgreeModalReject}
            />
            <button
              className="mr-2"
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              // onClick={props.onHide}
              onClick={() => setStateTwo({ ...stateTwo, showDelete: true })}
            >
              Bayar
            </button>
            <ModalBayar
              show={stateTwo.showDelete}
              onHide={handleHideModalBayar}
              data={stateTwo.dataDelete}
              onAgree={handleAgreeModalBayar}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalDetail;
