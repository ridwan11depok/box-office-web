import React, { useState } from 'react';
import './style/style.css';
import Pagination from '../../../components/elements/Table/Pagination';
import Select from 'react-select';
import { useHistory } from 'react-router-dom';
import { Row, Col, Form, Button, Table } from 'react-bootstrap';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import tambahan from '../../../assets/icons/tambahan.svg';

const ReqPrintBarcode = (props) => {

  const history = useHistory();
  const {
    data,
    column,
    transformData,
    size: view,
    renderHeader,
    sizeOptions,
    withNumber
  } = props;
  const [state, setState] = useState({ page: 1, size: view });
  const handleChangePage = (page) => {
    setState({ ...state, page });
  };

  const options1 = [
    { value: 'a', label: 'Request' },
    { value: 'b', label: 'Reject' },
    { value: 'c', label: 'Done' }
  ]


  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12">
            <div className="section-stock">
              <h1>Request Print Barcode</h1>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px' }}
        >
          <div className="col-lg-12">
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Produk</label>
                  <Select options={options1} />
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">Dari tanggal</Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">Sampai tanggal</Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
            </div>
            <div className="row"
              style={{
                borderBottom: "1px solid #E8E8E8"
              }}
            >
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Tanggal</th>
                    <th>Gudang</th>
                    <th>Total SKU</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody
                  style={{
                    height: "417px"
                  }}
                >
                  <tr>
                    <td colSpan={5}
                      style={{
                        textAlign: "center"
                      }}
                    >
                      <p>Anda belum memilliki “Request Print Barcode” dari warehouse</p>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <div className="row">
              <div className="col-lg-9">
                <text className="text-view">
                  View
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-3">
                <div
                  style={{
                    float: "right",
                    paddingTop: "15px"
                  }}>
                  <Pagination
                    activePage={state.page}
                    changePage={handleChangePage}
                    totalPage={7}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default ReqPrintBarcode;
