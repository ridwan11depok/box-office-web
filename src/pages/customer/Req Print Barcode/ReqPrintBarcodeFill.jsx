import React, { useState } from 'react';
import './style/style.css';
import ModalDetail from './components/ModalDetail';
import DataTable from '../../../components/elements/Table/Table';
import Select from 'react-select';
import { Row, Col, Form, Button, Pagination, Modal } from 'react-bootstrap';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import tambahan from '../../../assets/icons/tambahan.svg';

const ReqPrintBarcodeFill = () => {
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [show5, setShow5] = useState(false);

  const [state, setState] = useState({
    showDelete: false,
    dataDelete: '',
    showChangePassword: false,
    dataChangePassword: {},
    showAddPersonil: false,
  });

  const handleHideModalDetail = () => {
    setState({ ...state, showDelete: false });
  };

  const handleAgreeModalDetail = () => {
    setState({ ...state, showDelete: false });
  };

  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  const handleClose4 = () => setShow4(false);
  const handleShow4 = () => setShow4(true);

  const handleClose5 = () => setShow5(false);
  const handleShow5 = () => setShow5(true);

  const options1 = [
    { value: 'a', label: 'Request' },
    { value: 'b', label: 'Reject' },
    { value: 'c', label: 'Done' },
  ];

  const renderDetail = (data) => {
    return (
      <div>
        <button
          onClick={() =>
            setState({ ...state, showDelete: true, dataDelete: data })
          }
          type="button"
          className="btn"
          style={{
            backgroundColor: '#E8E8E8',
            borderRadius: '4px',
            borderWidth: 2,
          }}
        >
          <text className="btn-right" style={{ color: '#192A55' }}>
            Detail
          </text>
        </button>
      </div>
    );
  };

  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="section-stock">
              <h1>Request Print Barcode</h1>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{
            border: '1px solid #E8E8E8',
            borderRadius: '5px',
            borderBottom: 'none',
          }}
        >
          <div className="col-lg-12">
            <div className="row" style={{}}>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Produk</label>
                  <Select options={options1} />
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">
                      Dari tanggal
                    </Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">
                      Sampai tanggal
                    </Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <DataTable
            column={[
              {
                heading: 'Tanggal',
                key: 'tanggal',
              },
              {
                heading: 'Gudang',
                key: 'gudang',
              },
              {
                heading: 'Total SKU',
                key: 'totalsku',
              },
              {
                heading: 'Status',
                key: 'status',
              },
              { heading: 'Aksi', render: renderDetail },
            ]}
            data={[
              {
                tanggal: '30 Juni 2021',
                gudang: 'Gudang A',
                totalsku: '5',
                status: 'Request',
              },
              {
                tanggal: '30 Juni 2021',
                gudang: 'Gudang B',
                totalsku: '8',
                status: 'Request',
              },
              {
                tanggal: '30 Juni 2021',
                gudang: 'Gudang C',
                totalsku: '5',
                status: 'Reject',
              },
              {
                tanggal: '30 Juni 2021',
                gudang: 'Gudang D',
                totalsku: '5',
                status: 'Reject',
              },
              {
                tanggal: '30 Juni 2021',
                gudang: 'Gudang E',
                totalsku: '5',
                status: 'Request',
              },
            ]}
            transformData={(item) => ({
              ...item,
              tanggal: item.tanggal,
              gudang: item.gudang,
              totalsku: item.totalsku,
              status: item.status,
            })}
            withNumber={false}
          />
        </div>
      </section>
      <ModalDetail
        show={state.showDelete}
        onHide={handleHideModalDetail}
        data={state.dataDelete}
        onAgree={handleAgreeModalDetail}
      />
    </div>
  );
};

export default ReqPrintBarcodeFill;
