import React from 'react';
import './style/style.css';
import { Row, Col, Form, Button, Select, Pagination } from 'react-bootstrap';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import exportmutasi from '../../../assets/icons/exportmutasi.svg';
import date from '../../../assets/icons/date.svg';

const SemuaTransaksi = () => {
  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-12">
            <div className="section-stock">
              <h1>List Pindah Gudang</h1>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{
            marginBottom: '15px',
            border: '1px solid #E8E8E8',
            borderRadius: '5px',
          }}
        >
          <div
            className="col-lg-3"
            style={{ paddingTop: '4px', paddingBottom: '4px' }}
          >
            <div
              className=""
              style={{
                backgroundColor: '#EBF0FA',
                height: '40px',
                borderRadius: '4px',
                marginLeft: '-10px',
              }}
            >
              <p className="transaksi">Semua Transaksi</p>
            </div>
          </div>
          <div className="col-lg-3">
            <div
              className=""
              style={{ backgroundColor: '#ffff', height: '40px' }}
            >
              <p className="non-transaksi">Transaksi Pending</p>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="">
              <p className="non-transaksi">Transaksi Sent</p>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="">
              <p className="non-transaksi">Transaksi Done</p>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px' }}
        >
          <div className="col-lg-12">
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <div
                  className="input-group"
                  style={{ paddingTop: '10px', paddingBottom: '10px' }}
                >
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search"
                  />
                </div>
              </div>
              <div className="col-lg-4"></div>
              <div className="col-lg-4"></div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-3">
                <p className="table-heading">No Transaksi</p>
              </div>
              <div className="col-lg-3">
                <p className="table-heading">Qty SKU</p>
              </div>
              <div className="col-lg-3">
                <p className="table-heading">Qty Item</p>
              </div>
              <div className="col-lg-3">
                <p className="table-heading">Status</p>
              </div>
            </div>
            <div
              className="row"
              style={{ height: '440px', borderBottom: '1px solid #E8E8E8' }}
            >
              <div
                className="col-lg-12"
                style={{ padding: '200px', textAlign: 'center' }}
              >
                <p>Kamu belum memiliki proses pindah gudang</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-10">
                <text className="text-view">
                  View
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-2">
                <text className="pagination">
                  <img src={prev} style={{ paddingRight: '10px' }} />
                  Prev
                  <img
                    src={pagination}
                    style={{
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      marginTop: '5px',
                      marginBottom: '5px',
                    }}
                  />
                  Next
                  <img src={next} style={{ paddingLeft: '10px' }} />
                </text>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SemuaTransaksi;
