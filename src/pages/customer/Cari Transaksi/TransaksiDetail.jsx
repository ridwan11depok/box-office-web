import React from 'react';
import { Link } from 'react-router-dom';
import './style/style.css';
import left from '../../../assets/icons/short_left.svg';
import speed from '../../../assets/icons/speed.svg';
import Invoice from '../../../assets/images/Img.svg';

const TransaksiDetail = () => {
  return (
    <div className="main-content">
      <div className="section">
        <div className="row back">
          <div className="col-lg-12">
            <div style={{ display: 'flex', paddingBottom: '20px' }}>
              <button
                type="button"
                className="btn"
                style={{
                  borderRadius: '4px',
                  backgroundColor: '#F2F2F2',
                  height: '40px',
                }}
              >
                <text className="btn-export">
                  <img src={left} style={{}} />
                </text>
              </button>
              <h1 className="text-back">0123456789101112</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div
              style={{
                height: '456px',
                border: '1px solid #E8E8E8',
                borderRadius: '4px',
              }}
            >
              <div className="row">
                <div className="col-lg-12">
                  <p className="text-invoice">Invoice</p>
                  <div
                    className=""
                    style={{
                      height: '132px',
                      border: '1px dashed #E8E8E8',
                      marginLeft: '20px',
                      marginRight: '20px',
                    }}
                  >
                    <img src={Invoice} style={{ width: '100%' }} />
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="jarak top">
                    <p className="text-heading">No Invoice</p>
                    <p className="text-isi">0123456789101112</p>
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="jarak">
                    <p className="text-heading">Pesan</p>
                    <p className="text-isi">Packing dengan rapi</p>
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="jarak">
                    <p className="text-heading">Logistik</p>
                    <p className="text-isi">JNE COD</p>
                  </div>
                </div>
                <div className="col-lg-12">
                  <div className="jarak">
                    <p className="text-heading">AWB</p>
                    <p className="text-isi">BOOOD123456789</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-12">
                <div
                  className="row"
                  style={{
                    height: '190px',
                    border: '1px solid #E8E8E8',
                    borderRadius: '4px',
                  }}
                >
                  <div className="col-lg-12">
                    <p className="text-invoice">Data Pengirim</p>
                  </div>
                  <div className="col-lg-12">
                    <div className="jarak">
                      <p className="text-heading">Nama Pengirim</p>
                      <p className="text-isi">Bessie Cooper</p>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="jarak">
                      <p className="text-heading">Nomor Telepon Pelanggan</p>
                      <p className="text-isi">(+62) 8123456789</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12" style={{ paddingTop: '15px' }}>
                <div
                  className="row"
                  style={{
                    height: '250px',
                    border: '1px solid #E8E8E8',
                    borderRadius: '4px',
                  }}
                >
                  <div className="col-lg-12">
                    <p className="text-invoice">Data Penerima</p>
                  </div>
                  <div className="col-lg-12">
                    <div className="jarak">
                      <p className="text-heading">Nama Penerima</p>
                      <p className="text-isi">Leslie Alexander</p>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="jarak">
                      <p className="text-heading">Nomor Telepon Penerima</p>
                      <p className="text-isi">(+62) 8123456789</p>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <div className="jarak">
                      <p className="text-heading">Alamat</p>
                      <p className="text-isi">
                        Sekeloa utara 2 Coblong, Bandung, Jawa Barat
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row" style={{ paddingTop: '16px' }}>
          <div className="col-lg-12">
            <div style={{ border: '1px solid #E8E8E8', height: '56px' }}>
              <p className="jarak top1 text-gudang">Produk Dikirim</p>
            </div>
          </div>
          <div className="col-lg-12">
            <div
              className="table-responsive"
              style={{ border: '1px solid #E8E8E8' }}
            >
              <table class="table table-striped" id="table-1">
                <thead>
                  <tr>
                    <th className="th">Produk</th>
                    <th className="th" style={{ width: '150px' }}>
                      Qty
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="td">Shoes ABC</td>
                    <td className="td">1</td>
                  </tr>
                  <tr>
                    <td className="td">Bag 123</td>
                    <td className="td">1</td>
                  </tr>
                  <tr>
                    <td className="td">Sandal ABC</td>
                    <td className="td">1</td>
                  </tr>
                  <tr>
                    <td className="td">Headset ABC</td>
                    <td className="td">1</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ paddingTop: '16px', paddingBottom: '16px' }}
        >
          <div className="col-lg-12">
            <div style={{ textAlign: 'end' }}>
              <Link to="/cari-transaksi-fill" className="a-login">
                <button
                  type="button"
                  className="btn"
                  style={{
                    borderRadius: '4px',
                    backgroundColor: '#E8E8E8',
                    height: '32px',
                  }}
                >
                  <text className="btn-export" style={{ display: 'block' }}>
                    Tutup
                  </text>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TransaksiDetail;
