import React from 'react';
// import './style/style.css'
import { Row, Col, Form, Button, Select, Pagination } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import exportmutasi from '../../../assets/icons/exportmutasi.svg';
import date from '../../../assets/icons/date.svg';

const CariTransaksiFill = () => {
  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-12">
            <div className="section-stock">
              <h1>Cari Transaksi</h1>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{
            marginBottom: '15px',
            border: '1px solid #E8E8E8',
            borderRadius: '5px',
          }}
        >
          <div className="col-lg-3">
            <div className="form-group">
              <label className="table-heading">Cari Gudang</label>
              <select className="form-control">
                <option>Semua Gudang</option>
                <option>Status 1</option>
                <option>Status 2</option>
              </select>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="form-group">
              <label className="table-heading">Produk</label>
              <select className="form-control">
                <option>Semua Produk</option>
                <option>Status 1</option>
                <option>Status 2</option>
              </select>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="form-group">
              <label className="table-heading">Dari tanggal</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <div className="input-group-text">
                    <img
                      src={date}
                      style={{ marginRight: '-15px', marginLeft: '-15px' }}
                    />
                  </div>
                </div>
                <input type="text" className="form-control daterange-cus" />
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="form-group">
              <label className="table-heading">Sampai tanggal</label>
              <div className="input-group">
                <div className="input-group-prepend">
                  <div className="input-group-text">
                    <img
                      src={date}
                      style={{ marginRight: '-15px', marginLeft: '-15px' }}
                    />
                  </div>
                </div>
                <input type="text" className="form-control daterange-cus" />
              </div>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px' }}
        >
          <div className="col-lg-12">
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <div
                  className="input-group"
                  style={{ paddingTop: '10px', paddingBottom: '10px' }}
                >
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Cari pembeli, invoice dan resi"
                  />
                </div>
              </div>
              <div className="col-lg-4"></div>
              <div className="col-lg-4"></div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-1">
                <p className="table-heading">No</p>
              </div>
              <div className="col-lg-4">
                <p className="table-heading">Invoice</p>
              </div>
              <div className="col-lg-4">
                <p className="table-heading">Timestamp</p>
              </div>
              <div className="col-lg-3">
                <p className="table-heading">Aksi</p>
              </div>
            </div>
            <div
              className="row"
              style={{
                borderBottom: '1px solid #E8E8E8',
                backgroundColor: '#F2F2F2',
              }}
            >
              <div className="col-lg-1">
                <p className="fill text">1</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">0123456789101112</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">2021-07-04 13:29:57</p>
              </div>
              <div className="col-lg-3">
                <div
                  className=""
                  style={{ paddingTop: '6px', paddingBottom: '4px' }}
                >
                  <Link to="/transaksi-detail" className="a-login">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        backgroundColor: '#E8E8E8',
                        height: '32px',
                      }}
                    >
                      <text className="btn-export" style={{ display: 'block' }}>
                        Detail
                      </text>
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-1">
                <p className="fill text">2</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">0123456789101112</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">2021-07-04 13:29:57</p>
              </div>
              <div className="col-lg-3">
                <div
                  className=""
                  style={{ paddingTop: '6px', paddingBottom: '4px' }}
                >
                  <Link to="/transaksi-detail" className="a-login">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        backgroundColor: '#E8E8E8',
                        height: '32px',
                      }}
                    >
                      <text className="btn-export" style={{ display: 'block' }}>
                        Detail
                      </text>
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div
              className="row"
              style={{
                borderBottom: '1px solid #E8E8E8',
                backgroundColor: '#F2F2F2',
              }}
            >
              <div className="col-lg-1">
                <p className="fill text">3</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">0123456789101112</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">2021-07-04 13:29:57</p>
              </div>
              <div className="col-lg-3">
                <div
                  className=""
                  style={{ paddingTop: '6px', paddingBottom: '4px' }}
                >
                  <Link to="/transaksi-detail" className="a-login">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        backgroundColor: '#E8E8E8',
                        height: '32px',
                      }}
                    >
                      <text className="btn-export" style={{ display: 'block' }}>
                        Detail
                      </text>
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-1">
                <p className="fill text">4</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">0123456789101112</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">2021-07-04 13:29:57</p>
              </div>
              <div className="col-lg-3">
                <div
                  className=""
                  style={{ paddingTop: '6px', paddingBottom: '4px' }}
                >
                  <Link to="/transaksi-detail" className="a-login">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        backgroundColor: '#E8E8E8',
                        height: '32px',
                      }}
                    >
                      <text className="btn-export" style={{ display: 'block' }}>
                        Detail
                      </text>
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div
              className="row"
              style={{
                borderBottom: '1px solid #E8E8E8',
                backgroundColor: '#F2F2F2',
              }}
            >
              <div className="col-lg-1">
                <p className="fill text">5</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">0123456789101112</p>
              </div>
              <div className="col-lg-4">
                <p className="fill text">2021-07-04 13:29:57</p>
              </div>
              <div className="col-lg-3">
                <div
                  className=""
                  style={{ paddingTop: '6px', paddingBottom: '4px' }}
                >
                  <Link to="/transaksi-detail" className="a-login">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        backgroundColor: '#E8E8E8',
                        height: '32px',
                      }}
                    >
                      <text className="btn-export" style={{ display: 'block' }}>
                        Detail
                      </text>
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div
              className="row"
              style={{ height: '236px', borderBottom: '1px solid #E8E8E8' }}
            >
              {/* <div className="col-lg-12" style={{ padding: "200px", textAlign: "center" }}>
                                <p>Belum ada data transaksi saat ini</p>
                            </div> */}
            </div>
            <div className="row">
              <div className="col-lg-10">
                <text className="text-view">
                  View
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-2">
                <text className="pagination">
                  <img src={prev} style={{ paddingRight: '10px' }} />
                  Prev
                  <img
                    src={pagination}
                    style={{
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      marginTop: '5px',
                      marginBottom: '5px',
                    }}
                  />
                  Next
                  <img src={next} style={{ paddingLeft: '10px' }} />
                </text>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default CariTransaksiFill;
