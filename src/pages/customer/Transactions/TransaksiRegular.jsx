import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormPengirimOrPenerima from './components/FormPengirimOrPenerima';
import './styles.css';
import { TabHeader, TabContent } from '../../../components/elements/Tabs/';
import FormDetailPaket from './components/FormDetailPaket';
import PilihKurir from './components/PilihKurir';

const itemTab = (props, item) => {
  const { activeColor, activeTab, inActiveColor } = props;
  const { tab, title } = item;
  return (
    <div
      className="w-100"
      style={{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        fontWeight: '600',
        color:
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF',
        textAlign: 'left',
        borderBottom: `8px solid ${
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF'
        }`,
      }}
    >
      <h6>
        <span
          style={{
            borderRadius: '100%',
            backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
            width: '24px',
            height: '24px',
            padding: '3px 5px',
          }}
        >
          0{tab}
        </span>{' '}
        {title}
      </h6>
    </div>
  );
};

const Transactions = () => {
  const history = useHistory();
  const [step, setStep] = useState(1);
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Transaksi Regular"
              onClick={() => history.push('/transactions')}
            />
          </div>
        )}
      >
        <ContentItem border={false} spaceBottom={3}>
          <TabHeader
            border={false}
            activeTab={step}
            activeBackground="transparent"
            inActiveBackground="transparent"
            listHeader={[
              {
                tab: 1,
                title: 'Data Pengirim dan Penerima',
                render: itemTab,
              },
              {
                tab: 2,
                title: 'Detail Paket',
                render: itemTab,
              },
              {
                tab: 3,
                title: 'Pilih Kurir',
                render: itemTab,
              },
            ]}
            isClickable={false}
          />
        </ContentItem>
        <TabContent tab={1} activeTab={step}>
          <ContentItem
            className="p-3"
            spaceRight="0 pr-md-2"
            col="col-12 col-md-6"
            spaceBottom={3}
          >
            <FormPengirimOrPenerima type="Pengirim" />
          </ContentItem>
          <ContentItem
            className="p-3"
            spaceRight="0 pl-md-2"
            col="col-12 col-md-6"
            spaceBottom={3}
          >
            <FormPengirimOrPenerima type="Penerima" />
          </ContentItem>
        </TabContent>
        <TabContent tab={2} activeTab={step}>
          <ContentItem className="p-3" spaceBottom={3}>
            <FormDetailPaket />
          </ContentItem>
        </TabContent>

        <TabContent tab={3} activeTab={step}>
          <ContentItem spaceBottom={3}>
            <PilihKurir />
          </ContentItem>
        </TabContent>

        <div className="ml-auto">
          <Button
            style={{ width: '140px' }}
            styleType="lightBlueFill"
            text="Batal"
            onClick={() => {
              step === 1 && history.push('/transactions');
              step === 2 && setStep(1);
              step === 3 && setStep(2);
            }}
          />
          <Button
            style={{ minWidth: '140px', marginLeft: '10px' }}
            styleType="blueFill"
            text={
              step === 1
                ? 'Simpan dan Isi Data Paket'
                : step === 2
                ? 'Simpan dan Pilih Kurir'
                : 'Request Pickup'
            }
            onClick={() => {
              step === 1 && setStep(2);
              step === 2 && setStep(3);
            }}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default Transactions;
