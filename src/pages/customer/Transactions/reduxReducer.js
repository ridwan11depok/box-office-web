import actionType from './reduxConstant';

const initialState = {
  pageDropshipDetail: {},
};

const transactionsReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.SET_DATA_PAGE_DROPSHIP_DETAIL:
      return {
        ...prevState,
        pageDropshipDetail: action.payload,
      };
    default:
      return prevState;
  }
};

export default transactionsReducer;
