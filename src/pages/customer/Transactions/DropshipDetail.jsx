import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import ProdukDikirimTable from './components/ProdukDikirimTable';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInvoice from './components/FormInvoice';
import FormDataPengirim from './components/FormDataPengirim';
import FormDataPenerima from './components/FormDataPenerima';
import { useSelector } from 'react-redux';
import Badge from '../../../components/elements/Badge';
import CancelTransactionModal from './components/CancelTransactionModal';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const DropshipDetail = () => {
  const classes = useStyles();
  const history = useHistory();
  const { pageDropshipDetail } = useSelector((state) => state.transactions);
  const [state, setState] = useState({
    isEdit: false,
    cancelTransactionModal: false,
  });
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label={pageDropshipDetail.noinvoice || 'Kembali'}
              onClick={() => history.push('/transactions')}
            />
            <Badge
              style={{
                fontSize: '14px',
                fontWeight: '700',
                padding: '16px 8px',
              }}
              styleType={
                pageDropshipDetail.status === 'pending' ? 'black' : 'green'
              }
              label={
                pageDropshipDetail.status === 'pending' ? 'Pending' : 'Done'
              }
            />
          </div>
        )}
      >
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: state.isEdit ? '615px' : '450px' }}
        >
          <FormInvoice isEdit={state.isEdit} data={pageDropshipDetail} />
        </ContentItem>
        <ContentItem
          border={false}
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            minHeight: state.isEdit ? '615px' : '450px',
          }}
        >
          <div
            className="p-3 mb-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPengirim isEdit={state.isEdit} data={pageDropshipDetail} />
          </div>
          <div
            className="p-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPenerima isEdit={state.isEdit} data={pageDropshipDetail} />
          </div>
        </ContentItem>
        <ContentItem
          className="mt-3"
          border={false}
          col="col-12"
          spaceBottom={3}
        >
          <ProdukDikirimTable isEdit={state.isEdit} data={pageDropshipDetail} />
        </ContentItem>
        {state.isEdit && pageDropshipDetail.status === 'pending' ? (
          <div className="ml-auto">
            <Button
              style={{ width: '140px', marginRight: '10px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={() => setState({ ...state, isEdit: false })}
            />
            <Button
              style={{ width: '140px' }}
              styleType="blueFill"
              text="Kirim"
            />
          </div>
        ) : !state.isEdit && pageDropshipDetail.status === 'pending' ? (
          <ContentItem
            className="mt-3 d-flex flex-row justify-content-between"
            border={false}
            col="col-12"
            spaceBottom={3}
          >
            <Button
              style={{ minWidth: '140px', marginRight: '10px' }}
              styleType="redOutline"
              text="Batal Transaksi"
              onClick={() =>
                setState({
                  ...state,
                  isEdit: false,
                  cancelTransactionModal: true,
                })
              }
            />
            <Button
              className="ml-auto"
              style={{ minWidth: '140px' }}
              styleType="blueFill"
              text="Edit Transaksi"
              onClick={() => setState({ ...state, isEdit: true })}
            />
          </ContentItem>
        ) : (
          <div className="ml-auto">
            <Button
              style={{ width: '140px' }}
              styleType="lightBlueFill"
              text="Tutup"
            />
          </div>
        )}
      </ContentContainer>
      <CancelTransactionModal
        data={pageDropshipDetail}
        show={state.cancelTransactionModal}
        onHide={() => setState({ ...state, cancelTransactionModal: false })}
      />
    </>
  );
};

export default DropshipDetail;
