import actionType from './reduxConstant';

export const setDataPageDropshipDetail = (data) => {
  return {
    type: actionType.SET_DATA_PAGE_DROPSHIP_DETAIL,
    payload: data,
  };
};
