import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import ProdukDikirimTable from './components/ProdukDikirimTable';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInvoice from './components/FormInvoice';
import FormDataPengirim from './components/FormDataPengirim';
import FormDataPenerima from './components/FormDataPenerima';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const Transactions = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Dropship"
              onClick={() => history.push('/transactions')}
            />
          </div>
        )}
      >
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <FormInvoice />
        </ContentItem>
        <ContentItem
          border={false}
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            minHeight: '615px',
          }}
        >
          <div
            className="p-3 mb-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPengirim />
          </div>
          <div
            className="p-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPenerima />
          </div>
        </ContentItem>
        {/* <ContentItem container border={false} spaceBottom={3}>
          <ContentItem
            item
            className="p-3"
            style={{ flexBasis: '49%' }}
          >
            <FormInvoice />
          </ContentItem>
          <ContentItem
            border={false}
            item
            className="ml-auto"
            style={{
              flexBasis: '49%',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}
          >
            <div
              className="p-3 mb-3"
              style={{
                flex: 0.49,
                border: '1px solid #E8E8E8',
                borderRadius: '4px',
              }}
            >
              <FormDataPengirim />
            </div>
            <div
              className="p-3"
              style={{
                flex: 0.49,
                border: '1px solid #E8E8E8',
                borderRadius: '4px',
              }}
            >
              <FormDataPengirim />
            </div>
          </ContentItem>
        </ContentItem> */}
        <ContentItem
          className="mt-3"
          border={false}
          col="col-12"
          spaceBottom={3}
        >
          <ProdukDikirimTable />
        </ContentItem>
        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text="Kirim"
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default Transactions;
