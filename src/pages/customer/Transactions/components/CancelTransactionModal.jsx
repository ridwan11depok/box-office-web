import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';

const CancelTransactionModal = (props) => {
  return (
    <>
      <Modal
        titleModal="Batal Transaksi"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        data={props.data}
        content={(data) => (
          <>
            <h6
              className="mt-3 mb-3"
              style={{
                fontSize: '14px',
                fontWeight: '400',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
              }}
            >
              {`Apakah anda yakin akan membatalkan invoice ${
                data?.noinvoice || ''
              } dari proses transaksi?`}
            </h6>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text="Batal Transaksi"
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Kembali"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

export default CancelTransactionModal;
