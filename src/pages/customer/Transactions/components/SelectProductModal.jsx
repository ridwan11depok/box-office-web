import React, { useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { SearchField } from '../../../../components/elements/InputField';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import CloseIcon from '@material-ui/icons/Close';

const listProduct = [
  { id: 1, product: 'Baju Kemeja', stock: 10 },
  { id: 2, product: 'Celana Bahan', stock: 10 },
  { id: 3, product: 'Sepatu Adidas', stock: 10 },
  { id: 4, product: 'Topi Boboboy', stock: 10 },
  { id: 5, product: 'Sarung', stock: 10 },
  { id: 6, product: 'Jas', stock: 10 },
  { id: 7, product: 'Peci', stock: 10 },
];
const SelectProductModal = (props) => {
  const [selected, setSelected] = useState([]);
  const onChange = (e, productItem) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setSelected([...selected, productItem]);
      console.log([...selected, productItem]);
    } else {
      let newSelected = selected.filter((item) => item.id !== productItem.id);
      setSelected(newSelected);
      console.log(newSelected);
    }
  };
  const handleDelete = (product) => {
    let newSelected = selected.filter((item) => item.id !== product.id);
    setSelected(newSelected);
  };
  const [products, setProducts] = useState(listProduct);
  const handleSearch = (e) => {
    let value = e.target.value;
    let filtered = listProduct.filter((item) =>
      item.product.toLowerCase().includes(value.toLowerCase())
    );
    setProducts(filtered);
  };
  return (
    <>
      <Modal
        titleModal="Pilih Produk"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <>
            <Container withHeader={false}>
              <Item spaceBottom={3}>
                <div
                  className="p-2"
                  style={{ borderBottom: '1px solid #E8E8E8' }}
                >
                  <SearchField
                    onChange={handleSearch}
                    placeholder="Cari produk"
                  />
                </div>
                <div style={{ height: '250px', overflowY: 'auto' }}>
                  <table className="table table-striped mt-0 mb-5">
                    <tr
                      className="pt-0 pb-0"
                      style={{
                        borderBottom: '1px solid #E8E8E8',
                      }}
                    >
                      <th scope="col">Produk</th>
                      <th scope="col">Stock</th>
                    </tr>
                    <tbody>
                      {products.map((item, idx) => (
                        <tr
                          key={idx.toString()}
                          style={{ borderBottom: '1px solid #E8E8E8' }}
                        >
                          <td>
                            <Checkbox
                              color=""
                              style={{ color: '#192A55' }}
                              checked={
                                selected.filter((prod) => prod.id === item.id)
                                  .length
                              }
                              onChange={(e) => onChange(e, item)}
                            />
                            {item.product}
                          </td>
                          <td>{item.stock}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Item>
              <Item className="p-2">
                <h6
                  style={{
                    fontSize: '14px',
                    fontWeight: '600',
                    color: '#1c1c1c',
                    fontFamily: 'Open Sans',
                  }}
                >
                  {selected.length} Produk yang dipilih
                </h6>
                {selected.map((item) => (
                  <Chip
                    label={item.product}
                    // clickable
                    color=""
                    style={{
                      color: '#192A55',
                      border: '2px solid #192A55',
                      fontWeight: '600',
                      marginLeft: '5px',
                      marginBottom: '10px',
                    }}
                    onDelete={() => handleDelete(item)}
                    deleteIcon={<CloseIcon style={{ color: '#192A55' }} />}
                    variant="outlined"
                  />
                ))}
              </Item>
            </Container>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="blueFill"
              text="Tambah"
            />
          </div>
        )}
      />
    </>
  );
};

export default SelectProductModal;
