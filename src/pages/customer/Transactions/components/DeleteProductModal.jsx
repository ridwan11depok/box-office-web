import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';

const DeleteProductModal = (props) => {
  return (
    <>
      <Modal
        titleModal="Hapus Transaksi"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <>
            <h6
              className="mt-3 mb-3"
              style={{
                fontSize: '14px',
                fontWeight: '400',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
              }}
            >
              {`Apakah anda yakin akan menghapus "${
                data?.product || ''
              }" dari proses transaksi?`}
            </h6>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text="Hapus"
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

export default DeleteProductModal;
