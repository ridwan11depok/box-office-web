import React, { useState } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import { TabHeader } from '../../../../components/elements/Tabs/';
import { kurirOptions } from './dataDummy';

function PilihKurir() {
  const [state, setState] = useState({
    tabKurir: 1,
    selectedKurir: { name: '' },
  });
  return (
    <>
      <Row className="no-gutters" style={{ borderBottom: '1px solid #E8E8E8' }}>
        <Col className="no-gutters d-flex align-items-center p-3 col-12 col-md-6">
          <h6>Pilih Kurir</h6>
        </Col>
        <Col className="p-2 pl-0 col-12 col-md-6">
          <div
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
              width: '100%',
            }}
          >
            <TabHeader
              listHeader={[
                {
                  title: 'Regular',
                  tab: 1,
                },
                {
                  title: 'Express',
                  tab: 2,
                },
                {
                  title: 'Trucking',
                  tab: 3,
                },
              ]}
              activeTab={state.tabKurir}
              border={false}
              onChange={(tab) => setState({ ...state, tabKurir: tab })}
            />
          </div>
        </Col>
      </Row>
      <Row className="no-gutters p-3">
        {kurirOptions.map((item, idx) => (
          <Col
            className={`col-12 col-md-6 ${
              idx % 2 === 0 ? 'pr-0 pr-md-2' : 'pl-0 pl-md-2'
            } mb-2`}
          >
            <div
              onClick={() => setState({ ...state, selectedKurir: item })}
              style={{
                border:
                  state.selectedKurir.name === item.name
                    ? '1px solid #192A55'
                    : '1px solid #E8E8E8',
                backgroundColor:
                  state.selectedKurir.name === item.name && '#EBF0FA',
                cursor: 'pointer',
              }}
              className="w-100 d-flex flex-row rounded p-2"
            >
              <div style={{ flex: 0.3 }}>
                <img style={{ height: '40px' }} src={item.image} alt="" />
              </div>
              <div style={{ flex: 0.4 }}>
                <h6
                  style={{
                    fontFamily: 'Open Sans',
                    fontWeight: '600',
                    fontSize: '14px',
                    color: '#1c1c1c',
                  }}
                >
                  {item.name}
                </h6>
                <h6
                  style={{
                    fontFamily: 'Open Sans',
                    fontWeight: '600',
                    fontSize: '12px',
                    color: '#4f4f4f',
                  }}
                >
                  {item.duration}
                </h6>
              </div>
              <div
                style={{ flex: 0.3 }}
                className="d-flex justify-content-end align-items-center"
              >
                <h6
                  style={{
                    fontFamily: 'Open Sans',
                    fontWeight: '700',
                    fontSize: '14px',
                    color: '#192A55',
                  }}
                >
                  {item.price}
                </h6>
              </div>
            </div>
          </Col>
        ))}
      </Row>
      <Row className="no-gutters" style={{ borderTop: '1px solid #E8E8E8' }}>
        <Col
          className="no-gutters p-3 d-flex align-items-center justify-content-between"
          style={{
            borderRight: '1px solid #E8E8E8',
            height: '56px',
            fontFamily: 'Open Sans',
            fontWeight: '700',
            fontSize: '14px',
            color: '#1c1c1c',
          }}
        >
          <Form.Group className="mb-0 w-50" controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox"
              label="Tambahkan asuransi"
              style={{ fontWeight: '400' }}
            />
          </Form.Group>
          <h6>Rp 0</h6>
        </Col>
        <Col
          className="no-gutters p-3 d-flex align-items-center justify-content-between"
          style={{
            height: '56px',
            fontFamily: 'Open Sans',
            fontWeight: '700',
            fontSize: '14px',
            color: '#1c1c1c',
          }}
        >
          <h6>Total</h6>
          <h6>Rp 0</h6>
        </Col>
      </Row>
    </>
  );
}

export default PilihKurir;
