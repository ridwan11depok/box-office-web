import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import GetAppIcon from '@material-ui/icons/GetApp';
import MakeTransactionModal from './MakeTransactionModal';
import { Form, InputGroup } from 'react-bootstrap';
import { dummy } from './dataDummy';
import BatchInvoiceModal from './BatchInvoiceModal';
import Select from 'react-select';
import { setDataPageDropshipDetail } from '../reduxAction';

const options1 = [
  { value: 'a', label: 'Januari 2021' },
  { value: 'b', label: 'Februari 2021' },
  { value: 'c', label: 'Maret 2021' },
  { value: 'd', label: 'April 2021' },
  { value: 'e', label: 'Mei 2021' },
  { value: 'f', label: 'Juni 2021' },
  { value: 'g', label: 'Juli 2021' },
  { value: 'h', label: 'Agustus 2021' },
  { value: 'i', label: 'September 2021' },
  { value: 'j', label: 'Oktober 2021' },
  { value: 'k', label: 'November 2021' },
  { value: 'l', label: 'Desember 2021' },
];
const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => {
        dispatch(setDataPageDropshipDetail(data));
        history.push('/transactions/dropship-detail');
      }}
    />
  );
};

const EmptyTransaction = ({ onClick }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada transaksi yang dilakukan saat ini.
      </h6>
      <Button onClick={onClick} text="Buat Transaksi" styleType="blueFill" />
    </div>
  );
};

const HeaderTable = (props) => {
  const classes = useStyles();
  const { handleAddTransaction, handleBatchInvoice } = props;
  const styles = {
    control: (css) => ({
      ...css,
      // paddingLeft: '1rem',
      width: '200px',
      backgroundColor: 'transparent',
      border: '1px solid #1C1C1C',
    }),
  };
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        {/* <InputGroup controlId="dob">
          <Form.Control placeholder="All" type="date" name="dob" />
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">
              <i class="fas fa-chevron-down"></i>
            </InputGroup.Text>
          </InputGroup.Prepend>
        </InputGroup> */}
        <Select placeholder="Juni 2021" options={options1} styles={styles} />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          startIcon={() => <GetAppIcon />}
          styleType="blueOutline"
          text="Export Logs"
        />
        <Button
          className="ml-1 mr-1"
          styleType="blueOutline"
          text="Batch Invoice"
          onClick={handleBatchInvoice}
        />
        <Button
          styleType="blueFill"
          text="Buat Transaksi"
          onClick={handleAddTransaction}
        />
      </div>
    </div>
  );
};

const StatusField = ({ data }) => {
  return (
    <h6
      style={{
        color: data.status === 'done' ? '#1B5E20' : '#1c1c1c',
        fontWeight: '700',
        fontSize: '14px',
      }}
    >
      {data.status === 'done' ? 'Done' : 'Pending'}
    </h6>
  );
};

function TransactionsTable(props) {
  const dispatch = useDispatch();
  const { skuProduct } = useSelector((state) => state.gudang);
  const [state, setState] = useState({
    modalTransaction: false,
    modalBatchInvoice: false,
  });
  const handleAddTransaction = () => {
    setState({ ...state, modalTransaction: true });
  };
  const handleBatchInvoice = () => {
    setState({ ...state, modalBatchInvoice: true });
  };
  const { tab } = props;
  const setData = () => {
    if (tab === 1) {
      return [...dummy.pending, ...dummy.done];
    } else if (tab === 2) {
      return dummy.pending;
    } else if (tab === 3) {
      return dummy.done;
    }
  };
  return (
    <div>
      <DataTable
        column={[
          {
            heading: 'Tanggal ',
            key: 'date',
          },
          {
            heading: 'Nomor Invoice',
            key: 'noinvoice',
          },
          {
            heading: 'Nama Penerima',
            key: 'receiver',
          },
          {
            heading: 'Kurir',
            key: 'kurir',
          },
          {
            heading: 'Status',
            render: (data) => <StatusField data={data} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={setData()}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddTransaction={handleAddTransaction}
            handleBatchInvoice={handleBatchInvoice}
          />
        )}
        renderEmptyData={() => (
          <EmptyTransaction onClick={handleAddTransaction} />
        )}
      />
      <MakeTransactionModal
        onHide={() => setState({ ...state, modalTransaction: false })}
        show={state.modalTransaction}
      />
      <BatchInvoiceModal
        onHide={() => setState({ ...state, modalBatchInvoice: false })}
        show={state.modalBatchInvoice}
      />
    </div>
  );
}

export default TransactionsTable;
