import React, { useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import Banner from '../../../../components/elements/Banner';
import GetAppIcon from '@material-ui/icons/GetApp';

const BatchInvoiceModal = (props) => {
  const invoiceList = React.createRef();
  const [file, setFile] = useState('');
  const handleChangeFile = (e) => {
    console.log(e.target.value);
    setFile(e.target.value);
  };
  return (
    <>
      <Modal
        titleModal="Upload Batch INV"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <>
            <Banner
              description="Untuk mengupload file excel silahkan download template terlebih dahulu."
              button={{
                isShow: true,
                styleType: 'blueFill',
                text: 'Download Template',
                startIcon: () => <GetAppIcon />,
              }}
            />
            <h6
              className="mt-3 mb-3"
              style={{
                fontSize: '14px',
                fontWeight: '400',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
              }}
            >
              Silahkan upload daftar INV dalam bentuk file excel, dengan format
              yang telah ditentukan.
            </h6>
            <div
              style={{
                height: '140px',
                borderRadius: '4px',
                border: '2px dashed #CFCFCF',
                backgroundColor: '#F2F2F2',
              }}
              className="d-flex flex-column justify-content-center align-items-center "
            >
              <h6
                // className="mt-3 mb-3"
                style={{
                  fontSize: '14px',
                  fontWeight: '400',
                  fontFamily: 'Open Sans',
                  color: '#1c1c1c',
                }}
              >
                {file
                  ? file.split('\\')[file.split('\\').length - 1]
                  : 'Upload daftar INV dalam file excel'}
              </h6>
              <Button
                className="mt-3"
                styleType="blueOutline"
                text={file ? 'Ganti Invoice' : 'Upload Invoice'}
                onClick={() => invoiceList.current.click()}
              />
            </div>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ width: '140px' }}
              className="mr-3"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
            <Button
              style={{ width: '140px' }}
              styleType="blueFill"
              text="Upload"
            />
          </div>
        )}
      />
      <input
        ref={invoiceList}
        type="file"
        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
        className="d-none"
        onChange={handleChangeFile}
      />
    </>
  );
};

export default BatchInvoiceModal;
