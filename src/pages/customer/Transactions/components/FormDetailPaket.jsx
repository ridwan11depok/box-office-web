import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';

function FormDetailPaket(props) {
  return (
    <Container withHeader={false}>
      <h6>Detail Paket</h6>
      <Item border={false}>
        <Row className="mb-3">
          <Col md={6}>
            <SelectField
              label="Kategori Produk"
              options={[
                {
                  value: '0',
                  description: 'Pilih kategori produk',
                },
                {
                  value: '1',
                  description: 'Opsi 1',
                },
                {
                  value: '2',
                  description: 'Opsi 2',
                },
              ]}
            />
            <TextField
              label="Panjang"
              className="w-100"
              placeholder="Input panjang"
              suffix={() => <span>cm</span>}
            />
            <TextField
              label="Tinggi"
              className="w-100"
              placeholder="Input tinggi"
              suffix={() => <span>cm</span>}
            />
            <TextField
              label="Nilai Barang"
              className="w-100"
              placeholder="Input nilai barang"
              prefix={() => <span>Rp</span>}
            />
          </Col>
          <Col md={6}>
            <SelectField
              label="Sub Kategori Produk"
              options={[
                {
                  value: '0',
                  description: 'Pilih sub kategori produk',
                },
                {
                  value: '1',
                  description: 'Opsi 1',
                },
                {
                  value: '2',
                  description: 'Opsi 2',
                },
              ]}
            />
            <TextField
              label="Lebar"
              className="w-100"
              placeholder="Input tinggi"
              suffix={() => <span>cm</span>}
            />
            <TextField
              label="Berat"
              className="w-100"
              placeholder="Input berat"
              suffix={() => <span>kg</span>}
            />
          </Col>
        </Row>
      </Item>
    </Container>
  );
}

export default FormDetailPaket;
