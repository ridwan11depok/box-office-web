import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { dummy } from './dataDummy';
import AddIcon from '@material-ui/icons/Add';
import Counter from '../../../../components/elements/Counter';
import DeleteProductModal from './DeleteProductModal';
import SelectProductModal from './SelectProductModal';

const useStyles = makeStyles({
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
});

const ActionField = ({ data, onClick }) => {
  return (
    <div
      onClick={() => onClick(data)}
      className="p-1 rounded button"
      style={{ border: '2px solid red', width: 'min-content' }}
    >
      <i
        className="far fa-trash-alt"
        style={{ fontSize: '20px', color: 'red', cursor: 'pointer' }}
      ></i>
    </div>
  );
};

const QtyField = ({ data = {}, isEdit }) => {
  const dispatch = useDispatch();

  return isEdit ? (
    <Counter onDecrease={() => {}} onIncrease={() => {}} value={data.qty} />
  ) : (
    <span>{data.qty}</span>
  );
};

const EmptyData = ({ onClick }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada yang dikirim saat ini.</h6>
      <Button
        startIcon={() => <AddIcon />}
        text="Pilih Produk"
        styleType="blueOutline"
      />
    </div>
  );
};

const HeaderTable = (props) => {
  const classes = useStyles();
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <h6>Produk Dikirim</h6>
      </div>
      {props.isEdit && (
        <Button
          startIcon={() => <AddIcon />}
          text="Pilih Produk"
          styleType="blueOutline"
          onClick={props.onClick}
        />
      )}
    </div>
  );
};

function ProdukDikirimTable(props) {
  const dispatch = useDispatch();
  const { isEdit, data } = props;
  const [state, setState] = useState({
    deleteModal: false,
    dataDelete: {},
    selectProductModal: false,
  });
  return (
    <div>
      <DataTable
        column={[
          {
            heading: 'Produk ',
            key: 'product',
          },
          {
            heading: 'Qty',
            render: (item) => <QtyField isEdit={isEdit} data={item} />,
          },
          isEdit && {
            heading: 'Aksi',
            render: (item) => (
              <ActionField
                onClick={(item) =>
                  setState({ ...state, deleteModal: true, dataDelete: item })
                }
                data={item}
              />
            ),
          },
        ]}
        data={[
          {
            id: 1,
            product: 'Almari',
            qty: 10,
          },
        ]}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            isEdit={isEdit}
            onClick={() => setState({ ...state, selectProductModal: true })}
          />
        )}
        renderEmptyData={() => <EmptyData />}
      />
      <DeleteProductModal
        onHide={() => setState({ ...state, deleteModal: false })}
        show={state.deleteModal}
        data={state.dataDelete}
      />
      <SelectProductModal
        onHide={() => setState({ ...state, selectProductModal: false })}
        show={state.selectProductModal}
      />
    </div>
  );
}
ProdukDikirimTable.defaultProps = {
  isEdit: true,
  data: {},
};
export default ProdukDikirimTable;
