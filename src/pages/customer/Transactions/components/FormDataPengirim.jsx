import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormDataPengirim(props) {
  const classes = useStyles();
  const { isEdit, data } = props;
  return (
    <div>
      <h6>Data Pengirim</h6>
      {isEdit ? (
        <>
          {/* <Item border={false}> */}
          <TextField label="Nama Pengirim" placeholder="Input nama pengirim" />
          <Row>
            <Col>
              <SelectField
                label="Nomor Telepon Pelanggan"
                options={[
                  {
                    value: '1',
                    description: 'Indonesia(+62)',
                  },
                  {
                    value: '2',
                    description: 'Opsi 2',
                  },
                  {
                    value: '3',
                    description: 'Opsi 3',
                  },
                ]}
              />
            </Col>
            <Col className="d-flex align-items-end">
              <TextField className="w-100" placeholder="Input nomor telepon" />
            </Col>
          </Row>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Tampilkan logo bisnis" />
          </Form.Group>
          {/* </Item> */}
        </>
      ) : (
        <>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.text}>Nama Pengirim</h6>
            <h6 className={classes.text}>{data?.sender || 'No data'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.text}>Nomor Telepon Pelanggan</h6>
            <h6 className={classes.text}>{data?.customerPhone || 'No data'}</h6>
          </Item>
        </>
      )}
    </div>
  );
}

FormDataPengirim.defaultProps = {
  isEdit: true,
  data: {},
};

export default FormDataPengirim;
