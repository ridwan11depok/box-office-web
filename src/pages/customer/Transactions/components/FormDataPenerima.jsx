import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormDataPenerima(props) {
  const classes = useStyles();
  const { isEdit, data } = props;
  return (
    <div>
      <h6>Data Penerima</h6>
      {isEdit ? (
        <>
          {/* <Item border={false}> */}
          <TextField label="Nama Penerima" placeholder="Input nama penerima" />
          <Row>
            <Col>
              <SelectField
                label="Nomor Telepon Pelanggan"
                options={[
                  {
                    value: '1',
                    description: 'Indonesia(+62)',
                  },
                  {
                    value: '2',
                    description: 'Opsi 2',
                  },
                  {
                    value: '3',
                    description: 'Opsi 3',
                  },
                ]}
              />
            </Col>
            <Col className="d-flex align-items-end">
              <TextField className="w-100" placeholder="Input nomor telepon" />
            </Col>
          </Row>
          <TextArea
            className="mb-3"
            label="Alamat"
            placeholder="Input alamat penerima"
            rows={3}
          />
          {/* </Item> */}
        </>
      ) : (
        <>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.text}>Nama Penerima</h6>
            <h6 className={classes.text}>{data?.receiver || 'No data'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.text}>Nomor Telepon Penerima</h6>
            <h6 className={classes.text}>{data?.phoneReceiver || 'No Data'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.text}>Alamat</h6>
            <h6 className={classes.text}>{data?.address || 'No data'}</h6>
          </Item>
        </>
      )}
    </div>
  );
}

FormDataPenerima.defaultProps = {
  isEdit: true,
  data: {},
};

export default FormDataPenerima;
