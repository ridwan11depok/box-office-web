import React, { useState } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import { setDataPageDropshipDetail } from '../reduxAction';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormInvoice(props) {
  const classes = useStyles();
  const [state, setState] = useState({ imageINV: '' });
  const invoiceImageRef = React.createRef();
  const handleChangeImage = (e) => {
    setState({ ...state, imageINV: e.target.files[0] });
  };
  const { isEdit, data } = props;
  return (
    <>
      <Container withHeader={false}>
        <h6>Invoice</h6>
        {isEdit ? (
          <>
            <Item border={false} spaceBottom="3">
              <div
                style={{
                  height: '130px',
                  borderRadius: '4px',
                  border: '2px dashed #CFCFCF',
                  backgroundColor: '#F2F2F2',
                  overflow: 'hidden',
                  position: 'relative',
                }}
                className="d-flex flex-column justify-content-center align-items-center "
              >
                {state.imageINV ? (
                  <>
                    <Button
                      style={{
                        position: 'absolute',
                        right: '10px',
                        top: '10px',
                      }}
                      styleType="lightBlueFillOutline"
                      text="Ganti Invoice"
                      onClick={() => invoiceImageRef.current.click()}
                    />
                    <img
                      src={URL.createObjectURL(state.imageINV)}
                      alt=""
                      style={{ height: '130px' }}
                    />
                  </>
                ) : (
                  <>
                    <img className="mt-2" src={pictureIcon} alt="" />
                    <h6
                      className="mt-2"
                      style={{
                        fontSize: '14px',
                        fontWeight: '400',
                        fontFamily: 'Open Sans',
                        color: '#1c1c1c',
                      }}
                    >
                      Upload bukti gambar invoice di sini
                    </h6>
                    <Button
                      className="mt-2 mb-3"
                      styleType="blueOutline"
                      text="Upload Invoice"
                      onClick={() => invoiceImageRef.current.click()}
                    />
                  </>
                )}
              </div>
            </Item>
            <Item border={false}>
              <TextField
                label="No Invoice (Wajib)"
                placeholder="Input nomor invoice"
              />
              <TextField
                label="Pesan (Opsional)"
                placeholder="Input pesan di sini"
              />
              <SelectField
                label="Logistik (Wajib)"
                options={[
                  {
                    value: '1',
                    description: 'All',
                  },
                  {
                    value: '2',
                    description: 'Opsi 2',
                  },
                  {
                    value: '3',
                    description: 'Opsi 3',
                  },
                ]}
              />
              <TextField label="AWB (Wajib)" placeholder="Input kode AWB" />
            </Item>
          </>
        ) : (
          <>
            <Item border={false} spaceBottom="3">
              <div
                style={{
                  height: '130px',
                  borderRadius: '4px',
                  border: '2px dashed #CFCFCF',
                  backgroundColor: '#F2F2F2',
                  overflow: 'hidden',
                  position: 'relative',
                }}
                className="d-flex flex-column justify-content-center align-items-center "
              >
                <img src={''} alt="" style={{ height: '130px' }} />
              </div>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>No. Invoice</h6>
              <h6 className={classes.text}>{data.noinvoice || 'no data'}</h6>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>Pesan</h6>
              <h6 className={classes.text}>{data.message || 'no data'}</h6>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>Logistik</h6>
              <h6 className={classes.text}>{data.logistik || 'no data'}</h6>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>AWB</h6>
              <h6 className={classes.text}>{data.awb || 'no data'}</h6>
            </Item>
          </>
        )}
      </Container>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={invoiceImageRef}
        type="file"
        className="d-none"
      />
    </>
  );
}
FormInvoice.defaultProps = {
  isEdit: true,
  data: {},
};

export default FormInvoice;
