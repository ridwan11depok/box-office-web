import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';

function FormPengirimOrPenerima({ type = '' }) {
  return (
    <Container withHeader={false}>
      <h6>Data {type}</h6>
      <Item border={false}>
        <TextField
          className="mb-3"
          label={`Nama ${type}`}
          placeholder="Input nomor invoice"
        />
        <Row className="mb-3">
          <Col>
            <SelectField
              label="Nomor Telepon Pelanggan"
              options={[
                {
                  value: '1',
                  description: 'Indonesia(+62)',
                },
                {
                  value: '2',
                  description: 'Opsi 2',
                },
                {
                  value: '3',
                  description: 'Opsi 3',
                },
              ]}
            />
          </Col>
          <Col className="d-flex align-items-end">
            <TextField className="w-100" placeholder="Input nomor telepon" />
          </Col>
        </Row>
        <div className="map-regular-transaction mb-3">
          <div
            className="d-flex justify-content-center align-items-center w-100 p-3"
            style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
          >
            <span
              style={{
                fontSize: '14px',
                fontWeight: '600',
                fontFamily: 'Open Sans',
              }}
            >
              Tandai lokasi peta Anda agar alamat lebih akurat.
            </span>
            <Button
              style={{ width: '200px' }}
              styleType="whiteOutline"
              text="Tandai Lokasi"
            />
          </div>
        </div>
        <TextArea
          className="mb-3"
          label="Alamat"
          placeholder={`Input alamat ${type.toLowerCase()}`}
        />
        <TextArea
          className="mb-3"
          label="Catatan Tambahan (Opsional)"
          placeholder="Input catatan tambahan"
          rows={3}
        />
      </Item>
    </Container>
  );
}

export default FormPengirimOrPenerima;
