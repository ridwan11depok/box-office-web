import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { TabHeader } from '../../../components/elements/Tabs/';
import { makeStyles } from '@material-ui/core/styles';
import TransactionTable from './components/TransactionsTable';
import { WarehouseSelect } from '../../../components/elements/InputField';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const Transactions = () => {
  const [state, setState] = useState({ tab: 1 });
  const classes = useStyles();
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
            <PageTitle title="Transaksi" />
            <div className="d-flex flex-row align-items-center">
              <h6 className={classes.selectWarehouse}>Pilih Gudang :</h6>
              <WarehouseSelect />
            </div>
          </div>
        )}
      >
        <ContentItem col="col-12" spaceBottom={3}>
          <TabHeader
            listHeader={[
              {
                title: 'Semua Transaksi',
                tab: 1,
              },
              {
                title: 'Transaksi Pending',
                tab: 2,
              },
              {
                title: 'Transaksi Selesai',
                tab: 3,
              },
            ]}
            activeTab={state.tab}
            border={false}
            onChange={(tab) => setState({ tab })}
          />
        </ContentItem>
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <TransactionTable tab={state.tab} />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default Transactions;
