import React, { useState, useEffect } from 'react';
import './styles.css';
import ModalAddUser from './components/ModalAddUser';
import ModalUpdateProfile from './components/ModalUpdateProfile';
import ModalChangePassword from './components/ModalChangePassword';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import { Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import AccountTable from './components/AccountTable';
import { useSelector, useDispatch } from 'react-redux';
import { getListRole, getDetailAccount } from './reduxAction';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#4F4F4F',
    marginBottom: '5px',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
    marginBottom: '15px',
  },
  changePassword: {
    fontSize: '14px',
    fontWeight: '700',
    color: '#192A55',
    fontFamily: 'Open Sans',
    cursor: 'pointer',
  },
});

const Account = () => {
  const classes = useStyles();
  const [state, setState] = useState({
    showChangePassword: false,
    dataChangePassword: {},
    showUpdateProfile: false,
    dataDetailUser: null,
  });
  const dispatch = useDispatch();
  const { infoAccount } = useSelector((state) => state.systemSettingCustomer);
  const { dataUser } = useSelector((state) => state.login);
  const handleHideModalChangePassword = () => {
    setState({ ...state, showChangePassword: false });
  };
  const handleAgreeModalChangePassword = () => {
    setState({ ...state, showChangePassword: false });
  };

  useEffect(() => {
    dispatch(getListRole());
    dispatch(getDetailAccount());
  }, []);
  return (
    <>
      <ContentContainer title="Akun">
        <ContentItem col="col-12" spaceBottom={3}>
          <div
            className="d-flex p-3 justify-content-between align-items-center"
            style={{ borderBottom: '1px solid #E8E8E8' }}
          >
            <h6 className="mb-0">Data User</h6>
            <Button
              text="Edit Profile"
              styleType="blueOutline"
              onClick={() =>
                setState({
                  ...state,
                  showUpdateProfile: true,
                })
              }
            />
          </div>
          <Row className="p-3">
            <Col md={6} xs={12}>
              <div>
                <h6 className={classes.label}>Nama</h6>
                <h6 className={classes.value}>
                  {infoAccount?.name || 'No Data'}
                </h6>
              </div>
              <div>
                <h6 className={classes.label}>Email</h6>
                <h6 className={classes.value}>
                  {infoAccount?.email || 'No Data'}
                </h6>
              </div>
              <div>
                <h6 className={classes.label}>Akses</h6>
                <h6 className={classes.value}>
                  {infoAccount?.display_name_role || 'No Data'}
                </h6>
              </div>
            </Col>
            <Col md={6} xs={12}>
              <div>
                <h6 className={classes.label}>Nomor Telepon</h6>
                <h6 className={classes.value}>
                  {infoAccount?.phone_number || 'No Data'}
                </h6>
              </div>
              <div>
                <h6 className={classes.label}>Alamat</h6>
                <h6 className={classes.value}>
                  {infoAccount?.address || 'No Data'}
                </h6>
              </div>
              <div>
                <h6 className={classes.label}>Password</h6>
                <h6
                  className={classes.changePassword}
                  onClick={() =>
                    setState({ ...state, showChangePassword: true })
                  }
                >
                  Ubah Password
                </h6>
              </div>
            </Col>
          </Row>
        </ContentItem>
        <ContentItem border={false} spaceTop={3}>
          <AccountTable />
        </ContentItem>
      </ContentContainer>
      <ModalUpdateProfile
        show={state.showUpdateProfile}
        onHide={() => setState({ ...state, showUpdateProfile: false })}
        initialValues={infoAccount}
      />
      <ModalChangePassword
        show={state.showChangePassword}
        onHide={handleHideModalChangePassword}
        onAgree={handleAgreeModalChangePassword}
      />
    </>
  );
};

export default Account;
