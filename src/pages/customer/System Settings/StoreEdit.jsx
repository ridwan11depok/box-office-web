import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInfoStore from './components/FormInfoStore';
import FormLokasiStore from './components/FormLokasiStore';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { updateStore, getDetailStore } from './reduxAction';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});

const validationSchema = Yup.object({
  // store_logo: Yup.mixed().required('Required'),
  store_name: Yup.string().required('Harap untuk mengisi nama store'),
  store_address: Yup.string().required('Harap untuk mengisi alamat store'),
  province_id: Yup.number().required('Harap untuk memilih provinsi'),
  city_id: Yup.number().required('Harap untuk memilih kota'),
  district_id: Yup.string().required('Harap untuk memilih kecamatan'),
  longitude: Yup.number().required('Harap untuk menandai lokasi'),
  latitude: Yup.number().required('Harap untuk menandai lokasi'),
});

const AddStore = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailStore } = useSelector((state) => state.systemSettingCustomer);

  const formik = useFormik({
    initialValues: {
      store_logo: '',
      store_name: '',
      store_address: '',
      province_id: '',
      city_id: '',
      district_id: '',
      longitude: '',
      latitude: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('store_logo', values.store_logo);
      data.append('store_name', values.store_name);
      data.append('store_address', values.store_address);
      data.append('province_id', values.province_id);
      data.append('city_id', values.city_id);
      data.append('district_id', values.district_id);
      data.append('longitude', values.longitude);
      data.append('latitude', values.latitude);

      // other
      data.append('store_id', detailStore?.id);
      const res = await dispatch(updateStore(data));
      history.push('/system-settings/store');
    } catch (er) {}
  };

  useEffect(() => {
    dispatch(getDetailStore());
  }, []);
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Edit Store"
              onClick={() => history.push('/system-settings/store')}
            />
          </div>
        )}
      >
        <ContentItem
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Info Store</h6>
          </div>
          <div className="p-3">
            <FormInfoStore formik={formik} initialValues={detailStore} />
          </div>
        </ContentItem>
        <ContentItem
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Lokasi Store</h6>
          </div>
          <div className="p-3">
            <FormLokasiStore formik={formik} initialValues={detailStore} />
          </div>
        </ContentItem>

        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
            onClick={() => {
              formik.resetForm();
            }}
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text={isLoading ? 'Loading...' : 'Simpan'}
            onClick={(e) => formik.handleSubmit(e)}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default AddStore;
