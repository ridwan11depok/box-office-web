import React, { useEffect, useState } from 'react';
import MasterSKUTable from './components/MasterSKUTable';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import { Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Badge from '../../../components/elements/Badge';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getDetailStore } from './reduxAction';
import MapModal from '../../../components/elements/Map/MapModal';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#4F4F4F',
    marginBottom: '5px',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
    marginBottom: '15px',
  },
  changePassword: {
    fontSize: '14px',
    fontWeight: '700',
    color: '#192A55',
    fontFamily: 'Open Sans',
    cursor: 'pointer',
  },
  avatar: {
    width: '64px',
    height: '64px',
    marginRight: '8px',
  },
  storeName: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#000000',
  },
  storeType: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  btnBlueNoFill: {
    marginRight: 0,
    paddingLeft: 0,
  },
});

const Store = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    showMap: false,
  });
  const { detailStore } = useSelector((state) => state.systemSettingCustomer);

  useEffect(() => {
    dispatch(getDetailStore());
  }, []);
  return (
    <>
      <ContentContainer title="Store">
        <ContentItem col="col-12" spaceBottom={3}>
          <div
            className="d-flex p-3 justify-content-between align-items-center"
            style={{ borderBottom: '1px solid #E8E8E8' }}
          >
            <h6 className="mb-0">Data Store</h6>
            <Button
              text="Edit Store"
              styleType="blueOutline"
              onClick={() => history.push('/system-settings/store/edit')}
            />
          </div>
          <Row className="p-3">
            <Col md={6} xs={12}>
              <div className="d-flex flex-row">
                <Avatar
                  className={classes.avatar}
                  alt="Logo"
                  variant="rounded"
                  src={detailStore?.store_logo_full_url || ''}
                />
                <div>
                  <h6 className={classes.storeName}>
                    {detailStore?.store_name || 'No Data'}
                  </h6>
                  <Badge
                    label={
                      detailStore?.status === 1
                        ? 'Verified'
                        : detailStore?.status === 2
                        ? 'Verified'
                        : 'Waiting'
                    }
                    styleType={
                      detailStore?.status === 1
                        ? 'green'
                        : detailStore?.status === 2
                        ? 'red'
                        : 'yellow'
                    }
                  />
                  <h6 className={classes.storeType}>
                    {detailStore?.type || 'No Data'}
                  </h6>
                </div>
              </div>
            </Col>
            <Col md={6} xs={12}>
              <div>
                <h6 className={classes.label}>Alamat</h6>
                <h6 className={classes.value}>
                  {detailStore?.store_address || 'No Data'}
                </h6>
                <Button
                  styleType="blueNoFill"
                  className={classes.btnBlueNoFill}
                  text="Lihat Lokasi di Peta"
                  startIcon={() => <MapOutlinedIcon />}
                  onClick={() => setState({ ...state, showMap: true })}
                />
              </div>
            </Col>
          </Row>
        </ContentItem>

        <ContentItem border={false} spaceTop={3}>
          <MasterSKUTable />
        </ContentItem>
      </ContentContainer>
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        initialCoordinate={{
          lng: detailStore?.longitude,
          lat: detailStore?.latitude,
        }}
        mode="viewer"
      />
    </>
  );
};

export default Store;
