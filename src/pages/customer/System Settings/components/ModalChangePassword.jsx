import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { PasswordField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { changePassword } from '../reduxAction';

const validationSchema = Yup.object({
  old_password: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .required('Harap untuk mengisi password lama'),
  new_password: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .required('Harap untuk mengisi password baru'),
  new_password_confirmation: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .oneOf([Yup.ref('new_password'), null], 'Password tidak sama')
    .required('Harap untuk mengisi ulang password baru'),
});

const ModalChangePassword = (props) => {
  const formik = useFormik({
    initialValues: {
      old_password: '',
      new_password: '',
      new_password_confirmation: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const handleSubmit = async (values) => {
    try {
      await dispatch(changePassword(values));
      props.onHide();
    } catch (err) {}
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Ganti Password"
        titleClassName="title-modal"
        bodyClassName="body-modal"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <PasswordField
              label="Password Lama"
              placeholder="Input password lama kamu"
              name="old_password"
              withValidate
              formik={formik}
              className="mt-3"
            />
            <PasswordField
              label="Password Baru"
              placeholder="Input password baru kamu"
              name="new_password"
              withValidate
              formik={formik}
            />
            <PasswordField
              label="Konfirmasi Password Baru"
              placeholder="Input ulang password baru kamu"
              name="new_password_confirmation"
              withValidate
              formik={formik}
            />
            <div className="mb-0 mt-3">
              <h6 className="input-label mb-1">Buatlah password yang:</h6>
              <ul style={{ listStyle: 'none', paddingLeft: 0 }}>
                <li>
                  <i
                    className="fa fa-check-circle mr-1"
                    style={{ color: '#1C1C1C' }}
                    aria-hidden="true"
                  ></i>
                  Minimal 8 karakter
                </li>
                <li>
                  <i
                    className="fa fa-check-circle mr-1"
                    style={{ color: '#1C1C1C' }}
                    aria-hidden="true"
                  ></i>
                  Tidak berisi nama kamu atau alamat email
                </li>
                <li>
                  <i
                    className="fa fa-check-circle mr-1"
                    style={{ color: '#1C1C1C' }}
                    aria-hidden="true"
                  ></i>
                  Memilki huruf kecil (a-z) dan huruf besar (A-Z)
                </li>
              </ul>
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              className="mr-2"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
            <Button
              styleType="blueFill"
              text={isLoading ? 'Loading...' : 'Ganti'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalChangePassword;
