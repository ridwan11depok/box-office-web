import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import DataTable from '../../../../components/elements/Table/Table';
import Button from '../../../../components/elements/Button';
import GetAppIcon from '@material-ui/icons/GetApp';
import AddIcon from '@material-ui/icons/Add';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { SearchField } from '../../../../components/elements/InputField';
import { AddSKUModal } from '../../../../components/elements/Modal';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ModalDeleteSKU from './ModalDeleteSKU';
import { useSelector, useDispatch } from 'react-redux';
import {
  addProductSKU,
  getListProductSKU,
  updateProductSKU,
  deleteBatchProductSKU,
} from '../reduxAction';
import { formatRupiah } from '../../../../utils/text';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  itemOption: {
    fontSize: '14px',
    fontWeight: '700',
    color: '#1c1c1c',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    color: '#1C1C1C',
    fontFamily: 'Work Sans',
  },
}));

const MasterSKUTable = (props) => {
  const classes = useStyles();
  const { dataUser } = useSelector((state) => state.login);
  const dispatch = useDispatch();
  const { listSKU } = useSelector((state) => state.systemSettingCustomer);
  const [state, setState] = useState({
    showAddSKU: false,
    dataDetailSKU: {},
    showDeleteSKU: false,
    search: '',
    selectedSKU: [],
  });

  const handleSelectSKU = (id, checked) => {
    let newSelected = [];
    if (checked) {
      newSelected = [...state.selectedSKU, id];
      setState({ ...state, selectedSKU: newSelected });
    } else {
      newSelected = state.selectedSKU.filter((item) => item !== id);
      setState({ ...state, selectedSKU: newSelected });
    }
  };
  const handleShowAddSKUModal = () => {
    setState({ ...state, showAddSKU: true });
  };
  const handleShowDeleteSKUModal = () => {
    setState({ ...state, showDeleteSKU: true });
  };
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClickOptions = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  const renderHeaderTable = () => {
    return (
      <Grid container>
        <Grid xs={12}>
          <h6 className={`p-3 mb-0 ${classes.titleTable}`}>Master SKU</h6>
        </Grid>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justifyContent="space-between"
          style={{
            borderTop: '1px solid #E8E8E8',
          }}
          className="p-3"
        >
          <SearchField placeholder="Cari produk, SKU" onEnter={handleSearch} />

          <div className="d-flex flex-wrap align-items-center">
            <Button
              onClick={handleShowAddSKUModal}
              styleType="blueFill"
              text="Tambah SKU"
              startIcon={() => <AddIcon />}
            />
            <Button
              className="m-1"
              styleType="redOutline"
              text={state.selectedSKU.length ? 'Delete' : 'Delete All'}
              onClick={handleShowDeleteSKUModal}
            />
            <div>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                placement="bottom"
              >
                <MenuItem className={classes.itemOption}>
                  Upload Batch SKU
                </MenuItem>
                <MenuItem className={classes.itemOption}>
                  Print Barcode
                </MenuItem>
                <MenuItem className={classes.itemOption}>Export SKU</MenuItem>
              </Menu>
              <Button
                styleType="blueOutline"
                isOptionButton
                onClick={handleClickOptions}
              />
            </div>
          </div>
        </Grid>
      </Grid>
    );
  };
  const renderEdit = (data) => {
    return (
      <Button
        text="Edit"
        startIcon={() => (
          <i
            className="fas fa-pen"
            style={{ fontSize: '12px', color: '#192A55' }}
          ></i>
        )}
        styleType="blueOutline"
        onClick={() =>
          setState({ ...state, dataDetailSKU: data, showAddSKU: true })
        }
      />
    );
  };
  const renderSKUField = (data) => {
    return (
      <div>
        <FormControlLabel
          control={
            <Checkbox
              checked={state.checkedA}
              onChange={(e) => handleSelectSKU(data.id, e.target.checked)}
              name="checkedA"
            />
          }
          label={data.sku}
        />
      </div>
    );
  };

  const handleAddSKU = async (payload) => {
    try {
      const storeId = dataUser?.store[0]?.id;
      const data = new FormData();
      data.append('store_id', storeId);
      Object.keys(payload).forEach((key) => {
        if (key === 'images') {
          payload.images.forEach((item, idx) => {
            if (item !== '') {
              data.append(`images[${idx}][image]`, item);
            }
          });
        } else if (key === 'qc_step' || key === 'is_fefo') {
          data.append(`${key}`, payload[key] ? 1 : 0);
        } else {
          data.append(`${key}`, payload[key]);
        }
      });
     
      if (state?.dataDetailSKU?.id) {
        await dispatch(updateProductSKU(data, state.dataDetailSKU.id));
      } else {
        await dispatch(addProductSKU(data));
      }

      setState({ ...state, showAddSKU: false, dataDetailSKU: {} });
    } catch (err) {}
  };

  const fetchListSKU = (payload) => {
    dispatch(getListProductSKU(payload));
  };

  const handleDeleteSKU = async () => {
    try {
      const payload = { product_id: state.selectedSKU };
      if (state.selectedSKU.length) {
        await dispatch(deleteBatchProductSKU(payload));
      } else {
        await dispatch(deleteBatchProductSKU());
      }
      setState({ ...state, selectedSKU: [], showDeleteSKU: false });
    } catch (err) {}
  };

  return (
    <>
      <DataTable
        action={fetchListSKU}
        totalPage={listSKU?.last_page || 5}
        params={{
          search: state.search,
        }}
        column={[
          {
            heading: 'SKU',
            render: renderSKUField,
          },
          {
            heading: 'Produk',
            key: 'name',
          },
          {
            heading: 'Harga',
            key: 'sell_price_in_rupiah',
          },
          { heading: 'Aksi', render: renderEdit },
        ]}
        data={listSKU?.data || []}
        transformData={(item) => ({
          ...item,
          sell_price_in_rupiah: formatRupiah(item.sell_price),
        })}
        renderHeader={renderHeaderTable}
        withNumber={false}
      />
      <AddSKUModal
        show={state.showAddSKU}
        onHide={() =>
          setState({ ...state, showAddSKU: false, dataDetailSKU: {} })
        }
        initialValues={Object.keys(state.dataDetailSKU).length === 0 ? null : state.dataDetailSKU }
        onAgree={handleAddSKU}
      />
      <ModalDeleteSKU
        show={state.showDeleteSKU}
        onHide={() => setState({ ...state, showDeleteSKU: false })}
        onAgree={handleDeleteSKU}
        data={state.selectedSKU}
      />
    </>
  );
};

export default MasterSKUTable;
