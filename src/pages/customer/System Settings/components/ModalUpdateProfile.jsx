import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form, Row, Col } from 'react-bootstrap';
import {
  PasswordField,
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import { useSelector, useDispatch } from 'react-redux';
import { updateProfile } from '../reduxAction';

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);
const validationSchema = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama user'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Harap untuk mengisi alamat email'),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon'),
  address: Yup.string().required('Harap untuk mengisi alamat user'),
});

const ModalUpdateProfile = (props) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState(null);
  const { isLoading } = useSelector((state) => state.loading);
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      phone_number: '',
      address: '',
      country_code: '62',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmitForm(values);
    },
  });

  const handleSubmitForm = async (payload) => {
    try {
      await dispatch(updateProfile(payload));
      props.onHide();
    } catch (err) {
      setErrors(err?.error?.errors || null);
    }
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
      setErrors(null);
    }
    if (props.initialValues && props.show) {
      const init = { ...props.initialValues };
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('email', init.email);
      formik.setFieldValue('phone_number', init.phone_number);
      formik.setFieldValue('address', init.address);
      formik.setFieldValue('country_code', init.country_code);
      formik.setFieldValue('role_id', init.role_id);
    }
  }, [props.show, props.initialValues]);

  return (
    <>
      <Modal
        titleModal="Edit Profile"
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        content={(data) => (
          <>
            <TextField
              className="mt-3"
              label="Nama User"
              placeholder="Input nama user"
              name="name"
              withValidate
              formik={formik}
            />
            <TextField
              label="Email Address"
              placeholder="Input email address"
              name="email"
              withValidate
              formik={formik}
            />
            {errors?.email && (
              <Form.Text className="text-input-error mt-0">
                {errors?.email[0] || ''}
              </Form.Text>
            )}

            <Row>
              <Col>
                <SelectField
                  label="Nomor Telepon"
                  options={[
                    {
                      value: '62',
                      description: 'Indonesia(+62)',
                    },
                  ]}
                  name="country_code"
                  formik={formik}
                  withValidate
                />
              </Col>
              <Col className="d-flex align-items-end">
                <TextField
                  className="w-100"
                  placeholder="Input nomor telepon"
                  name="phone_number"
                  formik={formik}
                  withValidate
                  withError={false}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
                {formik.errors.phone_number && formik.touched.phone_number && (
                  <Form.Text className="text-input-error">
                    {formik.errors.phone_number}
                  </Form.Text>
                )}
                {errors?.phone_number && (
                  <Form.Text className="text-input-error mt-0">
                    {errors?.phone_number[0] || ''}
                  </Form.Text>
                )}
              </Col>
            </Row>
            <TextArea
              className="mb-3"
              label="Alamat"
              placeholder="Input alamat"
              rows={3}
              withValidate
              name="address"
              formik={formik}
            />
            {/* {errors &&
              Object.keys(errors).map((key) => (
                <Form.Text className="text-input-error">
                  {errors[key][0]}
                </Form.Text>
              ))} */}
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                if (!Boolean(formik.values.role_id)) {
                  formik.setFieldTouched('role_id', true);
                }
                formik.handleSubmit(e);
              }}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading ? 'Loading...' : 'Perbarui'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalUpdateProfile.defaultProps = {
  initialValues: null,
};
export default ModalUpdateProfile;
