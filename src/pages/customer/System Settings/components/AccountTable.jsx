import React, { useState, useEffect } from 'react';
import DataTable from '../../../../components/elements/Table/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { SearchField } from '../../../../components/elements/InputField';
import ModalAddUser from './ModalAddUser';
import ModalDetailUser from './ModalDetailUser';
import ModalDeleteUser from './ModalDeleteUser';
import { useSelector, useDispatch } from 'react-redux';
import { getListTeam } from '../reduxAction';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#4F4F4F',
    marginBottom: '5px',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
    marginBottom: '15px',
  },
});

const ActionField = ({ data, onClick }) => {
  // const history = useHistory();
  return (
    <>
      <Button text="Detail" styleType="lightBlueFill" onClick={onClick} />
    </>
  );
};

const AccountTable = () => {
  const classes = useStyles();
  const [state, setState] = useState({
    showAddUser: false,
    dataDetailUser: null,
    showDetailUser: false,
    showDeleteUser: false,
    search: '',
  });

  const dispatch = useDispatch();
  const { listTeam } = useSelector((state) => state.systemSettingCustomer);
  const { dataUser } = useSelector((state) => state.login);
  const fetchListTeam = (payload) => {
    dispatch(getListTeam(payload));
  };
  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const renderHeaderTable = () => {
    return (
      <div className="d-flex flex-row justify-content-between align-items-center p-3 w-100">
        <div>
          <SearchField placeholder="Cari staff" onEnter={handleSearch} />
        </div>
        <Button
          text="Tambah User"
          onClick={() => setState({ ...state, showAddUser: true })}
          startIcon={() => <AddIcon />}
          styleType={
            dataUser?.role[0]?.display_name === 'Owner'
              ? 'blueFill'
              : 'blueFillDisabled'
          }
          disabled={dataUser?.role[0]?.display_name !== 'Owner'}
        />
      </div>
    );
  };
  return (
    <>
      <DataTable
        action={fetchListTeam}
        totalPage={listTeam?.last_page || 5}
        params={{
          search: state.search,
        }}
        column={[
          {
            heading: 'Nama User',
            key: 'name',
          },
          {
            heading: 'Email',
            key: 'email',
          },
          {
            heading: 'Role',
            key: 'display_name_role',
          },
          {
            heading: 'Alamat',
            key: 'address',
          },
          {
            heading: 'Nomor Telepon',
            key: 'phone_number',
          },
          {
            heading: 'Aksi',
            render: (dataRow) => (
              <ActionField
                onClick={() =>
                  setState({
                    ...state,
                    showDetailUser: true,
                    dataDetailUser: dataRow,
                  })
                }
                data={dataRow}
              />
            ),
          },
        ]}
        data={listTeam?.data || []}
        transformData={(item) => ({
          ...item,
          email: item.email,
          auth: item.auth,
        })}
        renderHeader={renderHeaderTable}
        withNumber={false}
      />
      <ModalAddUser
        show={state.showAddUser}
        onHide={() =>
          setState({ ...state, showAddUser: false, dataDetailUser: null })
        }
        initialValues={state.dataDetailUser}
      />
      <ModalDetailUser
        show={state.showDetailUser}
        onHide={() =>
          setState({ ...state, showDetailUser: false, dataDetailUser: null })
        }
        onClickDelete={() =>
          setState({ ...state, showDetailUser: false, showDeleteUser: true })
        }
        onClickEdit={() =>
          setState({ ...state, showDetailUser: false, showAddUser: true })
        }
        data={state.dataDetailUser}
      />
      <ModalDeleteUser
        show={state.showDeleteUser}
        onHide={() => setState({ ...state, showDeleteUser: false })}
        data={state.dataDetailUser}
      />
    </>
  );
};

export default AccountTable;
