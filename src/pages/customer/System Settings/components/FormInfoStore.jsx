import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { Form } from 'react-bootstrap';
import { TextField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Badge from '../../../../components/elements/Badge';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  storeName: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#000000',
  },
  storeType: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  avatar: {
    width: '64px',
    height: '64px',
    marginRight: '8px',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormInfoStore(props) {
  const classes = useStyles();
  const [state, setState] = useState({ image: '' });
  const inputImage = React.createRef();
  const { formik } = props;
  const handleChangeImage = (e) => {
    setState({ ...state, image: e.target.files[0] });
    formik.setFieldValue('store_logo', e.target.files[0]);
  };

  const { detailStore } = useSelector((state) => state.storeAdministrator);
  useEffect(() => {
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('store_name', init.store_name);
    }
  }, [props.initialValues]);
  return (
    <>
      <Container withHeader={false}>
        {props.isEdit ? (
          <>
            <Item border={false} spaceBottom="3">
              <Form.Group className="mb-1" controlId="formBasicEmail">
                <Form.Label as={'h6'} className="input-label">
                  Logo Store
                </Form.Label>
                <div className="row">
                  <div className="col-12 col-lg-4">
                    <div
                      className="rounded d-flex justify-content-center align-items-center"
                      style={{
                        width: '100px',
                        height: '100px',
                        border: '2px dashed #CFCFCF',
                        backgroundColor: '#F2F2F2',
                        marginTop: '7px',
                        overflow: 'hidden',
                      }}
                    >
                      {state.image || props.initialValues?.store_logo ? (
                        <img
                          style={{ width: '100px' }}
                          src={
                            state.image
                              ? URL.createObjectURL(state.image)
                              : props.initialValues?.store_logo_full_url || ''
                          }
                          alt=""
                        />
                      ) : (
                        <img src={pictureIcon} alt="" />
                      )}
                    </div>
                  </div>
                  <div className="col-12 col-lg-8 pl-3">
                    <div
                      className="text-justify"
                      style={{
                        fontWeight: '400',
                        color: '#4F4F4F',
                        fontSize: '14px',
                      }}
                    >
                      <p>
                        Ukuran optimal 300 x 300 piksel dengan Besar file:
                        Maksimum 10.000.000 bytes (10 Megabytes). Ekstensi file
                        yang diperbolehkan: JPG, JPEG, PNG
                      </p>
                      <Button
                        styleType="blueOutline"
                        onClick={() => inputImage.current.click()}
                        text={state.image ? 'Ganti Foto' : 'Pilih Foto'}
                        style={{
                          width: '100%',
                          maxWidth: '296px',
                          height: '32px',
                        }}
                      />
                    </div>
                  </div>
                </div>
                {formik.touched.store_logo && formik.errors.store_logo && (
                  <Form.Text className="text-input-error">
                    {formik.errors.store_logo}
                  </Form.Text>
                )}
              </Form.Group>
            </Item>
            <Item border={false}>
              <TextField
                label="Nama Store"
                placeholder="Input nama store"
                name="store_name"
                formik={formik}
                withValidate
              />
            </Item>
          </>
        ) : (
          <div>
            <div className="d-flex flex-row">
              <Avatar
                className={classes.avatar}
                alt="Logo"
                variant="rounded"
                src={props.initialValues?.store_logo || ''}
              />
              <div>
                <h6 className={classes.storeName}>
                  {props.initialValues?.store_name}
                </h6>
                <Badge
                  label={
                    props.initialValues?.status === 0
                      ? 'Waiting'
                      : props.initialValues?.status === 1
                      ? 'Verified'
                      : 'Rejected'
                  }
                  styleType={
                    props.initialValues?.status === 0
                      ? 'yellow'
                      : props.initialValues?.status === 1
                      ? 'green'
                      : 'red'
                  }
                />
                <h6 className={classes.storeType}>
                  {props.initialValues?.type || ''}
                </h6>
              </div>
            </div>
            {props.initialValues?.status === 2 && (
              <div className="mt-3">
                <h6 className={classes.label}>Alasan Menolak</h6>
                <h6 className={classes.value}>{props.initialValues?.reason}</h6>
              </div>
            )}
          </div>
        )}
      </Container>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={inputImage}
        type="file"
        className="d-none"
      />
    </>
  );
}
FormInfoStore.defaultProps = {
  initialValues: null,
  isEdit: true,
};

export default FormInfoStore;
