import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

const ModalDetailUser = (props) => {
  const classes = useStyles();
  const { dataUser } = useSelector((state) => state.login);
  return (
    <>
      <Modal
        bodyClassName="pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        titleClassName="w-100"
        data={props.data}
        header={(data) => (
          <div className="d-flex flex-between w-100 align-items-center">
            User Detail
            <div className="ml-auto">
              <Button
                style={{ minWidth: '120px' }}
                styleType={
                  dataUser?.role[0]?.display_name === 'Owner' &&
                  Number(dataUser?.user?.id) !== data?.id
                    ? 'redOutline'
                    : 'redOutlineDisabled'
                }
                text="Hapus"
                onClick={props.onClickDelete}
                disabled={
                  dataUser?.role[0]?.display_name === 'Owner' &&
                  Number(dataUser?.user?.id) !== data?.id
                    ? false
                    : true
                }
              />
              <Button
                style={{ minWidth: '120px' }}
                className="ml-1"
                styleType="blueOutline"
                styleType={
                  dataUser?.role[0]?.display_name === 'Owner' &&
                  Number(dataUser?.user?.id) !== data?.id
                    ? 'blueOutline'
                    : 'blueOutlineDisabled'
                }
                text="Edit"
                onClick={props.onClickEdit}
                disabled={
                  dataUser?.role[0]?.display_name === 'Owner' &&
                  Number(dataUser?.user?.id) !== data?.id
                    ? false
                    : true
                }
              />
            </div>
          </div>
        )}
        content={(data) => (
          <>
            <div className="mt-3">
              <h6 className={classes.text}>Nama User</h6>
              <h6 className={classes.text}>{data?.name || 'No.Data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Email</h6>
              <h6 className={classes.text}>{data?.email || 'No.Data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Role</h6>
              <h6 className={classes.text}>
                {data?.display_name_role || 'No.Data'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Nomor Telepon</h6>
              <h6 className={classes.text}>
                {data?.phone_number || 'No.Data'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Alamat</h6>
              <h6 className={classes.text}>{data?.address || 'No.Data'}</h6>
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              onClick={props.onHide}
              text="Kembali"
              styleType="lightBlueFill"
            />
          </>
        )}
        footerClassName="p-3"
      />
    </>
  );
};

export default ModalDetailUser;
