import actionType from './reduxConstant';

const initialState = {
  listTeam: null,
  infoAccount: {},
  listRole: [],
  listCategoryProduct: [{ value: '', description: 'Pilih Kategori' }],
  listSubCategoryProduct: [{ value: '', description: 'Pilih Sub Kategori' }],
  listSKU: {},
  detailStore: {},
};

const SystemSettingCustomer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_TEAM_USER_CUSTOMER:
      return {
        ...prevState,
        listTeam: action.payload,
      };
    case actionType.LIST_ROLE_USER_CUSTOMER:
      return {
        ...prevState,
        listRole: action.payload,
      };
    case actionType.DETAIL_ACCOUNT_CUSTOMER:
      return {
        ...prevState,
        infoAccount: action.payload,
      };
    case actionType.LIST_CATEGORY_PRODUCT_CUSTOMER: {
      const category = action.payload.map((item) => ({
        value: item.id,
        description: item.name,
      }));
      return {
        ...prevState,
        listCategoryProduct: [
          { value: '', description: 'Pilih Kategori' },
          ...category,
        ],
      };
    }
    case actionType.LIST_SUB_CATEGORY_PRODUCT_CUSTOMER: {
      const subCategory = action.payload.map((item) => ({
        value: item.id,
        description: item.name,
      }));
      return {
        ...prevState,
        listSubCategoryProduct: [
          { value: '', description: 'Pilih Sub Kategori' },
          ...subCategory,
        ],
      };
    }
    case actionType.LIST_PRODUCT_SKU_CUSTOMER:
      return {
        ...prevState,
        listSKU: action.payload,
      };
    case actionType.SET_DETAIL_STORE_CUSTOMER:
      return {
        ...prevState,
        detailStore: action.payload,
      };
    default:
      return prevState;
  }
};

export default SystemSettingCustomer;
