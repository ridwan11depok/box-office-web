import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

export const getListTeam =
  (params = {}) =>
  async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      dispatch(setLoadingTable(true));
      let data = {};
      let headers = { token: token };
      let response = await consume.getWithParams(
        'userAccountTeam',
        data,
        headers,
        (params = { ...params, store_id: storeId })
      );
      if (response) {
        const listTeamData = response.result.data.map((item) => ({
          ...item,
          role_id: item?.roles[0]?.id,
          display_name_role: item?.roles[0]?.display_name,
        }));
        dispatch(listTeam({ ...response.result, data: listTeamData }));
      }
      dispatch(setLoadingTable(false));
    } catch (err) {
      dispatch(setLoadingTable(false));
    }
  };

const listTeam = (data) => {
  return {
    type: actionType.LIST_TEAM_USER_CUSTOMER,
    payload: data,
  };
};

export const getDetailAccount = () => async (dispatch, getState) => {
  try {
    const { token } = getState().login;
    dispatch(setLoading(true));
    let data = {};
    let headers = { token: token };
    let response = await consume.getWithParams(
      'userAccountDetail',
      data,
      headers
    );
    if (response) {
      const detailAccount = {
        ...response.result,
        role_id: response.result.roles[0].id,
        display_name_role: response.result.roles[0].display_name,
      };
      dispatch(setDetailAccount(detailAccount));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(setDetailAccount({}));
  }
};

const setDetailAccount = (payload) => {
  return {
    type: actionType.DETAIL_ACCOUNT_CUSTOMER,
    payload,
  };
};

export const getListRole = () => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    let data = {};
    let headers = { token: token };
    let response = await consume.getWithParams('listRoleTeam', data, headers, {
      store_id: storeId,
    });
    if (response) {
      dispatch(listRole(response.result));
      dispatch(setLoading(false));
    }
  } catch (err) {
    dispatch(setLoading(false));
  }
};

const listRole = (data) => {
  return {
    type: actionType.LIST_ROLE_USER_CUSTOMER,
    payload: data,
  };
};

export const addUser =
  (payload = {}) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      let data = { ...payload, store_id: storeId };
      let headers = { token: token };
      let response = await consume.post('userAccountTeam', data, headers);
      if (response) {
        dispatch(getListTeam({ page: 1, per_page: 10 }));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menambah user baru.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menambah user baru.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const updateUser =
  (payload = {}, userId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      const userIdLogin = dataUser?.user?.id;
      let data = {};
      let headers = { token: token };
      let params = { ...payload, store_id: storeId };
      let id = userId;
      const response = await consume.put(
        'userAccountTeam',
        data,
        headers,
        params,
        id
      );
      if (response) {
        if (userIdLogin === userId) {
          dispatch(getDetailAccount());
        }
        dispatch(getListTeam({ page: 1, per_page: 10 }));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data user.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data user.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const deleteUser = (userId) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));

    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    let data = { store_id: storeId };
    let headers = { token: token };
    let id = userId;
    const response = await consume.delete('userAccountTeam', data, headers, id);
    if (response) {
      dispatch(getListTeam({ page: 1, per_page: 10 }));
    }
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menghapus data user.',
        type: 'success',
      })
    );
    dispatch(setLoading(false));
    return Promise.resolve('success');
  } catch (err) {
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal menghapus data user.',
        type: 'error',
      })
    );
    dispatch(setLoading(false));
    return Promise.reject('failed');
  }
};

export const changePassword = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };
    const response = await consume.post(
      'userAccountChangePassword',
      data,
      headers
    );
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil mengubah password.',
        type: 'success',
      })
    );
    dispatch(setLoading(false));
    return Promise.resolve('success');
  } catch (err) {
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal mengubah password.',
        type: 'error',
      })
    );
    dispatch(setLoading(false));
    return Promise.reject(err);
  }
};

export const getListCategory =
  (id = null) =>
  async (dispatch, getState) => {
    try {
      const { token } = getState().login;
      dispatch(setLoading(true));
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.getWithParams(
          'subCategoryProductCustomer',
          data,
          headers,
          {
            category_product_id: id,
          }
        );
      } else {
        response = await consume.get('categoryProductCustomer', data, headers);
      }

      if (response) {
        if (id) {
          dispatch(listSubCategoryProduct(response.result));
        } else {
          dispatch(listCategoryProduct(response.result));
        }
        dispatch(setLoading(false));
      }
    } catch (err) {
      dispatch(setLoading(false));
    }
  };

const listCategoryProduct = (data) => {
  return {
    type: actionType.LIST_CATEGORY_PRODUCT_CUSTOMER,
    payload: data,
  };
};

const listSubCategoryProduct = (data) => {
  return {
    type: actionType.LIST_SUB_CATEGORY_PRODUCT_CUSTOMER,
    payload: data,
  };
};

export const getListProductSKU =
  (params = {}) =>
  async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      dispatch(setLoadingTable(true));
      let data = {};
      let headers = { token: token };
      let response = await consume.getWithParams('productSku', data, headers, {
        ...params,
        store_id: storeId,
      });
      if (response) {
        dispatch(listProductSKU(response.result));
      }
      dispatch(setLoadingTable(false));
    } catch (err) {
      dispatch(setLoadingTable(false));
    }
  };

const listProductSKU = (data) => {
  return {
    type: actionType.LIST_PRODUCT_SKU_CUSTOMER,
    payload: data,
  };
};

export const addProductSKU =
  (payload = {}) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token, content_type: 'multipart/form-data' };
      let response = await consume.post('productSku', data, headers);
      if (response) {
        dispatch(getListProductSKU({ page: 1, per_page: 10 }));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menambah SKU baru.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menambah SKU baru.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const getDetailStore = () => async (dispatch, getState) => {
  try {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    dispatch(setLoading(true));
    let data = {};
    let headers = { token: token };
    let response = await consume.getWithParams(
      'storeDetailCustomer',
      data,
      headers,
      {
        store_id: storeId,
      }
    );
    if (response) {
      dispatch(detailStore(response.result));
    }
    dispatch(setLoading(false));
  } catch (err) {
    dispatch(setLoading(false));
  }
};

const detailStore = (data) => {
  return {
    type: actionType.SET_DETAIL_STORE_CUSTOMER,
    payload: data,
  };
};

export const updateProductSKU =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token, content_type: 'multipart/form-data' };
      const otherEndpoint = `update/${id}`;
      let response = await consume.post(
        'productSku',
        data,
        headers,
        otherEndpoint
      );
      if (response) {
        dispatch(getListProductSKU({ page: 1, per_page: 10 }));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui SKU.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui SKU.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const deleteBatchProductSKU =
  (payload = {}) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const { listSKU } = getState().systemSettingCustomer;
      const storeId = dataUser?.store[0]?.id;
      let data;
      if (Object.keys(payload).length) {
        data = { ...payload, store_id: storeId };
      } else {
        const product_id = listSKU?.data?.map((item) => item.id) || [];
        data = { product_id, store_id: storeId };
      }
      let headers = { token: token };
      let response = await consume.delete(
        'deleteProductBatchCustomer',
        data,
        headers
      );
      if (response) {
        dispatch(getListProductSKU({ page: 1, per_page: 10 }));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus SKU.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus SKU.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const updateStore =
  (payload = {}) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token, content_type: 'multipart/form-data' };
      let response = await consume.post('updateStoreCustomer', data, headers);
      if (response) {
        dispatch(getDetailStore());
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui store.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui store.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const updateProfile =
  (payload = {}) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const { listTeam } = getState().systemSettingCustomer;
      const userId = dataUser?.user?.id;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post('updateProfileCustomer', data, headers);
      if (response) {
        dispatch(getDetailAccount());
        if (listTeam?.data?.filter((item) => item.id === userId).length) {
          dispatch(getListTeam({ page: 1, per_page: 10 }));
        }
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data profile.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data profile.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };
