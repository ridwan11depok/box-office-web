const actionType = {
  LIST_TEAM_USER_CUSTOMER: 'LIST_TEAM_USER_CUSTOMER',
  LIST_ROLE_USER_CUSTOMER: 'LIST_ROLE_USER_CUSTOMER',
  DETAIL_ACCOUNT_CUSTOMER: 'DETAIL_ACCOUNT_CUSTOMER',
  LIST_PRODUCT_SKU_CUSTOMER: 'LIST_PRODUCT_SKU_CUSTOMER',
  LIST_CATEGORY_PRODUCT_CUSTOMER: 'LIST_CATEGORY_PRODUCT_CUSTOMER',
  LIST_SUB_CATEGORY_PRODUCT_CUSTOMER: 'LIST_SUB_CATEGORY_PRODUCT_CUSTOMER',
  SET_DETAIL_STORE_CUSTOMER: 'SET_DETAIL_STORE_CUSTOMER',
};
export default actionType;
