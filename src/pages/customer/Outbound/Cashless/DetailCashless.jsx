import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import TableProductSKU from '../components/TableProductSKU';
import BackButton from '../../../../components/elements/BackButton';
import Button from '../../../../components/elements/Button';
import { useHistory, useParams } from 'react-router-dom';
import FormInvoice from '../components/FormInvoice';
import FormDataPengirim from '../components/FormDataPengirim';
import FormDataPenerima from '../components/FormDataPenerima';
import { useSelector, useDispatch } from 'react-redux';
import Badge from '../../../../components/elements/Badge';
import ModalCancelTransaction from '../components/ModalCancelCashless';
import { getCashless } from '../reduxAction';
import { Table } from '../../../../components/elements/Table';
import { formatRupiah, statusDispatchOrder } from '../../../../utils/text';
import DetailLogistik from '../components/DetailLogistik';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    textAlign: 'right',
    margin: '10px 0',
  },
  label: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    textAlign: 'left',
    margin: '10px 0',
  },
  total: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '15px 0',
  },
});
const DetailDispatchOrder = () => {
  const classes = useStyles();
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();

  const { detailCashless: detailDispatchOrder } = useSelector(
    (state) => state.outboundCustomer
  );
  const [state, setState] = useState({
    isEdit: false,
    cancelTransactionModal: false,
  });

  const handleCancelTransaction = () => {
    setState({
      ...state,
      cancelTransactionModal: true,
    });
  };

  useEffect(() => {
    dispatch(getCashless({}, params?.id));
  }, [params?.id]);

  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label={
                <div>
                  Detail Cashless
                  <Badge
                    style={{
                      fontSize: '14px',
                      fontWeight: '700',
                      padding: '16px 8px',
                      marginLeft: '10px',
                    }}
                    styleType={
                      detailDispatchOrder?.status === 6
                        ? 'green'
                        : detailDispatchOrder?.status === 7 ||
                          detailDispatchOrder?.status === 8 ||
                          detailDispatchOrder?.status === 9
                        ? 'red'
                        : detailDispatchOrder?.status === 0
                        ? 'black'
                        : 'yellow'
                    }
                    label={statusDispatchOrder(
                      detailDispatchOrder?.status || 0
                    )}
                  />
                </div>
              }
              onClick={() => history.goBack()}
              // onClick={() => history.push('/outbound/cashless')}
            />
            <div>
              {detailDispatchOrder?.status === 0 && (
                <>
                  <Button
                    style={{ minWidth: '140px' }}
                    styleType="redOutline"
                    text="Batal Transaksi"
                    onClick={handleCancelTransaction}
                  />
                  {/* <Button
                    className="ml-auto"
                    style={{ minWidth: '140px' }}
                    styleType="blueOutline"
                    text="Edit"
                  /> */}
                </>
              )}
            </div>
          </div>
        )}
      >
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: state.isEdit ? '615px' : '400px' }}
        >
          <FormInvoice isEdit={state.isEdit} data={detailDispatchOrder} />
        </ContentItem>
        <ContentItem
          className="p-3"
          spaceRight="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: state.isEdit ? '615px' : '400px' }}
        >
          <DetailLogistik data={detailDispatchOrder} />
        </ContentItem>
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: state.isEdit ? '615px' : '350px' }}
        >
          <FormDataPengirim isEdit={state.isEdit} data={detailDispatchOrder} />
        </ContentItem>
        <ContentItem
          className="p-3"
          spaceRight="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: state.isEdit ? '615px' : '350px' }}
        >
          <FormDataPenerima isEdit={state.isEdit} data={detailDispatchOrder} />
        </ContentItem>
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <TableProductSKU isEdit={state.isEdit} data={detailDispatchOrder} />
        </ContentItem>
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <Table
            renderHeader={() => (
              <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
                <h6 className="mb-2 mt-2">Packaging</h6>
              </div>
            )}
            column={[
              {
                heading: 'Nama Packaging',
                key: 'name',
              },
              {
                heading: 'Ukuran',
                key: 'size',
              },
              {
                heading: 'Nilai Packaging',
                key: 'value',
              },
              {
                heading: 'Qty',
                key: 'qty',
              },
            ]}
            data={detailDispatchOrder?.packagings || []}
            transformData={(item) => ({
              ...item,
              name: item?.packaging?.name || '-',
              size: `${item?.packaging?.length || 0}cm x ${
                item?.packaging?.width || 0
              }cm ${
                item?.packaging?.height ? `x ${item?.packaging?.height}cm` : ''
              }`,
              qty: item?.quantity_user || '-',
              value: formatRupiah(item?.price_user || 0),
            })}
            showFooter={false}
            withNumber={false}
          />
        </ContentItem>
        <ContentItem className="p-3" col="col-12" spaceBottom={3}>
          <h6>Total Pembayaran</h6>
          <Row>
            <Col xs={6}>
              <h6 className={classes.label}>Biaya Packaging</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>
                {formatRupiah(
                  detailDispatchOrder?.total_default_packaging || 0
                )}
              </h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.total}>Total Pembayaran</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.total} style={{ textAlign: 'right' }}>
                {formatRupiah(detailDispatchOrder?.total_price || 0)}
              </h6>
            </Col>
          </Row>
        </ContentItem>
      </ContentContainer>
      <ModalCancelTransaction
        data={detailDispatchOrder}
        show={state.cancelTransactionModal}
        onHide={() => setState({ ...state, cancelTransactionModal: false })}
      />
    </>
  );
};

export default DetailDispatchOrder;
