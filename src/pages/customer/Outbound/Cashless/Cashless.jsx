import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../../components/elements/BaseContent';
import { TabHeader } from '../../../../components/elements/Tabs';
import { makeStyles } from '@material-ui/core/styles';
import TableDispatchOrder from '../components/TableDispatchOrder';
import { WarehouseSelect } from '../../../../components/elements/InputField';
import { getListStorage } from '../../Inbound/reduxAction';
import { setSelectedStorage } from '../reduxAction';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const Cashless = () => {
  const { selectedStorage } = useSelector(state => state.outboundCustomer)
  const [state, setState] = useState({
    tab: 0,
    listStorage: [],
    storageId: selectedStorage?.id || null,
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const fetchListStorage = async () => {
    try {
      const res = await dispatch(getListStorage());
      setState({
        ...state,
        listStorage: res.map((item) => ({
          ...item,
          name: item.warehouse?.name,
          address: item.warehouse?.address,
          status: item.warehouse?.status === 1 ? 'open' : 'tutup',
        })),
      });
    } catch (err) {}
  };


  const handleSelectWarehouse = (storage) => {
    setState({ ...state, storageId: storage?.id });
    dispatch(setSelectedStorage(storage));
  };

  useEffect(() => {
    fetchListStorage();
  }, [state.tab]);

  useEffect(() => {
    if(selectedStorage){
      setState({...state, storageId: selectedStorage?.id})
    } else {
      setState({...state, storageId: null})
      dispatch(setSelectedStorage(null));
    }
  }, [selectedStorage]);

  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
            <PageTitle title="Cashless" />
            <div className="d-flex flex-row align-items-center">
              <h6 className={classes.selectWarehouse}>Pilih Gudang :</h6>
              <WarehouseSelect
                defaultValue={selectedStorage || ''}
                options={state.listStorage}
                onChange={handleSelectWarehouse}
              />
            </div>
          </div>
        )}
      >
        <ContentItem col="col-12" spaceBottom={3}>
          <TabHeader
            listHeader={[
              {
                title: 'Semua Transaksi',
                tab: 0,
              },
              {
                title: 'Transaksi Proses',
                tab: 1,
              },
              {
                title: 'Transaksi Selesai',
                tab: 2,
              },
              {
                title: 'Transaksi Batal',
                tab: 3,
              },
            ]}
            activeTab={state.tab}
            border={false}
            onChange={(tab) => setState({ tab })}
          />
        </ContentItem>
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <TableDispatchOrder tab={state.tab} storageId={state.storageId} />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default Cashless;
