import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import BackButton from '../../../../components/elements/BackButton';
import { useHistory } from 'react-router-dom';
import './styles.css';
import { TabHeader, TabContent } from '../../../../components/elements/Tabs';
import FormDataInvoiceDispatchOrder from '../components/FormDataInvoiceDispatchOrder';
import FormSenderAndReceiverDispatchOrder from '../components/FormSenderAndReceiverDispatchOrder';
import FormDataProductAndPackaging from '../components/FormDataProductAndPackaging';
import { getListPackaging, getListProductStorage, setDataFormCreateDispatch } from '../reduxAction';
import { useDispatch, useSelector } from 'react-redux';

const itemTab = (props, item) => {
  const { activeColor, activeTab, inActiveColor } = props;
  const { tab, title } = item;

  return (
    <div
      className="w-100"
      style={{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        fontWeight: '600',
        color:
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF',
        textAlign: 'left',
        borderBottom: `8px solid ${
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF'
        }`,
      }}
    >
      <h6>
        <span
          style={{
            borderRadius: '100%',
            backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
            width: '24px',
            height: '24px',
            padding: '3px 5px',
          }}
        >
          {tab < activeTab ? (
            <i className="fa fa-check" aria-hidden="true"></i>
          ) : (
            `0${tab}`
          )}
        </span>{' '}
        {title}
      </h6>
    </div>
  );
};

const Transactions = () => {
  const history = useHistory();
  const [step, setStep] = useState(1);
  const handleChangeTab = (tab) => setStep(tab);

  const dispatch = useDispatch();
  const { selectedStorage } = useSelector((state) => state.outboundCustomer);
  useEffect(() => {
    if (selectedStorage) {
      dispatch(getListProductStorage(selectedStorage?.id));
      dispatch(getListPackaging(selectedStorage?.warehouse?.id));
    } else {
      history.push('/outbound/cashless');
    }
  }, []);

  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Cashless"
              onClick={() => {
                dispatch(setDataFormCreateDispatch({}))
                history.push('/outbound/cashless')
              }}
            />
          </div>
        )}
      >
        <ContentItem border={false} spaceBottom={3}>
          <TabHeader
            border={false}
            activeTab={step}
            activeBackground="transparent"
            inActiveBackground="transparent"
            activeColor="#828282"
            inActiveColor="#192a55"
            listHeader={[
              {
                tab: 1,
                title: 'Data Invoice',
                render: itemTab,
              },
              {
                tab: 2,
                title: 'Data Pengirim dan Penerima',
                render: itemTab,
              },
              {
                tab: 3,
                title: 'Data Produk dan Packaging',
                render: itemTab,
              },
            ]}
            isClickable={false}
          />
        </ContentItem>
        <TabContent tab={1} activeTab={step}>
          <FormDataInvoiceDispatchOrder handleChangeTab={handleChangeTab} />
        </TabContent>
        <TabContent tab={2} activeTab={step}>
          <FormSenderAndReceiverDispatchOrder
            handleChangeTab={handleChangeTab}
          />
        </TabContent>

        <TabContent tab={3} activeTab={step}>
          <FormDataProductAndPackaging handleChangeTab={handleChangeTab} />
        </TabContent>
      </ContentContainer>
    </>
  );
};

export default Transactions;
