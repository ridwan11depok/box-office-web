import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col } from 'react-bootstrap';
import { formatRupiah } from '../../../../utils/text';
import Avatar from '@material-ui/core/Avatar';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
  price: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#4f4f4f',
    textAlign: 'right',
  },
});

function DetailTotalPaymentRegTrans() {
  const classes = useStyles();
  const { detailRegularTransaction } = useSelector(
    (state) => state.outboundCustomer
  );
  return (
    <div>
      <h6>Total Pembayaran</h6>

      <Row>
        <Col xs={8}>
          <div className="mb-3">
            <h6 className={classes.label}>Total Pembayaran Logistik</h6>
          </div>
        </Col>
        <Col xs={4}>
          <div className="mb-3">
            <h6 className={classes.price}>
              {formatRupiah(
                (detailRegularTransaction?.vendor_price || 0) +
                  (detailRegularTransaction?.insurance_price || 0)
              )}
            </h6>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={8}>
          <div className="mb-3">
            <h6 className={classes.label}>Biaya Pickup</h6>
          </div>
        </Col>
        <Col xs={4}>
          <div className="mb-3">
            <h6 className={classes.price}>
              {formatRupiah(detailRegularTransaction?.pickup_price || 0)}
            </h6>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={8}>
          <div className="mb-3">
            <h6
              className={classes.label}
              style={{ color: '#1C1C1C', fontWeight: '700' }}
            >
              Total Pembayaran
            </h6>
          </div>
        </Col>
        <Col xs={4}>
          <div className="mb-3">
            <h6 className={classes.price} style={{ color: '#1C1C1C' }}>
              {formatRupiah(
                (detailRegularTransaction?.vendor_price || 0) +
                  (detailRegularTransaction?.insurance_price || 0) +
                  (detailRegularTransaction?.pickup_price || 0)
              )}
            </h6>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DetailTotalPaymentRegTrans;
