import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

function FormDataPengirim(props) {
  const classes = useStyles();
  const { isEdit, data, formik } = props;
  return (
    <div>
      <h6>Data Pengirim</h6>
      {isEdit ? (
        <>
          {/* <Item border={false}> */}
          <TextField
            label="Nama Pengirim"
            placeholder="Input nama pengirim"
            withValidate
            formik={formik}
            name="sender_name"
          />
          <Row>
            <Col>
              <SelectField
                label="Nomor Telepon Pelanggan"
                options={[
                  {
                    value: '62',
                    description: 'Indonesia(+62)',
                  },
                ]}
                withValidate
                formik={formik}
                name="sender_country_code"
                withError={false}
              />
            </Col>
            <Col className="d-flex align-items-end">
              <TextField
                className="w-100"
                placeholder="Input nomor telepon"
                withValidate
                formik={formik}
                name="sender_phone"
                withError={false}
              />
            </Col>
            {formik.touched.sender_phone && formik.errors.sender_phone && (
              <Col xs={12}>
                <Form.Text className="text-input-error mb-3 mt-0">
                  {formik.errors.sender_phone}
                </Form.Text>
              </Col>
            )}
          </Row>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox"
              label="Tampilkan logo bisnis"
              onChange={(e) =>
                formik.setFieldValue('show_logo', e.target.checked ? 1 : 0)
              }
            />
          </Form.Group>
          {/* </Item> */}
        </>
      ) : (
        <>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Nama Pengirim</h6>
            <h6 className={classes.value}>{data?.sender_name || '-'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Nomor Telepon</h6>
            <h6 className={classes.value}>{data?.sender_phone || '-'}</h6>
          </Item>
          {/* <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Alamat Pengirim</h6>
            <h6 className={classes.value}>{data?.sender_phone || '-'}</h6>
          </Item> */}
        </>
      )}
    </div>
  );
}

FormDataPengirim.defaultProps = {
  isEdit: true,
  data: {},
  formik: {},
};

export default FormDataPengirim;
