import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import GetAppIcon from '@material-ui/icons/GetApp';
import BatchInvoiceModal from './BatchInvoiceModal';
import { getDispatchOrder, exportLogsDispatchOrder, downloadTemplateBatchDispatchOrder, uploadInvoiceDispatchOrder } from '../reduxAction';
import AddIcon from '@material-ui/icons/Add';
import Badge from '../../../../components/elements/Badge';
import { DatePicker } from '../../../../components/elements/InputField';
import moment from 'moment';
import Toast from '../../../../components/elements/Toast';
import { statusDispatchOrder } from '../../../../utils/text';
import { useDownloadFile } from '../../../../utils/hook';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data }) => {
  const history = useHistory();
  return (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => {
        history.push(`/outbound/dispatch-order/detail/${data?.id}`);
      }}
    />
  );
};

const EmptyTransaction = ({ onClick }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada transaksi Dispatch Order yang dilakukan saat ini.
      </h6>
      <Button
        startIcon={() => <AddIcon />}
        onClick={onClick}
        text="Buat Dispatch Order"
        styleType="blueFill"
      />
    </div>
  );
};

const HeaderTable = (props) => {
  const {
    handleAddDispatchOrder,
    handleBatchInvoice,
    handleFilterDate,
    handleExportLogs,
  } = props;
  const getFileName = () => {
    return `Dispatch Order.xlsx`;
  };
  const { ref, url, download, name } = useDownloadFile({
    apiDefinition: handleExportLogs,
    // preDownloading,
    // postDownloading,
    // onError: onErrorDownloadFile,
    getFileName,
  });
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div className="mt-1 mb-1">
        <DatePicker
          format="MMMM YYYY"
          views={['year', 'month']}
          noMarginBottom
          onDateChange={handleFilterDate}
          placeholder="Pilih bulan"
        />
      </div>
      <div className="d-flex flex-row flex-wrap mt-1 mb-1">
        <a href={url} download={name} className="hidden" ref={ref} />
        <Button
          startIcon={() => <GetAppIcon />}
          styleType="blueOutline"
          text="Export Logs"
          className="mt-1 mr-1 mb-1"
          onClick={download}
        />
        <Button
          className="mt-1 mr-1 mb-1"
          styleType="blueOutline"
          text="Batch Invoice"
          onClick={handleBatchInvoice}
        />
        <Button
          className="mt-1 mr-1 mb-1"
          startIcon={() => <AddIcon />}
          styleType="blueFill"
          text="Buat Dispatch Order"
          onClick={handleAddDispatchOrder}
        />
      </div>
    </div>
  );
};

const StatusField = ({ status }) => {
  return (
    <Badge
      styleType={
        status === 6
          ? 'green'
          : status === 7 || status === 8 || status === 9
          ? 'red'
          : status == 0
          ? 'black'
          : 'yellow'
      }
      label={statusDispatchOrder(status)}
      size="md"
      style={{ fontSize: '14px' }}
    />
  );
};

function TransactionsTable(props) {
  const { tab, storageId } = props;
  const dispatch = useDispatch();
  const history = useHistory();
  const { dispatchOrder, selectedStorage } = useSelector(
    (state) => state.outboundCustomer
  );
  const { dataUser } = useSelector(
    (state) => state.login
  );

  const [state, setState] = useState({
    modalTransaction: false,
    modalBatchInvoice: false,
    date: null,
  });
  const handleAddDispatchOrder = () => {
    if (!selectedStorage) {
      Toast(
        { type: 'warning', message: 'Harap memilih gudang terlebih dahulu' },
        { autoClose: 5000, position: 'top-right' }
      );
    } else {
      history.push('/outbound/dispatch-order/add');
    }
  };
  const handleBatchInvoice = () => {
    setState({ ...state, modalBatchInvoice: true });
  };

  const handleFetchDispatchOrder = (params) => {
    dispatch(getDispatchOrder(params));
  };

  const handleFilterDate = (date) => {
    if (date) {
      setState({ ...state, date: moment(date).format('YYYY-MM') });
    } else {
      setState({ ...state, date: null });
    }
  };

  const handleExportLogs = async () => {
    const params = {
      date: state.date,
      status: tab,
      storage_id: selectedStorage?.id,
    };
    const response = await dispatch(exportLogsDispatchOrder(params));
    const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `Export Logs D.xlsx`);
      link.setAttribute('download', `Export Logs Dispatch Order.xlsx`);
      document.body.appendChild(link);
      link.click();
  };

  const handleDownloadTemplate = async () => {
    try {
      const response = await dispatch(downloadTemplateBatchDispatchOrder())
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `Template Batch Dispatch Order.xlsx`);
      document.body.appendChild(link);
      link.click();
    }
    catch (err) { }
  }

  const handleUploadInvoice = async (file) => {
    try{
      if(!selectedStorage?.id){
        Toast(
          { type: 'warning', message: 'Harap memilih gudang terlebih dahulu' },
          { autoClose: 2000, position: 'top-right' }
        );
      } else if (!file){
        Toast(
          { type: 'error', message: 'Anda belum memilih file invoice' },
          { autoClose: 2000, position: 'top-right' }
        );
      } else {
        const storeId = dataUser?.store[0]?.id
        const data = new FormData()
        data.append('storage_id', selectedStorage.id)
        data.append('file', file)
        data.append('store_id', storeId)
        const response = await dispatch(uploadInvoiceDispatchOrder(data))
        console.log('response', response);
        if(response?.status === 200){
          Toast(
            { type: 'success', message: response?.message || 'Sukses mengupload batch invoice' },
            { autoClose: 2000, position: 'top-right' }
          );
          setState({...state, modalBatchInvoice: false})
        } else {
          Toast(
            { type: 'error', message: response?.message || 'Gagal mengupload batch invoice' },
            { autoClose: 2000, position: 'top-right' }
          );
        }
      }
    } 
    catch(err){
      Toast(
        { type: 'error', message: err?.message || 'Gagal mengupload batch invoice' },
        { autoClose: 2000, position: 'top-right' }
      );
    }
  }
  

  return (
    <div>
      <DataTable
        action={handleFetchDispatchOrder}
        params={{ status: tab, storage_id: storageId, date: state.date }}
        column={[
          {
            heading: 'Tanggal ',
            key: 'date',
          },
          {
            heading: 'Nomor Invoice',
            key: 'noinvoice',
          },
          {
            heading: 'Nama Penerima',
            key: 'receiver',
          },
          {
            heading: 'Kurir',
            key: 'courier',
          },
          {
            heading: 'Status',
            render: (data) => <StatusField status={data?.status || 0} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={dispatchOrder?.data || []}
        transformData={(item) => ({
          ...item,
          date: moment(item.created_at).format('DD MMMM YYYY'),
          noinvoice: item.invoice_number,
          receiver: item.receiver_name,
          courier: item.vendor_service.vendor.name,
        })}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddDispatchOrder={handleAddDispatchOrder}
            handleBatchInvoice={handleBatchInvoice}
            handleFilterDate={handleFilterDate}
            handleExportLogs={handleExportLogs}
          />
        )}
        renderEmptyData={() => (
          <EmptyTransaction onClick={handleAddDispatchOrder} />
        )}
        totalPage={dispatchOrder?.last_page || 1}
      />

      <BatchInvoiceModal
        onHide={() => setState({ ...state, modalBatchInvoice: false })}
        show={state.modalBatchInvoice}
        onDownloadTemplate={handleDownloadTemplate}
        onUploadInvoice={handleUploadInvoice}
      />
    </div>
  );
}

export default TransactionsTable;
