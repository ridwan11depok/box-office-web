import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  ReactSelect,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import { getListVendor, getListServiceVendor } from '../reduxAction';
import { Row, Col, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { get_url_extension } from '../../../../utils/text';
import PreviewPDF from '../../../../components/elements/PreviewPDF';
import { PictureAsPdf } from '@material-ui/icons';
import ModalShowInvoice from './ModalShowInvoice';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});
function FormInvoice(props) {
  const { isEdit, data, formik } = props;
  const classes = useStyles();
  const dispatch = useDispatch();

  const [state, setState] = useState({
    imageINV: '',
    vendorId: '',
    serviceVendorId: '',
    showModalInvoice: false
  });
  const invoiceImageRef = React.createRef();

  const { listVendor, listServiceVendor } = useSelector(
    (state) => state.outboundCustomer
  );

  const handleChangeImage = (e) => {
    setState({ ...state, imageINV: e.target.files[0] });
    formik.setFieldValue('invoice_file', e.target.files[0]);
  };

  const handleChangeVendor = (e) => {
    setState({ ...state, vendorId: Number(e.target.value) });
    dispatch(getListServiceVendor(Number(e.target.value)));
  };

  const handleChangeServiceVendor = (e) => {
    setState({ ...state, serviceVendorId: Number(e.target.value) });
  };

  useEffect(() => {
    if (listVendor.length === 0) {
      dispatch(getListVendor());
    }
  }, []);


  return (
    <>
      <Container withHeader={false}>
        <h6>Invoice</h6>
        {isEdit ? (
          <>
            <Item border={false} spaceBottom="3">
              <div
                style={{
                  height: '130px',
                  borderRadius: '4px',
                  border: '2px dashed #CFCFCF',
                  backgroundColor: '#F2F2F2',
                  overflow: 'hidden',
                  position: 'relative',
                }}
                className="d-flex flex-column justify-content-center align-items-center "
              >
                {state.imageINV ? (
                  <>
                    <Button
                      style={{
                        position: 'absolute',
                        right: '10px',
                        top: '10px',
                      }}
                      styleType="lightBlueFillOutline"
                      text="Ganti Invoice"
                      onClick={() => invoiceImageRef.current.click()}
                    />
                    <img
                      src={URL.createObjectURL(state.imageINV)}
                      alt=""
                      style={{ height: '130px' }}
                    />
                  </>
                ) : (
                  <>
                    <img className="mt-2" src={pictureIcon} alt="" />
                    <h6
                      className="mt-2"
                      style={{
                        fontSize: '14px',
                        fontWeight: '400',
                        fontFamily: 'Open Sans',
                        color: '#1c1c1c',
                      }}
                    >
                      Upload bukti gambar invoice di sini
                    </h6>
                    <Button
                      className="mt-2 mb-3"
                      styleType="blueOutline"
                      text="Upload Invoice"
                      onClick={() => invoiceImageRef.current.click()}
                    />
                  </>
                )}
              </div>
              {formik.touched.invoice_file && formik.errors.invoice_file && (
                // <Col xs={12}>
                <Form.Text className="text-input-error mb-3 mt-0">
                  {formik.errors.invoice_file}
                </Form.Text>
                // </Col>
              )}
            </Item>
            <Item border={false}>
              <TextField
                label="No Invoice (Wajib)"
                placeholder="Input nomor invoice"
                withValidate
                formik={formik}
                name="invoice_number"
              />
              <TextField
                label="Pesan (Opsional)"
                placeholder="Input pesan di sini"
                withValidate
                formik={formik}
                name="message"
              />
              <Row>
                <Col xs={6}>
                  <ReactSelect
                    className="mb-3"
                    label="Logistik (Wajib)"
                    placeholder="Pilih Logistik"
                    onChange={handleChangeVendor}
                    options={listVendor}
                    name="vendor_id"
                    formik={formik}
                    withValidate
                  />
                </Col>
                <Col xs={6}>
                  <ReactSelect
                    className="mb-3"
                    label="Service (Wajib)"
                    placeholder="Pilih service"
                    onChange={handleChangeServiceVendor}
                    name="vendor_service_id"
                    formik={formik}
                    withValidate
                    options={listServiceVendor}
                  />
                </Col>
              </Row>
              <TextField
                label="AWB (Wajib)"
                placeholder="Input kode AWB"
                withValidate
                formik={formik}
                name="awb_number"
              />
            </Item>
          </>
        ) : (
          <>
            <Item border={false} spaceBottom="3">
              <div
                style={{
                  height: '150px',
                  borderRadius: '4px',
                  border: '2px dashed #CFCFCF',
                  backgroundColor: '#F2F2F2',
                  overflow: 'hidden',
                  position: 'relative',
                }}
                className="d-flex flex-column justify-content-center align-items-center "
              >
                {
                  data?.invoice_file_full_url &&
                    get_url_extension(data?.invoice_file_full_url) === 'pdf'
                    ?
                    <>
                      <PictureAsPdf style={{ fontSize: "50px" }} />
                      <span>{data?.invoice_file}</span>
                      <Button
                        styleType="lightBlueFillOutline"
                        text="Lihat PDF"
                        onClick={() => setState({ ...state, showModalInvoice: true })}
                        style={{ minWidth: '120px', marginTop: '20px' }}
                      />
                    </>
                    :
                    <img
                      src={data?.invoice_file_full_url || ''}
                      alt=""
                      style={{ width: '100%' }}
                    />
                }
              </div>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.label}>No. Invoice</h6>
              <h6 className={classes.value}>{data?.invoice_number || '-'}</h6>
            </Item>
            {/* <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>Pesan</h6>
              <h6 className={classes.text}>{data?.message || '-'}</h6>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>Logistik</h6>
              <h6 className={classes.text}>
                {data.vendor_service?.vendor?.name || '-'}
              </h6>
            </Item>
            <Item border={false} spaceBottom="3">
              <h6 className={classes.text}>AWB</h6>
              <h6 className={classes.text}>{data?.awb_number || '-'}</h6>
            </Item> */}
          </>
        )}
      </Container>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={invoiceImageRef}
        type="file"
        className="d-none"
      />
      <ModalShowInvoice
        show={state.showModalInvoice}
        onHide={() => setState({ ...state, showModalInvoice: false })}
        pdfUrl={data?.invoice_file_full_url}
        pdfName={data?.invoice_file}
      />
    </>
  );
}
FormInvoice.defaultProps = {
  isEdit: true,
  data: {},
  formik: {},
};

export default FormInvoice;
