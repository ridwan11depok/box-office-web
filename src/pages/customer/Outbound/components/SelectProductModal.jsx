import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { SearchField } from '../../../../components/elements/InputField';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Checkbox from '@material-ui/core/Checkbox';
import { useSelector } from 'react-redux';
import Chip from '@material-ui/core/Chip';
import CloseIcon from '@material-ui/icons/Close';

const listProduct = [
  { id: 1, product: 'Baju Kemeja', stock: 10 },
  { id: 2, product: 'Celana Bahan', stock: 10 },
  { id: 3, product: 'Sepatu Adidas', stock: 10 },
  { id: 4, product: 'Topi Boboboy', stock: 10 },
  { id: 5, product: 'Sarung', stock: 10 },
  { id: 6, product: 'Jas', stock: 10 },
  { id: 7, product: 'Peci', stock: 10 },
];
const SelectProductModal = (props) => {
  const [selected, setSelected] = useState([]);
  const onChange = (e, productItem) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setSelected([...selected, productItem]);
      // console.log([...selected, productItem]);
    } else {
      let newSelected = selected.filter((item) => item.id !== productItem.id);
      setSelected(newSelected);
      // console.log(newSelected);
    }
  };
  const handleDelete = (product) => {
    let newSelected = selected.filter((item) => item.id !== product.id);
    setSelected(newSelected);
  };
  const { listAllProductSku } = useSelector((state) => state.inbound);
  const [products, setProducts] = useState(listAllProductSku);
  const handleSearch = (e) => {
    let value = e.target.value;
    let filtered = listAllProductSku.filter((item) =>
      item.product.toLowerCase().includes(value.toLowerCase())
    );
    setProducts(filtered);
  };

  useEffect(() => {
    if (!props.show) {
      setSelected([]);
    } else if (props.show) {
      setSelected(props.initialValues);
    }
  }, [props.show]);

  const handleSelect = () => {
    props.onAgree(selected);
    props.onHide();
  };
  return (
    <>
      <Modal
        titleModal="Pilih Produk"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        scrollable
        content={(data) => (
          <>
            <Container withHeader={false}>
              <Item spaceBottom={3}>
                <div
                  className="p-2"
                  style={{
                    borderBottom: '1px solid #E8E8E8',
                    position: 'sticky',
                    top: '0px',
                    backgroundColor: '#FFFFFF',
                    zIndex: '1050',
                  }}
                >
                  <SearchField
                    onChange={handleSearch}
                    placeholder="Cari produk"
                  />
                </div>
                <div>
                  <table className="table table-striped mt-0 mb-5">
                    <tr
                      className="pt-0 pb-0"
                      style={{
                        borderBottom: '1px solid #E8E8E8',
                      }}
                    >
                      <th scope="col">Produk</th>
                    </tr>
                    <tbody>
                      {products.map((item, idx) => (
                        <tr
                          key={idx.toString()}
                          style={{ borderBottom: '1px solid #E8E8E8' }}
                        >
                          <td>
                            <Checkbox
                              color=""
                              style={{ color: '#192A55' }}
                              checked={
                                selected.filter((prod) => prod.id === item.id)
                                  .length
                              }
                              onChange={(e) => onChange(e, item)}
                            />
                            {item.name}
                          </td>
                          {/* <td>{item.stock}</td> */}
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Item>
            </Container>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="blueFill"
              text="Pilih SKU"
              onClick={handleSelect}
            />
          </div>
        )}
      />
    </>
  );
};

export default SelectProductModal;
