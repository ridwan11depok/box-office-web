import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { setDataFormCreateDispatch } from '../reduxAction';
import ModalShowInvoice from './ModalShowInvoice';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
  link: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#192A55',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
});

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

const validationSchema = Yup.object({
  sender_name: Yup.string().required('Harap untuk mengisi nama pengirim'),
  sender_country_code: Yup.string().required(
    'Harap untuk memilih kode nomor telepon'
  ),
  sender_phone: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon pengirim'),
  receiver_name: Yup.string().required('Harap untuk mengisi nama penerima'),
  receiver_country_code: Yup.string().required(
    'Harap untuk memilih kode nomor telepon'
  ),
  receiver_phone: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon penerima'),
  receiver_address: Yup.string().required(
    'Harap untuk mengisi alamat penerima'
  ),
  receiver_province_id: Yup.string().required('Harap untuk memilih provinsi'),
  receiver_city_id: Yup.string().required('Harap untuk memilih kabupaten/kota'),
  receiver_district_id: Yup.string().required('Harap untuk memilih kecamatan'),
});

function FormSenderAndReceiverDispatchOrder({ handleChangeTab }) {
  const [state, setState] = useState({
    showModalInvoice: false,
    provice_id: 0,
    city_id: 0,
    district_id: 0,
    invoiceImage: '',
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const { formDataCreateDispatch } = useSelector(
    (state) => state.outboundCustomer
  );

  const formik = useFormik({
    initialValues: {
      sender_name: formDataCreateDispatch?.sender_name || '',
      sender_country_code: formDataCreateDispatch?.sender_country_code || '62',
      sender_phone: formDataCreateDispatch?.sender_phone || '',
      receiver_name: formDataCreateDispatch?.receiver_name || '',
      receiver_country_code: formDataCreateDispatch?.receiver_country_code || '62',
      receiver_phone: formDataCreateDispatch?.receiver_phone || '',
      receiver_address: formDataCreateDispatch?.receiver_address || '',
      receiver_province_id: formDataCreateDispatch?.receiver_province_id || '',
      receiver_city_id: formDataCreateDispatch?.receiver_city_id || '',
      receiver_district_id: formDataCreateDispatch?.receiver_district_id || '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // handleSubmit(values);
      dispatch(setDataFormCreateDispatch(values));
      handleChangeTab(3);
    },
  });

  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);

  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(state.provice_id, Number(e.target.value)));
  };
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  };
  const handleValueArea = (area_id = null, listArea = []) => {
    if (area_id) {
      const find = listArea.find((e) => (e.value === area_id))
      return find
    }
  }
  return (
    <Container>
      <Item className="p-3" spaceRight="0" col="col-12">
        <Row className="p-1 pt-2 pb-2">
          <Col>
            <h6 className={classes.label}>Logistik</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.vendor_name || '-'}
            </h6>
          </Col>
          <Col>
            <h6 className={classes.label}>Service</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.vendor_service || '-'}
            </h6>
          </Col>
          <Col>
            <h6 className={classes.label}>AWB</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.awb_number || '-'}
            </h6>
          </Col>
          <Col>
            <h6 className={classes.label}>Platform</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.platform || '-'}
            </h6>
          </Col>
        </Row>
      </Item>
      <Item className="p-3" spaceRight="0" col="col-12" spaceBottom={3}>
        <Row className="p-1 pt-2 pb-2">
          <Col>
            <h6 className={classes.label}>Foto Invoice</h6>
            <h6
              className={classes.link}
              onClick={() => setState({ ...state, showModalInvoice: true })}
            >
              Lihat Foto
            </h6>
          </Col>
          <Col>
            <h6 className={classes.label}>Nomor Invoice</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.invoice_number || '-'}
            </h6>
          </Col>
          <Col>
            <h6 className={classes.label}>Pesan</h6>
            <h6 className={classes.value}>
              {formDataCreateDispatch?.message || '-'}
            </h6>
          </Col>
        </Row>
      </Item>
      <Item
        className="p-3"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '500px' }}
      >
        <h6>Data Pengirim</h6>
        <TextField
          className="mb-3"
          label="Nama Pengirim"
          placeholder="Input nama pengirim"
          withValidate
          formik={formik}
          name="sender_name"
        />
        <Row className="mb-3">
          <Col xs={12} lg={6}>
            <SelectField
              label="Nomor Telepon Pengirim"
              options={[
                {
                  value: '62',
                  description: 'Indonesia(+62)',
                },
              ]}
              withValidate
              formik={formik}
              name="sender_country_code"
              withError={false}
            />
          </Col>
          <Col xs={12} lg={6} className="d-flex align-items-end">
            <TextField
              className="w-100"
              placeholder="Input nomor telepon"
              withValidate
              formik={formik}
              name="sender_phone"
              withError={false}
            />
          </Col>
          <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
            {formik.errors.sender_phone && formik.touched.sender_phone && (
              <Form.Text className="text-input-error">
                {formik.errors.sender_phone}
              </Form.Text>
            )}
          </Col>
        </Row>
      </Item>
      <Item
        className="p-3"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '500px' }}
      >
        <h6>Data Penerima</h6>
        <TextField
          className="mb-3"
          label="Nama Penerima"
          placeholder="Input nama penerima"
          withValidate
          formik={formik}
          name="receiver_name"
        />
        <Row className="mb-3">
          <Col xs={12} lg={6}>
            <SelectField
              label="Nomor Telepon Penerima"
              options={[
                {
                  value: '62',
                  description: 'Indonesia(+62)',
                },
              ]}
              withValidate
              formik={formik}
              name="receiver_country_code"
              withError={false}
            />
          </Col>
          <Col xs={12} lg={6} className="d-flex align-items-end">
            <TextField
              className="w-100"
              placeholder="Input nomor telepon"
              withValidate
              formik={formik}
              name="receiver_phone"
              withError={false}
            />
          </Col>
          <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
            {formik.errors.receiver_phone && formik.touched.receiver_phone && (
              <Form.Text className="text-input-error">
                {formik.errors.receiver_phone}
              </Form.Text>
            )}
          </Col>
        </Row>
        <TextArea
          className="mb-3"
          label="Alamat"
          placeholder="Input alamat penerima"
          name="receiver_address"
          formik={formik}
          withValidate
        />
        <Row>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
              value={handleValueArea(formDataCreateDispatch?.receiver_province_id || null, listProvince)}
              className="mb-3"
              label="Provinsi"
              placeholder="Pilih provinsi"
              onChange={handleChangeProvince}
              options={listProvince}
              name="receiver_province_id"
              formik={formik}
              withValidate
            />
          </Col>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
              value={handleValueArea(formDataCreateDispatch?.receiver_city_id || null, listCity)}
              className="mb-3"
              label="Kabupaten/Kota"
              placeholder="Pilih kabuten/kota"
              onChange={handleChangeCity}
              name="receiver_city_id"
              formik={formik}
              withValidate
              options={listCity}
            />
          </Col>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
            value={handleValueArea(formDataCreateDispatch?.receiver_district_id || null, listDistrict)}
              className="mb-3"
              label="Kecamatan"
              placeholder="Pilih kecamatan"
              onChange={handleChangeDistrict}
              name="receiver_district_id"
              formik={formik}
              withValidate
              options={listDistrict}
            />
          </Col>
        </Row>
      </Item>
      <div className="ml-auto">
        <Button
          style={{ width: '140px' }}
          styleType="lightBlueFill"
          text="Kembali"
          onClick={() => handleChangeTab(1)}
        />
        <Button
          style={{ minWidth: '140px', marginLeft: '10px' }}
          styleType="blueFill"
          text="Lanjutkan"
          onClick={formik.handleSubmit}
        />
      </div>
      <ModalShowInvoice
        show={state.showModalInvoice}
        onHide={() => setState({ ...state, showModalInvoice: false })}
      />
    </Container>
  );
}

export default FormSenderAndReceiverDispatchOrder;
