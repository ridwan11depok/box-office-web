import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col } from 'react-bootstrap';
import { formatRupiah } from '../../../../utils/text';
import Avatar from '@material-ui/core/Avatar';
import { useSelector } from 'react-redux';
import TimeLine from '../../../../components/elements/Timeline/TimeLine';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Button from '../../../../components/elements/Button';

const useStyles = makeStyles({
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
  courierName: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#363636',
  },
  price: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#4f4f4f',
    textAlign: 'right',
  },
});

function DetailCourierRegTrans() {
  const classes = useStyles();
  const { detailRegularTransaction } = useSelector(
    (state) => state.outboundCustomer
  );
  return (
    <div>
      <h6>Logistik</h6>
      <Row>
        <Col xs={8} className="d-flex align-items-center">
          <div className="mb-3 mr-3">
            <img
              style={{ width: '55px', height: 'auto' }}
              alt="Courier Logo"
              src={
                detailRegularTransaction?.vendor_service?.vendor
                  ?.logo_full_url || '-'
              }
            />
          </div>
          <div className="mb-3">
            <h6 className={classes.courierName}>
              {detailRegularTransaction?.vendor_service?.vendor?.name || '-'}-
              {detailRegularTransaction?.vendor_service?.service || '-'}
            </h6>
            <h6
              className={classes.courierName}
              style={{ fontSize: '12px', fontWeight: '400' }}
            >
              1-2 hari
            </h6>
          </div>
        </Col>
        <Col xs={4}>
          <div className="mb-3">
            <h6 className={classes.price}>
              {formatRupiah(detailRegularTransaction?.vendor_price || 0)}
            </h6>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <TimeLine />
        </Col>
        <Col xs={12} className="d-flex justify-content-center">
          <Button
            style={{ fontSize: '14px' }}
            styleType="blueNoFill"
            text="Selengkapnya"
            endIcon={() => <KeyboardArrowDownIcon />}
          />
        </Col>
      </Row>
    </div>
  );
}

export default DetailCourierRegTrans;
