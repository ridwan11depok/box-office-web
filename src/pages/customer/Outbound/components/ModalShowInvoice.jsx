import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import { get_url_extension } from '../../../../utils/text';

const dummyAWB =
  'https://s3-alpha-sig.figma.com/img/8630/010f/50eda9982250b3daeebe204a9f787838?Expires=1638144000&Signature=PF-ogSPSyrjPL01GwVtfWGDrWL~oXIjDBNDVtekBQSIComWutapRTQl-Kv-QUCNGJvDBW8MYn3Kmy7zQo2LDZssukkDoVNwlKhxeLYdlvBmKnC60mTorPgKU969YojmXgR0QnLqEx9qZs7VEVpMRKrkf9WEiGS5Pn6P2cB8a161YPp01sHhlx4YLHSMk-n8DmLj~7syEWY6COKySjnYaBe6RtDh7k46T0qkzlzqIcXyTxnVTXmNmNzaGc4c7j72AXGfMeqlrALP9pcxT7ZUO91uBdzwToyzOtkz1I-qsB0KpqaazJ8252m6hvojEGh2Ahudoyk8DstJN7MZg6qvzqA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA';
const ModalShowInvoice = (props) => {

  const { formDataCreateDispatch } = useSelector(
    (state) => state.outboundCustomer
  );

  console.log();

  return (
    <>
      <Modal
        titleModal="Invoice"
        bodyClassName="p-1"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <div className="text-left">
            {props?.pdfUrl ?
              get_url_extension(props?.pdfUrl) === 'pdf' &&
              <iframe src={props?.pdfUrl} title={props?.pdfName} width="100%" height="400px" />
              :
              formDataCreateDispatch?.invoice_file?.type === 'application/pdf' ?
              <iframe src={URL.createObjectURL(formDataCreateDispatch?.invoice_file)} title={formDataCreateDispatch?.invoice_file?.name} width="100%" height="400px" />
                :
                <img
                  alt="Invoice"
                  src={
                    formDataCreateDispatch?.invoice_file
                      ? URL.createObjectURL(formDataCreateDispatch?.invoice_file)
                      : ''
                  }
                  className="w-100"
                />
            }
          </div>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              text="Kembali"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

ModalShowInvoice.defaultProps = {
  src: null,
};
export default ModalShowInvoice;
