import React, { useEffect, useState } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

function FormDataPenerima(props) {
  const classes = useStyles();
  const { isEdit, data, formik } = props;
  const dispatch = useDispatch();
  const { listProvince, listCity } = useSelector(
    (state) => state.gudangAdministratorReducer
  );

  const [state, setState] = useState({
    provice_id: 0,
    city_id: 0,
  });

  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);

  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
  };
  return (
    <div>
      <h6>Data Penerima</h6>
      {isEdit ? (
        <>
          {/* <Item border={false}> */}
          <TextField
            label="Nama Penerima"
            placeholder="Input nama penerima"
            withValidate
            formik={formik}
            name="receiver_name"
          />
          <Row>
            <Col>
              <SelectField
                label="Nomor Telepon Penerima"
                options={[
                  {
                    value: '62',
                    description: 'Indonesia(+62)',
                  },
                ]}
                withValidate
                formik={formik}
                name="receiver_country_code"
                withError={false}
              />
            </Col>
            <Col className="d-flex align-items-end">
              <TextField
                className="w-100"
                placeholder="Input nomor telepon"
                withValidate
                formik={formik}
                name="receiver_phone"
                withError={false}
              />
            </Col>
            {formik.touched.receiver_phone && formik.errors.receiver_phone && (
              <Col xs={12}>
                <Form.Text className="text-input-error mb-3 mt-0">
                  {formik.errors.receiver_phone}
                </Form.Text>
              </Col>
            )}
          </Row>
          <TextArea
            className="mb-3"
            label="Alamat"
            placeholder="Input alamat penerima"
            rows={3}
            withValidate
            formik={formik}
            name="receiver_address"
          />
          <Row>
            <Col xs={6}>
              <ReactSelect
                className="mb-3"
                label="Provinsi"
                placeholder="Pilih provinsi"
                onChange={handleChangeProvince}
                options={listProvince}
                name="receiver_province_id"
                formik={formik}
                withValidate
              />
            </Col>
            <Col xs={6}>
              <ReactSelect
                className="mb-3"
                label="Kota"
                placeholder="Pilih kota"
                onChange={handleChangeCity}
                name="receiver_city_id"
                formik={formik}
                withValidate
                options={listCity}
              />
            </Col>
          </Row>
          {/* </Item> */}
        </>
      ) : (
        <>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Nama Penerima</h6>
            <h6 className={classes.value}>{data?.receiver_name || '-'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Nomor Telepon</h6>
            <h6 className={classes.value}>{data?.receiver_phone || '-'}</h6>
          </Item>
          <Item border={false} spaceBottom={3}>
            <h6 className={classes.label}>Alamat</h6>
            <h6 className={classes.value}>{data?.receiver_address || '-'}</h6>
          </Item>
          <Row>
            <Col xs={6}>
              <h6 className={classes.label}>Kabupaten/Kota</h6>
              <h6 className={classes.value}>
                {data?.receiver_city?.name || '-'}
              </h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Provinsi</h6>
              <h6 className={classes.value}>
                {data?.receiver_province?.name || '-'}
              </h6>
            </Col>
          </Row>
        </>
      )}
    </div>
  );
}

FormDataPenerima.defaultProps = {
  isEdit: true,
  data: {},
  formik: {},
};

export default FormDataPenerima;
