import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import dispatchOrder from '../../../../assets/images/dispatchorder.svg';
import payung from '../../../../assets/images/payung.svg';
import { useHistory } from 'react-router-dom';

const MakeTransactionModal = (props) => {
  const history = useHistory();
  return (
    <>
      <Modal
        titleModal="Buat Transaksi Baru"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <div className="d-flex flex-row justify-content-between">
            <div
              style={{ flex: 0.33 }}
              className="d-flex flex-column align-items-center"
            >
              <img className="mb-3" src={dispatchOrder} alt="" />
              <Button
                styleType="blueFill"
                text="Dispacth Order"
                onClick={() => {
                  props.onHide();
                  history.push('/transactions/dispatch-order');
                }}
                className="w-100"
              />
            </div>
            <div
              style={{ flex: 0.33 }}
              className="d-flex flex-column align-items-center"
            >
              <img className="mb-3" src={payung} alt="" />
              <Button
                styleType="blueFill"
                text="Dropship"
                className="w-100"
                onClick={() => {
                  props.onHide();
                  history.push('/transactions/dropship');
                }}
              />
            </div>
            <div
              style={{ flex: 0.33 }}
              className="d-flex flex-column align-items-center"
            >
              <img className="mb-3" src={payung} alt="" />
              <Button
                styleType="blueFill"
                text="Regular"
                onClick={props.onHide}
                className="w-100"
                onClick={() => {
                  props.onHide();
                  history.push('/transactions/transaksi-regular');
                }}
              />
            </div>
          </div>
        )}
        showFooter={false}
      />
    </>
  );
};

export default MakeTransactionModal;
