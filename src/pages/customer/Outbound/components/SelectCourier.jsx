import React, { useState, useEffect } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import { TabHeader } from '../../../../components/elements/Tabs/';
import { kurirOptions } from './dataDummy';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { getListCourier } from '../reduxAction';
import { formatRupiah } from '../../../../utils/text';
import { ContentItem } from '../../../../components/elements/BaseContent';

const useStyles = makeStyles({
  containerTabHeader: {
    border: '1px solid #E8E8E8',
    borderRadius: '4px',
    width: '100%',
  },
  courierName: {
    fontFamily: 'Open Sans',
    fontWeight: '600',
    fontSize: '14px',
    color: '#1c1c1c',
  },
  courierDuration: {
    fontFamily: 'Open Sans',
    fontWeight: '600',
    fontSize: '12px',
    color: '#4f4f4f',
  },
  courierDuration: {
    fontFamily: 'Open Sans',
    fontWeight: '700',
    fontSize: '14px',
    color: '#192A55',
  },
  ansurance: {
    borderRight: '1px solid #E8E8E8',
    height: '56px',
    fontFamily: 'Open Sans',
    fontWeight: '700',
    fontSize: '14px',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
  price: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#4f4f4f',
    textAlign: 'right',
  },
});

function PilihKurir({
  formikFormDetailPaket,
  formikFormReceiver,
  formikFormSender,
  formikFormCourier,
}) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    tabKurir: 1,
    selectedKurir: null,
    price: 0,
  });
  const { listCourier } = useSelector((state) => state.outboundCustomer);

  useEffect(() => {
    const params = {
      sender_province_id:
        formikFormSender.getFieldProps('sender_province_id')?.value,
      sender_city_id: formikFormSender.getFieldProps('sender_city_id')?.value,
      sender_district_id:
        formikFormSender.getFieldProps('sender_district_id')?.value,
      receiver_province_id: formikFormReceiver.getFieldProps(
        'receiver_province_id'
      )?.value,
      receiver_city_id:
        formikFormReceiver.getFieldProps('receiver_city_id')?.value,
      receiver_district_id: formikFormReceiver.getFieldProps(
        'receiver_district_id'
      )?.value,
      weight: formikFormDetailPaket.getFieldProps('weight')?.value,
    };
    dispatch(getListCourier(params));
  }, []);

  const handleSelectCourier = (itemCourier) => {
    setState({
      ...state,
      selectedKurir: itemCourier?.service,
      price: itemCourier?.data?.price,
    });
    formikFormCourier.setFieldValue(
      'vendor_id',
      itemCourier?.service?.vendor_id
    );
    formikFormCourier.setFieldValue(
      'vendor_service_id',
      itemCourier?.service?.id
    );
  };
  return (
    <>
      <ContentItem spaceBottom={3}>
        <Row
          className="no-gutters w-100"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <Col className="no-gutters d-flex align-items-center p-3 col-12 col-md-6">
            <h6>Pilih Kurir Logistik</h6>
          </Col>
          <Col className="p-2 pl-0 col-12 col-md-6">
            <div className={classes.containerTabHeader}>
              <TabHeader
                listHeader={[
                  {
                    title: 'Regular',
                    tab: 1,
                  },
                  {
                    title: 'Express',
                    tab: 2,
                  },
                  {
                    title: 'Trucking',
                    tab: 3,
                  },
                ]}
                activeTab={state.tabKurir}
                border={false}
                onChange={(tab) => setState({ ...state, tabKurir: tab })}
              />
            </div>
          </Col>
        </Row>
        <Row className="no-gutters p-3">
          {listCourier?.[0]?.map((item, idx) => (
            <Col
              className={`col-12 col-md-6 ${
                idx % 2 === 0 ? 'pr-0 pr-md-2' : 'pl-0 pl-md-2'
              } mb-2`}
            >
              <div
                onClick={() => handleSelectCourier(item)}
                style={{
                  border:
                    state.selectedKurir?.id === item?.service?.id
                      ? '1px solid #192A55'
                      : '1px solid #E8E8E8',
                  backgroundColor:
                    state.selectedKurir?.id === item?.service?.id && '#EBF0FA',
                  cursor: 'pointer',
                }}
                className="w-100 d-flex flex-row rounded p-2"
              >
                <div style={{ flex: 0.3 }}>
                  <img
                    style={{ height: '40px' }}
                    src={item?.service?.vendor?.logo_full_url}
                    alt={item?.service?.vendor?.name || ''}
                  />
                </div>
                <div style={{ flex: 0.4 }}>
                  <h6 className={classes.courierName}>
                    {item?.service?.vendor?.name || '-'}
                  </h6>
                  <h6 className={classes.courierDuration}>
                    {item?.data?.estimation || '-'}
                  </h6>
                </div>
                <div
                  style={{ flex: 0.3 }}
                  className="d-flex justify-content-end align-items-center"
                >
                  <h6 className={classes.courierPrice}>
                    {formatRupiah(item?.data?.price || 0)}
                  </h6>
                </div>
              </div>
            </Col>
          ))}
        </Row>
        <Row className="no-gutters" style={{ borderTop: '1px solid #E8E8E8' }}>
          <Col
            className={clsx(
              'no-gutters p-3 d-flex align-items-center justify-content-between border-right',
              classes.insurance
            )}
          >
            <Form.Group className="mb-0 w-50" controlId="formBasicCheckbox">
              <Form.Check
                type="checkbox"
                label="Tambahkan asuransi"
                style={{ fontWeight: '400' }}
                className="mb-0 mt-0"
              />
            </Form.Group>
            <h6>Rp 0</h6>
          </Col>
          <Col
            className="no-gutters p-3 d-flex align-items-center justify-content-between"
            style={{
              height: '56px',
              fontFamily: 'Open Sans',
              fontWeight: '700',
              fontSize: '14px',
              color: '#1c1c1c',
            }}
          >
            <h6>Total</h6>
            <h6>Rp 0</h6>
          </Col>
        </Row>
      </ContentItem>
      <ContentItem spaceBottom={3} className="p-3">
        <h6>Total Pembayaran</h6>

        <Row>
          <Col xs={8}>
            <div className="mb-3">
              <h6 className={classes.label}>Total Pembayaran Logistik</h6>
            </div>
          </Col>
          <Col xs={4}>
            <div className="mb-3">
              <h6 className={classes.price}>{formatRupiah(state.price)}</h6>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={8}>
            <div className="mb-3">
              <h6 className={classes.label}>Biaya Pickup</h6>
            </div>
          </Col>
          <Col xs={4}>
            <div className="mb-3">
              <h6 className={classes.price}>{formatRupiah(0)}</h6>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={8}>
            <div className="mb-3">
              <h6
                className={classes.label}
                style={{ color: '#1C1C1C', fontWeight: '700' }}
              >
                Total Pembayaran
              </h6>
            </div>
          </Col>
          <Col xs={4}>
            <div className="mb-3">
              <h6 className={classes.price} style={{ color: '#1C1C1C' }}>
                {formatRupiah(state.price)}
              </h6>
            </div>
          </Col>
        </Row>
      </ContentItem>
    </>
  );
}

export default PilihKurir;
