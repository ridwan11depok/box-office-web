import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { formatRupiah } from '../../../../utils/text';

const useStyles = makeStyles({
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
});

function DetailPaketRegTrans() {
  const classes = useStyles();
  const { detailRegularTransaction } = useSelector(
    (state) => state.outboundCustomer
  );
  return (
    <div>
      <h6>Detail Paket</h6>
      <Row>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Kategori Produk</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.category_product?.name || '-'}
            </h6>
          </div>
        </Col>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Sub Kategori Produk</h6>
            <h6 className={classes.value}>
              {' '}
              {detailRegularTransaction?.sub_category_product?.name || '-'}
            </h6>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Panjang</h6>
            <h6 className={classes.value}>
              {' '}
              {detailRegularTransaction?.length + ' cm' || '-'}
            </h6>
          </div>
        </Col>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Lebar</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.width + ' cm' || '-'}
            </h6>
          </div>
        </Col>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Tinggi</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.height
                ? `${detailRegularTransaction?.height} cm`
                : '-'}
            </h6>
          </div>
        </Col>
        <Col>
          <div className="mb-3">
            <h6 className={classes.label}>Berat</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.weight
                ? `${detailRegularTransaction?.weight} kg`
                : '-'}
            </h6>
          </div>
        </Col>
      </Row>
      <div className="mb-3">
        <h6 className={classes.label}>Nilai Barang</h6>
        <h6 className={classes.value}>
          {formatRupiah(detailRegularTransaction?.item_price || 0)}
        </h6>
      </div>
    </div>
  );
}

export default DetailPaketRegTrans;
