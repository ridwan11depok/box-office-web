export const dummy = {
  pending: [
    {
      id: 1,
      date: '12 Desember 2020',
      noinvoice: '123431',
      receiver: 'Nama Penerima',
      kurir: 'Nama Kurir',
      status: 'pending',
    },
  ],
  done: [
    {
      id: 1,
      date: '12 Desember 2020',
      noinvoice: '123431',
      receiver: 'Nama Penerima',
      kurir: 'Nama Kurir',
      status: 'done',
    },
    {
      id: 2,
      date: '12 Desember 2020',
      noinvoice: '121212',
      receiver: 'Nama Penerima',
      kurir: 'Nama Kurir',
      status: 'done',
    },
  ],
};

export const kurirOptions = [
  {
    image:
      'https://s3-alpha-sig.figma.com/img/d9e9/21a3/cec5a7672609d1f9921db74266bec9ae?Expires=1629676800&Signature=IuujKwNTmsV4LYsN1Bxp1WDUzjq4BcTurLJ2iOHxyvF1i0n9qrg6pPyENFWEhNpkkiH0vXHENXnEuBv02yshVm0HqrTlgSNojfF6OikFB4jOJyLMBR9tnuRqaN6JpQQ1FFe~kxYmp5ilHw5OAsYoRzg1Ph1E5BXgK9oN7S5XwjXJ1k3WUhhiGbfk6-gI28Cd4uGxSZKJkGCL1N~l5CT9ZVGyldPNKmcYasXQ3VUMAsASdgqcpRwTQJheGbXnVQ6LE-sCZDF96-~-A~oeD3JdfZZ5m7dzHdJGTBMh7tdHIsGhLB0EUyHX2xQUhhdkqcGwgACrwld556QDalFlNw4Y9A__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'Wahana-Regular',
    duration: '2 hari',
    price: 'Rp 5.000',
  },
  {
    image:
      'https://s3-alpha-sig.figma.com/img/79d9/d449/a99146904958368f4da26cb51cdf0071?Expires=1629676800&Signature=U0B-daSLH5E1XJedQCtmVgcG-Fq3gOqhI-wPCWdlJk0nGDQUf-PqGFK8KK3IecBX2t3OC4PK4G69ncCs2L6Hp9ZGIAScE~3Y2KpBHR7mFOoDga0qoi6Or7MERyMgJREELZ3B2s0eO1~nJsptcoQyU-5gXWXbuj04i6YhNPH96v5Z989Ev3reHFdkiVUG7XIjauA4DQFSvwwpTIeJEX36GbkUkqeoV8mDAV8v4QQyOnZ3GfkWyaVGE2PJjy6wkD8JpFAoWlVBTmZ0DwBYJeu9AknaKxmMvgch3w7tbDPUU1f8lecI~ysI-TuXxL7Pv6aCAVywwEghw3RiZ79MzK86Sw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'Tiki reg',
    duration: '2 hari',
    price: 'Rp 7.000',
  },
  {
    image:
      'https://s3-alpha-sig.figma.com/img/d1bb/c237/61dd1c3e9ab89f07c9d686c1e697af38?Expires=1629676800&Signature=Vl2UKhakwTRmwHe9GIljqXvs1IyO~zBLT-dDZMYDks-LqKn2QgUt0kX~ezK1rPkkiDwzuEIDpBljmsadZg7BceWpL0l0~JVajU8zWEOii6BpjlRTScp5729wZ9xygKc7J1tmzHQZULZTEM-OLDKqNN4TFOOVZ9nMCjI9I6kTe-cyr1ZL0FeVeZQaV0njtjNJ3cAjYQGHfdRv7ztXZc0Mpq9WYQ9aTkaqTuRJoKzrBL4VM-~6jPITkZpb4mt1Um5O984rMdVefvY8xcoXznHICd-kj~1a~abCQoBUYHCT1qbwP2sv44XX15fFxT-f6uAXApOeTblpgoqa9qo~C2tBxg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'JNE-CTE',
    duration: '1-2 hari',
    price: 'Rp 5.000',
  },
  {
    image:
      'https://s3-alpha-sig.figma.com/img/8d2a/cd7c/0346dd66a96b436c26e3bd5953a5f93b?Expires=1629676800&Signature=KpCqWCphSQRNcntgxClCCzODMy498Z~kQcTIOlPwq8GtLWFwvrOT-egKfO7HtuDco6ucDnyVs8bXc50Ky7iDO4arWRvdcXkYnY-177QEM1I0ldOFr-tS406tfOz3No1e4frqa5mXvbYrnOsNXxl5RhBKOdG~Kg5Fly9Vnvea1fK1434a4j6CW~uaKcWe~lsjmURbiO0i0fP7C9-LH9LXq1pQt0BdEZ7c5WKokQypPwml~xhpUqnU33sZWoFU~MT9j1ZajmfUpKUfcMmYrF3Kb13hTF5oWMHybsrWn8B3sPktimGQaE4U6UMR4F1i9mFL270nCJ~EHmM0fNAm24uEow__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'Lion Parcel-REGPACK',
    duration: '3-5 hari',
    price: 'Rp 8.500',
  },
  {
    image:
      'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1629676800&Signature=DksYwq5doAOxwqZngxlfaT06YEdn--gyakeciL7moMSp-ZhWjXQCFYoyqUZgHAjh5tVHdb~HeaP42NbBrMpJokfvfwdMwF05ESWhszkIbdwlcvkqyTSpqC6Deeceu8Wen5zR3eKbAU8Njwfui0XvEftmseKyfeeK68FjzBTK8tzFM75aE7Cn3A8BQbLMdSQNPe5HtM1zMU0CKCKjeN3~Y0iSU4dmjCGRK~Rxi-oqX3c1hPo7gFHwIpBSl0R~0DMGNWrfcoyo6HTt0sScmoFooM7ajQQ94uKCjO3NpVmphtbquiykGKfLz9w-fjxM8OkBZ6mCR32YUSgsUd8gXFNB3A__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'Si Cepat-REG',
    duration: '1-2 hari',
    price: 'Rp 9.000',
  },
  {
    image:
      'https://s3-alpha-sig.figma.com/img/992c/0615/d05cc9ab96ae64087683b22181d655ab?Expires=1629676800&Signature=PgkfOXsEe0k7vw8~w2sZAxOnviy6MTL-Bt-cwrKOIhouDJcr03RgHLR~nJzqFOQFz3M423~jhZw9LjqJ63Nrm~Rzt9jMSW~GssbjPj9-ssp7UE1W60mt-tgVvxqRAcSk8P9ZlYAjW~F9mwwMa5mGG8GmR1bZ-3zp45N~m~0pCFJzKFrCnUvA50CensEWMi~naZeOj0nYJ5UkeeTsC-rT8E9udfTInYD6i8Alb-R0bQ~vBEvyvVKKb0fVKtgjkrfK5BbZYXwh~uN~bsph5HQVlgLtbwcQ84TkkJOk~vrTl6EsC6JeDBdHECRem5BmL8HCCzS~Nt3t1hPZ-LB8HFMEnQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
    name: 'J&T-EXPRESS',
    duration: '1-3 hari',
    price: 'Rp 10.000',
  },
];
