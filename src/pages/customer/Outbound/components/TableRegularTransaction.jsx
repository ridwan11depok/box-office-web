import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import GetAppIcon from '@material-ui/icons/GetApp';
import {
  getRegularTransaction,
  exportLogsRegularTransaction,
} from '../reduxAction';
import AddIcon from '@material-ui/icons/Add';
import Badge from '../../../../components/elements/Badge';
import { DatePicker } from '../../../../components/elements/InputField';
import Toast from '../../../../components/elements/Toast';
import moment from 'moment';
import { statusRegularTransaction } from '../../../../utils/text';
import { useDownloadFile } from '../../../../utils/hook';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data }) => {
  const history = useHistory();
  return (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => {
        history.push(`/outbound/regular-transaction/detail/${data?.id}`);
      }}
    />
  );
};

const EmptyTransaction = ({ onClick }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada transaksi yang dilakukan saat ini.
      </h6>
      <Button
        startIcon={() => <AddIcon />}
        onClick={onClick}
        text="Buat Transaksi Regular"
        styleType="blueFill"
      />
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleAddRegularTransaction, handleFilterDate, handleExportLogs } =
    props;
  const getFileName = () => {
    return `Regular-Transaction.xlsx`;
  };
  const { ref, url, download, name } = useDownloadFile({
    apiDefinition: handleExportLogs,
    // preDownloading,
    // postDownloading,
    // onError: onErrorDownloadFile,
    getFileName,
  });
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div className="mt-1 mb-1">
        <DatePicker
          format="MMMM YYYY"
          views={['year', 'month']}
          noMarginBottom
          onDateChange={handleFilterDate}
          placeholder="Pilih bulan"
        />
      </div>
      <div className="d-flex flex-row flex-wrap mt-1 mb-1">
        <a href={url} download={name} className="hidden" ref={ref} />
        <Button
          startIcon={() => <GetAppIcon />}
          styleType="blueOutline"
          text="Export Logs"
          className="mt-1 mr-1 mb-1"
          onClick={download}
        />
        <Button
          className="mt-1 mr-1 mb-1"
          startIcon={() => <AddIcon />}
          styleType="blueFill"
          text="Buat Transaksi Regular"
          onClick={handleAddRegularTransaction}
        />
      </div>
    </div>
  );
};

const StatusField = ({ status }) => {
  return (
    <Badge
      styleType={
        status === 6
          ? 'green'
          : status === 7 || status === 8
          ? 'red'
          : status == 0
          ? 'black'
          : 'yellow'
      }
      label={statusRegularTransaction(status)}
      size="md"
      style={{ fontSize: '14px' }}
    />
  );
};

function TransactionsTable(props) {
  const { tab, storageId } = props;
  const dispatch = useDispatch();
  const history = useHistory();
  const { regularTransaction, selectedStorage } = useSelector(
    (state) => state.outboundCustomer
  );
  const [state, setState] = useState({
    date: null,
  });
  const handleAddRegularTransaction = () => {
    if (!selectedStorage) {
      Toast(
        { type: 'warning', message: 'Harap memilih gudang terlebih dahulu' },
        { autoClose: 5000, position: 'top-right' }
      );
    } else {
      history.push('/outbound/regular-transaction/add');
    }
  };

  const handleFetchRegularTransaction = (params) => {
    dispatch(getRegularTransaction(params));
  };

  const handleFilterDate = (date) => {
    if (date) {
      setState({ ...state, date: moment(date).format('YYYY-MM') });
    } else {
      setState({ ...state, date: null });
    }
  };

  const handleExportLogs = () => {
    const params = {
      date: state.date,
      status: tab,
      storage_id: selectedStorage?.id,
    };
    return dispatch(exportLogsRegularTransaction(params));
  };

  return (
    <div>
      <DataTable
        action={handleFetchRegularTransaction}
        params={{ status: tab, storage_id: storageId, date: state.date }}
        column={[
          {
            heading: 'Tanggal ',
            key: 'date',
          },
          {
            heading: 'Nomor Transaksi',
            key: 'transaction_number',
          },
          {
            heading: 'Nama Penerima',
            key: 'receiver_name',
          },
          {
            heading: 'Nama Pengirim',
            key: 'sender_name',
          },
          {
            heading: 'Kurir',
            key: 'courier',
          },
          {
            heading: 'Status',
            render: (data) => <StatusField status={data?.status || 0} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={regularTransaction?.data || []}
        transformData={(item) => ({
          ...item,
          date: moment(item.created_at).format('DD MMMM YYYY HH:mm:ss'),
          courier: item?.vendor_service?.vendor.name || '-',
        })}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddRegularTransaction={handleAddRegularTransaction}
            handleFilterDate={handleFilterDate}
            handleExportLogs={handleExportLogs}
          />
        )}
        renderEmptyData={() => (
          <EmptyTransaction onClick={handleAddRegularTransaction} />
        )}
        totalPage={regularTransaction?.last_page || 1}
      />
    </div>
  );
}

export default TransactionsTable;
