import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { ContentItem as Item } from '../../../../components/elements/BaseContent';
import MapModal from '../../../../components/elements/Map/MapModal';
import { Row, Col } from 'react-bootstrap';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
});

function DetailReceiverRegTrans() {
  const classes = useStyles();
  const [state, setState] = useState({ showMap: false, initialCoordinate: {} });
  const { detailRegularTransaction } = useSelector(
    (state) => state.outboundCustomer
  );
  return (
    <div>
      <h6>Data Penerima</h6>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Nama Pelanggan</h6>
        <h6 className={classes.value}>
          {detailRegularTransaction?.receiver_name || '-'}
        </h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Nomor Telepon</h6>
        <h6 className={classes.value}>
          {detailRegularTransaction?.receiver_phone_number || '-'}
        </h6>
      </Item>{' '}
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Alamat</h6>
        <h6 className={classes.value}>
          {detailRegularTransaction?.receiver_address || '-'}
        </h6>
      </Item>
      <Button
        styleType="blueNoFill"
        className="p-0"
        text="Lihat Lokasi di Peta"
        startIcon={() => <MapOutlinedIcon />}
        onClick={() => setState({ ...state, showMap: true })}
      />
      <Row>
        <Col xs={6}>
          <div className="mt-3">
            <h6 className={classes.label}>Kota</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.receiver_city?.name || '-'}
            </h6>
          </div>
        </Col>
        <Col xs={6}>
          <div className="mt-3">
            <h6 className={classes.label}>Provinsi</h6>
            <h6 className={classes.value}>
              {detailRegularTransaction?.receiver_province?.name || '-'}
            </h6>
          </div>
        </Col>
      </Row>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Pesan</h6>
        <h6 className={classes.value}>
          {detailRegularTransaction?.receiver_note || '-'}
        </h6>
      </Item>
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        initialCoordinate={{
          lat: detailRegularTransaction?.receiver_latitude,
          lng: detailRegularTransaction?.receiver_longitude,
        }}
        mode="viewer"
      />
    </div>
  );
}

export default DetailReceiverRegTrans;
