import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { SearchField } from '../../../../components/elements/InputField';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Checkbox from '@material-ui/core/Checkbox';
import { useSelector } from 'react-redux';
import Banner from '../../../../components/elements/Banner';
import Radio from '@material-ui/core/Radio';
import { Avatar } from '@material-ui/core';

const dummyLogistic = [
  {
    id: 1,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
  {
    id: 2,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
  {
    id: 3,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
];
const ModalSelectLogistic = (props) => {
  const [selected, setSelected] = useState({});

  const { listProductStorage } = useSelector((state) => state.outboundCustomer);

  const onChange = (e, logisticItem) => {
    // const isChecked = e.target.checked;
    // if (isChecked) {
    //   setSelected([...selected, logisticItem]);
    // } else {
    //   let newSelected = selected.filter((item) => item.id !== logisticItem.id);
    //   setSelected(newSelected);
    // }
    setSelected(logisticItem);
  };
  const handleDelete = (product) => {
    let newSelected = selected.filter((item) => item.id !== product.id);
    setSelected(newSelected);
  };
  // const { listAllProductSku } = useSelector((state) => state.inbound);
  const [logistics, setLogistics] = useState(dummyLogistic);

  const handleSearch = (e) => {
    let value = e.target.value;
    let filtered = dummyLogistic.filter((item) =>
      item?.name.toLowerCase().includes(value.toLowerCase())
    );
    setLogistics(filtered);
  };

  useEffect(() => {
    if (!props.show) {
      setSelected([]);
    } else if (props.show) {
      setSelected(props.initialValues);
    }
  }, [props.show]);

  const handleSelect = () => {
    props.onAgree(selected);
    props.onHide();
  };
  const renderItemLogistic = (item) => (
    <div
      className="d-flex p-2 align-items-center rounded mb-3"
      style={{ border: '1px solid #E8E8E8' }}
    >
      <Radio
        color=""
        style={{ color: '#192A55' }}
        onChange={(e) => onChange(e, item)}
        checked={item?.id === selected?.id}
      />
      <img
        alt="Logo"
        src={dummyLogistic[0].logo}
        style={{ height: '30px', margin: '0 15px' }}
      />
      <span
        style={{
          fontSize: '14px',
          fontWeight: '400',
          fontFamily: 'Open Sans',
          color: '#363636',
        }}
      >
        {item.name}
      </span>
    </div>
  );
  return (
    <>
      <Modal
        titleModal="Logistik"
        bodyClassName="pb-3 pt-0 p-0"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        scrollable
        content={(data) => (
          <>
            <Container withHeader={false}>
              <Item spaceBottom={3} border={false}>
                <div
                  className="p-3"
                  style={{
                    borderBottom: '1px solid #E8E8E8',
                    position: 'sticky',
                    top: '0px',
                    backgroundColor: '#FFFFFF',
                    zIndex: '1050',
                  }}
                >
                  <SearchField
                    onChange={handleSearch}
                    placeholder="Cari produk"
                  />
                  <h6
                    style={{
                      fontFamily: 'Open Sans',
                      fontSize: '14px',
                      color: '#363636',
                      fontWeight: '600',
                      margin: '10px 0px 0px',
                    }}
                  >
                    Pilih Logistik
                  </h6>
                </div>

                <div className="p-3">
                  {/* <table className="table table-striped mt-0 mb-5">
                    <tr
                      className="pt-0 pb-0"
                      style={{
                        borderBottom: '1px solid #E8E8E8',
                      }}
                    >
                      <th scope="col">Nama Produk</th>
                    </tr>
                    <tbody> */}
                  {logistics.map((item, idx) => renderItemLogistic(item))}
                  {!logistics.length && (
                    <div
                      style={{
                        height: '100px',
                        fontSize: '14px',
                        color: '#1C1C1C',
                        fontFamily: 'Open Sans',
                      }}
                      className="d-flex justify-content-center align-items-center"
                    >
                      Hasil pencarian logistik tidak ditemukan
                    </div>
                  )}
                </div>
              </Item>
            </Container>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType={selected?.id ? 'blueFill' : 'blueFillDisabled'}
              text="Pilih Logistik"
              onClick={handleSelect}
              disabled={!Boolean(selected?.id)}
            />
          </div>
        )}
      />
    </>
  );
};

export default ModalSelectLogistic;
