import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import MapModal from '../../../../components/elements/Map/MapModal';
import Banner from '../../../../components/elements/Banner';
import SwitchToggle from '../../../../components/elements/SwitchToggle';
import { makeStyles } from '@material-ui/core/styles';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
    marginBottom: '15px',
  },
});

function FormPengirimOrPenerima({ type = '', formik }) {
  const classes = useStyles();
  const [state, setState] = useState({
    showMap: false,
    provice_id: 0,
    city_id: 0,
    district_id: 0,
    isDropshipper: false,
    showMapStore: false,
  });

  const dispatch = useDispatch();
  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const { dataUser: dataStore } = useSelector((state) => state.login);
  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);

  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(state.provice_id, Number(e.target.value)));
  };
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  };

  const selectCoordinate = (coordinate) => {
    const lng = type === 'Pengirim' ? 'sender_longitude' : 'receiver_longitude';
    const lat = type === 'Pengirim' ? 'sender_latitude' : 'receiver_latitude';
    formik.setFieldValue(lng, `${coordinate.lng}`);
    formik.setFieldValue(lat, `${coordinate.lat}`);
    setState({ ...state, showMap: false });
  };
  const handleCheckedDropshipper = (checked) => {
    const isChecked = checked ? '1' : '0';
    formik.setFieldValue('is_dropshipper', isChecked);
    setState({ ...state, isDropshipper: checked });
  };
  return (
    <Container withHeader={false}>
      <h6>Data {type}</h6>
      {type === 'Pengirim' && (
        <Item border={false}>
          <Banner
            styleType="blue"
            description="Kirim sebagai dropshipper merupakan fitur untuk memudahkan store mengirim produk sesuai nama dropshipper setiap store."
            button={{ isShow: false }}
          />
          <Row className="mt-3 mb-3">
            <Col xs={6}>
              <Form.Label as={'h6'} className="input-label">
                Kirim sebagai Dropshipper
              </Form.Label>
            </Col>
            <Col xs={6} className="d-flex justify-content-end">
              <SwitchToggle
                onChange={handleCheckedDropshipper}
                name="isDropshipper"
                containerClass="ml-auto"
              />
            </Col>
          </Row>
        </Item>
      )}

      <Item border={false}>
        {type === 'Penerima' || (type == 'Pengirim' && state.isDropshipper) ? (
          <>
            <TextField
              className="mb-3"
              label={`Nama ${type}`}
              placeholder={`Input nama ${type.toLowerCase()}`}
              withValidate
              formik={formik}
              name={type === 'Pengirim' ? 'sender_name' : 'receiver_name'}
            />
            <Row className="mb-3">
              <Col>
                <SelectField
                  label={`Nomor Telepon ${type}`}
                  options={[
                    {
                      value: '+62',
                      description: 'Indonesia(+62)',
                    },
                  ]}
                  withValidate
                  formik={formik}
                  name={
                    type === 'Pengirim'
                      ? 'sender_country_code'
                      : 'receiver_country_code'
                  }
                />
              </Col>
              <Col className="d-flex align-items-end">
                <TextField
                  className="w-100"
                  placeholder="Input nomor telepon"
                  withValidate
                  formik={formik}
                  name={
                    type === 'Pengirim'
                      ? 'sender_phone_number'
                      : 'receiver_phone_number'
                  }
                  withError={false}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
                {formik.errors[
                  type === 'Pengirim'
                    ? 'sender_phone_number'
                    : 'receiver_phone_number'
                ] &&
                  formik.touched[
                    type === 'Pengirim'
                      ? 'sender_phone_number'
                      : 'receiver_phone_number'
                  ] && (
                    <Form.Text className="text-input-error">
                      {
                        formik.errors[
                          type === 'Pengirim'
                            ? 'sender_phone_number'
                            : 'receiver_phone_number'
                        ]
                      }
                    </Form.Text>
                  )}
              </Col>
            </Row>
            <div className="map-regular-transaction mb-3">
              <div
                className="d-flex flex-row justify-content-between align-items-center w-100 p-3 flex-wrap"
                style={{
                  backgroundColor: 'rgba(28,28,28,0.7)',
                  height: '100px',
                }}
              >
                <div
                  style={{
                    fontSize: '14px',
                    fontWeight: '600',
                    fontFamily: 'Open Sans',
                    marginRight: '10px',
                    flexGrow: 1,
                  }}
                >
                  Tandai lokasi peta Anda agar alamat lebih akurat.
                </div>
                <Button
                  style={{ width: '170px' }}
                  styleType="whiteOutline"
                  text="Tandai Lokasi"
                  onClick={() => setState({ ...state, showMap: true })}
                />
              </div>
            </div>
            {formik.touched[
              type === 'Pengirim' ? 'sender_latitude' : 'receiver_latitude'
            ] &&
              formik.errors[
                type === 'Pengirim' ? 'sender_latitude' : 'receiver_latitude'
              ] && (
                <Form.Text className="text-input-error mb-3 mt-0">
                  {
                    formik.errors[
                      type === 'Pengirim'
                        ? 'sender_latitude'
                        : 'receiver_latitude'
                    ]
                  }
                </Form.Text>
              )}
            <TextArea
              className="mb-3"
              label="Alamat"
              placeholder={`Input alamat ${type.toLowerCase()}`}
              withValidate
              formik={formik}
              name={type === 'Pengirim' ? 'sender_address' : 'receiver_address'}
            />
            <Row>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                  className="mb-3"
                  label="Provinsi"
                  placeholder="Pilih provinsi"
                  onChange={handleChangeProvince}
                  options={listProvince}
                  withValidate
                  formik={formik}
                  name={
                    type === 'Pengirim'
                      ? 'sender_province_id'
                      : 'receiver_province_id'
                  }
                />
              </Col>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                  className="mb-3"
                  label="Kota"
                  placeholder="Pilih kota"
                  onChange={handleChangeCity}
                  name="city_id"
                  options={listCity}
                  withValidate
                  formik={formik}
                  name={
                    type === 'Pengirim' ? 'sender_city_id' : 'receiver_city_id'
                  }
                />
              </Col>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                  className="mb-3"
                  label="Kecamatan"
                  placeholder="Pilih kecamatan"
                  onChange={handleChangeDistrict}
                  name="city_id"
                  options={listDistrict}
                  withValidate
                  formik={formik}
                  name={
                    type === 'Pengirim'
                      ? 'sender_district_id'
                      : 'receiver_district_id'
                  }
                />
              </Col>
            </Row>
          </>
        ) : (
          <>
            <h6 className={classes.label}>Nama Store</h6>
            <h6 className={classes.value}>
              {dataStore?.store?.[0]?.store_name || '-'}
            </h6>
            <h6 className={classes.label}>Nomor Telepon Store</h6>
            <h6 className={classes.value}>
              {dataStore?.user?.phone_number || '-'}
            </h6>
            <h6 className={classes.label}>Alamat Store</h6>
            <h6 className={classes.value}>
              {dataStore?.store?.[0]?.store_address || '-'}
            </h6>
            <Button
              style={{ paddingLeft: '0', marginBottom: '10px' }}
              styleType="blueNoFill"
              text="Lihat Lokasi di Peta"
              startIcon={() => <MapOutlinedIcon />}
              onClick={() => setState({ ...state, showMapStore: true })}
            />
          </>
        )}
        <TextArea
          className="mb-3"
          label="Pesan (Opsional)"
          placeholder="Input pesan"
          rows={3}
          withValidate
          formik={formik}
          name={type === 'Pengirim' ? 'sender_note' : 'receiver_note'}
        />
      </Item>

      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        onAgree={selectCoordinate}
      />
      <MapModal
        show={state.showMapStore}
        onHide={() => setState({ ...state, showMapStore: false })}
        mode="viewer"
        initialCoordinate={{
          lng: dataStore?.store?.[0]?.longitude,
          lat: dataStore?.store?.[0]?.latitude,
        }}
      />
    </Container>
  );
}

export default FormPengirimOrPenerima;
