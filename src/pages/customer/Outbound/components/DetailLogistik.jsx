import React from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

function DetailLogistik({ data = {} }) {
  const classes = useStyles();
  return (
    <>
      <h6>Data Logistik</h6>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Logistik</h6>
        <h6 className={classes.value}>
          {data?.vendor_service?.vendor?.name || '-'}
        </h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Service</h6>
        <h6 className={classes.value}>
          {data?.vendor_service?.code || '-'}-
          {data?.vendor_service?.service || '-'}
        </h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>AWB</h6>
        <h6 className={classes.value}>{data?.awb_number || '-'}</h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Platform</h6>
        <h6 className={classes.value}>{data?.platform || '-'}</h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Pesan</h6>
        <h6 className={classes.value}>{data?.message || '-'}</h6>
      </Item>
    </>
  );
}

export default DetailLogistik;
