import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import { makeStyles } from '@material-ui/core/styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { setDataFormCreateDispatch } from '../reduxAction';
import Banner from '../../../../components/elements/Banner';
import SwitchToggle from '../../../../components/elements/SwitchToggle';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
    marginBottom: '15px',
  },
});

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

const validationSchema = Yup.object({
  //sender
  sender_name: Yup.string().required('Harap untuk mengisi nama pengirim'),
  sender_country_code: Yup.string().required(
    'Harap untuk memilih kode nomor telepon'
  ),
  sender_phone: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon pengirim'),
  sender_address: Yup.string().required('Harap untuk mengisi alamat pengirim'),
  sender_province_id: Yup.string().required('Harap untuk memilih provinsi'),
  sender_city_id: Yup.string().required('Harap untuk memilih kabupaten/kota'),
  sender_district_id: Yup.string().required('Harap untuk memilih kecamatan'),
  //receiver
  receiver_name: Yup.string().required('Harap untuk mengisi nama penerima'),
  receiver_country_code: Yup.string().required(
    'Harap untuk memilih kode nomor telepon'
  ),
  receiver_phone: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon penerima'),
  receiver_address: Yup.string().required(
    'Harap untuk mengisi alamat penerima'
  ),
  receiver_province_id: Yup.string().required('Harap untuk memilih provinsi'),
  receiver_city_id: Yup.string().required('Harap untuk memilih kabupaten/kota'),
  receiver_district_id: Yup.string().required('Harap untuk memilih kecamatan'),
});

function FormSenderAndReceiverDispatchOrder({ handleChangeTab }) {
  const [state, setState] = useState({
    showModalInvoice: false,
    provice_id: 0,
    city_id: 0,
    district_id: 0,
    invoiceImage: '',
    isDropshipper: false,
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const { formDataCreateDispatch } = useSelector(state => state.outboundCustomer)
  const { dataUser: dataStore } = useSelector((state) => state.login);

  const formik = useFormik({
    initialValues: {
      is_dropshipper: '0',
      //sender
      sender_name: formDataCreateDispatch?.sender_name || '',
      sender_country_code: formDataCreateDispatch?.sender_country_code || '62',
      sender_phone: formDataCreateDispatch?.sender_phone || '',
      sender_address: formDataCreateDispatch?.sender_address || '',
      sender_province_id: formDataCreateDispatch?.sender_province_id || '',
      sender_city_id: formDataCreateDispatch?.sender_city_id || '',
      sender_district_id: formDataCreateDispatch?.sender_district_id || '',
      //receiver
      receiver_name: formDataCreateDispatch?.receiver_name || '',
      receiver_country_code: formDataCreateDispatch?.receiver_country_code || '62',
      receiver_phone: formDataCreateDispatch?.receiver_phone || '',
      receiver_address: formDataCreateDispatch?.receiver_address || '',
      receiver_province_id: formDataCreateDispatch?.receiver_province_id || '',
      receiver_city_id: formDataCreateDispatch?.receiver_city_id || '',
      receiver_district_id: formDataCreateDispatch?.receiver_district_id || '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // handleSubmit(values);
      dispatch(setDataFormCreateDispatch(values));
      handleChangeTab(2);
    },
  });

  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);

  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(state.provice_id, Number(e.target.value)));
  };
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  };
  const handleValueArea = (area_id = null, listArea = []) => {
    if (area_id) {
      const find = listArea.find((e) => (e.value === area_id))
      return find
    }
  }
  const selectCoordinate = (coordinate) => {
    formik.setFieldValue('longitude', coordinate.lng);
    formik.setFieldValue('latitude', coordinate.lat);
    setState({ ...state, showMap: false });
  };

  const handleCheckedDropshipper = (checked) => {
    const isChecked = checked ? '1' : '0';
    formik.setFieldValue('is_dropshipper', isChecked);
    setState({ ...state, isDropshipper: checked });
  };

  const handleSubmit = (e) => {
    if (!state.isDropshipper) {
      formik.setValues({
        ...formik.values,
        sender_name: dataStore?.store?.[0]?.store_name,
        sender_country_code: dataStore?.user?.country_code,
        sender_phone: dataStore?.user?.phone_number,
        sender_address: dataStore?.store?.[0]?.store_address,
        sender_province_id: dataStore?.store?.[0]?.province_id,
        sender_city_id: dataStore?.store?.[0]?.city_id,
        sender_district_id: dataStore?.store?.[0]?.district_id,
      });
    }
    formik.handleSubmit(e);
  };
  return (
    <Container>
      <Item
        className="p-3"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '600px' }}
      >
        <h6>Data Pengirim</h6>
        <Banner
          styleType="blue"
          description="Kirim sebagai dropshipper merupakan fitur untuk memudahkan store mengirim produk sesuai nama dropshipper setiap store."
          button={{ isShow: false }}
        />
        <Row className="mt-3 mb-3">
          <Col xs={6}>
            <Form.Label as={'h6'} className="input-label">
              Kirim sebagai Dropshipper
            </Form.Label>
          </Col>
          <Col xs={6} className="d-flex justify-content-end">
            <SwitchToggle
              onChange={handleCheckedDropshipper}
              name="isDropshipper"
            />
          </Col>
        </Row>
        {state.isDropshipper ? (
          <>
            <TextField
              className="mb-3"
              label="Nama Dropshipper"
              placeholder="Input nama dropshipper"
              withValidate
              formik={formik}
              name="sender_name"
            />
            <Row className="mb-3">
              <Col xs={12} lg={6}>
                <SelectField
                  label="Nomor Telepon Dropshipper"
                  options={[
                    {
                      value: '62',
                      description: 'Indonesia(+62)',
                    },
                  ]}
                  withValidate
                  formik={formik}
                  name="sender_country_code"
                  withError={false}
                />
              </Col>
              <Col xs={12} lg={6} className="d-flex align-items-end">
                <TextField
                  className="w-100"
                  placeholder="Input nomor telepon"
                  withValidate
                  formik={formik}
                  name="sender_phone"
                  withError={false}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
                {formik.errors.sender_phone && formik.touched.sender_phone && (
                  <Form.Text className="text-input-error">
                    {formik.errors.sender_phone}
                  </Form.Text>
                )}
              </Col>
            </Row>
            <TextArea
              className="mb-3"
              label="Alamat Dropshipper"
              placeholder="Input alamat dropshipper"
              name="sender_address"
              formik={formik}
              withValidate
            />
            <Row>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                value={handleValueArea(formDataCreateDispatch?.sender_province_id || null, listProvince)}
                  className="mb-3"
                  label="Provinsi"
                  placeholder="Pilih provinsi"
                  onChange={handleChangeProvince}
                  options={listProvince}
                  name="sender_province_id"
                  formik={formik}
                  withValidate
                />
              </Col>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                value={handleValueArea(formDataCreateDispatch?.sender_city_id || null, listCity)}
                  className="mb-3"
                  label="Kabupaten/Kota"
                  placeholder="Pilih kabuten/kota"
                  onChange={handleChangeCity}
                  name="sender_city_id"
                  formik={formik}
                  withValidate
                  options={listCity}
                />
              </Col>
              <Col xs={12} md={6} lg={4}>
                <ReactSelect
                 value={handleValueArea(formDataCreateDispatch?.sender_district_id || null, listDistrict)}
                  className="mb-3"
                  label="Kecamatan"
                  placeholder="Pilih kecamatan"
                  onChange={handleChangeDistrict}
                  name="sender_district_id"
                  formik={formik}
                  withValidate
                  options={listDistrict}
                />
              </Col>
            </Row>
          </>
        ) : (
          <>
            <h6 className={classes.label}>Nama Store</h6>
            <h6 className={classes.value}>
              {dataStore?.store?.[0]?.store_name || '-'}
            </h6>
            <h6 className={classes.label}>Nomor Telepon Store</h6>
            <h6 className={classes.value}>
              {dataStore?.user?.phone_number || '-'}
            </h6>
            <h6 className={classes.label}>Alamat Store</h6>
            <h6 className={classes.value}>
              {dataStore?.store?.[0]?.store_address || '-'}
            </h6>
          </>
        )}
      </Item>
      <Item
        className="p-3"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '600px' }}
      >
        <h6>Data Penerima</h6>
        <TextField
          className="mb-3"
          label="Nama Penerima"
          placeholder="Input nama penerima"
          withValidate
          formik={formik}
          name="receiver_name"
        />
        <Row className="mb-3">
          <Col xs={12} lg={6}>
            <SelectField
              label="Nomor Telepon Penerima"
              options={[
                {
                  value: '62',
                  description: 'Indonesia(+62)',
                },
              ]}
              withValidate
              formik={formik}
              name="receiver_country_code"
              withError={false}
            />
          </Col>
          <Col xs={12} lg={6} className="d-flex align-items-end">
            <TextField
              className="w-100"
              placeholder="Input nomor telepon"
              withValidate
              formik={formik}
              name="receiver_phone"
              withError={false}
            />
          </Col>
          <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
            {formik.errors.receiver_phone && formik.touched.receiver_phone && (
              <Form.Text className="text-input-error">
                {formik.errors.receiver_phone}
              </Form.Text>
            )}
          </Col>
        </Row>
        {/* <Form.Label as={'h6'} className="input-label">
          Pilih Koordinat
        </Form.Label>
        <div className="background-map mb-3">
          <div
            className="d-flex flex-column flex-lg-row justify-content-between align-items-center w-100 p-3"
            style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
          >
            <span
              style={{
                fontSize: '14px',
                fontWeight: '600',
                fontFamily: 'Open Sans',
              }}
            >
              Tandai lokasi.
            </span>
            <Button
              style={{ width: '200px', backgroundColor: '#FAFAFF' }}
              styleType="blueNoFill"
              text="Tandai Lokasi"
              onClick={() => setState({ ...state, showMap: true })}
            />
          </div>
        </div>
        {formik.touched.latitude && formik.errors.latitude && (
          <Form.Text className="text-input-error mb-3 mt-0">
            {formik.errors.latitude}
          </Form.Text>
        )} */}
        <TextArea
          className="mb-3"
          label="Alamat"
          placeholder="Input alamat penerima"
          name="receiver_address"
          formik={formik}
          withValidate
        />
        <Row>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
            value={handleValueArea(formDataCreateDispatch?.receiver_province_id || null, listProvince)}
              className="mb-3"
              label="Provinsi"
              placeholder="Pilih provinsi"
              onChange={handleChangeProvince}
              options={listProvince}
              name="receiver_province_id"
              formik={formik}
              withValidate
            />
          </Col>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
            value={handleValueArea(formDataCreateDispatch?.receiver_city_id || null, listCity)}
              className="mb-3"
              label="Kabupaten/Kota"
              placeholder="Pilih kabuten/kota"
              onChange={handleChangeCity}
              name="receiver_city_id"
              formik={formik}
              withValidate
              options={listCity}
            />
          </Col>
          <Col xs={12} md={6} lg={4}>
            <ReactSelect
             value={handleValueArea(formDataCreateDispatch?.receiver_district_id || null, listDistrict)}
              className="mb-3"
              label="Kecamatan"
              placeholder="Pilih kecamatan"
              onChange={handleChangeDistrict}
              name="receiver_district_id"
              formik={formik}
              withValidate
              options={listDistrict}
            />
          </Col>
        </Row>
      </Item>
      <div className="ml-auto">
        <Button
          style={{ width: '140px' }}
          styleType="lightBlueFill"
          text="Kembali"
          onClick={() => history.push('/outbound/dispatch-order')}
        />
        <Button
          style={{ minWidth: '140px', marginLeft: '10px' }}
          styleType="blueFill"
          text="Lanjutkan"
          onClick={handleSubmit}
        />
      </div>
    </Container>
  );
}

export default FormSenderAndReceiverDispatchOrder;
