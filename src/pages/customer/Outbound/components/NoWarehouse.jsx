import React from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import warehouseSelect from '../../../../assets/images/warehouseSelect.svg';
import Button from '../../../../components/elements/Button';

function NoWarehouse() {
  return (
    <ContentContainer withHeader={false}>
      <ContentItem
        border={false}
        style={{
          height: '75vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
        }}
      >
        <img src={warehouseSelect} alt="" />
        <h6
          style={{
            fontFamily: 'Work Sans',
            fontWeight: '700',
            fontSize: '18px',
            color: '#1c1c1c',
            marginTop: '15px',
            marginBottom: '10px',
          }}
        >
          Anda belum memiliki gudang
        </h6>
        <h6
          style={{
            fontFamily: 'Open Sans',
            fontWeight: '400',
            fontSize: '14px',
            color: '#4f4f4f',
            marginBottom: '15px',
          }}
        >
          Silahkan untuk memilih gudang agar bisa menggunakan fitur “Mutasi
          Stock”
        </h6>
        <Button styleType="blueFill" text="Pilih Gudang" />
      </ContentItem>
    </ContentContainer>
  );
}

export default NoWarehouse;
