import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { dummy } from './dataDummy';
import AddIcon from '@material-ui/icons/Add';
import Counter from '../../../../components/elements/Counter';
import DeleteProductModal from './DeleteProductModal';
import SelectProductModal from './SelectProductModal';

const useStyles = makeStyles({
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const ActionField = ({ data, onClick }) => {
  return (
    <div
      onClick={() => onClick(data)}
      className="p-1 rounded button"
      style={{ border: '2px solid #192A55', width: 'min-content' }}
    >
      <i
        className="far fa-trash-alt"
        style={{ fontSize: '20px', color: '#192A55', cursor: 'pointer' }}
      ></i>
    </div>
  );
};

const QtyField = ({ data = {}, isEdit, handleCounter }) => {
  return isEdit ? (
    <Counter
      onDecrease={() => handleCounter(data, '-')}
      onIncrease={() => handleCounter(data, '+')}
      value={data?.qty}
    />
  ) : (
    <span>{data?.qty}</span>
  );
};

const EmptyData = ({ onClick, isEdit }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada produk yang akan dikirim saat ini.
      </h6>
      {isEdit && (
        <Button
          startIcon={() => <AddIcon />}
          text="Pilih SKU"
          styleType="blueOutline"
          onClick={onClick}
        />
      )}
    </div>
  );
};

const HeaderTable = (props) => {
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <h6 className="mb-2 mt-2">Produk Dikirim</h6>
      </div>
      {props.isEdit && (
        <Button
          startIcon={() => <AddIcon />}
          text="Pilih SKU"
          styleType="blueOutline"
          onClick={props.onClick}
        />
      )}
    </div>
  );
};

function ProdukDikirimTable(props) {
  const dispatch = useDispatch();
  const { isEdit, data } = props;
  const [state, setState] = useState({
    deleteModal: false,
    dataDelete: {},
    selectProductModal: false,
  });
  const [selected, setSelected] = useState([]);

  const handleSelectSKU = (selectedSKU = []) => {
    let newSelected = [];
    selectedSKU.forEach((itemSKU) => {
      const checkSKU = selected.filter((item) => item.id === itemSKU.id).length;
      if (!Boolean(checkSKU)) {
        newSelected = [...newSelected, { ...itemSKU, qty: 0 }];
      }
    });
    setSelected([...selected, ...newSelected]);
    // handleSelectProductSKU([...selected, ...newSelected]);
  };

  const handleCounter = (itemSKU, action = '+') => {
    if (action === '+' && itemSKU.qty >= 0) {
      const newSKU = selected.map((item) => {
        if (item.id === itemSKU.id) {
          return {
            ...item,
            qty: itemSKU.qty + 1,
          };
        } else {
          return item;
        }
      });
      setSelected(newSKU);
      // handleSelectProductSKU(newSKU);
    } else if (action === '-' && itemSKU.qty > 0) {
      const newSKU = selected.map((item) => {
        if (item.id === itemSKU.id) {
          return {
            ...item,
            qty: itemSKU.qty - 1,
          };
        } else {
          return item;
        }
      });
      setSelected(newSKU);
      // handleSelectProductSKU(newSKU);
    }
  };

  return (
    <div>
      <DataTable
        column={[
          {
            heading: 'SKU ',
            key: 'sku',
          },
          {
            heading: 'Produk ',
            key: 'name',
          },
          isEdit && {
            heading: 'Ukuran ',
            key: 'size',
          },
          {
            heading: 'Qty',
            render: (item) => (
              <QtyField
                isEdit={isEdit}
                data={item}
                handleCounter={handleCounter}
              />
            ),
          },
          isEdit && {
            heading: 'Aksi',
            render: (item) => (
              <ActionField
                onClick={(item) =>
                  setState({ ...state, deleteModal: true, dataDelete: item })
                }
                data={item}
              />
            ),
          },
        ]}
        data={isEdit ? selected : data?.items}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            isEdit={isEdit}
            onClick={() => setState({ ...state, selectProductModal: true })}
          />
        )}
        renderEmptyData={() => (
          <EmptyData
            isEdit={isEdit}
            onClick={() => setState({ ...state, selectProductModal: true })}
          />
        )}
        transformData={(item) =>
          isEdit
            ? {
                ...item,
                size: `${item?.length || 0}cm x ${item?.width || 0}cm ${
                  item?.height ? `x ${item?.height}cm` : ''
                }`,
              }
            : {
                ...item,
                sku: item?.product_storage?.product?.sku || '-',
                name: item?.product_storage?.product?.name || '-',
                qty: item?.quantity || '-',
              }
        }
        showFooter={false}
      />
      <DeleteProductModal
        onHide={() => setState({ ...state, deleteModal: false })}
        show={state.deleteModal}
        data={state.dataDelete}
      />
      <SelectProductModal
        onHide={() => setState({ ...state, selectProductModal: false })}
        show={state.selectProductModal}
        initialValues={selected}
        onAgree={handleSelectSKU}
      />
    </div>
  );
}
ProdukDikirimTable.defaultProps = {
  isEdit: true,
  data: {},
};
export default ProdukDikirimTable;
