import React from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import transactionSuccess from '../../../../assets/images/transactionSuccess.svg';
import Button from '../../../../components/elements/Button';
import BackButton from '../../../../components/elements/BackButton';
import { useHistory } from 'react-router-dom';

function TransactionSuccess() {
  const history = useHistory();
  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label="Kembali ke Transaksi"
            onClick={() => history.push('/transactions')}
          />
        </div>
      )}
    >
      <ContentItem
        border={false}
        style={{
          height: '70vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
        }}
      >
        <img src={transactionSuccess} alt="" />
        <h6
          style={{
            fontFamily: 'Work Sans',
            fontWeight: '700',
            fontSize: '18px',
            color: '#1c1c1c',
            marginTop: '15px',
            marginBottom: '10px',
          }}
        >
          Transaksi berhasil diterima gudang
        </h6>
        <h6
          style={{
            fontFamily: 'Open Sans',
            fontWeight: '400',
            fontSize: '14px',
            color: '#4f4f4f',
            marginBottom: '15px',
          }}
        >
          Proses pengemasan serta pengiriman ke kurir akan diproses dalam waktu
          kurang dari 24 jam.{' '}
        </h6>
        <Button styleType="blueFill" text="Buat Transaksi" />
      </ContentItem>
    </ContentContainer>
  );
}

export default TransactionSuccess;
