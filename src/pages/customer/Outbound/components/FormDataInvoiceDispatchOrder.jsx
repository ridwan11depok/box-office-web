import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import PreviewPDF from '../../../../components/elements/PreviewPDF';
import { Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { validateFile } from '../../../../utils/validate';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { PictureAsPdf } from '@material-ui/icons';
import {
  setDataFormCreateDispatch,
  getListVendor,
  checkAWBCashless,
} from '../reduxAction';
import Toastify from '../../../../components/elements/Toast';
import { convertDataURIToBinary } from '../../../../utils/uint8array';

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

const validationSchema = Yup.object({
  invoice_file: Yup.mixed().required('Harap memasukkan bukti gambar invoice'),
  invoice_number: Yup.string().required('Harap untuk mengisi nomor invoice'),
  vendor_id: Yup.string().required('Harap untuk memilih logistik'),
  awb_number: Yup.string().required('Harap untuk mengisi AWB'),
  // message: Yup.string(),
});

const allowedExtentions = [
  'image/png',
  'image/jpg',
  'image/jpeg',
  'application/pdf',
];

function FormDataInvoice({ handleChangeTab }) {
  const [state, setState] = useState({
    showMap: false,
    provice_id: 0,
    city_id: 0,
    invoiceImage: '',
  });

  const dispatch = useDispatch();
  const history = useHistory();

  const { listVendor, formDataCreateDispatch } = useSelector((state) => state.outboundCustomer);

  useEffect(() => {
    if (formDataCreateDispatch?.invoice_file) setState({ ...state, invoiceImage: formDataCreateDispatch?.invoice_file })
  }, [formDataCreateDispatch])

  useEffect(() => {
    if (!listVendor.length) {
      dispatch(getListVendor());
    }
  }, []);

  const formik = useFormik({
    initialValues: {
      invoice_file: formDataCreateDispatch?.invoice_file ? formDataCreateDispatch?.invoice_file : '',
      invoice_number: formDataCreateDispatch?.invoice_number ? formDataCreateDispatch?.invoice_number : '',
      vendor_id: formDataCreateDispatch?.vendor_id ? formDataCreateDispatch?.vendor_id : '',
      awb_number: formDataCreateDispatch?.awb_number ? formDataCreateDispatch?.awb_number : '',
      message: formDataCreateDispatch?.message ? formDataCreateDispatch?.message : '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
      // console.log(values);
    },
  });

  const handleSubmit = async (values) => {
    try {
      const result = await dispatch(
        checkAWBCashless(values.vendor_id, values.awb_number)
      );
      if (result.code === 200) {
        dispatch(
          setDataFormCreateDispatch({
            ...values,
            vendor_service: result?.result?.service,
            // vendor_name: listVendor.filter(
            //   (item) => item.value == values.vendor_id
            // )?.[0]?.label,
            vendor_name: result?.result?.vendor_service?.vendor?.name,
            platform: result?.result?.platform,
            vendor_service_id: result?.result?.vendor_service?.id,
          })
        );
        handleChangeTab(2);
      } else {
        Toastify({ type: 'error', message: 'Nomor AWB tidak ditemukan' });
      }
    } catch (err) {
      Toastify({
        type: 'error',
        message: err?.error?.message || 'Nomor AWB tidak ditemukan',
      });
    }
  };
  const invoiceRef = React.createRef();

  const previewPDF = (base64PDF) => {
    const uint8array = convertDataURIToBinary(base64PDF)
    setState({ ...state, invoicePDF: uint8array })
  }

  const handleChangeImage = async (e) => {
    const files = e.target.files;
    const { file } = await validateFile({
      files,
      type: 'application',
      allowedExtensions: ['jpg', 'png', 'jpeg', 'pdf'],
    }, previewPDF);
    if (file) {
      setState({ ...state, invoiceImage: file });
      formik.setFieldValue('invoice_file', file);
    }
  };

  const handleValueLogistik = ( vendor_id = null, listVendor = [] ) => {
    if(vendor_id){
      const find = listVendor.find((e) => e.id === vendor_id)
      return {
        ...find,
        label: find?.name,
        value: find?.id
      }
    }
  }

  return (
    <Container withHeader={false}>
      <Item
        className="p-3"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '500px' }}
      >
        <h6>Invoice</h6>
        <h6 className="input-label">
          Silahkan upload bukti gambar invoice dibawah dengan format gambar .jpg
          .jpeg .png atau .pdf
        </h6>
        <div
          style={{
            height: '200px',
            borderRadius: '4px',
            border: '2px dashed #CFCFCF',
            backgroundColor: '#F2F2F2',
            overflow: 'hidden',
            position: 'relative',
          }}
          className="d-flex flex-column justify-content-center align-items-center mb-1"
        >
          {state.invoiceImage ? (
            <>
              <Button
                style={{
                  position: 'absolute',
                  right: '10px',
                  top: '10px',
                }}
                styleType="lightBlueFillOutline"
                text="Ganti Invoice"
                onClick={() => invoiceRef.current.click()}
              />
              {state.invoiceImage?.type === 'application/pdf' ?
                <>
                  <PictureAsPdf style={{ fontSize: "50px" }} />
                  <span>{state.invoiceImage?.name}</span>
                  
                </>
                :
                <img
                  src={URL.createObjectURL(state.invoiceImage)}
                  alt=""
                  style={{ height: '200px' }}
                />
              }

            </>
          ) : (
            <>
              <img className="mt-2" src={pictureIcon} alt="" />

              <Button
                className="mt-2 mb-3"
                styleType="blueOutline"
                text="Upload Invoice"
                onClick={() => invoiceRef.current.click()}
              />
            </>
          )}
          <input
            onChange={(e) => handleChangeImage(e)}
            ref={invoiceRef}
            type="file"
            className="d-none"
            accept={allowedExtentions.join(',')}
          />
        </div>
        {formik.touched.invoice_file && formik.errors.invoice_file && (
          <Form.Text className="text-input-error">
            {formik.errors.invoice_file}
          </Form.Text>
        )}
        <TextField
          className="mb-3 mt-3"
          label="Nomor Invoice"
          placeholder="Input nomor invoice"
          formik={formik}
          name="invoice_number"
          withValidate
        />
      </Item>
      <Item
        className="p-3"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '500px' }}
      >
        <h6>Data Logistik</h6>
        <ReactSelect
          value={handleValueLogistik(formDataCreateDispatch?.vendor_id || null, listVendor)}
          className="mb-3"
          label="Pilih Logistik"
          placeholder="Pilih logistik"
          formik={formik}
          name="vendor_id"
          withValidate
          options={listVendor}
        />
        <h6 className="input-label">AWB</h6>
        <h6
          style={{
            fontSize: '14px',
            fontFamily: 'Open Sans',
            fontWeight: '400',
            color: '#4F4F4F',
          }}
        >
          AWB yang diinput akan divalidasi sesuai dengan logistik yang
          digunakan.
        </h6>
        <TextField
          className="mb-3"
          placeholder="Input kode AWB"
          formik={formik}
          name="awb_number"
          withValidate
        />
        <TextArea
          className="mb-3"
          label="Pesan (Opsional)"
          placeholder="Input pesan"
          formik={formik}
          name="message"
          withValidate
        />
      </Item>
      <div className="ml-auto">
        <Button
          style={{ width: '140px' }}
          styleType="lightBlueFill"
          text="Batal"
          onClick={() => history.push('/outbound/cashless')}
        />
        <Button
          style={{ minWidth: '140px', marginLeft: '10px' }}
          styleType="blueFill"
          text="Simpan"
          onClick={(e) => formik.handleSubmit(e)}
        />
      </div>
    </Container>
  );
}

export default FormDataInvoice;
