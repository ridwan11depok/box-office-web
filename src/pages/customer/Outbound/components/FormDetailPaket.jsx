import React, { useEffect, useState } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import {
  TextField,
  SelectField,
  TextArea,
  CurrencyInput,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { Row, Col, Form } from 'react-bootstrap';
import { getListCategory } from '../../System Settings/reduxAction';
import { useSelector, useDispatch } from 'react-redux';

function FormDetailPaket({ formik }) {
  const [state, setState] = useState({ category_product_id: null });
  const dispatch = useDispatch();
  const { listCategoryProduct, listSubCategoryProduct } = useSelector(
    (state) => state.systemSettingCustomer
  );

  useEffect(() => {
    dispatch(getListCategory());
  }, []);

  const handleChangeCategory = (e) => {
    dispatch(getListCategory(e.target.value));
    setState({ ...state, category_product_id: e.target.value });
  };
  return (
    <Container withHeader={false}>
      <h6>Detail Paket</h6>
      <Item border={false}>
        <Row className="mb-3">
          <Col md={6}>
            <SelectField
              label="Kategori Produk"
              onChange={handleChangeCategory}
              withValidate
              formik={formik}
              name="category_product_id"
              options={listCategoryProduct}
            />
            <TextField
              label="Panjang"
              className="w-100"
              placeholder="Input panjang"
              suffix={() => <span>cm</span>}
              withValidate
              formik={formik}
              name="length"
            />
            <TextField
              label="Tinggi"
              className="w-100"
              placeholder="Input tinggi"
              suffix={() => <span>cm</span>}
              withValidate
              formik={formik}
              name="height"
            />
            <CurrencyInput
              label="Nilai Barang"
              className="w-100"
              placeholder="Input nilai barang"
              withValidate
              formik={formik}
              name="item_price"
            />
          </Col>
          <Col md={6}>
            <SelectField
              label="Sub Kategori Produk"
              options={listSubCategoryProduct}
              withValidate
              formik={formik}
              name="sub_category_product_id"
              disabled={!Boolean(state.category_product_id)}
            />
            <TextField
              label="Lebar"
              className="w-100"
              placeholder="Input tinggi"
              suffix={() => <span>cm</span>}
              withValidate
              formik={formik}
              name="width"
            />
            <TextField
              label="Berat"
              className="w-100"
              placeholder="Input berat"
              suffix={() => <span>kg</span>}
              withValidate
              formik={formik}
              name="weight"
            />
          </Col>
        </Row>
      </Item>
    </Container>
  );
}

export default FormDetailPaket;
