import React, { useState } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import {
  ReactSelect,
  LogisticSelect,
} from '../../../../components/elements/InputField';
import Checkbox from '@material-ui/core/Checkbox';
import { Row, Col } from 'react-bootstrap';
import { formatRupiah } from '../../../../utils/text';
import Radio from '@material-ui/core/Radio';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { getListServiceVendor, createDispatchOrder } from '../reduxAction';
import { useHistory } from 'react-router-dom';

const dummyLogistic = [
  {
    id: 1,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
  {
    id: 2,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
  {
    id: 3,
    name: 'SiCepat',
    logo: 'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1640563200&Signature=WeYoJ8TEvJefviA8BuIWUdQqcSUhsTX-Wv2NhpuBRkEekDFsGbxM52fBGa2v9RsBe0DFQBVPcz1nhxs6bqGyGP-1~kxAiWKicig5Kf~~178k9N7PKCFyRoHJXqEaFjZoNTMEkKnOjI4WbjIzgpJqGBgorhlkgReYEYyi9om-ibQP2ZRBVNVyxWgUqiQZH21ECidtfeZ~M4sYDriiC4VMJCGO4hmxwCqIuvNotF1gqsE9jgJAcFNXiuikK1o41Cn-byBJjytdtu5MmT9p33yZ3SbPPqwdP0ryarrNp~g7u8QEH4tI0ozX3pzjd3YO~tquQKgI4JCdkQVz7HVjrBwTJg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
  },
];

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#363636',
    textAlign: 'right',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  total: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '15px 0',
  },
  duration: {
    fontSize: '12px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4F4F4F',
  },
});

function FormLogisticDispatchOrderRegular(props) {
  const { handleChangeTab } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [state, setState] = useState({
    vendor: null,
    vendorService: null,
  });
  const { isLoading } = useSelector((state) => state.loading);
  const {
    listVendor,
    listServiceVendor,
    formDataCreateDispatch,
    selectedStorage,
  } = useSelector((state) => state.outboundCustomer);
  const { dataUser } = useSelector((state) => state.login);
  const renderItemServiceLogistic = (item) => (
    <Col xs={12} md={6} lg={4}>
      <div
        className="d-flex p-2 align-items-center rounded mb-3"
        style={{ border: '1px solid #E8E8E8' }}
      >
        <Radio
          color=""
          style={{ color: '#192A55' }}
          onChange={(e) => handleSelectVendorService(item)}
          checked={state.vendorService?.id === item?.id}
        />
        <div>
          <h6 className={classes.label}>{item?.code}</h6>
          <h6 className={classes.duration}>2 hari</h6>
        </div>
        <div className="ml-auto">
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </div>
    </Col>
  );

  const renderEmptyService = () => {
    return (
      <div
        className="d-flex flex-column align-items-center justify-content-center w-100"
        style={{ height: '30vh' }}
      >
        <h6 className={classes.description}>
          Tidak ada layanan logistik saat ini. Silahkan pilih logistik terlebih
          dahulu
        </h6>
      </div>
    );
  };

  const handleSelectVendor = (selected) => {
    // formik.setFieldValue('vendor_id', selected?.id);
    dispatch(getListServiceVendor(selected?.id));
    setState({ ...state, vendor: selected });
  };
  const handleSelectVendorService = (selected) => {
    // formik.setFieldValue('vendor_service_id', selected?.id);
    setState({ ...state, vendorService: selected });
  };
  const handleChangeInsurance = (e) => {
    const isChecked = e.target.checked ? '1' : '0';
    // formik.setFieldValue('is_insurance', isChecked);
  };

  const handleSubmit = async () => {
    const data = new FormData();
    data.append('store_id', dataUser?.store?.[0]?.id);
    data.append('storage_id', selectedStorage?.id);
    //sender
    data.append('sender_name', formDataCreateDispatch?.sender_name);
    data.append(
      'sender_country_code',
      formDataCreateDispatch?.sender_country_code
    );
    data.append('sender_phone', formDataCreateDispatch?.sender_phone);
    data.append('sender_address', formDataCreateDispatch?.sender_address);
    data.append(
      'sender_province_id',
      formDataCreateDispatch?.sender_province_id
    );
    data.append('sender_city_id', formDataCreateDispatch?.sender_city_id);
    data.append(
      'sender_district_id',
      formDataCreateDispatch?.sender_district_id
    );
    //receiver
    data.append('receiver_name', formDataCreateDispatch?.receiver_name);
    data.append(
      'receiver_country_code',
      formDataCreateDispatch?.receiver_country_code
    );
    data.append('receiver_phone', formDataCreateDispatch?.receiver_phone);
    data.append('receiver_address', formDataCreateDispatch?.receiver_address);
    data.append(
      'receiver_province_id',
      formDataCreateDispatch?.receiver_province_id
    );
    data.append('receiver_city_id', formDataCreateDispatch?.receiver_city_id);
    data.append(
      'receiver_district_id',
      formDataCreateDispatch?.receiver_district_id
    );
    //product & packaging
    formDataCreateDispatch?.selectedProduct.forEach((item, idx) =>
      data.append(`product[${idx}][id]`, item?.id)
    );
    formDataCreateDispatch?.selectedProduct.forEach((item, idx) =>
      data.append(`product[${idx}][quantity]`, item?.qty)
    );
    formDataCreateDispatch?.selectedPackaging.forEach((item, idx) =>
      data.append(`packaging[${idx}][id]`, item?.id)
    );
    formDataCreateDispatch?.selectedPackaging.forEach((item, idx) =>
      data.append(`packaging[${idx}][quantity]`, item?.qty)
    );
    //logistic
    data.append('vendor_id', state.vendor?.id);
    data.append('vendor_service_id', state.vendorService?.id);
    data.append('is_dropshipper', formDataCreateDispatch?.is_dropshipper);

    try {
      const result = await dispatch(createDispatchOrder(data));
      history.push(`/outbound/dispatch-order/detail/${result?.result?.id}`);
    } catch (err) {}
    // console.log(formDataCreateDispatch);
  };

  return (
    <>
      <Item className="p-0" col="col-12" spaceBottom={3}>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-0">Pilih Logistik</h6>
        </div>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className={classes.description}>
            Silahkan pilih kurir yang diinginkan agar order yang dikirim akan
            sampai ke pelanggan dengan cepat. Perkiraan sampai produk terhitung
            sejak transaksi selesai dilakukan.
          </h6>
          <LogisticSelect
            onSelect={handleSelectVendor}
            options={listVendor.map((item) => ({
              ...item,
              logo: item.logo_full_url,
            }))}
          />
        </div>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="mb-3">Layanan Logistik</h6>
          <Row>
            {listServiceVendor.length && listVendor.length && state.vendor
              ? listServiceVendor.map((item) => renderItemServiceLogistic(item))
              : renderEmptyService()}
          </Row>
        </div>
        <div className="p-0 w-100 d-flex flex-column flex-lg-row">
          <div
            className="p-3 w-100 w-lg-50 d-flex align-items-center"
            style={{ borderRight: '1px solid #E8E8E8' }}
          >
            <h6 className={`${classes.label} mb-0`}>
              <Checkbox
                color=""
                style={{ color: '#192A55' }}
                onChange={handleChangeInsurance}
              />{' '}
              Tambahkan Asuransi
            </h6>
            <h6 className={`${classes.value} mb-0 ml-auto`}>Rp.1000</h6>
          </div>
          <div className="p-3 w-100 w-lg-50 d-flex align-items-center">
            <h6 className={`${classes.label} m-0`}>
              Total Pembayaran Logistik
            </h6>
            <h6 className={`${classes.value} mb-0 ml-auto`}>Rp.1000</h6>
          </div>
        </div>
      </Item>
      <Item className="p-3" col="col-12" spaceBottom={3}>
        <h6>Total Pembayaran</h6>
        <Row>
          <Col xs={6}>
            <h6 className={classes.label}>Total pembayaran Logistik</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.label}>Biaya Packaging</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.total}>Total Pembayaran</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.total} style={{ textAlign: 'right' }}>
              {formatRupiah(3000)}
            </h6>
          </Col>
        </Row>
      </Item>
      <div className="ml-auto">
        <Button
          style={{ width: '140px' }}
          styleType="lightBlueFill"
          text="Batal"
          onClick={() => handleChangeTab(2)}
        />
        <Button
          style={{ minWidth: '140px', marginLeft: '10px' }}
          styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
          text={isLoading ? 'Loading...' : 'Simpan'}
          onClick={handleSubmit}
        />
      </div>
    </>
  );
}

export default FormLogisticDispatchOrderRegular;
