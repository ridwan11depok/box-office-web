import React from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import BackButton from '../../../../components/elements/BackButton';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
  nameKurir: {
    fontFamily: 'Open Sans',
    fontWeight: '600',
    fontSize: '14px',
    color: '#1c1c1c',
  },
  duration: {
    fontFamily: 'Open Sans',
    fontWeight: '600',
    fontSize: '12px',
    color: '#4f4f4f',
  },
  price: {
    fontFamily: 'Open Sans',
    fontWeight: '700',
    fontSize: '14px',
    color: '#192A55',
  },
});

function DropshipDetailRegular(props) {
  const { data } = props;
  const history = useHistory();
  const classes = useStyles();
  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label="0101010101"
            onClick={() => history.push('/transactions')}
          />
        </div>
      )}
    >
      <ContentItem
        spaceRight="0 pr-md-1"
        col="col-12 col-md-6"
        spaceBottom="3"
        className="p-3"
      >
        <h6>Data Pengirim</h6>
        <div className="mt-3">
          <h6 className={classes.label}>Nama Pengirim</h6>
          <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Nomor Telepon Pelanggan</h6>
          <h6 className={classes.value}>{data?.customerPhone || 'No data'}</h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Alamat</h6>
          <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
        </div>
      </ContentItem>

      <ContentItem
        spaceLeft="0 pl-md-1"
        col="col-12 col-md-6"
        spaceBottom="3"
        className="p-3"
      >
        <h6>Data Penerima</h6>
        <div className="mt-3">
          <h6 className={classes.label}>Nama Penerima</h6>
          <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Nomor Telepon Penerima</h6>
          <h6 className={classes.value}>{data?.customerPhone || 'No data'}</h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Alamat</h6>
          <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
        </div>
      </ContentItem>

      <ContentItem spaceBottom={3} className="p-3">
        <h6>Detail Paket</h6>
        <Row>
          <Col md={6}>
            <div className="mt-2">
              <h6 className={classes.label}>Kategori Produk</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Panjang</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Tinggi</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
          </Col>
          <Col md={6}>
            <div className="mt-2">
              <h6 className={classes.label}>Sub Kategori Produk</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Lebar</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Berat</h6>
              <h6 className={classes.value}>{data?.sender || 'No data'}</h6>
            </div>
          </Col>
        </Row>
      </ContentItem>

      <ContentItem className="p-3">
        <h6>Kurir Logistik</h6>
        <div className="d-flex flex-row justify-content-between align-items-center">
          <img
            style={{ height: '30px' }}
            src="https://s3-alpha-sig.figma.com/img/d1bb/c237/61dd1c3e9ab89f07c9d686c1e697af38?Expires=1629676800&Signature=Vl2UKhakwTRmwHe9GIljqXvs1IyO~zBLT-dDZMYDks-LqKn2QgUt0kX~ezK1rPkkiDwzuEIDpBljmsadZg7BceWpL0l0~JVajU8zWEOii6BpjlRTScp5729wZ9xygKc7J1tmzHQZULZTEM-OLDKqNN4TFOOVZ9nMCjI9I6kTe-cyr1ZL0FeVeZQaV0njtjNJ3cAjYQGHfdRv7ztXZc0Mpq9WYQ9aTkaqTuRJoKzrBL4VM-~6jPITkZpb4mt1Um5O984rMdVefvY8xcoXznHICd-kj~1a~abCQoBUYHCT1qbwP2sv44XX15fFxT-f6uAXApOeTblpgoqa9qo~C2tBxg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
            alt=""
          />
          <div className="ml-3">
            <h6 className={classes.nameKurir}>JNE-CTC</h6>
            <h6 className={classes.duration}>1-2 hari</h6>
          </div>
          <div
            style={{ flex: 1 }}
            className="d-flex justify-content-end justify-content-md-end align-items-center"
          >
            <h6 className={classes.price}>Rp 8.000</h6>
          </div>
        </div>
      </ContentItem>
    </ContentContainer>
  );
}

DropshipDetailRegular.defaultProps = {
  data: {},
};

export default DropshipDetailRegular;
