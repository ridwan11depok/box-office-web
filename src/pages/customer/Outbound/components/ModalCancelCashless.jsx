import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { TextArea } from '../../../../components/elements/InputField';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { cancelCashless } from '../reduxAction';
import { useHistory } from 'react-router-dom';

const validationSchema = Yup.object({
  reason: Yup.string().required(
    'Harap untuk mengisi alasan membatalkan dispatch order'
  ),
});

const ModalCancelCashless = (props) => {
  const [reason, setReason] = useState('');
  const dispatch = useDispatch();
  const history = useHistory();
  const { detailStore } = useSelector((state) => state.storeAdministrator);
  const { isLoading } = useSelector((state) => state.loading);

  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  const handleSubmit = async (values) => {
    try {
      await dispatch(cancelCashless(values, props.data?.id));
      props.onHide();
      history.push('/outbound/cashless');
    } catch (err) {}
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Batal Transaksi"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={detailStore}
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <TextArea
            label="Alasan Membatalkan Transaksi Dispatch Order"
            placeholder="Input alasan membatalkan transaksi dispatch order"
            formik={formik}
            withValidate
            name="reason"
          />
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text={isLoading ? 'Loading...' : 'Batal Transaksi'}
              onClick={formik.handleSubmit}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

ModalCancelCashless.defaultProps = {
  data: {},
};
export default ModalCancelCashless;
