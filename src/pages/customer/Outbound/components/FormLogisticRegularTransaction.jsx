import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import { LogisticSelect } from '../../../../components/elements/InputField';
import Checkbox from '@material-ui/core/Checkbox';
import { Row, Col } from 'react-bootstrap';
import { formatRupiah } from '../../../../utils/text';
import Radio from '@material-ui/core/Radio';
import Button from '../../../../components/elements/Button';
import { getListServiceVendor } from '../reduxAction';
import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#363636',
    textAlign: 'right',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  total: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '15px 0',
  },
  duration: {
    fontSize: '12px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4F4F4F',
  },
});

function FormLogisticRegularTransaction(props) {
  const { handleChangeTab, formik } = props;
  const [state, setState] = useState({
    vendorService: null,
  });
  const dispatch = useDispatch();
  const classes = useStyles();
  const { isLoading } = useSelector((state) => state.loading);
  const { listVendor, listServiceVendor } = useSelector(
    (state) => state.outboundCustomer
  );
  const renderItemServiceLogistic = (item) => (
    <Col xs={12} md={6} lg={4}>
      <div
        className="d-flex p-2 align-items-center rounded mb-3"
        style={{ border: '1px solid #E8E8E8' }}
      >
        <Radio
          color=""
          style={{ color: '#192A55' }}
          onChange={(e) => handleSelectVendorService(item)}
          checked={state.vendorService?.id === item?.id}
        />
        <div>
          <h6 className={classes.label}>{item?.code}</h6>
          <h6 className={classes.duration}>2 hari</h6>
        </div>
        <div className="ml-auto">
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </div>
    </Col>
  );

  const renderEmptyService = () => {
    return (
      <div
        className="d-flex flex-column align-items-center justify-content-center w-100"
        style={{ height: '30vh' }}
      >
        <h6 className={classes.description}>
          Tidak ada layanan logistik saat ini. Silahkan pilih logistik terlebih
          dahulu
        </h6>
      </div>
    );
  };
  const handleSelectVendor = (selected) => {
    formik.setFieldValue('vendor_id', selected?.id);
    dispatch(getListServiceVendor(selected?.id));
  };
  const handleSelectVendorService = (selected) => {
    formik.setFieldValue('vendor_service_id', selected?.id);
    setState({ ...state, vendorService: selected });
  };
  const handleChangeInsurance = (e) => {
    const isChecked = e.target.checked ? '1' : '0';
    formik.setFieldValue('is_insurance', isChecked);
  };
  return (
    <>
      <Item className="p-0" col="col-12" spaceBottom={3}>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-0">Pilih Logistik</h6>
        </div>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className={classes.description}>
            Silahkan pilih kurir yang diinginkan agar order yang dikirim akan
            sampai ke pelanggan dengan cepat. Perkiraan sampai produk terhitung
            sejak transaksi selesai dilakukan.
          </h6>
          <LogisticSelect
            onSelect={handleSelectVendor}
            options={listVendor.map((item) => ({
              ...item,
              logo: item.logo_full_url,
            }))}
          />
        </div>
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="mb-3">Layanan Logistik</h6>
          <Row>
            {listServiceVendor.length
              ? listServiceVendor.map((item) => renderItemServiceLogistic(item))
              : renderEmptyService()}
          </Row>
        </div>
        <div className="p-0 w-100 d-flex flex-column flex-lg-row">
          <div
            className="p-3 w-100 w-lg-50 d-flex align-items-center"
            style={{ borderRight: '1px solid #E8E8E8' }}
          >
            <h6 className={`${classes.label} mb-0`}>
              <Checkbox
                color=""
                style={{ color: '#192A55' }}
                onChange={handleChangeInsurance}
              />{' '}
              Tambahkan Asuransi
            </h6>
          </div>
          <div className="p-3 w-100 w-lg-50 d-flex align-items-center">
            <h6 className={`${classes.label} m-0`}>
              Total Pembayaran Logistik
            </h6>
            <h6 className={`${classes.value} mb-0 ml-auto`}>Rp.1000</h6>
          </div>
        </div>
      </Item>
      <Item className="p-3" col="col-12" spaceBottom={3}>
        <h6>Total Pembayaran</h6>
        <Row>
          <Col xs={6}>
            <h6 className={classes.label}>Total pembayaran Logistik</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.label}>Biaya Packaging</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.total}>Total Pembayaran</h6>
          </Col>
          <Col xs={6}>
            <h6 className={classes.total} style={{ textAlign: 'right' }}>
              {formatRupiah(3000)}
            </h6>
          </Col>
        </Row>
      </Item>
    </>
  );
}

export default FormLogisticRegularTransaction;
