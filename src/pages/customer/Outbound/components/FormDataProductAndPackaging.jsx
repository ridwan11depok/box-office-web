import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Table } from '../../../../components/elements/Table';
import AddIcon from '@material-ui/icons/Add';
import ModalAddProduct from './ModalAddProduct';
import ModalAddPackaging from './ModalAddPackaging';
import Counter from '../../../../components/elements/Counter';
import { formatRupiah } from '../../../../utils/text';
import { createDispatchOrder, createCashless, setDataFormCreateDispatch } from '../reduxAction';
import { useHistory } from 'react-router-dom';
import Toast from '../../../../components/elements/Toast';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
  link: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#192A55',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const ActionColumn = ({ data, handleDelete }) => {
  return (
    <div
      className="p-1 rounded button"
      style={{ border: '2px solid #192A55', width: 'min-content' }}
      onClick={() => handleDelete(data.id)}
    >
      <i
        className="far fa-trash-alt"
        style={{ fontSize: '20px', color: '#192A55', cursor: 'pointer' }}
      ></i>
    </div>
  );
};

const QuantityColumn = ({ data, handleCounter }) => {
  return (
    <div className="d-flex">
      <Counter
        onDecrease={() => handleCounter(data, '-')}
        onIncrease={() => handleCounter(data, '+')}
        value={data.qty}
      />
    </div>
  );
};

function FormDataProductAndPackaging({ handleChangeTab }) {
  const [state, setState] = useState({
    showModalAddProduct: false,
    showModalAddPackaging: false,
    provice_id: 0,
    city_id: 0,
    invoiceImage: '',
    dataProductSelected: [],
    isErrorProduct: false,
    isErrorPackaging: false,
  });
  const { formDataCreateDispatch, selectedStorage } = useSelector(
    (state) => state.outboundCustomer
  );
  const [selectedProduct, setSelectedProduct] = useState(formDataCreateDispatch?.selectedProduct || []);
  const [selectedPackaging, setSelectedPackaging] = useState(formDataCreateDispatch?.selectedPackaging || []);
  const classes = useStyles();

  const dispatch = useDispatch();
  const history = useHistory();

  const { dataUser } = useSelector((state) => state.login);
  const { isLoading } = useSelector((state) => state.loading);

  const handleSelectProduct = (products = []) => {
    let newSelected = [];
    products.forEach((product) => {
      // const checkProduct = selectedProduct.map(
      //   (item) => item.id === product.id
      // ).length;
      newSelected = [...newSelected, { ...product, qty: 1 }];
    });
    setSelectedProduct(newSelected);
    dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedProduct: newSelected}));
  };

  const handleCounter = (product, action = '+') => {
    if (action === '+' && product.qty >= 0) {
      const newSelectedProduct = selectedProduct.map((item) => {
        if (item.id === product.id) {
          return {
            ...item,
            qty: product.qty + 1,
          };
        } else {
          return item;
        }
      });
      dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedProduct: newSelectedProduct}));
      setSelectedProduct(newSelectedProduct);
    } else if (action === '-' && product.qty > 0) {
      const newSelectedProduct = selectedProduct.map((item) => {
        if (item.id === product.id) {
          return {
            ...item,
            qty: product.qty - 1,
          };
        } else {
          return item;
        }
      });
      dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedProduct: newSelectedProduct}));
      setSelectedProduct(newSelectedProduct);
    }
  };

  const handleDeleteProduct = (productId) => {
    const newSelectedProduct = selectedProduct.filter(
      (item) => item.id !== productId
    );
    setSelectedProduct(newSelectedProduct);
    dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedProduct: newSelectedProduct}));
  };

  const handleSelectPackaging = (packaging = []) => {
    let newSelected = [];
    packaging.forEach((packageItem) => {
      // const checkPackaging = selectedPackaging.filter(
      //   (item) => item.id === packageItem.id
      // ).length;
      // if (!Boolean(checkPackaging)) {
      newSelected = [...newSelected, { ...packageItem, qty: 1 }];
      // }
    });
    setSelectedPackaging(newSelected);
    dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedPackaging: newSelected}));
  };

  const handleCounterPackaging = (packageItem, action = '+') => {
    if (action === '+' && packageItem.qty >= 0) {
      const newSelectedPackaging = selectedPackaging.map((item) => {
        if (item.id === packageItem.id) {
          return {
            ...item,
            qty: packageItem.qty + 1,
          };
        } else {
          return item;
        }
      });
      setSelectedPackaging(newSelectedPackaging);
      dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedPackaging: newSelectedPackaging}));
    } else if (action === '-' && packageItem.qty > 0) {
      const newSelectedPackaging = selectedPackaging.map((item) => {
        if (item.id === packageItem.id) {
          return {
            ...item,
            qty: packageItem.qty - 1,
          };
        } else {
          return item;
        }
      });
      setSelectedPackaging(newSelectedPackaging);
      dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedPackaging: newSelectedPackaging}));
    }
  };

  const handleDeletePackaging = (packageId) => {
    const newSelectedPackaging = selectedPackaging.filter(
      (item) => item.id !== packageId
    );
    setSelectedPackaging(newSelectedPackaging);
    dispatch(setDataFormCreateDispatch({...formDataCreateDispatch, selectedPackaging: newSelectedPackaging}));
  };

  const handleSubmit = async () => {
    const data = new FormData();
    data.append('store_id', dataUser?.store?.[0]?.id);
    data.append('storage_id', selectedStorage?.id);
    data.append('sender_name', formDataCreateDispatch?.sender_name);
    data.append(
      'sender_country_code',
      formDataCreateDispatch?.sender_country_code
    );
    data.append('sender_phone', formDataCreateDispatch?.sender_phone);
    // data.append('sender_note', formDataCreateDispatch?.sender_note);
    data.append('show_logo', 1);
    data.append('receiver_name', formDataCreateDispatch?.receiver_name);
    data.append(
      'receiver_country_code',
      formDataCreateDispatch?.receiver_country_code
    );
    data.append('receiver_phone', formDataCreateDispatch?.receiver_phone);
    data.append('receiver_address', formDataCreateDispatch?.receiver_address);
    data.append(
      'receiver_province_id',
      formDataCreateDispatch?.receiver_province_id
    );
    data.append('receiver_city_id', formDataCreateDispatch?.receiver_city_id);
    data.append(
      'receiver_district_id',
      formDataCreateDispatch?.receiver_district_id
    );
    data.append('vendor_id', formDataCreateDispatch?.vendor_id);
    data.append('vendor_service_id', formDataCreateDispatch?.vendor_service_id);
    data.append('invoice_number', formDataCreateDispatch?.invoice_number);
    data.append('invoice_file', formDataCreateDispatch?.invoice_file);
    data.append('message', formDataCreateDispatch?.message);
    data.append('awb_number', formDataCreateDispatch?.awb_number);
    data.append('platform', formDataCreateDispatch?.platform);
    formDataCreateDispatch?.selectedProduct.forEach((item, idx) =>
      data.append(`product[${idx}][id]`, item?.id)
    );
    formDataCreateDispatch?.selectedProduct.forEach((item, idx) =>
      data.append(`product[${idx}][quantity]`, item?.qty)
    );
    formDataCreateDispatch?.selectedPackaging.forEach((item, idx) =>
      data.append(`packaging[${idx}][id]`, item?.id)
    );
    formDataCreateDispatch?.selectedPackaging.forEach((item, idx) =>
      data.append(`packaging[${idx}][quantity]`, item?.qty)
    );
    try {
      const result = await dispatch(createCashless(data));
      history.push(`/outbound/cashless/detail/${result?.result?.id}`);
      dispatch(setDataFormCreateDispatch({}))
    } catch (err) { }
  };

  const handleSubmitAll = () => {
    // setState({ ...state, isErrorPackaging: false, isErrorProduct: false });
    if (formDataCreateDispatch?.selectedPackaging?.length === 0 && formDataCreateDispatch?.length === 0) {
      setState({ ...state, isErrorPackaging: true, isErrorProduct: true });
    } else if (formDataCreateDispatch?.selectedPackaging?.length === 0) {
      setState({ ...state, isErrorPackaging: true });
    } else if (formDataCreateDispatch?.length === 0) {
      setState({ ...state, isErrorProduct: true });
    } else if (
      formDataCreateDispatch?.selectedPackaging.filter((item) => item.qty === 0).length &&
      formDataCreateDispatch.filter((item) => item.qty === 0).length
    ) {
      Toast(
        {
          type: 'error',
          message:
            'Kuantitas item produk dan item packaging minimal berjumlah 1',
        },
        { autoClose: 5000 }
      );
    } else if (formDataCreateDispatch?.selectedPackaging.filter((item) => item.qty === 0).length) {
      Toast(
        {
          type: 'error',
          message: 'Kuantitas item packaging minimal berjumlah 1',
        },
        { autoClose: 5000 }
      );
    } else if (formDataCreateDispatch?.selectedProduct.filter((item) => item.qty === 0).length) {
      Toast(
        { type: 'error', message: 'Kuantitas item produk minimal berjumlah 1' },
        { autoClose: 5000 }
      );
    } else {
      handleSubmit();
    }
  };
  return (
    <Container classname="w-100">
      <Item className="p-0" spaceRight="0" col="col-12" spaceBottom={3}>
        <Table
          renderHeader={() => (
            <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
              <h6 className={classes.titleTable}>Produk Dikirim</h6>
              <Button
                startIcon={() => <AddIcon />}
                onClick={() =>
                  setState({ ...state, showModalAddProduct: true })
                }
                text="Tambah Produk"
                styleType="blueFill"
              />
            </div>
          )}
          column={[
            {
              heading: 'SKU',
              key: 'sku',
            },
            {
              heading: 'Produk',
              key: 'name',
            },
            {
              heading: 'Ukuran',
              key: 'size',
            },
            {
              heading: 'Qty',
              render: (item) => (
                <QuantityColumn data={item} handleCounter={handleCounter} />
              ),
            },
            {
              heading: 'Aksi',
              render: (item) => (
                <ActionColumn data={item} handleDelete={handleDeleteProduct} />
              ),
            },
          ]}
          data={formDataCreateDispatch?.selectedProduct || selectedProduct}
          transformData={(item) => ({
            ...item,
            sku: item?.product?.sku,
            name: item?.product?.name,
            // size: `${item?.product?.length}x${item?.product?.width}x${item?.product?.height}`,
            size: `${item?.product?.length || 0} cm x ${item?.product?.width || 0
              } cm ${item?.product?.height ? `x ${item?.product?.height} cm` : ''}`,
          })}
          showBorderContainer={false}
          showFooter={false}
          withNumber={false}
          renderEmptyData={() => (
            <div
              style={{ height: '100px' }}
              className="d-flex justify-content-center align-items-center"
            >
              <h6
                className={classes.description}
                style={{ color: state.isErrorProduct && 'red' }}
              >
                Tidak ada produk yang ditambahkan saat ini
              </h6>
            </div>
          )}
        />
      </Item>

      <Item className="p-0" spaceRight="0" col="col-12" spaceBottom={3}>
        <Table
          renderHeader={() => (
            <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
              <h6 className={classes.titleTable}>Packaging</h6>
              <Button
                startIcon={() => <AddIcon />}
                onClick={() =>
                  setState({ ...state, showModalAddPackaging: true })
                }
                text="Tambah Packaging"
                styleType="blueFill"
              />
            </div>
          )}
          column={[
            {
              heading: 'Nama Packaging',
              key: 'name',
            },
            {
              heading: 'Ukuran',
              key: 'size',
            },
            {
              heading: 'Nilai Packaging',
              key: 'value',
            },
            {
              heading: 'Qty',
              render: (item) => (
                <QuantityColumn
                  data={item}
                  handleCounter={handleCounterPackaging}
                />
              ),
            },
            {
              heading: 'Aksi',
              render: (item) => (
                <ActionColumn
                  data={item}
                  handleDelete={handleDeletePackaging}
                />
              ),
            },
          ]}
          data={formDataCreateDispatch?.selectedPackaging || selectedPackaging}
          transformData={(item) => ({
            ...item,
            name: item?.name,
            size: `${item?.length || 0} cm x ${item?.width || 0} cm ${item?.height ? `x ${item?.height} cm` : ''
              }`,
            value: formatRupiah(item?.price ? item?.price * item.qty : 0),
          })}
          showBorderContainer={false}
          showFooter={false}
          withNumber={false}
          renderEmptyData={() => (
            <div
              style={{ height: '100px' }}
              className="d-flex justify-content-center align-items-center"
            >
              <h6
                className={classes.description}
                style={{ color: state.isErrorPackaging && 'red' }}
              >
                Tidak ada packaging yang ditambahkan saat ini
              </h6>
            </div>
          )}
        />
      </Item>
      <div className="ml-auto">
        <Button
          style={{ width: '140px' }}
          styleType="lightBlueFill"
          text="Batal"
          onClick={() => handleChangeTab(2)}
        />
        <Button
          style={{ minWidth: '140px', marginLeft: '10px' }}
          styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
          text={isLoading ? 'Loading...' : 'Simpan'}
          onClick={handleSubmitAll}
        />
      </div>

      <ModalAddProduct
        show={state.showModalAddProduct}
        onHide={() => setState({ ...state, showModalAddProduct: false })}
        onAgree={handleSelectProduct}
        initialValues={[]}
      />
      <ModalAddPackaging
        show={state.showModalAddPackaging}
        onHide={() => setState({ ...state, showModalAddPackaging: false })}
        onAgree={handleSelectPackaging}
        initialValues={[]}
      />
    </Container>
  );
}

export default FormDataProductAndPackaging;
