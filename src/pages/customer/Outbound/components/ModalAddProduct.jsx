import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { SearchField } from '../../../../components/elements/InputField';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Checkbox from '@material-ui/core/Checkbox';
import { useSelector } from 'react-redux';
import Banner from '../../../../components/elements/Banner';

const ModalAddProduct = (props) => {
  const [selected, setSelected] = useState([]);

  const { listProductStorage } = useSelector((state) => state.outboundCustomer);

  const onChange = (e, productItem) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      setSelected([...selected, productItem]);
    } else {
      let newSelected = selected.filter((item) => item.id !== productItem.id);
      setSelected(newSelected);
    }
  };
  const handleDelete = (product) => {
    let newSelected = selected.filter((item) => item.id !== product.id);
    setSelected(newSelected);
  };
  // const { listAllProductSku } = useSelector((state) => state.inbound);
  const [products, setProducts] = useState(listProductStorage);

  const handleSearch = (e) => {
    let value = e.target.value;
    let filtered = listProductStorage.filter((item) =>
      item?.product?.name.toLowerCase().includes(value.toLowerCase())
    );
    setProducts(filtered);
  };

  useEffect(() => {
    if (!props.show) {
      setSelected([]);
    } else if (props.show) {
      setSelected(props.initialValues);
    }
  }, [props.show]);

  const handleSelect = () => {
    props.onAgree(selected);
    props.onHide();
    setSelected([])
  };
  return (
    <>
      <Modal
        titleModal="Tambah Produk"
        bodyClassName="pb-3 pt-0"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        scrollable
        content={(data) => (
          <>
            <Container withHeader={false}>
              <Item spaceBottom={3}>
                <div
                  className="p-2"
                  style={{
                    borderBottom: '1px solid #E8E8E8',
                    position: 'sticky',
                    top: '0px',
                    backgroundColor: '#FFFFFF',
                    zIndex: '1050',
                  }}
                >
                  <Banner
                    styleType="blue"
                    description="Silahkan pilih produk yang ingin anda kirimkan sesuai dengan invoice yang telah anda upload"
                    button={{ isShow: false }}
                    className="mb-3"
                  />
                  <SearchField
                    onChange={handleSearch}
                    placeholder="Cari produk"
                  />
                </div>
                <div>
                  <table className="table table-striped mt-0 mb-5">
                    <tr
                      className="pt-0 pb-0"
                      style={{
                        borderBottom: '1px solid #E8E8E8',
                      }}
                    >
                      <th scope="col">Nama Produk</th>
                    </tr>
                    <tbody>
                      {products.map((item, idx) => (
                        <tr
                          key={idx.toString()}
                          style={{ borderBottom: '1px solid #E8E8E8' }}
                        >
                          <td>
                            <Checkbox
                              color=""
                              style={{ color: '#192A55' }}
                              checked={
                                selected.filter((prod) => prod.id === item.id)
                                  .length
                              }
                              onChange={(e) => onChange(e, item)}
                            />
                            {item?.product?.name}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                  {!products.length && (
                    <div
                      style={{
                        height: '100px',
                        fontSize: '14px',
                        color: '#1C1C1C',
                        fontFamily: 'Open Sans',
                      }}
                      className="d-flex justify-content-center align-items-center"
                    >
                      Hasil pencarian produk tidak ditemukan
                    </div>
                  )}
                </div>
              </Item>
            </Container>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType={selected.length ? 'blueFill' : 'blueFillDisabled'}
              text="Pilih Product"
              onClick={handleSelect}
              disabled={!Boolean(selected.length)}
            />
          </div>
        )}
      />
    </>
  );
};

export default ModalAddProduct;
