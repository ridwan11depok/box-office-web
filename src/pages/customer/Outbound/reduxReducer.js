import actionType from './reduxConstant';

const initialState = {
  pageDropshipDetail: {},
  dispatchOrder: {},
  detailDispatchOrder: {},
  listVendor: [],
  listServiceVendor: [],
  selectedStorage: null,
  listProductStorage: [],
  listPackaging: [],
  formDataCreateDispatch: {},
  regularTransaction: {},
  detailRegularTransaction: {},
  formDataCreateRegularTransaction: {},
  listCourier: null,
  cashless: {},
  detailCashless: {},
};

const transactionsReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.SET_DATA_PAGE_DROPSHIP_DETAIL:
      return {
        ...prevState,
        pageDropshipDetail: action.payload,
      };
    case actionType.DISPATCH_ORDER_CUSTOMER:
      return {
        ...prevState,
        dispatchOrder: action.payload,
      };
    case actionType.DETAIL_DISPATCH_ORDER_CUSTOMER:
      return {
        ...prevState,
        detailDispatchOrder: action.payload,
      };
    case actionType.LIST_VENDOR_CUSTOMER: {
      const vendors = action.payload.map((vendor) => ({
        ...vendor,
        value: vendor.id,
        label: vendor.name,
      }));
      return {
        ...prevState,
        listVendor: vendors,
      };
    }
    case actionType.LIST_SERVICE_VENDOR_CUSTOMER: {
      const services = action.payload.map((service) => ({
        ...service,
        value: service.id,
        label: service.service,
      }));
      return {
        ...prevState,
        listServiceVendor: services,
      };
    }
    case actionType.SELECTED_STORAGE_CUSTOMER:
      return {
        ...prevState,
        selectedStorage: action.payload,
      };
    case actionType.LIST_PRODUCT_STORAGE_CUSTOMER:
      return {
        ...prevState,
        listProductStorage: action.payload,
      };
    case actionType.LIST_PACKAGING_CUSTOMER:
      return {
        ...prevState,
        listPackaging: action.payload,
      };
    case actionType.FORM_DATA_CREATE_DISPATCH_ORDER_CUSTOMER:
      if (Object.keys(action.payload).length === 0) {
        return {
          ...prevState,
          formDataCreateDispatch: {}
        }
      }
      else {
        return {
          ...prevState,
          formDataCreateDispatch: {
            ...prevState.formDataCreateDispatch,
            ...action.payload,
          },
        };
      }
    case actionType.REGULAR_TRANSACTION_CUSTOMER:
      return {
        ...prevState,
        regularTransaction: action.payload,
      };
    case actionType.DETAIL_REGULAR_TRANSACTION_CUSTOMER:
      return {
        ...prevState,
        detailRegularTransaction: action.payload,
      };
    case actionType.FORM_DATA_CREATE_REGULAR_TRANSACTION_CUSTOMER:
      return {
        ...prevState,
        formDataCreateRegularTransaction: {
          ...prevState.formDataCreateRegularTransaction,
          ...action.payload,
        },
      };
    case actionType.LIST_COURIER_CUSTOMER:
      return {
        ...prevState,
        listCourier: action.payload,
      };
    case actionType.CASHLESS_CUSTOMER:
      return {
        ...prevState,
        cashless: action.payload,
      };
    case actionType.DETAIL_CASHLESS_CUSTOMER:
      return {
        ...prevState,
        detailCashless: action.payload,
      };
    default:
      return prevState;
  }
};

export default transactionsReducer;
