import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import TableProductSKU from './components/TableProductSKU';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInvoice from './components/FormInvoice';
import FormDataPengirim from './components/FormDataPengirim';
import FormDataPenerima from './components/FormDataPenerima';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { getListAllProductSKU } from '../Inbound/reduxAction';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);
const validationSchema = Yup.object({
  sender_name: Yup.string().required('Harap untuk mengisi nama pengirim'),
  sender_country_code: Yup.string().required('Required'),
  sender_phone: Yup.string()
    .matches(phoneRegex, 'Nomor telepon tidak valid')
    .required('Harap untuk mengisi nomor telepon pengirim'),
  receiver_name: Yup.string().required('Harap untuk mengisi nama penerima'),
  receiver_country_code: Yup.string().required('Required'),
  receiver_phone: Yup.string()
    .matches(phoneRegex, 'Nomor telepon tidak valid')
    .required('Harap untuk mengisi nomor telepon penerima'),
  receiver_address: Yup.string().required('Harap untuk mengisi lokasi'),
  receiver_province_id: Yup.number().required('Harap untuk mengisi provinsi'),
  receiver_city_id: Yup.number().required('Harap untuk mengisi kota'),
  invoice_number: Yup.number()
    .typeError('Isian harus angka')
    .required('Harap untuk mengisi nomor invoice'),
  invoice_file: Yup.mixed().required('Harap melampirkan gambar invoice'),
  awb_number: Yup.string().required('Harap untuk mengisi AWB'),
  vendor_id: Yup.number().required('Harap untuk memilih logistik'),
  vendor_service_id: Yup.number().required('Harap untuk memilih service'),
});

const Transactions = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const { dataUser } = useSelector((state) => state.login);
  const formik = useFormik({
    initialValues: {
      sender_name: '',
      sender_country_code: '62',
      sender_phone: '',
      sender_note: '',
      show_logo: 0,

      receiver_name: '',
      receiver_country_code: '62',
      receiver_phone: '',
      receiver_address: '',
      receiver_province_id: '',
      receiver_city_id: '',

      invoice_number: '',
      invoice_file: '',
      message: '',
      awb_number: '',
      product: [],
      vendor_id: '',
      vendor_service_id: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // handleSubmit(values)
      console.log(values);
    },
  });

  useEffect(() => {
    dispatch(getListAllProductSKU());
  }, []);

  const handleSubmit = (values) => {
    const data = new FormData();
    data.append('store_id', dataUser?.store?.[0]?.id);
    data.append('sender_name', values.sender_name);
    data.append('sender_country_code', values.sender_country_code);
    data.append('sender_phone', values.sender_phone);
    data.append('sender_note', values.sender_note);
    data.append('show_logo', values.show_logo);

    data.append('receiver_name', values.receiver_name);
    data.append('receiver_country_code', values.receiver_country_code);
    data.append('receiver_phone', values.receiver_phone);
    data.append('receiver_address', values.receiver_address);
    data.append('receiver_province_id', values.receiver_province_id);
    data.append('receiver_city_id', values.receiver_city_id);

    data.append('invoice_number', values.invoice_number);
    data.append('invoice_file', values.invoice_file);
    data.append('message', values.message);
    data.append('awb_number', values.awb_number);
    data.append('vendor_id', values.vendor_id);
    data.append('vendor_service_id', values.vendor_service_id);
  };
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Dispatch Order"
              onClick={() => history.push('/outbound/dispatch-order')}
            />
          </div>
        )}
      >
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <FormInvoice formik={formik} />
        </ContentItem>
        <ContentItem
          border={false}
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            minHeight: '615px',
          }}
        >
          <div
            className="p-3 mb-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPengirim formik={formik} />
          </div>
          <div
            className="p-3"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '4px',
            }}
          >
            <FormDataPenerima formik={formik} />
          </div>
        </ContentItem>
        <ContentItem
          className="mt-3"
          border={false}
          col="col-12"
          spaceBottom={3}
        >
          <TableProductSKU formik={formik} />
        </ContentItem>
        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text="Kirim"
            onClick={formik.handleSubmit}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default Transactions;
