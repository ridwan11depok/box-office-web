import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import BackButton from '../../../../components/elements/BackButton';
import { TabHeader, TabContent } from '../../../../components/elements/Tabs';
import '../styles.css';
import FormLogisticDispatchOrderRegular from '../components/FormLogisticDispatchOrderRegular';
import FormSenderAndReceiverDispatchOrderRegular from '../components/FormSenderAndReceiverDispatchOrderRegular';
import FormDataProductAndPackaging from '../components/FormDataProductAndPackagingDispatchOrderRegular';
import {
  getListPackaging,
  getListProductStorage,
  getListVendor,
} from '../reduxAction';

const itemTab = (props, item) => {
  const { activeColor, activeTab, inActiveColor } = props;
  const { tab, title } = item;

  return (
    <div
      className="w-100"
      style={{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        fontWeight: '600',
        color:
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF',
        textAlign: 'left',
        borderBottom: `8px solid ${
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF'
        }`,
      }}
    >
      <h6>
        <span
          style={{
            borderRadius: '100%',
            backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
            width: '24px',
            height: '24px',
            padding: '3px 5px',
          }}
        >
          {tab < activeTab ? (
            <i className="fa fa-check" aria-hidden="true"></i>
          ) : (
            `0${tab}`
          )}
        </span>{' '}
        {title}
      </h6>
    </div>
  );
};

const Transactions = () => {
  const history = useHistory();
  const [step, setStep] = useState(1);
  const handleChangeTab = (tab) => setStep(tab);

  const dispatch = useDispatch();
  const { selectedStorage } = useSelector((state) => state.outboundCustomer);
  useEffect(() => {
    if (selectedStorage) {
      dispatch(getListProductStorage(selectedStorage?.id));
      dispatch(getListPackaging(selectedStorage?.warehouse?.id));
      dispatch(getListVendor());
    } else {
      history.push('/outbound/dispatch-order');
    }
  }, []);
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Dispatch Order"
              onClick={() => history.push('/outbound/dispatch-order')}
            />
          </div>
        )}
      >
        <ContentItem border={false} spaceBottom={3}>
          <TabHeader
            border={false}
            activeTab={step}
            activeBackground="transparent"
            inActiveBackground="transparent"
            activeColor="#828282"
            inActiveColor="#192a55"
            listHeader={[
              {
                tab: 1,
                title: 'Data Pengirim dan Penerima',
                render: itemTab,
              },
              {
                tab: 2,
                title: 'Data Produk dan Packaging',
                render: itemTab,
              },
              {
                tab: 3,
                title: 'Data Logistik',
                render: itemTab,
              },
            ]}
            isClickable={false}
          />
        </ContentItem>
        <TabContent tab={1} activeTab={step}>
          <FormSenderAndReceiverDispatchOrderRegular
            handleChangeTab={handleChangeTab}
          />
        </TabContent>
        <TabContent tab={2} activeTab={step}>
          <FormDataProductAndPackaging handleChangeTab={handleChangeTab} />
        </TabContent>

        <TabContent tab={3} activeTab={step}>
          <FormLogisticDispatchOrderRegular handleChangeTab={handleChangeTab} />
        </TabContent>
      </ContentContainer>
    </>
  );
};

export default Transactions;
