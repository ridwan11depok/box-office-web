import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import axios from 'axios';
import baseUrl from '../../../api/url';

export const setDataPageDropshipDetail = (data) => {
  return {
    type: actionType.SET_DATA_PAGE_DROPSHIP_DETAIL,
    payload: data,
  };
};

export const setSelectedStorage = (storage) => {
  return {
    type: actionType.SELECTED_STORAGE_CUSTOMER,
    payload: storage,
  };
};
export const getDispatchOrder =
  (params = {}, transactionId) =>
    async (dispatch, getState) => {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store?.[0]?.id;
      dispatch(setLoadingTable(true));
      try {
        let result;
        if (transactionId) {
          result = await consume.getWithParams(
            'dispatchOrderCustomer',
            {},
            { token },
            { store_id: storeId },
            `detail/${transactionId}`
          );
          dispatch(setDetailDispatchOrder(result.result));
        } else {
          result = await consume.getWithParams(
            'dispatchOrderCustomer',
            {},
            { token },
            { ...params, store_id: storeId }
          );
          dispatch(setDispatchOrder(result.result));
        }
        dispatch(setLoadingTable(false));
        return Promise.resolve(result.result);
      } catch (err) {
        if (transactionId) {
          dispatch(setDetailDispatchOrder({}));
        } else {
          dispatch(setDispatchOrder({}));
        }
        dispatch(setLoadingTable(false));
        return Promise.reject(err);
      }
    };

function setDispatchOrder(data) {
  return {
    type: actionType.DISPATCH_ORDER_CUSTOMER,
    payload: data,
  };
}

function setDetailDispatchOrder(data) {
  return {
    type: actionType.DETAIL_DISPATCH_ORDER_CUSTOMER,
    payload: data,
  };
}

export const getListVendor = () => async (dispatch, getState) => {
  const { token } = getState().login;
  try {
    let result = await consume.getWithParams('listVendor', {}, { token }, {});
    dispatch(setListVendor(result.result));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListVendor([]));
    return Promise.reject(err);
  }
};

function setListVendor(data) {
  return {
    type: actionType.LIST_VENDOR_CUSTOMER,
    payload: data,
  };
}

export const getListServiceVendor =
  (vendorId) => async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result = await consume.getWithParams(
        'listServiceVendor',
        {},
        { token },
        { vendor_id: vendorId }
      );
      dispatch(setListServiceVendor(result.result));
      return Promise.resolve(result.result);
    } catch (err) {
      dispatch(setListServiceVendor([]));
      return Promise.reject(err);
    }
  };

function setListServiceVendor(data) {
  return {
    type: actionType.LIST_SERVICE_VENDOR_CUSTOMER,
    payload: data,
  };
}

export const createDispatchOrder = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('dispatchOrderCustomer', data, headers);
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil membuat dispatch order.',
        type: 'success',
      })
    );
    return Promise.resolve(response);
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: err?.error?.message || 'Gagal membuat dispatch order.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const getListProductStorage =
  (storageId) => async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      let data = {};
      let headers = { token: token };
      let response = await consume.getWithParams(
        'listProductStorageCustomer',
        data,
        headers,
        {
          storage_id: storageId,
          store_id: storeId,
        }
      );
      if (response) {
        dispatch(listProductStorage(response.result));
      }
    } catch (err) {
      dispatch(listProductStorage([]));
    }
  };

const listProductStorage = (data) => {
  return {
    type: actionType.LIST_PRODUCT_STORAGE_CUSTOMER,
    payload: data,
  };
};

export const getListPackaging = (warehouseId) => async (dispatch, getState) => {
  try {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    let data = {
      warehouse_id: warehouseId,
      store_id: storeId,
    };
    let headers = { token: token };
    let response = await consume.post('listPackagingCustomer', data, headers);
    if (response) {
      dispatch(listPackaging(response.result));
    }
  } catch (err) {
    dispatch(listPackaging([]));
  }
};

const listPackaging = (data) => {
  return {
    type: actionType.LIST_PACKAGING_CUSTOMER,
    payload: data,
  };
};

export const checkAWB = (vendorId, awbNumber) => async (dispatch, getState) => {
  const { token, dataUser } = getState().login;
  const storeId = dataUser?.store[0]?.id;
  try {
    let result = await consume.post(
      'checkAWBDispatchOrderCustomer',
      { store_id: storeId, vendor_id: vendorId, awb_number: awbNumber },
      { token }
    );
    return Promise.resolve(result);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const setDataFormCreateDispatch = (payload) => {
  return {
    type: actionType.FORM_DATA_CREATE_DISPATCH_ORDER_CUSTOMER,
    payload,
  };
};

export const cancelDispatchOrder =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token, dataUser } = getState().login;
        const storeId = dataUser?.store[0]?.id;
        let data = { ...payload, store_id: storeId };
        let headers = { token: token };
        const otherEndpoint = `cancel/${id}/`;
        const response = await consume.post(
          'dispatchOrderCustomer',
          data,
          headers,
          otherEndpoint
        );
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: 'Berhasil membatalkan dispatch order',
            type: 'success',
          })
        );
        dispatch(getDispatchOrder({}, id));
        return Promise.resolve('success');
      } catch (err) {
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: `${err?.error?.message || 'Gagal membatalkan dispatch order'
              }`,
            type: 'error',
          })
        );
        return Promise.reject('failed');
      }
    };

export const getRegularTransaction =
  (params = {}, transactionId) =>
    async (dispatch, getState) => {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store?.[0]?.id;
      dispatch(setLoadingTable(true));
      try {
        let result;
        if (transactionId) {
          result = await consume.getWithParams(
            'transactionRegularCustomer',
            {},
            { token },
            { store_id: storeId },
            `detail/${transactionId}`
          );
          dispatch(setDetailRegularTransaction(result.result));
        } else {
          result = await consume.getWithParams(
            'transactionRegularCustomer',
            {},
            { token },
            { ...params, store_id: storeId }
          );
          dispatch(setRegularTransaction(result.result));
        }
        dispatch(setLoadingTable(false));
        return Promise.resolve(result.result);
      } catch (err) {
        if (transactionId) {
          dispatch(setDetailRegularTransaction({}));
        } else {
          dispatch(setRegularTransaction({}));
        }
        dispatch(setLoadingTable(false));
        return Promise.reject(err);
      }
    };

function setRegularTransaction(data) {
  return {
    type: actionType.REGULAR_TRANSACTION_CUSTOMER,
    payload: data,
  };
}

function setDetailRegularTransaction(data) {
  return {
    type: actionType.DETAIL_REGULAR_TRANSACTION_CUSTOMER,
    payload: data,
  };
}

export const cancelRegularTransaction =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token, dataUser } = getState().login;
        const storeId = dataUser?.store[0]?.id;
        let data = { ...payload, store_id: storeId };
        let headers = { token: token };
        const otherEndpoint = `cancel/${id}/`;
        const response = await consume.post(
          'transactionRegularCustomer',
          data,
          headers,
          otherEndpoint
        );
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: 'Berhasil membatalkan transaksi regular',
            type: 'success',
          })
        );
        return Promise.resolve('success');
      } catch (err) {
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: `${err?.error?.message || 'Gagal membatalkan transaksi regular'
              }`,
            type: 'error',
          })
        );
        return Promise.reject('failed');
      }
    };

export const setDataFormCreateRegularTransaction = (payload) => {
  return {
    type: actionType.FORM_DATA_CREATE_REGULAR_TRANSACTION_CUSTOMER,
    payload,
  };
};

export const getListCourier = (params) => async (dispatch, getState) => {
  const { token, dataUser } = getState().login;
  const storeId = dataUser?.store[0]?.id;
  try {
    let result = await consume.post(
      'listCourierCustomer',
      { store_id: storeId, ...params },
      { token }
    );
    dispatch(setListCourier(result?.result));
    return Promise.resolve(result);
  } catch (err) {
    dispatch(setListCourier([]));
    return Promise.reject(err);
  }
};

const setListCourier = (data) => {
  return {
    type: actionType.LIST_COURIER_CUSTOMER,
    payload: data,
  };
};

export const createRegularTransaction =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      let data = { store_id: storeId, ...payload };
      let headers = { token: token };

      const response = await consume.post(
        'transactionRegularCustomer',
        data,
        headers
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil membuat transaksi regular.',
          type: 'success',
        })
      );
      return Promise.resolve(response);
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal membuat transaksi regular.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const exportLogsCashless =
  (payload) => async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      payload = {...payload, ...{store_id: storeId}};

      const config = {
        url: baseUrl.cashlessCustomer + 'export',
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          'Content-Disposition': `attachment; filename="Cashless.xlsx"`,
          Accept: 'application/prs.boxxoffice.v1+json'
        },
        params: payload,
        responseType: 'blob',
      };
      
      const res = await axios.request(config);
      
      return Promise.resolve(res);
    } catch (err) {
      
      dispatch(setToast({
        isShow: true,
        messages: err?.error?.message || 'Gagal export logs.',
        type: 'error',
      }));
      return Promise.reject(err);
    }
  };

export const exportLogsDispatchOrder =
  (payload) => async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      payload = {...payload, ...{store_id: storeId}};
      const config = {
        url: baseUrl.dispatchOrderCustomer + 'export',
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Disposition': `attachment; filename="Dispatch Order.xlsx"; filename*=UTF-8''Dispatch Order.xlsx`,
          'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        params: payload,
        responseType: 'blob',
      };
      console.log(`config`, config)
      const res = await axios.request(config);

      return Promise.resolve(res);
    } catch (err) {
      console.log('error exportLogsCashless', err);
      dispatch(setToast({
        isShow: true,
        messages: err?.error?.message || 'Gagal export logs.',
        type: 'error',
      }))
      return Promise.reject(err);
    }
  };

export const exportLogsRegularTransaction =
  (payload) => async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      const config = {
        url: baseUrl.exportLogsRegularTransactionCustomer,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          // 'Access-Control-Allow-Origin': '*',
          'Content-Disposition': `attachment; filename="Transaction Reguler.xlsx"; filename*=UTF-8''Transaction Reguler.xlsx`,
          // 'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        data: {
          store_id: storeId,
          ...payload,
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);

      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

export const getCashless =
  (params = {}, transactionId) =>
    async (dispatch, getState) => {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store?.[0]?.id;
      dispatch(setLoadingTable(true));
      try {
        let result;
        if (transactionId) {
          result = await consume.getWithParams(
            'cashlessCustomer',
            {},
            { token },
            { store_id: storeId },
            `detail/${transactionId}`
          );
          dispatch(setDetailCashless(result.result));
        } else {
          result = await consume.getWithParams(
            'cashlessCustomer',
            {},
            { token },
            { ...params, store_id: storeId }
          );
          dispatch(setCashless(result.result));
        }
        dispatch(setLoadingTable(false));
        return Promise.resolve(result.result);
      } catch (err) {
        console.log(err);
        if (transactionId) {
          dispatch(setDetailCashless({}));
        } else {
          dispatch(setCashless({}));
        }
        dispatch(setLoadingTable(false));
        return Promise.reject(err);
      }
    };

function setCashless(data) {
  return {
    type: actionType.CASHLESS_CUSTOMER,
    payload: data,
  };
}

function setDetailCashless(data) {
  return {
    type: actionType.DETAIL_CASHLESS_CUSTOMER,
    payload: data,
  };
}

export const createCashless = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('cashlessCustomer', data, headers);
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil membuat cashless.',
        type: 'success',
      })
    );
    return Promise.resolve(response);
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: err?.error?.message || 'Gagal membuat cashless.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const checkAWBCashless =
  (vendorId, awbNumber) => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    try {
      let result = await consume.post(
        'checkAWBCashlessCustomer',
        { store_id: storeId, vendor_id: vendorId, awb_number: awbNumber },
        { token }
      );
      return Promise.resolve(result);
    } catch (err) {
      return Promise.reject(err);
    }
  };

export const cancelCashless =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token, dataUser } = getState().login;
        const storeId = dataUser?.store[0]?.id;
        let data = { ...payload, store_id: storeId };
        let headers = { token: token };
        const otherEndpoint = `cancel/${id}/`;
        const response = await consume.post(
          'cashlessCustomer',
          data,
          headers,
          otherEndpoint
        );
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: 'Berhasil membatalkan cashless',
            type: 'success',
          })
        );
        return Promise.resolve('success');
      } catch (err) {
        dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: `${err?.error?.message || 'Gagal membatalkan cashless'
              }`,
            type: 'error',
          })
        );
        return Promise.reject('failed');
      }
    };

export const downloadTemplateBatch =
  () => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    try {
      const config = {
        url: baseUrl.cashlessCustomer + 'template-batch',
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        params: {
          store_id: storeId,
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };

  export const downloadTemplateBatchDispatchOrder =
  () => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    try {
      dispatch(setLoading(true));
      const config = {
        url: baseUrl.dispatchOrderCustomer + 'template-batch',
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        params: {
          store_id: storeId,
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      dispatch(setLoading(false));
        dispatch(
          setToast({
            isShow: true,
            messages: 'Berhasil mendownload template',
            type: 'success',
          })
        );
      return Promise.resolve(res);
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: `${err?.error?.message || 'Gagal mendownload template'
            }`,
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

  export const uploadInvoice =
  (payload) => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    try {

      const config = {
        url: baseUrl.cashlessCustomer + 'upload-batch',
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: payload,
        responseType: 'blob',
      };
      const res = await axios.request(config);
      return Promise.resolve(res);
    } catch (err) {
      console.log('err', err);
      return Promise.reject(err);
    }
  };

  export const uploadInvoiceDispatchOrder =
  (payload) => async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      dispatch(setLoading(true));
      const config = {
        url: baseUrl.dispatchOrderCustomer + 'upload-batch',
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        data: payload,
        responseType: 'blob',
      };
      console.log('config', config);
      const res = await axios.request(config);
      dispatch(setLoading(false));
      
      return Promise.resolve(res);
    } catch (err) {
      dispatch(setLoading(false));
      
      return Promise.reject(err);
    }
  };

  