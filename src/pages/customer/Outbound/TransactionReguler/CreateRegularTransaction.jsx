import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import BackButton from '../../../../components/elements/BackButton';
import Button from '../../../../components/elements/Button';
import { TabHeader, TabContent } from '../../../../components/elements/Tabs';
import FormPengirimOrPenerima from '../components/FormPengirimOrPenerima';
import '../styles.css';
import FormDetailPaket from '../components/FormDetailPaket';
import SelectCourier from '../components/SelectCourier';
import { createRegularTransaction, getListVendor } from '../reduxAction';

import FormLogisticRegularTransaction from '../components/FormLogisticRegularTransaction';

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

const validationSchemaSender = Yup.object({
  sender_name: Yup.string().required(
    'Harap untuk melakukan input nama pengirim'
  ),
  sender_phone_number: Yup.string()
    .matches(phoneRegex, 'Nomor telepon tidak valid')
    .required('Harap untuk melakukan input nomor telepon pengirim'),
  sender_longitude: Yup.number().required('Harap untuk menandai lokasi'),
  sender_latitude: Yup.number().required('Harap untuk menandai lokasi'),
  sender_address: Yup.string().required(
    'Harap untuk melakukan input alamat pengirim'
  ),
  sender_province_id: Yup.string().required('Harap untuk pilih provinsi'),
  sender_city_id: Yup.string().required('Harap untuk pilih kota'),
  sender_district_id: Yup.string().required('Harap untuk pilih kecamatan'),
});

const validationSchemaReceiver = Yup.object({
  receiver_name: Yup.string().required(
    'Harap untuk melakukan input nama penerima'
  ),
  receiver_phone_number: Yup.string()
    .matches(phoneRegex, 'Nomor telepon tidak valid')
    .required('Harap untuk melakukan input nomor telepon penerima'),
  receiver_longitude: Yup.number().required('Harap untuk menandai lokasi'),
  receiver_latitude: Yup.number().required('Harap untuk menandai lokasi'),
  receiver_address: Yup.string().required(
    'Harap untuk melakukan input alamat penerima'
  ),
  receiver_province_id: Yup.string().required('Harap untuk pilih provinsi'),
  receiver_city_id: Yup.string().required('Harap untuk pilih kota'),
  receiver_district_id: Yup.string().required('Harap untuk pilih kecamatan'),
});

const validationSchemaDetailPaket = Yup.object({
  category_product_id: Yup.string().required(
    'Harap untuk memilih kategori produk'
  ),
  sub_category_product_id: Yup.string().required(
    'Harap untuk memilih kategori produk'
  ),
  length: Yup.number()
    .typeError('Harus berupa angka')
    .required('Harap untuk melakukan input panjang'),
  width: Yup.number()
    .typeError('Harus berupa angka')
    .required('Harap untuk melakukan input lebar'),
  height: Yup.number()
    .typeError('Harus berupa angka')
    .required('Harap untuk melakukan input tinggi'),
  weight: Yup.number()
    .typeError('Harus berupa angka')
    .required('Harap untuk memilih kategori berat'),
  item_price: Yup.string().required('Harap untuk mengisi nilai barang'),
});

const validationSchemaLogistic = Yup.object({
  vendor_id: Yup.string().required('Harap untuk memilih logistik'),
  vendor_service_id: Yup.string().required(
    'Harap untuk memilih service logistik'
  ),
});

const itemTab = (props, item) => {
  const { activeColor, activeTab, inActiveColor } = props;
  const { tab, title } = item;
  return (
    <div
      className="w-100"
      style={{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        fontWeight: '600',
        color:
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF',
        textAlign: 'left',
        borderBottom: `8px solid ${
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF'
        }`,
      }}
    >
      <h6>
        <span
          style={{
            borderRadius: '100%',
            backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
            width: '24px',
            height: '24px',
            padding: '3px 5px',
          }}
        >
          {tab < activeTab ? (
            <i className="fa fa-check" aria-hidden="true"></i>
          ) : (
            `0${tab}`
          )}
        </span>{' '}
        {title}
      </h6>
    </div>
  );
};

const Transactions = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [step, setStep] = useState(1);
  const handleChangeTab = (tab) => setStep(tab);

  const { selectedStorage } = useSelector((state) => state.outboundCustomer);
  const { isLoading } = useSelector((state) => state.loading);
  const { dataUser: dataStore } = useSelector((state) => state.login);

  useEffect(() => {
    if (selectedStorage) {
      // dispatch(getListVendor());
    } else {
      history.push('/outbound/regular-transaction');
    }
  }, []);

  useEffect(() => {
    dispatch(getListVendor());
  }, [step]);
  const formikFormSender = useFormik({
    initialValues: {
      sender_name: '',
      sender_country_code: '+62',
      sender_phone_number: '',
      sender_longitude: '',
      sender_latitude: '',
      sender_address: '',
      sender_province_id: '',
      sender_city_id: '',
      sender_district_id: '',
      sender_note: '',
      is_dropshipper: '0',
    },
    validationSchema: validationSchemaSender,
    onSubmit: (values) => {
      // handleSubmit(values);
      // dispatch(setDataFormCreateDispatch(values));
      // handleChangeTab(3);
    },
  });

  const formikFormReceiver = useFormik({
    initialValues: {
      receiver_name: '',
      receiver_country_code: '+62',
      receiver_phone_number: '',
      receiver_longitude: '',
      receiver_latitude: '',
      receiver_address: '',
      receiver_province_id: '',
      receiver_city_id: '',
      receiver_district_id: '',
      receiver_note: '',
    },
    validationSchema: validationSchemaReceiver,
    onSubmit: (values) => {
      handleChangeTab(2);
    },
  });

  const formikFormDetailPaket = useFormik({
    initialValues: {
      category_product_id: '',
      sub_category_product_id: '',
      length: '',
      width: '',
      height: '',
      weight: '',
      item_price: '',
    },
    validationSchema: validationSchemaDetailPaket,
    onSubmit: (values) => {
      handleChangeTab(3);
    },
  });

  const formikFormLogistic = useFormik({
    initialValues: {
      vendor_id: '',
      vendor_service_id: '',
      is_insurance: '0',
    },
    validationSchema: validationSchemaLogistic,
    onSubmit: (values) => {
      const params = {
        ...formikFormSender.values,
        ...formikFormReceiver.values,
        ...formikFormDetailPaket.values,
        ...values,
        storage_id: selectedStorage?.id,
      };
      handleSubmitAll(params);
      // dispatch(createRegularTransaction(params));
    },
  });

  const handleSubmitAll = async (params) => {
    try {
      const res = await dispatch(createRegularTransaction(params));
      history.push(`/outbound/regular-transaction/detail/${res?.result?.id}`);
    } catch (err) {}
  };

  const handleSubmit = (e) => {
    if (step === 1) {
      if (formikFormSender.values.is_dropshipper === '0') {
        formikFormSender.setValues({
          sender_name: dataStore?.store?.[0]?.store_name,
          sender_country_code: dataStore?.user?.country_code,
          sender_phone_number: dataStore?.user?.phone_number,
          sender_longitude: dataStore?.store?.[0]?.longitude,
          sender_latitude: dataStore?.store?.[0]?.latitude,
          sender_address: dataStore?.store?.[0]?.store_address,
          sender_province_id: dataStore?.store?.[0]?.province_id,
          sender_city_id: dataStore?.store?.[0]?.city_id,
          sender_district_id: dataStore?.store?.[0]?.district_id,
          is_dropshipper: '0',
        });
      }
      formikFormSender.handleSubmit(e);
      formikFormReceiver.handleSubmit(e);
    } else if (step === 2) {
      formikFormDetailPaket.handleSubmit(e);
    } else {
      formikFormLogistic.handleSubmit(e);
    }
  };

  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Transaksi Regular"
              onClick={() => history.push('/outbound/regular-transaction')}
            />
          </div>
        )}
      >
        <ContentItem border={false} spaceBottom={3}>
          <TabHeader
            border={false}
            activeTab={step}
            activeBackground="transparent"
            inActiveBackground="transparent"
            activeColor="#828282"
            inActiveColor="#192a55"
            listHeader={[
              {
                tab: 1,
                title: 'Data Pengirim dan Penerima',
                render: itemTab,
              },
              {
                tab: 2,
                title: 'Detail Paket',
                render: itemTab,
              },
              {
                tab: 3,
                title: 'Data Logistik',
                render: itemTab,
              },
            ]}
            isClickable={false}
          />
        </ContentItem>
        <TabContent tab={1} activeTab={step}>
          <ContentItem
            className="p-3"
            spaceRight="0 pr-md-2"
            col="col-12 col-md-6"
            spaceBottom={3}
            style={{ minHeight: '800px' }}
          >
            <FormPengirimOrPenerima formik={formikFormSender} type="Pengirim" />
          </ContentItem>
          <ContentItem
            className="p-3"
            spaceRight="0 pl-md-2"
            col="col-12 col-md-6"
            spaceBottom={3}
            style={{ minHeight: '800px' }}
          >
            <FormPengirimOrPenerima
              formik={formikFormReceiver}
              type="Penerima"
            />
          </ContentItem>
        </TabContent>
        <TabContent tab={2} activeTab={step}>
          <ContentItem className="p-3" spaceBottom={3}>
            <FormDetailPaket formik={formikFormDetailPaket} />
          </ContentItem>
        </TabContent>

        <TabContent tab={3} activeTab={step}>
          {/* <ContentItem spaceBottom={3}> */}
          <FormLogisticRegularTransaction
            formikFormDetailPaket={formikFormDetailPaket}
            formikFormReceiver={formikFormReceiver}
            formikFormSender={formikFormSender}
            formik={formikFormLogistic}
          />
          {/* </ContentItem> */}
        </TabContent>

        <div className="ml-auto">
          <Button
            style={{ width: '140px' }}
            styleType="lightBlueFill"
            text={step == 1 ? 'Batal' : 'Kembali'}
            onClick={() => {
              step === 1 && history.push('/outbound/regular-transaction');
              step === 2 && setStep(1);
              step === 3 && setStep(2);
            }}
          />
          <Button
            style={{ minWidth: '140px', marginLeft: '10px' }}
            styleType="blueFill"
            text={
              step === 1
                ? 'Simpan'
                : step === 2
                ? 'Lanjutkan'
                : isLoading
                ? 'Loading...'
                : 'Request Pickup'
            }
            onClick={handleSubmit}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default Transactions;
