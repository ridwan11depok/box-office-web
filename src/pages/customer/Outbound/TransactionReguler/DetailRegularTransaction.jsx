import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import BackButton from '../../../../components/elements/BackButton';
import Button from '../../../../components/elements/Button';
import Badge from '../../../../components/elements/Badge';
import { statusRegularTransaction } from '../../../../utils/text';
import CancelTransactionModal from '../components/CancelTransactionModal';
import DetailSenderRegTrans from '../components/DetailSenderRegTrans';
import DetailReceiverRegTrans from '../components/DetailReceiverRegTrans';
import DetailPaketRegTrans from '../components/DetailPaketRegTrans';
import DetailLogistikRegTrans from '../components/DetailLogistikRegTrans';
import { getRegularTransaction } from '../reduxAction';
import DetailTotalPaymentRegTrans from '../components/DetailTotalPaymentRegTrans';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const DetailRegularTransaction = () => {
  const classes = useStyles();
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();
  const { detailRegularTransaction } = useSelector(
    (state) => state.outboundCustomer
  );
  const [state, setState] = useState({
    cancelTransactionModal: false,
  });

  const handleCancelTransaction = () => {
    setState({
      ...state,
      cancelTransactionModal: true,
    });
  };

  useEffect(() => {
    dispatch(getRegularTransaction({}, params?.id));
  }, [params?.id]);

  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label={
                <div>
                  Transaksi Regular
                  <Badge
                    style={{
                      fontSize: '14px',
                      fontWeight: '700',
                      padding: '16px 8px',
                      marginLeft: '10px',
                    }}
                    styleType={
                      detailRegularTransaction?.status === 6
                        ? 'green'
                        : detailRegularTransaction?.status === 7 ||
                          detailRegularTransaction?.status === 8
                        ? 'red'
                        : detailRegularTransaction?.status == 0
                        ? 'black'
                        : 'yellow'
                    }
                    label={statusRegularTransaction(
                      detailRegularTransaction?.status || 0
                    )}
                  />
                </div>
              }
              onClick={() => history.goBack()}
              // onClick={() => history.push('/outbound/regular-transaction')}
            />
            <div>
              {detailRegularTransaction?.status === 0 && (
                <>
                  <Button
                    style={{ minWidth: '140px', marginRight: '10px' }}
                    styleType="redOutline"
                    text="Batal Transaksi"
                    onClick={handleCancelTransaction}
                  />
                  {/* <Button
                    className="ml-auto"
                    style={{ minWidth: '140px' }}
                    styleType="blueOutline"
                    text={state.isEdit ? 'Simpan' : 'Edit Transaksi'}
                    onClick={state.isEdit ? handleSaveButton : handleEditButton}
                  /> */}
                </>
              )}
            </div>
          </div>
        )}
      >
        <ContentItem
          className="p-3"
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '450px' }}
        >
          <DetailSenderRegTrans />
        </ContentItem>
        <ContentItem
          className="p-3"
          spaceRight="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '450px' }}
        >
          <DetailReceiverRegTrans />
        </ContentItem>
        <ContentItem className="p-3" spaceBottom={3}>
          <DetailPaketRegTrans />
        </ContentItem>
        <ContentItem className="p-3" spaceBottom={3}>
          <DetailLogistikRegTrans />
        </ContentItem>
        <ContentItem className="p-3">
          <DetailTotalPaymentRegTrans />
        </ContentItem>
      </ContentContainer>

      <CancelTransactionModal
        data={detailRegularTransaction}
        show={state.cancelTransactionModal}
        onHide={() => setState({ ...state, cancelTransactionModal: false })}
      />
    </>
  );
};

export default DetailRegularTransaction;
