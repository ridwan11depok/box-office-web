import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import {
  WarehouseSelect,
  SearchField,
} from '../../../components/elements/InputField';
import Button from '../../../components/elements/Button';
import { Table } from '../../../components/elements/Table';
import Counter from '../../../components/elements/Counter';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const HeaderTable = (props) => {
  const { isEdit, onUpdate, onSave, onCancel } = props;
  return (
    <div className="d-flex flex-row justify-content-between align-items-center p-3 w-100">
      <SearchField placeholder="Cari Produk, SKU" />
      <div>
        {isEdit && (
          <Button
            style={{ minWidth: '140px' }}
            text="Batal"
            styleType="lightBlueFill"
            onClick={onCancel}
          />
        )}

        <Button
          style={{ minWidth: '140px' }}
          className="ml-1"
          text={isEdit ? 'Simpan' : 'Manual Update'}
          styleType="blueFill"
          onClick={() => (isEdit ? onSave() : onUpdate())}
        />
      </div>
    </div>
  );
};

const EditData = (props) => {
  const { data, isEdit } = props;
  return isEdit ? (
    <Counter
      // onDecrease={() => handleChangeQty('decrease')}
      // onIncrease={() => handleChangeQty('increase')}
      value={data.dataSeller}
    />
  ) : (
    <span>{data.dataSeller}</span>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak proses kirim stock yang dilakukan saat ini.
      </h6>
    </div>
  );
};

const Stock = (props) => {
  const history = useHistory();
  const [state, setState] = useState({ isEdit: false });
  const handleChangePage = (page) => {
    setState({ ...state, page });
  };
  const classes = useStyles();
  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
          <PageTitle title="Stocks" />
          <div className="d-flex flex-row align-items-center">
            <h6 className={classes.selectWarehouse}>Pilih Gudang :</h6>
            <WarehouseSelect />
          </div>
        </div>
      )}
    >
      <ContentItem border={false}>
        <Table
          column={[
            {
              heading: 'SKU',
              key: 'sku',
            },
            {
              heading: 'Produk',
              key: 'product',
            },
            {
              heading: 'Data Gudang',
              key: 'dataGudang',
            },
            {
              heading: 'Data Seller',
              render: (data) => <EditData data={data} isEdit={state.isEdit} />,
            },
          ]}
          data={[
            {
              sku: 'SKU123',
              product: 'Baju Bagus',
              dataGudang: '125',
              dataSeller: '10',
            },
          ]}
          renderHeader={() => (
            <HeaderTable
              isEdit={state.isEdit}
              onUpdate={() => setState({ ...state, isEdit: true })}
              onCancel={() => setState({ ...state, isEdit: false })}
              onSave={() => setState({ ...state, isEdit: false })}
            />
          )}
          withNumber={false}
          renderEmptyData={() => <EmptyData />}
        />
      </ContentItem>
    </ContentContainer>
  );
};

export default Stock;
