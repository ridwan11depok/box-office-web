import React, { useState } from 'react';
import './style/style.css';
import Pagination from '../../../components/elements/Table/Pagination';
import { Row, Col, Form, Button, Table } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Select from 'react-select';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import exportmutasi from '../../../assets/icons/exportmutasi.svg';
import date from '../../../assets/icons/date.svg';

const Mutasistock = (props) => {
  const history = useHistory();
  const {
    data,
    column,
    transformData,
    size: view,
    renderHeader,
    sizeOptions,
    withNumber
  } = props;
  const [state, setState] = useState({ page: 1, size: view });
  const handleChangePage = (page) => {
    setState({ ...state, page });
  };

  const options1 = [
    { value: 'a', label: 'Januari 2021' },
    { value: 'b', label: 'Februari 2021' },
    { value: 'c', label: 'Maret 2021' },
    { value: 'd', label: 'April 2021' },
    { value: 'e', label: 'Mei 2021' },
    { value: 'f', label: 'Juni 2021' },
    { value: 'g', label: 'Juli 2021' },
    { value: 'h', label: 'Agustus 2021' },
    { value: 'i', label: 'September 2021' },
    { value: 'j', label: 'Oktober 2021' },
    { value: 'k', label: 'November 2021' },
    { value: 'l', label: 'Desember 2021' }
  ]


  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12">
            <div className="section-stock">
              <h1>Mutasi Stock</h1>
            </div>
          </div>
          <div className="col-lg-2 col-md-6 col-sm-12 col-xs-12">
            <div
              style={{
                marginTop: '15px',
                paddingRight: '15px',
                float: 'right',
              }}
            >
              <text>Pilih Gudang :</text>
            </div>
          </div>
          <div
            className="col-lg-2 col-md-6 col-sm-12 col-xs-12"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '5px',
              height: '50px',
              display: 'flex',
            }}
          >
            <div>
              <img
                src={gudang}
                style={{ marginLeft: '-12px', marginTop: '3px' }}
              />
            </div>
            <div className="jarak-gudang">
              <span className="gudang-a">Gudang A</span>
              <br></br>
              <span className="lokasi-gudang">Bandung, Jawa Barat</span>
            </div>
            <div
              style={{ position: 'absolute', right: '0', marginRight: '3PX' }}
            >
              <img src={gudangdropdown} style={{ marginTop: '3px' }} />
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px' }}
        >
          <div className="col-lg-12">
            <div
              className="row"
              style={{ borderBottom: '1px solid #E8E8E8', height: '72px' }}
            >
              <div
                className="col-lg-12"
                style={{ textAlign: 'end', marginTop: '15px' }}
              >
                <div>
                  <button
                    type="button"
                    className="btn"
                    style={{
                      borderRadius: '4px',
                      borderWidth: 2,
                      borderColor: '#192A55',
                      height: '40px',
                    }}
                  >
                    <text className="btn-export">
                      <img
                        src={exportmutasi}
                        style={{ paddingRight: '10px' }}
                      />
                      Export Mutasi Stock
                    </text>
                  </button>
                </div>
              </div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Produk</label>
                  <Select options={options1} />
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">Dari tanggal</Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">Sampai tanggal</Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
            </div>
            <div className="row"
              style={{
                borderBottom: "1px solid #E8E8E8"
              }}
            >
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Produk</th>
                    <th>Awal</th>
                    <th>Akhir</th>
                  </tr>
                </thead>
                <tbody
                  style={{
                    height: "417px"
                  }}
                >
                  <tr>
                    <td colSpan={3}
                      style={{
                        textAlign: "center"
                      }}
                    >
                      <p>Stok barang yang keluar masuk masih kosong</p>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <div className="row">
              <div className="col-lg-9">
                <text className="text-view">
                  View
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-3">
                <div
                  style={{
                    float: "right",
                    paddingTop: "15px"
                  }}>
                  <Pagination
                    activePage={state.page}
                    changePage={handleChangePage}
                    totalPage={7}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Mutasistock;
