import React from 'react';
import pilihgudang from '../../../assets/icons/pickuporder.svg';

const Pilihgudang = () => {
  return (
    <div className="main-content">
      <div className="section">
        <div className="row" style={{ position: 'relative' }}>
          <div className="col-lg-4"></div>
          <div className="col-lg-4" style={{ paddingTop: '172PX' }}>
            <center>
              <img src={pilihgudang} />
              <p className="text-pickup-order">Anda belum memiliki gudang</p>
              <p className="desc-pickup-order">
                Silahkan untuk memilih gudang agar bisa menggunakan fitur
                “Mutasi Stock”
              </p>
              <div>
                <button
                  type="button"
                  className="btn"
                  style={{
                    marginLeft: '10px',
                    backgroundColor: '#192A55',
                    borderRadius: '4px',
                    borderWidth: 2,
                    borderColor: '#192A55',
                    height: '40px',
                  }}
                >
                  <text className="btn-gudang-center">Pilih Gudang</text>
                </button>
              </div>
            </center>
          </div>
          <div className="col-lg-4"></div>
        </div>
      </div>
    </div>
  );
};

export default Pilihgudang;
