import React from 'react';
import './style/style.css';
import Select from 'react-select';
import DataTable from '../../../components/elements/Table/Table';
import { Row, Col, Form, Button, Pagination } from 'react-bootstrap';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import exportmutasi from '../../../assets/icons/exportmutasi.svg';
import date from '../../../assets/icons/date.svg';

const Mutasistockfill = () => {
  const options1 = [
    { value: 'a', label: 'Bag 123' },
    { value: 'b', label: 'Headset ABC' },
    { value: 'c', label: 'Sandal ABC' },
    { value: 'd', label: 'Shoes ABC' },
    { value: 'e', label: 'Shoes 123' }
  ]

  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12">
            <div className="section-stock">
              <h1>Mutasi Stock</h1>
            </div>
          </div>
          <div className="col-lg-2 col-md-6 col-sm-12 col-xs-12">
            <div
              style={{
                marginTop: '15px',
                paddingRight: '15px',
                float: 'right',
              }}
            >
              <text>Pilih Gudang :</text>
            </div>
          </div>
          <div
            className="col-lg-2 col-md-6 col-sm-12 col-xs-12"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '5px',
              height: '50px',
              display: 'flex',
            }}
          >
            <div>
              <img
                src={gudang}
                style={{ marginLeft: '-12px', marginTop: '3px' }}
              />
            </div>
            <div className="jarak-gudang">
              <span className="gudang-a">Gudang A</span>
              <br></br>
              <span className="lokasi-gudang">Bandung, Jawa Barat</span>
            </div>
            <div
              style={{ position: 'absolute', right: '0', marginRight: '3PX' }}
            >
              <img src={gudangdropdown} style={{ marginTop: '3px' }} />
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px', borderBottom: 'none' }}
        >
          <div className="col-lg-12">
            <div
              className="row"
              style={{ borderBottom: '1px solid #E8E8E8', height: '72px' }}
            >
              <div
                className="col-lg-12"
                style={{ textAlign: 'end', marginTop: '15px' }}
              >
                <div>
                  <button
                    type="button"
                    className="btn"
                    style={{
                      borderRadius: '4px',
                      borderWidth: 2,
                      borderColor: '#192A55',
                      height: '40px',
                    }}
                  >
                    <text className="btn-export">
                      <img
                        src={exportmutasi}
                        style={{ paddingRight: '10px' }}
                      />
                      Export Mutasi Stock
                    </text>
                  </button>
                </div>
              </div>
            </div>
            <div className="row" style={{}}>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Produk</label>
                  <Select options={options1} />
                </div>
              </div>
              <div className="col-lg-4">
                <Form.Group controlId="dob">
                  <Form.Label className="table-heading">Dari tanggal</Form.Label>
                  <Form.Control type="date" name="dob" />
                </Form.Group>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <Form.Group controlId="dob">
                    <Form.Label className="table-heading">Sampai tanggal</Form.Label>
                    <Form.Control type="date" name="dob" />
                  </Form.Group>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <DataTable
            column={[
              {
                heading: 'Produk',
                key: 'produk',
              },
              {
                heading: 'Awal',
                key: 'awal',
              },
              {
                heading: 'Akhir',
                key: 'akhir',
              },
            ]}
            data={[
              {
                produk: 'Shoes ABC',
                awal: '15',
                akhir: '15',
              },
              {
                produk: 'Bag 123',
                awal: '20',
                akhir: '20',
              },
              {
                produk: 'Sandal ABC',
                awal: '22',
                akhir: '22',
              },
              {
                produk: 'Headset ABC',
                awal: '18',
                akhir: '18',
              },
              {
                produk: 'Shoes ABC',
                awal: '25',
                akhir: '25',
              },
            ]}
            transformData={(item) => ({
              ...item,
              sku: item.sku,
              produk: item.produk,
              gudang: item.gudang,
              seller: item.seller,
            })}
            withNumber={false}
          />
        </div>
      </section>
    </div>
  );
};

export default Mutasistockfill;
