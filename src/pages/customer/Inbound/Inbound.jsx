import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { WarehouseSelect } from '../../../components/elements/InputField';
import { makeStyles } from '@material-ui/core/styles';
import InboundTable from './components/InboundTable';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});

const Inbound = (props) => {
  const classes = useStyles();

  return (
    <ContentContainer title="Inbound">
      <ContentItem border={false}>
        <InboundTable />
      </ContentItem>
    </ContentContainer>
  );
};

export default Inbound;
