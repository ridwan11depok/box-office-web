import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import SKUTable from './components/SKUTable';
import Button from '../../../components/elements/Button';
import DataForm from './components/DataForm';
import nobarcode from '../../../assets/images/nobarcode.svg';
import Badge from '../../../components/elements/Badge';
import { useSelector, useDispatch } from 'react-redux';
import { getAllInbound } from './reduxAction';
import ModalCancelInbound from './components/ModalCancelInbound';

const useStyles = makeStyles({
  textValue: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  textLabel: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: '5px',
  },
});

const InboundDetail = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    isEdit: true,
    requestPickup: false,
    deleteInbound: false,
    sendInbound: false,
    showFeedback: false,
    feedbackType: '',
    showModalCancel: false,
  });
  const { detailInbound } = useSelector((state) => state.inbound);
  const params = useParams();
  const fetchDataDetail = async () => {
    try {
      const res = await dispatch(getAllInbound({}, params?.id));
    } catch (err) {}
  };
  useEffect(() => {
    fetchDataDetail();
  }, []);

  const handleCancelInbound = () => {
    setState({
      ...state,
      showModalCancel: true,
    });
  };
  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            onClick={() => history.push('/inbound')}
            label={
              <div>
                {detailInbound?.order_number || 'Detail Inbound'}
                <Badge
                  style={{
                    fontSize: '14px',
                    fontWeight: '700',
                    padding: '16px 8px',
                    marginLeft: '10px',
                  }}
                  styleType={
                    detailInbound?.status === 0
                      ? 'black'
                      : detailInbound?.status === 1
                      ? 'yellow'
                      : detailInbound?.status === 2
                      ? 'blue'
                      : 'green'
                  }
                  label={
                    detailInbound?.status === 0
                      ? 'Kirim'
                      : detailInbound?.status === 1
                      ? 'Pickup'
                      : detailInbound?.status === 2
                      ? 'Sampai di Gudang'
                      : 'Selesai'
                  }
                />
              </div>
            }
          />
          <div>
            {detailInbound?.status === 0 && (
              <>
                <Button
                  style={{ minWidth: '140px', marginRight: '10px' }}
                  styleType="redOutline"
                  text="Batalkan"
                  onClick={handleCancelInbound}
                />
              </>
            )}
          </div>
        </div>
      )}
    >
      <DataForm page="detail" />
      <ContentItem border={false}>
        <SKUTable isEdit={false} />
      </ContentItem>
      <div className="ml-auto">
        <Button
          style={{ minWidth: '140px' }}
          styleType="lightBlueFill"
          text="Kembali"
          onClick={() => history.push('/inbound')}
        />
      </div>
      <ModalCancelInbound
        data={detailInbound}
        show={state.showModalCancel}
        onHide={() => setState({ ...state, showModalCancel: false })}
      />
    </ContentContainer>
  );
};

export default InboundDetail;
