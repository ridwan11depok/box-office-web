import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import { useHistory } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import SKUTable from './components/SKUTable';
import Button from '../../../components/elements/Button';
import { TabHeader, TabContent } from '../../../components/elements/Tabs/';
import FormSenderAndWarehouse from './components/FormSenderAndWarehouse';
import { useDispatch, useSelector } from 'react-redux';
import { getListAllProductSKU, addInbound, setSelectedSKUAction } from './reduxAction';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ModalConfirmSend from './components/ModalConfirmSend';
import Toast from '../../../components/elements/Toast';

const useStyles = makeStyles({
  textValue: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  textLabel: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: '5px',
  },
});

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

const validationSchema = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama pelanggan'),
  country_code: Yup.string().required('Required'),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Nomor telepon tidak valid')
    .required('Harap untuk mengisi nomor telepon pelanggan'),
  longitude: Yup.number().required('Harap untuk memilih koordinat lokasi'),
  latitude: Yup.number().required('Harap untuk memilih koordinat lokasi'),
  address: Yup.string().required('Harap untuk mengisi lokasi'),
  province_id: Yup.number().required('Harap untuk mengisi provinsi'),
  city_id: Yup.number().required('Harap untuk mengisi kota'),
  district_id: Yup.number().required('Harap untuk mengisi Kecamatan'),
  // is_request_pickup: Yup.number().required('Required'),
  storage_id: Yup.number().required('Harap untuk memilih gudang'),
});

const itemTab = (props, item) => {
  const { activeColor, activeTab, inActiveColor } = props;
  const { tab, title } = item;
  return (
    <div
      className="w-100"
      style={{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        fontWeight: '600',
        color:
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF',
        textAlign: 'left',
        borderBottom: `8px solid ${
          tab === activeTab
            ? activeColor
            : tab < activeTab
            ? inActiveColor
            : '#CFCFCF'
        }`,
      }}
    >
      <h6>
        <span
          style={{
            borderRadius: '100%',
            backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
            width: '24px',
            height: '24px',
            padding: '3px 5px',
          }}
        >
          0{tab}
        </span>{' '}
        {title}
      </h6>
    </div>
  );
};

const InboundSend = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    isEdit: true,
    modalConfirmSend: false,
    dataProductSelected: [],
  });
  const [step, setStep] = useState(1);

  const { dataUser } = useSelector((state) => state.login);
  useEffect(() => {
    dispatch(getListAllProductSKU());
  }, []);

  const handleSelectProductSKU = (dataProductSelected) => {
    setState({ ...state, dataProductSelected });
  };
  const formik = useFormik({
    initialValues: {
      name: '',
      country_code: '62',
      phone_number: '',
      longitude: '',
      latitude: '',
      address: '',
      province_id: '',
      city_id: '',
      district_id: '',
      is_request_pickup: '0',
      date_pickup: '',
      storage_id: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      setStep(2);
      // setState({ ...state, dataInbound: values });
      // console.log(values);
    },
  });

  const handleSubmitAll = async () => {
    const values = formik.values;
    const data = new FormData();
    data.append('store_id', dataUser?.store?.[0]?.id);
    data.append('name', values.name);
    data.append('country_code', values.country_code);
    data.append('phone_number', values.phone_number);
    data.append('longitude', values.longitude);
    data.append('latitude', values.latitude);
    data.append('address', values.address);
    data.append('province_id', values.province_id);
    data.append('city_id', values.city_id);
    data.append('district_id', values.district_id);
    data.append('is_request_pickup', values.is_request_pickup);
    if (Number(values.is_request_pickup) === 1) {
      data.append('date_pickup', values.date_pickup);
    }
    data.append('storage_id', values.storage_id);
    state.dataProductSelected.forEach((item) =>
      data.append('product_id[]', item.id)
    );
    state.dataProductSelected.forEach((item) =>
      data.append('quantity[]', item.qty)
    );
    try {
      await dispatch(addInbound(data));
      dispatch(setSelectedSKUAction([]))
      history.push('/inbound');
      setState({ ...state, dataProductSelected: [] });
    } catch (err) {}
  };
  const handleSendInbound = () => {
    const checkQuantity = state.dataProductSelected.filter(
      (item) => item.qty === 0
    );
    if (state.dataProductSelected.length === 0) {
      Toast({ type: 'error', message: 'Silahkan pilih SKU terlebih dahulu.' });
    } else if (checkQuantity.length > 0) {
      Toast({ type: 'error', message: 'Quantity SKU tidak boleh nol.' });
    } else {
      setState({ ...state, modalConfirmSend: true });
    }
  };
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Kirim Stock"
              onClick={() => history.push('/inbound')}
            />
          </div>
        )}
      >
        <ContentItem border={false} spaceBottom={3}>
          <TabHeader
            border={false}
            activeTab={step}
            activeBackground="transparent"
            inActiveBackground="transparent"
            listHeader={[
              {
                tab: 1,
                title: 'Data Pengirim dan Gudang',
                render: itemTab,
              },
              {
                tab: 2,
                title: 'Pilih Produk',
                render: itemTab,
              },
            ]}
            isClickable={false}
          />
        </ContentItem>
        <TabContent tab={1} activeTab={step}>
          <FormSenderAndWarehouse formik={formik} />
        </TabContent>
        <TabContent tab={2} activeTab={step}>
          <ContentItem border={false}>
            <SKUTable
              isEdit={state.isEdit}
              handleSelectProductSKU={handleSelectProductSKU}
            />
          </ContentItem>
        </TabContent>
        <div className="ml-auto">
          <Button
            style={{ minWidth: '140px' }}
            styleType="lightBlueFill"
            text={step === 1 ? 'Batal' : 'Sebelumnya'}
            onClick={() => {
              if (step === 1) {
                history.push('/inbound');
              } else {
                setStep(1);
              }
            }}
          />
          <Button
            style={{ minWidth: '140px', marginLeft: '10px' }}
            styleType="blueFill"
            text={step === 1 ? 'Selanjutnya' : 'Kirim'}
            onClick={(e) => {
              if (step === 1) {
                formik.handleSubmit(e);
              } else {
                handleSendInbound();
              }
            }}
          />
        </div>
        <ModalConfirmSend
          show={state.modalConfirmSend}
          onHide={() => setState({ ...state, modalConfirmSend: false })}
          onAgree={handleSubmitAll}
        />
      </ContentContainer>
    </>
  );
};

export default InboundSend;
