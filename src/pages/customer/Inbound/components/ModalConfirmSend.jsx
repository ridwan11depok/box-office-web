import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';

const ModalConfirmSend = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal="Konfirmasi Pengiriman"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <p className="text-left">
            {`Apakah anda yakin akan melakukan pengiriman stock?`}
          </p>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Kirim'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalConfirmSend;
