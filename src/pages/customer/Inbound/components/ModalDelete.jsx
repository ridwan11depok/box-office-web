import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';

const ModalDelete = (props) => {
  return (
    <>
      <Modal
        titleModal="Hapus Inbound"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        data={props.data}
        content={(data) => (
          <p className="text-left">
            {`Apakah anda yakin akan menghapus "${
              data || ''
            }" dari proses inbound?`}
          </p>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="redFill"
              text="Hapus"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalDelete;
