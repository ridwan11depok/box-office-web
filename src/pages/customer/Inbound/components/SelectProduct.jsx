// import React, { useState } from 'react';
// import {
//   ContentContainer,
//   ContentItem,
// } from '../../../components/elements/BaseContent';
// import { makeStyles } from '@material-ui/core/styles';
// import BackButton from '../../../components/elements/BackButton';
// import { useHistory } from 'react-router-dom';
// import {
//   TextField,
//   SelectField,
//   TextArea,
//   SearchField,
// } from '../../../components/elements/InputField';
// import { Row, Col } from 'react-bootstrap';
// import InboundSendTable from './components/InboundSendTable';
// import Button from '../../../components/elements/Button';
// import DataForm from './components/DataForm';
// import ModalDelete from './components/ModalDelete';
// import ModalSend from './components/ModalSend';
// import ModalRequest from './components/ModalRequest';
// import { FeedbackPage } from '../../../components/elements/Feedback';
// import nobarcode from '../../../assets/images/nobarcode.svg';
// import { TabHeader, TabContent } from '../../../components/elements/Tabs/';
// import FormSenderAndWarehouse from './components/FormSenderAndWarehouse';

// const useStyles = makeStyles({
//   textValue: {
//     color: '#1c1c1c',
//     fontSize: '14px',
//     fontWeight: '400',
//     fontFamily: 'Open Sans',
//   },
//   textLabel: {
//     color: '#1c1c1c',
//     fontSize: '14px',
//     fontWeight: '600',
//     fontFamily: 'Open Sans',
//     marginBottom: '5px',
//   },
// });

// const itemTab = (props, item) => {
//   const { activeColor, activeTab, inActiveColor } = props;
//   const { tab, title } = item;
//   return (
//     <div
//       className="w-100"
//       style={{
//         fontFamily: 'Open Sans',
//         fontSize: '14px',
//         fontWeight: '600',
//         color:
//           tab === activeTab
//             ? activeColor
//             : tab < activeTab
//             ? inActiveColor
//             : '#CFCFCF',
//         textAlign: 'left',
//         borderBottom: `8px solid ${
//           tab === activeTab
//             ? activeColor
//             : tab < activeTab
//             ? inActiveColor
//             : '#CFCFCF'
//         }`,
//       }}
//     >
//       <h6>
//         <span
//           style={{
//             borderRadius: '100%',
//             backgroundColor: activeTab === tab ? '#E8E8E8' : '#F2F2F2',
//             width: '24px',
//             height: '24px',
//             padding: '3px 5px',
//           }}
//         >
//           0{tab}
//         </span>{' '}
//         {title}
//       </h6>
//     </div>
//   );
// };

// const InboundSend = (props) => {
//   const classes = useStyles();
//   const history = useHistory();
//   const [state, setState] = useState({
//     isEdit: true,
//     requestPickup: false,
//     deleteInbound: false,
//     sendInbound: false,
//     showFeedback: false,
//     feedbackType: '',
//   });
//   const [step, setStep] = useState(1);
//   return (
//     <>
//       <ContentContainer
//         renderHeader={() => (
//           <div className="d-flex flex-row justify-content-between align-items-center">
//             <BackButton
//               label="Kirim Stock"
//               onClick={() => history.push('/inventory/inbound')}
//             />
//           </div>
//         )}
//       >
//         <ContentItem border={false} spaceBottom={3}>
//           <TabHeader
//             border={false}
//             activeTab={step}
//             activeBackground="transparent"
//             inActiveBackground="transparent"
//             listHeader={[
//               {
//                 tab: 1,
//                 title: 'Data Pengirim dan Gudang',
//                 render: itemTab,
//               },
//               {
//                 tab: 2,
//                 title: 'Pilih Produk',
//                 render: itemTab,
//               },
//             ]}
//             isClickable={false}
//           />
//         </ContentItem>
//         <FormSenderAndWarehouse />
//         <div className="ml-auto">
//             <Button
//               style={{ minWidth: '140px' }}
//               styleType={state.isEdit ? 'lightBlueFill' : 'blueFill'}
//               text={state.isEdit ? 'Batal' : 'Request Pickup'}
//               onClick={() =>
//                 state.isEdit
//                   ? history.push('/inventory/inbound')
//                   : setState({ ...state, requestPickup: true })
//               }
//             />
//             <Button
//               style={{ minWidth: '140px', marginLeft: '10px' }}
//               styleType="blueFill"
//               text={state.isEdit ? 'Simpan' : 'Kirim'}
//               onClick={() => {
//                 state.isEdit
//                   ? setState({ ...state, isEdit: false })
//                   : setState({ ...state, sendInbound: true });
//               }}
//             />
//           </div>
//       </ContentContainer>
//       {state.showFeedback ? (
//         <FeedbackPage
//           feedbackType={state.feedbackType}
//           onClick={() => setState({ ...state, showFeedback: false })}
//         />
//       ) : (
//         <ContentContainer
//           renderHeader={() => (
//             <div className="d-flex flex-row justify-content-between align-items-center">
//               <BackButton
//                 label="Kirim Stock"
//                 onClick={() => history.push('/inventory/inbound')}
//               />
//               {!state.isEdit && (
//                 <div>
//                   <Button
//                     styleType="redOutline"
//                     text="Hapus"
//                     style={{ marginRight: '10px', minWidth: '140px' }}
//                     onClick={() => setState({ ...state, deleteInbound: true })}
//                   />
//                   <Button
//                     styleType="blueOutline"
//                     text="Edit"
//                     onClick={() => setState({ ...state, isEdit: true })}
//                     style={{ minWidth: '140px' }}
//                   />
//                 </div>
//               )}
//             </div>
//           )}
//         >
//           {/* Form */}
//           {state.isEdit ? (
//             <>
//               <ContentItem
//                 spaceBottom={3}
//                 spaceRight="0 pr-md-2"
//                 col="col-12 col-md-6"
//                 className="p-3"
//                 style={{ minHeight: '320px' }}
//               >
//                 <TextField
//                   className="mt-1 mb-2"
//                   label="Nama Pelanggan"
//                   placeholder="Input nama pelanggan"
//                 />
//                 <Row>
//                   <Col>
//                     <SelectField
//                       label="Nomor Telepon Pelanggan"
//                       options={[
//                         {
//                           value: '1',
//                           description: 'Indonesia(+62)',
//                         },
//                         {
//                           value: '2',
//                           description: 'Opsi 2',
//                         },
//                         {
//                           value: '3',
//                           description: 'Opsi 3',
//                         },
//                       ]}
//                     />
//                   </Col>
//                   <Col className="d-flex align-items-end">
//                     <TextField
//                       className="w-100"
//                       placeholder="Input nomor telepon"
//                     />
//                   </Col>
//                 </Row>
//                 <TextArea
//                   className="mt-1 mb-2"
//                   label="Alamat"
//                   placeholder="Input alamat"
//                 />
//               </ContentItem>
//               <ContentItem
//                 spaceBottom={3}
//                 spaceLeft="0 pl-md-2"
//                 col="col-12 col-md-6"
//                 className="p-3"
//                 style={{ minHeight: '320px' }}
//               >
//                 <Row>
//                   <Col xs={6}>
//                     <SearchField
//                       className="mt-1 mb-2"
//                       label="Kota"
//                       placeholder="Pilih kota"
//                       iconPosition="end"
//                     />
//                   </Col>
//                   <Col xs={6} className="d-flex align-items-end">
//                     <SearchField
//                       className="mt-1 mb-2 w-100"
//                       label="Provinsi"
//                       placeholder="Pilih provinsi"
//                       iconPosition="end"
//                     />
//                   </Col>
//                 </Row>
//                 <TextField
//                   className="mt-1 mb-2"
//                   label="Kode Pos"
//                   placeholder="Input kode pos"
//                 />
//                 <TextField
//                   className="mt-1 mb-2"
//                   label="Tanggal Pickup Order"
//                   type="date"
//                 />
//               </ContentItem>
//             </>
//           ) : (
//             <DataForm />
//           )}

//           <ContentItem
//             spaceBottom={3}
//             spaceRight="0 pr-md-2"
//             col="col-12 col-md-6"
//             className="p-3"
//             style={{ minHeight: '130px' }}
//           >
//             <h6 className={classes.textLabel}>Alamat Gudang</h6>
//             <h6 className={classes.textValue}>
//               {' '}
//               Jl. Gatot Subroto No. 45 B, Malabar, Harapan Indah, Kec. Lengkong,
//               Kota Bandung, Jawa Barat 40262
//             </h6>
//           </ContentItem>
//           <ContentItem
//             spaceBottom={3}
//             spaceLeft="0 pl-md-2"
//             col="col-12 col-md-6"
//             className="p-3"
//             style={{
//               minHeight: '130px',
//               display: 'flex',
//               flexDirection: 'row',
//             }}
//           >
//             <img className="mr-2" src={nobarcode} alt="" />
//             <div>
//               <h6 className={classes.textLabel}>Tanpa Barcode</h6>
//               <h6 className={classes.textValue}>
//                 Disarankan SKU lebih dari 10
//               </h6>
//               <h6 className={classes.textValue}>
//                 Tidak menggunakan sistem barcode. Pengecekan dilakukan secara
//                 manual, dan tidak menggunakan barcode scanner.
//               </h6>
//             </div>
//           </ContentItem>
//           <ContentItem border={false}>
//             <InboundSendTable isEdit={state.isEdit} />
//           </ContentItem>
//           <div className="ml-auto">
//             <Button
//               style={{ minWidth: '140px' }}
//               styleType={state.isEdit ? 'lightBlueFill' : 'blueFill'}
//               text={state.isEdit ? 'Batal' : 'Request Pickup'}
//               onClick={() =>
//                 state.isEdit
//                   ? history.push('/inventory/inbound')
//                   : setState({ ...state, requestPickup: true })
//               }
//             />
//             <Button
//               style={{ minWidth: '140px', marginLeft: '10px' }}
//               styleType="blueFill"
//               text={state.isEdit ? 'Simpan' : 'Kirim'}
//               onClick={() => {
//                 state.isEdit
//                   ? setState({ ...state, isEdit: false })
//                   : setState({ ...state, sendInbound: true });
//               }}
//             />
//           </div>
//           <ModalDelete
//             show={state.deleteInbound}
//             onHide={() => setState({ ...state, deleteInbound: false })}
//             data="INB-12345"
//           />
//           <ModalSend
//             show={state.sendInbound}
//             onHide={() => setState({ ...state, sendInbound: false })}
//             onAgree={() =>
//               setState({ showFeedback: true, feedbackType: 'sendStock' })
//             }
//           />
//           <ModalRequest
//             show={state.requestPickup}
//             onHide={() => setState({ ...state, requestPickup: false })}
//             onAgree={() =>
//               setState({ showFeedback: true, feedbackType: 'pickupStock' })
//             }
//           />
//         </ContentContainer>
//       )}
//     </>
//   );
// };

// export default InboundSend;
