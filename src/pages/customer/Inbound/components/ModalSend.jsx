import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';

const ModalKirim = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal="Kirim"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        bodyStyle={{ border: 'none' }}
        sizeModal="md"
        content={(data) => (
          <p className="text-left">
            {`Apakah anda yakin akan melakukan proses pengiriman barang sendiri ke gudang?`}
          </p>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Kirim'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalKirim;
