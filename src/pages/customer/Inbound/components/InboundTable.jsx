import React, { useEffect, useState } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import Select from 'react-select';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getListStorage, getAllInbound } from '../reduxAction';
import { ReactSelect } from '../../../../components/elements/InputField';
import clsx from 'clsx';
import Badge from '../../../../components/elements/Badge';
import { DatePicker } from '../../../../components/elements/InputField';
import moment from 'moment';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const optionsStatus = [
  { value: 'all', label: 'Semua Status' },
  { value: '0', label: 'Kirim' },
  { value: '1', label: 'Pickup' },
  { value: '2', label: 'Sampai di Gudang' },
  { value: '3', label: 'Selesai' },
];

const HeaderTable = ({ handleFilter }) => {
  const history = useHistory();
  const { listStorage } = useSelector((state) => state.inbound);
  return (
    <div className="d-flex flex-row flex-wrap t align-items-center p-3 w-100">
      <div style={{ width: '200px', marginRight: '5px', marginBottom: '2px' }}>
        <DatePicker
          format="MMMM YYYY"
          views={['year', 'month']}
          noMarginBottom
          onDateChange={(date) => handleFilter('date', date)}
          placeholder="Pilih bulan"
        />
      </div>
      <div style={{ width: '200px', marginRight: '5px', marginBottom: '2px' }}>
        <ReactSelect
          placeholder="Semua Status"
          options={optionsStatus}
          onChange={(e) => handleFilter('status', e.target.value)}
        />
      </div>
      <div style={{ width: '200px', marginRight: '5px', marginBottom: '2px' }}>
        <ReactSelect
          placeholder="Semua Gudang"
          options={listStorage}
          onChange={(e) => handleFilter('storage_id', e.target.value)}
        />
      </div>
      <Button
        style={{ minWidth: '200px', marginBottom: '2px' }}
        className="ml-xl-auto"
        text="Kirim Stock"
        styleType="blueFill"
        startIcon={() => <AddIcon />}
        onClick={() => history.push('/inbound/send')}
      />
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses Inbound yang dilakukan saat ini.
      </h6>
      <Button
        style={{ minWidth: '140px' }}
        className="mt-2"
        text="Kirim Stock"
        styleType="blueFill"
        startIcon={() => <AddIcon />}
        onClick={() => history.push('/inbound/send')}
      />
    </div>
  );
};

const ActionColumn = ({ data }) => {
  const history = useHistory();
  return (
    <Button
      onClick={() => {
        history.push(`/inbound/detail/${data?.id}`);
      }}
      text="Detail"
      styleType="lightBlueFill"
    />
  );
};

const StatusColumn = ({ status = 0 }) => {
  return (
    <Badge
      label={
        status === 0
          ? 'Kirim'
          : status === 1
          ? 'Pickup'
          : status === 2
          ? 'Sampai di Gudang'
          : 'Selesai'
      }
      styleType={
        status === 0
          ? 'black'
          : status === 1
          ? 'yellow'
          : status === 2
          ? 'blue'
          : 'green'
      }
      size="medium"
      style={{ fontSize: '14px' }}
    />
  );
};

function InboundTable() {
  const dispatch = useDispatch();
  const { inbounds } = useSelector((state) => state.inbound);
  const [state, setState] = useState({
    status: 'all',
    storage_id: 'all',
    date: '',
  });

  const handleFilter = (type, value) => {
    if (type === 'date') {
      if (value) {
        setState({ ...state, date: moment(value).format('YYYY-MM') });
      } else {
        setState({ ...state, date: '' });
      }
    } else if (type === 'storage_id') {
      setState({ ...state, storage_id: value });
    } else if (type === 'status') {
      setState({ ...state, status: value });
    }
  };

  const fetchListInbound = (params) => {
    dispatch(getAllInbound(params));
  };
  useEffect(() => {
    dispatch(getListStorage());
  }, []);

  useEffect(() => {
    console.log('inbounds :>> ', inbounds);
  }, [inbounds]);

  return (
    <div>
      <Table
        action={fetchListInbound}
        totalPage={inbounds?.last_page || 1}
        params={{
          date: state.date !== '' ? state.date : null,
          status: state.status !== 'all' ? state.status : null,
          storage_id: state.storage_id !== 'all' ? state.storage_id : null,
        }}
        column={[
          {
            heading: 'Tanggal',
            key: 'date_transform',
          },
          {
            heading: 'Kode Inbound',
            key: 'order_number',
          },
          {
            heading: 'Gudang',
            key: 'warehouseName',
          },
          {
            heading: 'Total Produk',
            key: 'total_sku',
          },
          {
            heading: 'Total Item',
            key: 'total_items',
          },
          {
            heading: 'Status',
            render: (data) => <StatusColumn status={data.status} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionColumn data={data} />,
          },
        ]}
        transformData={(item) => ({
          ...item,
          date_transform: moment(item.date).format('DD MMMM YYYY  HH:MM:ss'),
          warehouseName: item?.storage?.warehouse?.name,
        })}
        data={inbounds?.data || []}
        renderHeader={() => <HeaderTable handleFilter={handleFilter} />}
        withNumber={false}
        renderEmptyData={() => <EmptyData />}
      />
    </div>
  );
}

export default InboundTable;
