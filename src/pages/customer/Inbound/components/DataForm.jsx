import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Row, Col } from 'react-bootstrap';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import MapModal from '../../../../components/elements/Map/MapModal';
import moment from 'moment';
import localization from 'moment/locale/id';

const useStyles = makeStyles({
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4f4f4f',
  },
});

function DataForm(props) {
  const { data, page } = props;
  const [state, setState] = useState({ showMap: false, initialCoordinate: {} });
  const history = useHistory();
  const classes = useStyles();
  const { detailInbound } = useSelector((state) => state.inbound);
  const handleShowMap = (type) => {
    if (type === 'sender') {
      setState({
        ...state,
        initialCoordinate: {
          lat: detailInbound?.latitude,
          lng: detailInbound?.longitude,
        },
        showMap: true,
      });
    } else {
      setState({
        ...state,
        initialCoordinate: {
          lat: detailInbound?.storage?.warehouse?.latitude,
          lng: detailInbound?.storage?.warehouse?.longitude,
        },
        showMap: true,
      });
    }
  };
  return (
    <>
      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom="3"
        className="p-3"
        style={{ minHeight: '500px' }}
      >
        <div className="pt-3 pb-3">
          <h6 className="mb-0">Data Pengirim</h6>
        </div>
        <div className="mt-1">
          <h6 className={classes.label}>Nama Pelanggan</h6>
          <h6 className={classes.value}>{detailInbound?.name || '-'}</h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Nomor Telepon</h6>
          <h6 className={classes.value}>
            {detailInbound?.phone_number || '-'}
          </h6>
        </div>
        <div className="mt-3">
          <h6 className={classes.label}>Email</h6>
          <h6 className={classes.value}>{detailInbound?.email || '-'}</h6>
        </div>
        <Button
          styleType="blueNoFill"
          className="p-0"
          text="Lihat Lokasi di Peta"
          startIcon={() => <MapOutlinedIcon />}
          onClick={() => handleShowMap('sender')}
        />
        <Row>
          <Col xs={6}>
            <div className="mt-3">
              <h6 className={classes.label}>Kota</h6>
              <h6 className={classes.value}>
                {detailInbound?.city?.name || '-'}
              </h6>
            </div>
          </Col>
          <Col xs={6}>
            <div className="mt-3">
              <h6 className={classes.label}>Provinsi</h6>
              <h6 className={classes.value}>
                {detailInbound?.province?.name || '-'}
              </h6>
            </div>
          </Col>
          <Col xs={12}>
            <div className="mt-3">
              <h6 className={classes.label}>Pickup Order</h6>
              <h6 className={classes.value}>
                {detailInbound?.date_pickup
                  ? moment(detailInbound?.date_pickup)
                      .locale('id', localization)
                      .format('dddd, DD MMMM yyyy')
                  : '-'}
              </h6>
            </div>
          </Col>
        </Row>
      </ContentItem>

      <ContentItem
        spaceLeft="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom="3"
        className="p-3"
        style={{ minHeight: '500px' }}
      >
        {page === 'detail' ? (
          <>
            <div className="pt-3 pb-3">
              <h6 className="mb-0">Data Gudang</h6>
            </div>
            <div className="mt-1">
              <h6 className={classes.label}>Tujuan Gudang</h6>
              <h6 className={classes.value}>
                {detailInbound?.storage?.warehouse?.name || '-'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Nomor Telepon</h6>
              <h6 className={classes.value}>
                {detailInbound?.storage?.warehouse?.phone_number || '-'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Email</h6>
              <h6 className={classes.value}>
                {' '}
                {detailInbound?.storage?.warehouse?.email || '-'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.label}>Alamat</h6>
              <h6 className={classes.value}>
                {detailInbound?.storage?.warehouse?.address || '-'}
              </h6>
            </div>
            <Button
              styleType="blueNoFill"
              className="p-0"
              text="Lihat Lokasi di Peta"
              startIcon={() => <MapOutlinedIcon />}
              onClick={() => handleShowMap('warehouse')}
            />
          </>
        ) : (
          <div className="mt-3">
            <h6 className={classes.label}>Alamat</h6>
            <h6 className={classes.value}>{data?.sender || '-'}</h6>
          </div>
        )}
      </ContentItem>
      <MapModal
        show={state.showMap}
        onHide={() =>
          setState({ ...state, showMap: false, initialCoordinate: {} })
        }
        initialCoordinate={state.initialCoordinate}
        mode="viewer"
      />
    </>
  );
}
DataForm.defaultProps = {
  data: {},
  page: '',
};
export default DataForm;
