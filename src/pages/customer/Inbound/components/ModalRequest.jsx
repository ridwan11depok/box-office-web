import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';

const ModalRequest = (props) => {
  return (
    <>
      <Modal
        titleModal="Request Pickup"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <p className="text-left">
            {`Apakah anda yakin akan melakukan proses request pickup 
            dan barang akan dijemput oleh pihak gudang?`}
          </p>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType="blueFill"
              text="Request Pickup"
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalRequest;
