import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { TextArea } from '../../../../components/elements/InputField';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { cancelInbound } from '../reduxAction';
import { useHistory } from 'react-router-dom';

const validationSchema = Yup.object({
  reason: Yup.string().required(
    'Harap untuk mengisi alasan membatalkan inbound'
  ),
});

const ModalCancelInbound = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isLoading } = useSelector((state) => state.loading);

  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  const handleSubmit = async (values) => {
    try {
      await dispatch(cancelInbound(values, props.data?.id));
      props.onHide();
      history.push('/inbound');
    } catch (err) {}
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Batalkan Inbound"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <TextArea
            label="Alasan Membatalkan"
            placeholder="Input alasan membatalkan"
            formik={formik}
            withValidate
            name="reason"
          />
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text={isLoading ? 'Loading...' : 'Batalkan'}
              onClick={formik.handleSubmit}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

ModalCancelInbound.defaultProps = {
  data: {},
};
export default ModalCancelInbound;
