import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import Counter from '../../../../components/elements/Counter';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { AddSKUModal } from '../../../../components/elements/Modal';
import ModalSelectSKU from './ModalSelectSKU';
import { useSelector } from 'react-redux';
import { setSelectedSKUAction } from '../reduxAction'
import { useDispatch } from 'react-redux';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  status: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
  },
  saved: {
    color: '#1c1c1c',
  },
  transit: {
    color: '#F57F17',
  },
  received: {
    color: '#1B5E20',
  },
});

const HeaderTable = (props) => {
  const { isEdit, onClickSelectSKU } = props;
  const { detailWarehouse } = useSelector((state) => state.gudang);
  return (
    <div className="d-flex flex-row flex-wrap justify-content-between align-items-center p-3 w-100">
      <h6 className="mb-0">{detailWarehouse?.name || 'No Data'}</h6>
      {isEdit && (
        <Button
          style={{ minWidth: '140px', marginBottom: '2px' }}
          className="ml-md-auto"
          text="Pilih SKU"
          styleType="blueOutline"
          startIcon={() => <AddIcon />}
          onClick={() => onClickSelectSKU()}
        />
      )}
    </div>
  );
};

const EmptyData = ({ onClickSelectSKU }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Belum ada SKU.</h6>
      <Button
        style={{ minWidth: '140px' }}
        className="mt-2"
        text="Pilih SKU"
        styleType="blueOutline"
        startIcon={() => <AddIcon />}
        onClick={() => onClickSelectSKU()}
      />
    </div>
  );
};

const QuantityColumn = ({ data, isEdit, handleCounter, handleDelete }) => {
  return isEdit ? (
    <div className="d-flex">
      <Counter
        onDecrease={() => handleCounter(data, '-')}
        onIncrease={() => handleCounter(data, '+')}
        value={data.qty}
      />
      <div
        className="p-1 rounded button ml-1 d-flex align-items-center"
        style={{ border: '2px solid #192A55', width: 'min-content', cursor: 'pointer' }}
        onClick={() => handleDelete(data.id)}
      >
        <i
          className="far fa-trash-alt"
          style={{ fontSize: '20px', color: '#192A55', cursor: 'pointer' }}
        ></i>
      </div>
    </div>
  ) : (
    <span>{data.quantity_shipped}</span>
  );
};

const ActionColumn = ({ data }) => {
  return (
    <div
      className="p-1 rounded button"
      style={{ border: '2px solid red', width: 'min-content' }}
    >
      <i
        className="far fa-trash-alt"
        style={{ fontSize: '20px', color: 'red', cursor: 'pointer' }}
      ></i>
    </div>
  );
};

function SKUTable(props) {
  const { isEdit, handleSelectProductSKU } = props;
  const [state, setState] = useState({
    showSelectSKU: false,
  });
  const [selected, setSelected] = useState([]);
  const { detailInbound, selectedSKU } = useSelector((state) => state.inbound);
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setSelectedSKUAction(selected))
  }, [selected]);

  useEffect(() => {
    if(selectedSKU?.length) setSelected(selectedSKU)
  }, [selectedSKU]);

  const handleSelectSKU = (selectedSKU = []) => {
    let newSelected = [];
    selectedSKU.forEach((itemSKU) => {
      newSelected = [...newSelected, { ...itemSKU, qty: 1 }];
    });
    setSelected(newSelected);
    handleSelectProductSKU(newSelected);
  };

  const handleCounter = (itemSKU, action = '+') => {
    if (action === '+' && itemSKU.qty >= 0) {
      const newSKU = selected.map((item) => {
        if (item.id === itemSKU.id) {
          return {
            ...item,
            qty: itemSKU.qty + 1,
          };
        } else {
          return item;
        }
      });
      setSelected(newSKU);
      handleSelectProductSKU(newSKU);
    } else if (action === '-' && itemSKU.qty > 0) {
      const newSKU = selected.map((item) => {
        if (item.id === itemSKU.id) {
          return {
            ...item,
            qty: itemSKU.qty - 1,
          };
        } else {
          return item;
        }
      });
      setSelected(newSKU);
      handleSelectProductSKU(newSKU);
    }
  };

  const handleDeleteSKUItem = (skuId) => {
    const newSKU = selected.filter((item) => item.id !== skuId);
    setSelected(newSKU);
    handleSelectProductSKU(newSKU);
  };

  const transformData = (item) => {
    if (isEdit) {
      return {
        ...item,
        size: `${item?.length || 0}cm x${item?.width || 0}cm ${item?.height ? `x${item?.height}cm` : ''
          }`,
      };
    } else {
      return {
        ...item,
        sku: item?.product?.sku,
        name: item?.product?.name,
        size: `${item?.product?.length || 0}cm x ${item?.product?.width || 0
          }cm ${item?.product?.height ? `x ${item?.product?.height}cm` : ''}`,
      };
    }
  };
  return (
    <div>
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'sku',
          },
          {
            heading: 'Produk',
            key: 'name',
          },
          {
            heading: 'Ukuran',
            key: 'size',
          },
          {
            heading: 'Tambahan Qty',
            render: (data) => (
              <QuantityColumn
                isEdit={isEdit}
                data={data}
                handleCounter={handleCounter}
                handleDelete={handleDeleteSKUItem}
              />
            ),
          },
        ]}
        data={isEdit ? selected : detailInbound?.item || []}
        // data={selected}
        transformData={transformData}
        renderHeader={() => (
          <HeaderTable
            onClickSelectSKU={() => setState({ ...state, showSelectSKU: true })}
            isEdit={isEdit}
          />
        )}
        withNumber={false}
        showFooter={false}
        renderEmptyData={() => (
          <EmptyData
            onClickSelectSKU={() => setState({ ...state, showSelectSKU: true })}
          />
        )}
      />
      <ModalSelectSKU
        show={state.showSelectSKU}
        onHide={() => setState({ ...state, showSelectSKU: false })}
        onAgree={handleSelectSKU}
        // initialValues={selected}
      />
    </div>
  );
}

SKUTable.defaultProps = {
  handleSelectProductSKU: () => { },
};
export default SKUTable;
