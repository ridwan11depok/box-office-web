import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';

import { useHistory } from 'react-router-dom';
import {
  TextField,
  SelectField,
  TextArea,
  ReactSelect,
  WarehouseSelect,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import Button from '../../../../components/elements/Button';
import SwitchToggle from '../../../../components/elements/SwitchToggle';
import Banner from '../../../../components/elements/Banner';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import MapModal from '../../../../components/elements/Map/MapModal';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../../administrator/Gudang/reduxAction';
import { getListStorage } from '../reduxAction';
import { getListWarehouse } from '../../Gudang/reduxAction';
import DatePicker from '../../../../components/elements/InputField/DatePicker';
import moment from 'moment';

const useStyles = makeStyles({
  textValue: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    marginBottom: '15px',
  },
  textLabel: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: '5px',
  },
});

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);

// const validationSchema = Yup.object({
//   name: Yup.string().required('Harap untuk mengisi nama pelanggan'),
//   country_code: Yup.string().required('Required'),
//   phone_number: Yup.string()
//     .matches(phoneRegex, 'Nomor telepon tidak valid')
//     .required('Harap untuk mengisi nomor telepon pelanggan'),
//   longitude: Yup.number().required('Harap untuk memilih koordinat lokasi'),
//   latitude: Yup.number().required('Harap untuk memilih koordinat lokasi'),
//   address: Yup.string().required('Harap untuk mengisi lokasi'),
//   province_id: Yup.number().required('Harap untuk mengisi provinsi'),
//   city_id: Yup.number().required('Harap untuk mengisi kota'),
//   // is_request_pickup: Yup.number().required('Required'),
//   storage_id: Yup.number().required('Harap untuk memilih gudang'),
// });

const InboundSend = (props) => {
  const { formik } = props;
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = useState({
    showMap: false,
    provice_id: 0,
    city_id: 0,
    district_id: 0,
    is_request_pickup: formik?.getFieldProps('is_request_pickup')?.value ? formik?.getFieldProps('is_request_pickup')?.value : 0,
    listStorage: [],
    warehouseSelected: null,
    showMapWarehouse: false,
  });

  
  const dispatch = useDispatch();
  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
    );
    
    const fetchListStorage = async () => {
      try {
        const res = await dispatch(getListStorage());
        setState({
          ...state,
          listStorage: res.map((item) => ({
            ...item,
            name: item.warehouse?.name,
            address: item.warehouse?.address,
            status: item.warehouse?.status === 1 ? 'open' : 'tutup',
          })),
        });
      } catch (err) { }
    };
    useEffect(() => {
      if(state.listStorage?.length){
        if(formik?.getFieldProps('storage_id')?.value){
          const find = state.listStorage.find((item) => item.id === formik?.getFieldProps('storage_id')?.value)
          setState({...state, warehouseSelected: find})
        }
      }
    }, [state.listStorage]);
  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
    fetchListStorage();
  }, []);
  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(Number(state.provice_id), Number(e.target.value)));
  };
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  }


  const selectCoordinate = (coordinate) => {
    formik.setFieldValue('longitude', coordinate.lng);
    formik.setFieldValue('latitude', coordinate.lat);
    setState({ ...state, showMap: false });
  };
  const handleCheckedRequestPickup = (checked) => {
    const isChecked = checked ? 1 : 0;
    formik.setFieldValue('is_request_pickup', isChecked);
    if (checked) {
      formik.setFieldValue('date_pickup', moment().format('yyyy-MM-DD'));
    } else {
      formik.setFieldValue('date_pickup', '');
    }
    setState({ ...state, is_request_pickup: isChecked });
  };
  const handleSelectWarehouse = (warehouseSelected) => {
    setState({ ...state, warehouseSelected });
    formik.setFieldValue('storage_id', warehouseSelected.id);
    dispatch(getListWarehouse({}, warehouseSelected?.warehouse_id));
  };
  const handleValueArea = (area_id = null, listArea = []) => {
    if (area_id) {
      const find = listArea.find((e) => e.value === area_id)
      return find
    }
  }



  return (
    <>
      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: '700px' }}
        className="p-3"
      >
        <div className="pt-3 pb-3">
          <h6 className="mb-0">Data Pengirim</h6>
        </div>
        <TextField
          className="mt-1 mb-2"
          label="Nama Pelanggan"
          placeholder="Input nama pelanggan"
          name="name"
          withValidate
          formik={formik}
        />
        <Row>
          <Col>
            <SelectField
              label="Nomor Telepon Pelanggan"
              options={[
                {
                  value: '1',
                  description: 'Indonesia(+62)',
                },
              ]}
              name="country_code"
              formik={formik}
              withValidate
            />
          </Col>
          <Col className="d-flex align-items-end">
            <TextField
              className="w-100"
              placeholder="Input nomor telepon"
              name="phone_number"
              formik={formik}
              withValidate
              withError={false}
            />
          </Col>
          <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
            {formik.errors.phone_number && formik.touched.phone_number && (
              <Form.Text className="text-input-error">
                {formik.errors.phone_number}
              </Form.Text>
            )}
          </Col>
        </Row>
        <div className="background-map mb-3">
          <div
            className="d-flex flex-column flex-lg-row justify-content-between align-items-center w-100 p-3"
            style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
          >
            <span
              style={{
                fontSize: '14px',
                fontWeight: '600',
                fontFamily: 'Open Sans',
              }}
            >
              Tandai lokasi peta Anda agar alamat lebih akurat.
            </span>
            <Button
              style={{ width: '200px', backgroundColor: '#FAFAFF' }}
              styleType="blueNoFill"
              text="Tandai Lokasi"
              onClick={() => setState({ ...state, showMap: true })}
            />
          </div>
        </div>
        {formik.touched.latitude && formik.errors.latitude && (
          <Form.Text className="text-input-error mb-3 mt-0">
            {formik.errors.latitude}
          </Form.Text>
        )}
        <TextArea
          className="mt-1 mb-2"
          label="Alamat Pengirim"
          placeholder="Input alamat"
          withValidate
          name="address"
          formik={formik}
        />
        <Row>
          <Col xs={12} md={12} lg={4}>
            <ReactSelect
              value={handleValueArea(formik?.getFieldProps('province_id')?.value ? formik?.getFieldProps('province_id')?.value : null, listProvince)}
              className="mt-1 mb-2"
              label="Provinsi"
              placeholder="Pilih provinsi"
              onChange={handleChangeProvince}
              options={listProvince}
              name="province_id"
              formik={formik}
              withValidate
            />
          </Col>
          <Col xs={12} md={12} lg={4}>
            <ReactSelect
            value={handleValueArea(formik?.getFieldProps('city_id')?.value ? formik?.getFieldProps('city_id')?.value : null, listCity)}
              className="mt-1 mb-2"
              label="Kota"
              placeholder="Pilih kota"
              onChange={handleChangeCity}
              name="city_id"
              formik={formik}
              withValidate
              options={listCity}
            />
          </Col>
          <Col xs={12} md={12} lg={4}>
            <ReactSelect
            value={handleValueArea(formik?.getFieldProps('district_id')?.value ? formik?.getFieldProps('district_id')?.value : null, listDistrict)}
              className="mt-1 mb-2"
              label="Kecamatan"
              placeholder="Pilih Kecamatan"
              onChange={handleChangeDistrict}
              name="district_id"
              formik={formik}
              withValidate
              options={listDistrict}
            />
          </Col>
        </Row>
      </ContentItem>
      <ContentItem
        spaceBottom={3}
        spaceLeft="0 pl-md-2"
        col="col-12 col-md-6"
        className="p-0 d-flex flex-column"
        style={{ minHeight: '700px' }}
        border={false}
      >
        <div style={{ border: '1px solid #E8E8E8' }} className="rounded p-3">
          <div className="pt-3 pb-3">
            <h6 className="mb-0">Pickup Order</h6>
          </div>
          <Row>
            <Col col={12}>
              <Banner
                description="Pickup order merupakan fitur yang mana pihak dari kami menjemput barang ke seller."
                button={{ isShow: false }}
                className="w-100"
                styleType="blue"
                infoIcon
              />
            </Col>
          </Row>
          <Row className='mt-2'>
            <Col xs={6}>
              <Form.Label as={'h6'} className="input-label">
                Pickup Order
              </Form.Label>
            </Col>
            <Col xs={6} className="d-flex justify-content-end">
              <SwitchToggle
                onChange={handleCheckedRequestPickup}
                name="is_request_pickup"
                // defaultValue={state.is_request_pickup}
                defaultValue={
                  state.is_request_pickup === 1 ? true : false
                }
              />
            </Col>
            {state.is_request_pickup === 1 && (
              <Col xs={12} className="mt-2">
                <DatePicker
                  value={formik.getFieldProps('date_pickup')?.value && formik.getFieldProps('date_pickup')?.value}
                  onDateChange={(date) => {
                    formik.setFieldValue(
                      'date_pickup',
                      moment(date).format('yyyy-MM-DD')
                    )}}
                />
              </Col>
            )}
          </Row>
        </div>
        <div
          style={{ border: '1px solid #E8E8E8', flex: 1 }}
          className="rounded p-3 mt-3"
        >
          <div className="pt-3 pb-3">
            <h6 className="mb-0">Data Gudang</h6>
          </div>
          <Form.Label as={'h6'} className="input-label">
            Pilih Gudang
          </Form.Label>
          <span>Tentukan gudang yang akan menjadi tujuan penyimpanan produk anda</span>
          <WarehouseSelect
            defaultValue={state.warehouseSelected && state.warehouseSelected}
            style={{ width: '100%', marginTop: '5px' }}
            options={state.listStorage}
            onChange={handleSelectWarehouse}
          />
          {formik.errors.storage_id && formik.touched.storage_id && (
            <Form.Text className="text-input-error mb-3">
              {formik.errors.storage_id}
            </Form.Text>
          )}
        </div>
        <div
          style={{ border: '1px solid #E8E8E8', flex: 1 }}
          className="rounded p-3 mt-3"
        >
          <h6 className={classes.textLabel}>Nomor Telepon</h6>
          <h6 className={classes.textValue}>
            {state.warehouseSelected?.warehouse?.phone_number || '-'}
          </h6>
          <h6 className={classes.textLabel}>Email</h6>
          <h6 className={classes.textValue}>
            {state.warehouseSelected?.warehouse?.email || '-'}
          </h6>
          <h6 className={classes.textLabel}>Alamat Gudang</h6>
          <h6 className={classes.textValue}>
            {state.warehouseSelected?.warehouse?.address || '-'}
          </h6>
          <Button
            styleType="blueNoFill"
            className="p-0"
            text="Lihat Lokasi di Peta"
            startIcon={() => <MapOutlinedIcon />}
            onClick={() => setState({ ...state, showMapWarehouse: true })}
          />
        </div>
      </ContentItem>

      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        onAgree={selectCoordinate}
        initialCoordinate={{
          lng: props.initialValues?.longitude,
          lat: props.initialValues?.latitude,
        }}
      />
      <MapModal
        show={state.showMapWarehouse}
        onHide={() => setState({ ...state, showMapWarehouse: false })}
        initialCoordinate={{
          lng: state.warehouseSelected?.warehouse?.longitude,
          lat: state.warehouseSelected?.warehouse?.latitude,
        }}
        mode="viewer"
      />
    </>
  );
};

InboundSend.defaultProps = {};
export default InboundSend;
