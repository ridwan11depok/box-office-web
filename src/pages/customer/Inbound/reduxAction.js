import actionType from "./reduxConstant";
import consume from "../../../api/consume";
import { setLoading, setLoadingTable, setToast } from "../../../redux/actions";

export const getAllInbound =
  (params = {}, inboundId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (inboundId) {
        result = await consume.getWithParams(
          "inboundCustomer",
          {},
          { token },
          { store_id: storeId },
          inboundId
        );
        dispatch(setDetailInbound(result.result));
      } else {
        result = await consume.getWithParams(
          "inboundCustomer",
          {},
          { token },
          { ...params, store_id: storeId }
        );
        dispatch(setInbound(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      if (inboundId) {
        dispatch(setDetailInbound({}));
      } else {
        dispatch(setInbound({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setInbound(data) {
  return {
    type: actionType.INBOUND_CUSTOMER,
    payload: data,
  };
}
function setDetailInbound(data) {
  return {
    type: actionType.DETAIL_INBOUND_CUSTOMER,
    payload: data,
  };
}

export const getListStorage = () => async (dispatch, getState) => {
  const { token, dataUser } = getState().login;
  const storeId = dataUser?.store?.[0]?.id;
  dispatch(setLoading(true));
  try {
    const result = await consume.getWithParams(
      "listStorageCustomer",
      {},
      { token },
      { store_id: storeId }
    );
    dispatch(setListStorage(result.result));
    dispatch(setLoading(false));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStorage([]));
    dispatch(setLoading(false));
    return Promise.reject(err);
  }
};

function setListStorage(data) {
  return {
    type: actionType.LIST_STORAGE_CUSTOMER,
    payload: data,
  };
}

export const setDataPageInboundDetail = (data) => {
  return {
    type: actionType.SET_DATA_PAGE_INBOUND_DETAIL,
    payload: data,
  };
};

export const getListAllProductSKU =
  (params = {}) =>
  async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      dispatch(setLoadingTable(true));
      let data = {};
      let headers = { token: token };
      let response = await consume.getWithParams(
        "listAllProductSku",
        data,
        headers,
        {
          ...params,
          store_id: storeId,
        }
      );
      if (response) {
        dispatch(listAllProductSKU(response.result));
      }
      dispatch(setLoadingTable(false));
    } catch (err) {
      dispatch(setLoadingTable(false));
      dispatch(listAllProductSKU([]));
    }
  };

const listAllProductSKU = (data) => {
  return {
    type: actionType.LIST_ALL_PRODUCT_SKU_CUSTOMER,
    payload: data,
  };
};

export const addInbound = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: "multipart/form-data" };

    const response = await consume.post("inboundCustomer", data, headers);
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: "Berhasil menambahkan inbound baru.",
        type: "success",
      })
    );
    dispatch(getAllInbound({ page: 1, per_page: 10 }));
    return Promise.resolve(response);
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: "Gagal menambahkan inbound baru.",
        type: "error",
      })
    );
    return Promise.reject("failed");
  }
};

export const cancelInbound =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      let data = { ...payload, store_id: storeId };
      let headers = { token: token };
      const response = await consume.post("inboundCustomer", data, headers, id);
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: "Berhasil membatalkan inbound",
          type: "success",
        })
      );
      return Promise.resolve("success");
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: `${err?.error?.message || "Gagal membatalkan inbound"}`,
          type: "error",
        })
      );
      return Promise.reject("failed");
    }
  };

export function setSelectedSKUAction(data) {
  return {
    type: actionType.SELECTED_SKU,
    payload: data,
  };
}
