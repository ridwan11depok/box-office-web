import actionType from './reduxConstant';

const initialState = {
  inbounds: {},
  detailInbound: {},
  pageInboundDetail: {},
  listStorage: [],
  selectedSKU: [],
  listAllProductSku: [],
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.INBOUND_CUSTOMER:
      return {
        ...prevState,
        inbounds: action.payload,
      };
    case actionType.DETAIL_INBOUND_CUSTOMER:
      return {
        ...prevState,
        detailInbound: action.payload,
      };
    case actionType.LIST_STORAGE_CUSTOMER: {
      const listStorage = action.payload.map((item) => ({
        value: item.id,
        label: item?.warehouse?.name,
      }));
      return {
        ...prevState,
        listStorage: [{ value: 'all', label: 'Semua Gudang' }, ...listStorage],
      };
    }
    case actionType.LIST_ALL_PRODUCT_SKU_CUSTOMER:
      return {
        ...prevState,
        listAllProductSku: action.payload,
      };
    case actionType.SET_DATA_PAGE_INBOUND_DETAIL:
      return {
        ...prevState,
        pageInboundDetail: action.payload,
      };
    case actionType.SELECTED_SKU:
      return {
        ...prevState,
        selectedSKU: action.payload
      }
    default:
      return prevState;
  }
};

export default inboundReducer;
