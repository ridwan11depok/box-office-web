import React from 'react';
import './style/style.css';
import { Row, Col, Form, Button, Select, Pagination } from 'react-bootstrap';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
const PengajuanReimburse = () => {
  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-12">
            <div className="section-stock">
              <h1>History Reimburse</h1>
            </div>
          </div>
        </div>
        <div className="row" style={{ marginBottom: '20px' }}>
          <div className="col-lg-6">
            <div
              className="row"
              style={{
                border: '1px  solid #E8E8E8',
                paddingTop: '5px',
                paddingLeft: '5px',
                borderRadius: '5px',
              }}
            >
              <div
                className="col-lg-6"
                style={{
                  backgroundColor: '#EBF0FA',
                  borderRadius: '4px',
                  height: '40px',
                }}
              >
                <div style={{ paddingTop: '10px' }}>
                  <p className="text-reimburse-pengajuan">
                    Pengajuan Reimburse
                  </p>
                </div>
              </div>
              <div className="col-lg-6">
                <div style={{ paddingTop: '10px' }}>
                  <p className="text-reimburse-history">History Reimburse</p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div style={{ textAlign: 'end' }}>
              <button
                type="button"
                className="btn"
                style={{
                  backgroundColor: '#192A55',
                  borderRadius: '4px',
                  borderWidth: 2,
                  borderColor: '#192A55',
                }}
              >
                <text className="btn-right">Buat Reimburse</text>
              </button>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{
            border: '1px solid #E8E8E8',
            borderRadius: '5px',
            marginBottom: '50px',
          }}
        >
          <div className="col-lg-12">
            {/* <div className="row" style={{ borderBottom: "1px solid #E8E8E8", height: "72px" }}>
                            <div className="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <Form style={{ marginTop: "15px" }}>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                        <Form.Control type="email" placeholder="Cari Product, SKU" />
                                    </Form.Group>
                                </Form>
                            </div>
                            <div className="col-lg-9 col-md-6 col-sm-6 col-xs-6" style={{ textAlign: "end", marginTop: "15px" }}>
                                <div>
                                    <button type="button" className="btn"
                                        style={{ backgroundColor: '#192A55', borderRadius: "4px", borderWidth: 2, borderColor: '#192A55' }}
                                    >
                                        <text className="btn-right">Manual Update</text>
                                    </button>
                                </div>
                            </div>
                        </div> */}
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-2">
                <p className="table-heading">Tanggal</p>
              </div>
              <div className="col-lg-2">
                <p className="table-heading">Gudang</p>
              </div>
              <div className="col-lg-2">
                <p className="table-heading">Nominal</p>
              </div>
              <div className="col-lg-2">
                <p className="table-heading">Tipe</p>
              </div>
              <div className="col-lg-2">
                <p className="table-heading">Keterangan</p>
              </div>
              <div className="col-lg-2">
                <p className="table-heading">Status</p>
              </div>
            </div>
            <div
              className="row"
              style={{ height: '440px', borderBottom: '1px solid #E8E8E8' }}
            >
              <div
                className="col-lg-12"
                style={{ padding: '200px', textAlign: 'center' }}
              >
                <p>Belum ada data reimburse saat ini</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-10">
                <text className="text-view">
                  View
                  {/* <img src={dropdown} style={{ paddingLeft: "10px", paddingRight: "10px", marginTop: "5px", marginBottom: "5px" }} /> data per page */}
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-2">
                <text className="pagination">
                  <img src={prev} style={{ paddingRight: '10px' }} />
                  Prev
                  <img
                    src={pagination}
                    style={{
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      marginTop: '5px',
                      marginBottom: '5px',
                    }}
                  />
                  Next
                  <img src={next} style={{ paddingLeft: '10px' }} />
                </text>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default PengajuanReimburse;
