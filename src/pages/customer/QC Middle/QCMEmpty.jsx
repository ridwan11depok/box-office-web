import React from 'react';
import './style/style.css';
import { Row, Col, Form, Button, Select, Pagination } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import dropdown from '../../../assets/icons/Dropdown.svg';
import next from '../../../assets/icons/next.svg';
import prev from '../../../assets/icons/prev.svg';
import pagination from '../../../assets/icons/pagination.svg';
import exportmutasi from '../../../assets/icons/exportmutasi.svg';
import date from '../../../assets/icons/date.svg';

const QCMEmpty = () => {
  const history = useHistory();
  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-8 col-md-6 col-sm-12 col-xs-12">
            <div className="section-stock">
              <h1>QC Middle</h1>
            </div>
          </div>
          <div className="col-lg-2 col-md-6 col-sm-12 col-xs-12">
            <div
              style={{
                marginTop: '15px',
                paddingRight: '15px',
                float: 'right',
              }}
            >
              <text>Pilih Gudang :</text>
            </div>
          </div>
          <div
            className="col-lg-2 col-md-6 col-sm-12 col-xs-12"
            style={{
              border: '1px solid #E8E8E8',
              borderRadius: '5px',
              height: '50px',
              display: 'flex',
            }}
          >
            <div>
              <img
                src={gudang}
                style={{ marginLeft: '-12px', marginTop: '3px' }}
              />
            </div>
            <div className="jarak-gudang">
              <span className="gudang-a">Gudang A</span>
              <br></br>
              <span className="lokasi-gudang">Bandung, Jawa Barat</span>
            </div>
            <div
              style={{ position: 'absolute', right: '0', marginRight: '3PX' }}
            >
              <img src={gudangdropdown} style={{ marginTop: '3px' }} />
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{ border: '1px solid #E8E8E8', borderRadius: '5px' }}
        >
          <div className="col-lg-12">
            <div
              className="row"
              style={{ borderBottom: '1px solid #E8E8E8', height: '72px' }}
            >
              <div className="col-lg-6" style={{ marginTop: '15px' }}>
                <button
                  type="button"
                  className="btn"
                  style={{
                    height: '40px',
                    backgroundColor: '#192A55',
                    borderRadius: '4px',
                    borderWidth: 2,
                    borderColor: '#192A55',
                  }}
                >
                  <text className="btn-request-qc">Buat Request QC</text>
                </button>
              </div>
              <div
                className="col-lg-6"
                style={{ textAlign: 'end', marginTop: '15px' }}
              >
                <div>
                  <button
                    type="button"
                    className="btn"
                    style={{
                      borderRadius: '4px',
                      borderWidth: 2,
                      borderColor: '#192A55',
                      height: '40px',
                    }}
                  >
                    <text className="btn-export">
                      <img
                        src={exportmutasi}
                        style={{ paddingRight: '10px' }}
                      />
                      Export QC Middle
                    </text>
                  </button>
                </div>
              </div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Status</label>
                  <select className="form-control">
                    <option>Semua Status</option>
                    <option>Status 1</option>
                    <option>Status 2</option>
                  </select>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Dari tanggal</label>
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <div className="input-group-text">
                        <img
                          src={date}
                          style={{ marginRight: '-15px', marginLeft: '-15px' }}
                        />
                      </div>
                    </div>
                    <input type="text" className="form-control daterange-cus" />
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="form-group">
                  <label className="table-heading">Sampai tanggal</label>
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <div className="input-group-text">
                        <img
                          src={date}
                          style={{ marginRight: '-15px', marginLeft: '-15px' }}
                        />
                      </div>
                    </div>
                    <input type="text" className="form-control daterange-cus" />
                  </div>
                </div>
              </div>
            </div>
            <div className="row" style={{ borderBottom: '1px solid #E8E8E8' }}>
              <div className="col-lg-4">
                <p className="table-heading">Tanggal</p>
              </div>
              <div className="col-lg-4">
                <p className="table-heading">Total SKU</p>
              </div>
              <div className="col-lg-4">
                <p className="table-heading">Status</p>
              </div>
            </div>
            <div
              className="row"
              style={{ height: '440px', borderBottom: '1px solid #E8E8E8' }}
            >
              <div
                className="col-lg-12"
                style={{ padding: '200px', textAlign: 'center' }}
              >
                <p>Saat ini anda belum memiliki produk yang dilakukan QC</p>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-10">
                <text className="text-view">
                  View
                  {/* <img src={dropdown} style={{ paddingLeft: "10px", paddingRight: "10px", marginTop: "5px", marginBottom: "5px" }} /> data per page */}
                  <div
                    className="form-group"
                    style={{
                      marginTop: '10px',
                      maxWidth: '100px',
                      paddingLeft: '15px',
                      paddingRight: '15px',
                    }}
                  >
                    <select className="form-control">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                    </select>
                  </div>
                  data per page
                </text>
              </div>
              <div className="col-lg-2">
                <text className="pagination">
                  <img src={prev} style={{ paddingRight: '10px' }} />
                  Prev
                  <img
                    src={pagination}
                    style={{
                      paddingLeft: '10px',
                      paddingRight: '10px',
                      marginTop: '5px',
                      marginBottom: '5px',
                    }}
                  />
                  <div
                    style={{ cursor: 'pointer' }}
                    onClick={(e) => {
                      e.preventDefault();
                      history.push('/qcm-fill');
                    }}
                  >
                    Next
                    <img src={next} style={{ paddingLeft: '10px' }} />
                  </div>
                </text>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default QCMEmpty;
