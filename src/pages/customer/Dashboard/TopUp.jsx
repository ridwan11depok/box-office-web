import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import {
  ReactSelect,
  CurrencyInput,
} from '../../../components/elements/InputField';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { formatRupiah } from '../../../utils/text';
import { Table } from '../../../components/elements/Table';
import ModalConfirmPayment from './components/ModalConfirmPayment';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  subTitle: {
    color: '#1C1C1C',
    fontSize: '16px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
});
const TopUp = () => {
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = useState({
    showModalConfirmPayment: false,
  });
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Top Up"
              onClick={() => history.push('/dashboard')}
            />
          </div>
        )}
      >
        <ContentItem className="p-0" col="col-12" spaceBottom={3}>
          <div className="p-3 mb-0">
            <h6 className={classes.subTitle}>
              Masukkan nominal yang ingin anda tambahkan ke dalam saldo
            </h6>
          </div>
          <div className="d-flex align-items-center pl-3 pr-3 pb-3 pt-0 flex-wrap">
            <div style={{ width: '300px' }}>
              <CurrencyInput
                label="Masukkan nominal"
                noMarginBottom
                className="w-100"
                placeholder="Masukkan nominal"
              />
            </div>
            <div
              style={{
                width: '300px',
                marginLeft: '10px',
                marginRight: '10px',
              }}
            >
              <ReactSelect
                className="w-100"
                label="Bank"
                options={[
                  {
                    label: 'BCA',
                    value: 1,
                  },
                ]}
                placeholder="Pilih Bank"
              />
            </div>

            <Button
              styleType="blueFill"
              className="ml-auto mt-auto"
              text="Isi Dompet"
              style={{ minWidth: '120px' }}
              onClick={() =>
                setState({ ...state, showModalConfirmPayment: true })
              }
            />
          </div>
        </ContentItem>

        <ContentItem col="col-12" spaceBottom={3} border={false}>
          <Table
            renderHeader={() => (
              <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
                <h6 className="mb-2 mt-2">Riwayat Top Up</h6>
              </div>
            )}
            column={[
              {
                heading: 'Tanggal',
                key: 'date',
              },
              {
                heading: 'Nominal',
                key: 'nominal',
              },
              {
                heading: 'Status',
                key: 'status',
              },
            ]}
            data={[]}
            withNumber={false}
          />
        </ContentItem>
      </ContentContainer>
      <ModalConfirmPayment
        show={state.showModalConfirmPayment}
        onHide={() => setState({ ...state, showModalConfirmPayment: false })}
      />
    </>
  );
};

export default TopUp;
