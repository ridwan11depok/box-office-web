import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import * as DashboardIcon from './components/Icons';
import { formatRupiah } from '../../../utils/text';
import { TabHeader } from '../../../components/elements/Tabs';
import AddIcon from '@material-ui/icons/Add';
import TableWallet from './components/TableWallet';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});
const Wallet = () => {
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = useState({
    tab: 0,
  });
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Dompet"
              onClick={() => history.push('/dashboard')}
            />
          </div>
        )}
      >
        <ContentItem className="p-0" col="col-12" spaceBottom={3}>
          <div style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="m-3">Dompet</h6>
          </div>
          <div className="d-flex align-items-center justify-between p-3">
            <DashboardIcon.WalletIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Saldo Dompet</h6>
              <h6 className={classes.value}>{formatRupiah(1000)}</h6>
            </div>
            <Button
              styleType="blueFill"
              className="ml-auto"
              text="Topup"
              onClick={() => history.push('/dashboard/topup')}
              startIcon={() => <AddIcon />}
            />
          </div>
        </ContentItem>
        <ContentItem col="col-12" spaceBottom={3}>
          <TabHeader
            listHeader={[
              {
                title: 'Dompet',
                tab: 0,
              },
              {
                title: 'Kekurangan Pembayaran',
                tab: 1,
              },
              {
                title: 'Reimburse',
                tab: 2,
              },
            ]}
            activeTab={state.tab}
            border={false}
            onChange={(tab) => setState({ tab })}
          />
        </ContentItem>
        <ContentItem col="col-12" spaceBottom={3} border={false}>
          <TableWallet tab={state.tab} />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default Wallet;
