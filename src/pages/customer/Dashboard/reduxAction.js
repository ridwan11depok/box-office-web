import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';

export const getDataDashboard =
  (params = {}) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    try {
      dispatch(setLoading(true));
      const result = await consume.getWithParams(
        'inboundWarehouse',
        {},
        { token },
        { store_id: storeId, ...params }
      );
      dispatch(setDataDashboard(result.result));
    } catch (err) {
      if (err.code === 422) {
        // console.log('errorr', err)
      }
      dispatch(setDataDashboard({}));
      // return Promise.reject(err);
    }
  };

function setDataDashboard(data) {
  return {
    type: actionType.DASHBOARD_CUSTOMER,
    payload: data,
  };
}
