import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: '#DBE3F5',
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#0D152B',
  },
}))(LinearProgress);

const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    margin: '20px 0',
  },
  logo: {
    width: '100px',
  },
  containerProgressBar: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    marginLeft: '10px',
  },
  valueProgressBar: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#363636',
    textAlign: 'right',
    marginTop: '10px',
  },
});

const dummyImageLogistic =
  'https://s3-alpha-sig.figma.com/img/9cda/7761/6d9e6d4fc22aa9fa3d2c0aa744df189c?Expires=1639353600&Signature=WQlX3JfY3saQv93bqxUtYl~7lIr5CybuAyrKKx7bg1KOaQ7gaiNeu2nM3ne6FQQeuMdZXrXmblW5YanJEft~xiAWp7~N64MhlKy9o-bk8xaxyvwBwZkCyNXkcOHYfybQi9HYHwaDfktHnCxMXD7K6A3Ij5iZLR7GyYzvwAFx~LYsuV7JFdmOBTliP2zXmdlGVBMP8UzBSZF~y8zufEU8WDUxAruenF1OOL4VS9bFpU5D3SiZrg9DkcMeXNvSz09-pG01UMEP9DodWdm52~egvZR8uQRj5vsV49JZNjgJVCTPVZ5wGBXyIA~ZwrcNMVNXWAwPZ2WN~7PQD~HPrXtVOQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA';
export default function CardLogistic() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <img className={classes.logo} alt="Logo" src={dummyImageLogistic} />
      <div className={classes.containerProgressBar}>
        <BorderLinearProgress variant="determinate" value={50} />
        <h6 className={classes.valueProgressBar}>50/100 Paket</h6>
      </div>
    </div>
  );
}
