import React from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import { formatRupiah } from '../../../../utils/text';

const useStyles = makeStyles({
  tooltipContainer: {
    border: '1px solid #3D65C7',
    padding: '8px',
    textAlign: 'center',
    minWidth: '107px',
    minHeight: '56px',
    backgroundColor: '#FAFAFF',
    borderRadius: '8px',
    boxShadow: '-3px 18px 47px -19px rgba(61,101,199,0.7)',
    '-webkitBoxShadow': '-3px 18px 47px -19px rgba(61,101,199,0.7)',
    '-mozBoxShadow': '-3px 18px 47px -19px rgba(61,101,199,0.7)',
  },
  tooltipLabel: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#363636',
  },
  tooltipValue: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#363636',
  },
});

const data = [
  {
    name: '2021-01-01T17:00:00.000Z',
    value: 14000,
  },
  {
    name: '2021-02-01T17:00:00.000Z',
    value: 24000,
  },
  {
    name: '2021-03-01T17:00:00.000Z',
    value: 4000,
  },
  {
    name: '2021-04-01T17:00:00.000Z',
    value: 9000,
  },
  {
    name: '2021-05-01T17:00:00.000Z',
    value: 4000,
  },
  {
    name: '2021-06-01T17:00:00.000Z',
    value: 7000,
  },
  {
    name: '2021-07-01T17:00:00.000Z',
    value: 10000,
  },
  {
    name: '2021-08-01T17:00:00.000Z',
    value: 11000,
  },
  {
    name: '2021-09-01T17:00:00.000Z',
    value: 4000,
  },
  {
    name: '2021-10-01T17:00:00.000Z',
    value: 34000,
  },
  {
    name: '2021-11-01T17:00:00.000Z',
  },
  {
    name: '2021-12-01T17:00:00.000Z',
  },
];

function Chart() {
  const classes = useStyles();
  const renderTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <div className={classes.tooltipContainer}>
          <h6 className={classes.tooltipLabel}>
            {moment(label).format('MMMM')}
          </h6>
          <h6 className={classes.tooltipValue}>
            {formatRupiah(payload[0].value)}
          </h6>
        </div>
      );
    }
    return null;
  };
  return (
    <div className="mt-4 mb-4">
      <ResponsiveContainer height={370} width="100%">
        <LineChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 10,
            right: 30,
            left: 20,
            bottom: 10,
          }}
        >
          <CartesianGrid vertical={false} />
          <XAxis
            dataKey="name"
            tickLine={false}
            axisLine={false}
            tickFormatter={(label) => moment(label).format('MMM')}
          />
          <YAxis
            axisLine={false}
            tickLine={false}
            tickFormatter={(label) => formatRupiah(label).substr(3)}
          />
          <Tooltip content={renderTooltip} />
          <Line
            type="monotone"
            dataKey="value"
            stroke="#3D65C7"
            strokeWidth={2}
            strokeDasharray="5 5"
            activeDot={{ r: 7 }}
            dot={{
              r: 7,
              fill: '#3D65C7',
              strokeWidth: 0,
            }}
          />
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}

export default Chart;
