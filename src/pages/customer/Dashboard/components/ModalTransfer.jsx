import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  value: {
    color: '#1C1C1C',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '10px 0',
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    textAlign: 'left',
    margin: '10px 0',
  },
  total: {
    color: '#1C1C1C',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '15px 0',
  },
});

const ModalTransfer = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  const classes = useStyles();
  return (
    <>
      <Modal
        titleModal="Transfer"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <Row>
            <Col xs={12}>
              <h6 className={classes.label}>
                Nama Bank : <span className={classes.value}>Bank BCA</span>
              </h6>
            </Col>
            <Col xs={12}>
              <h6 className={classes.label}>
                Atas Nama : <span className={classes.value}>Solehudin</span>
              </h6>
            </Col>

            <Col xs={12}>
              <h6 className={classes.label}>
                Nomor Rekening :{' '}
                <span className={classes.value}>123408123456789</span>
              </h6>
            </Col>

            <Col xs={6}>
              <h6 className={classes.total}>Total Pembayaran</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.total} style={{ textAlign: 'right' }}>
                Rp.1000
              </h6>
            </Col>
            <Col xs={12}>
              <h6 className={classes.label} style={{ fontFamily: 'Work Sans' }}>
                Mohon lakukan pembayaran kurang dari{' '}
                <span style={{ color: '#DA101A' }}>04:59</span> detik sehingga
                dompet dapat digunakan dengan segera.
              </h6>
            </Col>
          </Row>
        )}
        data={props.data}
        footer={(data) => (
          <>
            {/* <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            /> */}
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Selesai'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalTransfer;
