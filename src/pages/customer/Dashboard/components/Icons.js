import React from 'react';

export const WalletIcons = () => {
  return (
    <div>
      <svg
        width="40"
        height="40"
        viewBox="0 0 40 40"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="40" height="40" rx="20" fill="#DBE3F5" />
        <path d="M24 20H26V24H24V20Z" fill="#182953" />
        <path
          d="M28 15V13C28 11.897 27.103 11 26 11H13C11.346 11 10 12.346 10 14V26C10 28.201 11.794 29 13 29H28C29.103 29 30 28.103 30 27V17C30 15.897 29.103 15 28 15ZM13 13H26V15H13C12.7425 14.9885 12.4994 14.8781 12.3213 14.6918C12.1431 14.5055 12.0437 14.2577 12.0437 14C12.0437 13.7423 12.1431 13.4945 12.3213 13.3082C12.4994 13.1219 12.7425 13.0115 13 13ZM28 27H13.012C12.55 26.988 12 26.805 12 26V16.815C12.314 16.928 12.647 17 13 17H28V27Z"
          fill="#182953"
        />
      </svg>
    </div>
  );
};

export const TruckIcons = () => {
  return (
    <div>
      <svg
        width="40"
        height="40"
        viewBox="0 0 40 40"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="40" height="40" rx="20" fill="#DBE3F5" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M21 16H10V25H10.1707C10.5825 23.8348 11.6938 23 13 23C14.3062 23 15.4175 23.8348 15.8293 25H21V16ZM23 16V14H8V27H10.1707C10.5825 28.1652 11.6938 29 13 29C14.3062 29 15.4175 28.1652 15.8293 27H21H23H24.1707C24.5825 28.1652 25.6938 29 27 29C28.3062 29 29.4175 28.1652 29.8293 27H32V21.5L29 16H23ZM23 18V25H24.1707C24.5825 23.8348 25.6938 23 27 23C28.3062 23 29.4175 23.8348 29.8293 25H30V22.01L29.4491 21H26V19H28.3582L27.8127 18H23ZM14 26C14 26.5523 13.5523 27 13 27C12.4477 27 12 26.5523 12 26C12 25.4477 12.4477 25 13 25C13.5523 25 14 25.4477 14 26ZM28 26C28 26.5523 27.5523 27 27 27C26.4477 27 26 26.5523 26 26C26 25.4477 26.4477 25 27 25C27.5523 25 28 25.4477 28 26ZM14 19H12V21H14V19Z"
          fill="#182953"
        />
      </svg>
    </div>
  );
};

export const BoxIcons = () => {
  return (
    <div>
      <svg
        width="40"
        height="40"
        viewBox="0 0 40 40"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="40" height="40" rx="20" fill="#DBE3F5" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M20 10.9463L30 14.2796V14.576V25.0004V26.4525L20 30.0636L10 26.4525V24.5004V14.576V14.2796L20 10.9463ZM20 13.0545L27.3911 15.5182L25.6198 16.1578L18.337 13.6088L20 13.0545ZM15.236 14.6425L12.6089 15.5182L20 18.1872L22.64 17.2338L15.236 14.6425ZM24 18.8691L21 19.9525V27.576L28 25.0483V25.0004V17.4247L26 18.1469V20.0004L24 21.0004V18.8691ZM12 24.5004V17.4247L19 19.9525V27.576L12 25.0483V24.5004Z"
          fill="#182953"
        />
      </svg>
    </div>
  );
};

export const HandOpenIcons = () => {
  return (
    <div>
      <svg
        width="40"
        height="40"
        viewBox="0 0 40 40"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="40" height="40" rx="20" fill="#DBE3F5" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M20 13H16V20V20.8783L18.8494 20H25V23.7208L21.1623 25H27V13H22V16H20V13ZM10 20H14V11H29V25H30V30H16H14H10V20ZM12 22H14V28H12V22ZM16 22.9712V28H28V27H18.5V23.7792L23 22.2792V22H19.1506L16 22.9712Z"
          fill="#182953"
        />
      </svg>
    </div>
  );
};

export const CheckedBoxIcons = () => {
  return (
    <div>
      <svg
        width="40"
        height="40"
        viewBox="0 0 40 40"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="40" height="40" rx="20" fill="#DBE3F5" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M28 12H12V15H28V12ZM10 17H11V30H29V17H30V10H10V17ZM13 28V17H27V28H13ZM18.9258 25.34L25.4056 20.1562L24.1562 18.5945L19.0742 22.66L17 20.5858L15.5858 22L18.9258 25.34Z"
          fill="#182953"
        />
      </svg>
    </div>
  );
};
