import React from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import {
  DatePicker,
  ReactSelect,
} from '../../../../components/elements/InputField';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const optionsStatus = [
  { value: 'all', label: 'Semua Status' },
  { value: '0', label: 'Belum Dibayar' },
  { value: '1', label: 'Telah Dibayar' },
];

const HeaderTable = ({ tab }) => {
  return (
    <div className="d-flex flex-row  align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div className="mt-1 mb-1" style={{ width: '200px' }}>
        <DatePicker
          format="MMMM YYYY"
          views={['year', 'month']}
          noMarginBottom
          placeholder="Pilih bulan"
          // onDateChange={handleFilterDate}
        />
      </div>
      {tab !== 0 && (
        <div style={{ width: '200px', marginLeft: '15px' }}>
          <ReactSelect
            placeholder="Semua Status"
            options={optionsStatus}
            // onChange={(e) => handleFilter('status', e.target.value)}
          />
        </div>
      )}
    </div>
  );
};

const EmptyWallet = ({ tab }) => {
  const classes = useStyles();
  const processOptions = ['dompet', 'kekurangan pembayaran', 'reimburse'];
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses {processOptions[tab]} yang dilakukan saat ini.
      </h6>
    </div>
  );
};

function TableWallet(props) {
  const { tab } = props;
  const columOptions = [
    [
      {
        heading: 'Tanggal ',
        key: 'date',
      },
      {
        heading: 'Deskripsi',
        key: 'noinvoice',
      },
      {
        heading: 'Nominal',
        key: 'receiver',
      },
      {
        heading: 'Dompet',
        key: 'wallet',
      },
    ],
    [
      {
        heading: 'Tanggal ',
        key: 'date',
      },
      {
        heading: 'Nomor Transaksi',
        key: 'noinvoice',
      },
      {
        heading: 'Tipe Transaksi',
        key: 'receiver',
      },
      {
        heading: 'Status',
        key: 'wallet',
      },
      {
        heading: 'Nominal',
        key: 'wallet',
      },
      {
        heading: 'Aksi',
        key: 'wallet',
      },
    ],
    [
      {
        heading: 'Tanggal ',
        key: 'date',
      },
      {
        heading: 'Nomor Transaksi',
        key: 'noinvoice',
      },
      {
        heading: 'Tipe Transaksi',
        key: 'receiver',
      },
      {
        heading: 'Status',
        key: 'wallet',
      },
      {
        heading: 'Nominal',
        key: 'wallet',
      },
      {
        heading: 'Aksi',
        key: 'wallet',
      },
    ],
  ];
  return (
    <div>
      <Table
        // action={handleFetchDispatchOrder}
        // params={{ status: tab, storage_id: storageId, date: state.date }}\
        column={columOptions[tab]}
        data={[]}
        // transformData={(item) => ({
        //   ...item,
        //   date: moment(item.created_at).format('DD MMMM YYYY'),
        //   noinvoice: item.invoice_number,
        //   receiver: item.receiver_name,
        //   courier: item.vendor_service.vendor.name,
        // })}
        withNumber={false}
        renderHeader={() => <HeaderTable tab={tab} />}
        renderEmptyData={() => <EmptyWallet tab={tab} />}
        // totalPage={dispatchOrder?.last_page || 1}
      />
    </div>
  );
}

export default TableWallet;
