import actionType from './reduxConstant';

const initialState = {
  dashboard: {},
};

const dashboardReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.DASHBOARD_CUSTOMER:
      return {
        ...prevState,
        dashboard: action.payload,
      };
    default:
      return prevState;
  }
};

export default dashboardReducer;
