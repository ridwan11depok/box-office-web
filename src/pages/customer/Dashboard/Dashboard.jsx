import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import * as DashboardIcon from './components/Icons';
import { formatRupiah } from '../../../utils/text';
import TableBestSeller from './components/TableBestSeller';
import CardLogistic from './components/CardLogistic';
import { useHistory } from 'react-router-dom';
import Chart from './components/Chart';
import {
  DatePicker,
  ReactSelect,
} from '../../../components/elements/InputField';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const Dashboard = (props) => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
          <PageTitle title="Dashboard" />
          <div className="d-flex flex-row align-items-center">
            <div style={{ width: '200px', marginRight: '15px' }}>
              <ReactSelect
                placeholder="Pilih periode"
                // options={optionsStatus}
                // onChange={(e) => handleFilter('status', e.target.value)}
              />
            </div>
            <div className="mt-1 mb-1" style={{ width: '200px' }}>
              <DatePicker
                format="MMMM YYYY"
                views={['year', 'month']}
                noMarginBottom
                placeholder="Pilih bulan"
                // onDateChange={handleFilterDate}
              />
            </div>
          </div>
        </div>
      )}
    >
      <ContentItem
        className="p-0"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Dompet</h6>
        </div>
        <div className="d-flex align-items-center justify-between p-3">
          <DashboardIcon.WalletIcons />
          <div className="ml-2">
            <h6 className={classes.label}>Saldo Dompet</h6>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </div>
          <Button
            styleType="blueFill"
            className="ml-auto"
            text="Dompet"
            onClick={() => history.push('/dashboard/wallet')}
          />
        </div>
      </ContentItem>
      <ContentItem
        className="p-0"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Stok Produk</h6>
        </div>
        <div className="d-flex flex-lg-row flex-column">
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.WalletIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Jumlah Stok Gudang</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.WalletIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Jumlah Produk Terjual</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
        </div>
      </ContentItem>

      <ContentItem
        className="p-0"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Pemasukan dan Pengeluaran</h6>
        </div>
        <div
          className="d-flex flex-lg-row flex-column"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.TruckIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Pemasukan</h6>
              <h6 className={classes.value}>{formatRupiah(1000)}</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.BoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Biaya Gudang</h6>
              <h6 className={classes.value}>{formatRupiah(1000)}</h6>
            </div>
          </div>
        </div>
        <div className="d-flex flex-lg-row flex-column">
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.HandOpenIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Biaya Kurir</h6>
              <h6 className={classes.value}>{formatRupiah(1000)}</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.CheckedBoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Biaya Packaging</h6>
              <h6 className={classes.value}>{formatRupiah(1000)}</h6>
            </div>
          </div>
        </div>
      </ContentItem>

      <ContentItem
        className="p-0"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Produktifitas Store</h6>
        </div>
        <div
          className="d-flex flex-lg-row flex-column"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.BoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Semua Transaksi</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.TruckIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Transaksi Proses</h6>
              <h6 className={classes.value}>10000</h6>
            </div>
          </div>
        </div>
        <div className="d-flex flex-lg-row flex-column">
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.HandOpenIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Transaksi Selesai</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.CheckedBoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Transaksi Batal</h6>
              <h6 className={classes.value}>10000</h6>
            </div>
          </div>
        </div>
      </ContentItem>
      <ContentItem className="p-0" col="col-12" spaceBottom={3}>
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Growth Transaction</h6>
        </div>
        <Chart />
      </ContentItem>
      <ContentItem col="col-12" spaceBottom={3} border={false}>
        <TableBestSeller />
      </ContentItem>
      <ContentItem className="p-0" col="col-12" spaceBottom={3}>
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Logistik</h6>
        </div>
        <div className="d-flex flex-column flex-lg-row">
          <div
            className="w-lg-50 w-100 p-3"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <h6 className={classes.label}>Kemarin</h6>
            <div>
              {Array(10)
                .fill(null)
                .map((item) => (
                  <CardLogistic />
                ))}
            </div>
          </div>
          <div className="w-lg-50 w-100 p-3">
            <h6 className={classes.label}>Hari Ini</h6>
            <div className="w-100">
              {Array(10)
                .fill(null)
                .map((item) => (
                  <CardLogistic />
                ))}
            </div>
          </div>
        </div>
      </ContentItem>
    </ContentContainer>
  );
};

export default Dashboard;
