import React, { useState, useEffect, useCallback } from "react";
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import { getAllReportTransaction } from "../reduxAction";

const useStyles = makeStyles({
  value: {
    color: '#1C1C1C',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    textAlign: 'right',
    margin: '10px 0',
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    textAlign: 'left',
    margin: '10px 0',
  },
  total: {
    color: '#1C1C1C',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    margin: '15px 0',
  },
});

const ModalConfirmPayment = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  const classes = useStyles();
  const [state, setState] = useState({
    showDetail: false,
    vendorId: null,
    datestart: "",
    dateend: "",
    store_id: "all",
    storage_id: "all",
  });

  const dispatch = useDispatch();
  
//   useEffect(() => {
//     dispatch(getAllReportTransaction());
//   }, []);

  useEffect(() => { 
    const params = {
    "date[start]": state.datestart ? state.datestart : null,
    "date[end]": state.dateend ? state.dateend : null,
    type: "dispatch_order",
    storage_id: state.storage_id !== 'all' ? state.storage_id : null,
    };
    console.log(params)
    // dispatch(getAllReportTransaction(params));
  }, []);


const { inbounds } = useSelector((state) => state.reportUser);

const sum = useCallback(
    (key) => {
        return inbounds?.data?.map(item=>item[key] || 0).reduce((prev, current)=>prev+current).toLocaleString('id')
    },
    [inbounds],
    )

// const handleTotalInbound = () => {
//     const params = {
//     "date[start]": state.datestart ? state.datestart : null,
//     "date[end]": state.dateend ? state.dateend : null,
//     type: "dispatch_order",
//     storage_id: state.storage_id !== 'all' ? state.storage_id : null,
//     };
//     return dispatch(getAllReportTransaction(params));
// };


  return (
    <>
      <Modal
        titleModal="Pembayaran Invoice"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <Row>
            <Col xs={12}>
                <h6 className={classes.label}>
                    Apakah anda yakin untuk melakukan pembayaran invoice dengan rincian biaya sebagai berikut:
                </h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Total Pembayaran Inbound</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Rp. {sum('total_price')}</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Total Pembayaran Dispatch Order</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Rp.1000</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Total Pembayaran Transaksi Reguler</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Rp.1000</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Total Pembayaran Cashless</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Rp.1000</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.total}>Total Pembayaran Invoice</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.total} style={{ textAlign: 'right' }}>
                Rp.1000
              </h6>
            </Col>
          </Row>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Lanjutkan Pembayaran'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalConfirmPayment;
