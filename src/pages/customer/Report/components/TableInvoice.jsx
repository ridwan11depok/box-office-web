import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import RangeDate from './RangeDate';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { getAllReportInvoice, getListStorage, setDetailInvoice, exportLogsInvoice } from '../reduxAction';
import {
    ReactSelect,
} from '../../../../components/elements/InputField';
import moment from 'moment';
import Badge from '../../../../components/elements/Badge';
import localization from 'moment/locale/id';
import { Row, Col } from 'react-bootstrap';
import { useDownloadFile } from "../../../../utils/hook";


const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    textDetail: {
        color: '#182953',
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        cursor:'pointer'
    },
});

const HeaderTable = ({
    handleDate,
    handleFilter,
    data,
    handleExportLogs
}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { listStorage } = useSelector((state) => state.reportUser);

    const getFileName = () => {
      return `Invoice.xlsx`;
    };
  
    const { ref, url, download, name } = useDownloadFile({
      apiDefinition: handleExportLogs,
      getFileName,
    });

    return (
        <div className="d-flex flex-column w-100">
            <Row className="p-3">
                <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
                    <div className="mb-1">Tanggal</div>
                    <RangeDate
                        change= {handleDate}
                    />
                </Col>
                <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
                    <ReactSelect
                        label="Gudang"
                        placeholder="Semua Gudang"
                        options={listStorage}
                        onChange={(e) => handleFilter('storage_id', e.target.value)}
                    />
                </Col>
                <Col xs={12} md={6} lg={4} className="d-flex align-items-end justify-content-end mb-2">
                    <a href={url} download={name} className="hidden" ref={ref} />
                    <Button
                        text="Export Logs"
                        styleType="blueOutline"
                        onClick={download}
                    />
                </Col>
            </Row>
        </div>
    ); 
};

const EmptyData = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada proses Invoice saat ini.
            </h6>
        </div>
    );
};

const ActionField = ({ data, onClick }) => {
    let status = data.status;
    const classes = useStyles()
    const history = useHistory();
    const dispatch = useDispatch();
    const invoice = () => {
        dispatch(setDetailInvoice(data));
        history.push(`/report/invoice/detail/${data?.id}`)
    }
    return (
      <>
        {status === 0 ? 
            <Button
                text="Pembayaran"
                styleType="blueFill"
                onClick={invoice}
            />
            : 
            <span className={classes.textDetail}>Detail</span>
        }
        {/* <span className={classes.textDetail}>Detail</span> */}
        {/* <span onClick={invoice} className={classes.textDetail}>Detail</span> */}
      </>
    );
  };

  const TotalInvoice = ({ data }) => {
    let price = data?.total;
    return <span>Rp {price?.toLocaleString("id") || "-"}</span>;
  };

const StatusColumn = ({ status = 1 }) => {
    return (
      <Badge
        label={status === 0 ? 'Belum Dibayar' : 'Telah Dibayar'}
        styleType={status === 0 ? 'red' : 'green'}
        style={{ fontSize: '14px' }}
      />
    );
  };

function TableInvoice(props) {
    const { tab, storeId } = props;
    const router = useHistory()
    const [state, setState] = useState({
        showDetail: false,
        selectedRow: {},
        search: '',
        // storeId: null,
        vendorId: null,
        datestart: '',
        dateend: '',
        showPicker: '',
        store_id: "all",
        storage_id: "all",
    });
    const dispatch = useDispatch();

    const fetchAllReport = (params) => {
        dispatch(getAllReportInvoice(params));
    };

    useEffect(() => {
      dispatch(getListStorage());
    }, []);

    const { invoice } = useSelector(
        (state) => state.reportUser
    );

    const handleDate = (e) => {
        setState({ ...state, datestart: moment(e.value[0]).format('YYYY-MM-DD'), dateend: moment(e.value[1]).format('YYYY-MM-DD') });
    };

    const handleExportLogs = () => {
      const params = {
        "date[start]": state.datestart ? state.datestart : null,
        "date[end]": state.dateend ? state.dateend : null,
        storage_id: state.storage_id !== 'all' ? state.storage_id : null,
      };
      return dispatch(exportLogsInvoice(params));
    };

    const handleFilter = (type, value) => {
      if (type === 'storage_id') {
        setState({ ...state, storage_id: value });
      }
    };

    return (
        <div>
            <Table
                action={fetchAllReport}
                totalPage={invoice?.last_page || 1}
                params={{
                    "date[start]": state.datestart ? state.datestart : null,
                    "date[end]": state.dateend ? state.dateend : null,
                    store_id: storeId,
                    storage_id: state.storage_id !== 'all' ? state.storage_id : null,
                }}
                column={[
                    {
                    heading: 'Tanggal',
                    key: 'date',
                    },
                    {
                    heading: 'Nomor Invoice',
                    key: 'invoice_number',
                    },
                    {
                    heading: 'Gudang',
                    key: 'warehouse',
                    },
                    {
                    heading: 'Total Invoice',
                    render: (item) => <TotalInvoice data={item} />,
                    },
                    {
                    heading: 'Jatuh Tempo',
                    key: 'due_date',
                    },
                    {
                    heading: 'Status',
                    render: (data) => <StatusColumn status={data?.status || 0} />,
                    },
                    {
                    heading: 'Aksi',
                    render: (data) => <ActionField data={data || data?.status || 0 } />,
                    },
                ]}
                data={invoice?.data || []}
                transformData={(item) => ({
                  ...item,
                  date: moment(item?.created_at)
                    .locale('id', localization)
                    .format('DD MMMM yyyy HH:mm:ss'),
                    store: item?.store.store_name || '-',
                    warehouse: item?.warehouse.name || '-',
                  due_date: moment(item?.due_date)
                    .locale('id', localization)
                    .format('DD MMMM yyyy'),
                })}
                renderHeader={() => (
                    <HeaderTable
                        handleDate={handleDate}
                        handleFilter={handleFilter}
                        handleExportLogs={handleExportLogs}
                    />
                )}
                withNumber={false}
                renderEmptyData={() => <EmptyData />}
            />
        </div>
    );
}

export default TableInvoice;
