import React, { useState, useEffect, useCallback } from "react";
import { Table } from "../../../../components/elements/Table";
import RangeDate from "./RangeDate";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Button from "../../../../components/elements/Button";
import { getAllReportTransaction, getListStorage, exportLogsTransaction } from "../reduxAction";
import { ReactSelect } from "../../../../components/elements/InputField";
import moment from "moment";
import localization from "moment/locale/id";
import { Row, Col } from "react-bootstrap";
import { useDownloadFile } from "../../../../utils/hook";

const useStyles = makeStyles({
  description: {
    fontSize: "14px",
    fontWeight: "400",
    fontFamily: "Open Sans",
  },
  titleTable: {
    fontSize: "18px",
    fontWeight: "700",
    fontFamily: "Work Sans",
    color: "#1c1c1c",
    marginBottom: 0,
    marginRight: "30px",
  },
  textDetail: {
    color: "#182953",
    fontSize: "14px",
    fontWeight: "700",
    fontFamily: "Open Sans",
    cursor: "pointer",
  },
});

const HeaderTable = ({ handleDate, handleExportLogs, handleFilter }) => {
  const { listStorage } = useSelector((state) => state.reportUser);

  const getFileName = () => {
    return `Inbound.xlsx`;
  };

  const { ref, url, download, name } = useDownloadFile({
    apiDefinition: handleExportLogs,
    getFileName,
  });

  return (
    <div className="d-flex flex-column w-100">
      <Row className="p-3">
        <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
          <div className="mb-1">Tanggal</div>
          <RangeDate change={handleDate} />
        </Col>
        <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
          <ReactSelect
            label="Gudang"
            placeholder="Semua Gudang"
            options={listStorage}
            onChange={(e) => handleFilter('storage_id', e.target.value)}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="d-flex align-items-end justify-content-end mb-2">
          <a href={url} download={name} className="hidden" ref={ref} />
        <Button
          styleType="blueOutline"
          text="Print Invoice"
          onClick={download}
        />
        </Col>
      </Row>
    </div>
  );
};

const FooterTable = ({ inbounds }) => {
    const classes = useStyles();
    const sum = useCallback(
      (key) => {
        return inbounds?.data?.map(item=>item[key] || 0).reduce((prev, current)=>prev+current).toLocaleString('id')
      },
      [inbounds],
    )
  
    return (
      <div className="d-flex flex-column w-100">
        <Row className="pl-3 pr-3 pb-1 pt-1">
          <Col xs={12} md={8} lg={8} className="mb-lg-2 mb-lg-0 mb-2">
              <div className="mt-3 d-flex align-items-end justify-content-end">
                  <h6 className="d-flex">Total Pembayaran <span className="ml-3">{sum('total_sku')}</span></h6>
              </div>
          </Col>
          <Col
            xs={12}
            md={4}
            lg={4}
            className="d-flex align-items-end justify-content-end mb-2"
          >
          <h6>Rp. {sum('total_price')}</h6>
          </Col>
        </Row>
      </div>
    );
};

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: "30vh" }}
    >
      <h6 className={classes.description}>
        Tidak ada proses Inbound saat ini.
      </h6>
    </div>
  );
};

const TotalTransaction = ({ data }) => {
  let price = data?.total_price;
  return <span>Rp {price?.toLocaleString("id") || "-"}</span>;
};

function TableInbound(props) {
  const { tab, storeId } = props;
  const [state, setState] = useState({
    showDetail: false,
    vendorId: null,
    datestart: "",
    dateend: "",
    store_id: "all",
    storage_id: "all",
  });

  const dispatch = useDispatch();

  const fetchAllReport = (params) => {
    dispatch(getAllReportTransaction(params));
  };
  useEffect(() => {
    dispatch(getListStorage());
  }, []);

  const { inbounds } = useSelector((state) => state.reportUser);

  const handleDate = (e) => {
    setState({
      ...state,
      datestart: moment(e.value[0]).format("YYYY-MM-DD"),
      dateend: moment(e.value[1]).format("YYYY-MM-DD"),
    });
  };

  const handleExportLogs = () => {
    const params = {
      "date[start]": state.datestart ? state.datestart : null,
      "date[end]": state.dateend ? state.dateend : null,
      type: "inbound",
      storage_id: state.storage_id !== 'all' ? state.storage_id : null,
    };
    return dispatch(exportLogsTransaction(params));
  };

  const handleFilter = (type, value) => {
    if (type === 'storage_id') {
      setState({ ...state, storage_id: value });
    }
  };

  return (
    <div>
      <Table
        action={fetchAllReport}
        totalPage={inbounds?.last_page || 1}
        params={{
          "date[start]": state.datestart ? state.datestart : null,
          "date[end]": state.dateend ? state.dateend : null,
          type: "inbound",
          store_id: storeId,
          storage_id: state.storage_id !== 'all' ? state.storage_id : null,
        }}
        column={[
          {
            heading: "Tanggal",
            key: "date",
          },
          {
            heading: "Kode Inbound",
            key: "order_number",
          },
          {
            heading: "Gudang",
            key: "storage",
          },
          {
            heading: "Total SKU",
            key: "total_sku",
          },
          {
            heading: "Total Transaksi",
            render: (item) => <TotalTransaction data={item} />,
          },
        ]}
        data={inbounds?.data || []}
        transformData={(item) => ({
          ...item,
          date: moment(item?.created_at)
            .locale("id", localization)
            .format("DD MMMM yyyy HH:mm:ss"),
          storage: item?.storage.warehouse.name || "-",
        })}
        showFooter={false}
        renderFooter={() => <FooterTable inbounds={inbounds}/>}
        renderHeader={() => 
          <HeaderTable 
            handleDate={handleDate}
            handleExportLogs={handleExportLogs}
            handleFilter={handleFilter}
          />}
        withNumber={false}
        renderEmptyData={() => <EmptyData />}
      />
    </div>
  );
}

export default TableInbound;