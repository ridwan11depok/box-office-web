import React, { useState, useEffect, useLayoutEffect } from 'react';
import { ContentContainer, ContentItem } from "../../../../components/elements/BaseContent";
import BackButton from "../../../../components/elements/BackButton";
import Button from '../../../../components/elements/Button';
import { TabHeader, TabContent } from '../../../../components/elements/Tabs';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { getAllReportInvoice } from '../reduxAction';
import { useDispatch, useSelector } from 'react-redux';
import Badge from '../../../../components/elements/Badge';
import TableInbound from './TableInboundTab';
import TableDispatchOrder from './TableDispatchOrderTab';
import TableTransactionReguler from './TableTransactionRegulerTab';
import TableCashless from './TableCashlessTab';
import ModalConfirmPayment from './ModalConfirmPayment';

const useStyles = makeStyles({
    value: {
      color: '#1c1c1c',
      fontSize: '14px',
      fontWeight: '700',
      fontFamily: 'Work Sans',
      marginBottom: 0,
    },
    label: {
      color: '#1c1c1c',
      fontSize: '14px',
      fontWeight: '400',
      fontFamily: 'Open Sans',
    },
    status: {
      background: "#EDF7EE",
      padding: "0.5rem!important",
      color: "#1B5E20",
      width: "fit-content",
      borderRadius: "40%",
      marginTop: "5px",
    },
  });


const DetailInvoice = () => {
    const history = useHistory();
    const classes = useStyles();
    const location = useLocation();
    const [state, setState] = useState({
        tab: 0,
        splitURL: [],
        id: 0,
        showModalConfirmPayment: false,
    });
    const changeTab = (tab) => {
        setState({ ...state, tab });
    };
  
  const listHeader = [
    {
        tab: 0,
        title: 'Inbound',
        url: 'inbound',
        id: 0
    },
    {
        tab: 1,
        title: 'Outbound - Dispatch Order',
        url: 'dispatch_order',
        id: 1
    },
    {
        tab: 2,
        title: 'Outbound - Cashless',
        url: 'dispatch_order',
        id: 2
    },
    {
        tab: 3,
        title: 'Outbound - Transaksi Reguler',
        url: 'transaction_reguler',
        id: 3
    },
  ]
  
    useEffect(() => {
        const split = location.pathname.split('/')
        setState({ ...state, splitURL: split })
    }, [location?.pathname])
    
    const { detailInvoice } = useSelector((state) => state.reportUser);
    // console.log(detailInvoice)

    const StatusColumn = ({ status = 1 }) => {
        return (
            <Badge
                label={status === 0 ? 'Belum Dibayar' : 'Telah Dibayar'}
                styleType={status === 0 ? 'red' : 'green'}
                style={{ fontSize: '14px' }}
            />
        );
    };

    return(
      <>
          <ContentContainer
            renderHeader={() => (
              <div className="d-flex flex-row align-items-center">
                <BackButton
                  label={detailInvoice?.invoice_number || 'Invoice Detail'}
                  onClick={() => history.push('/report/invoice')}
                />
                <div className="ml-2">
                  <StatusColumn status={detailInvoice?.status || 0} />
                </div>
                <Button
                  styleType="blueFill"
                  className="ml-auto"
                  text="Pembayaran"
                  onClick={() =>
                    setState({ ...state, showModalConfirmPayment: true })
                  }
                />
              </div>
            )}
          >
            <ContentItem
              spaceRight="0 pr-md-2"
              col="col-12 col-md-12"
              spaceBottom={3}
            >
              <div className="row p-3">
                  <div className="col-12 col-md-3">
                      <h6 className={classes.label}>Tanggal dibuat</h6>
                      <h6 className={classes.value}>{detailInvoice.date}</h6>
                  </div>
                  <div className="col-12 col-md-3">
                      <h6 className={classes.label}>Gudang</h6>
                      <h6 className={classes.value}>{detailInvoice.warehouse}</h6>
                  </div>
                  <div className="col-12 col-md-3">
                      <h6 className={classes.label}>Total Pembayaran</h6>
                      <h6 className={classes.value}>Rp. {detailInvoice.total?.toLocaleString("id") || "-"}</h6>
                  </div>
                  <div className="col-12 col-md-3">
                      <h6 className={classes.label}>Tanggal Jatuh Tempo</h6>
                      <h6 className={classes.value}>{detailInvoice.due_date}</h6>
                  </div>
              </div>
            </ContentItem>
            <ContentItem className="mb-3">
                <TabHeader
                    border={false}
                    activeTab={state.tab}
                    listHeader={listHeader}
                    onChange={changeTab}
                    inActiveColor="#192a55"
                />
            </ContentItem>
            <ContentItem border={false}>           
              <TabContent tab={0} activeTab={state.tab}>
              {state.splitURL.length === 2 ?
                  <div>Kosong</div>
                  :
                  <TableInbound tab={listHeader[state.tab]} />
              }
              </TabContent> 
              <TabContent tab={1} activeTab={state.tab}>
              {state.splitURL.length === 2 ?
                  <div>Kosong</div>
                  :
                  <TableDispatchOrder tab={listHeader[state.tab]} />
              }
              </TabContent>
              <TabContent tab={2} activeTab={state.tab}>
              {state.splitURL.length === 2 ?
                  <div>Kosong</div>
                  :
                  <TableCashless tab={listHeader[state.tab]} />
              }
              </TabContent>
              <TabContent tab={3} activeTab={state.tab}>
              {state.splitURL.length === 2 ?
                  <div>Kosong</div>
                  :
                  <TableTransactionReguler tab={listHeader[state.tab]} />
              }
              </TabContent>
            </ContentItem>
          </ContentContainer>
          <ModalConfirmPayment
            show={state.showModalConfirmPayment}
            onHide={() => setState({ ...state, showModalConfirmPayment: false })}
          />
      </>
    )
}

export default DetailInvoice