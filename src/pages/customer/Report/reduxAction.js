import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import axios from 'axios';
import baseUrl from '../../../api/url';

export const getAllReportTransaction =
  (params = {}, transactionId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (transactionId) {
        result = await consume.getWithParams(
          'listReportTransactionUser',
          {},
          { token },
          { store_id: storeId },
          `detail/${transactionId}`
        );
        dispatch(setDetailInbound(result.result));
      } else {
        result = await consume.getWithParams(
          'listReportTransactionUser',
          {},
          { token },
          { ...params, store_id: storeId }
        );
        dispatch(setInbound(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      if (transactionId) {
        dispatch(setDetailInbound({}));
      } else {
        dispatch(setInbound({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

  function setInbound(data) {
    return {
      type: actionType.REPORT_INBOUND,
      payload: data,
    };
  }
  function setDetailInbound(data) {
    return {
      type: actionType.DETAIL_INBOUND_USER,
      payload: data,
    };
  }

  export const getListStorage = () => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoading(true));
    try {
      const result = await consume.getWithParams(
        'listStorageCustomer',
        {},
        { token },
        { store_id: storeId }
      );
      dispatch(setListStorage(result.result));
      dispatch(setLoading(false));
      return Promise.resolve(result.result);
    } catch (err) {
      dispatch(setListStorage([]));
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };
  
  function setListStorage(data) {
    return {
      type: actionType.LIST_STORAGE_CUSTOMER,
      payload: data,
    };
  }

  export const exportLogsTransaction =
(payload) => async (dispatch, getState) => {
  try {   
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;
    // console.log('data==>', storeId)    
    const config = {
      url: baseUrl.exportLogs,
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        store_id: storeId,
        ...payload,
      },
      responseType: 'blob',
    };
    const res = await axios.request(config);

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllReportInvoice =
  (params = {}, transactionId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (transactionId) {
        result = await consume.getWithParams(
          'listReportInvoiceUser',
          {},
          { token },
          { store_id: storeId },
          `detail/${transactionId}`
        );
        dispatch(setDetailInvoice(result.result));
      } else {
        result = await consume.getWithParams(
          'listReportInvoiceUser',
          {},
          { token },
          { ...params, store_id: storeId }
        );
        dispatch(setInvoice(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      if (transactionId) {
        dispatch(setDetailInvoice({}));
      } else {
        dispatch(setInvoice({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

  function setInvoice(data) {
    return {
      type: actionType.REPORT_INVOICE,
      payload: data,
    };
  }

  export const setDetailInvoice = (data) => {
    return {
      type: actionType.DETAIL_REPORT_INVOICE,
      payload: data,
    };
  };  


//   export const exportLogsInvoice =
// (payload) => async (dispatch, getState) => {
//   try {   
//     const { token, dataUser } = getState().login;
//     // console.log('data==>', storeId)    
//     const storeId = dataUser?.store[0]?.id;
//     const config = {
//       url: baseUrl.exportLogsReportInvoice,
//       method: 'POST',
//       headers: {
//         Authorization: `Bearer ${token}`,
//       },
//       data: {
//         store_id: storeId,
//         ...payload,
//       },
//       responseType: 'blob',
//     };
//     const res = await axios.request(config);

//     return Promise.resolve(res);
//   } catch (err) {
//     return Promise.reject(err);
//   }
// };

export const exportLogsInvoice =
(payload) => async (dispatch, getState) => {
  try {   
    const { token, dataUser } = getState().login;
    const storeId = dataUser?.store[0]?.id;   
    const config = {
      url: baseUrl.exportLogsReportInvoice,
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        store_id: storeId,
        ...payload,
      },
      responseType: 'blob',
    };
    const res = await axios.request(config);

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};