import actionType from './reduxConstant';

const initialState = {
  inbounds: {},
  invoice: {},
  detailInvoice: {},
  detailInbound: {},
  dispatchOrder: {},
  detailDispatchOrder: {},
  listStore: [],
  listStorage: [],
  transactionReguler: {},
  detailTransactionReguler: {},
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.REPORT_INBOUND:
      return {
        ...prevState,
        inbounds: action.payload,
      };
    case actionType.REPORT_INVOICE:
      return {
        ...prevState,
        invoice: action.payload,
      };
    case actionType.DETAIL_INBOUND_USER:
      return {
        ...prevState,
        detailInbound: action.payload,
      };
    case actionType.DETAIL_REPORT_INVOICE:
      return {
        ...prevState,
        detailInvoice: action.payload,
      };
    case actionType.LIST_STORAGE_CUSTOMER: {
      const listStorage = action.payload.map((item) => ({
        value: item.id,
        label: item?.warehouse?.name,
      }));
      return {
        ...prevState,
        listStorage: [{ value: 'all', label: 'Semua Gudang' }, ...listStorage],
      };
    }
    case actionType.TRANSACTION_REGULER:
      return {
        ...prevState,
        transactionReguler: action.payload,
      };
      case actionType.DETAIL_TRANSACTION_REGULER:
      return {
        ...prevState,
        detailTransactionReguler: action.payload,
      };
    default:
      return prevState;
  }
};

export default inboundReducer;
