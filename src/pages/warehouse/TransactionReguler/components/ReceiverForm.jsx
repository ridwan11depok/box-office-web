import React, {useState} from 'react'
import clsx from 'clsx'
import { MapOutlined } from '@material-ui/icons';
import Button from '../../../../components/elements/Button';
import MapModal from '../../../../components/elements/Map/MapModal';

export default function ReceiverForm(props) {
    const { classes, data } = props
    const [state, setState] = useState({
        showMap: false
    })
    return (
        <>
            <div className="w-50 p-3 d-flex flex-column border rounded">
                <span className={classes.textTitle}>Data Penerima</span>
                <div className={clsx("d-flex flex-column", classes.contentBox)}>
                    <div className={classes.contentItem}>
                        <span className={classes.titleItem}>Nama Pelanggan</span>
                        <span>{data?.receiver_name || 'Tidak ada data'}</span>
                    </div>
                    <div className={classes.contentItem}>
                        <span className={classes.titleItem}>Nomor Telepon</span>
                        <span className={classes.textBold}>{data?.receiver_phone_number || 'Tidak ada data'}</span>
                    </div>
                    <div className={classes.contentItem}>
                        <span className={classes.titleItem}>Alamat</span>
                        <span>{data?.receiver_address || 'Tidak ada data'}</span>
                    </div>
                    <div>
                        <Button
                            styleType="blueNoFill"
                            className="p-0"
                            text="Lihat Lokasi di Peta"
                            startIcon={() => <MapOutlined />}
                            onClick={() => setState({ ...state, showMap: true })}
                        />
                    </div>
                    <div className="w-50 d-flex justify-content-between">
                        <div className={classes.contentItem}>
                            <span>Kota</span>
                            <span className={classes.textBold}>{data?.receiver_city?.name || 'Tidak ada data'}</span>
                        </div>
                        <div className={classes.contentItem}>
                            <span>Provinsi</span>
                            <span className={classes.textBold}>{data?.receiver_province?.name || 'Tidak ada data'}</span>
                        </div>
                    </div>
                    <div className={classes.contentItem}>
                        <span className={classes.titleItem}>Pesan</span>
                        <span>{data?.receiver_note || 'Tidak ada data'}</span>
                    </div>
                </div>
            </div>
            <MapModal
                show={state.showMap}
                onHide={() => setState({ ...state, showMap: false })}
                initialCoordinate={{
                    lat: data?.receiver_latitude,
                    lng: data?.receiver_longitude,
                }}
                mode="viewer"
            />
        </>
    )
}
