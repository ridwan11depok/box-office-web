import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
// import { confirmManifestTransactionReguler } from '../reduxAction';

const validationSchema = Yup.object({
    awb_number: Yup.number().typeError('Nomor AWB harus berupa angka').required('Harap mengisi nomor AWB'),
});

const ModalAWB = (props) => {
    const { id } = props
    const history = useHistory();
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            awb_number: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            handleSubmit(values);
        },
    });

    useEffect(() => {
        if (!props.show) {
            formik.resetForm();
        }
    }, [props.show]);

    const handleSubmit = async (values) => {
        try {
            // await dispatch(confirmManifestTransactionReguler(values, id))
            props.onHide();
            props.onSuccess()
        } catch (err) { }
    };

    const { isLoading } = useSelector((state) => state.loading);
    return (
        <>
            <Modal
                titleModal={'Selesai Shipping'}
                titleClassName="title-modal"
                bodyClassName="body-modal pb-3"
                show={props.show}
                onHide={props.onHide}
                content={(data) => (
                    <>
                        <Form.Group className="mb-1 mt-3" controlId="formBasicEmail">
                            <Form.Label as={'h6'} className="input-label">
                                AWB
                            </Form.Label>
                            <span>
                                AWB yang diinput akan terdaftar dalam proses manifest dan dikelompok berdasarkan logistik
                            </span>
                            <Form.Control
                            className="mt-2"
                                type=""
                                placeholder="Input nomor AWB"
                                {...formik.getFieldProps('awb_number')}
                                style={{
                                    border:
                                        formik.touched.awb_number &&
                                        formik.errors.awb_number &&
                                        '1px solid #ee1b25',
                                }}
                            />
                            {formik.touched.awb_number && formik.errors.awb_number && (
                                <Form.Text className="text-input-error">
                                    {formik.errors.awb_number}
                                </Form.Text>
                            )}
                        </Form.Group>
                    </>
                )}
                footer={(data) => (
                    <>
                        <button
                            className="mr-2"
                            style={{
                                backgroundColor: '#E8E8E8',
                                width: '120px',
                                height: '40px',
                                borderRadius: '4px',
                                border: 'none',
                                outline: 'none',
                                color: '#192A55',
                                fontWeight: '700',
                                fontFamily: 'Open Sans',
                            }}
                            onClick={props.onHide}
                        >
                            Batal
                        </button>
                        <button
                            onClick={(e) => {
                                formik.handleSubmit(e);
                            }}
                            style={{
                                backgroundColor: '#192A55',
                                width: '120px',
                                height: '40px',
                                borderRadius: '4px',
                                border: 'none',
                                outline: 'none',
                                color: '#FFFFFF',
                                fontWeight: '700',
                                fontFamily: 'Open Sans',
                            }}
                        >
                            {isLoading ? 'Loading...' : 'Selesai'}
                        </button>
                    </>
                )}
            />
        </>
    );
};

ModalAWB.defaultProps = {
    initialValues: null,
};
export default ModalAWB;
