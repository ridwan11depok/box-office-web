import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Button from '../../../../components/elements/Button';
import HeaderDetail from './HeaderDetail';
import DetailPacket from './DetailPacket';
import { useDispatch } from 'react-redux';
import {
  confirmAtWarehouseTransactionReguler,
  confirmShippingTransactionReguler,
  getTransactionReguler,
  confirmManifestTransactionReguler,
} from '../reduxAction';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import ModalAddPicker from './ModalAddPicker';
import ModalConfirm from '../components/ModalConfirm';
import ModalAWB from './ModalAWB';
import SenderAndReceiver from './SenderAndReceiver';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { Link } from 'react-scroll'
import TrackingOrder from './TrackingOrder';

const useStyles = makeStyles({
  contentBox: {
    // gap: '15px',
    marginTop: '20px',
    flex: 0.5,
  },
  contentItem: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '16px'
  },
  textTitle: {
    color: '#1C1C1C',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    fontWeight: 'bold',
  },
  titleItem: {
    color: '#4F4F4F',
    fontFamily: 'Open Sans',
    fontWeight: 400,
    fontStyle: 'normal',
    fontSize: '14px',
    lineHeight: '20px'
  },
  valueItem: {
    color: '#4F4F4F',
    fontFamily: 'Open Sans',
    fontWeight: 600,
    fontStyle: 'normal',
    fontSize: '14px',
    lineHeight: '20px'
  },
  textBold: {
    color: '#363636',
    fontWeight: 'bold',
  },
});

const Detail = (props) => {
  const { id, changeTab, tab } = props;
  console.log('tab', tab)
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useHistory();
  const [state, setState] = useState({
    modalAddPicker: false,
    modalConfirm: false,
    modalConfirmShipping: false,
    modalAWB: false,
    modalConfirmAtWarehouse: false,
    isCopiedAWB: false,
  });

  const { detailTransactionReguler } = useSelector(
    (state) => state.outboundWarehouse
  );

  useEffect(() => {
    if (id) {
      getDetailTransactionReguler();
    }
  }, [id]);
  useEffect(() => {
    if (state.isCopiedAWB) {
      setTimeout(() => {
        setState({ ...state, isCopiedAWB: false });
      }, 3000);
    }
  }, [state.isCopiedAWB]);

  const getDetailTransactionReguler = async () => {
    try {
      await dispatch(getTransactionReguler({}, id));
    } catch (err) {
      
    }
  };

  const successAddPicker = () => {
    changeTab(1);
  };

  const successConfirmShipping = async () => {
    try {
      await dispatch(confirmManifestTransactionReguler({}, id));
      router.push('/warehouse/manifest');
    } catch (err) {}
  };

  const handleConfirm = async () => {
    try {
      await dispatch(confirmAtWarehouseTransactionReguler({}, id));
      changeTab(2);
    } catch (err) {}
  };

  const handleConfirmAtWarehouse = async () => {
    try {
      await dispatch(confirmShippingTransactionReguler({}, id));
      changeTab(3);
    } catch (err) {}
  };

  const ButtonByStatus = ({ tab }) => {
    
    return (
      <div className="w-100 d-flex flex-row mt-3 justify-content-end">
        <Button
          text="Kembali"
          styleType="lightBlueFill"
          style={{ minWidth: '120px' }}
          onClick={() => router.goBack()}
        />
        {tab === 'waiting' && (
          <Button
            styleType="blueFill"
            text="Start Order"
            onClick={() => setState({ ...state, modalAddPicker: true })}
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'pickup' && (
          <Button
            styleType="blueFill"
            className="p-2"
            text="Already Picked"
            onClick={() => setState({ ...state, modalConfirm: true })}
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'at_warehouse' && (
          <Button
            styleType="blueFill"
            className="p-2"
            text="Confirm at Warehouse"
            onClick={() =>
              setState({ ...state, modalConfirmAtWarehouse: true })
            }
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'shipping' && (
          <Button
          styleType={detailTransactionReguler?.awb_number ? "blueFill" : "blueFillDisabled"}
            className="p-2"
            text="Ready to Shipped"
            onClick={() => setState({ ...state, modalConfirmShipping: true })}
            disabled={detailTransactionReguler?.awb_number ? false : true}
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
      </div>
    );
  };

  const Picker = ({ data }) => {
    return (
      <div className="w-100 d-flex p-3 flex-column border ">
        <span className={classes.textTitle}>Picker</span>
        <div className={clsx('d-flex flex-column', classes.contentBox)}>
          <div className="w-100 d-flex justify-content-between">
            <span>Picker</span>
            <span className={classes.textBold}>
              {data?.picker_name || 'Tidak ada data'}
            </span>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div>
      <HeaderDetail data={detailTransactionReguler} tab={tab} />

      {detailTransactionReguler?.awb_number && <div className="w-100 d-flex p-3 justify-content-between border ">
        <div className="">
            <span>
                Nomor Resi : {' '}
                <span className={classes.textBold}>
                {detailTransactionReguler?.awb_number || 'Tidak ada data'}
                </span>
            </span>
            <CopyToClipboard text={`${detailTransactionReguler?.awb_number}`}>
                <Button
                text={state.isCopiedAWB ? 'Resi Disalin' : 'Salin No.Resi'}
                styleType="blueOutline"
                style={{ marginLeft: '10px' }}
                onClick={() => setState({ ...state, isCopiedAWB: true })}
                />
            </CopyToClipboard>
        </div>

        <Link to="tracking" spy={true} smooth={true}>
            <Button
                text="Riwayat Pengiriman"
                styleType="blueNoFill"
                endIcon={() => <ArrowDownwardIcon />}
            />
        </Link>
      </div>}

      {tab.url !== 'waiting' && <Picker data={detailTransactionReguler} />}
      <SenderAndReceiver classes={classes} data={detailTransactionReguler} />
      <DetailPacket classes={classes} data={detailTransactionReguler} />

      {detailTransactionReguler?.awb_number && <TrackingOrder detailTransactionReguler={detailTransactionReguler} id={id}/>}

      <ButtonByStatus tab={tab.url} />
      <ModalAddPicker
        show={state.modalAddPicker}
        onHide={() => setState({ ...state, modalAddPicker: false })}
        onSuccess={successAddPicker}
        id={detailTransactionReguler?.id}
      />
      <ModalConfirm
        show={state.modalConfirmShipping}
        onHide={() => setState({ ...state, modalConfirmShipping: false })}
        onAgree={successConfirmShipping}
        title="Selesai Shipping"
        description="Apakah anda yakin telah selesai melakukan proses shipping dan melanjutkan order ke proses manifest?"
      />
      <ModalConfirm
        show={state.modalConfirm}
        onHide={() => setState({ ...state, modalConfirm: false })}
        onAgree={handleConfirm}
        title="Konfirmasi Diterima"
        description="Apakah anda yakin telah menerima transaksi reguler yang dikirim ke gudang?"
      />
      <ModalConfirm
        show={state.modalConfirmAtWarehouse}
        onHide={() => setState({ ...state, modalConfirmAtWarehouse: false })}
        onAgree={handleConfirmAtWarehouse}
        title="Konfirmasi Sampai di Gudang"
        description="Apakah anda yakin telah menerima transaksi reguler yang sampai di gudang?"
      />
    </div>
  );
};

export default Detail;
