import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import TimeLine from '../../../../components/elements/Timeline/TimeLine';
import { Row, Col } from 'react-bootstrap';
import { getTracking } from '../reduxAction';

const useStyles = makeStyles({
  title: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    fontStyle: 'normal',
    fontSize: '14px',
    lineHeight: '20px',
  },
  titleCourier: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    fontStyle: 'normal',
    fontSize: '18px',
    lineHeight: '28px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4F4F4F',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  avatar: {
    width: '104px',
    height: '104px',
    marginRight: '8px',
  },
});

function TrackingOrder(props){
    const { detailTransactionReguler, id } = props
    const classes = useStyles();
    const dispatch = useDispatch();

    const { listTrackingOrder } = useSelector(
        (state) => state.cashlessWarehouse
    );
    
    useEffect(() => {
        if (id) {
            dispatch(getTracking(id));
        }
    }, [id])

    const totalPrice = detailTransactionReguler?.vendor_price + detailTransactionReguler?.insurance_price;

    return (
        <div className="w-100 p-3 border d-flex flex-wrap">
            <Row className="p-4" id="tracking">

                <Col lg={12} xs={12}>
                    <h6 className={classes.title}>Riwayat Pengiriman</h6>
                    <TimeLine data={listTrackingOrder}/>
                </Col>
            </Row>
        </div>
        
    );
}

export default TrackingOrder;
