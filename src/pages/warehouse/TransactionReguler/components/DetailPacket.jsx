import React from 'react'
import clsx from 'clsx'

export default function DetailPacket(props) {
    const { classes, data } = props
    return (
        <div>
            <div className="w-100 d-flex p-4 flex-column border rounded">
                <span className={classes.textTitle}>Detail Paket</span>
                <div className={clsx("d-flex flex-column", classes.contentBox)}>
                    <div className="w-50 d-flex justify-content-between">
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Kategori Produk</span>
                            <span className={classes.valueItem}>{data?.category_product?.name || 'Tidak ada data'}</span>
                        </div>
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Sub Kategori Produk</span>
                            <span className={classes.valueItem}>{data?.sub_category_product?.name || 'Tidak ada data'}</span>
                        </div>
                    </div>
                    <div className="w-75 d-flex justify-content-between">
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Panjang</span>
                            <span className={classes.valueItem}>{data?.length || 0} cm</span>
                        </div>
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Lebar</span>
                            <span className={classes.valueItem}>{data?.width || 0} cm</span>
                        </div>
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Tinggi</span>
                            <span className={classes.valueItem}>{data?.height || 0} cm</span>
                        </div>
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>Berat</span>
                            <span className={classes.valueItem}>{data?.weight || 0} kg</span>
                        </div>
                    </div>
                    <div className={classes.contentItem}>
                        <span className={classes.titleItem}>Nilai Paket</span>
                        <span className={classes.valueItem}>Rp {data?.item_price?.toLocaleString('id') || 'Tidak ada data'}</span>
                    </div>
                </div>
            </div>
            <div className="w-100 d-flex p-4 flex-column border rounded">
                <span className={classes.textTitle}>Kurir</span>
                <div className={clsx("d-flex flex-column", classes.contentBox)}>
                    <div className="w-100 d-flex justify-content-between">
                        {data?.vendor_service?.vendor?.logo_full_url 
                        ?
                        <img height={25} width={70} src={data?.vendor_service?.vendor?.logo_full_url} alt={data?.vendor_service?.vendor?.logo}/>
                        :
                        <span className={classes.titleItem}>Tidak ada logo</span>
                        }
                        <div className={classes.contentItem}>
                            <span className={classes.titleItem}>{data?.vendor_service?.vendor?.name || 'Tidak ada data'}</span>
                            <span className={classes.valueItem}>{data?.vendor_service?.service || 'Tidak ada data'}</span>
                        </div>
                        <span className={classes.valueItem}>Rp {data?.vendor_price?.toLocaleString('id') || 0}</span>
                    </div>
                    <div className="w-100 d-flex justify-content-between">
                        <span className={classes.titleItem}>Asuransi</span>
                        <span className={classes.valueItem}>Rp {data?.insurance_price?.toLocaleString('id') || 0}</span>
                    </div>
                    <div className="w-100 d-flex justify-content-between">
                        <span className={classes.valueItem}>Total</span>
                        <span className={classes.valueItem}>Rp {data?.vendor_price?.toLocaleString('id') || 0}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
