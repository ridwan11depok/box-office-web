import React, { useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import { setLoading } from '../../../../redux/actions';
import { useDispatch } from 'react-redux';

const ModalConfirm = (props) => {
  const dispatch = useDispatch()
  const { isLoading } = useSelector((state) => state.loading);
  useEffect(() => {
    if(isLoading){
      dispatch(setLoading(false))
    }
  },[])
  return (
    <>
      <Modal
        titleModal={props.title}
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => <p className="text-left">{props.description}</p>}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Konfirmasi'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

ModalConfirm.defaultProps = {
  title: 'Konfirmasi',
  description: 'Apakah yakin?',
};
export default ModalConfirm;
