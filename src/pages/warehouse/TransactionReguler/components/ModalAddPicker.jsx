import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { confirmPickupTransactionReguler } from '../reduxAction';

const validationSchema = Yup.object({
  picker_name: Yup.string().required('Harap mengisi nama picker'),
});

const ModalAddPicker = (props) => {
  const { id } = props;
  const history = useHistory();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      picker_name: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);

  const handleSubmit = async (values) => {
    try {
      await dispatch(confirmPickupTransactionReguler(values, id));
      props.onHide();
      props.onSuccess();
    } catch (err) {}
  };

  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal={'Picker'}
        titleClassName="title-modal"
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <Form.Group className="mb-1 mt-3" controlId="formBasicEmail">
              <Form.Label as={'h6'} className="input-label">
                Nama Picker
              </Form.Label>
              <Form.Control
                type=""
                placeholder="Input Nama Picker"
                {...formik.getFieldProps('picker_name')}
                style={{
                  border:
                    formik.touched.picker_name &&
                    formik.errors.picker_name &&
                    '1px solid #ee1b25',
                }}
              />
              {formik.touched.picker_name && formik.errors.picker_name && (
                <Form.Text className="text-input-error">
                  {formik.errors.picker_name}
                </Form.Text>
              )}
            </Form.Group>
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                formik.handleSubmit(e);
              }}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading ? 'Loading...' : 'Simpan'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalAddPicker.defaultProps = {
  initialValues: null,
};
export default ModalAddPicker;
