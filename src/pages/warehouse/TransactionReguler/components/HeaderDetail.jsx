import React from 'react';
import { useHistory } from 'react-router';
import BackButton from '../../../../components/elements/BackButton';
import Button from '../../../../components/elements/Button';
import { useDispatch } from 'react-redux';
import { generateAwb, printLabel } from '../reduxAction';

export default function HeaderDetail({ data, tab }) {
  const router = useHistory();
  const dispatch = useDispatch();
  
  const print = async () => {

    if(data?.awb_number === null){
        await dispatch(generateAwb(data?.id));
    }
    
    try {
      const response = await dispatch(printLabel(data?.id))
      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', data?.transaction_number+` - Shipping Label.pdf`);
      document.body.appendChild(link);
      link.click();
    }
    catch (err) { }
  } 

  return (
    <div className="w-100 p-3 d-flex justify-content-between border rounded flex-wrap">
      <BackButton
        label={data?.transaction_number || 'Kembali'}
        onClick={() => router.goBack()}
      />
      {tab?.tab === 3 && (
        <>
          <Button
              style={{ minWidth: '120px', margin: '3px 0px', marginLeft: '10px' }}
              text={data?.awb_number ? "Print Label" : 'Generate AWB & Print Label'}
              styleType="blueOutline"
              onClick={print}
          />
        </>
      )}
    </div>
  );
}
