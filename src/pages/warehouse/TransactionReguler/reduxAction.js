import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setErrorData, setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import axios from 'axios';
import baseUrl from '../../../api/url';

export const getListStore = () => async (dispatch, getState) => {
  const { token } = getState().login;
  try {
    let result = await consume.get('listStoreAdminWarehouse', {}, { token });
    dispatch(setListStore(result.result));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStore([]));
  }
};

function setListStore(data) {
  return {
    type: actionType.LIST_STORE_ADMIN_WAREHOUSE,
    payload: data,
  };
}

export const getTransactionReguler =
  (params = {}, id) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (id) {
        result = await consume.getWithParams(
          'transactionRegulerAdminWarehouse',
          {},
          { token },
          {},
          id
        );
        // console.log('resultId', result);
        dispatch(setDetailTransactionReguler(result.result));
      } else {
        result = await consume.getWithParams(
          'transactionRegulerAdminWarehouse',
          {},
          { token },
          params
        );
        dispatch(setTransactionReguler(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      if (id) {
        dispatch(setErrorData(err));
        dispatch(setDetailTransactionReguler({}));
      } else {
        dispatch(setTransactionReguler({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setTransactionReguler(data) {
  return {
    type: actionType.TRANSACTION_REGULER,
    payload: data,
  };
}

function setDetailTransactionReguler(data) {
  return {
    type: actionType.DETAIL_TRANSACTION_REGULER,
    payload: data,
  };
}

export const confirmPickupTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/pickup`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmAtWarehouseTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/at_warehouse`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmShippingTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/shipping`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmManifestTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/manifest`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const generateAwb =
  (id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const {
        token
      } = getState().login;
      let data = {};
      let headers = {
        token: token
      };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/generate-awb`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setDetailTransactionReguler(response.result));
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan generate AWB.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const printLabel =
  (id) => async (dispatch, getState) => {
    const {
      token,
      dataUser
    } = getState().login;
    try {

      const config = {
        url: baseUrl.transactionRegulerAdminWarehouse + id + '/shipping_label',
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/pdf`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
};

export const getTracking = (id) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    
    dispatch(setLoadingTable(true));
    try {
      
      let data = {};
      let headers = { token: token };
      let response = await consume.get(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/tracking`
      );
      dispatch(setTrackingOrder(response.result));
      dispatch(setLoadingTable(false));
      return Promise.resolve(response.result);
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal mengambil data tracking',
          type: 'error',
        })
      );
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
};

function setTrackingOrder(data) {
  return {
    type: actionType.LIST_TRACKING_ORDER,
    payload: data,
  };
}