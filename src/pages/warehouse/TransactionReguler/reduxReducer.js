import actionType from './reduxConstant';

const initialState = {
  listStore: [],
  transactionReguler: {},
  detailTransactionReguler: {},
  listTrackingOrder: []
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_STORE_ADMIN_WAREHOUSE: {
      const listStore = action.payload.map((item) => ({
        ...item,
        value: item.store_id,
        label: item.store.store_name,
      }));
      return {
        ...prevState,
        listStore,
      };
    }
    case actionType.TRANSACTION_REGULER:
      return {
        ...prevState,
        transactionReguler: action.payload,
      };
    case actionType.DETAIL_TRANSACTION_REGULER:
      return {
        ...prevState,
        detailTransactionReguler: action.payload,
      };
    case actionType.CASHLESS_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        cashless: action.payload,
      };
    case actionType.DETAIL_CASHLESS_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        detailCashless: action.payload,
      };
    case actionType.LIST_TRACKING_ORDER:
      return {
        ...prevState,
        listTrackingOrder: action.payload,
    };
    default:
      return prevState;
  }
};

export default inboundReducer;
