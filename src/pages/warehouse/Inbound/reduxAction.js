import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';

export const getAllInbound =
  (params = {}, inboundId) =>
  async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result;
      dispatch(setLoading(true));
      if (inboundId) {
        result = await consume.get(
          'inboundWarehouse',
          {},
          { token },
          inboundId
        );
        dispatch(setDetailInbound(result.result));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(true));
        result = await consume.getWithParams(
          'inboundWarehouse',
          {},
          { token },
          params
        );
        dispatch(setInbound(result.result));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      if (err.code === 422) {
        // console.log('errorr', err)
      }
      if (inboundId) {
        dispatch(setDetailInbound({}));
        dispatch(setLoading(false));
      } else {
        dispatch(setInbound({}));
        dispatch(setLoadingTable(false));
      }
      // return Promise.reject(err);
    }
  };

function setInbound(data) {
  return {
    type: actionType.INBOUND_WAREHOUSE,
    payload: data,
  };
}
function setDetailInbound(data) {
  return {
    type: actionType.DETAIL_INBOUND_WAREHOUSE,
    payload: data,
  };
}

export const confirmInbound =
  (payload = { status: null }, inboundId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'inboundWarehouseConfirmation',
        data,
        headers,
        inboundId
      );
      let prevStatus;
      if (data.status === 1) {
        prevStatus = 0;
      } else if (data.status === 2) {
        prevStatus = 1;
      } else if (data.status === 3) {
        prevStatus = 2;
      }
      dispatch(getAllInbound({ page: 1, per_page: 10, status: prevStatus }));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };
