import actionType from './reduxConstant';

const initialState = {
  inbounds: {},
  detailInbound: {},
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.INBOUND_WAREHOUSE:
      return {
        ...prevState,
        inbounds: action.payload,
      };
    case actionType.DETAIL_INBOUND_WAREHOUSE:
      return {
        ...prevState,
        detailInbound: action.payload,
      };
    default:
      return prevState;
  }
};

export default inboundReducer;
