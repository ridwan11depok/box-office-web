import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import DataTable from '../../../../components/elements/Table/Table';
import Avatar from '@material-ui/core/Avatar';
import Badge from '../../../../components/elements/Badge';
import { useSelector } from 'react-redux';

const ImageColumn = ({ images }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {images.map((item) => (
        <Avatar
          style={{ margin: '2px' }}
          alt="Logo"
          variant="rounded"
          src={item}
        />
      ))}
    </div>
  );
};

const ModalDetailInbound = (props) => {
  const { detailInbound } = useSelector((state) => state.inboundWarehouse);
  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal="Detail Inbound"
        bodyClassName="p-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="lg"
        bodyStyle={{ border: 'none' }}
        data={props.data}
        scrollable
        content={(data) => (
          <div>
            <div className="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center pl-4 pr-4">
              <h6>Kode Inbound: {detailInbound?.order_number || '-'}</h6>
              <h6>Store: {detailInbound?.store?.store_name || '-'}</h6>
              {detailInbound?.is_request_pickup === 1 &&
              detailInbound?.picker_name ? (
                <h6>Picker: {detailInbound?.picker_name || '-'}</h6>
              ) : (
                <h6>
                  Request Pickup :{' '}
                  <span>
                    <Badge
                      label={
                        detailInbound?.is_request_pickup === 0 ? 'No' : 'Yes'
                      }
                      styleType={
                        detailInbound?.is_request_pickup === 0 ? 'red' : 'green'
                      }
                      style={{ fontSize: '14px' }}
                    />
                  </span>
                </h6>
              )}
            </div>
            <DataTable
              column={[
                {
                  heading: 'Nama Barang',
                  key: 'name',
                },
                {
                  heading: 'SKU',
                  key: 'sku',
                },
                {
                  heading: 'Gambar',
                  render: (data) => <ImageColumn images={data?.images} />,
                },
                {
                  heading: 'Qty',
                  key: 'qty',
                },
              ]}
              data={detailInbound?.item}
              transformData={(item) => ({
                ...item,
                name: item?.product?.name,
                sku: item?.product?.sku,
                qty: item?.quantity_shipped,
                images: item?.product?.images_full_url || [],
              })}
              withNumber={false}
              showFooter={false}
              showBorderContainer={false}
            />
          </div>
        )}
        data={props.data}
        footerClassName="p-3"
        footer={(data) => (
          <>
            {detailInbound?.status != 3 ? (
              <>
                <Button
                  styleType="lightBlueFill"
                  text="Batal"
                  onClick={props.onHide}
                  style={{ minWidth: '120px' }}
                />
                <Button
                  className="ml-2"
                  styleType="blueFill"
                  text={
                    isLoading
                      ? 'Loading...'
                      : (detailInbound?.is_request_pickup === 0 &&
                          detailInbound?.status === 0) ||
                        detailInbound?.status === 1
                      ? 'Konfirmasi Sudah Diterima'
                      : 'Konfirmasi Pickup'
                  }
                  onClick={props.onAgree}
                  style={{ minWidth: '120px' }}
                />
              </>
            ) : (
              <Button
                styleType="lightBlueFill"
                text="Kembali"
                onClick={props.onHide}
                style={{ minWidth: '120px' }}
              />
            )}
          </>
        )}
      />
    </>
  );
};

export default ModalDetailInbound;
