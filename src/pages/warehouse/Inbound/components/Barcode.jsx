// import React, { Component } from 'react';
// // import logo from './logo.svg';
// // import './App.css';
// import bwipjs from 'bwip-js';

// class App extends Component {
//   componentDidMount() {
//     try {
//       // The return value is the canvas element
//       bwipjs.toCanvas('mycanvas', {
//         bcid: 'code128', // Barcode type
//         text: '0123456789', // Text to encode
//         scale: 1, // 3x scaling factor
//         height: 15, // Bar height, in millimeters
//         includetext: true, // Show human-readable text
//         textxalign: 'center', // Always good to set this
//         // width: 15,
//         // height: 100,
//         textyoffset: 3,
//       });
//     } catch (e) {
//       // `e` may be a string or Error object
//     }
//   }
//   render() {
//     return (
//       <div className="App">
//         <canvas id="mycanvas"></canvas>
//       </div>
//     );
//   }
// }
// export default App;

import React, { Component, PropTypes } from 'react';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import bwipjs from 'bwip-js';

export default class Export extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inbound: '12345678',
      sku: [
        '0123456780',
        '1123456781',
        '0123456782',
        '0123456783',
        '0123456784',
        '0123456785',
        '0123456786',
        '1123456787',
        '0123456788',
        '0123456789',
        '01234567810',
        '01234567811',
        '01234567812',
        '11234567813',
        '01234567814',
        '01234567815',
        '01234567816',
        '01234567817',
        '01234567818',
        '11234567819',
        '01234567820',
        '01234567821',
        '01234567822',
        '01234567823',
      ],
    };
  }

  componentDidMount() {
    this.generateAllBarcode();
  }

  componentDidUpdate(prevProps) {
    if (this.props.event !== prevProps.event && this.props.event === 'print') {
      this.printDocument();
    }
  }

  async generateAllBarcode() {
    const { listProduct } = this.props;
    const { sku } = this.state;
    const listSKU = listProduct
      .filter((item) => item.scanned)
      .map((item) => item.sku);
    try {
      for (let i = 0; i < listSKU.length; i++) {
        await this.generateBarcode(listSKU[i]);
      }
    } catch (err) {}
  }

  async generateBarcode(value) {
    try {
      // The return value is the canvas element
      await bwipjs.toCanvas(`canvas_${value}`, {
        bcid: 'code128', // Barcode type
        text: `${value}`, // Text to encode
        scale: 1, // 3x scaling factor
        height: 15, // Bar height, in millimeters
        includetext: true, // Show human-readable text
        textxalign: 'center', // Always good to set this
        // width: 35,
        textyoffset: 3,
      });
      Promise.resolve('success');
    } catch (e) {
      // `e` may be a string or Error object
      Promise.reject('failed');
    }
  }
  printDocument() {
    const input = document.getElementById('divToPrint');
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL('image/png');

      //paging
      let imgWidth = 100;
      let pageHeight = 150;
      let imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;

      //create pdf
      const pdf = new jsPDF({
        orientation: 'portrait',
        unit: 'mm',
        format: [100, 150],
      });
      let positionY = 0;

      pdf.addImage(imgData, 'PNG', 0, positionY, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        positionY = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(imgData, 'PNG', 0, positionY, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      // pdf.output('dataurlnewwindow');
      pdf.save('download.pdf');
    });
  }

  render() {
    const { listProduct } = this.props;
    const { sku } = this.state;
    const listSKU = listProduct
      .filter((item) => item.scanned)
      .map((item) => item.sku);
    return (
      <div style={{ border: '1px solid #000011' }}>
        <div id="divToPrint" style={{ width: '100mm' }}>
          <div className="row p-3 m-0">
            {listSKU.map((item, idx) => (
              <div
                className="col-6 d-flex justify-content-center align-items-center p-2"
                style={{
                  border: '1px dashed #000000',
                  marginBottom:
                    ((idx + 1) % 14 === 0 || (idx + 2) % 14 === 0) && '65px',
                }}
              >
                <canvas id={`canvas_${item}`}></canvas>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
