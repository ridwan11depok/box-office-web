import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllInbound, confirmInbound } from '../reduxAction';
import {
  ReactSelect,
  SearchField,
} from '../../../../components/elements/InputField';
import ModalDetailInbound from './ModalDetailInbound';
import Badge from '../../../../components/elements/Badge';
import CountTable from './CountTable';
import moment from 'moment';
import localization from 'moment/locale/id';
import DatePicker from '../../../../components/elements/InputField/DatePicker';
import ModalPicker from './ModalPicker';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const HeaderTable = (props) => {
  return (
    <div className="d-flex flex-row flex-wrap justify-content-between align-items-center p-3 w-100">
      <div style={{ width: '200px' }}>
        {' '}
        <DatePicker
          onDateChange={props.handleFilterDate}
          format="DD MMMM yyyy"
          label="Tanggal"
          prefix
          suffix={false}
        />
        {/* <ReactSelect label="Tanggal" placeholder="Pilih Tanggal" /> */}
      </div>
      <SearchField
        placeholder="Cari Kode, Seller"
        onEnter={props.handleSearch}
      />
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses Inbound yang masuk saat ini.
      </h6>
    </div>
  );
};

const ActionColumn = ({ data, tab, onClick, handleShowCountTable }) => {
  const dispatch = useDispatch();
  return tab === 2 ? (
    <Button
      text="Perhitungan Barang"
      styleType="blueFill"
      onClick={() => {
        dispatch(getAllInbound({}, data?.id));
        handleShowCountTable(true, data);
      }}
    />
  ) : (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => {
        dispatch(getAllInbound({}, data?.id));
        onClick();
      }}
    />
  );
};

const StatusColumn = ({ status = 1 }) => {
  return (
    <Badge
      label={status === 0 ? 'No' : 'Yes'}
      styleType={status === 0 ? 'red' : 'green'}
      style={{ fontSize: '14px' }}
    />
  );
};

function InboundTable(props) {
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    counting: false,
    date: '',
    showPicker: '',
  });
  const [selectedRow, setSelectedRow] = useState({});
  const dispatch = useDispatch();
  const fetchListInbound = (params) => {
    dispatch(getAllInbound(params));
  };
  const { inbounds, detailInbound } = useSelector(
    (state) => state.inboundWarehouse
  );

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  const showCountTable = (isCount, selectedRow = {}) => {
    setState({ ...state, counting: isCount, selectedRow });
    setSelectedRow(selectedRow);
  };

  useEffect(() => {
    if (props.tab !== 2) {
      showCountTable(false, {});
      setSelectedRow({});
    }
  }, [props.tab]);

  const handleFilterDate = (date) => {
    let dateSelected;
    if (date) {
      dateSelected = moment(date)
        .locale('id', localization)
        .format('yyyy-MM-DD');
    } else {
      dateSelected = '';
    }
    setState({ ...state, date: dateSelected });
  };

  const onAgreeModalDetail = async () => {
    if (
      (state.selectedRow?.is_request_pickup === 0 &&
        state.selectedRow?.status === 0) ||
      state.selectedRow?.status === 1
    ) {
      // Konfirmasi Sudah diterima, ke gudang/ status=2
      try {
        const payload = {
          status: 2,
        };
        const inboundId = detailInbound?.id;
        await dispatch(confirmInbound(payload, inboundId));
        setState({ ...state, showDetail: false });
      } catch (err) {}
    } else if (
      state.selectedRow?.is_request_pickup === 1 &&
      state.selectedRow?.status === 0
    ) {
      // Konfirmasi Pickup, ke status=1
      setState({ ...state, showDetail: false, showPicker: true });
    }
  };

  return (
    <div>
      {state.counting ? (
        <CountTable
          show={state.counting}
          showCountTable={showCountTable}
          data={selectedRow}
        />
      ) : (
        <Table
          action={fetchListInbound}
          totalPage={inbounds?.last_page || 1}
          params={{
            search: state.search,
            status: props.tab !== 4 ? props.tab : null,
            date: state.date ? state.date : null,
          }}
          variableTriggerAction={[props.tab]}
          column={[
            {
              heading: 'Kode Inbound',
              key: 'order_number',
            },
            {
              heading: 'Store',
              key: 'store_name',
            },
            {
              heading: 'Last Update',
              key: 'updated_at',
            },
            {
              heading: 'Request Pickup',
              render: (data) => (
                <StatusColumn status={data.is_request_pickup} />
              ),
            },
            {
              heading: 'Aksi',
              render: (data) => (
                <ActionColumn
                  data={data}
                  onClick={() =>
                    setState({ ...state, selectedRow: data, showDetail: true })
                  }
                  tab={props.tab}
                  handleShowCountTable={showCountTable}
                />
              ),
            },
          ]}
          data={inbounds?.data || []}
          transformData={(item) => ({
            ...item,
            updated_at: moment(item?.updated_at)
              .locale('id', localization)
              .format('DD MMMM yyyy HH:mm:ss'),
            store_name: item?.store?.store_name || '-',
          })}
          renderHeader={() => (
            <HeaderTable
              handleSearch={handleSearch}
              handleFilterDate={handleFilterDate}
            />
          )}
          withNumber={false}
          renderEmptyData={() => <EmptyData />}
        />
      )}

      <ModalDetailInbound
        show={state.showDetail}
        onHide={() => setState({ ...state, showDetail: false })}
        data={state.selectedRow}
        onAgree={onAgreeModalDetail}
      />
      <ModalPicker
        show={state.showPicker}
        onHide={() => setState({ ...state, showPicker: false })}
        data={state.selectedRow}
      />
    </div>
  );
}

export default InboundTable;
