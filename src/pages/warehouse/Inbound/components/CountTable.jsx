import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { TextField } from '../../../../components/elements/InputField';
import { formatRupiah } from '../../../../utils/text';
import Counter from '../../../../components/elements/Counter';
import { confirmInbound } from '../reduxAction';
import ModalConfirmAccepted from './ModalConfirmAccepted';
import ModalBarcode from './ModalBarcode';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
  },
});

const HeaderTable = ({
  onClickBack,
  inboundCode,
  storeName,
  handleFillBarcode,
  valueBarcode,
  handleShowBarcode,
}) => {
  const classes = useStyles();
  return (
    <div className="d-flex flex-column w-100">
      <div className="d-flex flex-row justify-content-between align-items-center w-100 p-2 pl-3">
        <h6 className={classes.titleTable}>
          <i
            onClick={() => onClickBack(false, {})}
            className="fas fa-arrow-left mr-3"
            style={{ fontSize: '18px', cursor: 'pointer' }}
          ></i>{' '}
          Perhitungan Barang
        </h6>
        <Button
          text="Print Barcode"
          styleType="blueOutline"
          onClick={handleShowBarcode}
        />
      </div>
      <div
        className="d-flex flex-row justify-content-between align-items-center p-2 pl-3"
        style={{
          borderTop: '1px solid #E8E8E8',
        }}
      >
        <h6 className="mb-0">Kode Inbound: {inboundCode}</h6>
        <h6 className="mb-0">Store: {storeName}</h6>
        <TextField
          prefix={() => <i class="fa fa-barcode" aria-hidden="true"></i>}
          onChange={handleFillBarcode}
          value={valueBarcode}
        />
      </div>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada barang saat ini.</h6>
    </div>
  );
};

const QuantityAccepted = ({ data, handleCounter }) => {
  return (
    <div>
      <Counter
        onDecrease={() => handleCounter(data, '-')}
        onIncrease={() => handleCounter(data, '+')}
        value={data?.qty_will_accepted}
      />
    </div>
  );
};

function CountTable(props) {
  const [state, setState] = useState({
    listProduct: [],
    showConfirmAccept: false,
    valueBarcode: '',
    showModalBarcode: false,
  });
  const dispatch = useDispatch();
  const { detailInbound } = useSelector((state) => state.inboundWarehouse);

  const handleCounter = (itemProduct, action = '+') => {
    if (action === '+' && itemProduct.qty_will_accepted >= 0) {
      const newListProduct = state.listProduct.map((item) => {
        if (item.id === itemProduct.id && item.scanned) {
          return {
            ...item,
            qty_will_accepted: itemProduct.qty_will_accepted + 1,
          };
        } else {
          return item;
        }
      });
      setState({ ...state, listProduct: newListProduct });
    } else if (action === '-' && itemProduct.qty_will_accepted > 0) {
      const newListProduct = state.listProduct.map((item) => {
        if (item.id === itemProduct.id) {
          return {
            ...item,
            qty_will_accepted: itemProduct.qty_will_accepted - 1,
          };
        } else {
          return item;
        }
      });
      setState({ ...state, listProduct: newListProduct });
    }
  };

  const handleCheckQuantityAccepted = () => {
    const checkQuantity = state.listProduct.filter(
      (item) => item.qty_will_accepted != item.quantity_shipped
    );
    if (state.listProduct.length === 0) {
      return false;
    } else if (checkQuantity.length > 0) {
      return false;
    } else {
      return true;
    }
  };

  useEffect(() => {
    const listProduct = detailInbound?.item
      ? detailInbound.item.map((item) => ({
          ...item,
          name: item?.product?.name,
          sku: item?.product?.sku,
          quantity_shipped: item?.quantity_shipped,
          sell_price: item?.product?.sell_price
            ? formatRupiah(item?.product?.sell_price)
            : '-',
          qty_will_accepted: 0,
          scanned: false,
        }))
      : [];
    setState({ ...state, listProduct });
  }, [props.show, detailInbound]);

  const handleFillBarcode = (e) => {
    const value = e.target.value;
    const newListProduct = state.listProduct.map((item) => {
      if (item.sku === value) {
        return {
          ...item,
          qty_will_accepted: item.qty_will_accepted + 1,
          scanned: true,
        };
      } else {
        return item;
      }
    });
    setState({ ...state, listProduct: newListProduct, valueBarcode: value });
    setTimeout(() => {
      setState({ ...state, listProduct: newListProduct, valueBarcode: '' });
    }, 500);
  };

  const confirmAcceptInbound = async () => {
    const item_id = state.listProduct?.map((item) => item?.id);
    const quantity = state.listProduct?.map((item) => item?.qty_will_accepted);
    const payload = {
      status: 3,
      item_id,
      quantity,
    };
    const inboundId = detailInbound?.id;
    try {
      await dispatch(confirmInbound(payload, inboundId));
      props.showCountTable(false, {});
    } catch (err) {}
  };

  const handleShowBarcode = () => {
    setState({ ...state, showModalBarcode: true });
  };
  return (
    <div
      style={{
        border: '1px solid #E8E8E8',
      }}
    >
      <Table
        column={[
          {
            heading: 'Nama Barang',
            key: 'name',
          },
          {
            heading: 'SKU',
            key: 'sku',
          },
          {
            heading: 'Harga',
            key: 'sell_price',
          },
          {
            heading: 'Qty yang Dikirim',
            key: 'quantity_shipped',
          },
          {
            heading: 'Qty yang Diterima',
            render: (data) => (
              <QuantityAccepted data={data} handleCounter={handleCounter} />
            ),
          },
        ]}
        data={state.listProduct}
        renderHeader={() => (
          <HeaderTable
            onClickBack={props.showCountTable}
            inboundCode={detailInbound?.order_number || '-'}
            storeName={detailInbound?.store?.store_name || '-'}
            handleFillBarcode={handleFillBarcode}
            valueBarcode={state.valueBarcode}
            handleShowBarcode={handleShowBarcode}
          />
        )}
        withNumber={false}
        showFooter={false}
        renderEmptyData={() => <EmptyData />}
        showBorderContainer={false}
      />
      <div className="w-100 d-flex justify-content-end p-3">
        <Button
          className="ml-auto"
          style={{ marginLeft: 'auto' }}
          text="Konfirmasi Sudah Diterima"
          styleType={
            handleCheckQuantityAccepted() ? 'blueFill' : 'blueFillDisabled'
          }
          disabled={!handleCheckQuantityAccepted()}
          onClick={() => setState({ ...state, showConfirmAccept: true })}
        />
      </div>
      <ModalConfirmAccepted
        show={state.showConfirmAccept}
        onHide={() => setState({ ...state, showConfirmAccept: false })}
        onAgree={confirmAcceptInbound}
      />
      <ModalBarcode
        show={state.showModalBarcode}
        onHide={() => setState({ ...state, showModalBarcode: false })}
        listProduct={state.listProduct}
        detailInbound={detailInbound}
      />
    </div>
  );
}

export default CountTable;
