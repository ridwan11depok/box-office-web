import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { WarehouseSelect } from '../../../components/elements/InputField';
import { makeStyles } from '@material-ui/core/styles';
import InboundTable from './components/InboundTable';
import { TabHeader } from '../../../components/elements/Tabs';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});

const Inbound = (props) => {
  const classes = useStyles();
  const [state, setState] = useState({ tab: 0 });
  const changeTab = (tab) => {
    setState({ ...state, tab });
  };
  return (
    <ContentContainer title="Inbound">
      <ContentItem className="mb-3">
        <TabHeader
          border={false}
          activeTab={state.tab}
          listHeader={[
            {
              tab: 0,
              title: 'Kirim',
            },
            {
              tab: 1,
              title: 'Pickup',
            },
            {
              tab: 2,
              title: 'Sampai Gudang',
            },
            {
              tab: 3,
              title: 'Selesai',
            },
            {
              tab: 4,
              title: 'All',
            },
          ]}
          onChange={changeTab}
        />
      </ContentItem>
      <ContentItem border={false}>
        <InboundTable tab={state.tab} />
      </ContentItem>
    </ContentContainer>
  );
};

export default Inbound;
