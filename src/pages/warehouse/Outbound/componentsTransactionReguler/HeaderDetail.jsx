import React from 'react';
import { useHistory } from 'react-router';
import BackButton from '../../../../components/elements/BackButton';
import Button from '../../../../components/elements/Button';
import { useDispatch } from 'react-redux';
import { useDownloadFile } from '../../../../utils/hook';
import { generateAWBRegularTransaction } from '../reduxAction';

export default function HeaderDetail({ data, tab }) {
  const router = useHistory();
  const dispatch = useDispatch();
  const handleExportLogs = () => {
    const payload = {
      id: data?.id,
    };
    return dispatch(generateAWBRegularTransaction(payload));
  };
  const getFileName = () => {
    return `Regular-Transaction.xlsx`;
  };
  const { ref, url, download, name } = useDownloadFile({
    apiDefinition: handleExportLogs,
    // preDownloading,
    // postDownloading,
    // onError: onErrorDownloadFile,
    getFileName,
  });
  return (
    <div className="w-100 p-3 d-flex justify-content-between border rounded flex-wrap">
      <BackButton
        label={data?.transaction_number || 'Kembali'}
        onClick={() => router.goBack()}
      />
      {tab?.tab === 3 && (
        <>
          <a href={url} download={name} className="hidden" ref={ref} />
          <Button
            style={{ minWidth: '250px', margin: '3px 0px' }}
            text="Generate AWB & Print Label"
            styleType="blueOutline"
            onClick={download}
          />
        </>
      )}
    </div>
  );
}
