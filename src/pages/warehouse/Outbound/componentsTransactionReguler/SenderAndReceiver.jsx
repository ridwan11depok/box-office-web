import React, { useState } from 'react';
import clsx from 'clsx';
import { MapOutlined } from '@material-ui/icons';
import Button from '../../../../components/elements/Button';
import MapModal from '../../../../components/elements/Map/MapModal';

export default function SenderAndReceiver(props) {
  const { classes, data } = props;
  const [state, setState] = useState({
    showMapSender: false,
    showMapReceiver: false,
  });
  return (
    <div className="w-100 p-3 border d-flex flex-wrap">
      <span className={clsx(classes.textTitle, 'w-100')}>
        Data Pengirim dan Penerima
      </span>
      <div className="w-100 d-flex flex-column flex-lg-row">
        <div className={clsx('d-flex flex-column pr-3', classes.contentBox)}>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Nama Pelanggan</span>
            <span>{data?.sender_name || 'Tidak ada data'}</span>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Nomor Telepon</span>
            <span className={classes.textBold}>
              {data?.sender_phone_number || 'Tidak ada data'}
            </span>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Alamat</span>
            <span>{data?.sender_address || 'Tidak ada data'}</span>
          </div>
          <div>
            <Button
              styleType="blueNoFill"
              className="p-0"
              text="Lihat Lokasi di Peta"
              startIcon={() => <MapOutlined />}
              onClick={() => setState({ ...state, showMapSender: true })}
            />
          </div>
          <div className="w-100 d-flex justify-content-between flex-wrap">
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Provinsi</span>
              <span className={classes.textBold}>
                {data?.sender_province?.name || 'Tidak ada data'}
              </span>
            </div>
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Kota</span>
              <span className={classes.textBold}>
                {data?.sender_city?.name || 'Tidak ada data'}
              </span>
            </div>
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Kecamatan</span>
              <span className={classes.textBold}>
                {data?.sender_district?.name || 'Tidak ada data'}
              </span>
            </div>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Pesan</span>
            <span>{data?.sender_note || 'Tidak ada data'}</span>
          </div>
        </div>
        <div className={clsx('d-flex flex-column pr-3', classes.contentBox)}>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Nama Pelanggan</span>
            <span>{data?.receiver_name || 'Tidak ada data'}</span>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Nomor Telepon</span>
            <span className={classes.textBold}>
              {data?.receiver_phone_number || 'Tidak ada data'}
            </span>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Alamat</span>
            <span>{data?.receiver_address || 'Tidak ada data'}</span>
          </div>
          <div>
            <Button
              styleType="blueNoFill"
              className="p-0"
              text="Lihat Lokasi di Peta"
              startIcon={() => <MapOutlined />}
              onClick={() => setState({ ...state, showMapReceiver: true })}
            />
          </div>
          <div className="w-100 d-flex justify-content-between flex-wrap">
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Provinsi</span>
              <span className={classes.textBold}>
                {data?.receiver_province?.name || 'Tidak ada data'}
              </span>
            </div>
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Kota</span>
              <span className={classes.textBold}>
                {data?.receiver_city?.name || 'Tidak ada data'}
              </span>
            </div>
            <div className={clsx(classes.contentItem, 'pr-1')}>
              <span>Kecamatan</span>
              <span className={classes.textBold}>
                {data?.receiver_district?.name || 'Tidak ada data'}
              </span>
            </div>
          </div>
          <div className={classes.contentItem}>
            <span className={classes.titleItem}>Pesan</span>
            <span>{data?.receiver_note || 'Tidak ada data'}</span>
          </div>
        </div>
      </div>
      <MapModal
        show={state.showMapSender}
        onHide={() => setState({ ...state, showMapSender: false })}
        initialCoordinate={{
          lat: data?.sender_latitude,
          lng: data?.sender_longitude,
        }}
        mode="viewer"
      />
      <MapModal
        show={state.showMapReceiver}
        onHide={() => setState({ ...state, showMapReceiver: false })}
        initialCoordinate={{
          lat: data?.receiver_latitude,
          lng: data?.receiver_longitude,
        }}
        mode="viewer"
      />
    </div>
  );
}
