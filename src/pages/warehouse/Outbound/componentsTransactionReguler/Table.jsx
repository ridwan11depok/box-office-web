import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getListStore, getTransactionReguler } from '../reduxAction';
import { getListVendor } from '../../../customer/Outbound/reduxAction';
import {
  ReactSelect,
  SearchField,
} from '../../../../components/elements/InputField';
import moment from 'moment';
import localization from 'moment/locale/id';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  textDetail: {
    color: '#182953',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    cursor: 'pointer',
  },
});

const HeaderTable = ({
  handleSearch,
  handleSelectVendor,
  handleSelectStore,
  tab,
}) => {
  const classes = useStyles();
  const { listVendor } = useSelector((state) => state.outboundCustomer);
  const { listStore } = useSelector((state) => state.outboundWarehouse);
  return (
    <div className="d-flex flex-column w-100">
      <Row className="p-3">
        <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
          <ReactSelect
            label="Store"
            placeholder="Pilih store"
            options={listStore}
            onChange={handleSelectStore}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
          <ReactSelect
            label="Logistik"
            placeholder="Pilih logistik"
            options={listVendor}
            onChange={handleSelectVendor}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="d-flex align-items-end mb-2">
          <SearchField
            placeholder="Cari pengirim, penerima, atau picker"
            className="w-100"
            onChange={handleSearch}
          />
        </Col>
      </Row>
      <div
        className="d-flex flex-row justify-content-between align-items-center p-3"
        style={{
          borderTop: '1px solid #E8E8E8',
        }}
      >
        <h6 className={classes.titleTable}>{tab?.title} List</h6>
        {tab?.id === 2 && (
          <Button
            text="Manifest"
            styleType="blueOutline"
            style={{ minWidth: '120px' }}
          />
        )}
      </div>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang masuk saat ini.
      </h6>
    </div>
  );
};

const ActionColumn = ({ data, onClick, tab }) => {
  const textButton = [
    'Start Order',
    'Already Picked',
    'Confirm at Warehouse',
    'Ready to Shipped',
    'Detail',
    'Detail',
  ];
  const classes = useStyles();
  return (
    // <span onClick={onClick} className={classes.textDetail}>
    <Button
      text={textButton[tab?.tab]}
      styleType={tab?.tab === 4 || tab?.tab === 5 ? 'blueNoFill' : 'blueFill'}
      onClick={onClick}
      style={{ minWidth: '120px' }}
    />
    //   Detail
    // </span>
  );
};

function TableTransactionReguler(props) {
  const { tab } = props;
  const router = useHistory();
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    date: '',
    showPicker: '',
  });
  const dispatch = useDispatch();

  const fetchListTransactionReguler = (params) => {
    // console.log('params', params);
    dispatch(getTransactionReguler(params));
  };

  const { transactionReguler } = useSelector(
    (state) => state.outboundWarehouse
  );

  const handleSearch = (e) => {
    setState({ ...state, search: e.target.value });
  };

  const handleSelectVendor = (e) => {
    setState({ ...state, vendorId: e.target.value });
  };

  const handleSelectStore = (e) => {
    setState({ ...state, storeId: e.target.value });
  };

  useEffect(() => {
    dispatch(getListVendor());
    dispatch(getListStore());
  }, [props.tab]);

  return (
    <div>
      <Table
        action={fetchListTransactionReguler}
        totalPage={transactionReguler?.last_page || 1}
        params={{
          search: state.search,
          status: tab.id,
          store_id: state.storeId,
          vendor_id: state.vendorId,
        }}
        variableTriggerAction={[props.tab]}
        column={[
          {
            heading: 'Tanggal',
            key: 'date',
          },
          {
            heading: 'Nomor Transaksi',
            key: 'transaction_number',
          },

          {
            heading: 'Pengirim',
            key: 'sender_name',
          },
          {
            heading: 'Penerima',
            key: 'receiver_name',
          },
          {
            heading: 'Logistik',
            key: 'vendor',
          },
          {
            heading: 'Picker',
            key: 'picker_name',
          },
          {
            heading: 'Aksi',
            render: (item) => (
              <ActionColumn
                data={item}
                onClick={() => {
                  router.push(
                    `/warehouse/outbound/regular-transaction/${tab.url}/${item.id}`
                  );
                }}
                tab={tab}
              />
            ),
          },
        ]}
        data={transactionReguler?.data || []}
        transformData={(item) => ({
          ...item,
          date: moment(item?.created_at)
            .locale('id', localization)
            .format('DD MMMM yyyy HH:mm:ss'),
          vendor: item?.vendor_service?.vendor?.name || '-',
        })}
        renderHeader={() => (
          <HeaderTable
            handleSearch={handleSearch}
            handleSelectStore={handleSelectStore}
            handleSelectVendor={handleSelectVendor}
            tab={tab}
          />
        )}
        withNumber={false}
        renderEmptyData={() => <EmptyData />}
      />
      {/* <ModalDetailInbound
        show={state.showDetail}
        onHide={() => setState({ ...state, showDetail: false })}
        data={state.selectedRow}
        onAgree={onAgreeModalDetail}
      />
      <ModalPicker
        show={state.showPicker}
        onHide={() => setState({ ...state, showPicker: false })}
        data={state.selectedRow}
      /> */}
    </div>
  );
}

export default TableTransactionReguler;
