import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Button from '../../../../components/elements/Button';
import HeaderDetail from './HeaderDetail';
import DetailPacket from './DetailPacket';
import { useDispatch } from 'react-redux';
import {
  confirmAtWarehouseTransactionReguler,
  confirmShippingTransactionReguler,
  getTransactionReguler,
  confirmManifestTransactionReguler,
} from '../reduxAction';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import ModalAddPicker from './ModalAddPicker';
import ModalConfirm from '../components/ModalConfirm';
import ModalAWB from './ModalAWB';
import SenderAndReceiver from './SenderAndReceiver';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

const useStyles = makeStyles({
  contentBox: {
    // gap: '15px',
    marginTop: '20px',
    flex: 0.5,
  },
  contentItem: {
    display: 'flex',
    flexDirection: 'column',
  },
  textTitle: {
    color: '#1C1C1C',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    fontWeight: 'bold',
  },
  titleItem: {
    color: '#4F4F4F',
  },
  textBold: {
    color: '#363636',
    fontWeight: 'bold',
  },
});

const DetailDispatchOrder = (props) => {
  const { id, changeTab, tab } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useHistory();
  const [state, setState] = useState({
    modalAddPicker: false,
    modalConfirm: false,
    modalAWB: false,
    modalConfirmAtWarehouse: false,
    isCopiedAWB: false,
  });

  const { detailTransactionReguler } = useSelector(
    (state) => state.outboundWarehouse
  );

  useEffect(() => {
    if (id) {
      getDetailTransactionReguler();
    }
  }, [id]);
  useEffect(() => {
    if (state.isCopiedAWB) {
      setTimeout(() => {
        setState({ ...state, isCopiedAWB: false });
      }, 3000);
    }
  }, [state.isCopiedAWB]);

  const getDetailTransactionReguler = async () => {
    try {
      await dispatch(getTransactionReguler({}, id));
    } catch (err) {
      // console.log('error', err);
    }
  };

  const successAddPicker = () => {
    changeTab(1);
  };

  const successConfirmShipping = async () => {
    try {
      await dispatch(confirmManifestTransactionReguler({}, id));
      router.push('/warehouse/manifest');
    } catch (err) {}
  };

  const handleConfirm = async () => {
    try {
      await dispatch(confirmAtWarehouseTransactionReguler({}, id));
      changeTab(2);
    } catch (err) {}
  };

  const handleConfirmAtWarehouse = async () => {
    try {
      await dispatch(confirmShippingTransactionReguler({}, id));
      changeTab(3);
    } catch (err) {}
  };

  const ButtonByStatus = ({ tab }) => {
    return (
      <div className="w-100 d-flex flex-row mt-3 justify-content-end">
        <Button
          text="Kembali"
          styleType="lightBlueFill"
          style={{ minWidth: '120px' }}
          onClick={() => router.goBack()}
        />
        {tab === 'waiting' && (
          <Button
            styleType="blueFill"
            text="Start Order"
            onClick={() => setState({ ...state, modalAddPicker: true })}
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'pickup' && (
          <Button
            styleType="blueFill"
            className="p-2"
            text="Already Picked"
            onClick={() => setState({ ...state, modalConfirm: true })}
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'at_warehouse' && (
          <Button
            styleType="blueFill"
            className="p-2"
            text="Confirm at Warehouse"
            onClick={() =>
              setState({ ...state, modalConfirmAtWarehouse: true })
            }
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
        {tab === 'shipping' && (
          <Button
            styleType="blueFillDisabled"
            className="p-2"
            text="Ready to Shipped"
            onClick={() => setState({ ...state, modalAWB: true })}
            disabled
            style={{ minWidth: '120px', marginLeft: '5px' }}
          />
        )}
      </div>
    );
  };

  const TotalPayment = ({ data }) => {
    return (
      <div className="w-100 d-flex p-3 flex-column border rounded">
        <span className={classes.textTitle}>Total Pembayaran</span>
        <div className={clsx('d-flex flex-column', classes.contentBox)}>
          <div className="w-100 d-flex justify-content-between">
            <span>Total Pembayaran Logistik</span>
            <span className={classes.textBold}>
              Rp {data?.vendor_price?.toLocaleString('id') || 0}
            </span>
          </div>
          <div className="w-100 d-flex justify-content-between">
            <span>Biaya Pickup</span>
            <span className={classes.textBold}>
              Rp {data?.pickup_price?.toLocaleString('id') || 0}
            </span>
          </div>
          <div className="w-100 d-flex justify-content-between">
            <span className={classes.textBold}>Total Pembayaran</span>
            <span className={classes.textBold}>
              Rp {data?.total_price?.toLocaleString('id') || 0}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const Picker = ({ data }) => {
    return (
      <div className="w-100 d-flex p-3 flex-column border ">
        <span className={classes.textTitle}>Picker</span>
        <div className={clsx('d-flex flex-column', classes.contentBox)}>
          <div className="w-100 d-flex justify-content-between">
            <span>Picker</span>
            <span className={classes.textBold}>
              {data?.picker_name || 'Tidak ada data'}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const AWBNumber = ({ data }) => {
    return (
      <div className="w-100 d-flex p-3 justify-content-between border ">
        <div className="">
          <span>
            Nomor Resi :{' '}
            <span className={classes.textBold}>
              {data?.awb_number || 'Tidak ada data'}
            </span>
          </span>
          <CopyToClipboard text={`${data?.awb_number}`}>
            <Button
              text={state.isCopiedAWB ? 'Resi Disalin' : 'Salin No.Resi'}
              styleType="blueOutline"
              style={{ marginLeft: '10px' }}
              onClick={() => setState({ ...state, isCopiedAWB: true })}
            />
          </CopyToClipboard>
        </div>
        <Button
          text="Riwayat Pengiriman"
          styleType="blueNoFill"
          endIcon={() => <ArrowDownwardIcon />}
        />
      </div>
    );
  };

  return (
    <div>
      <HeaderDetail data={detailTransactionReguler} tab={tab} />
      {tab.url === 'sent' ||
        (tab.url === 'done' && <AWBNumber data={detailTransactionReguler} />)}
      {tab.url !== 'waiting' && <Picker data={detailTransactionReguler} />}
      <SenderAndReceiver classes={classes} data={detailTransactionReguler} />
      {/* <div className="w-100 d-flex flex-row">
        <SenderForm classes={classes} data={detailTransactionReguler} />
        <ReceiverForm classes={classes} data={detailTransactionReguler} />
      </div> */}
      <DetailPacket classes={classes} data={detailTransactionReguler} />
      {tab.url === 'waiting' && (
        <TotalPayment data={detailTransactionReguler} />
      )}

      <ButtonByStatus tab={tab.url} />
      <ModalAddPicker
        show={state.modalAddPicker}
        onHide={() => setState({ ...state, modalAddPicker: false })}
        onSuccess={successAddPicker}
        id={detailTransactionReguler?.id}
      />
      <ModalAWB
        show={state.modalAWB}
        onHide={() => setState({ ...state, modalAWB: false })}
        onSuccess={successConfirmShipping}
        id={detailTransactionReguler?.id}
      />
      <ModalConfirm
        show={state.modalConfirm}
        onHide={() => setState({ ...state, modalConfirm: false })}
        onAgree={handleConfirm}
        title="Konfirmasi Diterima"
        description="Apakah anda yakin telah menerima transaksi reguler yang dikirim ke gudang?"
      />
      <ModalConfirm
        show={state.modalConfirmAtWarehouse}
        onHide={() => setState({ ...state, modalConfirmAtWarehouse: false })}
        onAgree={handleConfirmAtWarehouse}
        title="Konfirmasi Sampai di Gudang"
        description="Apakah anda yakin telah menerima transaksi reguler yang sampai di gudang?"
      />
    </div>
  );
};

export default DetailDispatchOrder;
