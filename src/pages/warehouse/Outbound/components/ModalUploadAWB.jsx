import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextField,
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { validateFile } from '../../../../utils/validate';

const validationSchema = Yup.object({
  awb: Yup.string().required('Harap untuk mengisi nomor AWB'),
  price: Yup.string().required('Harap untuk mengisi ongkos kirim'),
});

const allowedExtentions = ['image/png', 'image/jpg', 'image/jpeg'];

const ModalUploadAWB = (props) => {
  const [state, setState] = useState({ imageAWB: null });
  const formik = useFormik({
    initialValues: {
      awb: '',
      price: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  const awbRef = React.createRef();
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  // const { detailInbound } = useSelector((state) => state.inboundWarehouse);

  const handleSubmit = async (values) => {
    try {
      const payload = {
        status: 1,
        picker_name: values.name,
      };
      // const inboundId = detailInbound?.id;
      // await dispatch(confirmInbound(payload, inboundId));
      props.onHide();
    } catch (err) {}
  };

  const handleChangeImage = async (e) => {
    const files = e.target.files;
    const { file } = await validateFile({ files, type: 'image' });
    if (file) {
      setState({ ...state, imageAWB: file });
    }

    // formik.setFieldValue('invoice_file', e.target.files[0]);
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Upload AWB"
        show={props.show}
        onHide={props.onHide}
        scrollable
        content={(data) => (
          <>
            <TextField
              label="Nomor AWB"
              placeholder="Input nomor AWB"
              name="awb"
              withValidate
              formik={formik}
              className="mt-3"
            />
            <TextField
              label="Ongkos Kirim"
              placeholder="Input ongkos kirim"
              name="price"
              withValidate
              formik={formik}
              className="mt-3"
              prefix={() => 'Rp'}
            />
            <h6 className="input-label mt-3 mb-3">Foto AWB</h6>
            <div
              style={{
                height: '200px',
                borderRadius: '4px',
                border: '2px dashed #CFCFCF',
                backgroundColor: '#F2F2F2',
                overflow: 'hidden',
                position: 'relative',
              }}
              className="d-flex flex-column justify-content-center align-items-center "
            >
              {state.imageAWB ? (
                <>
                  <Button
                    style={{
                      position: 'absolute',
                      right: '10px',
                      top: '10px',
                    }}
                    styleType="lightBlueFillOutline"
                    text="Ganti Foto"
                    onClick={() => awbRef.current.click()}
                  />
                  <img
                    src={URL.createObjectURL(state.imageAWB)}
                    alt=""
                    style={{ height: '200px' }}
                  />
                </>
              ) : (
                <>
                  <img className="mt-2" src={pictureIcon} alt="" />

                  <Button
                    className="mt-2 mb-3"
                    styleType="blueOutline"
                    text="Upload Foto"
                    onClick={() => awbRef.current.click()}
                  />
                </>
              )}
              <input
                onChange={(e) => handleChangeImage(e)}
                ref={awbRef}
                type="file"
                className="d-none"
                accept={allowedExtentions.join(',')}
              />
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Return'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
              className="ml-2"
            />
          </>
        )}
      />
    </>
  );
};

export default ModalUploadAWB;
