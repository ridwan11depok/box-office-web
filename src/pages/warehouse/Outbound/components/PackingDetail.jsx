import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ReactSelect } from '../../../../components/elements/InputField';
import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import Avatar from '@material-ui/core/Avatar';
import Counter from '../../../../components/elements/Counter';
import ModalCancelPacking from './ModalCancelPacking';
import ModalConfirm from './ModalConfirm';
import { confirmShipping, getDispatchOrder } from '../reduxAction';
import { getListPackaging } from '../../Packaging/reduxAction';
import ModalAddPackaging from './ModalAddPackaging';
import { setToast } from '../../../../redux/actions';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

const HeaderTable = ({ handleBack, handleModalCancelPacking, data }) => {
  const classes = useStyles();
  return (
    <div className="d-flex flex-column w-100">
      <div
        className="d-flex justify-content-between align-items-center p-3 flex-wrap"
        style={{ borderBottom: '1px solid #E8E8E8' }}
      >
        <BackButton label={data?.invoice_number} onClick={handleBack} />
        <div className="d-flex flex-wrap">
          <Button
            style={{ minWidth: '120px', margin: '3px' }}
            text="Batalkan Packing"
            styleType="redOutline"
            onClick={handleModalCancelPacking}
          />
          <Button
            style={{ minWidth: '120px', margin: '3px 0px' }}
            text="Print Label"
            styleType="blueOutline"
          />
        </div>
      </div>
      <Row
        className="p-1 pt-2 pb-2"
        style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
      >
        <Col>
          <h6 className={classes.label}>Logistik</h6>
          <h6 className={classes.value}>{data?.vendor_service?.vendor?.name || 'Tidak ada data logistik'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Service</h6>
          <h6 className={classes.value}>{data?.vendor_service?.service || 'Tidak ada data service'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>AWB</h6>
          <h6 className={classes.value}>{data?.awb_number || 'Tidak ada data AWB'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Platform</h6>
          <h6 className={classes.value}>{data?.vendor_data?.platform || 'Tidak ada data Platform'}</h6>
        </Col>
      </Row>
      <Row
        className="p-1 pt-2 pb-2"
        style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
      >
        <Col>
          <h6 className={classes.label}>Store</h6>
          <h6 className={classes.value}>{data?.storage?.store?.store_name || 'Tidak ada data store'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Penerima</h6>
          <h6 className={classes.value}>{data?.receiver_name || 'Tidak ada data penerima'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Pesan</h6>
          <h6 className={classes.value}>{data?.message || 'Tidak ada pesan'}</h6>
        </Col>
      </Row>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};

const ActionField = ({ data, onClick }) => {
  return (
    <div
      onClick={() => onClick(data)}
      className="p-1 rounded button"
      style={{ border: '2px solid #192A55', width: 'min-content' }}
    >
      <i
        className="far fa-trash-alt"
        style={{ fontSize: '20px', color: '#192A55', cursor: 'pointer' }}
      ></i>
    </div>
  );
};

const Quantity = ({ data, handleCounter }) => {
  return (
    <span>
      <Counter
        onDecrease={() => handleCounter(data, '-')}
        onIncrease={() => handleCounter(data, '+')}
        value={data?.qty_will_accepted || 0}
      />
    </span>
  );
};

const ImageColumn = ({ data }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {data.length ?
        data?.map((item) => (
          <Avatar
            style={{ margin: '2px' }}
            alt={item?.name || 'Logo'}
            variant="rounded"
            src={item?.url}
          />
        ))
        :
        <span>Tidak ada gambar</span>
      }
    </div>
  );
};

const NilaiPackaging = ({ data }) => {
  let price = data?.default_price * data?.qty_will_accepted
  return (
    <span>Rp {price?.toLocaleString('id') || '-'}</span>
  )
}


function PackingDetail(props) {
  const [state, setState] = useState({
    search: '',
    storeId: null,
    vendorId: null,
    showModalCancelPacking: false,
    showModalConfirm: false,
    showModalAddPackaging: false,
    listPackaging: [],
    dataPackaging: []
  });
  const { id } = props
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useHistory()

  const { detailDispatchOrder } = useSelector(
    (state) => state.outboundWarehouse
  );

  const { listPackaging } = useSelector(
    (state) => state.packagingWarehouseReducer
  );

  useEffect(() => {
    dispatch(getListPackaging())
  }, [])

  useEffect(() => {
    if (id) {
      dispatch(getDispatchOrder({}, id))
    }
  }, [id])

  useEffect(() => {
    if (detailDispatchOrder && listPackaging.length) {
      const filter = listPackaging.filter((item, index) => {
        if (detailDispatchOrder?.packagings[index]?.packaging_id === item?.id && detailDispatchOrder?.packagings[index]?.quantity_user) {
          item.qty_will_accepted = detailDispatchOrder?.packagings[index]?.quantity_user
        }
        return detailDispatchOrder?.packagings[index]?.packaging_id === item?.id
      })
      setState({ ...state, dataPackaging: filter })
    }
  }, [detailDispatchOrder, listPackaging])

  useEffect(() => {
    if (listPackaging.length) {
      setState({ ...state, listPackaging: listPackaging })
    }
  }, [listPackaging])

  const handleBack = () => {
    props.changeTab(2)
  }

  const handleCounter = (data, operator) => {
    let tampArray = state.dataPackaging
    const map = tampArray.map((item) => {
      if (item.id === data.id) {
        if (operator === '+') {
          item.qty_will_accepted += 1
        } else {
          item.qty_will_accepted -= 1
        }
      }
      return item
    })

    setState({ ...state, dataPackaging: map })
  }

  const onAgreeAddPackging = (selected) => {
    setState({ ...state, dataPackaging: selected, showModalAddPackaging: false })
  }

  const handleDelete = (data) => {
    const filter = state.dataPackaging.filter((item) => {
      return item.id !== data.id
    })

    setState({ ...state, dataPackaging: filter })
  }

  const handleDonePacking = async () => {
    try {
      if (!state.dataPackaging.length) {
        dispatch(setToast({
          isShow: true,
          messages: 'Harap menambahkan packaging',
          type: 'error',
        })
        )
        setState({ ...state, showModalConfirm: false })
      }
      else{
        let data = new FormData()
        state.dataPackaging.forEach((item, index) => {
          data.append(`packaging[${index}][id]`, item.id)
          data.append(`packaging[${index}][quantity]`, item.qty_will_accepted)
        })

        await dispatch(confirmShipping(data, id))
        props.changeTab(3)
      }
    }
    catch (error) {
      console.log('err', error);
    }
  }

  return (
    <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'SKU',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },

          {
            heading: 'Gambar',
            render: (item) => <ImageColumn data={item?.product_storage?.product?.file_documents} />,
          },
          {
            heading: 'Qty Order',
            key: 'qty',
          },
        ]}
        data={detailDispatchOrder?.items?.length ? detailDispatchOrder?.items : []}
        transformData={(item) => ({
          ...item,
          SKU: item?.product_storage?.product?.sku || '-',
          name: item?.product_storage?.product?.name || '-',
          qty: item?.quantity || '-',
        })}
        renderHeader={() => (
          <HeaderTable
            handleBack={handleBack}
            handleModalCancelPacking={() =>
              setState({ ...state, showModalCancelPacking: true })
            }
            data={detailDispatchOrder}
          />
        )}
        withNumber={false}
        showFooter={false}
        showBorderContainer={false}
        renderEmptyData={() => <EmptyData />}
      />
      <Table
        column={[
          {
            heading: 'Nama Packaging',
            key: 'name',
          },
          {
            heading: 'Qty',
            render: (item) => <Quantity data={item} handleCounter={handleCounter} />,
          },

          {
            heading: 'Nilai Packaging',
            render: (item) => <NilaiPackaging data={item} />,
          },
          {
            heading: 'Aksi',
            render: (item) => <ActionField data={item} onClick={handleDelete} />,
          },
        ]}
        data={state.dataPackaging || []}
        showFooter={false}
        withNumber={false}
        renderHeader={() => (
          <div className="d-flex justify-content-between flex-wrap p-3 align-items-center w-100">
            <h6 className={classes.titleTable}>Tambah Packaging</h6>
            <Button
              text="Tambah Packaging"
              styleType="blueOutline"
              onClick={() => setState({ ...state, showModalAddPackaging: true })}
              startIcon={() => <AddIcon />}
            />
          </div>
        )}
      />
      <div className="d-flex justify-content-lg-end flex-wrap p-3">
        <Button
          text="Kembali"
          styleType="lightBlueFill"
          style={{ minWidth: '120px', marginRight: '5px' }}
          onClick={() => router.goBack()}
        />
        <Button
          text="Selesai"
          styleType="blueFill"
          style={{ minWidth: '120px' }}
          onClick={() => setState({ ...state, showModalConfirm: true })}
        />
      </div>
      <ModalCancelPacking
        show={state.showModalCancelPacking}
        onHide={() => setState({ ...state, showModalCancelPacking: false })}
        onSuccess={handleBack}
      />
      <ModalAddPackaging
        show={state.showModalAddPackaging}
        onHide={() => setState({ ...state, showModalAddPackaging: false })}
        onAgree={onAgreeAddPackging}
        initialValues={state.dataPackaging}
      />
      <ModalConfirm
        show={state.showModalConfirm}
        onHide={() => setState({ ...state, showModalConfirm: false })}
        onAgree={handleDonePacking}
        title="Selesai Packing"
        description="Apakah Anda yakin telah selesai melakukan packing produk?"
      />
    </div>
  );
}

export default PackingDetail;
