import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getDispatchOrder } from '../reduxAction';
import ModalReturn from './ModalReturn';

import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
  link: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#192A55',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
});

const HeaderTable = ({ data, handleBack, handleShowModalRetur }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div className="d-flex flex-column w-100">
      <div
        className="d-flex justify-content-between align-items-center p-3 flex-wrap"
        style={{ borderBottom: '1px solid #E8E8E8' }}
      >
        <BackButton label={data?.invoice_number} onClick={() => history.goBack()} />
        <Button
          style={{ minWidth: '120px', margin: '3px' }}
          text="Retur"
          styleType="redOutline"
          onClick={handleShowModalRetur}
        />
      </div>
      <Row
        className="p-1 pt-2 pb-2"
        style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
      >
        <Col>
          <h6 className={classes.label}>Logistik</h6>
          <h6 className={classes.value}>{data?.vendor_service?.vendor?.name || 'Tidak ada data logistik'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Service</h6>
          <h6 className={classes.value}>{data?.vendor_service?.service || 'Tidak ada data service'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>AWB</h6>
          <h6 className={classes.value}>{data?.awb_number || 'Tidak ada AWB'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Platform</h6>
          <h6 className={classes.value}>{data?.vendor_data?.platform || 'Tidak ada data platform'}</h6>
        </Col>
      </Row>
      <Row
        className="p-1 pt-2 pb-2"
        style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
      >
        <Col>
          <h6 className={classes.label}>Store</h6>
          <h6 className={classes.value}>{data?.storage?.store?.store_name || 'Tidak ada data store'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Penerima</h6>
          <h6 className={classes.value}>{data?.receiver_name || 'Tidak ada data penerima'}</h6>
        </Col>
        <Col>
          <h6 className={classes.label}>Pesan</h6>
          <h6 className={classes.value}>{data?.message || 'Tidak ada pesan'}</h6>
        </Col>
      </Row>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};


const ImageColumn = ({ data }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {
        data?.product_storage?.product?.file_documents?.length ?
          data?.product_storage?.product?.file_documents.map((item) => (
            <Avatar
              key={item?.id}
              style={{ margin: '2px' }}
              alt="Logo"
              variant="rounded"
              src={item?.url}
            />
          ))
          :
          <span>Tidak ada gambar</span>

      }
    </div>
  );
};


function DoneDetail(props) {
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    counting: false,
    date: '',
    showModalAWB: false,
    showModalReturn: false,
  });
  const classes = useStyles();
  const dispatch = useDispatch();

  const { id } = props

  useEffect(() => {
    if (id) {
      dispatch(getDispatchOrder({}, id))
    }
  }, [id])

  const { detailDispatchOrder } = useSelector(
    (state) => state.outboundWarehouse
  );


  const handleBack = () => {
    props.changeTab(6)
  }

  return (
    <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'SKU',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },

          {
            heading: 'Gambar',
            render: (item) => <ImageColumn data={item} />,
          },
          {
            heading: 'Qty Order',
            key: 'qty',
          },
        ]}
        data={detailDispatchOrder?.items || []}
        transformData={(item) => ({
          ...item,
          SKU: item?.product_storage?.product?.sku || '-',
          name: item?.product_storage?.product?.name || '-',
          qty: item?.quantity || '-',
        })}
        renderHeader={() => (
          <HeaderTable
            data={detailDispatchOrder}
            handleBack={handleBack}
            handleShowModalRetur={() => { setState({ ...state, showModalReturn: true }) }}
          />
        )}
        withNumber={false}
        showFooter={false}
        showBorderContainer={false}
        renderEmptyData={() => <EmptyData />}
      />
      <Table
        column={[
          {
            heading: 'Nama Packaging',
            key: 'name',
          },
          {
            heading: 'Qty',
            key: 'qty',
          },

          {
            heading: 'Nilai Packaging',
            key: 'value',
          },
        ]}
        data={detailDispatchOrder?.packagings || []}
        transformData={(item) => ({
          ...item,
          name: item?.packaging?.name || '-',
          qty: item?.quantity_user || '-',
          value: `Rp  ${item?.default_price?.toLocaleString('id')}` || '-'
        })}
        showFooter={false}
        withNumber={false}
        renderHeader={() => (
          <div className="d-flex justify-content-between flex-wrap p-3 align-items-center w-100">
            <h6 className={classes.titleTable}>Packaging</h6>
          </div>
        )}
      />
      <ModalReturn
        show={state.showModalReturn}
        onHide={() => {
          setState({ ...state, showModalReturn: false });
        }}
        onSuccess={() => props.changeTab(7)}
      />
    </div>
  );
}

export default DoneDetail;
