import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextField,
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { returnDispatchOrder } from '../reduxAction';
// import { confirmInbound } from '../reduxAction';

const validationSchema = Yup.object({
  reason: Yup.string().required('Harap untuk mengisi alasan return'),
});

const ModalReturn = (props) => {
  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailDispatchOrder } = useSelector((state) => state.outboundWarehouse);

  const handleSubmit = async (values) => {
    try {
      const id = detailDispatchOrder?.id;
      await dispatch(returnDispatchOrder(values, id));
      props.onHide();
      props.onSuccess()
    } catch (err) {}
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Return"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextArea
              label="Alasan Return"
              placeholder="Masukkan alasan return"
              name="reason"
              withValidate
              formik={formik}
              className="mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType={isLoading ? 'redFillDisabled' : 'redFill'}
              text={isLoading ? 'Loading...' : 'Return'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
              className="mr-2"
            />
            <Button
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalReturn;
