import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getAllInbound, confirmInbound } from '../reduxAction';
import {
  ReactSelect,
  SearchField,
  TextField,
} from '../../../../components/elements/InputField';
import ModalDetailInbound from './ModalDetailInbound';
import Badge from '../../../../components/elements/Badge';
import CountTable from './CountTable';
import moment from 'moment';
import localization from 'moment/locale/id';
import DatePicker from '../../../../components/elements/InputField/DatePicker';
import ModalPicker from './ModalPicker';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
});

const HeaderTable = ({ tab = 0 }) => {
  const classes = useStyles();
  const title = [
    'Picking List',
    'Packing List',
    'Shipping List',
    'Sent List',
    'Done List',
    'Reject List',
    'Return List',
    'Semua',
  ];
  return (
    <div className="d-flex flex-column w-100">
      <Row className="p-3">
        <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
          <ReactSelect
            label="Store"
            placeholder="Pilih store"
            options={[
              { value: 0, label: 'Semua Store' },
              { value: 1, label: 'Ecommerce' },
              { value: 2, label: 'Corporate' },
            ]}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
          <ReactSelect
            label="Logistik"
            placeholder="Pilih logistik"
            options={[
              { value: 0, label: 'Semua Logistik' },
              { value: 1, label: 'JNE' },
              { value: 2, label: 'J&T' },
            ]}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="d-flex align-items-end mb-2">
          <SearchField
            placeholder="Cari invoice, store, atau penerima"
            className="w-100"
          />
        </Col>
      </Row>
      {/* <div className="d-flex flex-row flex-wrap justify-content-between align-items-end p-2 pl-3 w-100">
        <div className="" style={{ width: '32%', minWidth: '120px' }}>
          <ReactSelect
            label="Store"
            placeholder="Pilih store"
            options={[
              { value: 0, label: 'Semua Store' },
              { value: 1, label: 'Ecommerce' },
              { value: 2, label: 'Corporate' },
            ]}
          />
        </div>
        <div style={{ width: '32%', minWidth: '120px' }}>
          <ReactSelect
            label="Logistik"
            placeholder="Pilih logistik"
            options={[
              { value: 0, label: 'Semua Logistik' },
              { value: 1, label: 'JNE' },
              { value: 2, label: 'J&T' },
            ]}
          />
        </div>
        <div
          className="d-flex flex-row align-items-end"
          style={{ width: '32%', minWidth: '120px' }}
        >
          <SearchField
            placeholder="Cari invoice, store, atau penerima"
            className="w-100"
          />
        </div>
      </div> */}
      <div
        className="d-flex flex-row justify-content-between align-items-center p-3"
        style={{
          borderTop: '1px solid #E8E8E8',
        }}
      >
        <h6 className={classes.titleTable}>{title[tab]}</h6>
        <div>
          {/* <SearchField placeholder="Cari invoice, store, atau penerima" /> */}
          {/* <TextField
            prefix={() => <i class="fa fa-barcode" aria-hidden="true"></i>}
            // onChange={handleFillBarcode}
            // value={valueBarcode}
            className="mr-2"
            placeholder="Barcode scan"
          />
          <Button
            text="Print Picklist"
            styleType={true ? 'blueFill' : 'blueFillDisabled'}
            // disabled={!handleCheckQuantityAccepted()}
            // onClick={() => setState({ ...state, showConfirmAccept: true })}
          /> */}
        </div>
      </div>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};

const ActionColumn = ({ data, tab, onClick, handleShowCountTable }) => {
  const dispatch = useDispatch();
  return tab === 2 ? (
    <Button
      text="Perhitungan Barang"
      styleType="blueFill"
      onClick={() => {
        dispatch(getAllInbound({}, data?.id));
        handleShowCountTable(true, data);
      }}
    />
  ) : (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => {
        dispatch(getAllInbound({}, data?.id));
        onClick();
      }}
    />
  );
};

const StatusColumn = ({ status = 1 }) => {
  return (
    <Badge
      label={status === 0 ? 'No' : 'Yes'}
      styleType={status === 0 ? 'red' : 'green'}
      style={{ fontSize: '14px' }}
    />
  );
};

function InboundTable(props) {
  const { tab } = props;
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    counting: false,
    date: '',
    showPicker: '',
  });
  const dispatch = useDispatch();
  const fetchListInbound = (params) => {
    dispatch(getAllInbound(params));
  };
  const { inbounds, detailInbound } = useSelector(
    (state) => state.inboundWarehouse
  );

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  const showCountTable = (isCount, selectedRow = {}) => {
    setState({ ...state, counting: isCount, selectedRow });
  };

  useEffect(() => {
    if (props.tab !== 2) {
      showCountTable(false);
    }
  }, [props.tab]);

  const handleFilterDate = (date) => {
    const dateSelect = moment(date)
      .locale('id', localization)
      .format('yyyy-MM-DD');
    // setDateSelected(dateSelect);
    setState({
      ...state,
      date: moment(date).locale('id', localization).format('yyyy-MM-DD'),
    });
  };

  const onAgreeModalDetail = async () => {
    if (
      (state.selectedRow?.is_request_pickup === 0 &&
        state.selectedRow?.status === 0) ||
      state.selectedRow?.status === 1
    ) {
      // Konfirmasi Sudah diterima, ke gudang/ status=2
      try {
        const payload = {
          status: 2,
        };
        const inboundId = detailInbound?.id;
        await dispatch(confirmInbound(payload, inboundId));
        setState({ ...state, showDetail: false });
      } catch (err) {}
    } else if (
      state.selectedRow?.is_request_pickup === 1 &&
      state.selectedRow?.status === 0
    ) {
      // Konfirmasi Pickup, ke status=1
      setState({ ...state, showDetail: false, showPicker: true });
    }
  };

  return (
    <div>
      {state.counting ? (
        <CountTable showCountTable={showCountTable} data={state.selectedRow} />
      ) : (
        <Table
          // action={fetchListInbound}
          // totalPage={inbounds?.last_page || 1}
          // params={{
          //   search: state.search,
          //   status: props.tab !== 4 ? props.tab : null,
          //   date: state.date ? state.date : null,
          // }}
          // variableTriggerAction={[props.tab]}
          column={[
            {
              heading: 'Tanggal',
              key: 'date',
            },
            {
              heading: 'Invoice',
              key: 'invoice',
            },

            {
              heading: 'Store',
              key: 'store',
            },
            {
              heading: 'Penerima',
              key: 'penerima',
            },
            {
              heading: 'Logistik',
              key: 'log',
            },
            {
              heading: 'Aksi',
              key: 'act',
            },
            // {
            //   heading: 'Request Pickup',
            //   render: (data) => (
            //     <StatusColumn status={data.is_request_pickup} />
            //   ),
            // },
            // {
            //   heading: 'Aksi',
            //   render: (data) => (
            //     <ActionColumn
            //       data={data}
            //       onClick={() =>
            //         setState({ ...state, selectedRow: data, showDetail: true })
            //       }
            //       tab={props.tab}
            //       handleShowCountTable={showCountTable}
            //     />
            //   ),
            // },
          ]}
          data={[]}
          // transformData={(item) => ({
          //   ...item,
          //   updated_at: moment(item?.updated_at)
          //     .locale('id', localization)
          //     .format('DD MMMM yyyy HH:mm:ss'),
          // })}
          renderHeader={() => (
            <HeaderTable
              handleSearch={handleSearch}
              handleFilterDate={handleFilterDate}
              tab={tab}
            />
          )}
          withNumber={false}
          renderEmptyData={() => <EmptyData />}
        />
      )}

      <ModalDetailInbound
        show={state.showDetail}
        onHide={() => setState({ ...state, showDetail: false })}
        data={state.selectedRow}
        onAgree={onAgreeModalDetail}
      />
      <ModalPicker
        show={state.showPicker}
        onHide={() => setState({ ...state, showPicker: false })}
        data={state.selectedRow}
      />
    </div>
  );
}

export default InboundTable;
