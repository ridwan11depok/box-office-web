import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
    ReactSelect,
    SearchField,
    TextField,
} from '../../../../components/elements/InputField';

import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import Avatar from '@material-ui/core/Avatar';
import Counter from '../../../../components/elements/Counter';
import ModalRejectOrder from './ModalRejectOrder';
import ModalConfirm from './ModalConfirm';
import { getDispatchOrder, confirmPicking } from '../reduxAction'
import { setToast } from '../../../../redux/actions';

const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    label: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
        color: '#4f4f4f',
    },
    value: {
        fontSize: '14px',
        fontWeight: '600',
        fontFamily: 'Open Sans',
        color: '#1c1c1c',
    },
});

const HeaderTable = ({ handleModalReject, data}) => {
    const router = useHistory()
    const classes = useStyles();
    return (
        <div className="d-flex flex-column w-100">
            <div
                className="d-flex justify-content-between align-items-center p-3 flex-wrap"
                style={{ borderBottom: '1px solid #E8E8E8' }}
            >
                <BackButton label={data?.invoice_number} onClick={() => router.goBack()} />
                <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Tolak Order"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />
            </div>
            <Row
                className="p-1 pt-2 pb-2"
                style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
            >
                <Col>
                    <h6 className={classes.label}>Logistik</h6>
                    <h6 className={classes.value}>{data?.vendor_service?.vendor?.name || 'Tidak ada data logistik'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Service</h6>
                    <h6 className={classes.value}>{data?.vendor_service?.service || 'Tidak ada data service'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>AWB</h6>
                    <h6 className={classes.value}>{data?.awb_number || 'Tidak ada AWB'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Platform</h6>
                    <h6 className={classes.value}>{data?.vendor_data?.platform || 'Tidak ada data platform'}</h6>
                </Col>
            </Row>
            <Row
                className="p-1 pt-2 pb-2"
                style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
            >
                <Col>
                    <h6 className={classes.label}>Store</h6>
                    <h6 className={classes.value}>{data?.storage?.store?.store_name || 'Tidak ada data store'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Penerima</h6>
                    <h6 className={classes.value}>{data?.receiver_name || 'Tidak ada data penerima'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Pesan</h6>
                    <h6 className={classes.value}>{data?.message || 'Tidak ada pesan'}</h6>
                </Col>
            </Row>
        </div>
    );
};

const HeaderTablePackaging = ({ data }) => {

    return (
        <div className="w-100 h-100">
            <div
                className="d-flex justify-content-between align-items-center p-3 flex-wrap"
                style={{ borderBottom: '1px solid #E8E8E8' }}
            >
                <span style={{ color: '#1c1c1c', fontSize: '20px', fontFamily: 'Work Sans', fontWeight: 'bold' }}>Packaging</span>
            </div>
        </div>
    );
};

const EmptyData = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada proses outbound yang masuk saat ini.
            </h6>
        </div>
    );
};

const EmptyDataPackaging = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada data packaging
            </h6>
        </div>
    );
};

const ImageColumn = ({ data }) => {
    return (
        <div className="d-flex flex-row flex-wrap">
            {
                data?.product_storage?.product?.file_documents?.length ?
                    data?.product_storage?.product?.file_documents.map((item) => (
                        <Avatar
                            key={item?.id}
                            style={{ margin: '2px' }}
                            alt="Logo"
                            variant="rounded"
                            src={item?.url}
                        />
                    ))
                    :
                    <span>Tidak ada gambar</span>

            }
        </div>
    );
};

const PackagingValue = ({ data }) => {
    return (
        <span>{`Rp ${data?.default_price?.toLocaleString('id')}` || '-'}</span>
    )
}

function WaitingDetail(props) {
    const { id } = props
    const router = useHistory()
    const [state, setState] = useState({
        search: '',
        storeId: null,
        vendorId: null,
        date: '',
        showModalReject: false,
        showModalConfirm: false,
    });

    const { detailDispatchOrder } = useSelector(
        (state) => state.outboundWarehouse
    );

    const dispatch = useDispatch();

    useEffect(() => {
        if (id) {
            dispatch(getDispatchOrder({}, id))
        }
    }, [id])

    useEffect(() => {
        if (detailDispatchOrder?.items?.length) {
            const tamp = detailDispatchOrder?.items
            const initValues = tamp.map((item) => {
                item.qty_will_accepted = 0
                return item
            })
            setState({ ...state, dataProduct: initValues })
        }
    }, [detailDispatchOrder])


    const handleConfirmPicking = async () => {
        try {
            const response = await dispatch(confirmPicking({}, id))
            setState({ ...state, showModalConfirm: false })
            props.changeTab(1)
        }
        catch (err) {
            console.log('err', err);
            setState({ ...state, showModalConfirm: false })
        }
    }

    return (
        <>
            <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
                <Table
                    column={[
                        {
                            heading: 'SKU',
                            key: 'SKU',
                        },
                        {
                            heading: 'Nama Produk',
                            key: 'name',
                        },

                        {
                            heading: 'Gambar',
                            render: (item) => <ImageColumn data={item} />,
                        },
                        {
                            heading: 'Qty Order',
                            key: 'qty',
                        },
                    ]}
                    data={state.dataProduct}
                    transformData={(item) => ({
                        ...item,
                        SKU: item?.product_storage?.product?.sku || '-',
                        name: item?.product_storage?.product?.name || '-',
                        qty: item?.quantity || '-',
                    })}
                    renderHeader={() => (
                        <HeaderTable
                            data={detailDispatchOrder}
                            handleModalReject={() =>
                                setState({ ...state, showModalReject: true })
                            }
                        />
                    )}
                    withNumber={false}
                    showFooter={false}
                    showBorderContainer={false}
                    renderEmptyData={() => <EmptyData />}
                />
            </div>
            <div style={{ border: '1px solid #E8E8E8' }} className="rounded mt-3">
                <Table
                    column={[
                        {
                            heading: 'Nama Packaging',
                            key: 'name',
                        },
                        {
                            heading: 'Qty',
                            key: 'qty',
                        },
                        {
                            heading: 'Nilai Packaging',
                            render: (item) => <PackagingValue data={item} />,
                        },
                    ]}
                    data={detailDispatchOrder?.packagings || []}
                    transformData={(item) => ({
                        ...item,
                        name: item?.packaging?.name || '-',
                        qty: item?.quantity_user || '-',
                    })}
                    renderHeader={() => (
                        <HeaderTablePackaging
                            data={detailDispatchOrder}
                        />
                    )}
                    withNumber={false}
                    showFooter={false}
                    showBorderContainer={false}
                    renderEmptyData={() => <EmptyDataPackaging />}
                />
            </div>
            <div className="d-flex justify-content-lg-end flex-wrap p-3">
                <Button
                    text="Kembali"
                    styleType="lightBlueFill"
                    style={{ minWidth: '120px', marginRight: '5px' }}
                    onClick={() => props.handleShowDetail(false)}
                />
                <Button
                    text="Selesai"
                    styleType="blueFill"
                    style={{ minWidth: '120px' }}
                    onClick={() => setState({ ...state, showModalConfirm: true })}
                />
            </div>
            <ModalRejectOrder
                show={state.showModalReject}
                onHide={() => setState({ ...state, showModalReject: false })}
            />
            <ModalConfirm
                show={state.showModalConfirm}
                onHide={() => setState({ ...state, showModalConfirm: false })}
                onAgree={handleConfirmPicking}
                title="Mulai Order"
                description="Apakah anda yakin akan memproses order dan mulai untuk memindahkan ke proses selanjutnya?"
            />
        </>
    );
}

export default WaitingDetail;
