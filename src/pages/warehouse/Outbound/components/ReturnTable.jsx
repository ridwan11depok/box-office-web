import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getListStore, confirmInbound, getDispatchOrder } from '../reduxAction';
import { getListVendor } from '../../../customer/Outbound/reduxAction';
import {
  ReactSelect,
  SearchField,
  TextField,
} from '../../../../components/elements/InputField';
import ModalDetailInbound from './ModalDetailInbound';
import Badge from '../../../../components/elements/Badge';
import CountTable from './CountTable';
import moment from 'moment';
import localization from 'moment/locale/id';
import ReturnDetail from './ReturnDetail';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
});

const HeaderTable = ({
  handleSearch,
  handleSelectVendor,
  handleSelectStore,
}) => {
  const classes = useStyles();
  const { listVendor } = useSelector((state) => state.outboundCustomer);
  const { listStore } = useSelector((state) => state.outboundWarehouse);
  return (
    <div className="d-flex flex-column w-100">
      <Row className="p-3">
        <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
          <ReactSelect
            label="Store"
            placeholder="Pilih store"
            options={listStore}
            onChange={handleSelectStore}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
          <ReactSelect
            label="Logistik"
            placeholder="Pilih logistik"
            options={listVendor}
            onChange={handleSelectVendor}
          />
        </Col>
        <Col xs={12} md={6} lg={4} className="d-flex align-items-end mb-2">
          <SearchField
            placeholder="Cari invoice, store, atau penerima"
            className="w-100"
            onChange={handleSearch}
          />
        </Col>
      </Row>
      <div
        className="d-flex flex-row justify-content-between align-items-center p-3"
        style={{
          borderTop: '1px solid #E8E8E8',
        }}
      >
        <h6 className={classes.titleTable}>Return List</h6>
        <div></div>
      </div>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};

const ActionColumn = ({ data, onClick }) => {
  return (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      style={{ minWidth: '130px' }}
      onClick={onClick}
    />
  );
};

const StatusColumn = ({ status = 1 }) => {
  return (
    <Badge
      label={status === 0 ? 'No' : 'Yes'}
      styleType={status === 0 ? 'red' : 'green'}
      style={{ fontSize: '14px' }}
    />
  );
};

function ReturnTable(props) {
  const { tab } = props;
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    counting: false,
    date: '',
    showPicker: '',
  });

  const router = useHistory()
  const dispatch = useDispatch();

  const fetchListDispatchOrder = (params) => {
    dispatch(getDispatchOrder(params));
  };

  const { dispatchOrder } = useSelector(
    (state) => state.outboundWarehouse
  );

  const handleSearch = (e) => {
    setState({ ...state, search: e.target.value });
  };

  const handleSelectVendor = (e) => {
    setState({ ...state, vendorId: e.target.value });
  };

  const handleSelectStore = (e) => {
    setState({ ...state, storeId: e.target.value });
  };

  const handleShowDetail = (isShow, selectedRow = {}) => {
    setState({ ...state, showDetail: isShow, selectedRow });
  };

  useEffect(() => {
    dispatch(getListVendor());
    dispatch(getListStore());
  }, [props.tab]);

  return (
    <div>
        <Table
          action={fetchListDispatchOrder}
          totalPage={dispatchOrder?.last_page || 1}
          params={{
            search: state.search,
            status: tab.tab,
            store_id: state.storeId,
            vendor_id: state.vendorId,
          }}
          variableTriggerAction={[props.tab]}
          column={[
            {
              heading: 'Tanggal',
              key: 'date',
            },
            {
              heading: 'Invoice',
              key: 'invoice',
            },

            {
              heading: 'Store',
              key: 'store',
            },
            {
              heading: 'Penerima',
              key: 'receiver',
            },
            {
              heading: 'Logistik',
              key: 'vendor',
            },
            {
              heading: 'Aksi',
              render: (item) => (
                <ActionColumn
                  data={item}
                  onClick={() => {
                    router.push(`/warehouse/outbound/${tab.url}/${item.id}`)
                  }}
                />
              ),
            },
          ]}
          data={dispatchOrder?.data || []}
          transformData={(item) => ({
            ...item,
            date: moment(item?.created_at)
              .locale('id', localization)
              .format('DD MMMM yyyy HH:mm:ss'),
            invoice: item?.invoice_number || '-',
            store: item?.store_id || '-',
            receiver: item?.receiver_name || '-',
            vendor: item?.vendor_service?.vendor?.name || '-',
          })}
          renderHeader={() => (
            <HeaderTable
              handleSearch={handleSearch}
              handleSelectStore={handleSelectStore}
              handleSelectVendor={handleSelectVendor}
            />
          )}
          withNumber={false}
          renderEmptyData={() => <EmptyData />}
        />
    </div>
  );
}

export default ReturnTable;
