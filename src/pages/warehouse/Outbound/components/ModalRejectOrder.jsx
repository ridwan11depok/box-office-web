import React, { useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { rejectOrder } from '../reduxAction';
import { useHistory } from 'react-router';

const validationSchema = Yup.object({
  reason: Yup.string().required('Harap untuk mengisi alasan menolak'),
});

const ModalRejectOrder = (props) => {
  const router = useHistory()
  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailDispatchOrder } = useSelector(
    (state) => state.outboundWarehouse
  );

  const handleSubmit = async (values) => {
    try {
      const dispatchOrderId = detailDispatchOrder?.id;
      await dispatch(rejectOrder(values, dispatchOrderId))
      props.onHide();
      router.goBack()
      
    } catch (err) {
    }
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Tolak Order"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextArea
              label="Alasan Menolak"
              placeholder="Masukkan alasan menolak"
              name="reason"
              withValidate
              formik={formik}
              className="mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType={isLoading ? 'redFillDisabled' : 'redFill'}
              text={isLoading ? 'Loading...' : 'Tolak Order'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
              className="mr-2"
            />
            <Button
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalRejectOrder;
