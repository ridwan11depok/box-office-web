import React, { useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import Barcode from './Barcode';

const ModalBarcode = (props) => {
  const [event, setEvent] = useState('');

  const handleTriggerPrint = () => {
    setEvent('print');
    props.onHide();
    setTimeout(() => {
      setEvent('');
    }, 1000);
  };
  return (
    <>
      <Modal
        titleModal="Barcode"
        bodyClassName="p-2"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={() => (
          <div className="d-flex justify-content-center w-100">
            <Barcode
              event={event}
              listProduct={props.listProduct}
              detailInbound={props.detailInbound}
            />
          </div>
        )}
        scrollable
        footer={() => (
          <>
            <Button
              styleType="blueFill"
              text="Print"
              onClick={handleTriggerPrint}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalBarcode;
