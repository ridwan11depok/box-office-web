import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';

const dummyAWB =
  'https://s3-alpha-sig.figma.com/img/8630/010f/50eda9982250b3daeebe204a9f787838?Expires=1638144000&Signature=PF-ogSPSyrjPL01GwVtfWGDrWL~oXIjDBNDVtekBQSIComWutapRTQl-Kv-QUCNGJvDBW8MYn3Kmy7zQo2LDZssukkDoVNwlKhxeLYdlvBmKnC60mTorPgKU969YojmXgR0QnLqEx9qZs7VEVpMRKrkf9WEiGS5Pn6P2cB8a161YPp01sHhlx4YLHSMk-n8DmLj~7syEWY6COKySjnYaBe6RtDh7k46T0qkzlzqIcXyTxnVTXmNmNzaGc4c7j72AXGfMeqlrALP9pcxT7ZUO91uBdzwToyzOtkz1I-qsB0KpqaazJ8252m6hvojEGh2Ahudoyk8DstJN7MZg6qvzqA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA';
const ModalShowAWB = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal="Foto AWB"
        bodyClassName="p-1"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <div className="text-left">
            <img alt="Foto AWB" src={dummyAWB} className="w-100" />
          </div>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              text="Kembali"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

ModalShowAWB.defaultProps = {
  title: 'Konfirmasi',
  description: 'Apakah yakin?',
};
export default ModalShowAWB;
