import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import axios from 'axios';
import baseUrl from '../../../api/url';

export const getAllInbound =
  (params = {}, inboundId) =>
  async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result;
      dispatch(setLoading(true));
      if (inboundId) {
        result = await consume.get(
          'inboundWarehouse',
          {},
          { token },
          inboundId
        );
        dispatch(setDetailInbound(result.result));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(true));
        result = await consume.getWithParams(
          'inboundWarehouse',
          {},
          { token },
          params
        );
        dispatch(setInbound(result.result));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      if (err.code === 422) {
        // console.log('errorr', err)
      }
      if (inboundId) {
        dispatch(setDetailInbound({}));
        dispatch(setLoading(false));
      } else {
        dispatch(setInbound({}));
        dispatch(setLoadingTable(false));
      }
      // return Promise.reject(err);
    }
  };

function setInbound(data) {
  return {
    type: actionType.INBOUND_WAREHOUSE,
    payload: data,
  };
}
function setDetailInbound(data) {
  return {
    type: actionType.DETAIL_INBOUND_WAREHOUSE,
    payload: data,
  };
}

export const confirmInbound =
  (payload = { status: null }, inboundId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'inboundWarehouseConfirmation',
        data,
        headers,
        inboundId
      );
      let prevStatus;
      if (data.status === 1) {
        prevStatus = 0;
      } else if (data.status === 2) {
        prevStatus = 1;
      } else if (data.status === 3) {
        prevStatus = 2;
      }
      dispatch(getAllInbound({ page: 1, per_page: 10, status: prevStatus }));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmPicking =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/picking`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const rejectOrder =
  (payload = {}, inboundId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${inboundId}/reject`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const cancelPacking =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/cancel`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response?.message || 'Berhasil membatalkan packing.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal membatalkan packing.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const unpack =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/unpack`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response?.message || 'Berhasil Unpack.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal Unpack.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const returnDispatchOrder =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/return`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response?.message || 'Berhasil return.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal membatalkan packing.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmPacking =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/packing`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmShipping =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/shipping`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmManifest =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/manifest`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const getDispatchOrder =
  (params = {}, transactionId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    // const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (transactionId) {
        result = await consume.getWithParams(
          'dispatchOrderAdminWarehouse',
          {},
          { token },
          {},
          transactionId
        );
        dispatch(setDetailDispatchOrder(result.result));
      } else {
        result = await consume.getWithParams(
          'dispatchOrderAdminWarehouse',
          {},
          { token },
          params
        );
        dispatch(setDispatchOrder(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      // console.log('err', err);
      if (transactionId) {
        dispatch(setDetailDispatchOrder({}));
      } else {
        dispatch(setDispatchOrder({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setDispatchOrder(data) {
  return {
    type: actionType.DISPATCH_ORDER_ADMIN_WAREHOUSE,
    payload: data,
  };
}

function setDetailDispatchOrder(data) {
  return {
    type: actionType.DETAIL_DISPATCH_ORDER_ADMIN_WAREHOUSE,
    payload: data,
  };
}

export const getListStore = () => async (dispatch, getState) => {
  const { token } = getState().login;
  try {
    let result = await consume.get('listStoreAdminWarehouse', {}, { token });
    dispatch(setListStore(result.result));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStore([]));
  }
};

function setListStore(data) {
  return {
    type: actionType.LIST_STORE_ADMIN_WAREHOUSE,
    payload: data,
  };
}

export const getTransactionReguler =
  (params = {}, id) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (id) {
        result = await consume.getWithParams(
          'transactionRegulerAdminWarehouse',
          {},
          { token },
          {},
          id
        );
        // console.log('resultId', result);
        dispatch(setDetailTransactionReguler(result.result));
      } else {
        result = await consume.getWithParams(
          'transactionRegulerAdminWarehouse',
          {},
          { token },
          params
        );
        dispatch(setTransactionReguler(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      if (id) {
        dispatch(setDetailTransactionReguler({}));
      } else {
        dispatch(setTransactionReguler({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setTransactionReguler(data) {
  return {
    type: actionType.TRANSACTION_REGULER,
    payload: data,
  };
}

function setDetailTransactionReguler(data) {
  return {
    type: actionType.DETAIL_TRANSACTION_REGULER,
    payload: data,
  };
}

export const confirmPickupTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/pickup`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmAtWarehouseTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/at_warehouse`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmShippingTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/shipping`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmManifestTransactionReguler =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'transactionRegulerAdminWarehouse',
        data,
        headers,
        null,
        `${id}/manifest`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

// export const confirmManifestTransactionReguler =
//   (payload = {}, id) =>
//   async (dispatch, getState) => {
//     try {
//       dispatch(setLoading(true));
//       const { token } = getState().login;
//       let data = payload;
//       let headers = { token: token };
//       let response = await consume.post(
//         'transactionRegulerAdminWarehouse',
//         data,
//         headers,
//         null,
//         `${id}/manifest`
//       );
//       dispatch(
//         setToast({
//           isShow: true,
//           messages: 'Berhasil melakukan konfirmasi.',
//           type: 'success',
//         })
//       );
//       dispatch(setLoading(false));
//       return Promise.resolve('success');
//     } catch (err) {
//       dispatch(
//         setToast({
//           isShow: true,
//           messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
//           type: 'error',
//         })
//       );
//       dispatch(setLoading(false));
//       return Promise.reject(err);
//     }
//   };

export const generateAWBRegularTransaction =
  (payload) => async (dispatch, getState) => {
    try {
      const { token, dataUser } = getState().login;
      const storeId = dataUser?.store[0]?.id;
      const config = {
        url: `${baseUrl.transactionRegulerAdminWarehouse}${payload.id}/generate-awb`,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil generate AWB.',
          type: 'success',
        })
      );
      return Promise.resolve(res);
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal generate AWB.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

export const getCashless =
  (params = {}, transactionId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    // const storeId = dataUser?.store?.[0]?.id;
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (transactionId) {
        result = await consume.getWithParams(
          'cashlessAdminWarehouse',
          {},
          { token },
          {},
          transactionId
        );
        dispatch(setDetailCashless(result.result));
      } else {
        result = await consume.getWithParams(
          'cashlessAdminWarehouse',
          {},
          { token },
          params
        );
        dispatch(setCashless(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      // console.log('err', err);
      if (transactionId) {
        dispatch(setDetailCashless({}));
      } else {
        dispatch(setCashless({}));
      }
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setCashless(data) {
  return {
    type: actionType.CASHLESS_ADMIN_WAREHOUSE,
    payload: data,
  };
}

function setDetailCashless(data) {
  return {
    type: actionType.DETAIL_CASHLESS_ADMIN_WAREHOUSE,
    payload: data,
  };
}
