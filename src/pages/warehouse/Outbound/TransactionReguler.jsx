import React, { useEffect, useState } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import { TabHeader, TabContent } from '../../../components/elements/Tabs';
import { useHistory, useLocation, useParams } from 'react-router';
import Table from './componentsTransactionReguler/Table';
import Detail from './componentsTransactionReguler/Detail';

const useStyles = makeStyles({});
const status = {
  pending: 0,
  picking: 1,
  packing: 2,
  shipping: 3,
  manifest: 4,
  sent: 5,
  done: 6,
  return: 7,
  cancel: 8,
  reject: 9,
};

const listHeader = [
  {
    tab: 0,
    title: 'Waiting',
    url: 'waiting',
    id: 0,
  },
  {
    tab: 1,
    title: 'Pickup',
    url: 'pickup',
    id: 1,
  },
  {
    tab: 2,
    title: 'Sampai di Gudang',
    url: 'at_warehouse',
    id: 2,
  },
  {
    tab: 3,
    title: 'Shipping',
    url: 'shipping',
    id: 3,
  },
  {
    tab: 5,
    title: 'Sent',
    url: 'sent',
    id: 5,
  },
  {
    tab: 6,
    title: 'Done',
    url: 'done',
    id: 6,
  },
  // {
  //     tab: 7,
  //     title: 'Semua',
  //     url: 'semua',
  //     id: 7
  // },
];

const DispatchOrder = (props) => {
  const { process } = useParams();
  const location = useLocation();
  const router = useHistory();
  const classes = useStyles();
  const [state, setState] = useState({
    tab: status[process] || 0,
    splitURL: [],
    id: 0,
  });
  const changeTab = (tab) => {
    setState({ ...state, tab });
    router.push('/warehouse/outbound/regular-transaction');
  };

  useEffect(() => {
    setState({ ...state, tab: status[process] });
  }, [process]);

  //0:pending,
  // 1:picking,
  // 2:packing,
  // 3:shipping,
  // 4:manifest,
  // 5:sent;
  // 6 done;
  // 7 return,
  // 8 cancel,
  // 9 reject

  useEffect(() => {
    const split = location.pathname.split('/');
    setState({ ...state, splitURL: split });
  }, [location?.pathname]);

  return (
    <ContentContainer title="Transaction Reguler">
      <ContentItem className="mb-3">
        <TabHeader
          border={false}
          activeTab={state.tab}
          listHeader={listHeader}
          onChange={changeTab}
          inActiveColor="#192a55"
        />
      </ContentItem>
      <ContentItem border={false}>
        {state.splitURL.length === 6 ? (
          <Detail
            changeTab={changeTab}
            id={state.splitURL[5]}
            tab={listHeader[state.tab]}
          />
        ) : (
          <Table tab={listHeader.find((item) => item.tab === state.tab)} />
        )}
      </ContentItem>
    </ContentContainer>
  );
};

export default DispatchOrder;
