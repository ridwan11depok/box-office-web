import React, { useEffect, useState } from "react";
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from "../../../components/elements/BaseContent";
import { makeStyles } from "@material-ui/core/styles";
import DispatchOrderTable from "./components/DispatchOrderTable";
import { TabHeader, TabContent } from "../../../components/elements/Tabs";
import PickingTable from "./components/PickingTable";
import PackingTable from "./components/PackingTable";
import ShippingTable from "./components/ShippingTable";
import SentTable from "./components/SentTable";
import DoneTable from "./components/DoneTable";
import RejectTable from "./components/RejectTable";
import ReturnTable from "./components/ReturnTable";
import AllStatusTable from "./components/AllStatusTable";
import WaitingList from "./components/WaitingList";
import { useHistory, useLocation, useParams } from "react-router";
import WaitingDetail from "./components/WaitingDetail";
import { Router } from "@material-ui/icons";
import PickingDetail from "./components/PickingDetail";
import PackingDetail from "./components/PackingDetail";
import ShippingDetail from "./components/ShippingDetail";
import SentDetail from "./components/SentDetail";
import DoneDetail from "./components/DoneDetail";
import ReturnDetail from "./components/ReturnDetail";
import RejectDetail from "./components/RejectDetail";

const useStyles = makeStyles({});
const status = {
  pending: 0,
  picking: 1,
  packing: 2,
  shipping: 3,
  manifest: 4,
  sent: 5,
  done: 6,
  return: 7,
  cancel: 8,
  reject: 9,
};

const listHeader = [
  {
    tab: 0,
    title: "Waiting",
    url: "waiting",
  },
  {
    tab: 1,
    title: "Picking",
    url: "picking",
  },
  {
    tab: 2,
    title: "Packing",
    url: "packing",
  },
  {
    tab: 3,
    title: "Shipping",
    url: "shipping",
  },
  {
    tab: 5,
    title: "Sent",
    url: "sent",
  },
  {
    tab: 6,
    title: "Done",
    url: "done",
  },
  {
    tab: 7,
    title: "Return",
    url: "return",
  },
  {
    tab: 9,
    title: "Reject",
    url: "reject",
  },
];

const tabContents = [
  {
    tab: 0,
    Table: WaitingList,
    Detail: WaitingDetail,
  },
  {
    tab: 1,
    Table: PickingTable,
    Detail: PickingDetail,
  },
  {
    tab: 2,
    Table: PackingTable,
    Detail: PackingDetail,
  },
  {
    tab: 3,
    Table: ShippingTable,
    Detail: ShippingDetail,
  },
  {
    tab: 5,
    Table: SentTable,
    Detail: SentDetail,
  },
  {
    tab: 6,
    Table: DoneTable,
    Detail: DoneDetail,
  },
  {
    tab: 7,
    Table: ReturnTable,
    Detail: ReturnDetail,
  },
  {
    tab: 9,
    Table: RejectTable,
    Detail: RejectDetail,
  },
];

const DispatchOrder = (props) => {
  let { process } = useParams();
  const location = useLocation();
  const router = useHistory();
  const classes = useStyles();
  const [state, setState] = useState({
    tab: status[process] || 0,
    isDetail: false,
    splitURL: [],
  });
  const changeTab = (tab) => {
    setState({ ...state, tab });
    router.push("/warehouse/outbound/dispatch-order");
  };

  useEffect(() => {
    setState({ ...state, tab: status[process] });
  }, [process]);

  useEffect(() => {
    const split = location.pathname.split("/");
    setState({ ...state, splitURL: split });
  }, [location?.pathname]);

  const Content = tabContents.find((item) => {
    return item.tab === state.tab
  });

  return (
    <ContentContainer title="Dispatch Order">
      <ContentItem className="mb-3">
        <TabHeader
          border={false}
          activeTab={state.tab}
          listHeader={listHeader}
          onChange={changeTab}
          inActiveColor="#192a55"
        />
      </ContentItem>
      <ContentItem border={false}>
        {process ? (
          <TabContent tab={Content.tab} activeTab={state.tab}>
            <Content.Detail changeTab={changeTab} id={state.splitURL[4]} />
          </TabContent>
        ) : (
          <TabContent tab={Content.tab} activeTab={state.tab}>
            <Content.Table tab={listHeader.find((item)=>item.tab === state.tab)} />
          </TabContent>
        )}

        {/* <TabContent tab={0} activeTab={state.tab}>
          {state.splitURL.length === 5 ?
            <WaitingDetail changeTab={changeTab} id={state.splitURL[4]} />
            :
            <WaitingList tab={listHeader[state.tab]} />
          }
        </TabContent>
        <TabContent tab={1} activeTab={state.tab}>
          {state.splitURL.length === 5 ?
            <>
              <PickingDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
            <PickingTable tab={listHeader[state.tab]} />
          }
        </TabContent>
        <TabContent tab={2} activeTab={state.tab}>
          {state.splitURL.length === 5 ?
            <>
              <PackingDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
            <PackingTable tab={listHeader[state.tab]} />
          }
        </TabContent>
        <TabContent tab={3} activeTab={state.tab}>
          {state.splitURL.length === 5 ?
            <>
              <ShippingDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
            <ShippingTable tab={listHeader[state.tab]} />
          }
        </TabContent>
        <TabContent tab={5} activeTab={state.tab}>
          {state.splitURL.length === 5 ?
            <>
              <SentDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
            <SentTable tab={listHeader.find((item) => item.tab === 5)} />
          }
        </TabContent>
        <TabContent tab={6} activeTab={state.tab}>
          {process === "done" ?
            <>
              <DoneDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
            <DoneTable tab={listHeader.find((item) => item.tab === 6)} />
          }
        </TabContent>
        <TabContent tab={7} activeTab={state.tab}>
        {state.splitURL.length === 5 ?
            <>
              <ReturnDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
          <ReturnTable tab={listHeader.find((item) => item.tab === 7)} />
        }
        </TabContent>
        <TabContent tab={9} activeTab={state.tab}>
        {state.splitURL.length === 5 ?
            <>
              <RejectDetail changeTab={changeTab} id={state.splitURL[4]} />
            </>
            :
          <RejectTable tab={listHeader.find((item) => item.tab === 9)} />
        }
        </TabContent> */}
      </ContentItem>
    </ContentContainer>
  );
};

export default DispatchOrder;
