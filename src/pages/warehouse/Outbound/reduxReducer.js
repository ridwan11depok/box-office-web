import actionType from './reduxConstant';

const initialState = {
  inbounds: {},
  detailInbound: {},
  dispatchOrder: {},
  detailDispatchOrder: {},
  listStore: [],
  transactionReguler: {},
  detailTransactionReguler: {},
  cashless: {},
  detailCashless: {},
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.INBOUND_WAREHOUSE:
      return {
        ...prevState,
        inbounds: action.payload,
      };
    case actionType.DETAIL_INBOUND_WAREHOUSE:
      return {
        ...prevState,
        detailInbound: action.payload,
      };
    case actionType.DISPATCH_ORDER_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        dispatchOrder: action.payload,
      };
    case actionType.DETAIL_DISPATCH_ORDER_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        detailDispatchOrder: action.payload,
      };
    case actionType.LIST_STORE_ADMIN_WAREHOUSE: {
      const listStore = action.payload.map((item) => ({
        ...item,
        value: item.store_id,
        label: item.store.store_name,
      }));
      return {
        ...prevState,
        listStore,
      };
    }
    case actionType.TRANSACTION_REGULER:
      return {
        ...prevState,
        transactionReguler: action.payload,
      };
    case actionType.DETAIL_TRANSACTION_REGULER:
      return {
        ...prevState,
        detailTransactionReguler: action.payload,
      };
    case actionType.CASHLESS_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        cashless: action.payload,
      };
    case actionType.DETAIL_CASHLESS_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        detailCashless: action.payload,
      };
    default:
      return prevState;
  }
};

export default inboundReducer;
