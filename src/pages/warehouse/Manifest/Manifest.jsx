import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import ManifestTable from './components/ManifestTable';

const useStyles = makeStyles({});

const DispatchOrder = (props) => {
  const classes = useStyles();
  return (
    <ContentContainer title="Manifest">
      <ContentItem border={false}>
        <ManifestTable />
      </ContentItem>
    </ContentContainer>
  );
};

export default DispatchOrder;
