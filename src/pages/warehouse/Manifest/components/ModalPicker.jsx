import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { TextField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { processManifest } from '../reduxAction';

const validationSchema = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama kurir'),
});

const ModalPicker = (props) => {
  const formik = useFormik({
    initialValues: {
      name: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailManifest } = useSelector((state) => state.manifestWarehouse);

  const handleSubmit = async (values) => {
    try {
      const payload = {
        picker_name: values.name,
      };
      
      await dispatch(processManifest(payload, detailManifest.id));
      props.onHide();
    } catch (err) {}
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Kurir"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextField
              label="Nama Kurir"
              placeholder="Input nama kurir"
              name="name"
              withValidate
              formik={formik}
              className="mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              className="mr-2"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Simpan'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalPicker;
