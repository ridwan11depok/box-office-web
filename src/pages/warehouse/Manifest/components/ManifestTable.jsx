import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  DatePicker,
  SearchField,
} from '../../../../components/elements/InputField';
import clsx from 'clsx';
import { getAllManifest } from '../reduxAction';
import moment from 'moment';
import localization from 'moment/locale/id';

const useStyles = makeStyles({
  rootStatus: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  pendingBorder: {
    textAlign: 'center',
    background: '#E8E8E8',
    borderRadius: '16px',
    width: '64px',
    height: '28px',
  },
  pendingText: {
    color: '#363636',
  },
  doneBorder: {
    textAlign: 'center',
    background: '#EDF7EE',
    borderRadius: '16px',
    width: '64px',
    height: '28px',
  },
  doneText: {
    color: '#1B5E20',
  }
});

const HeaderTable = ({ handleSearch, handleFilter }) => {
  const history = useHistory();
  const classes = useStyles();

  let optionsMonth = [];

  for (var i = 0; i < 12; i++) {
    optionsMonth.push({
      value: moment().format('YYYY') + '-' + ('0' + (i + 1)).slice(-2),
      label: moment().month(i).locale('id', localization).format('MMMM yyyy')
    })
  }

  return (
    <div className="d-flex flex-row flex-wrap align-items-center p-2 pl-3 w-100">

      <div
        className=""
        style={{ width: '20%', minWidth: '120px' }}
      >
        <DatePicker
          format="MMMM YYYY"
          views={['year', 'month']}
          noMarginBottom
          onDateChange={(date) => handleFilter('date', date)}
          placeholder="Pilih bulan"
        />
      </div>

      <div
        className="ml-2"
        style={{ width: '30%', minWidth: '120px' }}
      >
        <SearchField placeholder="Cari kode manifest, logistik" onChange={(e) =>
          setTimeout(() => {
            handleSearch(e.target.value);
          }, 500)
        } />
      </div>
      <Button
        styleType="blueOutline"
        text="Dispatch Order - Shipping"
        className="mt-1 mr-2 mb-1 ml-2"
        onClick={() => {
          history.push(`/warehouse/outbound/dispatch-order`);
        }}
      />

      <Button
        styleType="blueOutline"
        text="Transaksi Reguler - Shipping"
        className="mt-1 mr-1 mb-1"
        onClick={() => {
          history.push(`/warehouse/outbound/regular-transaction`);
        }}
      />

    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada proses manifest saat ini.</h6>
    </div>
  );
};

const ActionField = ({ data }) => {
  const history = useHistory();
  if (data?.status === 1) {
    return (
      <>
        <Button
          text="Detail"
          styleType="lightBlueFill"
          onClick={() => {
            history.push(`/warehouse/manifest/${data?.id}`, {
              manifestId: data.id,
            });
          }}
        />
      </>
    );
  } else {
    return (
      <Button
        text="Serahkan ke kurir"
        styleType="blueFill"
        onClick={() => {
          history.push(`/warehouse/manifest/${data?.id}`, {
            manifestId: data.id,
          });
        }}
      />
    )
  }
};

const StatusColumn = ({ status = 0 }) => {
  const classes = useStyles();
  return (
    <div className={clsx(classes.rootStatus, {
      [classes.pendingBorder]: status === 0,
      [classes.doneBorder]: status === 1
    })}>
      <p
        className={clsx(classes.rootStatus, {
          [classes.pendingText]: status === 0,
          [classes.doneText]: status === 1
        })}
      >
        {status === 0 ? 'Pending' : 'Selesai'}
      </p>
    </div>
  );
};

function ManifestTable(props) {
  const [state, setState] = useState({
    search: '',
    date: 'all',
  });

  const { manifest } = useSelector((state) => state.manifestWarehouse);
  const dispatch = useDispatch();

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const fetchListManifest = (params) => {
    dispatch(getAllManifest(params));
  };

  const handleFilter = (type, value) => {
    if (type === 'date') {
      if (value) {
        setState({ ...state, date: moment(value).format('YYYY-MM') });
      } else {
        setState({ ...state, date: 'all' });
      }
    }
  };

  return (
    <div>
      <Table
        action={fetchListManifest}
        totalPage={manifest?.last_page || 1}
        params={{
          search: state.search,
          date: state.date
        }}
        column={[
          {
            heading: 'Kode Manifest',
            key: 'code',
          },
          {
            heading: 'Logistik',
            render: (data) => <div>{data.vendor ? data.vendor.name : '-'}</div>
          },
          {
            heading: 'Tanggal',
            key: 'date',
          },
          {
            heading: 'Status',
            render: (data) => <StatusColumn status={data?.status || 0} />,
          },
          {
            heading: 'Picker',
            render: (data) =>
              <div>{data.picker_name ? data.picker_name : '-'}</div>
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={manifest?.data || []}
        transformData={(item) => ({
          ...item,
          date: moment(item?.created_at)
            .locale('id', localization)
            .format('DD MMMM yyyy HH:mm:ss'),
        })}
        renderHeader={() => (
          <HeaderTable
            handleSearch={handleSearch}
            handleFilter={handleFilter}
          />
        )}
        withNumber={false}
        renderEmptyData={() => <EmptyData />}
      />
    </div>
  );
}

export default ManifestTable;
