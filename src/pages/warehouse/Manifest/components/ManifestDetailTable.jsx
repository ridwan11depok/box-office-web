import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table } from '../../../../components/elements/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { getItemManifest } from '../reduxAction';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data.</h6>
    </div>
  );
};

function ManifestDetailTable(props) {
  const dispatch = useDispatch();
  const { itemManifest, detailManifest } = useSelector((state) => state.manifestWarehouse);
  const history = useHistory();
  const [state, setState] = useState({
    modalDetailSku: false,
    detailSku: {}
  });
  
  const fetchListItem = (params) => {
    dispatch(getItemManifest(params, history.location.state?.manifestId));
  };

  return (
    <div>
      <Table
        action={fetchListItem}
        totalPage={itemManifest?.last_page || 1}
        params={{
          search: state.search,
          status: state.status ? state.status : null,
          type: state.type ? state.type : null,
        }}
        withNumber={true}
        column={[
          {
            heading: 'Service',
            key: 'service',
          },
          {
            heading: 'AWB',
            key: 'awb_number',
          },
          {
            heading: 'Penerima',
            key: 'receiver_name',
          },
          {
            heading: 'Store',
            key: 'store',
          },
          {
            heading: 'Telepon Penerima',
            key: 'receiver_phone_number',
          },
          {
            heading: 'Alamat Penerima',
            key: 'receiver_address',
          },
        ]}
        showFooter={false}
        data={itemManifest || []}
        transformData={(item) => ({
          ...item,
          service: item?.service_name || '-',
          awb_number: item?.awb_number || '-',
          receiver_name: item?.receiver_name || '-',
          store: item?.store || '-',
          receiver_phone_number: item?.receiver_phone || '-',
          receiver_address: item?.receiver_address || '-',
        })}
        renderEmptyData={() => <EmptyData />}
      />
    </div>
  );
}

export default ManifestDetailTable;
