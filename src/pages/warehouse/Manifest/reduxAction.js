import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setErrorData, setLoading, setLoadingTable, setToast } from '../../../redux/actions';

export const getAllManifest =
  (params = {}, manifestId) =>
  async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result;
      if (manifestId) {
        dispatch(setLoading(true));
        result = await consume.get('manifestWarehouse', {}, { token }, manifestId);
        dispatch(setDetailManifest(result.result));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(true));
        result = await consume.getWithParams(
          'manifestWarehouse',
          {},
          { token },
          params
        );
        dispatch(setManifest(result.result));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      if (manifestId) {
        dispatch(setErrorData(err));
        dispatch(setDetailManifest({}));
        dispatch(setLoading(false));
      } else {
        dispatch(setManifest({}));
        dispatch(setLoadingTable(false));
      }
    }
  };

export const getItemManifest =
  (params = {}, manifestId) =>
  async (dispatch, getState) => {
    const {
      token
    } = getState().login;
    try {
      let result;
      dispatch(setLoading(true));
      result = await consume.get('manifestWarehouse', {}, {
        token
      }, manifestId+'/items');
      dispatch(setItemManifest(result.result));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setItemManifest([]));
      dispatch(setLoading(false));
    }
  };

function setManifest(data) {
  return {
    type: actionType.LIST_MANIFEST_WAREHOUSE,
    payload: data,
  };
}
function setDetailManifest(data) {
  return {
    type: actionType.DETAIL_MANIFEST_WAREHOUSE,
    payload: data,
  };
}

function setItemManifest(data) {
  return {
    type: actionType.LIST_ITEM_MANIFEST_WAREHOUSE,
    payload: data,
  };
}

export const processManifest =
  (payload = {}, manifestId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'manifestWarehouse',
        data,
        headers,
        manifestId+'/process'
      );
      
      dispatch(getAllManifest({}, manifestId));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Manifest berhasil di selesaikan.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal menyelesaikan manifest.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };
