import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import BackButton from '../../../components/elements/BackButton';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import ManifestDetailTable from './components/ManifestDetailTable';
import Button from '../../../components/elements/Button';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import DoneIcon from '@material-ui/icons/Done';
import { makeStyles } from '@material-ui/core/styles';
import ModalPicker from './components/ModalPicker';
import { getAllManifest } from './reduxAction';
import moment from 'moment';
import localization from 'moment/locale/id';

const useStyles = makeStyles({
  title: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 400,
    lineHeight: '20px',
    letterSpacing: '0em',
    textAlign: 'left',
    color: '#4F4F4F',
  },
  subtitle: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 600,
    lineHeight: '20px',
    letterSpacing: '0em',
    textAlign: 'left',

  }
});

const validationSchema = Yup.object({});

function ManifestDetail() {
  const history = useHistory();
  const params = useParams();
  const classes = useStyles();
  const [state, setState] = useState({
    showPicker: '',
  });

  //for edit
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailManifest } = useSelector((state) => state.manifestWarehouse);
  
  useEffect(() => {
    if (params?.id) {
      const manifestId = params?.id;
      dispatch(getAllManifest({}, manifestId));
    }
  }, [params?.id]);

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label={detailManifest?.code || 'Manifest Detail'}
            onClick={() => history.push('/warehouse/manifest')}
          />

          {detailManifest?.status == 0 ? <Button
            startIcon={() => <DoneIcon />}
            styleType="blueFill"
            text="Selesai"
            className="mt-1 mr-1 mb-1"
            onClick={() => {
              setState({ ...state, showPicker: true });
            }}
          /> : ''}
        </div>
      )}
    >

      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-12"
        spaceBottom={3}
      >
          <div style={{ borderBottom: '1px solid #E8E8E8' }}>

            <div className="row align-items-center">

              <div className="col-lg-10 border-right">

                <div className="row p-3">

                  <div className="col-lg-3">
                    <p className={classes.title}>Nomor Manifest</p>
                    <h6 className={classes.subtitle}>{detailManifest?.code}</h6>
                  </div>

                  <div className="col-lg-3">
                    <p className={classes.title}>Tanggal</p>
                    <h6 className={classes.subtitle}>
                      {moment(detailManifest?.date)
                      .locale('id', localization)
                      .format('DD MMMM yyyy')}
                    </h6>
                  </div>

                  <div className="col-lg-3">
                    <p className={classes.title}>Logistik</p>
                    <h6 className={classes.subtitle}>{detailManifest?.vendor?.name}</h6>
                  </div>

                  <div className="col-lg-3">
                    <p className={classes.title}>Kurir</p>
                    <h6 className={classes.subtitle}>{detailManifest?.picker_name || '-'}</h6>
                  </div>

                </div>

              </div>

              <div className="col-lg-2 text-center">
                <Button
                  styleType="blueOutline"
                  text="Print Manifest"
                  className="mt-1 mr-1 mb-1"
                  onClick={() => {
                    history.push(`/warehouse/outbound/regular-transaction`);
                  }}
                />
              </div>
            
          </div>
        </div>
        
      </ContentItem>

      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-12"
        spaceBottom={3}
      >
        <ManifestDetailTable detailManifest={detailManifest}/>
      </ContentItem>

      <ModalPicker
        show={state.showPicker}
        onHide={() => setState({ ...state, showPicker: false })}
      />
    </ContentContainer>
  );
}

export default ManifestDetail;
