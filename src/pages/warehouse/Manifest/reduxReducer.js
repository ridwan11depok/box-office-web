import actionType from './reduxConstant';

const initialState = {
  manifest: {},
  detailManifest: {},
  itemManifest: []
};

const manifestWarehouseReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_MANIFEST_WAREHOUSE:
      return {
        ...prevState,
        manifest: action.payload,
      };
    case actionType.DETAIL_MANIFEST_WAREHOUSE:
      return {
        ...prevState,
        detailManifest: action.payload,
      };
    case actionType.LIST_ITEM_MANIFEST_WAREHOUSE:
      return {
        ...prevState,
        itemManifest: action.payload,
      };
    default:
      return prevState;
  }
};

export default manifestWarehouseReducer;
