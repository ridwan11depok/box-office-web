import React from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import TableInvoice from './components/TableInvoice';

const Invoice = () => {
    return(
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
            <PageTitle title="Invoice" />
          </div>
        )}
      >
      <ContentItem border={false} className="mb-3">
        <TableInvoice/>
      </ContentItem>

      </ContentContainer>
    )
}

export default Invoice;