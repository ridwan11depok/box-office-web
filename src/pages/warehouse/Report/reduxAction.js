import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import axios from 'axios';
import baseUrl from '../../../api/url';

export const  getAllReportTransaction =
  (params = {}, inboundId) =>
    async (dispatch, getState) => {
      const { token } = getState().login;
      try {
        let result;
        dispatch(setLoading(true));
        if (inboundId) {
          result = await consume.get(
            'listReportTransaction',
            {},
            { token },
            inboundId
          );
          dispatch(setDetailInbound(result.result));
          dispatch(setLoading(false));
        } else {
          dispatch(setLoadingTable(true));
          result = await consume.getWithParams(
            'listReportTransaction',
            {},
            { token },
            params
          );
          dispatch(setInbound(result.result));
          dispatch(setLoadingTable(false));
        }
      } catch (err) {
        if (err.code === 422) {
          // console.log('errorr', err)
        }
        if (inboundId) {
          dispatch(setDetailInbound({}));
          dispatch(setLoading(false));
        } else {
          dispatch(setInbound({}));
          dispatch(setLoadingTable(false));
        }
        // return Promise.reject(err);
      }
    };

function setInbound(data) {
  return {
    type: actionType.REPORT_INBOUND,
    payload: data,
  };
}
function setDetailInbound(data) {
  return {
    type: actionType.DETAIL_INBOUND_WAREHOUSE,
    payload: data,
  };
}

export const getAllReportInvoice =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      if (id) {
        dispatch(setLoading(true));
      } else {
        dispatch(setLoadingTable(true));
      }
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('listReportInvoice', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'listReportInvoice',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailInvoice(response.result));
        } else {
          dispatch(setInvoice(response.result));
        }
        if (id) {
          dispatch(setLoading(false));
        } else {
          dispatch(setLoadingTable(false));
        }
      }
      // console.log('respon=>', response.result);
    } catch (err) {
      if (id) {
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(false));
      }
    }
  };

const setInvoice = (data) => {
  return {
    type: actionType.REPORT_INVOICE,
    payload: data,
  };
};

export const detailInvoice = (data) => {
  return {
    type: actionType.DETAIL_REPORT_INVOICE,
    payload: data,
  };
};  

export const exportLogsTransaction =
(payload) => async (dispatch, getState) => {
  try {   
    const { token, dataUser } = getState().login;
    // console.log('data==>', storeId)    
    const storeId = dataUser?.store[0]?.id;
    const config = {
      url: baseUrl.exportLogsTransaction,
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        store_id: storeId,
        ...payload,
      },
      responseType: 'blob',
    };
    const res = await axios.request(config);

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const exportLogsInvoice =
(payload) => async (dispatch, getState) => {
  try {   
    const { token, dataUser } = getState().login;
    // console.log('data==>', storeId)    
    const storeId = dataUser?.store[0]?.id;
    const config = {
      url: baseUrl.exportLogsInvoice,
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        store_id: storeId,
        ...payload,
      },
      responseType: 'blob',
    };
    const res = await axios.request(config);

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};



export const getListStore = () => async (dispatch, getState) => {
  const { token, dataUser } = getState().login;
  const storeId = dataUser?.store?.[0]?.id;
  dispatch(setLoading(true));
  try {
    const result = await consume.getWithParams(
      'listStoreAdminWarehouse',
      {},
      { token },
      { store_id: storeId }
    );
    dispatch(setListStorage(result.result));
    dispatch(setLoading(false));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStorage([]));
    dispatch(setLoading(false));
    return Promise.reject(err);
  }
};

function setListStorage(data) {
  return {
    type: actionType.LIST_STORE_ADMIN_WAREHOUSE,
    payload: data,
  };
}