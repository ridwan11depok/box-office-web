import actionType from './reduxConstant';

const initialState = {
  inbounds: {},
  invoice: {},
  detailInvoice: {},
  detailInbound: {},
  dispatchOrder: {},
  detailDispatchOrder: {},
  listStore: [],
  transactionReguler: {},
  detailTransactionReguler: {},
};

const inboundReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.REPORT_INBOUND:
      return {
        ...prevState,
        inbounds: action.payload,
      };
    case actionType.REPORT_INVOICE:
      return {
        ...prevState,
        invoice: action.payload,
      };
    case actionType.DETAIL_INBOUND_WAREHOUSE:
      return {
        ...prevState,
        detailInbound: action.payload,
      };
    case actionType.DETAIL_REPORT_INVOICE:
      return {
        ...prevState,
        detailInvoice: action.payload,
      };
    case actionType.DISPATCH_ORDER_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        dispatchOrder: action.payload,
      };
    case actionType.DETAIL_DISPATCH_ORDER_ADMIN_WAREHOUSE:
      return {
        ...prevState,
        detailDispatchOrder: action.payload,
      };
    case actionType.LIST_STORE_ADMIN_WAREHOUSE: {
      const listStore = action.payload.map((item) => ({
        ...item,
        value: item.store_id,
        label: item.store.store_name,
      }));
      return {
        ...prevState,
        listStore: [{ value: 'all', label: 'Semua Store' }, ...listStore],
      };
    }
    case actionType.TRANSACTION_REGULER:
      return {
        ...prevState,
        transactionReguler: action.payload,
      };
      case actionType.DETAIL_TRANSACTION_REGULER:
      return {
        ...prevState,
        detailTransactionReguler: action.payload,
      };
    default:
      return prevState;
  }
};

export default inboundReducer;
