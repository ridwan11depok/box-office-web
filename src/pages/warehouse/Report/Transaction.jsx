import React, { useEffect, useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { TabHeader, TabContent } from '../../../components/elements/Tabs';
import { useHistory, useLocation } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import TableInbound from './components/TableInbound';
import TableDispatchOrder from './components/TableDispatchOrder';
import TableTransactionReguler from './components/TableTransactionReguler';

const useStyles = makeStyles({});


const Transaction = (props) => {
  const location = useLocation()
  const router = useHistory()
  const classes = useStyles();
  const [state, setState] = useState({
      tab: 0,
      splitURL: [],
      id: 0
  });
  const changeTab = (tab) => {
      setState({ ...state, tab });
  };

  const listHeader = [
      {
          tab: 0,
          title: 'Inbound',
          url: 'inbound',
          id: 0
      },
      {
          tab: 1,
          title: 'Outbound - Dispatch Order',
          url: 'dispatch_order',
          id: 1
      },
      {
          tab: 2,
          title: 'Outbound - Transaksi Reguler',
          url: 'transaction_reguler',
          id: 2
      },
  ]

  useEffect(() => {
      const split = location.pathname.split('/')
      setState({ ...state, splitURL: split })
  }, [location?.pathname])


    return(
      <ContentContainer title="Transaksi">
          <ContentItem className="mb-3">
              <TabHeader
                  border={false}
                  activeTab={state.tab}
                  listHeader={listHeader}
                  onChange={changeTab}
                  inActiveColor="#192a55"
              />
          </ContentItem>
          <ContentItem border={false}>           
            <TabContent tab={0} activeTab={state.tab}>
            {state.splitURL.length === 3 ?
                <div>Kosong</div>
                :
                <TableInbound tab={listHeader[state.tab]} />
            }
            </TabContent> 
            <TabContent tab={1} activeTab={state.tab}>
            {state.splitURL.length === 3 ?
                <div>Kosong</div>
                :
                <TableDispatchOrder tab={listHeader[state.tab]} />
            }
            </TabContent>
            <TabContent tab={2} activeTab={state.tab}>
            {state.splitURL.length === 3 ?
                <div>Kosong</div>
                :
                <TableTransactionReguler tab={listHeader[state.tab]} />
            }
            </TabContent>
          </ContentItem>
      </ContentContainer>
    )
}

export default Transaction;