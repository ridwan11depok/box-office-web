import React, { useState, useEffect, useCallback } from 'react';
import { Table } from '../../../../components/elements/Table';
import RangeDate from './RangeDate';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { getAllReportTransaction } from '../reduxAction';
import moment from 'moment';
import localization from 'moment/locale/id';
import { Row, Col } from 'react-bootstrap';

const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    textDetail: {
        color: '#182953',
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        cursor:'pointer'
    }
});

const HeaderTable = ({
    handleDate,
}) => {
    const classes = useStyles();
    return (
        <div className="d-flex flex-column w-100">
        <Row className="pl-3 pr-3 pb-1 pt-1">
          <Col xs={12} md={6} lg={6} className="mb-lg-2 mb-lg-0 mb-2">
              <div className="mt-3">
                  <h4>Invoice Dispatch Order</h4>
              </div>
          </Col>
          <Col
            xs={12}
            md={6}
            lg={6}
            className="d-flex align-items-end justify-content-end mb-2"
          >
            <Button text="Print Invoice" styleType="blueOutline" />
          </Col>
        </Row>
        </div>
    );
};

const EmptyData = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada proses Transaction saat ini.
            </h6>
        </div>
    );
};

const FooterTable = ({ inbounds }) => {
    const classes = useStyles();
    const sum = useCallback(
      (key) => {
        return inbounds?.data?.map(item=>item[key] || 0).reduce((prev, current)=>prev+current).toLocaleString('id')
      },
      [inbounds],
    )
  
    return (
      <div className="d-flex flex-column w-100">
        <Row className="pl-3 pr-3 pb-1 pt-1">
          <Col xs={12} md={8} lg={8} className="mb-lg-2 mb-lg-0 mb-2">
              <div className="mt-3 d-flex align-items-end justify-content-end">
                  <h6 className="d-flex">Total Pembayaran <span className="ml-3">{sum('total_sku')}</span></h6>
              </div>
          </Col>
          <Col
            xs={12}
            md={4}
            lg={4}
            className="d-flex align-items-end justify-content-end mb-2"
          >
          <h6>Rp. {sum('total_price')}</h6>
          </Col>
        </Row>
      </div>
    );
  };

const TotalTransaction = ({ data }) => {
    let price = data?.total_price
    return (
      <span>Rp {price?.toLocaleString('id') || '-'}</span>
    )
}

// const ActionField = ({ data, onClick }) => {
//     const classes = useStyles()
//     const history = useHistory();
//     const [state, setState] = useState({
//       tab: 6,
//     });
//     const openTab = (tab) => {
//       setState({ ...state, tab });
//       history.push('/warehouse/outbound/dispatch-order')
//     };
//     return (
//       <>
//         <span 
//             onClick={openTab}
//             className={classes.textDetail}
//         >Detail</span>
//       </>
//     );
//   };

  
const ActionColumn = ({ data, onClick }) => {
    const classes = useStyles()
    return (
        <span onClick={onClick} className={classes.textDetail}>Detail</span>
    );
};


function TableDispatchOrderTab(props) {
    const { tab } = props;
    const router = useHistory()
    const [state, setState] = useState({
        showDetail: false,
        selectedRow: {},
        search: '',
        storeId: null,
        vendorId: null,
        datestart: '',
        dateend: '',
        showPicker: '',
    });

    const dispatch = useDispatch();

    const fetchAllReport = (params) => {
        dispatch(getAllReportTransaction(params));
    };

    const { inbounds } = useSelector(
        (state) => state.report
    );

    const handleDate = (e) => {
        setState({ ...state, datestart: moment(e.value[0]).format('YYYY-MM-DD'), dateend: moment(e.value[1]).format('YYYY-MM-DD') });
    };
    const handleSelectVendor = (e) => {
        setState({ ...state, vendorId: e.target.value });
    };

    const handleSelectStore = (e) => {
        setState({ ...state, storeId: e.target.value });
    };

    return (
        <div>
            <Table
                action={fetchAllReport}
                totalPage={inbounds?.last_page || 1}
                params={{
                    "date[start]": state.datestart ? state.datestart : null,
                    "date[end]": state.dateend ? state.dateend : null,
                    "type": "dispatch_order",
                }}
                column={[
                    {
                    heading: 'Tanggal',
                    key: 'date',
                    },
                    {
                    heading: 'Nomor Invoice',
                    key: 'invoice_number',
                    },
                    {
                    heading: 'Store',
                    key: 'store',
                    },
                    {
                    heading: 'Total SKU',
                    key: 'total_sku',
                    },
                    {
                    heading: 'Logistik',
                    key: 'vendor',
                    },
                    {
                    heading: 'Service',
                    key: 'service',
                    },
                    {
                    heading: 'Total Transaksi',
                    render: (item) => <TotalTransaction data={item} />,
                    },
                    {
                    heading: 'Aksi',
                        render: (item) => (
                            <ActionColumn
                                data={item}
                                onClick={() => {
                                    router.push(`/warehouse/outbound/done/${item.id}`)
                                }}
                            />
                        ),
                    },
                ]}
                data={inbounds?.data || []}
                transformData={(item) => ({
                  ...item,
                  date: moment(item?.created_at)
                    .locale('id', localization)
                    .format('DD MMMM yyyy HH:mm:ss'),
                    store: item?.store.store_name || '-',
                    vendor: item?.vendor_service?.vendor?.name || '-',
                    service: item?.vendor_service?.type || '-',
                })}
                renderHeader={() => (
                    <HeaderTable
                        handleDate={handleDate}
                        handleSelectStore={handleSelectStore}
                        handleSelectVendor={handleSelectVendor}
                    />
                )}
                showFooter={false}
                renderFooter={() => <FooterTable inbounds={inbounds}/>}
                withNumber={false}
                renderEmptyData={() => <EmptyData />}
            />
        </div>
    );
}

export default TableDispatchOrderTab;