import React from "react";
import { DateRangePickerComponent } from "@syncfusion/ej2-react-calendars";
import './material.css';

const RangeDate = (props) => {
    const { change } = props
    return(
        <DateRangePickerComponent
            format= "dd MMM yyyy"
            change= {change}
        >
        
        </DateRangePickerComponent>
    )
}

export default RangeDate;