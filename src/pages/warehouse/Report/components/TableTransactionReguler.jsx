import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import RangeDate from './RangeDate';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { getAllReportTransaction, exportLogsTransaction, getListStore } from '../reduxAction';
import {
    ReactSelect,
} from '../../../../components/elements/InputField';
import moment from 'moment';
import localization from 'moment/locale/id';
import { Row, Col } from 'react-bootstrap';
import { useDownloadFile } from "../../../../utils/hook";

const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    textDetail: {
        color: '#182953',
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        cursor:'pointer'
    }
});

const HeaderTable = ({
    handleDate,
    handleExportLogs,
    handleFilter
}) => {
    const classes = useStyles();
    const { listStore } = useSelector((state) => state.report);

    const getFileName = () => {
      return `Transaksi Reguler.xlsx`;
    };
  
    const { ref, url, download, name } = useDownloadFile({
      apiDefinition: handleExportLogs,
      getFileName,
    });

    return (
        <div className="d-flex flex-column w-100">
            <Row className="p-3">
                <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
                    <div className="mb-1">Tanggal</div>
                    <RangeDate change= {handleDate}/>
                </Col>
                <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2">
                    <ReactSelect
                        label="Store"
                        placeholder="Semua Store"
                        options={listStore}
                        onChange={(e) => handleFilter('store_id', e.target.value)}
                    />
                </Col>
                <Col xs={12} md={6} lg={4} className="d-flex align-items-end justify-content-end mb-2">
                    <a href={url} download={name} className="hidden" ref={ref} />
                    <Button
                        text="Export Logs"
                        styleType="blueOutline"
                        onClick={download}
                    />
                </Col>
            </Row>
        </div>
    );
};

const EmptyData = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada proses Transaction saat ini.
            </h6>
        </div>
    );
};

const TotalTransaction = ({ data }) => {
    let price = data?.total_price
    return (
      <span>Rp {price?.toLocaleString('id') || '-'}</span>
    )
}

const ActionColumn = ({ data, onClick }) => {
    const classes = useStyles()
    return (
        <span onClick={onClick} className={classes.textDetail}>Detail</span>
    );
};


function TableTransactionReguler(props) {
    const { tab } = props;
    const router = useHistory()
    const [state, setState] = useState({
        showDetail: false,
        selectedRow: {},
        search: '',
        storeId: null,
        vendorId: null,
        datestart: '',
        dateend: '',
        showPicker: '',
        store_id: "all",
    });
    const dispatch = useDispatch();

    const fetchAllReport = (params) => {
        dispatch(getAllReportTransaction(params));
    };

    useEffect(() => {
      dispatch(getListStore());
    }, []);

    const { inbounds } = useSelector(
        (state) => state.report
    );

    const handleDate = (e) => {
        setState({ ...state, datestart: moment(e.value[0]).format('YYYY-MM-DD'), dateend: moment(e.value[1]).format('YYYY-MM-DD') });
    };

    const handleFilter = (type, value) => {
      if (type === 'store_id') {
        setState({ ...state, store_id: value });
      }
    };

    const handleExportLogs = () => {
      const params = {
        "date[start]": state.datestart ? state.datestart : null,
        "date[end]": state.dateend ? state.dateend : null,
        type: "transaction_reguler",
      };
      return dispatch(exportLogsTransaction(params));
    };

    return (
        <div>
            <Table
                action={fetchAllReport}
                totalPage={inbounds?.last_page || 1}
                params={{
                    "date[start]": state.datestart ? state.datestart : null,
                    "date[end]": state.dateend ? state.dateend : null,
                    type: "transaction_reguler",
                    store_id: state.store_id !== 'all' ? state.store_id : null,
                }}
                column={[
                    {
                    heading: 'Tanggal',
                    key: 'date',
                    },
                    {
                    heading: 'Nomor Transaksi',
                    key: 'transaction_number',
                    },
                    {
                    heading: 'Store',
                    key: 'store',
                    },
                    {
                    heading: 'Logistik',
                    key: 'vendor',
                    },
                    {
                    heading: 'Service',
                    key: 'service',
                    },
                    {
                    heading: 'Total Transaksi',
                    render: (item) => <TotalTransaction data={item} />,
                    },
                    {
                    heading: 'Aksi',
                        render: (item) => (
                            <ActionColumn
                                data={item}
                                onClick={() => {
                                    router.push(`/warehouse/outbound/regular-transaction/done/${item.id}`)
                                }}
                            />
                        ),
                    },
                ]}
                data={inbounds?.data || []}
                transformData={(item) => ({
                  ...item,
                  date: moment(item?.created_at)
                    .locale('id', localization)
                    .format('DD MMMM yyyy HH:mm:ss'),
                    store: item?.store.store_name || '-',
                    vendor: item?.vendor_service?.vendor?.name || '-',
                    service: item?.vendor_service?.type || '-',
                })}
                renderHeader={() => (
                    <HeaderTable
                        handleDate={handleDate}
                        handleExportLogs={handleExportLogs}
                        handleFilter={handleFilter}
                    />
                )}
                withNumber={false}
                renderEmptyData={() => <EmptyData />}
            />
        </div>
    );
}

export default TableTransactionReguler;
