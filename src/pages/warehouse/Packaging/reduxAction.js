import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setLoadingTable, setToast } from '../../../redux/actions';

export const getPackaging =
    (params = {}, packagingId) =>
        async (dispatch, getState) => {
            const { token } = getState().login;
            dispatch(setLoadingTable(true));
            try {
                let result;
                if (packagingId) {
                    result = await consume.getWithParams(
                        'packagingAdminWarehouse',
                        {},
                        { token },
                        {},
                        packagingId
                    );
                    dispatch(setDetailPackaging(result.result));
                } else {
                    result = await consume.getWithParams(
                        'packagingAdminWarehouse',
                        {},
                        { token },
                        params
                    );
                    dispatch(setPackaging(result.result));
                }
                dispatch(setLoadingTable(false));
                return Promise.resolve(result.result);
            } catch (err) {
                console.log('err', err);
                if (packagingId) {
                    dispatch(setDetailPackaging({}));
                } else {
                    dispatch(setPackaging({}));
                }
                dispatch(setLoadingTable(false));
                return Promise.reject(err);
            }
        };


function setPackaging(data) {
    return {
        type: actionType.PACKAGING_WAREHOUSE,
        payload: data,
    };
}

function setDetailPackaging(data) {
    return {
        type: actionType.DETAIL_PACKAGING_WAREHOUSE,
        payload: data,
    };
}

export const getListPackaging = () => async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result = await consume.get('packagingAdminWarehouse', {}, { token }, null, null, 'list');
      dispatch(setListPackaging(result.result));
      return Promise.resolve(result.result);
    } catch (err) {
      dispatch(setListPackaging([]));
    }
  };
  
function setListPackaging(data) {
    return {
        type: actionType.LIST_PACKAGING,
        payload: data,
    };
}