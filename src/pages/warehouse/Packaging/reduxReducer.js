import actionType from './reduxConstant';

const initialState = {
  packaging: [],
  detailPackaging: {},
  listPackaging: []
};

const packagingWarehouseReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.PACKAGING_WAREHOUSE:
      return {
        ...prevState,
        packaging: action.payload,
      };
    case actionType.DETAIL_PACKAGING_WAREHOUSE:
      return {
        ...prevState,
        detailPackaging: action.payload,
      };
    case actionType.LIST_PACKAGING:
      const listPackaging = action?.payload?.map((item) => ({
        ...item,
        value: item.id,
        label: item.name,
      }));
      
      return {
        ...prevState,
        listPackaging:  listPackaging,
      };
    default:
      return prevState;
  }
};

export default packagingWarehouseReducer;
