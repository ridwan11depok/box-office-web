import actionType from './reduxConstant';

const initialState = {
  dispatchOrder: {},
  detailDispatchOrder: {},
  listStore: [],
  listTrackingOrder: []
};

const dispatchOrderReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.DISPATCH_ORDER_WAREHOUSE:
      return {
        ...prevState,
        dispatchOrder: action.payload,
      };
    case actionType.DETAIL_DISPATCH_ORDER_WAREHOUSE:
      return {
        ...prevState,
        detailDispatchOrder: action.payload,
      };
    case actionType.LIST_STORE_ADMIN_WAREHOUSE: {
      const listStore = action.payload.map((item) => ({
        ...item,
        value: item.store_id,
        label: item.store.store_name,
      }));
      return {
        ...prevState,
        listStore,
      };
    }
    case actionType.LIST_TRACKING_ORDER:
      return {
        ...prevState,
        listTrackingOrder: action.payload,
    };
    default:
      return prevState;
  }
};

export default dispatchOrderReducer;
