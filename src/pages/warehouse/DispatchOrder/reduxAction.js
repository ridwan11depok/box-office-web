import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setErrorData, setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import baseUrl from '../../../api/url';
import axios from 'axios';

export const startOrder =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/picking`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memulai order dispatch order.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal memulai order dispatch order.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const rejectOrder =
  (payload = {}, dispatchOrderId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${dispatchOrderId}/reject`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Dispatch Order berhasil ditolak.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan penolakan dispatch order.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const cancelPicking =
  (payload = {}, dispatchOrderId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${dispatchOrderId}/cancel_picking`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Picking dispatch order berhasil dibatalkan.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan pembatalan picking dispatch order.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const cancelPacking =
  (payload = {}, dispatchOrderId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${dispatchOrderId}/cancel`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Packing dispatch order berhasil dibatalkan.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan pembatalan packing dispatch order.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};


export const unpacking =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token } = getState().login;
        let data = payload;
        let headers = { token: token };
        let response = await consume.post(
          'dispatchOrderAdminWarehouse',
          data,
          headers,
          null,
          `${id}/unpack`
        );
        dispatch(
          setToast({
            isShow: true,
            messages: response?.message || 'Berhasil Unpack.',
            type: 'success',
          })
        );
        dispatch(setLoading(false));
        return Promise.resolve('success');
      } catch (err) {
        dispatch(
          setToast({
            isShow: true,
            messages: 'Gagal Unpack.',
            type: 'error',
          })
        );
        dispatch(setLoading(false));
        return Promise.reject(err);
      }
    };

export const returnOrder =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token } = getState().login;
        let data = payload;
        let headers = { token: token };
        let response = await consume.post(
          'dispatchOrderAdminWarehouse',
          data,
          headers,
          null,
          `${id}/return`
        );
        dispatch(
          setToast({
            isShow: true,
            messages: response?.message || 'Berhasil return.',
            type: 'success',
          })
        );
        dispatch(setLoading(false));
        return Promise.resolve('success');
      } catch (err) {
        dispatch(
          setToast({
            isShow: true,
            messages: err?.error?.message || 'Gagal mereturn order.',
            type: 'error',
          })
        );
        dispatch(setLoading(false));
        return Promise.reject(err);
      }
    };

export const confirmPacking =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/packing`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmShipping =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/shipping`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmManifest =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/manifest`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const getDispatchOrder =
  (params = {}, dispatchOrderId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (dispatchOrderId) {
        result = await consume.getWithParams(
          'dispatchOrderAdminWarehouse',
          {},
          { token },
          {},
          dispatchOrderId
        );
        dispatch(setDetailDispatchOrder(result.result));
      } else {
        result = await consume.getWithParams(
          'dispatchOrderAdminWarehouse',
          {},
          { token },
          params
        );
        dispatch(setDispatchOrder(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      dispatch(setErrorData(err));
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setDispatchOrder(data) {
  return {
    type: actionType.DISPATCH_ORDER_WAREHOUSE,
    payload: data,
  };
}

function setDetailDispatchOrder(data) {
  return {
    type: actionType.DETAIL_DISPATCH_ORDER_WAREHOUSE,
    payload: data,
  };
}

export const getListStore = () => async (dispatch, getState) => {
  const { token } = getState().login;
  try {
    let result = await consume.get('listStoreAdminWarehouse', {}, { token });
    dispatch(setListStore(result.result));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStore([]));
  }
};

function setListStore(data) {
  return {
    type: actionType.LIST_STORE_ADMIN_WAREHOUSE,
    payload: data,
  };
}

export const generateAwb =
  (id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const {
        token
      } = getState().login;
      let data = {};
      let headers = {
        token: token
      };
      let response = await consume.post(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/generate-awb`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setDetailDispatchOrder(response.result));
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan generate AWB.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const printLabel =
  (id) => async (dispatch, getState) => {
    const {
      token,
      dataUser
    } = getState().login;
    try {

      const config = {
        url: baseUrl.dispatchOrderAdminWarehouse + id + '/shipping_label',
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/pdf`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
};

export const getTracking = (id) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    
    dispatch(setLoadingTable(true));
    try {
      
      let data = {};
      let headers = { token: token };
      let response = await consume.get(
        'dispatchOrderAdminWarehouse',
        data,
        headers,
        null,
        `${id}/tracking`
      );
      dispatch(setTrackingOrder(response.result));
      dispatch(setLoadingTable(false));
      return Promise.resolve(response.result);
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal mengambil data tracking',
          type: 'error',
        })
      );
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
};

function setTrackingOrder(data) {
  return {
    type: actionType.LIST_TRACKING_ORDER,
    payload: data,
  };
}