import React, { useEffect, useState } from "react";
import {
  ContentContainer,
  ContentItem,
} from "../../../components/elements/BaseContent";
import { TabHeader, TabContent } from "../../../components/elements/Tabs";
import { useHistory, useLocation, useParams } from "react-router";
import TableData from "./components/TableData";
import WaitingDetail from "./components/WaitingDetail";
import PickingDetail from "./components/PickingDetail";
import PackingDetail from "./components/PackingDetail";
import ShippingDetail from "./components/ShippingDetail";
import SentDetail from "./components/SentDetail";
import DoneDetail from "./components/DoneDetail";
import ReturnDetail from "./components/ReturnDetail";
import RejectDetail from "./components/RejectDetail";

const status = {
  pending: 0,
  picking: 1,
  packing: 2,
  shipping: 3,
  manifest: 4,
  sent: 5,
  done: 6,
  return: 7,
  cancel: 8,
  reject: 9,
};

const listHeader = [
  {
    tab: 0,
    title: "Waiting",
    url: "waiting",
  },
  {
    tab: 1,
    title: "Picking",
    url: "picking",
  },
  {
    tab: 2,
    title: "Packing",
    url: "packing",
  },
  {
    tab: 3,
    title: "Shipping",
    url: "shipping",
  },
  {
    tab: 5,
    title: "Sent",
    url: "sent",
  },
  {
    tab: 6,
    title: "Done",
    url: "done",
  },
  {
    tab: 7,
    title: "Return",
    url: "return",
  },
  {
    tab: 9,
    title: "Reject",
    url: "reject",
  },
];

const tabContents = [
  {
    tab: 0,
    Table: () => {
      return <TableData process={listHeader[0].url} status={0}/>
    },
    Detail: WaitingDetail,
  },
  {
    tab: 1,
    Table: () => {
      return <TableData process={listHeader[1].url} status={1}/>
    },
    Detail: PickingDetail,
  },
  {
    tab: 2,
    Table: () => {
      return <TableData process={listHeader[2].url} status={2}/>
    },
    Detail: PackingDetail,
  },
  {
    tab: 3,
    Table: () => {
      return <TableData process={listHeader[3].url} status={3}/>
    },
    Detail: ShippingDetail,
  },
  {
    tab: 5,
    Table: () => {
      return <TableData process={listHeader[4].url} status={5}/>
    },
    Detail: SentDetail,
  },
  {
    tab: 6,
    Table: () => {
      return <TableData process={listHeader[5].url} status={6}/>
    },
    Detail: DoneDetail,
  },
  {
    tab: 7,
    Table: () => {
      return <TableData process={listHeader[6].url} status={7}/>
    },
    Detail: ReturnDetail,
  },
  {
    tab: 9,
    Table: () => {
      return <TableData process={listHeader[7].url} status={9}/>
    },
    Detail: RejectDetail,
  },
];

const DispatchOrder = (props) => {
  let { process, id } = useParams();
  const location = useLocation();
  const router = useHistory();
  const [state, setState] = useState({
    tab: status[id] || 0,
    isDetail: false,
    splitURL: [],
  });
  const changeTab = (tab) => {
    setState({ ...state, tab });
    router.push("/warehouse/outbound/dispatch-order");
  };

  useEffect(() => {
    const split = location.pathname.split("/");
    const found = listHeader.find((item) => {
      return item.url === process
    });
    
    if(found){
      setState({ ...state, tab: found.tab, splitURL: split });
    }
  }, [location?.pathname, process]);

  const Content = tabContents.find((item) => {
    return item.tab === state.tab
  });

  return (
    <ContentContainer title="Dispatch Order">
      <ContentItem className="mb-3">
        <TabHeader
          border={false}
          activeTab={state.tab}
          listHeader={listHeader}
          onChange={changeTab}
          inActiveColor="#192a55"
        />
      </ContentItem>
      <ContentItem border={false}>
        {id ? (
          <TabContent tab={Content.tab} activeTab={state.tab}>
            <Content.Detail changeTab={changeTab} id={state.splitURL[5]} tab={listHeader.find((item)=>item.tab === state.tab)} />
          </TabContent>
        ) : (
          <TabContent tab={Content.tab} activeTab={state.tab}>
            <Content.Table tab={listHeader.find((item)=>item.tab === state.tab)} />
          </TabContent>
        )}
      </ContentItem>
    </ContentContainer>
  );
};

export default DispatchOrder;
