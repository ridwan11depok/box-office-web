import React, { useState, useEffect, useRef } from 'react';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import {
  TextField,
} from '../../../../components/elements/InputField';
import ModalShowInvoice from './ModalShowInvoice';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { generateAwb, printLabel } from '../reduxAction';
import { Link } from 'react-scroll'

const useStyles = makeStyles({
    root: {
      color: '#1c1c1c',
      fontFamily: 'Work Sans',
      fontSize: '18px',
      fontWeight: '700',
      display: ' flex',
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 0,
    },
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    label: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
        color: '#4f4f4f',
    },
    value: {
        fontSize: '14px',
        fontWeight: '600',
        fontFamily: 'Open Sans',
        color: '#1c1c1c',
    },
    link: {
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        color: '#192A55',
        textDecoration: 'underline',
        cursor: 'pointer',
    },
    textBold: {
        color: '#363636',
        fontWeight: 'bold',
    },
});

const status = {
    pending: 0,
    picking: 1,
    packing: 2,
    shipping: 3,
    manifest: 4,
    sent: 5,
    done: 6,
    return: 7,
    cancel: 8,
    reject: 9,
};

const HeaderTable = ({ handleModalReject, data, withScanBarcode = false, handleFillBarcode, valueBarcode}) => {
    const router = useHistory()
    const classes = useStyles();
    const componentRef = useRef();
    const dispatch = useDispatch();

    const [state, setState] = useState({
        showModalInvoice: false,
        isCopiedAWB: false
    });

    const print = async () => {

        if(data?.awb_number === null){
            await dispatch(generateAwb(data?.id));
        }
        
        try {
          const response = await dispatch(printLabel(data?.id))
          const url = window.URL.createObjectURL(new Blob([response.data]))
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', data?.transaction_number+` - Shipping Label.pdf`);
          document.body.appendChild(link);
          link.click();
        }
        catch (err) { }
    } 

    return (
        <div className="d-flex flex-column w-100">
            <div
                className="d-flex justify-content-between align-items-center p-3 flex-wrap"
                style={{ borderBottom: '1px solid #E8E8E8' }}
            >
                <BackButton label={data?.transaction_number} onClick={() => router.goBack()} />
                {data?.status == status.pending && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Tolak Order"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.picking && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Batalkan Picking"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.packing && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Batalkan Packing"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.done && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Retur"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.shipping ? 
                    <div>
                        <Button
                            style={{ minWidth: '120px', margin: '3px 0px' }}
                            text="Unpack"
                            styleType="redOutline"
                            onClick={handleModalReject}
                        />

                        <Button
                            style={{ minWidth: '120px', margin: '3px 0px', marginLeft: '10px' }}
                            text={data?.awb_number ? "Print Label" : 'Generate AWB & Print Label'}
                            styleType="blueOutline"
                            onClick={print}
                        />

                    </div>
                : ''}
            </div>

            {data?.awb_number && <div className="w-100 d-flex p-3 justify-content-between border ">
                <div className="">
                    <span>
                        Nomor Resi : {' '}
                        <span className={classes.textBold}>
                        {data?.awb_number || 'Tidak ada data'}
                        </span>
                    </span>
                    <CopyToClipboard text={`${data?.awb_number}`}>
                        <Button
                        text={state.isCopiedAWB ? 'Resi Disalin' : 'Salin No.Resi'}
                        styleType="blueOutline"
                        style={{ marginLeft: '10px' }}
                        onClick={() => setState({ ...state, isCopiedAWB: true })}
                        />
                    </CopyToClipboard>
                </div>
                    
                <Link to="tracking" spy={true} smooth={true}>
                    <Button
                        text="Riwayat Pengiriman"
                        styleType="blueNoFill"
                        endIcon={() => <ArrowDownwardIcon />}
                    />
                </Link>
               
            </div>}

            <Row className="p-1 pt-3 pb-2" style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}>
                <Col sm={12} md={12} lg={12} xl={12}>
                  <h1 className={classes.root}>
                    Data Pengirim dan Penerima
                  </h1>
                </Col>

                <Col sm={12} md={12} lg={6} xl={6} className="mt-5">
                  <Row>
                    <Col sm={12} md={12} lg={12} xl={12}>
                        <h6 className={classes.label}>Nama Pengirim</h6>
                        <h6 className={classes.value}>{data?.sender_name || 'Tidak ada nama pengirim'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={12} xl={12} className="mt-2">
                        <h6 className={classes.label}>Nomor Telepon Pengirim</h6>
                        <h6 className={classes.value}>{data?.sender_country_code+data?.sender_phone || 'Tidak Ada Nomor Telepon Pengirim'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={12} xl={12} className="mt-2">
                        <h6 className={classes.label}>Alamat Pengirim</h6>
                        <h6 className={classes.value}>{data?.sender_address || 'Tidak Ada Alamat Pengirim'}</h6>
                    </Col>
                  </Row>
                </Col>

                <Col sm={12} md={12} lg={6} xl={6} className="mt-5">
                  <Row>
                    <Col sm={12} md={12} lg={12} xl={12}>
                        <h6 className={classes.label}>Nama Penerima</h6>
                        <h6 className={classes.value}>{data?.receiver_name || 'Tidak ada nama pengirim'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={12} xl={12} className="mt-2">
                        <h6 className={classes.label}>Nomor Telepon Penerima</h6>
                        <h6 className={classes.value}>{data?.receiver_country_code+data?.receiver_phone || 'Tidak Ada Nomor Telepon Penerima'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={12} xl={12} className="mt-2">
                        <h6 className={classes.label}>Alamat Penerima</h6>
                        <h6 className={classes.value}>{data?.receiver_address || 'Tidak Ada Alamat Penerima'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={4} xl={4} className="mt-2">
                        <h6 className={classes.label}>Provinsi</h6>
                        <h6 className={classes.value}>{data?.receiver_province?.name || 'Tidak Ada Provinsi'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={4} xl={4} className="mt-2">
                        <h6 className={classes.label}>Kota</h6>
                        <h6 className={classes.value}>{data?.receiver_city?.name || 'Tidak Ada Kota'}</h6>
                    </Col>

                    <Col sm={12} md={12} lg={4} xl={4} className="mt-2">
                        <h6 className={classes.label}>Kecamatan</h6>
                        <h6 className={classes.value}>{data?.receiver_district?.name || 'Tidak Ada Kecamatan'}</h6>
                    </Col>
                  </Row>
                </Col>

            </Row>
            
            {withScanBarcode && 
                <div className="d-flex justify-content-md-end align-items-center p-3">
                    <TextField
                    prefix={() => <i className="fa fa-barcode" aria-hidden="true"></i>}
                    placeholder="Scan barcode"
                    onChange={handleFillBarcode}
                    value={valueBarcode}
                    />
                </div>
            }

            <ModalShowInvoice
                show={state.showModalInvoice}
                onHide={() => setState({ ...state, showModalInvoice: false })}
                pdfUrl={data?.invoice_file_full_url}
                pdfName={data?.invoice_file}
            />
        </div>
    );
};

export default HeaderTable;