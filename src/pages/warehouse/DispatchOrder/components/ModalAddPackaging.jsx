import React, { useState, useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { SearchField } from '../../../../components/elements/InputField';
import {
    ContentContainer as Container,
    ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import Checkbox from '@material-ui/core/Checkbox';
import { useSelector } from 'react-redux';
import Banner from '../../../../components/elements/Banner';

const ModalAddPackaging = (props) => {
    const [selected, setSelected] = useState([]);

    const { listPackaging } = useSelector(
        (state) => state.packagingWarehouseReducer
    );

    
    const [packagings, setProducts] = useState([]);
    
    useEffect(() => {
        if(listPackaging?.length){
            setProducts(listPackaging)
        }
    },[listPackaging])

    const onChange = (e, packagingItem) => {
        const isChecked = e.target.checked;
        if (isChecked) {
            setSelected([...selected, packagingItem]);
        } else {
            let newSelected = selected.filter((item) => item.id !== packagingItem.id);
            setSelected(newSelected);
        }
    };

    const handleSearch = (e) => {
        let value = e.target.value;
        let filtered = listPackaging.filter((item) =>
            item?.name.toLowerCase().includes(value.toLowerCase())
        );
        setProducts(filtered);
    };

    useEffect(() => {
        if (!props.show) {
            setSelected([]);
        } else if (props.show) {
            setSelected(props.initialValues);
        }
    }, [props.show]);

    const handleSelect = () => {
        const map = selected.map((item) => {
            if(!item?.qty_will_accepted){
                item.qty_will_accepted = 1
            }
            return item
        })
        props.onAgree(map);
    };
    return (
        <>
            <Modal
                titleModal="Tambah Packaging"
                bodyClassName="pb-3 pt-0"
                show={props.show}
                onHide={props.onHide}
                sizeModal="md"
                data={props.data}
                bodyStyle={{ border: 'none' }}
                scrollable
                content={(data) => (
                    <>
                        <Container withHeader={false}>
                            <Item spaceBottom={3}>
                                <div
                                    className="p-2"
                                    style={{
                                        borderBottom: '1px solid #E8E8E8',
                                        position: 'sticky',
                                        top: '0px',
                                        backgroundColor: '#FFFFFF',
                                        zIndex: '1050',
                                    }}
                                >
                                    <Banner
                                        styleType="blue"
                                        description="Silahkan pilih packaging sesuai dengan dimensi produk yang anda ingin kirimkan sesuai invoice"
                                        button={{ isShow: false }}
                                        className="mb-3"
                                    />
                                    <SearchField
                                        onChange={handleSearch}
                                        placeholder="Cari packaging"
                                    />
                                </div>
                                <div>
                                    <table className="table table-striped mt-0 mb-5">
                                        <tr
                                            className="pt-0 pb-0"
                                            style={{
                                                borderBottom: '1px solid #E8E8E8',
                                            }}
                                        >
                                            <th scope="col">Nama Packaging</th>
                                        </tr>
                                        <tbody>
                                            {packagings.map((item, idx) => (
                                                <tr
                                                    key={idx.toString()}
                                                    style={{ borderBottom: '1px solid #E8E8E8' }}
                                                >
                                                    <td>
                                                        <Checkbox
                                                            color=""
                                                            style={{ color: '#192A55' }}
                                                            checked={
                                                                selected.filter((packaging) => {
                                                                    return packaging?.id === item?.id
                                                                }).length 
                                                            }
                                                            onChange={(e) => onChange(e, item)}
                                                        />
                                                        {item?.name}
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                    {!packagings.length && (
                                        <div
                                            style={{
                                                height: '100px',
                                                fontSize: '14px',
                                                color: '#1C1C1C',
                                                fontFamily: 'Open Sans',
                                            }}
                                            className="d-flex justify-content-center align-items-center"
                                        >
                                            Hasil pencarian packaging tidak ditemukan
                                        </div>
                                    )}
                                </div>
                            </Item>
                        </Container>
                    </>
                )}
                footer={() => (
                    <div>
                        <Button
                            style={{ minWidth: '140px' }}
                            className="mr-3"
                            styleType="lightBlueFill"
                            text="Batal"
                            onClick={props.onHide}
                        />
                        <Button
                            style={{ minWidth: '140px' }}
                            styleType="blueFill"
                            text="Pilih Packaging"
                            onClick={handleSelect}
                        />
                    </div>
                )}
            />
        </>
    );
};

export default ModalAddPackaging;
