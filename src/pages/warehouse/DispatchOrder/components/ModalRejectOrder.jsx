import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextArea,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { rejectOrder, cancelPicking, cancelPacking, unpacking, returnOrder } from '../reduxAction';
import { useHistory } from 'react-router';

const validationSchema = Yup.object({
  reason: Yup.string().required('Harap untuk mengisi alasan'),
});

const listStatus = {
  0: {
    status: 'pending',
    title: 'Tolak Order',
    label: 'Alasan Menolak',
    subtitle: 'Masukkan alasan menolak',
    button: 'Tolak Order'
  },
  1: {
    status: 'picking',
    title: 'Batalkan Picking',
    label: 'Alasan Membatalkan',
    subtitle: 'Masukkan alasan membatalkan picking',
    button: 'Lanjutkan'
  },
  2: {
    status: 'packing',
    title: 'Batalkan Packing',
    label: 'Alasan Membatalkan',
    subtitle: 'Masukkan alasan membatalkan packing',
    button: 'Lanjutkan'
  },
  3: {
    status: 'shipping',
    title: 'Unpack',
    label: 'Alasan Melakukan Unpacking',
    subtitle: 'Masukkan alasan unpacking',
    button: 'Unpack'
  },
  4: {
    status: 'manifest',
  },
  5: {
    status: 'sent',
  },
  6: {
    status: 'done',
    title: 'Retur',
    label: 'Alasan Melakukan Retur',
    subtitle: 'Masukkan alasan retur',
    button: 'Retur'
  },
  7: {
    status: 'return',
  },
  8: {
    status: 'cancel',
  },
  9: {
    status: 'reject',
  },
};

const ModalRejectOrder = (props) => {
  const router = useHistory()
  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailDispatchOrder } = useSelector(
    (state) => state.dispatchOrderWarehouse
  );

  const [state, setState] = useState({
    currentStatus: ''
  });

  useEffect(() => {
    if(detailDispatchOrder.status || detailDispatchOrder.status === 0){
      setState({...state, currentStatus: listStatus[detailDispatchOrder.status]})
    }
  }, [detailDispatchOrder.status])
  
  const handleSubmit = async (values) => {
    try {
      const id = detailDispatchOrder?.id;

      if(state.currentStatus.status == 'pending'){
        await dispatch(rejectOrder(values, id))
      }
      else if(state.currentStatus.status == 'picking'){
        await dispatch(cancelPicking(values, id))
      }
      else if(state.currentStatus.status == 'packing'){
        await dispatch(cancelPacking(values, id))
      }
      else if(state.currentStatus.status == 'shipping'){
        await dispatch(unpacking(values, id))
      }
      else if(state.currentStatus.status == 'done'){
        await dispatch(returnOrder(values, id))
      }
      else{
        await dispatch(cancelPacking(values, id))
      }
      
      props.onHide();
      router.goBack()
      
    } catch (err) {
    }
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal={state.currentStatus.title}
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextArea
              label={state.currentStatus.label}
              placeholder={state.currentStatus.subtitle}
              name="reason"
              withValidate
              formik={formik}
              className="mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType={isLoading ? 'redFillDisabled' : 'redFill'}
              text={isLoading ? 'Loading...' : state.currentStatus.button}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
              className="mr-2"
            />
            <Button
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalRejectOrder;
