import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getDispatchOrder, confirmManifest } from '../reduxAction';
import ModalConfirm from './ModalConfirm';
import HeaderTableDetail from './HeaderTableDetail';
import ModalRejectOrder from './ModalRejectOrder';
import Avatar from '@material-ui/core/Avatar';
import TrackingOrder from './TrackingOrder';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};

const ImageColumn = ({ data }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {
        data?.product_storage?.product?.file_documents?.length ?
          data?.product_storage?.product?.file_documents.map((item) => (
            <Avatar
              key={item?.id}
              style={{ margin: '2px' }}
              alt="Logo"
              variant="rounded"
              src={item?.url}
            />
          ))
          :
          <span>Tidak ada gambar</span>

      }
    </div>
  );
};

const PackagingValue = ({ data }) => {
  return (
      <span>{`Rp ${data?.default_price?.toLocaleString('id')}` || '-'}</span>
  )
}

function ShippingDetail(props) {
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    counting: false,
    date: '',
    showModalReject: false,
    showModalConfirm: false,
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const router = useHistory()
  const { id } = props

  useEffect(() => {
    if (id) {
      dispatch(getDispatchOrder({}, id))
    }
  }, [id])

  const { detailDispatchOrder } = useSelector(
    (state) => state.dispatchOrderWarehouse
  );

  const handleBack = () => {
    props.changeTab(3)
  }

  const handleConfirmShipping = async () => {
    try{
      const respon = await dispatch(confirmManifest({}, id))
      router.push('/warehouse/manifest')
    }
    catch(err){
    }
  }


  return (
    <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'SKU',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },

          {
            heading: 'Gambar',
            render: (item) => <ImageColumn data={item} />,
          },
          {
            heading: 'Qty Order',
            key: 'qty',
          },
        ]}
        data={detailDispatchOrder?.items || []}
        transformData={(item) => ({
          ...item,
          SKU: item?.product_storage?.product?.sku || '-',
          name: item?.product_storage?.product?.name || '-',
          qty: item?.quantity || '-',
        })}
        renderHeader={() => (
          <HeaderTableDetail
            handleBack={handleBack}
            handleModalReject={() =>
              setState({ ...state, showModalReject: true })
            }
            data={detailDispatchOrder}
          />
        )}
        withNumber={false}
        showFooter={false}
        showBorderContainer={false}
        renderEmptyData={() => <EmptyData />}
      />
      <Table
        column={[
          {
            heading: 'Nama Packaging',
            key: 'name',
          },
          {
            heading: 'Qty',
            key: 'default_quantity',
          },

          {
            heading: 'Nilai Packaging',
            key: 'total_default_price',
          },
        ]}
        data={detailDispatchOrder?.packagings || []}
        transformData={(item) => ({
            ...item,
            name: item?.packaging?.name || '-',
            default_quantity: item?.default_quantity || '-',
            total_default_price: 'Rp '+item?.total_default_price?.toLocaleString('id') || '-'
        })}
        showFooter={false}
        withNumber={false}
        renderHeader={() => (
          <div className="d-flex justify-content-between flex-wrap p-3 align-items-center w-100">
            <h6 className={classes.titleTable}>Packaging</h6>
          </div>
        )}
      />

      {detailDispatchOrder?.awb_number && <TrackingOrder detailDispatchOrder={detailDispatchOrder} id={id}/>}

      <div className="d-flex justify-content-lg-end flex-wrap p-3">
        <Button
        onClick={handleBack}
          text="Kembali"
          styleType="lightBlueFill"
          style={{ minWidth: '120px', marginRight: '5px' }}
        />
        <Button
          text="Selesai"
          styleType={detailDispatchOrder?.awb_number ? "blueFill" : "blueFillDisabled"}
          style={{ minWidth: '120px' }}
          disabled={detailDispatchOrder?.awb_number ? false : true}
          onClick={() => setState({ ...state, showModalConfirm: true })}
        />
      </div>
      <ModalRejectOrder
        show={state.showModalReject}
        onHide={() => setState({ ...state, showModalReject: false })}
      />
      <ModalConfirm
        show={state.showModalConfirm}
        onHide={() => setState({ ...state, showModalConfirm: false })}
        onAgree={handleConfirmShipping}
        buttonText="Selesai"
        title="Selesai Shipping"
        description="Apakah anda yakin telah selesai melakukan proses shipping dan melanjutkan order ke proses manifest?"
      />
    </div>
  );
}

export default ShippingDetail;
