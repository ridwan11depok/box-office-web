import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import TimeLine from '../../../../components/elements/Timeline/TimeLine';
import { Row, Col } from 'react-bootstrap';
import { getTracking } from '../reduxAction';
import Image from 'react-bootstrap/Image';

const useStyles = makeStyles({
  title: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    fontStyle: 'normal',
    fontSize: '14px',
    lineHeight: '20px',
  },
  titleCourier: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    fontStyle: 'normal',
    fontSize: '18px',
    lineHeight: '28px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4F4F4F',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  avatar: {
    width: '104px',
    height: '104px',
    marginRight: '8px',
  },
});

function TrackingOrder(props){
    const { detailDispatchOrder, id } = props
    const classes = useStyles();
    const dispatch = useDispatch();

    const { listTrackingOrder } = useSelector(
        (state) => state.cashlessWarehouse
    );
    
    useEffect(() => {
        if (id) {
            dispatch(getTracking(id));
        }
    }, [id])

    const totalPrice = detailDispatchOrder?.vendor_price + detailDispatchOrder?.insurance_price;

    return (
        <Row className="p-4" id="tracking">

            <Col lg={12} xs={12} className="mb-4">
                <h6 className={classes.titleCourier}>Kurir</h6>
                
                <Row>
                    <Col lg={4} md={12} sm={12}>
                        <Image
                            className={classes.avatar}
                            fluid={true}
                            style={{
                                width: '64px',
                                height: '24px',
                            }}
                            src={detailDispatchOrder?.vendor_service?.vendor?.logo_full_url || ''}
                        />
                    </Col>

                    <Col lg={4} md={12} sm={12}>
                        <h6 className={classes.label}>{detailDispatchOrder?.vendor_service?.vendor?.name}</h6>
                        <h6 className={classes.value}>{detailDispatchOrder?.estimation}</h6>
                    </Col>

                    <Col lg={4} md={12} sm={12}>
                        <br/>
                        <h6 className={classes.value} style={{textAlign: 'right'}}>{`Rp. ${detailDispatchOrder?.vendor_price.toLocaleString('id')}` || 'No.Data'}</h6>
                    </Col>
                </Row>

                {detailDispatchOrder?.insurance_price && 
                <Row>
                    <Col lg={9} md={12} sm={12}>
                        <h6 className={classes.value}>Asuransi</h6>
                    </Col>

                    <Col lg={3} md={12} sm={12}>
                        <h6 className={classes.value} style={{textAlign: 'right'}}>{`Rp. ${detailDispatchOrder?.insurance_price.toLocaleString('id')}` || 'No.Data'}</h6>
                    </Col>
                </Row>}

                <Row>
                    <Col lg={9} md={12} sm={12}>
                        <h6 className={classes.value}>Total</h6>
                    </Col>

                    <Col lg={3} md={12} sm={12}>
                        <h6 className={classes.value} style={{textAlign: 'right'}}>{`Rp. ${totalPrice.toLocaleString('id')}` || 'No.Data'}</h6>
                    </Col>
                </Row>
            </Col>

            <Col lg={12} xs={12}>
                <h6 className={classes.title}>Riwayat Pengiriman</h6>
                <TimeLine data={listTrackingOrder}/>
            </Col>
        </Row>
    );
}

export default TrackingOrder;
