import actionType from './reduxConstant';

const initialState = {
  cashless: {},
  detailCashless: {},
  listStore: [],
  listTrackingOrder: []
};

const cashlessReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.CASHLESS_WAREHOUSE:
      return {
        ...prevState,
        cashless: action.payload,
      };
    case actionType.DETAIL_CASHLESS_WAREHOUSE:
      return {
        ...prevState,
        detailCashless: action.payload,
      };
    case actionType.LIST_STORE_ADMIN_WAREHOUSE: {
      const listStore = action.payload.map((item) => ({
        ...item,
        value: item.store_id,
        label: item.store.store_name,
      }));
      return {
        ...prevState,
        listStore,
      };
    }
    case actionType.LIST_TRACKING_ORDER:
      return {
        ...prevState,
        listTrackingOrder: action.payload,
    };
    default:
      return prevState;
  }
};

export default cashlessReducer;
