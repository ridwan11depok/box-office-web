import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setErrorData, setIsNotFound, setLoading, setLoadingTable, setToast } from '../../../redux/actions';
import baseUrl from '../../../api/url';
import axios from 'axios';

export const startOrder =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${id}/picking`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memulai order cashless.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal memulai order cashless.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const rejectOrder =
  (payload = {}, cashlessId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${cashlessId}/reject`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Cashless berhasil ditolak.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan penolakan cashless.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const cancelPicking =
  (payload = {}, cashlessId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${cashlessId}/cancel_picking`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Picking cashless berhasil dibatalkan.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan pembatalan picking cashless.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};

export const cancelPacking =
  (payload = {}, cashlessId) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${cashlessId}/cancel`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Packing cashless berhasil dibatalkan.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal melakukan pembatalan packing cashless.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
};


export const unpacking =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token } = getState().login;
        let data = payload;
        let headers = { token: token };
        let response = await consume.post(
          'cashlessWarehouse',
          data,
          headers,
          null,
          `${id}/unpack`
        );
        dispatch(
          setToast({
            isShow: true,
            messages: response?.message || 'Berhasil Unpack.',
            type: 'success',
          })
        );
        dispatch(setLoading(false));
        return Promise.resolve('success');
      } catch (err) {
        dispatch(
          setToast({
            isShow: true,
            messages: 'Gagal Unpack.',
            type: 'error',
          })
        );
        dispatch(setLoading(false));
        return Promise.reject(err);
      }
    };

export const returnOrder =
  (payload = {}, id) =>
    async (dispatch, getState) => {
      try {
        dispatch(setLoading(true));
        const { token } = getState().login;
        let data = payload;
        let headers = { token: token };
        let response = await consume.post(
          'cashlessWarehouse',
          data,
          headers,
          null,
          `${id}/return`
        );
        dispatch(
          setToast({
            isShow: true,
            messages: response?.message || 'Berhasil return.',
            type: 'success',
          })
        );
        dispatch(setLoading(false));
        return Promise.resolve('success');
      } catch (err) {
        dispatch(
          setToast({
            isShow: true,
            messages: 'Gagal mereturn order.',
            type: 'error',
          })
        );
        dispatch(setLoading(false));
        return Promise.reject(err);
      }
    };

export const confirmPacking =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${id}/packing`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil melakukan konfirmasi.',
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmShipping =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${id}/shipping`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const confirmManifest =
  (payload = {}, id) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.post(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${id}/manifest`
      );
      dispatch(
        setToast({
          isShow: true,
          messages: response.message,
          type: 'success',
        })
      );
      dispatch(setLoading(false));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal melakukan konfirmasi.',
          type: 'error',
        })
      );
      dispatch(setLoading(false));
      return Promise.reject(err);
    }
  };

export const getCashless =
  (params = {}, cashlessId) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    
    dispatch(setLoadingTable(true));
    try {
      let result;
      if (cashlessId) {
        result = await consume.getWithParams(
          'cashlessWarehouse',
          {},
          { token },
          {},
          cashlessId
        );
        dispatch(setDetailCashless(result.result));
      } else {
        result = await consume.getWithParams(
          'cashlessWarehouse',
          {},
          { token },
          params
        );
        dispatch(setCashless(result.result));
      }
      dispatch(setLoadingTable(false));
      return Promise.resolve(result.result);
    } catch (err) {
      dispatch(setErrorData(err));
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
  };

function setCashless(data) {
  return {
    type: actionType.CASHLESS_WAREHOUSE,
    payload: data,
  };
}

function setDetailCashless(data) {
  return {
    type: actionType.DETAIL_CASHLESS_WAREHOUSE,
    payload: data,
  };
}

export const getListStore = () => async (dispatch, getState) => {
  const { token } = getState().login;
  try {
    let result = await consume.get('listStoreAdminWarehouse', {}, { token });
    dispatch(setListStore(result.result));
    return Promise.resolve(result.result);
  } catch (err) {
    dispatch(setListStore([]));
  }
};

function setListStore(data) {
  return {
    type: actionType.LIST_STORE_ADMIN_WAREHOUSE,
    payload: data,
  };
}

export const getTracking = (id) =>
  async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    
    dispatch(setLoadingTable(true));
    try {
      let result;
      let data = {};
      let headers = { token: token };
      let response = await consume.get(
        'cashlessWarehouse',
        data,
        headers,
        null,
        `${id}/tracking`
      );
      dispatch(setTrackingOrder(response.result));
      dispatch(setLoadingTable(false));
      return Promise.resolve(response.result);
    } catch (err) {
      dispatch(
        setToast({
          isShow: true,
          messages: err?.error?.message || 'Gagal mengambil data tracking',
          type: 'error',
        })
      );
      dispatch(setLoadingTable(false));
      return Promise.reject(err);
    }
};

function setTrackingOrder(data) {
  return {
    type: actionType.LIST_TRACKING_ORDER,
    payload: data,
  };
}

export const printLabel =
  (id) => async (dispatch, getState) => {
    const { token, dataUser } = getState().login;
    try {
      
      const config = {
        url: baseUrl.cashlessWarehouse+id+'/shipping_label',
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': `application/pdf`,
          Accept: 'application/prs.boxxoffice.v1+json',
        },
        responseType: 'blob',
      };
      const res = await axios.request(config);
      return Promise.resolve(res);
    } catch (err) {
      return Promise.reject(err);
    }
  };