import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Counter from '../../../../components/elements/Counter';
import ModalRejectOrder from './ModalRejectOrder';
import HeaderTableDetail from './HeaderTableDetail';
import ModalConfirm from './ModalConfirm';
import { getCashless, confirmPacking } from '../reduxAction'
import { setErrorData, setToast } from '../../../../redux/actions';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
});

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang masuk saat ini.
      </h6>
    </div>
  );
};

const Quantity = ({ data, handleCounter }) => {
  return (
    <div>
      <Counter
        onDecrease={() => handleCounter(data, '-')}
        onIncrease={() => handleCounter(data, '+')}
        value={data?.qty_will_accepted || 0}
        max={data?.qty}
      />
    </div>
  );
};

const ImageColumn = ({ data }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {
        data?.product_storage?.product?.file_documents?.length ?
          data?.product_storage?.product?.file_documents.map((item) => (
            <Avatar
              key={item?.id}
              style={{ margin: '2px' }}
              alt="Logo"
              variant="rounded"
              src={item?.url}
            />
          ))
          :
          <span>Tidak ada gambar</span>

      }
    </div>
  );
};

function PickingDetail(props) {
  const { id } = props
  const router = useHistory()
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    counting: false,
    date: '',
    showModalReject: false,
    showModalConfirm: false,
    valueBarcode: '',
    dataProduct: []
  });

  const status = {
    pending: 0,
    picking: 1,
    packing: 2,
    shipping: 3,
    manifest: 4,
    sent: 5,
    done: 6,
    return: 7,
    cancel: 8,
    reject: 9,
  };


  const { detailCashless } = useSelector(
    (state) => state.cashlessWarehouse
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (id) {
      dispatch(getCashless({}, id))
    }
  }, [id])

  useEffect(() => {
    if (detailCashless?.items?.length) {
      const tamp = detailCashless?.items
      const initValues = tamp.map((item) => {
        item.qty_will_accepted = 0
        return item
      })
      setState({
        ...state,
        dataProduct: initValues
      })
    }
  }, [detailCashless])

  const handleCounter = (data, operator) => {
    let tampArray = state.dataProduct
    const map = tampArray.map((item) => {
      if (item.id === data.id) {
        if (operator === '+') {
          item.qty_will_accepted += 1
        } else {
          item.qty_will_accepted -= 1
        }
      }
      return item
    })

    setState({ ...state, dataProduct: map })
  }

  const handleFillBarcode = (e) => {
    const value = e.target.value;
    const map = state.dataProduct.map((item) => {
      if (item?.product_storage?.product?.sku === value) {
        if(item?.qty_will_accepted === item?.quantity){
          dispatch(
            setToast({
              isShow: true,
              messages: 'Quantity yang diambil sudah maksimal',
              type: 'error',
            })
          );
        }else{
          item.qty_will_accepted += 1
        }
      } 
      return item
    });
    setState({ ...state, dataProduct: map, valueBarcode: value });
    setTimeout(() => {
      setState({ ...state, dataProduct: map, valueBarcode: '' });
    }, 500);
  };

  const handleConfirmPacking = async () => {
    try{
      let error = state.dataProduct.some((item) => item.qty_will_accepted === 0)
      if(error){
        dispatch(
          setToast({
            isShow: true,
            messages: 'Quantity yang diambil tidak boleh 0',
            type: 'error',
          })
        );
        setState({ ...state, showModalConfirm: false })
      } else {
        let data = new FormData()
        state.dataProduct.forEach((item, index) => {
          data.append([`item[${index}][id]`], item.id)
          data.append([`item[${index}][quantity_warehouse]`], item.qty_will_accepted)
        })
        const response = await dispatch(confirmPacking(data, id))
        setState({ ...state, showModalConfirm: false })
        props.changeTab(2)
      }
    }
    catch(err){
      setState({ ...state, showModalConfirm: false })
    }
  }

  return (
    <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'SKU',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },

          {
            heading: 'Gambar',
            render: (item) => <ImageColumn data={item} />,
          },
          {
            heading: 'Qty Order',
            key: 'qty',
          },
          {
            heading: 'Qty yang Diambil',
            render: (item) => <Quantity handleCounter={handleCounter} data={item} />,
          },
        ]}
        data={state.dataProduct}
        transformData={(item) => ({
          ...item,
          SKU: item?.product_storage?.product?.sku || '-',
          name: item?.product_storage?.product?.name || '-',
          qty: item?.quantity || '-',
        })}
        renderHeader={() => (
          <HeaderTableDetail
            withScanBarcode={true}
            data={detailCashless}
            handleShowDetail={props.handleShowDetail}
            handleModalReject={() =>
              setState({ ...state, showModalReject: true })
            }
            handleFillBarcode={handleFillBarcode}
            valueBarcode={state.valueBarcode}
          />
        )}
        withNumber={false}
        showFooter={false}
        showBorderContainer={false}
        renderEmptyData={() => <EmptyData />}
      />
      <div className="d-flex justify-content-lg-end flex-wrap p-3">
        <Button
          text="Kembali"
          styleType="lightBlueFill"
          style={{ minWidth: '120px', marginRight: '5px' }}
          onClick={() => props.handleShowDetail(false)}
        />
        <Button
          text="Selesai"
          styleType="blueFill"
          style={{ minWidth: '120px' }}
          onClick={() => setState({ ...state, showModalConfirm: true })}
        />
      </div>
      <ModalRejectOrder
        show={state.showModalReject}
        onHide={() => setState({ ...state, showModalReject: false })}
      />
      <ModalConfirm
        show={state.showModalConfirm}
        onHide={() => setState({ ...state, showModalConfirm: false })}
        onAgree={handleConfirmPacking}
        title="Selesai Picking"
        description="Apakah Anda yakin telah selesai melakukan picking produk?"
      />
    </div>
  );
}

export default PickingDetail;
