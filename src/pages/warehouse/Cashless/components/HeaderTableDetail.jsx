import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import {
  TextField,
} from '../../../../components/elements/InputField';
import ModalShowInvoice from './ModalShowInvoice';
import { printLabel } from '../reduxAction';

const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    label: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
        color: '#4f4f4f',
    },
    value: {
        fontSize: '14px',
        fontWeight: '600',
        fontFamily: 'Open Sans',
        color: '#1c1c1c',
    },
    link: {
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        color: '#192A55',
        textDecoration: 'underline',
        cursor: 'pointer',
    },
});

const status = {
    pending: 0,
    picking: 1,
    packing: 2,
    shipping: 3,
    manifest: 4,
    sent: 5,
    done: 6,
    return: 7,
    cancel: 8,
    reject: 9,
};

const HeaderTable = ({ handleModalReject, data, withScanBarcode = false, handleFillBarcode, valueBarcode}) => {
    const router = useHistory()
    const classes = useStyles();
    const dispatch = useDispatch();
    const componentRef = useRef();

    const [state, setState] = useState({
        showModalInvoice: false
    });

    const print = async () => {
      try {
        const response = await dispatch(printLabel(data?.id))
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', data?.transaction_number+` - Shipping Label.pdf`);
        document.body.appendChild(link);
        link.click();
      }
      catch (err) { }
    }  

    return (
        <div className="d-flex flex-column w-100">
            <div
                className="d-flex justify-content-between align-items-center p-3 flex-wrap"
                style={{ borderBottom: '1px solid #E8E8E8' }}
            >
                <BackButton label={data?.transaction_number} onClick={() => router.goBack()} />
                {data?.status == status.pending && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Tolak Order"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.picking && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Batalkan Picking"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.packing && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Batalkan Packing"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.done && <Button
                    style={{ minWidth: '120px', margin: '3px 0px' }}
                    text="Retur"
                    styleType="redOutline"
                    onClick={handleModalReject}
                />}

                {data?.status == status.shipping ? 
                    <div>
                        <Button
                            style={{ minWidth: '120px', margin: '3px 0px' }}
                            text="Unpack"
                            styleType="redOutline"
                            onClick={handleModalReject}
                        />

                        <Button
                            style={{ minWidth: '120px', margin: '3px 0px', marginLeft: '10px' }}
                            text="Print Label"
                            styleType="blueOutline"
                            onClick={print}
                        />

                    </div>
                : ''}
            </div>
            <Row
                className="p-1 pt-3 pb-2"
                style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
            >
                <Col>
                    <h6 className={classes.label}>Nomor Invoice</h6>
                    <h6 className={classes.value}>{data?.invoice_number || 'Tidak ada data nomor invoice'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Foto Invoice</h6>
                    <h6
                    className={classes.link}
                    onClick={() => setState({ ...state, showModalInvoice: true })}
                    >
                    Lihat Foto
                    </h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Nama Pengirim</h6>
                    <h6 className={classes.value}>{data?.sender_name || 'Tidak ada nama pengirim'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Penerima</h6>
                    <h6 className={classes.value}>{data?.receiver_name || 'Tidak ada nama penerima'}</h6>
                </Col>
            </Row>
            <Row
                className="p-1 pt-2 pb-2"
                style={{ borderBottom: '1px solid #E8E8E8', margin: 0 }}
            >
                <Col>
                    <h6 className={classes.label}>Logistik</h6>
                    <h6 className={classes.value}>{data?.vendor_service?.vendor?.name || 'Tidak ada data logistik'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Service</h6>
                    <h6 className={classes.value}>{data?.vendor_service?.code || 'Tidak ada data service'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>AWB</h6>
                    <h6 className={classes.value}>{data?.awb_number || 'Tidak ada data AWB'}</h6>
                </Col>
                <Col>
                    <h6 className={classes.label}>Platform</h6>
                    <h6 className={classes.value}>{data?.platform?.name || 'Tidak ada data platform'}</h6>
                </Col>
            </Row>
            {withScanBarcode && 
                <div className="d-flex justify-content-md-end align-items-center p-3">
                    <TextField
                    prefix={() => <i className="fa fa-barcode" aria-hidden="true"></i>}
                    placeholder="Scan barcode"
                    onChange={handleFillBarcode}
                    value={valueBarcode}
                    />
                </div>
            }

            <ModalShowInvoice
                show={state.showModalInvoice}
                onHide={() => setState({ ...state, showModalInvoice: false })}
                pdfUrl={data?.invoice_file_full_url}
                pdfName={data?.invoice_file}
            />
        </div>
    );
};

export default HeaderTable;