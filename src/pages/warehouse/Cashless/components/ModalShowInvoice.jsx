import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { get_url_extension } from '../../../../utils/text';

const ModalShowInvoice = (props) => {

  return (
    <>
      <Modal
        titleModal="Invoice"
        bodyClassName="p-1"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <div className="text-left">
            { props?.pdfUrl ? 
                get_url_extension(props?.pdfUrl) === 'pdf' ?
                <iframe src={props?.pdfUrl} title={props?.pdfName} width="100%" height="400px" />
                :
                <img
                    alt="Invoice"
                    src={props?.pdfUrl}
                    className="w-100"
                  />
              : ''
            }
          </div>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              text="Kembali"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

ModalShowInvoice.defaultProps = {
  src: null,
};
export default ModalShowInvoice;
