import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getCashless } from '../reduxAction';
import Avatar from '@material-ui/core/Avatar';
import HeaderTableDetail from './HeaderTableDetail';
import TrackingOrder from './TrackingOrder';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    color: '#1c1c1c',
    marginBottom: 0,
    marginRight: '30px',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#4f4f4f',
  },
  value: {
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    color: '#1c1c1c',
  },
  link: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#192A55',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
});

const EmptyData = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada proses outbound yang dilakukan saat ini.
      </h6>
    </div>
  );
};




const ImageColumn = ({ data }) => {
  return (
    <div className="d-flex flex-row flex-wrap">
      {
        data?.product_storage?.product?.file_documents?.length ?
          data?.product_storage?.product?.file_documents.map((item) => (
            <Avatar
              key={item?.id}
              style={{ margin: '2px' }}
              alt="Logo"
              variant="rounded"
              src={item?.url}
            />
          ))
          :
          <span>Tidak ada gambar</span>

      }
    </div>
  );
};

function ReturnDetail(props) {
  const [state, setState] = useState({
    showDetail: false,
    selectedRow: {},
    search: '',
    storeId: null,
    vendorId: null,
    counting: false,
    date: '',
  });
  const classes = useStyles();
  const dispatch = useDispatch();

  const { id } = props

  useEffect(() => {
    if (id) {
      dispatch(getCashless({}, id))
    }
  }, [id])

  const { detailCashless } = useSelector(
    (state) => state.cashlessWarehouse
  );

  const handleBack = () => {
    props.changeTab(7)
  }

  return (
    <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
      <Table
        column={[
          {
            heading: 'SKU',
            key: 'SKU',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },

          {
            heading: 'Gambar',
            render: (item) => <ImageColumn data={item} />,
          },
          {
            heading: 'Qty Order',
            key: 'qty',
          },
        ]}
        data={detailCashless?.items || []}
        transformData={(item) => ({
          ...item,
          SKU: item?.product_storage?.product?.sku || '-',
          name: item?.product_storage?.product?.name || '-',
          qty: item?.quantity || '-',
        })}
        renderHeader={() => (
          <HeaderTableDetail
            data={detailCashless}
            handleBack={handleBack}
          />
        )}
        withNumber={false}
        showFooter={false}
        showBorderContainer={false}
        renderEmptyData={() => <EmptyData />}
      />
      <Table
        column={[
          {
            heading: 'Nama Packaging',
            key: 'name',
          },
          {
            heading: 'Qty',
            key: 'default_quantity',
          },

          {
            heading: 'Nilai Packaging',
            key: 'total_default_price',
          },
        ]}
        data={detailCashless?.packagings || []}
        transformData={(item) => ({
            ...item,
            name: item?.packaging?.name || '-',
            default_quantity: item?.default_quantity || '-',
            total_default_price: 'Rp '+item?.total_default_price?.toLocaleString('id') || '-'
        })}
        showFooter={false}
        withNumber={false}
        renderHeader={() => (
          <div className="d-flex justify-content-between flex-wrap p-3 align-items-center w-100">
            <h6 className={classes.titleTable}>Packaging</h6>
          </div>
        )}
      />

      <TrackingOrder id={id}/>
    </div>
  );
}

export default ReturnDetail;
