import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
    ReactSelect,
    SearchField,
    TextField,
} from '../../../../components/elements/InputField';

import { Row, Col } from 'react-bootstrap';
import BackButton from '../../../../components/elements/BackButton';
import Avatar from '@material-ui/core/Avatar';
import ModalRejectOrder from './ModalRejectOrder';
import HeaderTableDetail from './HeaderTableDetail';
import ModalConfirm from './ModalConfirm';
import { getCashless, startOrder} from '../reduxAction'
import { setErrorData, setToast } from '../../../../redux/actions';

const useStyles = makeStyles({
    description: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
    },
    titleTable: {
        fontSize: '18px',
        fontWeight: '700',
        fontFamily: 'Work Sans',
        color: '#1c1c1c',
        marginBottom: 0,
        marginRight: '30px',
    },
    label: {
        fontSize: '14px',
        fontWeight: '400',
        fontFamily: 'Open Sans',
        color: '#4f4f4f',
    },
    value: {
        fontSize: '14px',
        fontWeight: '600',
        fontFamily: 'Open Sans',
        color: '#1c1c1c',
    },
    link: {
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Open Sans',
        color: '#192A55',
        textDecoration: 'underline',
        cursor: 'pointer',
    },
});

const status = {
    pending: 0,
    picking: 1,
    packing: 2,
    shipping: 3,
    manifest: 4,
    sent: 5,
    done: 6,
    return: 7,
    cancel: 8,
    reject: 9,
};

const EmptyData = () => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <div
            className="d-flex flex-column align-items-center justify-content-center"
            style={{ height: '30vh' }}
        >
            <h6 className={classes.description}>
                Tidak ada proses outbound yang masuk saat ini.
            </h6>
        </div>
    );
};

const ImageColumn = ({ data }) => {
    return (
        <div className="d-flex flex-row flex-wrap">
            {
                data?.product_storage?.product?.file_documents?.length ?
                    data?.product_storage?.product?.file_documents.map((item) => (
                        <Avatar
                            key={item?.id}
                            style={{ margin: '2px' }}
                            alt="Logo"
                            variant="rounded"
                            src={item?.url}
                        />
                    ))
                    :
                    <span>Tidak ada gambar</span>

            }
        </div>
    );
};

const PackagingValue = ({ data }) => {
    return (
        <span>{`Rp ${data?.default_price?.toLocaleString('id')}` || '-'}</span>
    )
}

function WaitingDetail(props) {
    const { id } = props
    const router = useHistory();
    const classes = useStyles();
    const [state, setState] = useState({
        search: '',
        storeId: null,
        vendorId: null,
        date: '',
        showModalReject: false,
        showModalConfirm: false,
        showModalInvoice: false
    });

    const { detailCashless } = useSelector(
        (state) => state.cashlessWarehouse
    );

    const dispatch = useDispatch();

    useEffect(() => {
        if (id) {
            dispatch(getCashless({}, id))
        }
    }, [id])

    useEffect(() => {
        if (detailCashless?.items?.length) {
            const tamp = detailCashless?.items
            const initValues = tamp.map((item) => {
                item.qty_will_accepted = 0
                return item
            })
            setState({ ...state, dataProduct: initValues })
        }
    }, [detailCashless])


    const handleConfirmPicking = async () => {
        try {
            const response = await dispatch(startOrder({}, id))
            setState({ ...state, showModalConfirm: false })
            props.changeTab(1)
        }
        catch (err) {
            setState({ ...state, showModalConfirm: false })
        }
    }

    const imageInvoice = () => {
        setState({ ...state, showModalInvoice: true });
    }

    return (
        <>
            <div style={{ border: '1px solid #E8E8E8' }} className="rounded">
                <Table
                    column={[
                        {
                            heading: 'SKU',
                            key: 'SKU',
                        },
                        {
                            heading: 'Nama Produk',
                            key: 'name',
                        },

                        {
                            heading: 'Gambar',
                            render: (item) => <ImageColumn data={item} />,
                        },
                        {
                            heading: 'Qty Order',
                            key: 'qty',
                        },
                    ]}
                    data={state.dataProduct}
                    transformData={(item) => ({
                        ...item,
                        SKU: item?.product_storage?.product?.sku || '-',
                        name: item?.product_storage?.product?.name || '-',
                        qty: item?.quantity || '-',
                    })}
                    renderHeader={() => (
                        <HeaderTableDetail
                            data={detailCashless}
                            handleModalReject={() =>
                                setState({ ...state, showModalReject: true })
                            }
                            onClick={imageInvoice}
                        />
                    )}
                    withNumber={false}
                    showFooter={false}
                    showBorderContainer={false}
                    renderEmptyData={() => <EmptyData />}
                />
            </div>
            <div style={{ border: '1px solid #E8E8E8' }} className="rounded mt-3">
                <Table
                    column={[
                    {
                        heading: 'Nama Packaging',
                        key: 'name',
                    },
                    {
                        heading: 'Qty',
                        key: 'default_quantity',
                    },

                    {
                        heading: 'Nilai Packaging',
                        key: 'total_default_price',
                    },
                    ]}
                    data={detailCashless?.packagings || []}
                    transformData={(item) => ({
                        ...item,
                        name: item?.packaging?.name || '-',
                        default_quantity: item?.default_quantity || '-',
                        total_default_price: 'Rp '+item?.total_default_price?.toLocaleString('id') || '-'
                    })}
                    showFooter={false}
                    withNumber={false}
                    renderHeader={() => (
                    <div className="d-flex justify-content-between flex-wrap p-3 align-items-center w-100">
                        <h6 className={classes.titleTable}>Packaging</h6>
                    </div>
                    )}
                />
            </div>
            <div className="d-flex justify-content-lg-end flex-wrap p-3">
                <Button
                    text="Kembali"
                    styleType="lightBlueFill"
                    style={{ minWidth: '120px', marginRight: '5px' }}
                    onClick={() => router.goBack()}
                />
                <Button
                    text="Start Order"
                    styleType="blueFill"
                    style={{ minWidth: '120px' }}
                    onClick={() => setState({ ...state, showModalConfirm: true })}
                />
            </div>
            <ModalRejectOrder
                show={state.showModalReject}
                onHide={() => setState({ ...state, showModalReject: false })}
            />
            <ModalConfirm
                show={state.showModalConfirm}
                onHide={() => setState({ ...state, showModalConfirm: false })}
                onAgree={handleConfirmPicking}
                title="Mulai Order"
                description="Apakah anda yakin akan memproses order dan mulai untuk memindahkan ke proses selanjutnya?"
            />
        </>
    );
}

export default WaitingDetail;
