import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import TimeLine from '../../../../components/elements/Timeline/TimeLine';
import { Row, Col } from 'react-bootstrap';
import { getTracking } from '../reduxAction';

const useStyles = makeStyles({
  title: {
    fontFamily: 'Work Sans',
    fontWeight: '700',
    fontStyle: 'normal',
    fontSize: '14px',
    lineHeight: '20px',
    marginBottom: '16px',
    marginLeft: '-8px',
    marginTop: '-8px',
  }
});

function TrackingOrder(props){
    const { id } = props
    const classes = useStyles();
    const dispatch = useDispatch();

    const { listTrackingOrder } = useSelector(
        (state) => state.cashlessWarehouse
    );

    useEffect(() => {
        if (id) {
            dispatch(getTracking(id));
        }
    }, [id])

    return (
        <Row className="p-4">
            <Col lg={12} xs={12}>
                <h6 className={classes.title}>Riwayat Pengiriman</h6>
                <TimeLine data={listTrackingOrder}/>
            </Col>
        </Row>
    );
}

export default TrackingOrder;
