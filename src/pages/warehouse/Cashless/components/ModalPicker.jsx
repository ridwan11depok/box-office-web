import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { TextField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { useDispatch, useSelector } from 'react-redux';
import { confirmInbound } from '../reduxAction';

const validationSchema = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama picker'),
});

const ModalPicker = (props) => {
  const formik = useFormik({
    initialValues: {
      name: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailInbound } = useSelector((state) => state.inboundWarehouse);

  const handleSubmit = async (values) => {
    try {
      const payload = {
        status: 1,
        picker_name: values.name,
      };
      const inboundId = detailInbound?.id;
      await dispatch(confirmInbound(payload, inboundId));
      props.onHide();
    } catch (err) {}
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Picker"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextField
              label="Nama Picker"
              placeholder="Input nama picker"
              name="name"
              withValidate
              formik={formik}
              className="mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <Button
              styleType="lightBlueFill"
              className="mr-2"
              text="Batal"
              onClick={props.onHide}
              style={{ width: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Simpan'}
              onClick={formik.handleSubmit}
              style={{ width: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalPicker;
