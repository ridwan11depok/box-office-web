import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import BackButton from '../../../components/elements/BackButton';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { Table } from '../../../components/elements/Table';
import Button from '../../../components/elements/Button';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import InfoStore from './components/InfoStore';
import InfoLocation from './components/InfoLocation';
import Banner from '../../../components/elements/Banner';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import ModalVerificationStore from './components/ModalVerificationStore';
import { getAllStore, approval, getUserStore } from './reduxAction';

const useStyles = makeStyles({
  textBanner: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#1c1c1c',
    fontWeight: '400',
    marginBottom: '0px',
  },
  title: {
    color: '#1c1c1c',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    fontWeight: '700',
    display: ' flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 0,
  },
});

const validationSchema = Yup.object({});

function StoreDetail() {
  const history = useHistory();
  const params = useParams();
  const classes = useStyles();
  const [state, setState] = useState({
    modalDelete: false,
    isEdit: false,
    modalVerificationStore: false,
    status: 0,
    showBanner: true,
  });

  //for edit
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailStore, listUserStore } = useSelector((state) => state.storeWarehouse);
  
  useEffect(() => {
    if (params?.id) {
      const storeId = params?.id;
      dispatch(getAllStore({}, storeId));
      dispatch(getUserStore({}, storeId));
    }
  }, [params?.id]);

  const handleApprovalStore = async (payload) => {
    try {
      const storeId = params?.id;
      dispatch(approval(payload, storeId));
      setState({
        ...state,
        modalVerificationStore: false,
      });
    } catch (err) {}
  };

  const fetchUserStore = (data) => {
    dispatch(getUserStore(data, params?.id));
  };

  const EmptyData = () => {
    const classes = useStyles();
    return (
      <div
        className="d-flex flex-column align-items-center justify-content-center"
        style={{ height: '30vh' }}
      >
        <h6 className={classes.description}>Tidak ada data user store saat ini.</h6>
      </div>
    );
  };

  const HeaderTable = () => {
    return (
      <div className="d-flex flex-row flex-wrap justify-content-between align-items-center p-2 pl-3 w-100" style={{height: '60px'}}>
        <h1 className={classes.title}>
          Data User
        </h1>
      </div>
    );
  };

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label={detailStore?.store?.store_name || 'Store Detail'}
            onClick={() => history.push('/warehouse/store')}
          />
        </div>
      )}
    >
      {/* (detailStore?.status === 0 || detailStore?.status === 2) */}
      {(detailStore?.status === 0 || detailStore?.status === 2) &&
        state.showBanner && (
          <ContentItem col="col-12 p-3" spaceBottom={3} border={false}>
            <Banner
              className="row align-items-lg-center"
              styleType={detailStore?.status === 0 ? 'yellow' : 'red'}
            >
              <div className="col-lg-9 col-md-6 col-sm-12">
                <h6
                  className={`${classes.textBanner} col-12 col-lg-8 mb-lg-0 mb-1`}
                >
                  {detailStore?.status === 0
                    ? 'Store sedang menunggu pengajuan untuk proses penggunaan gudang dengan mengirimkan produk'
                    : 'Store dalam keadaan rejected dan sedang menunggu persetujuan pihak gudang untuk menggunakan warehouse.'}
                </h6>
              </div>
              
              {detailStore?.status === 0 ? (
                <div className="col-lg-3 col-md-6 col-sm-12 text-right">

                  <Button
                    styleType="redFill"
                    startIcon={() => <CloseIcon />}
                    text="Reject"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 2,
                      })
                    }
                    style={{ minWidth: '140px' }}
                    className="col-lg-6 m-1 mb-lg-0"
                  />

                  <Button
                    styleType="greenFill"
                    startIcon={() => <DoneIcon />}
                    text="Approve"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 1,
                      })
                    }
                    style={{ minWidth: '140px' }}
                    className="col-lg-6 m-1 mb-lg-0"
                  />
                  
                </div>
              ) : (
                <div>
                  <Button
                    styleType="lightBlueFill"
                    text="Batal"
                    style={{ minWidth: '140px' }}
                    onClick={() => setState({ ...state, showBanner: false })}
                    className="m-1 mb-lg-0"
                  />
                  <Button
                    styleType="greenFill"
                    startIcon={() => <DoneIcon />}
                    text="Approve"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 1,
                      })
                    }
                    style={{ minWidth: '140px' }}
                    className="m-1 mb-lg-0"
                  />
                </div>
              )}
            </Banner>
          </ContentItem>
        )}
      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: state.isEdit ? '615px' : '200px' }}
      >
        <div className="p-3">
          <InfoStore initialValues={detailStore} />
        </div>
      </ContentItem>
      <ContentItem
        spaceLeft="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: state.isEdit ? '615px' : '200px' }}
      >
        <div className="p-3">
          <InfoLocation initialValues={detailStore} />
        </div>
      </ContentItem>

      <ContentItem
        col="col-lg-12 col-md-12"
        spaceBottom={3}
      >
        <Table
          action={fetchUserStore}
          totalPage={listUserStore?.last_page || 1}
          column={[
            {
              heading: 'Nama User',
              key: 'name',
            },
            {
              heading: 'Email',
              key: 'email',
            },
            {
              heading: 'Role',
              key: 'role',
            },
            {
              heading: 'Nomor Telepon',
              key: 'phone_number',
            },
          ]}
          data={listUserStore?.data || []}
          transformData={(item) => ({
            ...item,
            name: item?.user?.name,
            email: item?.user?.email,
            role: item?.user?.roles[0].display_name,
            phone_number: item?.user?.country_code+''+item?.user?.phone_number,
          })}
          renderHeader={() => (
            <HeaderTable/>
          )}
          withNumber={false}
          renderEmptyData={() => <EmptyData />}
        />
      </ContentItem>

      <ModalVerificationStore
        show={state.modalVerificationStore}
        onHide={() => setState({ ...state, modalVerificationStore: false })}
        onAgree={handleApprovalStore}
        status={state.status}
        data={detailStore?.store?.store_name || '-'}
      />
    </ContentContainer>
  );
}

export default StoreDetail;
