import actionType from './reduxConstant';

const initialState = {
  store: [],
  detailStore: {},
  listUserStore: []
};

const storeWarehouseReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_STORE_WAREHOUSE:
      return {
        ...prevState,
        store: action.payload,
      };
    case actionType.DETAIL_STORE_WAREHOUSE:
      return {
        ...prevState,
        detailStore: action.payload,
      };
    case actionType.LIST_USER_STORE:
      return {
        ...prevState,
        listUserStore: action.payload,
      };
    default:
      return prevState;
  }
};

export default storeWarehouseReducer;
