import React, { useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import { TextArea } from '../../../../components/elements/InputField';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
  reason: Yup.string().required(
    'Harap untuk mengisi alasan menolak penggunaan gudang'
  ),
});

const ModalVerificationStore = (props) => {
  const [reason, setReason] = useState('');
  // const { detailStore } = useSelector((state) => state.storeAdministrator);
  const { isLoading } = useSelector((state) => state.loading);

  const formik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      props.onAgree({ status: props.status, reason: values.reason });
    },
  });

  const handleSubmit = (e) => {
    if (props.status === 1) {
      props.onAgree({ status: props.status });
    } else if (props.status === 2) {
      formik.handleSubmit(e);
    }
  };
  return (
    <>
      <Modal
        titleModal="Request Warehouse"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        content={(data) =>
          props.status === 1 ? (
            <h6
              style={{
                fontFamily: 'Open Sans',
                fontSize: '14px',
                color: '#1c1c1c',
                fontWeight: '400',
              }}
            >
              Apakah anda yakin menyetujui {data} untuk menggunakan warehouse
              anda?
            </h6>
          ) : (
            <TextArea
              label="Alasan Menolak"
              placeholder="Input alasan menolak store"
              formik={formik}
              withValidate
              name="reason"
            />
          )
        }
        footer={() =>
          props.status === 1 ? (
            <div>
              <Button
                style={{ minWidth: '140px' }}
                styleType="lightBlueFill"
                text="Batal"
                onClick={props.onHide}
              />
              <Button
                style={{ minWidth: '140px' }}
                className="ml-3"
                styleType="greenFill"
                text={isLoading ? 'Loading...' : 'Verified'}
                onClick={handleSubmit}
              />
            </div>
          ) : (
            <div>
              <Button
                style={{ minWidth: '140px' }}
                className="mr-3"
                styleType="redFill"
                text={isLoading ? 'Loading...' : 'Reject'}
                onClick={handleSubmit}
              />
              <Button
                style={{ minWidth: '140px' }}
                styleType="lightBlueFill"
                text="Batal"
                onClick={props.onHide}
              />
            </div>
          )
        }
      />
    </>
  );
};

ModalVerificationStore.defaultProps = {
  status: 0,
};
export default ModalVerificationStore;
