import React, { useState, useEffect } from 'react';
import { Table } from '../../../../components/elements/Table';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  ReactSelect,
  SearchField,
} from '../../../../components/elements/InputField';
import Avatar from '@material-ui/core/Avatar';
import clsx from 'clsx';
import { getAllStore } from '../reduxAction';

const useStyles = makeStyles({
  rootStatus: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  waiting: {
    color: '#F57F17',
  },
  rejected: {
    color: '#DA101A',
  },
  approved: {
    color: '#1B5E20',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const HeaderTable = ({ handleFilter, handleSearch }) => {
  const classes = useStyles();
  return (
    <div className="d-flex flex-row flex-wrap justify-content-between align-items-center p-2 pl-3 w-100">
      <div style={{ width: '32%', minWidth: '120px' }}>
        <ReactSelect
          label="Tipe Store"
          placeholder="Pilih tipe store"
          options={[
            { value: 'all', label: 'Pilih Semua' },
            { value: 'Ecommerce', label: 'Ecommerce' },
            { value: 'Corporation', label: 'Corporation' },
          ]}
          onChange={(e) => handleFilter('type', e.target.value)}
        />
      </div>
      <div style={{ width: '32%', minWidth: '120px' }}>
        <ReactSelect
          label="Status Pengajuan"
          placeholder="Pilih status pengajuan"
          options={[
            { value: 'all', label: 'Semua Status' },
            { value: '0', label: 'Waiting' },
            { value: '1', label: 'Approved' },
            { value: '2', label: 'Rejected' },
          ]}
          onChange={(e) => handleFilter('status', e.target.value)}
        />
      </div>
      <div
        className="align-self-end"
        style={{ width: '32%', minWidth: '120px' }}
      >
        <SearchField placeholder="Cari store" onEnter={handleSearch} />
      </div>
    </div>
  );
};

const EmptyData = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data store saat ini.</h6>
    </div>
  );
};

const ActionField = ({ data, onClick }) => {
  return (
    <>
      <Button
        text="Detail"
        styleType="lightBlueFill"
        onClick={onClick}
      />
    </>
  );
};

const StatusColumn = ({ status = 0 }) => {
  const classes = useStyles();
  return (
    <h6
      className={clsx(classes.rootStatus, {
        [classes.waiting]: status === 0,
        [classes.approved]: status === 1,
        [classes.rejected]: status === 2,
      })}
    >
      {status === 0 ? 'Waiting' : status === 1 ? 'Approved' : 'Rejected'}
    </h6>
  );
};

const LogoColumn = ({ data }) => {
  return <Avatar alt="Logo" variant="square" src={data?.store?.store_logo_full_url || ''} />;
};

function StoreTable(props) {
  const [state, setState] = useState({
    search: '',
    status: null,
    type: null,
  });

  const { store } = useSelector((state) => state.storeWarehouse);
  const dispatch = useDispatch();
  const history = useHistory();

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const handleFilter = (filterType, value) => {
    if (filterType === 'type') {
      setState({ ...state, type: value === 'all' ? null : value });
    } else if (filterType === 'status') {
      setState({ ...state, status: value === 'all' ? null : value });
    }
  };

  const fetchListStore = (params) => {
    dispatch(getAllStore(params));
  };
  return (
    <div>
      <Table
        action={fetchListStore}
        totalPage={store?.last_page || 1}
        params={{
          search: state.search,
          status: state.status ? state.status : null,
          type: state.type ? state.type : null,
        }}
        column={[
          {
            heading: 'Logo',
            render: (data) => <LogoColumn data={data} />,
          },
          {
            heading: 'Nama Store',
            key: 'name',
          },
          {
            heading: 'Tipe Store',
            key: 'type',
          },
          {
            heading: 'Alamat',
            key: 'address',
          },
          {
            heading: 'Status',
            render: (data) => <StatusColumn status={data?.status || 0} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} onClick={() => {
              history.push(`/warehouse/store/${data?.id}`, {
                storeId: data.id,
              });
            }}/>,
          },
        ]}
        data={store?.data || []}
        transformData={(item) => ({
          ...item,
          logo: item?.store?.store_logo_full_url,
          name: item?.store?.store_name,
          type: item?.store?.type,
          address: item?.store?.store_address,
        })}
        renderHeader={() => (
          <HeaderTable
            handleSearch={handleSearch}
            handleFilter={handleFilter}
          />
        )}
        withNumber={false}
        renderEmptyData={() => <EmptyData />}
      />
    </div>
  );
}

export default StoreTable;
