import React, { useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import {
  ContentContainer,
  ContentItem,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles({
  avatar: {
    width: '150px',
    height: '150px',
    marginRight: '8px',
    marginBottom: '8px',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1C1C1C',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1C1C1C',
    marginBottom: '15px',
  },
});

const ModalDetailSKU = (props) => {
  const classes = useStyles();
  return (
    <>
      <Modal
        titleModal="SKU Detail"
        bodyClassName="p-2"
        show={props.show}
        onHide={props.onHide}
        sizeModal="lg"
        content={() => (
          <ContentContainer withHeader={false}>
            <ContentItem
              spaceRight="0 pr-md-2"
              col="col-12 col-md-6"
              spaceBottom={3}
              style={{ minHeight: '550px' }}
              className="p-3"
            >
              <div>
                <h6 className={classes.label}>Foto Produk</h6>
                <div className="d-flex flex-wrap">
                  {[1, 2, 3].map((photo) => (
                    <Avatar
                      alt="Logo"
                      className={classes.avatar}
                      variant="rounded"
                      src="https://s3-alpha-sig.figma.com/img/d00d/a9c6/0d5c90398571b1c2705be632fb32a210?Expires=1635120000&Signature=Z7Zn30UUfh0Kt9AQzyB8N~b3X2QeFjA9walyru8GzBi9io98LsofM8Jh79rgHmxHZunmh--icWHlsWEQ6AlQeYyh8x1wWxii7tORYt32l5AmztR78i-ppc8F4RtQlfgRgVEk836o4FV38waQhFjc0lFJnPtr9Bt~NRE6Fx4PjLhfr49D01SSHQUN08Kxk4fg6Ax92b6dlnLZv2Wg23XrfO3UrswHiyO9XsmynM8rMojr7KMqcJW2HiyhhF9DpPvFy--vFAxsM8YQ2U1QugwAH3iTTfSENKT4x-y3B3ivf7sBCIZa5ABxIsnSQdQpOw2xFCVuMzpQttDGkp0L0UjOuw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                    />
                  ))}
                </div>
              </div>
              <div>
                <h6 className={classes.label}>SKU</h6>
                <h6 className={classes.value}>123-SKU</h6>
              </div>
              <div>
                <h6 className={classes.label}>Nama Produk</h6>
                <h6 className={classes.value}>-</h6>
              </div>
              <div>
                <h6 className={classes.label}>Harga Jual</h6>
                <h6 className={classes.value}>-</h6>
              </div>
            </ContentItem>
            <ContentItem
              spaceLeft="0 pl-md-2"
              col="col-12 col-md-6"
              spaceBottom={3}
              style={{ minHeight: '550px' }}
              className="p-3"
            >
              <div>
                <h6 className={classes.label}>Kategori Produk</h6>
                <h6 className={classes.value}>-</h6>
              </div>
              <div>
                <h6 className={classes.label}>Sub Kategori Produk</h6>
                <h6 className={classes.value}>123-SKU</h6>
              </div>
              <div className="row">
                <div className="col-6">
                  <h6 className={classes.label}>Lebar</h6>
                  <h6 className={classes.value}>-</h6>
                </div>
                <div className="col-6">
                  <h6 className={classes.label}>Tinggi</h6>
                  <h6 className={classes.value}>-</h6>
                </div>
                <div className="col-6">
                  <h6 className={classes.label}>Panjang</h6>
                  <h6 className={classes.value}>-</h6>
                </div>
                <div className="col-6">
                  <h6 className={classes.label}>Berat</h6>
                  <h6 className={classes.value}>-</h6>
                </div>
              </div>
              <div>
                <h6 className={classes.label}>FEFO</h6>
                <h6 className={classes.value}>-</h6>
              </div>
            </ContentItem>
          </ContentContainer>
        )}
        scrollable
        footer={() => (
          <>
            <Button
              styleType="lightBlueFill"
              text="Kembali"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalDetailSKU;
