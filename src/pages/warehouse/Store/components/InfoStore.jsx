import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { Form } from 'react-bootstrap';
import { TextField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'react-bootstrap/Image'
import Badge from '../../../../components/elements/Badge';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  storeName: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#000000',
  },
  storeType: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  avatar: {
    width: '104px',
    height: '104px',
    marginRight: '8px',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4F4F4F',
    marginBottom: '4px',
    lineHeight: '20px'
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#363636',
  },
});

function FormInfoStore(props) {
  const classes = useStyles();
  const [state, setState] = useState({ image: '' });
  
  const { detailStore } = useSelector((state) => state.storeAdministrator);
  
  return (
    <>
      <div>
        <div className="row">
          <div className="col-lg-2">
            <Image
              className={classes.avatar}
              fluid={true}
              src={props.initialValues?.store?.store_logo_full_url || ''}
            />
          </div>
          
          <div className="col-lg-10">
            <h6 className={classes.label}>Nama Store</h6>
            <h6 className={classes.value}>{props.initialValues?.store?.store_name || 'No Data'}</h6>

            <Badge
              label={
                props.initialValues?.status === 0
                  ? 'Waiting'
                  : props.initialValues?.status === 1
                  ? 'Verified'
                  : 'Rejected'
              }
              style={
                {
                  fontSize: '14px'
                }
              }
              styleType={
                props.initialValues?.status === 0
                  ? 'yellow'
                  : props.initialValues?.status === 1
                  ? 'green'
                  : 'red'
              }
            />
          </div>
          
          <div className={props.initialValues?.status === 2 ? "col-lg-6 mt-3" : "col-lg-12 mt-3"}>
            <h6 className={classes.label}>Tipe Store</h6>
            <h6 className={classes.value}>{props.initialValues?.store?.type || 'No Data'}</h6>
          </div>

          {props.initialValues?.status === 2 && (
            <div className="col-lg-6 mt-3">
              <h6 className={classes.label}>Alasan Ditolak</h6>
              <h6 className={classes.value}>{props.initialValues?.reason}</h6>
            </div>
          )}

        </div>
        
      </div>
    </>
  );
}
FormInfoStore.defaultProps = {
  initialValues: null,
  isEdit: true,
};

export default FormInfoStore;
