import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import ModalDetailSKU from './ModalDetailSKU';
// import ModalDetailUser from './ModalDetailUser';
// import ModalDeleteUser from './ModalDeleteUser';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const ActionField = ({ onClick = () => {}, data = {} }) => {
  return (
    <Button
      text="Detail"
      styleType="lightBlueFill"
      onClick={() => onClick(data)}
    />
  );
};

const EmptyUser = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data saat ini</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  return (
    <div className="d-flex flex-row justify-content-between align-items-center p-3">
      <h6 className="mb-0">SKU Produk</h6>
    </div>
  );
};

function StoreDetailTable(props) {
  const dispatch = useDispatch();
  const {
    columnUser,
    columnRateHandling,
    columnWarehouse,
    listUserStore,
    listRateHandlingStock,
    listWarehouseStore,
  } = useSelector((state) => state.storeAdministrator);
  const history = useHistory();
  const [state, setState] = useState({
    modalDetailSku: false,
    detailSku: {},
  });

  const handleShowDetailSku = (data) => {
    setState({ ...state, modalDetailSku: true, detailSku: data });
  };

  const fetchListData = async (params) => {
    if (state.tab === 1) {
      // dispatch(getListUserStore(params));
    } else if (state.tab === 2) {
      // dispatch(getListRateHandlingStock(params));
    } else {
      // dispatch(getListWarehouseStore(params));
    }
  };

  return (
    <div>
      <DataTable
        action={fetchListData}
        // totalPage={listUserStore?.last_page || 5}
        // params={{
        //   search: state.search,
        //   store_id: props.storeId,
        // }}
        column={[
          {
            heading: 'SKU',
            key: 'sku',
          },
          {
            heading: 'Produk',
            key: 'prod',
          },
          {
            heading: 'Kategori',
            key: 'cat',
          },
          {
            heading: 'Sub Kategori',
            key: 'sub',
          },
          {
            heading: 'Qty',
            key: 'qty',
          },
          {
            heading: 'Aksi',
            render: (dataRow) => (
              <ActionField data={dataRow} onClick={handleShowDetailSku} />
            ),
          },
        ]}
        data={[
          {
            sku: '123-SKU',
            prod: 'Baju',
            cat: 'Busana',
            sub: 'Baju Dinas',
            qty: 15,
          },
        ]}
        withNumber={false}
        renderHeader={() => <HeaderTable />}
        renderEmptyData={() => <EmptyUser />}
      />
      <ModalDetailSKU
        show={state.modalDetailSku}
        onHide={() => setState({ ...state, modalDetailSku: false })}
        onAgree={() => setState({ ...state, modalDetailSku: false })}
      />
    </div>
  );
}

export default StoreDetailTable;
