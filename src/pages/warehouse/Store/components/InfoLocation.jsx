import React, { useEffect, useState } from 'react';
import {
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
// import { getArea } from '../../Gudang/reduxAction';
import MapModal from '../../../../components/elements/Map/MapModal';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4F4F4F',
    marginBottom: '4px',
    lineHeight: '20px'
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#363636',
  },
});

function FormLokasiStore(props) {
  const classes = useStyles();
  const [state, setState] = useState({
    provice_id: 0,
    city_id: 0,
    showMap: false,
  });
  return (
    <div>
      <h6 className={classes.label}>Alamat</h6>
      <h6 className={classes.value}>
        {props.initialValues?.store?.store_address || 'No Address'}
      </h6>
      <Button
        styleType="blueNoFill"
        className="pl-0"
        text="Lihat Lokasi di Peta"
        startIcon={() => <MapOutlinedIcon />}
        onClick={() => setState({ ...state, showMap: true })}
      />
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        initialCoordinate={{
          lng: props.initialValues?.store?.longitude,
          lat: props.initialValues?.store?.latitude,
        }}
        mode="viewer"
      />
    </div>
  );
}

FormLokasiStore.defaultProps = {
  initialValues: null,
  isEdit: true,
};

export default FormLokasiStore;
