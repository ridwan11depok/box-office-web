import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import StoreTable from './components/StoreTable';
import { TabHeader } from '../../../components/elements/Tabs';

const useStyles = makeStyles({});

const DispatchOrder = (props) => {
  const classes = useStyles();
  return (
    <ContentContainer title="Store">
      <ContentItem border={false}>
        <StoreTable />
      </ContentItem>
    </ContentContainer>
  );
};

export default DispatchOrder;
