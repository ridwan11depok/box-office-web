import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setErrorData, setLoading, setLoadingTable, setToast } from '../../../redux/actions';

export const getAllStore =
  (params = {}, storeId) =>
  async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result;
      if (storeId) {
        dispatch(setLoading(true));
        const otherEndpoint = `detail/${storeId}`;
        result = await consume.get('storeWarehouse', {}, { token }, null, null, otherEndpoint);
        dispatch(setDetailStore(result.result));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(true));
        result = await consume.getWithParams(
          'storeWarehouse',
          {},
          { token },
          params
        );
        dispatch(setStore(result.result));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      if (storeId) {
        dispatch(setErrorData(err));
        dispatch(setDetailStore({}));
        dispatch(setLoading(false));
      } else {
        dispatch(setStore({}));
        dispatch(setLoadingTable(false));
      }
      // return Promise.reject(err);
    }
  };

export const getUserStore =
  (params = {}, storeId) =>
  async (dispatch, getState) => {
    const { token } = getState().login;
    try {
      let result;
      dispatch(setLoading(true));
      const otherEndpoint = `detail/${storeId}/users`;
      result = await consume.getWithParams('storeWarehouse', {}, { token }, params, null, otherEndpoint);
      dispatch(setUserStore(result.result));
      dispatch(setLoading(false));
    } catch (err) {
      dispatch(setUserStore([]));
       dispatch(setLoadingTable(false));
    }
  };

function setUserStore(data) {
  return {
    type: actionType.LIST_USER_STORE,
    payload: data,
  };
}

function setStore(data) {
  return {
    type: actionType.LIST_STORE_WAREHOUSE,
    payload: data,
  };
}
function setDetailStore(data) {
  return {
    type: actionType.DETAIL_STORE_WAREHOUSE,
    payload: data,
  };
}

export const approval = (payload, id) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };
    const otherEndpoint = `approval/${id}`;
    const response = await consume.post(
      'storeWarehouse',
      data,
      headers,
      otherEndpoint
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: `Berhasil ${
          payload.status === 1 ? 'memverifikasi' : 'menolak/me-reject'
        } pengajuan penggunaan gudang.`,
        type: 'success',
      })
    );
    dispatch(getAllStore({}, id));
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: `Gagal ${
          payload.status === 1 ? 'memverifikasi' : 'menolak/me-reject'
        } pengajuan penggunaan gudang.`,
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};