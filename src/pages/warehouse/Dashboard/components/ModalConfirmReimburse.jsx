import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    textAlign: 'right',
    margin: '10px 0',
  },
  label: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    textAlign: 'left',
    margin: '10px 0',
  },
});

const ModalConfirmReimburse = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  const classes = useStyles();
  return (
    <>
      <Modal
        titleModal="Konfirmasi Pembayaran"
        bodyClassName="pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <Row>
            <Col xs={6}>
              <h6 className={classes.label}>Seller</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>OneStop Polos</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Tipe Reimburse</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Packaging</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.label}>Nominal</h6>
            </Col>
            <Col xs={6}>
              <h6 className={classes.value}>Rp 15.000</h6>
            </Col>
          </Row>
        )}
        data={props.data}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
              style={{ minWidth: '120px' }}
            />
            <Button
              styleType={isLoading ? 'blueFillDisabled' : 'blueFill'}
              text={isLoading ? 'Loading...' : 'Konfirmasi'}
              onClick={props.onAgree}
              style={{ minWidth: '120px' }}
              disabled={isLoading}
            />
          </>
        )}
      />
    </>
  );
};

export default ModalConfirmReimburse;
