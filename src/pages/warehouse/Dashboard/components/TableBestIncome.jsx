import React from 'react';
import { Table } from '../../../../components/elements/Table';

function TableBestSeller() {
  return (
    <div>
      <Table
        renderHeader={() => (
          <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
            <h6 className="mb-2 mt-2">Penjualan Store Terbaik</h6>
          </div>
        )}
        column={[
          {
            heading: 'SKU',
            key: 'sku',
          },
          {
            heading: 'Nama Produk',
            key: 'name',
          },
          {
            heading: 'Kategori',
            key: 'cat',
          },
          {
            heading: 'Sub Kategori',
            key: 'sub',
          },
          {
            heading: 'Jumlah Produk Terjual',
            key: 'qty',
          },
        ]}
        data={[]}
        showFooter={false}
        withNumber={false}
      />
    </div>
  );
}

export default TableBestSeller;
