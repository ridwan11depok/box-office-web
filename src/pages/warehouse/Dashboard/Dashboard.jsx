import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import * as DashboardIcon from './components/Icons';
import { formatRupiah } from '../../../utils/text';
import TableBestIncome from './components/TableBestIncome';
import CardLogistic from './components/CardLogistic';
import { useHistory } from 'react-router-dom';
import Chart from './components/Chart';
import {
  DatePicker,
  ReactSelect,
} from '../../../components/elements/InputField';
import Banner from '../../../components/elements/Banner';
import Avatar from '@material-ui/core/Avatar';
import Badge from '../../../components/elements/Badge';

const dummyAvatar =
  'https://s3-alpha-sig.figma.com/img/91c7/a185/e1c28911c2f04de2414bed34443e81c4?Expires=1639958400&Signature=UDF1cuVmYq0Yxe~PbE5sZ66b874Rdsxy4ADGzgCxKqOrZ8fsPvGJnxErAeJ~k1WSge20e~LrtGUdtEQVdPwID6zjMAV1tleQvmdGskaIl3Umcqo7U2MQkM3GbBva9DsVrWivHm2uf-5gX0qCyiLT-OfS0qYohMWo4AuMQFnDvbMb2BxXKQh1i05~WmcqLWgLkE0bjKYW9bb8QcQlnsD9QbLK-dMYz9wtLXGhsCqdNXc9l6WyOQXl7B-0-K~L7jo8TTeGgqZPnKEKsfGApW5svZ-350XROGkhCFTMz-vXrvlXr~Pkitg9dg-wi8svr4JC0zQYtH-T~sLpVJCiwTMVWQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA';

const useStyles = makeStyles({
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  avatar: {
    width: '104px',
    height: '104px',
    marginRight: '8px',
  },
  storeName: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#000000',
  },
});

const Dashboard = (props) => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
          <PageTitle title="Dashboard" />
          <div className="d-flex flex-row align-items-center">
            <div style={{ width: '200px', marginRight: '15px' }}>
              <ReactSelect
                placeholder="Pilih periode"
                // options={optionsStatus}
                // onChange={(e) => handleFilter('status', e.target.value)}
              />
            </div>
            <div className="mt-1 mb-1" style={{ width: '200px' }}>
              <DatePicker
                format="MMMM YYYY"
                views={['year', 'month']}
                noMarginBottom
                placeholder="Pilih bulan"
                suffixIcon="calendar"
                // onDateChange={handleFilterDate}
              />
            </div>
          </div>
        </div>
      )}
    >
      <ContentItem border={false} className="mb-3">
        <Banner
          button={{ isShow: false }}
          styleType="green"
          description="Informasi"
        />
      </ContentItem>
      <ContentItem
        className="p-3 d-flex"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <Avatar
          className={classes.avatar}
          alt="Logo"
          variant="rounded"
          src={dummyAvatar}
        />
        <div>
          <h6 className={classes.label}>Nama Warehouse</h6>
          <h6 className={classes.value}>Gudang MeubelNew</h6>
          <Badge
            label="Verified"
            styleType="green"
            style={{ marginTop: '15px' }}
            size="md"
          />
        </div>
      </ContentItem>
      <ContentItem
        className="p-0"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div
          className="p-3 d-flex justify-content-between align-items-center"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <h6 className="m-0">Dompet</h6>
          <Button
            styleType="blueOutline"
            // className="ml-auto"
            text="Reimburse"
            onClick={() => history.push('/warehouse/dashboard/reimburse')}
          />
        </div>
        <div className="d-flex align-items-center  p-3">
          <DashboardIcon.WalletIcons />
          <div className="ml-2">
            <h6 className={classes.label}>Saldo Dompet</h6>
            <h6 className={classes.value}>{formatRupiah(1000)}</h6>
          </div>
        </div>
      </ContentItem>
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.RupiahIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Penghasilan Gudang</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.UserIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Reimburse Kurir</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.BoxIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Reimburse Packaging</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem
        className="p-0"
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Inbound Summary</h6>
        </div>
        <div
          className="d-flex flex-lg-row flex-column"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.BoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Total Inbound</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.TruckIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Pickup</h6>
              <h6 className={classes.value}>850</h6>
            </div>
          </div>
        </div>
        <div className="d-flex flex-lg-row flex-column">
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.HandOpenIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Sampai Gudang</h6>
              <h6 className={classes.value}>900</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.CheckedBoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Selesai</h6>
              <h6 className={classes.value}>900</h6>
            </div>
          </div>
        </div>
      </ContentItem>

      <ContentItem
        className="p-0"
        spaceRight="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
      >
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Outbound Summary</h6>
        </div>
        <div
          className="d-flex flex-lg-row flex-column"
          style={{ borderBottom: '1px solid #E8E8E8' }}
        >
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.BoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Total Outbound</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.MinusBoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Picking</h6>
              <h6 className={classes.value}>10000</h6>
            </div>
          </div>
        </div>
        <div className="d-flex flex-lg-row flex-column">
          <div
            className="d-flex align-items-center justify-between p-3 w-lg-50 w-100"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <DashboardIcon.TruckIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Shipping</h6>
              <h6 className={classes.value}>1000</h6>
            </div>
          </div>
          <div className="d-flex align-items-center justify-between p-3 w-lg-50 w-100">
            <DashboardIcon.CheckedBoxIcons />
            <div className="ml-2">
              <h6 className={classes.label}>Selesai</h6>
              <h6 className={classes.value}>10000</h6>
            </div>
          </div>
        </div>
      </ContentItem>
      <ContentItem className="p-0" col="col-12" spaceBottom={3}>
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Penghasilan Gudang</h6>
        </div>
        <Chart />
      </ContentItem>
      <ContentItem col="col-12" spaceBottom={3} border={false}>
        <TableBestIncome />
      </ContentItem>
      <ContentItem className="p-0" col="col-12" spaceBottom={3}>
        <div style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="m-3">Logistik</h6>
        </div>
        <div className="d-flex flex-column flex-lg-row">
          <div
            className="w-lg-50 w-100 p-3"
            style={{
              borderRight: '1px solid #E8E8E8',
            }}
          >
            <h6 className={classes.label}>Kemarin</h6>
            <div>
              {Array(10)
                .fill(null)
                .map((item) => (
                  <CardLogistic />
                ))}
            </div>
          </div>
          <div className="w-lg-50 w-100 p-3">
            <h6 className={classes.label}>Hari Ini</h6>
            <div className="w-100">
              {Array(10)
                .fill(null)
                .map((item) => (
                  <CardLogistic />
                ))}
            </div>
          </div>
        </div>
      </ContentItem>
    </ContentContainer>
  );
};

export default Dashboard;
