import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import * as DashboardIcon from './components/Icons';
import { formatRupiah } from '../../../utils/text';
import TableBestIncome from './components/TableBestIncome';
import CardLogistic from './components/CardLogistic';
import { useHistory } from 'react-router-dom';
import Chart from './components/Chart';
import {
  DatePicker,
  ReactSelect,
  SearchField,
} from '../../../components/elements/InputField';
import Banner from '../../../components/elements/Banner';
import Avatar from '@material-ui/core/Avatar';
import Badge from '../../../components/elements/Badge';
import BackButton from '../../../components/elements/BackButton';
import { Table } from '../../../components/elements/Table';
import ModalConfirmReimburse from './components/ModalConfirmReimburse';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  value: {
    color: '#363636',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginBottom: 0,
  },
  label: {
    color: '#4F4F4F',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const ActionField = ({ onClick }) => {
  return (
    <>
      <Button text="Proses" styleType="blueFill" onClick={onClick} />
    </>
  );
};

const EmptyReimburse = ({ onClick }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>
        Tidak ada riwayat reimburse yang masuk saat ini.
      </h6>
    </div>
  );
};

const Reimburse = (props) => {
  const [state, setState] = useState({ showModalConfirmReimburse: false });
  const classes = useStyles();
  const history = useHistory();

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label="Reimburse"
            onClick={() => history.push('/warehouse/dashboard')}
          />
        </div>
      )}
    >
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.WalletIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Saldo Dompet</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.WalletIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Reimburse Packaging</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem
        className="p-3 d-flex align-items-center"
        col="col-12 col-lg-4"
        spaceBottom={3}
      >
        <DashboardIcon.WalletIcons />
        <div className="ml-2">
          <h6 className={classes.label}>Reimburse Kurir</h6>
          <h6 className={classes.value}>{formatRupiah(1000)}</h6>
        </div>
      </ContentItem>
      <ContentItem col="col-12" spaceBottom={3} border={false}>
        <Table
          renderHeader={() => (
            <div className="w-100">
              <div
                className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100"
                style={{ borderBottom: '1px solid #E8E8E8' }}
              >
                <h6 className="mb-2 mt-2">Riwayat Reimburse</h6>
              </div>
              <div className="p-3 d-flex justify-content-between align-items-center flex-wrap w-100">
                <div
                  className="mt-1 mb-1"
                  style={{ minWidth: '250px', width: '32%' }}
                >
                  <DatePicker
                    format="MMMM YYYY"
                    views={['year', 'month']}
                    noMarginBottom
                    placeholder="Pilih tanggal"
                    prefixIcon="calendar"
                    prefix
                    suffix={false}
                    label="Tanggal"
                    // onDateChange={handleFilterDate}
                  />
                </div>
                <div
                  className="mt-1 mb-1"
                  style={{ minWidth: '250px', width: '32%' }}
                >
                  <ReactSelect
                    placeholder="Pilih store"
                    label="Store"
                    // options={optionsStatus}
                    // onChange={(e) => handleFilter('status', e.target.value)}
                  />
                </div>
                <div
                  className="mt-auto mb-1"
                  style={{ minWidth: '250px', width: '32%' }}
                >
                  <SearchField placeholder="Cari kode, store" />
                </div>
              </div>
            </div>
          )}
          column={[
            {
              heading: 'Tanggal',
              key: 'date',
            },
            {
              heading: 'Kode Reimburse',
              key: 'code',
            },
            {
              heading: 'Seller',
              key: 'seller',
            },
            {
              heading: 'Tipe Reimburse',
              key: 'type',
            },
            {
              heading: 'Nominal',
              key: 'nominal',
            },
            {
              heading: 'Aksi',
              render: (item) => (
                <ActionField
                  data={item}
                  onClick={() =>
                    setState({ ...state, showModalConfirmReimburse: true })
                  }
                />
              ),
            },
          ]}
          data={[
            {
              date: '2 September 2021 16:43:36',
              code: 'RE123',
              seller: 'OneStop Polos',
              type: 'Packaging',
              nominal: 'Rp 15.000',
            },
          ]}
          withNumber={false}
          renderEmptyData={() => <EmptyReimburse />}
        />
      </ContentItem>
      <ModalConfirmReimburse
        show={state.showModalConfirmReimburse}
        onHide={() => setState({ ...state, showModalConfirmReimburse: false })}
      />
    </ContentContainer>
  );
};

export default Reimburse;
