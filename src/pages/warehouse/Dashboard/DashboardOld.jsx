import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import Banner from '../../../components/elements/Banner';
import { makeStyles } from '@material-ui/core/styles';
import rupiahIcon from '../../../assets/icons/rupiah.svg';
import homeIcon from '../../../assets/icons/homegate.svg';
import peopleIcon from '../../../assets/icons/people.svg';

const useStyles = makeStyles({
  value: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    marginBottom: 0,
  },
  label: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const Dashboard = (props) => {
  const classes = useStyles();

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
          <PageTitle title="Dashboard" />
          {/* <div className="d-flex flex-row align-items-center">
            <h6 className={classes.selectWarehouse}>Pilih Gudang :</h6>
            <WarehouseSelect />
          </div> */}
        </div>
      )}
    >
      <ContentItem border={false} className="mb-3">
        <Banner
          button={{ isShow: false }}
          styleType="green"
          description="Informasi"
        />
      </ContentItem>
      <ContentItem className="d-flex flex-md-row flex-column">
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={rupiahIcon} />
          <div>
            <h6 className={classes.label}>Komisi Gudang</h6>
            <h6 className={classes.value}>Rp 100.000</h6>
          </div>
        </div>
        <div
          className="d-flex align-items-center p-3"
          style={{
            flex: 1 / 3,
            borderLeft: '1px solid #E8E8E8',
            borderRight: '1px solid #E8E8E8',
          }}
        >
          <img className="mr-2" alt="" src={homeIcon} />
          <div>
            <h6 className={classes.label}>Reimburse Kurir</h6>
            <h6 className={classes.value}>Rp 100.000</h6>
          </div>
        </div>
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={peopleIcon} />
          <div>
            <h6 className={classes.label}>Reimburse Packaging</h6>
            <h6 className={classes.value}>Rp 100.000</h6>
          </div>
        </div>
      </ContentItem>
    </ContentContainer>
  );
};

export default Dashboard;
