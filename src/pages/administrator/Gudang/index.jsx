import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import WarehouseTable from './components/WarehouseTable';
import './styles.css';

const GudangAdministrator = () => {
  return (
    <>
      <ContentContainer title="Warehouse">
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <WarehouseTable />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default GudangAdministrator;
