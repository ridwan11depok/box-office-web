import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

export const getListGudang =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      // console.log('params gudang', params)
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      // const token = localStorage.getItem('access-token-jwt');
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        dispatch(setIdWarehouseSelected(id));
        response = await consume.get('warehouse', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'warehouse',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailGudang(response.result));
        } else {
          dispatch(listGudang(response.result));
        }
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
      return Promise.resolve('success');
    } catch (err) {
      if (id) {
        dispatch(detailGudang({}));
      } else {
        dispatch(listGudang({}));
      }
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
      return Promise.reject('failed');
    }
  };

const listGudang = (data) => {
  return {
    type: actionType.LIST_GUDANG,
    payload: data,
  };
};

const detailGudang = (data) => {
  return {
    type: actionType.DETAIL_GUDANG,
    payload: data,
  };
};
const setIdWarehouseSelected = (id) => {
  return {
    type: actionType.SET_ID_WAREHOUSE_SELECTED,
    payload: id,
  };
};
export const getArea =
  (provinceId = null, cityId = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id;
      if (cityId) {
        id = `${provinceId}/${cityId}`;
      } else if (provinceId) {
        id = provinceId;
      } else {
        id = null;
      }
      const response = await consume.get('area', data, headers, id);
      if (response) {
        if (cityId) {
          dispatch(setDistrict(response.result));
        } else if (provinceId) {
          dispatch(setCity(response.result));
        } else {
          dispatch(setProvince(response.result));
        }
        // if (id) {
        //   dispatch(setCity(response.result));
        // } else {
        //   dispatch(setProvince(response.result));
        // }
        dispatch(setLoading(false));
        return Promise.resolve(response);
      }
    } catch (err) {
      dispatch(setLoading(false));
    }
  };

export const setProvince = (data) => {
  return {
    type: actionType.LIST_PROVINCE,
    payload: data,
  };
};

export const setCity = (data) => {
  return {
    type: actionType.LIST_CITY,
    payload: data,
  };
};

export const setDistrict = (data) => {
  return {
    type: actionType.LIST_DISTRICT,
    payload: data,
  };
};

export const addWarehouse = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('warehouse', data, headers);
    await dispatch(getListGudang({}, response.result.id));
    dispatch(
      resultAddWarehouse({
        status: 'Success',
        result: response,
      })
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambahkan gudang baru.',
        type: 'success',
      })
    );

    return Promise.resolve(response);
  } catch (err) {
    dispatch(resultAddWarehouse({ status: 'Failed', result: err }));
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: err?.error?.message || 'Gagal menambahkan gudang baru.',
        type: 'error',
      })
    );
    return Promise.reject(err);
  }
};

export const resultAddWarehouse = (res) => {
  return {
    type: actionType.ADD_WAREHOUSE,
    payload: res,
  };
};

export const deleteWarehouse =
  (payload = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id = payload;
      const response = await consume.delete('warehouse', data, headers, id);
      dispatch(
        resultDeleteWarehouse({
          status: 'Success',
          result: response,
        })
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus gudang.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        resultDeleteWarehouse({
          status: 'Failed',
          result: err,
        })
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus gudang.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const resultDeleteWarehouse = (res) => {
  return {
    type: actionType.DELETE_WAREHOUSE,
    payload: res,
  };
};

export const getListStaffWarehouse =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('staffWarehouse', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'staffWarehouse',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailStaffWarehouse(response.result));
        } else {
          dispatch(listStaffWarehouse(response.result));
        }
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      if (id) {
        dispatch(detailStaffWarehouse({}));
      } else {
        dispatch(listStaffWarehouse({}));
      }
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
    }
  };

const listStaffWarehouse = (data) => {
  return {
    type: actionType.LIST_STAFF_WAREHOUSE,
    payload: data,
  };
};

const detailStaffWarehouse = (data) => {
  return {
    type: actionType.DETAIL_STAFF_WAREHOUSE,
    payload: data,
  };
};

const getDetailStaffWarehouse = (listStaff) => async (dispatch, getState) => {
  try {
    const { token } = getState().login;
    let data = {};
    let headers = { token: token };
    let result = [];
    for (let i = 0; i < listStaff.length; i++) {
      const response = await consume.get(
        'staffWarehouse',
        data,
        headers,
        listStaff[i].id
      );
      result = [...result, { ...response.result.user, id: response.result.id }];
    }
    return result;
  } catch (err) {}
};

export const addStaffWarehouse = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };

    const response = await consume.post('staffWarehouse', data, headers);
    dispatch(
      resultAddStaffWarehouse({
        status: 'Success',
        result: response,
      })
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambahkan staff baru.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch(resultAddStaffWarehouse({ status: 'Failed', result: err }));
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal menambahkan staff baru.',
        type: 'error',
      })
    );
    return Promise.reject(err);
  }
};

export const resultAddStaffWarehouse = (res) => {
  return {
    type: actionType.ADD_STAFF_WAREHOUSE,
    payload: res,
  };
};

export const deleteStaffWarehouse =
  (payload = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id = payload;
      const response = await consume.delete(
        'staffWarehouse',
        data,
        headers,
        id
      );
      dispatch(
        resultDeleteStaffWarehouse({
          status: 'Success',
          result: response,
        })
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus staff.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(
        resultDeleteStaffWarehouse({
          status: 'Failed',
          result: err,
        })
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus staff.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const resultDeleteStaffWarehouse = (res) => {
  return {
    type: actionType.DELETE_STAFF_WAREHOUSE,
    payload: res,
  };
};

export const updateStaffWarehouse =
  (params, id) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      const response = await consume.put(
        'staffWarehouse',
        data,
        headers,
        params,
        id
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data staff.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data staff.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

export const updateWarehouse = (payload, id) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };
    let params = {};
    const response = await consume.post('warehouse', data, headers, id);
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil memperbarui data warehouse.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal memperbarui data warehouse.',
        type: 'error',
      })
    );
    return Promise.reject(err);
  }
};
