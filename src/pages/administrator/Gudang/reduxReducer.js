import actionType from './reduxConstant';

const initialState = {
  listGudang: {},
  detailGudang: {},
  listProvince: [],
  listCity: [],
  listDistrict: [],
  addWareHouse: { status: null },
  deleteWareHouse: { status: null },
  idWarehouseSelected: null,

  listStaffWarehouse: {},
  detailStaffWarehouse: {},
  addStaffWareHouse: { status: null },
  deleteStaffWareHouse: { status: null },
};

const gudangAdministratorReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_GUDANG:
      return {
        ...prevState,
        listGudang: action.payload,
      };
    case actionType.DETAIL_GUDANG:
      return {
        ...prevState,
        detailGudang: action.payload,
      };
    case actionType.LIST_PROVINCE: {
      let provinces = action.payload.map((province) => ({
        value: province.id,
        label: province.name,
      }));
      if(provinces?.length){
        const selectAll = {value: '', label: 'Semua Provinsi'}
        provinces.unshift(selectAll)
      }
      return {
        ...prevState,
        listProvince: provinces,
      };
    }
    case actionType.LIST_CITY: {
      const cities = action.payload.map((city) => ({
        value: city.id,
        label: city.name,
      }));
      return {
        ...prevState,
        listCity: cities,
      };
    }
    case actionType.LIST_DISTRICT: {
      const districts = action.payload.map((district) => ({
        value: district.id,
        label: district.name,
      }));
      return {
        ...prevState,
        listDistrict: districts,
      };
    }
    case actionType.ADD_WAREHOUSE:
      return {
        ...prevState,
        addWareHouse: action.payload,
      };
    case actionType.DELETE_WAREHOUSE:
      return {
        ...prevState,
        deleteWareHouse: action.payload,
      };
    case actionType.LIST_STAFF_WAREHOUSE:
      return {
        ...prevState,
        listStaffWarehouse: action.payload,
      };
    case actionType.DETAIL_STAFF_WAREHOUSE:
      return {
        ...prevState,
        detailStaffWarehouse: action.payload,
      };
    case actionType.ADD_STAFF_WAREHOUSE:
      return {
        ...prevState,
        addStaffWareHouse: action.payload,
      };
    case actionType.DELETE_STAFF_WAREHOUSE:
      return {
        ...prevState,
        deleteStaffWareHouse: action.payload,
      };
    case actionType.SET_ID_WAREHOUSE_SELECTED:
      return {
        ...prevState,
        idWarehouseSelected: action.payload,
      };
    default:
      return prevState;
  }
};

export default gudangAdministratorReducer;
