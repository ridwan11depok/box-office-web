import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInfoWarehouse from './components/FormInfoWarehouse';
import FormLokasiWarehouse from './components/FormLokasiWarehouse';
import OpenDay from './components/OpenDay';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { addWarehouse, resultAddWarehouse, getListGudang } from './reduxAction';
import PhotoWarehouse from './components/PhotoWarehouse';
import { Maps } from '../../../components/elements/Maps';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});
const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);
const validationSchema = Yup.object({
  name: Yup.string().required('Required'),
  address: Yup.string().required('Required'),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Required'),
  province_id: Yup.number().required('Required'),
  city_id: Yup.number().required('Required'),
  email: Yup.string().email('Invalid email address').required('Required'),
  images: Yup.array().min(3, 'Minimal harus menyertakan 3 foto gudang'),
  delivery_day: Yup.array().min(1, 'Silahkan tentukan hari dan jam buka'),
  logo: Yup.mixed().required('Required'),
  longitude: Yup.number().required('Required'),
  latitude: Yup.number().required('Required'),
});

const AddWarehouse = () => {
  const [errors, setErrors] = useState(null);
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);

  const formik = useFormik({
    initialValues: {
      name: '',
      address: '',
      phone_number: '',
      province_id: '',
      city_id: '',
      district_id: '',
      email: '',
      images: '',
      delivery_day: '',
      logo: '',
      country_code: '62',
      longitude: '',
      latitude: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('name', values.name);
      data.append('phone_number', values.phone_number);
      data.append('email', values.email);
      data.append('address', values.address);
      data.append('province_id', values.province_id);
      data.append('city_id', values.city_id);
      data.append('district_id', values.district_id);
      data.append('logo', values.logo);
      data.append('longitude', values.longitude);
      data.append('latitude', values.latitude);
      values.images.forEach((item, idx) =>
        data.append(`images[${idx}][image]`, item)
      );
      values.delivery_day.forEach((item) =>
        data.append('delivery_day[]', item)
      );
      data.append('country code', values.country_code);

      //others
      data.append('status', '1');
      data.append('type', '1');
      data.append('status_approval', '1');
      data.append('reason', 'sample text');
      data.append('note', 'sample text');
      data.append('other_service', 'sample text');
      const res = await dispatch(addWarehouse(data));
      history.push(`/admin/warehouse/detail/${res?.result?.id}`);
    } catch (err) {
      setErrors(err?.error?.errors || null);
    }
  };
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Tambah Warehouse"
              onClick={() => history.push('/admin/warehouse')}
            />
          </div>
        )}
      >
        <ContentItem
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Info Warehouse</h6>
          </div>
          <div className="p-3">
            <FormInfoWarehouse formik={formik} errorServer={errors} />
          </div>
        </ContentItem>
        <ContentItem
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Lokasi Warehouse</h6>
          </div>
          <div className="p-3">
            <FormLokasiWarehouse formik={formik} errorServer={errors} />
          </div>
        </ContentItem>

        <ContentItem col="col-12" spaceBottom={3}>
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Hari Buka</h6>
          </div>
          <div className="p-3">
            <OpenDay formik={formik} errorServer={errors} />
          </div>
        </ContentItem>

        <ContentItem col="col-12" spaceBottom={3}>
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Foto Warehouse</h6>
          </div>
          <div className="p-3">
            <PhotoWarehouse formik={formik} errorServer={errors} />
          </div>
        </ContentItem>

        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
            onClick={() => {
              formik.resetForm();
            }}
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text={isLoading ? 'Loading...' : 'Simpan'}
            onClick={(e) => formik.handleSubmit(e)}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default AddWarehouse;
