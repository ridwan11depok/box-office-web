import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { getListGudang } from '../reduxAction';
import { SearchField } from '../../../../components/elements/InputField';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <>
      <Button
        text="Detail"
        styleType="lightBlueFill"
        onClick={() => {
          // dispatch(getListGudang({}, data.id));
          history.push(`/admin/warehouse/detail/${data.id}`);
        }}
      />
    </>
  );
};

const EmptyGudang = ({ onClick }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada gudang</h6>
      <Button onClick={onClick} text="Tambah Warehouse" styleType="blueFill" />
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleAddWarehouse, handleSearch } = props;

  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField
          placeholder="Cari Warehouse"
          onChange={(e) =>
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500)
          }
        />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType="blueFill"
          text="Tambah Warehouse"
          onClick={handleAddWarehouse}
          startIcon={() => <AddIcon />}
        />
      </div>
    </div>
  );
};

function WarehouseTable(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listGudang } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const { isLoading } = useSelector((state) => state.loading);

  const [state, setState] = useState({
    search: '',
  });

  const fetchListGudang = async (payload) => {
    dispatch(getListGudang(payload));
  };

  const handleAddWarehouse = () => {
    history.push('/admin/warehouse/add');
  };

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  return (
    <div>
      <DataTable
        action={fetchListGudang}
        totalPage={listGudang?.last_page || 5}
        params={{ name: state.search }}
        column={[
          {
            heading: 'Name Warehouse',
            key: 'name',
          },
          {
            heading: 'Nomor Telepon',
            key: 'phone_number',
          },
          {
            heading: 'Email',
            key: 'email',
          },
          {
            heading: 'Alamat',
            key: 'address',
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={listGudang?.data || []}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddWarehouse={handleAddWarehouse}
            handleSearch={handleSearch}
          />
        )}
        renderEmptyData={() => <EmptyGudang onClick={handleAddWarehouse} />}
      />
    </div>
  );
}

export default WarehouseTable;
