import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory, useParams } from 'react-router-dom';
import ModalAddStaff from './ModalAddStaff';
import ModalDetailStaff from './ModalDetailStaff';
import ModalDeleteStaff from './ModalDeleteStaff';
import { getListStaffWarehouse, deleteStaffWarehouse } from '../reduxAction';
import { SearchField } from '../../../../components/elements/InputField';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data, onClick }) => {
  return (
    <>
      <Button
        text="Detail"
        styleType="lightBlueFill"
        onClick={() => onClick(data)}
      />
    </>
  );
};

const EmptyGudang = ({ onClick }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data staff saat ini</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleAddStaff, handleSearch } = props;
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField placeholder="Cari Staff" onChange={(e) =>
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500)
          }/>
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType="blueFill"
          text="Tambah Staff"
          onClick={handleAddStaff}
        />
      </div>
    </div>
  );
};

function WarehouseDetailTable(props) {
  const dispatch = useDispatch();
  const { listStaffWarehouse, detailGudang, idWarehouseSelected } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const history = useHistory();
  const params = useParams();
  const [state, setState] = useState({
    modalAddStaff: false,
    modalDetailStaff: false,
    dataDetailStaff: {},
    modalDeleteStaff: false,
    idWarehouse: null,
    modalEditStaff: false,
    search: '',
  });

  const handleAddStaff = () => {
    setState({ ...state, modalAddStaff: true });
  };

  const handleDetailStaff = (data) => {
    setState({ ...state, modalDetailStaff: true, dataDetailStaff: data });
  };
  const handleModalDeleteStaff = (data) => {
    setState({ ...state, modalDetailStaff: false, modalDeleteStaff: true });
  };
  const handleModalEditStaff = (data) => {
    setState({ ...state, modalDetailStaff: false, modalEditStaff: true });
  };

  const fetchListStaff = async (params) => {
    dispatch(getListStaffWarehouse(params));
  };

  const handleDeleteStaff = async () => {
    try {
      await dispatch(deleteStaffWarehouse(state.dataDetailStaff.id));
      fetchListStaff({ warehouse_id: detailGudang.id });
      setState({ ...state, modalDeleteStaff: false });
    } catch (er) {}
  };
  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  return (
    <div>
      <DataTable
        action={fetchListStaff}
        totalPage={listStaffWarehouse?.last_page || 5}
        params={{
          warehouse_id: params?.id || null,
          search: state.search,
        }}
        column={[
          {
            heading: 'Nama Staff',
            key: 'name',
          },
          {
            heading: 'Email',
            key: 'email',
          },
          {
            heading: 'Role',
            key: 'role',
          },
          {
            heading: 'Nomor Telepon',
            key: 'phone_number',
          },
          {
            heading: 'Aksi',
            render: (data) => (
              <ActionField onClick={handleDetailStaff} data={data} />
            ),
          },
        ]}
        data={listStaffWarehouse?.data || []}
        transformData={(item) => ({
          ...item,
          name: item.user.name,
          email: item.user.email,
          phone_number: item.user.phone_number,
          role: `${item.user.roles.map((role) => role.display_name).join(',')}`,
        })}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddStaff={handleAddStaff}
            handleSearch={handleSearch}
          />
        )}
        renderEmptyData={() => <EmptyGudang onClick={handleAddStaff} />}
      />
      <ModalAddStaff
        show={state.modalAddStaff}
        onHide={() => setState({ ...state, modalAddStaff: false })}
        onAgree={() => setState({ ...state, modalAddStaff: false })}
      />
      <ModalDetailStaff
        show={state.modalDetailStaff}
        onHide={() => setState({ ...state, modalDetailStaff: false })}
        data={state.dataDetailStaff}
        handleDeleteStaff={handleModalDeleteStaff}
        handleEditStaff={handleModalEditStaff}
      />
      <ModalDeleteStaff
        show={state.modalDeleteStaff}
        onHide={() => setState({ ...state, modalDeleteStaff: false })}
        onAgree={handleDeleteStaff}
        data={state.dataDetailStaff}
      />
      <ModalAddStaff
        show={state.modalEditStaff}
        onHide={() => setState({ ...state, modalEditStaff: false })}
        initialValues={state.dataDetailStaff}
      />
    </div>
  );
}

export default WarehouseDetailTable;
