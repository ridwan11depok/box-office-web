import React, { useState } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../../../components/elements/Button';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';
import gudang from '../../../../assets/icons/profilegudang.svg';
import { useSelector } from 'react-redux';
import MapModal from '../../../../components/elements/Map/MapModal';

const useStyles = makeStyles({
  titleWarehouse: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#1c1c1c',
    marginLeft: '5px',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  btnBlueNoFill: {
    marginRight: 0,
    paddingLeft: 0,
  },
});

function DetailGudang(props) {
  const [showMap, setShowMap] = useState(false);
  const classes = useStyles();
  const { detailGudang } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  return (
    <div>
      <Item border={false} spaceBottom={3} className="d-flex">
        <img alt="gudang photo" src={gudang} />
        <h6 className={classes.titleWarehouse}>
          {detailGudang?.name || 'No Data'}
        </h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Nomor Telepon</h6>
        <h6 className={classes.value}>
          {detailGudang?.phone_number || 'No Data'}
        </h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Email</h6>
        <h6 className={classes.value}>{detailGudang?.email || 'No Data'}</h6>
      </Item>
      <Item border={false} spaceBottom={3}>
        <h6 className={classes.label}>Alamat</h6>
        <h6 className={classes.value}>{detailGudang?.address || 'No Data'}</h6>
        <Button
          styleType="blueNoFill"
          className={classes.btnBlueNoFill}
          text="Lihat Lokasi di Peta"
          startIcon={() => <MapOutlinedIcon />}
          onClick={() => setShowMap(true)}
        />
      </Item>
      <MapModal
        show={showMap}
        onHide={() => setShowMap(false)}
        mode="viewer"
        initialCoordinate={{
          lng: detailGudang.longitude,
          lat: detailGudang.latitude,
        }}
      />
    </div>
  );
}

export default DetailGudang;
