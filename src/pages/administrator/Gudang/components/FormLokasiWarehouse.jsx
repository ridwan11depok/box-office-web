import React, { useEffect, useState } from 'react';
import {
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { getArea, setCity, setDistrict } from '../reduxAction';
import MapModal from '../../../../components/elements/Map/MapModal';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormLokasiWarehouse(props) {
  const classes = useStyles();
  const { formik } = props;
  const [state, setState] = useState({
    provice_id: 0,
    city_id: 0,
    district_id: 0,
    showMap: false,
  });
  const dispatch = useDispatch();
  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);
  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));      

  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(state.provice_id, Number(e.target.value)));
  };
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  }

  useEffect(() => {
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('address', init.address);
      formik.setFieldValue('province_id', init.province_id);
      formik.setFieldValue('city_id', init.city_id);
      formik.setFieldValue('district_id', init.district_id);
      formik.setFieldValue('longitude', init.longitude);
      formik.setFieldValue('latitude', init.latitude);
    }
  }, [props.initialValues]);

  const selectCoordinate = (coordinate) => {
    formik.setFieldValue('longitude', coordinate.lng);
    // formik.setFieldTouched('longitude', true);
    formik.setFieldValue('latitude', coordinate.lat);
    // formik.setFieldTouched('latitude', true);
    setState({ ...state, showMap: false });
  };
  return (
    <div>
      <div className="background-map mb-3">
        <div
          className="d-flex flex-column flex-lg-row justify-content-between align-items-center w-100 p-3"
          style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
        >
          <span
            style={{
              fontSize: '14px',
              fontWeight: '600',
              fontFamily: 'Open Sans',
            }}
          >
            Tandai lokasi warehouse Anda.
          </span>
          <Button
            style={{ width: '200px', backgroundColor: '#FAFAFF' }}
            styleType="blueNoFill"
            text="Tandai Lokasi"
            onClick={() => setState({ ...state, showMap: true })}
          />
        </div>
      </div>
      {formik.touched.latitude && formik.errors.latitude ? (
        <Form.Text className="text-input-error mb-3 mt-0">
          {formik.errors.latitude}
        </Form.Text>
      ) : props.errorServer?.latitude?.[0] ||
        props.errorServer?.longitude?.[0] ? (
        <Form.Text className="text-input-error mb-3 mt-0">
          {props.errorServer?.latitude?.[0] ||
            props.errorServer?.longitude?.[0]}
        </Form.Text>
      ) : null}
      <TextArea
        className="mb-3"
        label="Alamat"
        placeholder="Input alamat"
        rows={3}
        withValidate
        name="address"
        formik={formik}
        withValidate
        errorServer={props.errorServer?.address?.[0] || ''}
      />
      <Row>
        <Col>
          <ReactSelect
            onChange={handleChangeProvince}
            value={state.provice_id}
            label="Provinsi"
            options={listProvince}
            name="province_id"
            formik={formik}
            withValidate
            placeholder="Pilih Provinsi"
            errorServer={props.errorServer?.province_id?.[0] || ''}
          />
        </Col>
        <Col>
          <ReactSelect
            onChange={handleChangeCity}
            value={state.city_id === 0 && ''}
            label="Kota"
            name="city_id"
            formik={formik}
            withValidate
            options={listCity}
            placeholder="Pilih Kota"
            errorServer={props.errorServer?.city_id?.[0] || ''}
          />
        </Col>
        <Col>
          <ReactSelect
            onChange={handleChangeDistrict}
            value={state.district_id === 0 && ''}
            label="Kecamatan"
            name="district_id"
            formik={formik}
            withValidate
            options={listDistrict}
            placeholder="Pilih Kota"
            errorServer={props.errorServer?.district_id?.[0] || ''}
          />
        </Col>
      </Row>
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        onAgree={selectCoordinate}
        initialCoordinate={{
          lng: props.initialValues?.longitude,
          lat: props.initialValues?.latitude,
        }}
      />
    </div>
  );
}

FormLokasiWarehouse.defaultProps = {
  initialValues: null,
  errorServer: null,
};

export default FormLokasiWarehouse;
