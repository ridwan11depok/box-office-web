import React, { useState, useEffect } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { validateFile } from '../../../../utils/validate';

const allowedExtentions = ['image/png', 'image/jpg', 'image/jpeg'];

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  imageContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  imageContent: {
    width: '150px',
    height: '150px',
    border: '2px dashed #CFCFCF',
    backgroundColor: '#F2F2F2',
    marginBottom: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '4px',
    overflow: 'hidden',
    cursor: 'pointer',
    position: 'relative',
    marginRight: '10px',
  },
  imageSelected: {
    width: '150px',
    height: 'auto',
  },
  deleteIconContainer: {
    position: 'absolute',
    right: '10px',
    bottom: '10px',
    padding: '3px',
    backgroundColor: '#FFFFFF',
    paddingLeft: '6px',
    paddingRight: '6px',
    zIndex: 5,
    borderRadius: '4px',
  },
  deleteIcon: {
    color: '#192A55',
    fontSize: '14px',
  },
});

function PhotoWarehouse(props) {
  const classes = useStyles();
  const { formik } = props;
  const [state, setState] = useState({
    firstImage: '',
    secondImage: '',
    thirdImage: '',
    fourthImage: '',
    fifthImage: '',
  });
  const firstImage = React.createRef();
  const secondImage = React.createRef();
  const thirdImage = React.createRef();
  const fourthImage = React.createRef();
  const fifthImage = React.createRef();
  const refs = { firstImage, secondImage, thirdImage, fourthImage, fifthImage };
  const handleChangeFile = async (e, keyState) => {
    const files = e.target.files;
    const response = await validateFile({
      files,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    });
    if (response) {
      if (state[keyState] === '') {
        setState({
          ...state,
          [keyState]: response?.file,
        });
      }
    }
  };
  const handleDeleteFile = (keyState) => {
    setState({
      ...state,
      [keyState]: '',
    });
  };
  useEffect(() => {
    const images = Object.keys(state).map((key) => {
      if (state[key] !== '') {
        return state[key];
      }
    });
    let newImages = [];
    for (let i = 0; i < images.length; i++) {
      if (images[i]) {
        newImages[i] = images[i];
      }
    }
    formik.setFieldValue('images', newImages);
  }, [
    state.firstImage,
    state.secondImage,
    state.thirdImage,
    state.fourthImage,
    state.fifthImage,
  ]);
  return (
    <>
      <Row>
        <Col xs={12} className="mb-3">
          <h6 className={classes.text}>
            Format gambar .jpg .jpeg .png dan ukuran minimum 300 x 300px (Untuk
            gambar optimal gunakan ukuran minimum 700 x 700 px)
          </h6>
        </Col>
        <Col xs={12}>
          <div className={classes.imageContainer}>
            {Object.keys(refs).map((key) => (
              <div className="d-flex flex-column">
                <div
                  className={classes.imageContent}
                  onClick={() => {
                    state[key] === '' && refs[key].current.click();
                  }}
                >
                  {state[key] ? (
                    <>
                      <img
                        className={classes.imageSelected}
                        src={URL.createObjectURL(state[key])}
                        alt=""
                      />
                      <div
                        className={classes.deleteIconContainer}
                        onClick={() => handleDeleteFile(key)}
                      >
                        <i
                          className={'far fa-trash-alt ' + classes.deleteIcon}
                        ></i>
                      </div>
                    </>
                  ) : (
                    <img src={pictureIcon} alt="" />
                  )}
                </div>
              </div>
            ))}
          </div>
        </Col>
        <Col xs={12}>
          {formik.errors.images && formik.touched.images ? (
            <Form.Text className="text-input-error">
              {formik.errors.images}
            </Form.Text>
          ) : props.errorServer?.images?.[0] ? (
            <Form.Text className="text-input-error">
              {props.errorServer?.images?.[0]}
            </Form.Text>
          ) : null}
        </Col>
      </Row>
      {Object.keys(refs).map((key) => (
        <input
          onChange={(e) => handleChangeFile(e, key)}
          ref={refs[key]}
          type="file"
          className="d-none"
          value={state[key]?.filename || ''}
          accept={allowedExtentions.join(',')}
        />
      ))}
    </>
  );
}

PhotoWarehouse.defaultProps = {
  errorServer: null,
};

export default PhotoWarehouse;
