import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { deleteWarehouse } from '../reduxAction';
import { useHistory } from 'react-router-dom';

const ModalConfirmDelete = (props) => {
  const { detailGudang } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const { isLoading } = useSelector((state) => state.loading);
  const dispatch = useDispatch();
  const history = useHistory();
  const handleDelete = async () => {
    try {
      const res = await dispatch(deleteWarehouse(detailGudang.id));
      props.onHide();
      history.push('/admin/warehouse');
    } catch (err) {
      props.onHide();
    }
  };
  return (
    <>
      <Modal
        titleModal="Hapus Warehouse"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={detailGudang}
        bodyStyle={{ border: 'none' }}
        content={() => (
          <>
            <h6
              className="mt-3 mb-3"
              style={{
                fontSize: '14px',
                fontWeight: '400',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
              }}
            >
              {`Apakah anda yakin akan menghapus "${
                detailGudang?.name || ''
              }" dari warehouse?`}
            </h6>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text={isLoading ? 'Loading...' : 'Hapus'}
              onClick={handleDelete}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

export default ModalConfirmDelete;
