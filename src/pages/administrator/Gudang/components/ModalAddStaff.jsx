import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form, Row, Col } from 'react-bootstrap';
import {
  PasswordField,
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import { useSelector, useDispatch } from 'react-redux';
import {
  addStaffWarehouse,
  getListStaffWarehouse,
  updateStaffWarehouse,
} from '../reduxAction';
import { hex_md5 } from '../../../../utils/md5';

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);
const validationSchemaAdd = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama staff'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Harap untuk mengisi alamat email'),
  role: Yup.string().required('Harap untuk memilih role staff'),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon'),
  password: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .required('Harap untuk mengisi password'),
  password_confirmation: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .oneOf([Yup.ref('password'), null], 'Password tidak sama')
    .required('Harap untuk mengisi ulang password'),
  address: Yup.string().required('Harap untuk mengisi alamat staff'),
});

const validationSchemaEdit = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama staff'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Harap untuk mengisi alamat email'),
  role: Yup.string().required('Harap untuk memilih role staff'),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon'),
  address: Yup.string().required('Harap untuk mengisi alamat staff'),
});

const ModalAddStaff = (props) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState(null);
  const { detailGudang } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const { isLoading } = useSelector((state) => state.loading);
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      role: 'Admin Staff',
      phone_number: '',
      password: '',
      password_confirmation: '',
      address: '',
      country_code: '62',
    },
    validationSchema: props.initialValues
      ? validationSchemaEdit
      : validationSchemaAdd,
    onSubmit: (values) => {
      const payload = {
        ...values,
        warehouse_id: detailGudang.id,
      };
      if (values.password && values.password_confirmation) {
        payload.password = hex_md5(values.password);
        payload.password_confirmation = hex_md5(values.password_confirmation);
      } else {
        delete payload.password;
        delete payload.password_confirmation;
      }
      handleSubmitForm(payload);
    },
  });

  const handleSubmitForm = async (payload) => {
    try {
      if (props.initialValues) {
        await dispatch(updateStaffWarehouse(payload, props.initialValues.id));
      } else {
        await dispatch(addStaffWarehouse(payload));
      }
      dispatch(
        getListStaffWarehouse({
          page: 1,
          per_page: 10,
          warehouse_id: detailGudang.id,
        })
      );
      props.onHide();
    } catch (err) {
      setErrors(err?.error?.errors || null);
    }
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
      setErrors(null);
    }
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('email', init.email);
      formik.setFieldValue('phone_number', init.phone_number);
      formik.setFieldValue('address', init.address);
      formik.setFieldValue('country_code', init.country_code);
    }
  }, [props.show, props.initialValues]);
  return (
    <>
      <Modal
        titleModal={props.initialValues ? 'Edit Staff' : 'Tambah Staff'}
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        content={(data) => (
          <>
            <TextField
              className="mt-3"
              label="Nama Staff"
              placeholder="Input nama staff"
              name="name"
              withValidate
              formik={formik}
              errorServer={errors?.name?.[0] || ''}
            />
            <TextField
              label="Email Address"
              placeholder="Input email address"
              name="email"
              withValidate
              formik={formik}
              errorServer={errors?.email?.[0] || ''}
            />
            <Form.Group className="mt-2" controlId="formBasicEmail">
              <Form.Label as={'h6'} className="input-label">
                Role
              </Form.Label>
              <Form.Text style={{ color: '#4F4F4F' }}>
                Silahkan pilih role sesuai dengan personil yang ditambahkan
              </Form.Text>
              <div className="d-flex flex-row">
                {[
                  { name: 'Admin Staff', key: 1 },
                  { name: 'Marketing', key: 2 },
                  { name: 'Finance', key: 3 },
                ].map((item) => (
                  <div
                    onClick={() => {
                      formik.setFieldValue('role', item.name);
                    }}
                    className="text-center d-flex align-items-center justify-content-center mt-2 mr-2"
                    style={{
                      width: '120px',
                      height: '40px',
                      borderRadius: '40px',
                      border:
                        formik.values.role !== item.name && '2px solid #828282',
                      color:
                        formik.values.role === item.name
                          ? '#FFFFFF'
                          : '#1C1C1C',
                      backgroundColor:
                        formik.values.role === item.name && '#192A55',
                      fontSize: '14px',
                      fontFamily: 'Open Sans',
                      fontWeight: '700',
                      cursor: 'pointer',
                    }}
                  >
                    {item.name}
                  </div>
                ))}
              </div>
              {formik.errors.role && formik.touched.role && (
                <Form.Text className="text-input-error">
                  {formik.errors.role}
                </Form.Text>
              )}
            </Form.Group>
            <Row>
              <Col>
                <SelectField
                  label="Nomor Telepon"
                  options={[
                    {
                      value: '62',
                      description: 'Indonesia(+62)',
                    },
                  ]}
                  name="country_code"
                  formik={formik}
                  withValidate
                />
              </Col>
              <Col className="d-flex align-items-end">
                <TextField
                  className="w-100"
                  placeholder="Input nomor telepon"
                  name="phone_number"
                  formik={formik}
                  withValidate
                  withError={false}
                />
              </Col>
              <Col xs={6}></Col>
              <Col xs={6} style={{ marginTop: '-7px', marginBottom: '5px' }}>
                {formik.errors.phone_number && formik.touched.phone_number ? (
                  <Form.Text className="text-input-error">
                    {formik.errors.phone_number}
                  </Form.Text>
                ) : errors?.phone_number?.[0] ? (
                  <Form.Text className="text-input-error">
                    {errors?.phone_number?.[0] || ''}
                  </Form.Text>
                ) : null}
              </Col>
            </Row>
            <PasswordField
              label="Password"
              placeholder="Input password"
              name="password"
              withValidate
              formik={formik}
              errorServer={errors?.password?.[0] || ''}
            />
            <PasswordField
              label="Masukkan Ulang Password"
              placeholder="Input password"
              name="password_confirmation"
              withValidate
              formik={formik}
              errorServer={errors?.password_confirmation?.[0] || ''}
            />
            <TextArea
              className="mb-3"
              label="Alamat"
              placeholder="Input alamat"
              rows={3}
              withValidate
              name="address"
              formik={formik}
              errorServer={errors?.address?.[0] || ''}
            />
            {/* {errors &&
              Object.keys(errors).map((key) => (
                <Form.Text className="text-input-error">
                  {errors[key][0]}
                </Form.Text>
              ))} */}
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                if (!Boolean(formik.values.role)) {
                  formik.setFieldTouched('role', true);
                }
                formik.handleSubmit(e);
              }}
              // disabled={formik.isSubmitting}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading
                ? 'Loading...'
                : props.initialValues
                ? 'Perbarui'
                : 'Tambah'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalAddStaff.defaultProps = {
  initialValues: null,
};
export default ModalAddStaff;
