import React from 'react';
import { ContentItem as Item } from '../../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const useStyles = makeStyles({
  title: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#1c1c1c',
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
    width: '30%',
  },
});

function DetailGudang(props) {
  const classes = useStyles();
  const { detailGudang } = useSelector(
    (state) => state.gudangAdministratorReducer
  );
  const listDeliveryDay = detailGudang?.delivery_day
    ? JSON.parse(detailGudang?.delivery_day).map((item) => ({
        day: item[0].charAt(0).toUpperCase() + item[0].slice(1),
        time: `${item[1]} - ${item[2]}`,
      }))
    : [];

  // React.useEffect(() => {
  //   console.log(JSON.parse(detailGudang?.delivery_day));
  // }, [detailGudang]);
  return (
    <div>
      <Item border={false} spaceBottom={3} className="d-flex">
        <h6 className={classes.title}>Hari Pengiriman</h6>
      </Item>
      {listDeliveryDay.map((item) => (
        <Item
          key={item.day}
          col="col-12"
          border={false}
          spaceBottom={1}
          className="d-flex"
        >
          <h6 className={classes.value}>{item.day}</h6>
          <h6 className={classes.value} style={{ width: '70%' }}>
            {item.time}
          </h6>
        </Item>
      ))}
    </div>
  );
}

export default DetailGudang;
