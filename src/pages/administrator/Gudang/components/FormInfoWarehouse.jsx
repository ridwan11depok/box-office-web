import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { Col, Form, Row } from 'react-bootstrap';
import {
  TextField,
  SelectField,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import { validateFile } from '../../../../utils/validate';

const allowedExtentions = ['image/png', 'image/jpg', 'image/jpeg'];

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

function FormInfoWarehouse(props) {
  const classes = useStyles();
  const [state, setState] = useState({ image: '' });
  const inputImage = React.createRef();
  const { formik } = props;
  const handleChangeImage = async (e) => {
    const files = e.target.files;
    const response = await validateFile({
      files,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    });
    if (response) {
      setState({ ...state, image: e.target.files[0] });
      formik.setFieldValue('logo', response?.file);
    }
  };

  useEffect(() => {
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('phone_number', init.phone_number);
      formik.setFieldValue('email', init.email);
    }
  }, [props.initialValues]);
  return (
    <>
      <Container withHeader={false}>
        <Item border={false} spaceBottom="3">
          <Form.Group className="mb-1" controlId="formBasicEmail">
            <Form.Label as={'h6'} className="input-label">
              Logo Warehouse
            </Form.Label>
            <div className="row">
              <div className="col-12 col-lg-4">
                <div
                  className="rounded d-flex justify-content-center align-items-center"
                  style={{
                    width: '100px',
                    height: '100px',
                    border: '2px dashed #CFCFCF',
                    backgroundColor: '#F2F2F2',
                    marginTop: '7px',
                    overflow: 'hidden',
                  }}
                >
                  {state.image || props.initialValues?.logo ? (
                    <img
                      style={{ width: '100px' }}
                      src={
                        state.image
                          ? URL.createObjectURL(state.image)
                          : props.initialValues?.logo_full_url || ''
                      }
                      alt=""
                    />
                  ) : (
                    <img src={pictureIcon} alt="" />
                  )}
                </div>
              </div>
              <div className="col-12 col-lg-8 pl-3">
                <div
                  className="text-justify"
                  style={{
                    fontWeight: '400',
                    color: '#4F4F4F',
                    fontSize: '14px',
                  }}
                >
                  <p>
                    Ukuran optimal 300 x 300 piksel dengan Besar file: Maksimum
                    10.000.000 bytes (10 Megabytes). Ekstensi file yang
                    diperbolehkan: JPG, JPEG, PNG
                  </p>
                  <Button
                    styleType="blueOutline"
                    onClick={() => inputImage.current.click()}
                    text={state.image ? 'Ganti Foto' : 'Pilih Foto'}
                    style={{ width: '100%', maxWidth: '296px', height: '32px' }}
                  />
                </div>
              </div>
            </div>
            {formik.touched.logo && formik.errors.logo ? (
              <Form.Text className="text-input-error">
                {formik.errors.logo}
              </Form.Text>
            ) : props.errorServer?.logo?.[0] ? (
              <Form.Text className="text-input-error">
                {props.errorServer?.logo?.[0]}
              </Form.Text>
            ) : null}
          </Form.Group>
        </Item>
        <Item border={false}>
          <TextField
            label="Nama Warehouse"
            placeholder="Input nama warehouse"
            name="name"
            formik={formik}
            withValidate
            errorServer={props.errorServer?.name?.[0] || ''}
          />
          <Row>
            <Col>
              <SelectField
                label="Nomor Telepon"
                options={[
                  {
                    value: '62',
                    description: 'Indonesia(+62)',
                  },
                ]}
                name="country_code"
                formik={formik}
                withValidate
              />
            </Col>
            <Col className="d-flex align-items-end">
              <TextField
                className="w-100"
                placeholder="Input nomor telepon"
                name="phone_number"
                formik={formik}
                withValidate
                withError={false}
              />
            </Col>
            <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
              {formik.errors.phone_number && formik.touched.phone_number ? (
                <Form.Text className="text-input-error">
                  {formik.errors.phone_number}
                </Form.Text>
              ) : props.errorServer?.phone_number?.[0] ? (
                <Form.Text className="text-input-error">
                  {props.errorServer?.phone_number?.[0]}
                </Form.Text>
              ) : null}
            </Col>
          </Row>
          <TextField
            label="Email"
            placeholder="Input email warehouse"
            name="email"
            formik={formik}
            withValidate
            errorServer={props.errorServer?.email?.[0] || ''}
          />
        </Item>
      </Container>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={inputImage}
        type="file"
        className="d-none"
        accept={allowedExtentions.join(',')}
      />
    </>
  );
}
FormInfoWarehouse.defaultProps = {
  initialValues: null,
  errorServer: null,
};

export default FormInfoWarehouse;
