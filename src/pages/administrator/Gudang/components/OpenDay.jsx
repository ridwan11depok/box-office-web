import React, { useState, useEffect } from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import { SelectField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import { time } from './time';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
});
const listDay = [
  'Senin',
  'Selasa',
  'Rabu',
  'Kamis',
  "Jum'at",
  'Sabtu',
  'Minggu',
];
const listSchedule = {
  day: listDay,
  open: time.map((item, idx) => ({ value: item, description: item })),
  close: time.map((item, idx) => ({ value: item, description: item })),
};
function OpenDay(props) {
  const classes = useStyles();
  const { formik } = props;
  const [state, setState] = useState({
    day: [],
    schedule: [listSchedule],
    open: ['07:00'],
    close: ['23:00'],
  });

  const handleAddSchedule = () => {
    setState({
      ...state,
      schedule: [...state.schedule, listSchedule],
      open: [...state.open, '07:00'],
      close: [...state.close, '23:00'],
    });
  };

  const handleSelectOpen = (idx, value) => {
    const newOpen = [...state.open];
    const newDay = state.day.map((item) => {
      if (item.idx === idx) {
        return {
          ...item,
          open: value,
        };
      } else {
        return item;
      }
    });
    newOpen[idx] = value;
    setState({ ...state, open: newOpen, day: newDay });
  };

  const handleSelectClose = (idx, value) => {
    const newDay = state.day.map((item) => {
      if (item.idx === idx) {
        return {
          ...item,
          close: value,
        };
      } else {
        return item;
      }
    });
    const newClose = [...state.close];
    newClose[idx] = value;
    setState({ ...state, close: newClose, day: newDay });
  };

  const handleSelectDay = (day, idx) => {
    let checkAvailableBaseIndex = state.day.filter(
      (item) => item.day === day && idx === item.idx
    );
    let checkAvailable = state.day.filter((item) => item.day === day);
    if (checkAvailableBaseIndex.length) {
      let newDay = state.day.filter((item) => item.day !== day);
      setState({ ...state, day: newDay });
    } else if (checkAvailable.length) {
      let newDay = state.day.map((item) => {
        if (item.day === day) {
          return {
            ...item,
            open: state.open[idx],
            close: state.close[idx],
            idx: idx,
          };
        } else {
          return item;
        }
      });
      setState({ ...state, day: newDay });
    } else {
      setState({
        ...state,
        day: [
          ...state.day,
          {
            day: day,
            open: state.open[idx],
            close: state.close[idx],
            idx,
            id: listDay.indexOf(day),
          },
        ],
      });
    }
  };

  const setSelectedIndicator = (day, idx) => {
    const selected = state.day.filter(
      (item) => item.day === day && item.idx === idx
    );
    if (selected.length) {
      return 'lightBlueFillOutline';
    } else {
      return 'greyOutline';
    }
  };

  useEffect(() => {
    const deliveryDay = state.day
      .sort((a, b) => a.id - b.id)
      .map((item) => `${item.day},${item.open},${item.close}`);
    formik.setFieldValue('delivery_day', deliveryDay);
  }, [state.day.length]);

  useEffect(() => {
    if (props.initialValues && props.initialValues?.delivery_day) {
      let initDay = JSON.parse(props.initialValues?.delivery_day).map(
        (item) => `${item[0]},${item[1]},${item[2]}`
      );
      formik.setFieldValue('delivery_day', initDay);
    }
  }, [props.initialValues]);
  return (
    <Row>
      <Col xs={12} className="mb-3">
        <h6 className={classes.text}>
          Tentukan hari dan jam berapa warehouse Anda buka
        </h6>
      </Col>
      {state.schedule.map((item, idx) => (
        <>
          <Col md={8} xs={12} className="d-flex align-items-end pb-2 flex-wrap">
            {item.day.map((day) => (
              <Button
                onClick={() => handleSelectDay(day, idx)}
                key={day}
                text={day}
                styleType={setSelectedIndicator(day, idx)}
                style={{
                  borderRadius: '40px',
                  marginRight: '3px',
                  marginBottom: '3px',
                }}
              />
            ))}
          </Col>
          <Col md={2} xs={3}>
            <SelectField
              value={state.open[idx]}
              onChange={(e) => {
                handleSelectOpen(idx, e.target.value);
              }}
              label="Jam Buka"
              options={item.open}
            />
          </Col>
          <Col md={2} xs={3}>
            <SelectField
              value={state.close[idx]}
              onChange={(e) => {
                handleSelectClose(idx, e.target.value);
              }}
              label="Jam Tutup"
              options={item.close}
            />
          </Col>
        </>
      ))}
      <Col xs={12} className="d-flex justify-content-end">
        <Button
          onClick={handleAddSchedule}
          text="Tambah Jadwal"
          styleType={
            state.day.length === 7 ? 'blueOutlineDisabled' : 'blueOutline'
          }
          disabled={state.day.length === 7}
        />
      </Col>
      <Col xs={12} className="mt-3">
        {props.initialValues && <h6>Daftar hari buka saat ini</h6>}
        <div className="row">
          {props.initialValues?.delivery_day &&
            JSON.parse(props.initialValues?.delivery_day).map((item) => (
              <>
                <div className="col-2">{item[0]}</div>
                <div className="col-10">
                  {item[1]}-{item[2]}
                </div>
              </>
            ))}
        </div>
      </Col>
      <Col xs={12}>
        {formik.errors.delivery_day && formik.touched.delivery_day ? (
          <Form.Text className="text-input-error">
            {formik.errors.delivery_day}
          </Form.Text>
        ) : props.errorServer?.delivery_day?.[0] ? (
          <Form.Text className="text-input-error">
            {props.errorServer?.delivery_day?.[0]}
          </Form.Text>
        ) : null}
      </Col>
    </Row>
  );
}

OpenDay.defaultProps = {
  initialValues: null,
  errorServer: null,
};
export default OpenDay;
