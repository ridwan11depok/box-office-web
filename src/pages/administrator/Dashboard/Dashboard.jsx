import React, { useState } from 'react';
import { Row, Col, ProgressBar } from 'react-bootstrap';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import warehouse from '../../../assets/dashboard/warehouse.svg';
import category from '../../../assets/dashboard/category.svg';
import store from '../../../assets/dashboard/store.svg';
import logo from '../../../assets/dashboard/logo.svg';
import pickup from '../../../assets/dashboard/pickup.svg';
import done from '../../../assets/dashboard/done.svg';
import finish from '../../../assets/dashboard/finish.svg';
import packing from '../../../assets/dashboard/packing.svg';
import packaging from '../../../assets/dashboard/packaging.svg';
import sicepat from '../../../assets/dashboard/sicepat.svg';
import jnt from '../../../assets/dashboard/jnt.svg';
import ninja from '../../../assets/dashboard/ninja.svg';
import tiki from '../../../assets/dashboard/tiki.svg';
import grab from '../../../assets/dashboard/grab.svg';
import gosend from '../../../assets/dashboard/gosend.svg';
import rupiahIcon from '../../../assets/icons/rupiah.svg';
import peopleIcon from '../../../assets/icons/people.svg';
import { ReactSelect } from '../../../components/elements/InputField';
import { DatePicker } from '../../../components/elements/InputField';
import ProgresBar from './components/ProgresBar';
import './components/style.css'

const useStyles = makeStyles({
  value: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
    marginBottom: 0,
  },
  label: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  status: {
    background: "#EDF7EE",
    padding: "0.5rem!important",
    color: "#1B5E20",
    width: "fit-content",
    borderRadius: "40%",
    marginTop: "5px",
  },
});

const testData = [
  { backgroundColor: "#0D152B", completed: 50 },
];

const Dashboard = (props) => {
  const classes = useStyles();

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
          <PageTitle title="Dashboard" />
        </div>
      )}
    >
      <ContentItem className="d-flex flex-md-row flex-column">
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={warehouse} />
          <div>
            <h6 className={classes.label}>Total Warehouse</h6>
            <h6 className={classes.value}>10</h6>
          </div>
        </div>
        <div
          className="d-flex align-items-center p-3"
          style={{
            flex: 1 / 3,
            borderLeft: '1px solid #E8E8E8',
            borderRight: '1px solid #E8E8E8',
          }}
        >
          <img className="mr-2" alt="" src={category} />
          <div>
            <h6 className={classes.label}>Total Category</h6>
            <h6 className={classes.value}>10</h6>
          </div>
        </div>
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={store} />
          <div>
            <h6 className={classes.label}>Total Store</h6>
            <h6 className={classes.value}>1.200</h6>
          </div>
        </div>
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3, borderLeft: '1px solid #E8E8E8' }}>
          <img className="mr-2" alt="" src={packaging} />
          <div>
            <h6 className={classes.label}>Total Packaging</h6>
            <h6 className={classes.value}>20</h6>
          </div>
        </div>
      </ContentItem>
      <ContentItem className="d-flex flex-md-row flex-column mt-3">
        <div className="d-flex flex-column w-100">
            <Row className="p-3">
                <Col xs={12} md={6} lg={4} className="mb-lg-2 mb-lg-0 mb-2">
                    <ReactSelect
                        label="Pilih Gudang"
                        placeholder="warehouse 01"
                        // options={listStore}
                    />
                </Col>
                <Col xs={12} md={6} lg={4} className="mb-lg-0 mb-2 mt-4">
                    <ReactSelect
                        placeholder="Bulanan"
                        // options={listVendor}
                    />
                </Col>
                <Col xs={12} md={6} lg={4} className="align-items-end mb-2 mt-4">
                  <DatePicker
                    onDateChange={props.handleFilterDate}
                    format="DD MMMM yyyy"
                    prefix
                    suffix={false}
                  />
                </Col>
            </Row>
        </div>
      </ContentItem>
      <ContentItem className="d-flex flex-md-row flex-column mt-3">
        <div className="d-flex p-3">
          <img className="mr-2" alt="" src={logo} />
          <div className="ml-2">
            <h6 className={classes.label}>Nama Warehouse</h6>
            <h6 className={classes.value}>Warehouse 01</h6>
            <div className={classes.status}>Verified</div>
          </div>
        </div>
      </ContentItem>
      <ContentItem className="d-flex flex-md-row flex-column mt-3">
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={rupiahIcon} />
          <div>
            <h6 className={classes.label}>Penghasilan Gudang</h6>
            <h6 className={classes.value}>Rp. 52.000.000</h6>
          </div>
        </div>
        <div
          className="d-flex align-items-center p-3"
          style={{
            flex: 1 / 3,
            borderLeft: '1px solid #E8E8E8',
            borderRight: '1px solid #E8E8E8',
          }}
        >
          <img className="mr-2" alt="" src={peopleIcon} />
          <div>
            <h6 className={classes.label}>Reimburse Kurir</h6>
            <h6 className={classes.value}>Rp. 550.000</h6>
          </div>
        </div>
        <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
          <img className="mr-2" alt="" src={packaging} />
          <div>
            <h6 className={classes.label}>Reimburse Packaging</h6>
            <h6 className={classes.value}>Rp. 200.000</h6>
          </div>
        </div>
      </ContentItem>
      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        className="mt-3"
      >
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="mb-0">Inbound Summary</h6>
        </div>
        <div className="row unrows">
          <div className="col-lg-6 border-right uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={packaging} />
              <div>
                <h6 className={classes.label}>Total Inbound</h6>
                <h6 className={classes.value}>1000</h6>
              </div>
            </div>
          </div>
          <div className="col-lg-6 uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={pickup} />
              <div>
                <h6 className={classes.label}>Pickup</h6>
                <h6 className={classes.value}>850</h6>
              </div>
            </div>
          </div>
        </div>
        <div className="row no-border">
          <div className="col-lg-6 border-right uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={finish} />
              <div>
                <h6 className={classes.label}>Sampai Gudang</h6>
                <h6 className={classes.value}>900</h6>
              </div>
            </div>
          </div>
          <div className="col-lg-6 uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={done} />
              <div>
                <h6 className={classes.label}>Selesai</h6>
                <h6 className={classes.value}>800</h6>
              </div>
            </div>
          </div>
        </div>
      </ContentItem>
      <ContentItem
        spaceLeft="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        className="mt-3"
      >
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="mb-0">Outbound Summary</h6>
        </div>
        <div className="row unrows">
          <div className="col-lg-6 border-right uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={packaging} />
              <div>
                <h6 className={classes.label}>Total Outbound</h6>
                <h6 className={classes.value}>1000</h6>
              </div>
            </div>
          </div>
          <div className="col-lg-6 uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={packing} />
              <div>
                <h6 className={classes.label}>Packing</h6>
                <h6 className={classes.value}>800</h6>
              </div>
            </div>
          </div>
        </div>
        <div className="row no-border">
          <div className="col-lg-6 border-right uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={pickup} />
              <div>
                <h6 className={classes.label}>Shipping</h6>
                <h6 className={classes.value}>900</h6>
              </div>
            </div>
          </div>
          <div className="col-lg-6 uncols">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={done} />
              <div>
                <h6 className={classes.label}>Selesai</h6>
                <h6 className={classes.value}>800</h6>
              </div>
            </div>
          </div>
        </div>
      </ContentItem>
      <ContentItem
        spaceLeft="0"
        col="col-12 col-md-12"
        spaceBottom={3}
        className="mt-3"
      >
        <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
          <h6 className="mb-0">Logistik</h6>
        </div>
        <div className="row">
          <div className="col-lg-6 border-right">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              Kemarin
            </div>
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={sicepat} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>782/1000 Paket</div>
            </div> 
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={jnt} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>520/1000 Paket</div>
            </div>  
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={ninja} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>156/1000 Paket</div>
            </div>
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={tiki} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>850/1000 Paket</div>
            </div>
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={grab} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>120/1000 Paket</div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="d-flex align-items-center p-3" style={{ flex: 1 / 3 }}>
              Hari Ini
            </div>
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={gosend} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>782/1000 Paket</div>
            </div> 
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={tiki} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>520/1000 Paket</div>
            </div> 
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={jnt} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>156/1000 Paket</div>
            </div> 
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={grab} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>850/1000 Paket</div>
            </div>
            <div className="d-flex align-items-center mr-3 ml-3 mt-3" style={{ flex: 1 / 3 }}>
              <img className="mr-2" alt="" src={sicepat} />
              {testData.map((item, idx) => (
                <ProgresBar key={idx} backgroundColor={item.backgroundColor} completed={item.completed} />
              ))}
            </div>
            <div className="d-flex justify-content-end mr-3 mb-3" style={{ flex: 1 / 3 }}>
              <div>120/1000 Paket</div>
            </div>  
          </div>
        </div>
      </ContentItem>
    </ContentContainer>
  );
};

export default Dashboard;