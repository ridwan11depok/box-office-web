import React from 'react';

const DashboardAdministrator = () => {
  return (
    <div className="main-content">
      <section className="section">
        <div className="row text-dashboard">
          <div className="col-lg-6">
            <div className="section-dashboard">
              <h1>Dashboard</h1>
            </div>
          </div>
          <div className="col-lg-6">
            <p>Bulanan</p>
          </div>
        </div>
        <div
          className="row"
          style={{
            height: '116px',
            backgroundColor: '#EBF0FA',
            borderLeft: '10px solid #8DA4DD',
            borderRadius: '10px',
          }}
        >
          <div className="col-lg-12">
            <div className="tambah-gudang">
              <div className="text-row">
                <p className="text-add-gudang">
                  Anda belum memiliki gudang penyimpanan. Silahkan tambahkan
                  gudang peyimpanan anda.
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-12">
            <div>
              <button
                type="button"
                className="btn"
                style={{
                  backgroundColor: '#192A55',
                  borderRadius: '4px',
                  borderWidth: 2,
                  borderColor: '#192A55',
                }}
              >
                <text className="btn-right">Tambah Gudang</text>
              </button>
            </div>
          </div>
        </div>
        <div
          className="row"
          style={{
            marginTop: '16px',
            border: '1px solid',
            borderRadius: '5px',
          }}
        >
          <div className="col-lg-12" style={{ borderBottom: '1px solid' }}>
            <div className="text-header">
              <p className="">Dompet Gudang</p>
            </div>
          </div>
          <div className="col-lg-10">
            <div>
              <img></img>
            </div>
            <div className="text-header">
              <p>Rp. 18.000</p>
            </div>
          </div>
          <div className="col-lg-2">
            <button
              type="button"
              className="btn"
              style={{
                backgroundColor: '#192A55',
                borderRadius: '4px',
                borderWidth: 2,
                borderColor: '#192A55',
              }}
            >
              <text className="btn-right">Isi Dompet</text>
            </button>
          </div>
        </div>
      </section>
    </div>
  );
};

export default DashboardAdministrator;
