import actionType from './reduxConstant';

const initialState = {
  listStore: null,
  detailStore: {},
  listUserStore: null,
  detailUserStore: {},
  listRateHandlingStock: null,
  detailRateHandlingStock: {},
  listWarehouseStore: null,
  listRoleStore: [],
  columnUser: [
    {
      heading: 'Nama User',
      key: 'name',
    },
    {
      heading: 'Email',
      key: 'email',
    },
    {
      heading: 'Role',
      key: 'display_name_role',
    },
    {
      heading: 'Alamat',
      key: 'address',
    },
    {
      heading: 'Nomor Telepon',
      key: 'phone_number',
    },
  ],
  columnRateHandling: [
    {
      heading: 'Nama Sub Kategori',
      key: 'name',
    },
  ],
  columnWarehouse: [
    {
      heading: 'Nama Warehouse',
      key: 'name',
    },
    {
      heading: 'Nomor Telepon',
      key: 'phone_number',
    },
    {
      heading: 'Email',
      key: 'email',
    },
    {
      heading: 'Alamat',
      key: 'address',
    },
  ],
};

const storeAdministratorReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_STORE_ADMINISTRATOR:
      return {
        ...prevState,
        listStore: action.payload,
      };
    case actionType.DETAIL_STORE_ADMINISTRATOR:
      return {
        ...prevState,
        detailStore: action.payload,
      };
    case actionType.USER_STORE_ADMINISTRATOR:
      return {
        ...prevState,
        listUserStore: action.payload,
      };
    case actionType.DETAIL_USER_STORE_ADMINISTRATOR:
      return {
        ...prevState,
        detailUserStore: action.payload,
      };
    case actionType.RATE_HANDLING_STOCK_ADMINISTRATOR:
      return {
        ...prevState,
        listRateHandlingStock: action.payload,
      };
    case actionType.DETAIL_RATE_HANDLING_STOCK_ADMINISTRATOR:
      return {
        ...prevState,
        detailRateHandlingStock: action.payload,
      };
    case actionType.WAREHOUSE_STORE_ADMINISTRATOR:
      return {
        ...prevState,
        listWarehouseStore: action.payload,
      };
    case actionType.LIST_ROLE_STORE:
      return {
        ...prevState,
        listRoleStore: action.payload,
      };
    default:
      return prevState;
  }
};

export default storeAdministratorReducer;
