import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import BackButton from '../../../components/elements/BackButton';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import StoreDetailTable from './components/StoreDetailTable';
import Button from '../../../components/elements/Button';
import ModalConfirmDelete from './components/ModalConfirmDelete';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import FormInfoStore from './components/FormInfoStore';
import FormLokasiStore from './components/FormLokasiStore';
import {
  getListStore,
  updateStore,
  approval,
  getListRoleUserStore,
} from './reduxAction';
import Banner from '../../../components/elements/Banner';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import ModalVerificationStore from './components/ModalVerificationStore';
import { getListSubCategoryOrDetailSubCategory } from '../Kategori/reduxAction';

const useStyles = makeStyles({
  textBanner: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#1c1c1c',
    fontWeight: '400',
    marginBottom: '0px',
  },
});

const validationSchema = Yup.object({});

function StoreDetail() {
  const history = useHistory();
  const params = useParams();
  const classes = useStyles();
  const [state, setState] = useState({
    modalDelete: false,
    isEdit: false,
    modalVerificationStore: false,
    status: 0,
    showBanner: true,
  });

  //for edit
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { detailStore } = useSelector((state) => state.storeAdministrator);
  const formik = useFormik({
    initialValues: {
      logo: '',
      name: '',
      type: '',
      address: '',
      province_id: '',
      city_id: '',
      longitude: '',
      latitude: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      if (values.logo) {
        data.append('store_logo', values.logo);
      }
      data.append('store_name', values.name);
      data.append('type', values.type);
      data.append('store_address', values.address);
      data.append('province_id', values.province_id);
      data.append('city_id', values.city_id);
      data.append('longitude', values.longitude);
      data.append('latitude', values.latitude);
      const res = await dispatch(updateStore(data, params?.id));
      setState({ ...state, isEdit: false });
    } catch (er) {}
  };
  useEffect(() => {
    if (params?.id) {
      dispatch(getListStore({}, params?.id));
      dispatch(getListRoleUserStore({ store_id: params?.id }));
      dispatch(getListSubCategoryOrDetailSubCategory({ per_page: 1000 }));
    }
  }, [params?.id]);

  const handleApprovalStore = async (payload) => {
    try {
      const idStore = params?.id;
      dispatch(approval(payload, idStore));
      setState({
        ...state,
        modalVerificationStore: false,
      });
    } catch (err) {}
  };
  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label={detailStore?.store_name || 'Store Detail'}
            onClick={() => history.push('/admin/store')}
          />
          {!state.isEdit && (
            <div>
              <Button
                style={{ minWidth: '120px' }}
                styleType={
                  detailStore?.status !== 0
                    ? 'redOutline'
                    : 'redOutlineDisabled'
                }
                text="Hapus"
                onClick={() => setState({ ...state, modalDelete: true })}
                disabled={detailStore?.status === 0}
              />
              <Button
                style={{ minWidth: '120px' }}
                className="ml-2"
                styleType="blueOutline"
                styleType={
                  detailStore?.status === 1
                    ? 'blueOutline'
                    : 'blueOutlineDisabled'
                }
                text="Edit"
                // onClick={() => setState({ ...state, isEdit: true })}
                disabled={detailStore?.status !== 1}
                onClick={() => history.push(`/admin/store/edit/${params?.id}`)}
              />
            </div>
          )}
        </div>
      )}
    >
      {(detailStore?.status === 0 || detailStore?.status === 2) &&
        state.showBanner && (
          <ContentItem col="col-12" spaceBottom={3} border={false}>
            <Banner
              className="w-100 d-flex flex-column flex-lg-row justify-content-between align-items-lg-center"
              styleType={detailStore?.status === 0 ? 'yellow' : 'red'}
            >
              <h6 className={classes.textBanner}>
                {detailStore?.status === 0
                  ? 'Store sedang menunggu dalam persetujuan oleh pihak Administrator.'
                  : 'Store dalam keadaan rejected dan sedang menunggu persetujuan pihak administrator'}
              </h6>
              {detailStore?.status === 0 ? (
                <div className="d-flex flex-row">
                  <Button
                    styleType="greenFill"
                    startIcon={() => <DoneIcon />}
                    text="Approve"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 1,
                      })
                    }
                    style={{ minWidth: '140px' }}
                  />
                  <Button
                    styleType="redFill"
                    startIcon={() => <CloseIcon />}
                    text="Reject"
                    className="ml-3"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 2,
                      })
                    }
                    style={{ minWidth: '140px' }}
                  />
                </div>
              ) : (
                <div>
                  <Button
                    styleType="lightBlueFill"
                    text="Batal"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 2,
                      })
                    }
                    style={{ minWidth: '140px' }}
                    onClick={() => setState({ ...state, showBanner: false })}
                  />
                  <Button
                    styleType="greenFill"
                    startIcon={() => <DoneIcon />}
                    text="Approve"
                    className="ml-3"
                    onClick={() =>
                      setState({
                        ...state,
                        modalVerificationStore: true,
                        status: 1,
                      })
                    }
                    style={{ minWidth: '140px' }}
                  />
                </div>
              )}
            </Banner>
          </ContentItem>
        )}
      <ContentItem
        spaceRight="0 pr-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: state.isEdit ? '615px' : '200px' }}
      >
        <div className="p-3">
          <FormInfoStore
            formik={formik}
            initialValues={detailStore}
            isEdit={state.isEdit}
          />
        </div>
      </ContentItem>
      <ContentItem
        spaceLeft="0 pl-md-2"
        col="col-12 col-md-6"
        spaceBottom={3}
        style={{ minHeight: state.isEdit ? '615px' : '200px' }}
      >
        <div className="p-3">
          <FormLokasiStore
            formik={formik}
            initialValues={detailStore}
            isEdit={state.isEdit}
          />
        </div>
      </ContentItem>
      {!state.isEdit && (
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <StoreDetailTable storeId={params?.id} status={detailStore?.status} />
        </ContentItem>
      )}

      {state.isEdit && (
        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
            onClick={() => {
              setState({ ...state, isEdit: false });
            }}
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text={isLoading ? 'Loading...' : 'Simpan'}
            onClick={formik.handleSubmit}
          />
        </div>
      )}

      <ModalConfirmDelete
        show={state.modalDelete}
        onHide={() => setState({ ...state, modalDelete: false })}
      />
      <ModalVerificationStore
        show={state.modalVerificationStore}
        onHide={() => setState({ ...state, modalVerificationStore: false })}
        onAgree={handleApprovalStore}
        status={state.status}
      />
    </ContentContainer>
  );
}

export default StoreDetail;
