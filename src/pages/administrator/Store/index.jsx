import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import StoreTable from './components/StoreTable';
import './styles.css';

const GudangAdministrator = () => {
  return (
    <>
      <ContentContainer title="Store">
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <StoreTable />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default GudangAdministrator;
