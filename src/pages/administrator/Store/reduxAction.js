import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable, setErrorData } from '../../../redux/actions';

export const getListStore =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      if (id) {
        dispatch(setLoading(true));
      } else {
        dispatch(setLoadingTable(true));
      }
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('storeAdministrator', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'storeAdministrator',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailStore(response.result));
        } else {
          dispatch(listStore(response.result));
        }
        if (id) {
          dispatch(setLoading(false));
        } else {
          dispatch(setLoadingTable(false));
        }
      }
    } catch (err) {
      if (id) {
        dispatch(setErrorData(err));
        dispatch(setLoading(false));
      } else {
        dispatch(setLoadingTable(false));
      }
    }
  };

const listStore = (data) => {
  return {
    type: actionType.LIST_STORE_ADMINISTRATOR,
    payload: data,
  };
};

const detailStore = (data) => {
  return {
    type: actionType.DETAIL_STORE_ADMINISTRATOR,
    payload: data,
  };
};

export const addStore = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('storeAdministrator', data, headers);
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambahkan store baru.',
        type: 'success',
      })
    );
    dispatch(getListStore({}, response.result.id));
    return Promise.resolve(response);
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages:err?.error?.message|| 'Gagal menambahkan store baru.',
        type: 'error',
      })
    );
    return Promise.reject(err);
  }
};

export const deleteStore =
  (payload = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id = payload;
      const response = await consume.delete(
        'storeAdministrator',
        data,
        headers,
        id
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus store.',
          type: 'success',
        })
      );
      dispatch(getListStore({ page: 1, per_page: 10 }));
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus store.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const updateStore = (payload, id) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };
    const response = await consume.post(
      'storeAdministrator',
      data,
      headers,
      id
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil memperbarui store.',
        type: 'success',
      })
    );
    dispatch(getListStore({}, id));
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal memperbarui store.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const getListUserStore =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('userStore', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'userStore',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailUserStore(response.result));
        } else {
          const listUser = response.result.data.map((item) => ({
            ...item,
            ...item.user,
            userIdInStore: item.id,
            role_id: item.user?.roles?.[0]?.id,
            display_name_role:
              item.user?.roles?.[0]?.display_name ||
              item.user?.roles?.[0]?.name,
          }));
          dispatch(listUserStore({ ...response.result, data: listUser }));
        }
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      dispatch(setLoadingTable(false));
    }
  };

const listUserStore = (data) => {
  return {
    type: actionType.USER_STORE_ADMINISTRATOR,
    payload: data,
  };
};

const detailUserStore = (data) => {
  return {
    type: actionType.DETAIL_USER_STORE_ADMINISTRATOR,
    payload: data,
  };
};

export const getListRateHandlingStock =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('rateHandlingStock', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'rateHandlingStock',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch(detailRateHandlingStock(response.result));
        } else {
          const listRateHandling = response.result.data.map((item) => ({
            ...item,
            ...item.sub_category,
            rateHandlingId: item.id,
          }));
          dispatch(
            listRateHandlingStock({
              ...response.result,
              data: listRateHandling,
            })
          );
        }
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      dispatch(setLoadingTable(false));
    }
  };

const listRateHandlingStock = (data) => {
  return {
    type: actionType.RATE_HANDLING_STOCK_ADMINISTRATOR,
    payload: data,
  };
};

const detailRateHandlingStock = (data) => {
  return {
    type: actionType.DETAIL_RATE_HANDLING_STOCK_ADMINISTRATOR,
    payload: data,
  };
};

export const getListWarehouseStore = (params) => async (dispatch, getState) => {
  try {
    dispatch(setLoadingTable(true));
    const { token } = getState().login;
    let data = {};
    let headers = { token: token };
    let response = await consume.getWithParams(
      'warehouseStore',
      data,
      headers,
      params
    );
    if (response) {
      // const listRateHandling = response.result.data.map((item) => ({...item,...item.sub_category}));
      dispatch(listWarehouseStore(response.result));
      dispatch(setLoadingTable(false));
    }
  } catch (err) {
    dispatch(setLoadingTable(false));
  }
};

const listWarehouseStore = (data) => {
  return {
    type: actionType.WAREHOUSE_STORE_ADMINISTRATOR,
    payload: data,
  };
};

export const approval = (payload, id) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };
    const otherEndpoint = `${id}/approval`;
    const response = await consume.post(
      'storeAdministrator',
      data,
      headers,
      otherEndpoint
    );
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: `Berhasil ${
          payload.status === 1 ? 'memverifikasi' : 'menolak/me-reject'
        } store.`,
        type: 'success',
      })
    );
    dispatch(getListStore({}, id));
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: `Gagal ${
          payload.status === 1 ? 'memverifikasi' : 'menolak/me-reject'
        } store.`,
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const addUserStore =
  (payload, store_id) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token, content_type: 'multipart/form-data' };
      let response = await consume.post('userStore', data, headers);
      if (response) {
        dispatch(getListUserStore({ page: 1, per_page: 10, store_id }));
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menambah user store.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menambah user store.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

export const updateUserStore =
  (payload, userId) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let params = payload;
      let response = await consume.put(
        'userStore',
        data,
        headers,
        params,
        userId
      );
      if (response) {
        dispatch(
          getListUserStore({
            page: 1,
            per_page: 10,
            store_id: payload.store_id,
          })
        );
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data user store.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data user store.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

export const deleteUserStore =
  (payload, userId) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.delete('userStore', data, headers, userId);
      if (response) {
        dispatch(
          getListUserStore({
            page: 1,
            per_page: 10,
            store_id: payload.store_id,
          })
        );
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus data user store.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus data user store.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const addRateHandlingStock = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };
    let response = await consume.post('rateHandlingStock', data, headers);
    if (response) {
      dispatch(
        getListRateHandlingStock({
          page: 1,
          per_page: 10,
          store_id: payload.store_id,
        })
      );
      dispatch(setLoading(false));
    }
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambah data rate handling stock.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal menambah data rate handling stock.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const updateRateHandlingStock =
  (payload, rateHandlingId) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let params = payload;
      let response = await consume.put(
        'rateHandlingStock',
        data,
        headers,
        params,
        rateHandlingId
      );
      if (response) {
        dispatch(
          getListRateHandlingStock({
            page: 1,
            per_page: 10,
            store_id: payload.store_id,
          })
        );
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data rate handling stock.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data rate handling stock.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const deleteRateHandlingStock =
  (payload, rateHandlingId) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token };
      let response = await consume.delete(
        'rateHandlingStock',
        data,
        headers,
        rateHandlingId
      );
      if (response) {
        dispatch(
          getListRateHandlingStock({
            page: 1,
            per_page: 10,
            store_id: payload.store_id,
          })
        );
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus data rate handling stock.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus data rate handling stock.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const getListRoleUserStore = (params) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = {};
    let headers = { token: token };
    let response = await consume.getWithParams(
      'listRoleUserStore',
      data,
      headers,
      params
    );
    if (response) {
      dispatch(listRoleUserStore(response.result));
      dispatch(setLoading(false));
    }
  } catch (err) {
    dispatch(setLoading(false));
  }
};

const listRoleUserStore = (data) => {
  return {
    type: actionType.LIST_ROLE_STORE,
    payload: data,
  };
};
