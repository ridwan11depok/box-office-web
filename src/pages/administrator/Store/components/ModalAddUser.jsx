import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form, Row, Col } from 'react-bootstrap';
import {
  PasswordField,
  TextField,
  SelectField,
  TextArea,
} from '../../../../components/elements/InputField';
import { useSelector, useDispatch } from 'react-redux';
import { addUserStore, updateUserStore } from '../reduxAction';
import { hex_md5, str_md5 } from '../../../../utils/md5';

const phoneRegex = RegExp(
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
);
const validationSchema = Yup.object({
  name: Yup.string().required('Harap untuk mengisi nama user'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Harap untuk mengisi alamat email'),
  role_id: Yup.string().required(
    'Silahkan pilih role sesuai dengan personil yang ditambahkan'
  ),
  phone_number: Yup.string()
    .matches(phoneRegex, 'Invalid phone')
    .required('Harap untuk mengisi nomor telepon'),
  password: Yup.string().min(8, 'Password minimal 8 karakter'),
  // .required('Harap untuk mengisi password'),
  password_confirmation: Yup.string()
    .min(8, 'Password minimal 8 karakter')
    .oneOf([Yup.ref('password'), null], 'Password tidak sama'),
  // .required('Harap untuk mengisi ulang password'),
  address: Yup.string().required('Harap untuk mengisi alamat user'),
});

const ModalAddStaff = (props) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState(null);
  const { detailStore, listRoleStore } = useSelector(
    (state) => state.storeAdministrator
  );
  const { isLoading } = useSelector((state) => state.loading);
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      role_id: '',
      phone_number: '',
      password: '',
      password_confirmation: '',
      address: '',
      country_code: '62',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmitForm(values);
    },
  });

  const handleSubmitForm = async (payload) => {
    try {
      if (props.initialValues) {
        const userId = props.initialValues.userIdInStore;
        if (payload.password) {
          payload.password = hex_md5(payload.password);
        }
        if (payload.password_confirmation) {
          payload.password_confirmation = hex_md5(
            payload.password_confirmation
          );
        }

        await dispatch(
          updateUserStore({ ...payload, store_id: detailStore.id }, userId)
        );
      } else {
        const data = new FormData();
        Object.keys(payload).forEach((key) =>
          data.append(`${key}`, payload[key])
        );
        data.append('store_id', detailStore.id);
        await dispatch(addUserStore(data, detailStore.id));
      }
      props.onHide();
    } catch (err) {
      setErrors(err?.error?.errors || null);
    }
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
      setErrors(null);
    }
    if (props.initialValues && props.show) {
      const init = { ...props.initialValues };
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('email', init.email);
      formik.setFieldValue('phone_number', init.phone_number);
      formik.setFieldValue('address', init.address);
      formik.setFieldValue('country_code', init.country_code);
      formik.setFieldValue('role_id', init.role_id);
    }
  }, [props.show, props.initialValues]);
  return (
    <>
      <Modal
        titleModal={props.initialValues ? 'Edit User' : 'Tambah User'}
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        content={(data) => (
          <>
            <TextField
              className="mt-3"
              label="Nama User"
              placeholder="Input nama user"
              name="name"
              withValidate
              formik={formik}
            />
            <TextField
              label="Email Address"
              placeholder="Input email address"
              name="email"
              withValidate
              formik={formik}
            />
            {errors?.email && (
              <Form.Text className="text-input-error mt-0">
                {errors?.email[0] || ''}
              </Form.Text>
            )}
            <Form.Group className="mt-2" controlId="formBasicEmail">
              <Form.Label as={'h6'} className="input-label">
                Role
              </Form.Label>
              {formik.errors.role_id && formik.touched.role_id ? (
                <Form.Text className="text-input-error">
                  {formik.errors.role_id}
                </Form.Text>
              ) : (
                <Form.Text style={{ color: '#4F4F4F' }}>
                  Silahkan pilih role sesuai dengan personil yang ditambahkan
                </Form.Text>
              )}
              <div className="d-flex flex-row">
                {listRoleStore.map((item) => (
                  <div
                    onClick={() => {
                      formik.setFieldValue('role_id', item.id);
                    }}
                    className="text-center d-flex align-items-center justify-content-center mt-2 mr-2"
                    style={{
                      width: '120px',
                      height: '40px',
                      borderRadius: '40px',
                      border:
                        formik.values.role_id !== item.id &&
                        '2px solid #828282',
                      color:
                        formik.values.role_id === item.id
                          ? '#FFFFFF'
                          : '#1C1C1C',
                      backgroundColor:
                        formik.values.role_id === item.id && '#192A55',
                      fontSize: '14px',
                      fontFamily: 'Open Sans',
                      fontWeight: '700',
                      cursor: 'pointer',
                    }}
                  >
                    {item.display_name}
                  </div>
                ))}
              </div>
            </Form.Group>
            <Row>
              <Col>
                <SelectField
                  label="Nomor Telepon"
                  options={[
                    {
                      value: '62',
                      description: 'Indonesia(+62)',
                    },
                  ]}
                  name="country_code"
                  formik={formik}
                  withValidate
                />
              </Col>
              <Col className="d-flex align-items-end">
                <TextField
                  className="w-100"
                  placeholder="Input nomor telepon"
                  name="phone_number"
                  formik={formik}
                  withValidate
                  withError={false}
                  type="number"
                />
              </Col>
              <Col xs={12} style={{ marginTop: '-7px', marginBottom: '5px' }}>
                {formik.errors.phone_number && formik.touched.phone_number && (
                  <Form.Text className="text-input-error">
                    {formik.errors.phone_number}
                  </Form.Text>
                )}
              </Col>
            </Row>
            <PasswordField
              label="Password"
              placeholder="Input password"
              name="password"
              withValidate
              formik={formik}
            />
            <PasswordField
              label="Masukkan Ulang Password"
              placeholder="Input password"
              name="password_confirmation"
              withValidate
              formik={formik}
            />
            <TextArea
              className="mb-3"
              label="Alamat"
              placeholder="Input alamat"
              rows={3}
              withValidate
              name="address"
              formik={formik}
            />
            {/* {errors &&
              Object.keys(errors).map((key) => (
                <Form.Text className="text-input-error">
                  {errors[key][0]}
                </Form.Text>
              ))} */}
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                if (!Boolean(formik.values.role_id)) {
                  formik.setFieldTouched('role_id', true);
                }
                formik.handleSubmit(e);
              }}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading
                ? 'Loading...'
                : props.initialValues
                ? 'Perbarui'
                : 'Tambah'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalAddStaff.defaultProps = {
  initialValues: null,
};
export default ModalAddStaff;
