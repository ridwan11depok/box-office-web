import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import ModalAddUser from './ModalAddUser';
import ModalDetailUser from './ModalDetailUser';
import ModalDeleteUser from './ModalDeleteUser';
import { SearchField } from '../../../../components/elements/InputField';
import AddIcon from '@material-ui/icons/Add';
import { TabHeader } from '../../../../components/elements/Tabs';
import {
  getListUserStore,
  getListRateHandlingStock,
  getListWarehouseStore,
  deleteUserStore,
} from '../reduxAction';
import ModalRateHandling from './ModalRateHandling';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ModalDeleteRateHandling from './ModalDeleteRateHandling';
import { formatRupiah } from '../../../../utils/text';

const useStyles = makeStyles({
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  textEdit: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#1C1C1C',
  },
  textDelete: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#DA101A',
  },
});

const ActionField = ({
  data,
  onClick = () => {},
  onClickEdit = () => {},
  onClickDelete = () => {},
  tab = 1,
  status,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return tab === 1 ? (
    <>
      <Button
        text="Detail"
        styleType={status === 1 ? 'lightBlueFill' : 'lightBlueFillDisabled'}
        onClick={() => onClick(data)}
        disabled={status !== 1}
      />
    </>
  ) : (
    <div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        placement="bottom"
      >
        <MenuItem
          onClick={() => {
            onClickEdit(data);
            handleClose();
          }}
          className={classes.textEdit}
        >
          Edit
        </MenuItem>
        <MenuItem
          onClick={() => {
            onClickDelete(data);
            handleClose();
          }}
          className={classes.textDelete}
        >
          Delete
        </MenuItem>
      </Menu>
      <Button
        styleType={status === 1 ? 'lightBlueFill' : 'lightBlueFillDisabled'}
        onClick={handleClick}
        isOptionButton
        disabled={status !== 1}
      />
    </div>
  );
};

const EmptyUser = () => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data saat ini</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleAddStaff, handleSearch, tab, status, handleAddRateHandling } =
    props;
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField
          placeholder="Cari di sini"
          onChange={(e) => {
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500);
          }}
        />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType={status === 1 ? 'blueFill' : 'blueFillDisabled'}
          text={tab === 1 ? 'Tambah User' : 'Rate Handling Stock'}
          onClick={tab === 1 ? handleAddStaff : handleAddRateHandling}
          startIcon={() => <AddIcon />}
          disabled={status !== 1}
        />
      </div>
    </div>
  );
};

const DefaultRateHandlingStock = ({ data }) => {
  return <span>{formatRupiah(data?.rate_handling_stock || 0)}</span>;
};

const RateHandlingStock = ({ data }) => {
  return <span>{formatRupiah(data?.amount || 0)}</span>;
};

function StoreDetailTable(props) {
  const dispatch = useDispatch();
  const {
    columnUser,
    columnRateHandling,
    columnWarehouse,
    listUserStore,
    listRateHandlingStock,
    listWarehouseStore,
  } = useSelector((state) => state.storeAdministrator);
  const history = useHistory();
  const [state, setState] = useState({
    modalAddStaff: false,
    modalDetailStaff: false,
    dataDetailStaff: {},
    modalDeleteStaff: false,
    idWarehouse: null,
    modalEditStaff: false,
    search: '',
    tab: 1,
    modalRateHandling: false,
    dataRateHandling: null,
    modalDeleteRateHandling: false,
  });

  const handleAddStaff = () => {
    setState({ ...state, modalAddStaff: true });
  };

  const handleDetailStaff = (data) => {
    setState({ ...state, modalDetailStaff: true, dataDetailStaff: data });
  };
  const handleModalDeleteStaff = (data) => {
    setState({ ...state, modalDetailStaff: false, modalDeleteStaff: true });
  };
  const handleModalEditStaff = (data) => {
    setState({ ...state, modalDetailStaff: false, modalEditStaff: true });
  };
  const handleAddRateHandling = () => {
    setState({ ...state, modalRateHandling: true, dataRateHandling: null });
  };
  const handleEditRateHandling = (values) => {
    setState({ ...state, modalRateHandling: true, dataRateHandling: values });
  };
  const handleDeleteRateHandling = (values) => {
    setState({
      ...state,
      modalDeleteRateHandling: true,
      dataRateHandling: values,
    });
  };

  const fetchListData = async (params) => {
    if (state.tab === 1) {
      dispatch(getListUserStore(params));
    } else if (state.tab === 2) {
      dispatch(getListRateHandlingStock(params));
    } else {
      dispatch(getListWarehouseStore(params));
    }
  };

  const handleDeleteStaff = async () => {
    try {
      const userId = state.dataDetailStaff.user_id;
      const payload = { store_id: state.dataDetailStaff.store_id };
      await dispatch(deleteUserStore(payload, userId));
      setState({ ...state, modalDeleteStaff: false });
    } catch (er) {}
  };
  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const changeTab = (tab) => {
    setState({ ...state, tab });
  };
  return (
    <div>
      <div className="w-100 mb-3" style={{ border: '1px solid #E8E8E8' }}>
        <TabHeader
          border={false}
          activeTab={state.tab}
          listHeader={[
            {
              tab: 1,
              title: 'User',
            },
            {
              tab: 2,
              title: 'Rate Handling Stock',
            },
            {
              tab: 3,
              title: 'Warehouse',
            },
          ]}
          onChange={changeTab}
        />
      </div>
      <DataTable
        action={fetchListData}
        totalPage={listUserStore?.last_page || 5}
        params={{
          search: state.search,
          store_id: props.storeId,
          // tab: state.tab,
        }}
        variableTriggerAction={[state.tab]}
        column={
          state.tab === 1
            ? [
                ...columnUser,
                {
                  heading: 'Aksi',
                  render: (data) => (
                    <ActionField
                      onClick={handleDetailStaff}
                      data={data}
                      status={props.status}
                    />
                  ),
                },
              ]
            : state.tab === 2
            ? [
                ...columnRateHandling,
                {
                  heading: 'Default Rate Handling Stock',
                  render: (data) => <DefaultRateHandlingStock data={data} />,
                },
                {
                  heading: 'Rate Handling Stock',
                  render: (data) => <RateHandlingStock data={data} />,
                },
                {
                  heading: 'Aksi',
                  render: (data) => (
                    <ActionField
                      onClickEdit={handleEditRateHandling}
                      onClickDelete={handleDeleteRateHandling}
                      data={data}
                      tab={state.tab}
                      status={props.status}
                    />
                  ),
                },
              ]
            : columnWarehouse
        }
        data={
          state.tab === 1
            ? listUserStore?.data || []
            : state.tab === 2
            ? listRateHandlingStock?.data || []
            : listWarehouseStore?.data || []
        }
        withNumber={false}
        renderHeader={() =>
          (state.tab === 1 || state.tab === 2) && (
            <HeaderTable
              handleAddStaff={handleAddStaff}
              handleSearch={handleSearch}
              tab={state.tab}
              status={props.status}
              handleAddRateHandling={handleAddRateHandling}
            />
          )
        }
        renderEmptyData={() => <EmptyUser />}
      />
      <ModalAddUser
        show={state.modalAddStaff}
        onHide={() => setState({ ...state, modalAddStaff: false })}
        onAgree={() => setState({ ...state, modalAddStaff: false })}
      />
      <ModalDetailUser
        show={state.modalDetailStaff}
        onHide={() => setState({ ...state, modalDetailStaff: false })}
        data={state.dataDetailStaff}
        handleDeleteStaff={handleModalDeleteStaff}
        handleEditStaff={handleModalEditStaff}
      />
      <ModalDeleteUser
        show={state.modalDeleteStaff}
        onHide={() => setState({ ...state, modalDeleteStaff: false })}
        onAgree={handleDeleteStaff}
        data={state.dataDetailStaff}
      />
      <ModalAddUser
        show={state.modalEditStaff}
        onHide={() => setState({ ...state, modalEditStaff: false })}
        initialValues={state.dataDetailStaff}
      />
      <ModalRateHandling
        show={state.modalRateHandling}
        onHide={() => setState({ ...state, modalRateHandling: false })}
        isEdit={Boolean(state.dataRateHandling)}
        initialValues={state.dataRateHandling}
      />
      <ModalDeleteRateHandling
        show={state.modalDeleteRateHandling}
        onHide={() => setState({ ...state, modalDeleteRateHandling: false })}
        data={state.dataRateHandling}
      />
    </div>
  );
}

export default StoreDetailTable;
