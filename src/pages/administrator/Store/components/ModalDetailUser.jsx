import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

const ModalDetailStaff = (props) => {
  const classes = useStyles();
  return (
    <>
      <Modal
        bodyClassName="pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        titleClassName="w-100"
        data={props.data}
        header={() => (
          <div className="d-flex flex-between w-100 align-items-center">
            User Detail
            <div className="ml-auto">
              <Button
                style={{ minWidth: '120px' }}
                styleType="redOutline"
                text="Hapus"
                onClick={props.handleDeleteStaff}
              />
              <Button
                style={{ minWidth: '120px' }}
                className="ml-1"
                styleType="blueOutline"
                text="Edit"
                onClick={props.handleEditStaff}
              />
            </div>
          </div>
        )}
        content={(data) => (
          <>
            <div className="mt-3">
              <h6 className={classes.text}>Nama User</h6>
              <h6 className={classes.text}>{data?.name || 'No.Data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Email</h6>
              <h6 className={classes.text}>{data?.email || 'No.Data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Role</h6>
              <h6 className={classes.text}>
                {data?.display_name_role || 'No.Data'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Nomor Telepon</h6>
              <h6 className={classes.text}>
                {data?.phone_number || 'No.Data'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.text}>Alamat</h6>
              <h6 className={classes.text}>{data?.address || 'No.Data'}</h6>
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              onClick={props.onHide}
              text="Kembali"
              styleType="lightBlueFill"
            />
          </>
        )}
        footerClassName="p-3"
      />
    </>
  );
};

export default ModalDetailStaff;
