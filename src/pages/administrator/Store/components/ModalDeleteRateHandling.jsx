import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { deleteRateHandlingStock } from '../reduxAction';

const ModalDeleteRateHandling = (props) => {
  const { isLoading } = useSelector((state) => state.loading);
  const dispatch = useDispatch();
  const handleSubmit = async () => {
    try {
      const payload = {
        store_id: props.data?.store_id || null,
      };
      const rateHandlingId = props.data?.rateHandlingId || null;
      await dispatch(deleteRateHandlingStock(payload, rateHandlingId));
      props.onHide();
    } catch (err) {}
  };
  return (
    <>
      <Modal
        titleModal="Hapus Rate Handling Stock"
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        content={(data) => (
          <>
            <h6
              className="mt-3 mb-3"
              style={{
                fontSize: '14px',
                fontWeight: '400',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
              }}
            >
              {`Apakah Anda yakin akan menghapus "${data?.name || ''}" ?`}
            </h6>
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              className="mr-3"
              styleType="redFill"
              text={isLoading ? 'Loading...' : 'Hapus'}
              onClick={handleSubmit}
            />
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text="Batal"
              onClick={props.onHide}
            />
          </div>
        )}
      />
    </>
  );
};

export default ModalDeleteRateHandling;
