import React, { useState, useEffect } from 'react';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { Form } from 'react-bootstrap';
import { TextField } from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'react-bootstrap/Image'
import Badge from '../../../../components/elements/Badge';
import { useSelector } from 'react-redux';
import { validateFile } from '../../../../utils/validate';

const allowedExtentions = ['image/png', 'image/jpg', 'image/jpeg'];

const useStyles = makeStyles({
  storeName: {
    fontFamily: 'Work Sans',
    fontSize: '14px',
    fontWeight: '700',
    color: '#000000',
  },
  storeType: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
    marginTop: 10,
  },
  avatar: {
    width: '64px',
    height: '64px',
    marginRight: '8px',
  },
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4F4F4F',
    marginBottom: '4px',
    lineHeight: '20px'
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#363636',
  },
});

function FormInfoStore(props) {
  const classes = useStyles();
  const [state, setState] = useState({ image: '' });
  const inputImage = React.createRef();
  const { formik } = props;
  const handleChangeImage = async (e) => {
    const files = e.target.files;
    const response = await validateFile({
      files,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    });
    if (response) {
      setState({ ...state, image: e.target.files[0] });
      formik.setFieldValue('logo', response?.file);
    }
  };

  const { detailStore } = useSelector((state) => state.storeAdministrator);
  useEffect(() => {
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('name', init.store_name);
      formik.setFieldValue('type', init.type);
    }
  }, [props.initialValues]);
  return (
    <>
      <Container withHeader={false}>
        {props.isEdit ? (
          <>
            <Item border={false} spaceBottom="3">
              <Form.Group className="mb-1" controlId="formBasicEmail">
                <Form.Label as={'h6'} className="input-label">
                  Logo Store
                </Form.Label>
                <div className="row">
                  <div className="col-12 col-lg-4">
                    <div
                      className="rounded d-flex justify-content-center align-items-center"
                      style={{
                        width: '100px',
                        height: '100px',
                        border: '2px dashed #CFCFCF',
                        backgroundColor: '#F2F2F2',
                        marginTop: '7px',
                        overflow: 'hidden',
                      }}
                    >
                      {state.image ||
                      props.initialValues?.store_logo_full_url ? (
                        <img
                          style={{ width: '115px', height: '100px' }}
                          src={
                            state.image
                              ? URL.createObjectURL(state.image)
                              : props.initialValues?.store_logo_full_url || ''
                          }
                          alt=""
                        />
                      ) : (
                        <img src={pictureIcon} alt="" />
                      )}
                    </div>
                  </div>
                  <div className="col-12 col-lg-8 pl-3">
                    <div
                      className="text-justify"
                      style={{
                        fontWeight: '400',
                        color: '#4F4F4F',
                        fontSize: '14px',
                      }}
                    >
                      <p>
                        Ukuran optimal 300 x 300 piksel dengan Besar file:
                        Maksimum 10.000.000 bytes (10 Megabytes). Ekstensi file
                        yang diperbolehkan: JPG, JPEG, PNG
                      </p>
                      <Button
                        styleType="blueOutline"
                        onClick={() => inputImage.current.click()}
                        text={state.image ? 'Ganti Foto' : 'Pilih Foto'}
                        style={{
                          width: '100%',
                          maxWidth: '296px',
                          height: '32px',
                        }}
                      />
                    </div>
                  </div>
                </div>
                {formik.touched.logo && formik.errors.logo ? (
                  <Form.Text className="text-input-error">
                    {formik.errors.logo}
                  </Form.Text>
                ) : props.errorServer?.store_logo?.[0] ? (
                  <Form.Text className="text-input-error">
                    {props.errorServer?.store_logo?.[0]}
                  </Form.Text>
                ) : null}
              </Form.Group>
            </Item>
            <Item border={false}>
              <TextField
                label="Nama Store"
                placeholder="Input nama store"
                name="name"
                formik={formik}
                withValidate
                errorServer={props.errorServer?.store_name?.[0] || ''}
              />
              <Form.Group className="mt-4" controlId="formBasicEmail">
                <Form.Label as={'h6'} className="input-label">
                  Tipe Store
                </Form.Label>
                <Form.Text style={{ color: '#4F4F4F' }}>
                  Silahkan pilih tipe store yang ingin digunakan
                </Form.Text>
                <div className="d-flex flex-row">
                  {[
                    { name: 'Ecommerce', key: 1 },
                    { name: 'Corporation', key: 2 },
                  ].map((item) => (
                    <div
                      onClick={() => {
                        formik.setFieldValue('type', item.name);
                      }}
                      className="text-center d-flex align-items-center justify-content-center mt-2 mr-2"
                      style={{
                        width: '120px',
                        height: '40px',
                        borderRadius: '40px',
                        border:
                          formik.values.type !== item.name &&
                          '2px solid #828282',
                        color:
                          formik.values.type === item.name
                            ? '#FFFFFF'
                            : '#1C1C1C',
                        backgroundColor:
                          formik.values.type === item.name && '#192A55',
                        fontSize: '14px',
                        fontFamily: 'Open Sans',
                        fontWeight: '700',
                        cursor: 'pointer',
                      }}
                    >
                      {item.name}
                    </div>
                  ))}
                </div>
                {formik.errors.type && formik.touched.type ? (
                  <Form.Text className="text-input-error">
                    {formik.errors.type}
                  </Form.Text>
                ) : props.errorServer?.type?.[0] ? (
                  <Form.Text className="text-input-error">
                    {props.errorServer?.type?.[0]}
                  </Form.Text>
                ) : null}
              </Form.Group>
            </Item>
          </>
        ) : (
          <div>
            <div className="row">
              <div className="col-lg-4">
                <Image
                  className={classes.avatar}
                  fluid={true}
                  style={{width: '104px', height: '104px'}}
                  src={props.initialValues?.store_logo_full_url || ''}
                />
              </div>
              
              <div className="col-lg-8">
                <h6 className={classes.label}>Nama Store</h6>
                <h6 className={classes.value}>{props.initialValues?.store_name || 'No Data'}</h6>

                <Badge
                  label={
                    props.initialValues?.status === 0
                      ? 'Waiting'
                      : props.initialValues?.status === 1
                      ? 'Verified'
                      : 'Rejected'
                  }
                  style={
                    {
                      fontSize: '14px'
                    }
                  }
                  styleType={
                    props.initialValues?.status === 0
                      ? 'yellow'
                      : props.initialValues?.status === 1
                      ? 'green'
                      : 'red'
                  }
                />
              </div>
              
              <div className="col-lg-6 mt-3">
                <h6 className={classes.label}>Tipe Store</h6>
                <h6 className={classes.value}>{props.initialValues?.type || 'No Data'}</h6>
              </div>

              <div className="col-lg-6 mt-3">
                <h6 className={classes.label}>Tipe Invoice</h6>
                <h6 className={classes.value}>{props.initialValues?.type_invoice}</h6>
              </div>

            </div>
            
          </div>
        )}
      </Container>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={inputImage}
        type="file"
        className="d-none"
        accept={allowedExtentions.join(',')}
      />
    </>
  );
}
FormInfoStore.defaultProps = {
  initialValues: null,
  isEdit: true,
  errorServer: null,
};

export default FormInfoStore;
