import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import {
  SearchField,
  ReactSelect,
} from '../../../../components/elements/InputField';
import AddIcon from '@material-ui/icons/Add';
import clsx from 'clsx';
import Avatar from '@material-ui/core/Avatar';
import { getListStore } from '../reduxAction';
import { Row, Col, Form } from 'react-bootstrap';

const useStyles = makeStyles({
  rootStatus: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  waiting: {
    color: '#F57F17',
  },
  rejected: {
    color: '#DA101A',
  },
  approved: {
    color: '#1B5E20',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
});

const ActionField = ({ data }) => {
  const history = useHistory();
  return (
    <>
      <Button
        text="Detail"
        styleType="lightBlueFill"
        onClick={() => {
          history.push(`/admin/store/detail/${data.id}`);
        }}
      />
    </>
  );
};

const EmptyStore = ({ onClick }) => {
  const classes = useStyles();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data store saat ini.</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleAddStore, handleSearch, handleFilter } = props;

  return (
    <Row className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
      <Col lg={3} md={6} sm={12} className="mb-2">
        <ReactSelect
          options={[
            { value: 'all', label: 'Pilih Semua' },
            { value: 'Ecommerce', label: 'Ecommerce' },
            { value: 'Corporation', label: 'Corporation' },
          ]}
          placeholder="Pilih Tipe Store"
          styleSelect={{ minWidth: '200px' }}
          onChange={(e) => handleFilter('type', e.target.value)}
        />
      </Col>
      
      <Col lg={3} md={6} sm={12} className="mb-2">
        <ReactSelect
          options={[
            { value: 'all', label: 'Pilih Semua' },
            { value: '0', label: 'Waiting' },
            { value: '1', label: 'Approved' },
            { value: '2', label: 'Rejected' },
          ]}
          placeholder="Pilih Status Store"
          styleSelect={{ minWidth: '200px' }}
          onChange={(e) => handleFilter('status', e.target.value)}
        />
      </Col>

      <Col lg={3} md={6} sm={12} className="mb-2">
        <SearchField
          placeholder="Cari Store"
          onChange={(e) => {
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500);
          }}
        />
      </Col>
      
      <Col lg={3} md={6} sm={12} className="text-right mb-2">
        <Button
          styleType="blueFill"
          text="Tambah Store"
          onClick={handleAddStore}
          startIcon={() => <AddIcon />}
        />
      </Col>
    </Row>
  );
};

const StatusColumn = ({ status }) => {
  const classes = useStyles();
  return (
    <h6
      className={clsx(classes.rootStatus, {
        [classes.waiting]: status === 0,
        [classes.approved]: status === 1,
        [classes.rejected]: status === 2,
      })}
    >
      {status === 0 ? 'Waiting' : status === 1 ? 'Approved' : 'Rejected'}
    </h6>
  );
};

const LogoColumn = ({ data }) => {
  return (
    <Avatar alt="Logo" variant="square" src={data.store_logo_full_url || ''} />
  );
};

function StoreTable(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listStore } = useSelector((state) => state.storeAdministrator);
  const [state, setState] = useState({
    search: '',
    status: null,
    type: null,
  });

  const handleAddStore = () => {
    history.push('/admin/store/add');
  };

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const fetchListStore = (params) => {
    dispatch(getListStore(params));
  };

  const handleFilter = (filterType, value) => {
    if (filterType === 'type') {
      setState({ ...state, type: value === 'all' ? null : value });
    } else if (filterType === 'status') {
      setState({ ...state, status: value === 'all' ? null : value });
    }
  };
  return (
    <div>
      <DataTable
        action={fetchListStore}
        totalPage={listStore?.last_page || 5}
        params={{
          search: state.search,
          status: state.status && state.status,
          type: state.type && state.type,
        }}
        column={[
          {
            heading: 'Logo',
            render: (data) => <LogoColumn data={data} />,
          },
          {
            heading: 'Nama Store',
            key: 'store_name',
          },
          {
            heading: 'Tipe Store',
            key: 'type',
          },
          {
            heading: 'Alamat',
            key: 'store_address',
          },
          {
            heading: 'Status',
            render: (data) => <StatusColumn status={data?.status || 0} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={listStore?.data || []}
        withNumber={false}
        customHeader={true}
        renderHeader={() => (
          <HeaderTable

            handleAddStore={handleAddStore}
            handleSearch={handleSearch}
            handleFilter={handleFilter}
          />
        )}
        renderEmptyData={() => <EmptyStore />}
      />
    </div>
  );
}

export default StoreTable;
