import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik, validateYupSchema } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import {
  TextField,
  ReactSelect,
  CurrencyInput,
} from '../../../../components/elements/InputField';
import Button from '../../../../components/elements/Button';
import { addRateHandlingStock, updateRateHandlingStock } from '../reduxAction';
import { InputGroup, Form } from 'react-bootstrap';

const validationSchema = Yup.object({
  sub_category_id: Yup.string().required('Silahkan pilih sub kategori'),
  rateHandling: Yup.number()
    .typeError(
      'Input Harus Berupa Angka, jika tidak bisa dihapus silahkan tutup dulu formnya, dan klik lagi "Tambah Sub Kategori"'
    )
    .required('Rate Handling Tidak Boleh Kosong'),
});

const ModalRateHandling = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { detailStore } = useSelector((state) => state.storeAdministrator);
  const { listSubCategory } = useSelector(
    (state) => state.categoryAdministratorReducer
  );
  const [rateHandling, setRateHandling] = useState('0');
  const [defaultSubCategory, setDefaultSubCategory] = useState({});
  const formik = useFormik({
    initialValues: {
      sub_category_id: '',
      rateHandling: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
      setRateHandling('');
    }
  }, [props.show]);

  const handleSubmit = async (values) => {
    try {
      const data = {
        sub_category_id: values.sub_category_id,
        amount: values.rateHandling,
        store_id: detailStore.id,
      };
      console.log('values', values);
      if (props.isEdit) {
        const rateHandlingId = props.initialValues?.rateHandlingId || null;
        const response = await dispatch(
          updateRateHandlingStock(data, rateHandlingId)
        );
      } else {
        const response = await dispatch(addRateHandlingStock(data));
      }
      props.onHide();
    } catch (err) {}
  };
  useEffect(() => {
    if (props.initialValues && props.show) {
      formik.setFieldValue(
        'sub_category_id',
        props.initialValues.sub_category_id
      );
      formik.setFieldValue('rateHandling', props.initialValues.amount);
      const initialSubCategory = listSubCategory?.data?.filter(
        (item) => item.id === props.initialValues.sub_category_id
      );
      const initValue = initialSubCategory.length
        ? { value: initialSubCategory[0].id, label: initialSubCategory[0].name }
        : {};
      setDefaultSubCategory(initValue);
    }
  }, [props.initialValues, props.show]);
  const { isLoading } = useSelector((state) => state.loading);
  const currencyToNumber = (value = '0.0') => value.split('.').join('');

  return (
    <>
      <Modal
        titleModal={
          props.isEdit
            ? 'Edit Rate Handling Stock'
            : 'Tambah Rate Handling Stock'
        }
        titleClassName="title-modal"
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <ReactSelect
              label="Sub Category"
              name="sub_category_id"
              formik={formik}
              withValidate
              options={listSubCategory?.data?.map((item) => ({
                value: item.id,
                label: item.name,
              }))}
              placeholder="Pilih Sub Category"
              className="mt-3 mb-2"
              value={props.isEdit && defaultSubCategory}
            />
            {/* <TextField
              label="Rate Handling Stock"
              prefix={() => 'Rp'}
              placeholder="Input Rate Handling Stock"
              name="amount"
              formik={formik}
              withValidate
            /> */}
            <CurrencyInput
              label="Rate Handling"
              placeholder="Input rate handling sub kategori"
              withValidate
              formik={formik}
              name="rateHandling"
              className="mb-3 mt-3"
            />
            {/* <InputGroup>
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">Rp</InputGroup.Text>
              </InputGroup.Prepend>
              <Form.Control
                placeholder="Input rate handling"
                {...formik.getFieldProps('rateHandling')}
                style={{
                  border:
                    formik.touched.rateHandling &&
                    formik.errors.rateHandling &&
                    '1px solid #ee1b25',
                }}
                value={
                  typeof Number(
                    formik.getFieldProps('rateHandling').value
                  ) === 'number'
                    ? Number(
                      formik.getFieldProps('rateHandling').value
                    ).toLocaleString('id')
                    : rateHandling
                }
                aria-label="rateHandling"
                aria-describedby="basic-addon1"
                onChange={(e) => {
                  setRateHandling(currencyToNumber(e.target.value));
                  formik.setFieldValue(
                    'rateHandling',
                    currencyToNumber(e.target.value)
                  );
                }}
              />
            </InputGroup>
            {formik.errors.rateHandling && (
                <Form.Text className="text-input-error">
                  {formik.errors.rateHandling}
                </Form.Text>
              )} */}
          </>
        )}
        footer={(data) => (
          <>
            <Button
              className="mr-2"
              text="Batal"
              styleType="lightBlueFill"
              onClick={props.onHide}
            />
            <Button
              text={
                isLoading ? 'Loading...' : props.isEdit ? 'Simpan' : 'Tambah'
              }
              styleType="blueFill"
              onClick={formik.handleSubmit}
            />
          </>
        )}
      />
    </>
  );
};

ModalRateHandling.defaultProps = {
  isEdit: false,
  initialValues: null,
};
export default ModalRateHandling;
