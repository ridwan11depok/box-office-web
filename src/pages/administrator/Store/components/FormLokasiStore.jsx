import React, { useEffect, useState } from 'react';
import {
  TextArea,
  ReactSelect,
} from '../../../../components/elements/InputField';
import { Row, Col, Form } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../../../../components/elements/Button';
import { useSelector, useDispatch } from 'react-redux';
import { getArea } from '../../Gudang/reduxAction';
import MapModal from '../../../../components/elements/Map/MapModal';
import MapOutlinedIcon from '@material-ui/icons/MapOutlined';

const useStyles = makeStyles({
  label: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#4F4F4F',
    marginBottom: '4px',
    lineHeight: '20px'
  },
  value: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#363636',
  },
});

function FormLokasiStore(props) {
  const classes = useStyles();
  const { formik } = props;
  const [state, setState] = useState({
    provice_id: 0,
    city_id: 0,
    showMap: false,
    defaultProvince: null,
    defaultCity: null,
  });
  const dispatch = useDispatch();
  const { listProvince, listCity } = useSelector(
    (state) => state.gudangAdministratorReducer
  );

  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);
  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: Number(e.target.value) });
    dispatch(getArea(Number(e.target.value)));
  };
  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
  };

  useEffect(() => {
    if (props.initialValues) {
      const init = { ...props.initialValues };
      formik.setFieldValue('address', init.store_address);
      formik.setFieldValue('province_id', init.province_id);
      formik.setFieldValue('city_id', init.city_id);
      formik.setFieldValue('longitude', init.longitude);
      formik.setFieldValue('latitude', init.latitude);

      dispatch(getArea(init.province_id)).then((res) => {
        if (res?.result?.length) {
          const cities = res.result.map((city) => ({
            value: city.id,
            label: city.name,
          }));
          const dataProvince = listProvince.find((e) => {
            return e.value === init.province_id;
          });
          const detailCity = cities.find((e) => {
            return e.value === init.city_id;
          });
          setState({
            ...state,
            defaultProvince: dataProvince,
            defaultCity: detailCity,
          });
        }
      });
    }
  }, [props.initialValues]);

  const selectCoordinate = (coordinate) => {
    formik.setFieldValue('longitude', coordinate.lng);
    formik.setFieldValue('latitude', coordinate.lat);
    setState({ ...state, showMap: false });
  };
  return (
    <div>
      {props.isEdit ? (
        <>
          <Form.Label as={'h6'} className="input-label">
            Pilih Koordinat
          </Form.Label>
          <div className="background-map mb-3">
            <div
              className="d-flex flex-column flex-lg-row justify-content-between align-items-center w-100 p-3"
              style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
            >
              <span
                style={{
                  fontSize: '14px',
                  fontWeight: '600',
                  fontFamily: 'Open Sans',
                }}
              >
                Tandai lokasi toko Anda.
              </span>
              <Button
                style={{ width: '200px', backgroundColor: '#FAFAFF' }}
                styleType="blueNoFill"
                text="Tandai Lokasi"
                onClick={() => setState({ ...state, showMap: true })}
              />
            </div>
          </div>
          {formik.touched.latitude && formik.errors.latitude ? (
            <Form.Text className="text-input-error mb-3 mt-0">
              {formik.errors.latitude}
            </Form.Text>
          ) : props.errorServer?.latitude?.[0] ||
            props.errorServer?.longitude?.[0] ? (
            <Form.Text className="text-input-error mb-3 mt-0">
              {props.errorServer?.latitude?.[0] ||
                props.errorServer?.longitude?.[0]}
            </Form.Text>
          ) : null}
          
          <Row>
            <Col>
              <ReactSelect
                onChange={handleChangeProvince}
                label="Provinsi"
                value={state.defaultProvince ?? state.defaultProvince}
                options={listProvince}
                name="province_id"
                formik={formik}
                withValidate
                placeholder="Pilih Provinsi"
                errorServer={props.errorServer?.province_id?.[0] || ''}
              />
            </Col>
            <Col>
              <ReactSelect
                onChange={handleChangeCity}
                value={state.defaultCity ?? state.defaultCity}
                label="Kota"
                name="city_id"
                formik={formik}
                withValidate
                options={listCity}
                placeholder="Pilih Kota"
                errorServer={props.errorServer?.city_id?.[0] || ''}
              />
            </Col>
          </Row>

          <TextArea
            className="mb-3 mt-3"
            label="Alamat"
            placeholder="Input alamat"
            rows={10}
            withValidate
            name="address"
            formik={formik}
            withValidate
            errorServer={props.errorServer?.store_address?.[0] || ''}
          />

          <MapModal
            show={state.showMap}
            onHide={() => setState({ ...state, showMap: false })}
            onAgree={selectCoordinate}
            initialCoordinate={{
              lng: props.initialValues?.longitude,
              lat: props.initialValues?.latitude,
            }}
          />
        </>
      ) : (
        <div>
          <h6 className={classes.label}>Alamat</h6>
          <h6 className={classes.value}>
            {props.initialValues?.store_address || 'No Address'}
          </h6>
          <Button
            styleType="blueNoFill"
            className="pl-0"
            text="Lihat Lokasi di Peta"
            startIcon={() => <MapOutlinedIcon />}
            onClick={() => setState({ ...state, showMap: true })}
          />
          <MapModal
            show={state.showMap}
            onHide={() => setState({ ...state, showMap: false })}
            initialCoordinate={{
              lng: props.initialValues?.longitude,
              lat: props.initialValues?.latitude,
            }}
            mode="viewer"
          />
        </div>
      )}
    </div>
  );
}

FormLokasiStore.defaultProps = {
  initialValues: null,
  isEdit: true,
  errorServer: null,
};

export default FormLokasiStore;
