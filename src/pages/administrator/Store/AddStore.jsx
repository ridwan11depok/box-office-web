import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import { makeStyles } from '@material-ui/core/styles';
import BackButton from '../../../components/elements/BackButton';
import Button from '../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import FormInfoStore from './components/FormInfoStore';
import FormLokasiStore from './components/FormLokasiStore';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { addStore } from './reduxAction';

const useStyles = makeStyles({
  selectWarehouse: {
    color: '#1c1c1c',
    fontSize: '14px',
    fontWeight: '600',
    fontFamily: 'Open Sans',
    marginRight: '10px',
  },
});

const validationSchema = Yup.object({
  logo: Yup.mixed().required('Harap memasukkan logo store'),
  name: Yup.string().required('Harap untuk mengisi nama store'),
  type: Yup.string().required('Harap untuk memilih tipe store'),
  address: Yup.string().required('Harap untuk mengisi alamat store'),
  province_id: Yup.number().required('Harap untuk memilih provinsi'),
  city_id: Yup.number().required('Harap untuk memilih kabupaten/kota'),
  longitude: Yup.number().required('Harap untuk menandai lokasi di peta'),
  latitude: Yup.number().required('Harap untuk menandai lokasi di peta'),
});

const AddStore = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [errors, setErrors] = useState(null);

  const { isLoading } = useSelector((state) => state.loading);

  const formik = useFormik({
    initialValues: {
      logo: '',
      name: '',
      type: '',
      address: '',
      province_id: '',
      city_id: '',
      longitude: '',
      latitude: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });
  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('store_logo', values.logo);
      data.append('store_name', values.name);
      data.append('type', values.type);
      data.append('store_address', values.address);
      data.append('province_id', values.province_id);
      data.append('city_id', values.city_id);
      data.append('longitude', values.longitude);
      data.append('latitude', values.latitude);
      const res = await dispatch(addStore(data));
      history.push(`/admin/store/detail/${res?.result?.id}`);
    } catch (err) {
      setErrors(err?.error?.errors || null);
    }
  };
  return (
    <>
      <ContentContainer
        renderHeader={() => (
          <div className="d-flex flex-row justify-content-between align-items-center">
            <BackButton
              label="Tambah Store"
              onClick={() => history.push('/admin/store')}
            />
          </div>
        )}
      >
        <ContentItem
          spaceRight="0 pr-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3" style={{ borderBottom: '1px solid #E8E8E8' }}>
            <h6 className="mb-0">Info Store</h6>
          </div>
          <div className="p-3">
            <FormInfoStore formik={formik} errorServer={errors} />
          </div>
        </ContentItem>
        <ContentItem
          spaceLeft="0 pl-md-2"
          col="col-12 col-md-6"
          spaceBottom={3}
          style={{ minHeight: '615px' }}
        >
          <div className="p-3">
            <FormLokasiStore formik={formik} errorServer={errors} />
          </div>
        </ContentItem>

        <div className="ml-auto">
          <Button
            style={{ width: '140px', marginRight: '10px' }}
            styleType="lightBlueFill"
            text="Batal"
            onClick={() => {
              formik.resetForm();
            }}
          />
          <Button
            style={{ width: '140px' }}
            styleType="blueFill"
            text={isLoading ? 'Loading...' : 'Simpan'}
            onClick={(e) => formik.handleSubmit(e)}
          />
        </div>
      </ContentContainer>
    </>
  );
};

export default AddStore;
