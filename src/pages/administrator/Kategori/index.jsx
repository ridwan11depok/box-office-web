import React from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import CategoryTable from './components/CategoryTable';

const CategoryAdministrator = () => {
  return (
    <>
      <ContentContainer title="Kategori">
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <CategoryTable />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default CategoryAdministrator;
