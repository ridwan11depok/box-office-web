import React, { useState, useEffect } from 'react';
import {
  ContentContainer,
  ContentItem,
} from '../../../components/elements/BaseContent';
import Button from '../../../components/elements/Button';
import BackButton from '../../../components/elements/BackButton';
import { useHistory, useParams } from 'react-router';
import CategoryDetailTable from './components/CategoryDetailTable';
import { useSelector, useDispatch } from 'react-redux';
import ModalDeleteCategory from './components/ModalDeleteCategory';
import { deleteCategory, updateCategory } from './reduxAction';
import ModalAddCategory from './components/ModalAddCategory';
import { getListOrDetailCategory } from './reduxAction';

const CategoryDetail = () => {
  const [state, setState] = useState({
    modalDelete: false,
    modalEdit: false,
  });
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();

  const { detailCategory } = useSelector(
    (state) => state.categoryAdministratorReducer
  );

  const handleDeleteCategory = async () => {
    try {
      await dispatch(deleteCategory(detailCategory.id));
      setState({ ...state, modalDelete: false });
      history.goBack();
    } catch (er) {}
  };

  const handleUpdateCategory = async (payload) => {
    try {
      await dispatch(updateCategory(payload, detailCategory.id));
      setState({ ...state, modalEdit: false });
    } catch (er) {}
  };

  useEffect(() => {
    if (params?.id) {
      dispatch(getListOrDetailCategory({}, params?.id));
    }
  }, [params?.id]);

  return (
    <ContentContainer
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label={detailCategory?.name}
            onClick={() => history.push('/admin/category')}
          />
          <div>
            <Button
              style={{ minWidth: '120px' }}
              styleType="redOutline"
              text="Hapus"
              onClick={() => setState({ ...state, modalDelete: true })}
            />
            <Button
              style={{ minWidth: '120px' }}
              className="ml-2"
              styleType="blueOutline"
              text="Edit"
              onClick={() => setState({ ...state, modalEdit: true })}
            />
          </div>
        </div>
      )}
    >
      <ContentItem border={false} col="col-12" spaceBottom={3}>
        <CategoryDetailTable />
      </ContentItem>
      <ModalDeleteCategory
        show={state.modalDelete}
        onHide={() => setState({ ...state, modalDelete: false })}
        onAgree={handleDeleteCategory}
        data={detailCategory}
      />
      <ModalAddCategory
        show={state.modalEdit}
        onHide={() => setState({ ...state, modalEdit: false })}
        initialValues={detailCategory}
        isEdit={true}
        onAgree={handleUpdateCategory}
      />
    </ContentContainer>
  );
};

export default CategoryDetail;
