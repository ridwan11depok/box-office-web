import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import {
  getListSubCategoryOrDetailSubCategory,
  deleteSubCategory,
  updateSubCategory,
} from '../reduxAction';
import { SearchField } from '../../../../components/elements/InputField';
import ModalAddSubCategory from './ModalAddSubCategory';
import ModalDeleteSubCategory from './ModalDeleteSubCategory';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { formatRupiah } from '../../../../utils/text';

const useStyles = makeStyles({
  textEdit: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#1C1C1C',
  },
  textDelete: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    color: '#DA101A',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
});

const ActionField = ({ data, onClickEdit, onClickDelete }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        placement="bottom"
      >
        <MenuItem
          onClick={() => {
            onClickEdit(data);
            handleClose();
          }}
          className={classes.textEdit}
        >
          Edit
        </MenuItem>
        <MenuItem
          onClick={() => {
            onClickDelete(data);
            handleClose();
          }}
          className={classes.textDelete}
        >
          Delete
        </MenuItem>
      </Menu>
      <Button styleType="lightBlueFill" onClick={handleClick} isOptionButton />
    </div>
  );
};

const EmptySubCategory = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada sub kategori</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const classes = useStyles();
  const { handleAddSubCategory, handleSearch } = props;
  const styles = {
    control: (css) => ({
      ...css,
      width: '200px',
      backgroundColor: 'transparent',
      border: '1px solid #1C1C1C',
    }),
  };
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField
          placeholder="Cari Sub Kategori"
          onChange={(e) =>
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500)
          }
        />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType="blueFill"
          text="Tambah Sub Kategori"
          onClick={handleAddSubCategory}
        />
      </div>
    </div>
  );
};

function CategoryDetailTable(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();

  const { listSubCategory, addSubCategory, detailSubCategory } = useSelector(
    (state) => state.categoryAdministratorReducer
  );
  const { isLoading } = useSelector((state) => state.loading);

  const [state, setState] = useState({
    showAddSubCategory: false,
    showDeleteSubCategory: false,
    search: '',
    showEditSubCategory: false,
    itemSeletected: null,
  });

  const handleHideModalAddSubCategory = () => {
    setState({ ...state, showAddSubCategory: false });
  };

  const fetchListSubCategory = async (payload, idCategory) => {
    payload.category_product_id = idCategory;
    dispatch(getListSubCategoryOrDetailSubCategory(payload));
  };

  const handleAddSubCategory = () => {
    setState({ ...state, showAddSubCategory: true });
  };

  const handleDeleteSubCategory = async () => {
    try {
      let idSubCategory = state.itemSeletected?.id || null;
      let idCategory = params?.id || null;
      await dispatch(deleteSubCategory(idSubCategory, idCategory));
      setState({ ...state, showDeleteSubCategory: false });
    } catch (err) {}
  };

  const handleEditSubCategory = async (values) => {
    try {
      let idSubCategory = state.itemSeletected?.id || null;
      let idCategory = params?.id || null;
      let payload = {
        name: values.nameSubCategory,
        rate_handling_stock: values.rateHandling,
        category_product_id: idCategory,
      };
      await dispatch(updateSubCategory(payload, idSubCategory));
      setState({ ...state, showEditSubCategory: false });
    } catch (err) {}
  };

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const handleEdit = (item) =>
    setState({
      ...state,
      itemSeletected: item,
      showEditSubCategory: true,
    });
  return (
    <div>
      <DataTable
        action={(data) => {
          if (params?.id) fetchListSubCategory(data, params?.id);
        }}
        totalPage={listSubCategory?.last_page || 5}
        params={{ search: state.search }}
        column={[
          {
            heading: 'Nama Sub Kategori',
            key: 'name',
          },
          {
            heading: 'Rate Handling',
            key: 'rate_handling_stock_in_rupiah',
          },
          {
            heading: 'Aksi',
            render: (data) => (
              <ActionField
                data={data}
                onClickDelete={(data) =>
                  setState({
                    ...state,
                    itemSeletected: data,
                    showDeleteSubCategory: true,
                  })
                }
                onClickEdit={handleEdit}
                // onClickEdit={(item) => {
                //   setState({
                //     ...state,
                //     itemSeletected: item,
                //     showEditSubCategory: true,
                //   });
                // }}
              />
            ),
          },
        ]}
        data={listSubCategory?.data}
        transformData={(item) => ({
          ...item,
          rate_handling_stock_in_rupiah: formatRupiah(item.rate_handling_stock),
        })}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddSubCategory={handleAddSubCategory}
            handleSearch={handleSearch}
          />
        )}
        renderEmptyData={() => (
          <EmptySubCategory onClick={handleAddSubCategory} />
        )}
      />
      <ModalAddSubCategory
        show={state.showAddSubCategory}
        onHide={handleHideModalAddSubCategory}
        onSuccess={() => {
          fetchListSubCategory(
            {
              page: 1,
              per_page: 10,
            },
            params?.id
          );
        }}
        idCategory={params?.id}
      />
      <ModalDeleteSubCategory
        show={state.showDeleteSubCategory}
        onHide={() => setState({ ...state, showDeleteSubCategory: false })}
        onAgree={handleDeleteSubCategory}
        data={state.itemSeletected}
      />
      <ModalAddSubCategory
        isEdit={true}
        initialValues={state.itemSeletected}
        show={state.showEditSubCategory}
        onHide={() =>
          setState({
            ...state,
            showEditSubCategory: false,
            itemSeletected: null,
          })
        }
        onAgree={handleEditSubCategory}
      />
    </div>
  );
}

export default CategoryDetailTable;
