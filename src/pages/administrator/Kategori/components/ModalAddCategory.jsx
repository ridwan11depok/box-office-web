import React, { useEffect, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form } from 'react-bootstrap';
import { addCategory } from '../reduxAction';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const validationSchema = Yup.object({
  nameCategory: Yup.string().required('Nama Kategori Tidak Boleh Kosong'),
});

const ModalAddCategory = (props) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      nameCategory: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (props.isEdit) {
        props.onAgree({ name: values.nameCategory });
      } else {
        handleSubmit(values);
      }
    },
  });

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);

  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('name', values.nameCategory);
      const response = await dispatch(addCategory(data));
      props.onSuccess();
      props.onHide();
    } catch (err) {}
  };

  useEffect(() => {
    if (props.initialValues && props.isEdit) {
      formik.setFieldValue('nameCategory', props.initialValues.name);
    }
  }, [props.initialValues, props.isEdit]);

  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal={props.isEdit ? 'Edit Kategori' : 'Tambah Kategory'}
        titleClassName="title-modal"
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <Form.Group className="mb-1 mt-3" controlId="formBasicEmail">
              <Form.Label as={'h6'} className="input-label">
                Nama Kategori
              </Form.Label>
              <Form.Control
                type=""
                placeholder="Input Nama Ketegori"
                {...formik.getFieldProps('nameCategory')}
                style={{
                  border:
                    formik.touched.nameCategory &&
                    formik.errors.nameCategory &&
                    '1px solid #ee1b25',
                }}
              />
              {formik.touched.nameCategory && formik.errors.nameCategory && (
                <Form.Text className="text-input-error">
                  {formik.errors.nameCategory}
                </Form.Text>
              )}
            </Form.Group>
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                // if (!Boolean(formik.values.role)) {
                //   formik.setFieldTouched('role', true);
                // }
                formik.handleSubmit(e);
              }}
              // disabled={formik.isSubmitting}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading ? 'Loading...' : props.isEdit ? 'Simpan' : 'Tambah'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalAddCategory.defaultProps = {
  isEdit: false,
  initialValues: null,
};
export default ModalAddCategory;
