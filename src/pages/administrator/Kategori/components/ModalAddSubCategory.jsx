import React, { useEffect } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { addSubCategory } from '../reduxAction';
import { useDispatch, useSelector } from 'react-redux';
import {
  CurrencyInput,
  TextField,
} from '../../../../components/elements/InputField';

const validationSchema = Yup.object({
  nameSubCategory: Yup.string().required(
    'Nama Sub Kategori Tidak Boleh Kosong'
  ),
  rateHandling: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .required('Rate Handling Tidak Boleh Kosong'),
});

const ModalAddSubCategory = (props) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      nameSubCategory: '',
      rateHandling: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (props.isEdit) {
        props.onAgree(values);
      } else {
        handleSubmit(values);
      }
    },
  });

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
  }, [props.show]);

  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('name', values.nameSubCategory);
      data.append('rate_handling_stock', values.rateHandling);
      data.append('category_product_id', props.idCategory);

      const response = await dispatch(addSubCategory(data));
      props.onSuccess();
      props.onHide();
    } catch (err) {}
  };
  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
    }
    if (props.initialValues && props.show) {
      const init = { ...props.initialValues };
      formik.setFieldValue('nameSubCategory', init.name);
      formik.setFieldValue('rateHandling', init.rate_handling_stock);
    }
  }, [props.initialValues, props.show]);

  const { isLoading } = useSelector((state) => state.loading);
  return (
    <>
      <Modal
        titleModal={props.isEdit ? 'Edit Sub Kategori' : 'Tambah Sub Kategori'}
        titleClassName="title-modal"
        bodyClassName="body-modal pb-3"
        show={props.show}
        onHide={props.onHide}
        content={(data) => (
          <>
            <TextField
              label="Nama Sub Kategori"
              placeholder="Input Nama Ketegori"
              formik={formik}
              name="nameSubCategory"
              withValidate
              className="mb-3 mt-3"
            />
            <CurrencyInput
              label="Rate Handling"
              placeholder="Input rate handling sub kategori"
              // onChange={(e) => console.log(e.target.value)}
              withValidate
              formik={formik}
              name="rateHandling"
              className="mb-3 mt-3"
            />
          </>
        )}
        footer={(data) => (
          <>
            <button
              className="mr-2"
              style={{
                backgroundColor: '#E8E8E8',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#192A55',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                // if (!Boolean(formik.values.role)) {
                //   formik.setFieldTouched('role', true);
                // }
                formik.handleSubmit(e);
              }}
              //   disabled={formik.isSubmitting}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading ? 'Loading...' : props.isEdit ? 'Simpan' : 'Tambah'}
            </button>
          </>
        )}
      />
    </>
  );
};

ModalAddSubCategory.defaultProps = {
  isEdit: false,
  initialValues: null,
};
export default ModalAddSubCategory;
