import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import Select from 'react-select';
import { getListOrDetailCategory } from '../reduxAction';
import { SearchField } from '../../../../components/elements/InputField';
import ModalAddCategory from './ModalAddCategory';

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const ActionField = ({ data }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  return (
    <>
      <Button
        text="Detail"
        styleType="lightBlueFill"
        onClick={() => {
          history.push(`/admin/category/detail/${data?.id}`);
        }}
      />
    </>
  );
};

const EmptyCategory = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada data kategori saat ini.</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const classes = useStyles();
  const { handleAddCategory, handleSearch } = props;
  const styles = {
    control: (css) => ({
      ...css,
      width: '200px',
      backgroundColor: 'transparent',
      border: '1px solid #1C1C1C',
    }),
  };
  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField
          placeholder="Cari Kategori"
          onChange={(e) =>
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500)
          }
        />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType="blueFill"
          text="Tambah Kategori"
          onClick={handleAddCategory}
        />
      </div>
    </div>
  );
};

function CategoryTable(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listCategory, addCategory } = useSelector(
    (state) => state.categoryAdministratorReducer
  );
  const { isLoading } = useSelector((state) => state.loading);

  const [state, setState] = useState({
    showBanner: false,
    showAddCategory: false,
    search: '',
  });

  useEffect(() => {
    if (addCategory.status === 'Success') {
      setState({ ...state, showBanner: true });
    }
  }, [addCategory.status]);

  const handleHideModalAddCategory = () => {
    setState({ ...state, showAddCategory: false });
  };

  const fetchListCategory = async (payload) => {
    dispatch(getListOrDetailCategory(payload));
  };

  const handleAddCategory = () => {
    setState({ ...state, showAddCategory: true });
    // history.push('/admin/warehouse/add');
  };

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };
  return (
    <div>
      <DataTable
        action={fetchListCategory}
        totalPage={listCategory?.last_page || 5}
        params={{ search: state.search }}
        column={[
          {
            heading: 'Name Kategori',
            key: 'name',
          },
          {
            heading: 'Jumlah Sub Kategori',
            key: 'sub_category_product_count',
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={listCategory?.data}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleAddCategory={handleAddCategory}
            handleSearch={handleSearch}
          />
        )}
        renderEmptyData={() => <EmptyCategory onClick={handleAddCategory} />}
        banner={{
          isShow: state.showBanner,
          info: 'Berhasil Menambah Kategori',
        }}
      />
      <ModalAddCategory
        show={state.showAddCategory}
        onHide={handleHideModalAddCategory}
        onSuccess={() => fetchListCategory({ page: 1, per_page: 10 })}
      />
    </div>
  );
}

export default CategoryTable;
