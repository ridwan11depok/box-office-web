import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

export const getListOrDetailCategory =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('category', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'category',
          data,
          headers,
          params
        );
      }
      if (response) {
        if (id) {
          dispatch({
            type: actionType.DETAIL_CATEGORY,
            payload: response.result,
          });
        } else {
          dispatch({
            type: actionType.LIST_CATEGORY,
            payload: response.result,
          });
        }
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      console.log('error', err);
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
    }
  };

export const addCategory = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('category', data, headers);
    console.log('response add category', response);
    dispatch({
      type: actionType.ADD_CATEGORY,
      payload: 'Success',
    });
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambahkan kategori baru.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch({
      type: actionType.ADD_CATEGORY,
      payload: 'Failed',
    });
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal menambahkan category baru.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const deleteCategory =
  (payload = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id = payload;
      const response = await consume.delete('category', data, headers, id);
      dispatch({
        type: actionType.DELETE_CATEGORY,
        payload: {
          status: 'Success',
          result: response,
        },
      });

      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus kategori.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch({
        type: actionType.DELETE_CATEGORY,
        payload: {
          status: 'Failed',
          result: err,
        },
      });
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus kategori.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const getListSubCategoryOrDetailSubCategory =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let response;
      if (id) {
        response = await consume.get('subCategory', data, headers, id);
      } else {
        response = await consume.getWithParams(
          'subCategory',
          data,
          headers,
          params
        );
      }
      // const response = await consume.get('warehouse', data, headers, id);
      if (response) {
        if (id) {
          // dispatch(detailGudang(response.result));
        } else {
          dispatch({
            type: actionType.LIST_SUB_CATEGORY,
            payload: response.result,
          });
        }
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
    } catch (err) {
      console.log('error', err);
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
    }
  };

export const addSubCategory = (payload) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token, content_type: 'multipart/form-data' };

    const response = await consume.post('subCategory', data, headers);
    dispatch({
      type: actionType.ADD_SUB_CATEGORY,
      payload: 'Success',
    });
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil menambahkan sub kategori baru.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch({
      type: actionType.ADD_SUB_CATEGORY,
      payload: 'Failed',
    });
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal menambahkan sub category baru.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const clearSubCategory = () => async (dispatch) => {
  dispatch({
    type: actionType.CLEAR_SUB_CATEGORY,
    payload: null,
  });
};

export const updateCategory = (payload, id) => async (dispatch, getState) => {
  try {
    dispatch(setLoading(true));
    const { token } = getState().login;
    let data = payload;
    let headers = { token: token };
    let params = {};

    const response = await consume.put('category', data, headers, params, id);
    dispatch(setLoading(false));
    dispatch(getListOrDetailCategory({}, id));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Berhasil memperbarui data kategori.',
        type: 'success',
      })
    );
    return Promise.resolve('success');
  } catch (err) {
    dispatch(setLoading(false));
    dispatch(
      setToast({
        isShow: true,
        messages: 'Gagal memperbarui data kategori.',
        type: 'error',
      })
    );
    return Promise.reject('failed');
  }
};

export const deleteSubCategory =
  (idSubCategory, idCategory) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let id = idSubCategory;
      const response = await consume.delete('subCategory', data, headers, id);
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menghapus sub kategori.',
          type: 'success',
        })
      );
      dispatch(
        getListSubCategoryOrDetailSubCategory({
          page: 1,
          per_page: 10,
          category_product_id: idCategory,
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menghapus sub kategori.',
          type: 'error',
        })
      );
      return Promise.reject('failed');
    }
  };

export const updateSubCategory =
  (payload, id) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = {};
      let headers = { token: token };
      let params = payload;
      const response = await consume.put(
        'subCategory',
        data,
        headers,
        params,
        id
      );
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil memperbarui data sub kategori.',
          type: 'success',
        })
      );
      dispatch(
        getListSubCategoryOrDetailSubCategory({
          page: 1,
          per_page: 10,
          category_product_id: payload.category_product_id,
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal memperbarui data sub kategori.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };
