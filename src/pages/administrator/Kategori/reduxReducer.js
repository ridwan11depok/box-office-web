import actionType from './reduxConstant';

const initialState = {
  listCategory: null,
  detailCategory: {},
  addCategory: { status: null },
  deleteCategory: { status: null },
  listSubCategory: null,
  detailSubCategory: {},
  addSubCategory: { status: null },
  deleteSubCategory: { status: null },
};

const categoryAdministratorReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_CATEGORY:
      return {
        ...prevState,
        listCategory: action.payload,
      };
    case actionType.DETAIL_CATEGORY:
      return {
        ...prevState,
        detailCategory: action.payload,
      };
    case actionType.ADD_CATEGORY:
      return {
        ...prevState,
        addCategory: action.payload,
      };
    case actionType.DELETE_CATEGORY:
      return {
        ...prevState,
        deleteCategory: action.payload,
      };
    case actionType.LIST_SUB_CATEGORY:
      return {
        ...prevState,
        listSubCategory: action.payload,
      };
    case actionType.CLEAR_SUB_CATEGORY:
      return {
        ...prevState,
        listSubCategory: action.payload

      }
    case actionType.DETAIL_SUB_CATEGORY:
      return {
        ...prevState,
        detailSubCategory: action.payload,
      };
    case actionType.ADD_SUB_CATEGORY:
      return {
        ...prevState,
        addSubCategory: action.payload,
      };
    case actionType.DELETE_SUB_CATEGORY:
      return {
        ...prevState,
        deleteSubCategory: action.payload,
      };
    default:
      return prevState;
  }
};

export default categoryAdministratorReducer;
