import React, { useEffect, useRef, useState } from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';
import {
  ContentContainer as Container,
  ContentItem as Item,
} from '../../../../components/elements/BaseContent';
import { Col, Form, Row, InputGroup } from 'react-bootstrap';
import {
  TextField,
  SelectField,
} from '../../../../components/elements/InputField';
import pictureIcon from '../../../../assets/icons/picture_icon.svg';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { validateFile } from '../../../../utils/validate';
import CurrencyInput from 'react-currency-input-field';
import { useSelector, useDispatch } from 'react-redux';
import { updatePackaging } from '../reduxAction';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
});

const ModalEditPackaging = (props) => {
  const classes = useStyles();
  const { isLoading } = useSelector((state) => state.loading);
  const dispatch = useDispatch();

  const [state, setState] = useState({
    image: '',
  });

  const [errorServer, setErrorServer] = useState(null);

  const inputImage = useRef(null);

  const handleChangeImage = async (e) => {
    const files = e.target.files;
    const response = await validateFile({
      files,
      allowedExtensions: ['jpg', 'jpeg', 'png'],
    });
    if (response) {
      setState({ ...state, image: e.target.files[0] });
      formik.setFieldValue('image', response);
    }
  };

  const validationSchema = Yup.object({
    name: Yup.string().required('Harap untuk mengisi nama packaging'),
    length: Yup.number()
      .typeError('Panjang harus berupa angka')
      .min(1, 'Panjang Minimal 1 cm atau lebih')
      .required('Harap untuk mengisi panjang packing'),
    width: Yup.number()
      .typeError('Lebar harus berupa angka')
      .min(1, 'Lebar Minimal 1 cm atau lebih')
      .required('Harap untuk mengisi lebar packing'),
    type: Yup.string().default('Large'),
    height: Yup.number().when('type', {
      is: (value) => value === 'Volume',
      then: Yup.number()
        .typeError('Tinggi harus berupa angka')
        .min(1, 'Tinggi Minimal 1 cm atau lebih')
        .required('Harap untuk mengisi nilai tinggi'),
    }),
    default_price: Yup.string().required('Harap untuk mengisi nilai barang'),
    image: Yup.object(),
  });

  const formik = useFormik({
    initialValues: {
      name: '',
      length: '',
      width: '',
      type: '',
      height: '',
      default_price: '',
      image: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  useEffect(() => {
    if (props?.data) {
      const init = props?.data;
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('type', init.type);
      formik.setFieldValue('default_price', init.default_price);
      init?.height
        ? formik.setFieldValue('height', init.height)
        : formik.setFieldValue('height', '');
      formik.setFieldValue('length', init.length);
      formik.setFieldValue('width', init.width);
    }
  }, [props]);

  const handleSubmit = async (values) => {
    try {
      const data = new FormData();
      data.append('name', values.name);
      data.append('type', values.type);
      data.append('length', values.length);
      data.append('width', values.width);
      data.append('height', values.height);
      data.append('default_price', values.default_price);
      if (values.image) {
        data.append('image', values.image.file);
      }
      await dispatch(updatePackaging(data, props?.data?.id));
      props.onHide();
      setState({ ...state, image: '' });
      formik.resetForm();
    } catch (err) {
      if (err?.code === 422) {
        setErrorServer(err?.error?.errors);
      }
    }
  };

  const renderError = (formikErrorCondition, errorServerCondition) => {
    if (formikErrorCondition && errorServerCondition) {
      return (
        <Form.Text className="text-input-error">
          {errorServerCondition?.[0]}
        </Form.Text>
      );
    }
  };

  return (
    <>
      <input
        onChange={(e) => handleChangeImage(e)}
        ref={inputImage}
        type="file"
        className="d-none"
      />
      <Modal
        bodyClassName="pb-3"
        show={props.show}
        onHide={() => {
          props.onHide();
          formik.resetForm();
          setErrorServer(null);
        }}
        scrollable
        titleClassName="w-100"
        data={props.data}
        header={() => (
          <div className="d-flex flex-between w-100 align-items-center">
            Edit Packaging
          </div>
        )}
        content={(data) => (
          <>
            <div className="mt-3">
              <Item border={false} spaceBottom="3">
                <Form.Group className="mb-1" controlId="formBasicEmail">
                  <Form.Label as={'h6'} className="input-label">
                    Gambar Packaging
                  </Form.Label>
                  <div className="row">
                    <div className="col-12 col-lg-4">
                      <div
                        className="rounded d-flex justify-content-center align-items-center"
                        style={{
                          width: '100px',
                          height: '100px',
                          border: '2px dashed #CFCFCF',
                          backgroundColor: '#F2F2F2',
                          marginTop: '7px',
                          overflow: 'hidden',
                        }}
                      >
                        {state.image || props.data?.image ? (
                          <img
                            style={{ width: '100px' }}
                            src={
                              state.image
                                ? URL.createObjectURL(state.image)
                                : props.data?.image_full_url || ''
                            }
                            alt=""
                          />
                        ) : (
                          <img src={pictureIcon} alt="" />
                        )}
                      </div>
                    </div>
                    <div className="col-12 col-lg-8 pl-3">
                      <div
                        className="text-justify"
                        style={{
                          fontWeight: '400',
                          color: '#4F4F4F',
                          fontSize: '12px',
                        }}
                      >
                        <p>
                          Ukuran optimal 300 x 300 piksel dengan Besar file:
                          Maksimum 10.000.000 bytes (10 Megabytes). Ekstensi
                          file yang diperbolehkan: JPG, JPEG, PNG
                        </p>
                        <Button
                          ref={inputImage}
                          styleType="blueOutline"
                          onClick={() => inputImage.current?.click()}
                          text={state.image ? 'Ganti Foto' : 'Pilih Foto'}
                          style={{
                            width: '100%',
                            maxWidth: '296px',
                            height: '32px',
                          }}
                        />
                      </div>
                    </div>
                  </div>
                  {formik.touched.image && formik.errors.image && (
                    <Form.Text className="text-input-error">
                      {formik.errors.image}
                    </Form.Text>
                  )}
                  {renderError(!formik.errors.image, errorServer?.['image'])}
                </Form.Group>
              </Item>
            </div>
            <div className="mt-3">
              <Item border={false}>
                <TextField
                  label="Nama Packaging"
                  placeholder="Input nama packaging"
                  name="name"
                  formik={formik}
                  withValidate
                />
                {renderError(!formik.errors.name, errorServer?.['name'])}
              </Item>
            </div>
            <div className="mt-3">
              <Item border={false}>
                <Form.Group className="mt-2" controlId="formBasicEmail">
                  <Form.Label as={'h6'} className="input-label">
                    Ukuran Packaging
                  </Form.Label>
                  <Form.Text style={{ color: '#4F4F4F' }}>
                    Data ukuran packaging dapat disesuaikan bisa luas (panjang x
                    lebar) atau volume (panjang x lebar x tinggi)
                  </Form.Text>
                  <div className="d-flex flex-row">
                    {[
                      { name: 'Large', key: 1 },
                      { name: 'Volume', key: 2 },
                    ].map((item) => (
                      <div
                        onClick={() => {
                          formik.setFieldValue('type', item.name);
                        }}
                        className="text-center d-flex align-items-center justify-content-center mt-2 mr-2"
                        style={{
                          width: '120px',
                          height: '40px',
                          borderRadius: '40px',
                          border:
                            formik.values.type !== item.name
                              ? '2px solid #828282'
                              : '2px solid #192A55',
                          color:
                            formik.values.type === item.name
                              ? '#192A55'
                              : '#1C1C1C',
                          backgroundColor:
                            formik.values.type === item.name && '#EBF0FA',
                          fontSize: '14px',
                          fontFamily: 'Open Sans',
                          fontWeight: '700',
                          cursor: 'pointer',
                        }}
                      >
                        {item.name === 'Large' ? 'Luas' : 'Volume'}
                      </div>
                    ))}
                  </div>
                  {formik.errors.type && formik.touched.type && (
                    <Form.Text className="text-input-error">
                      {formik.errors.type}
                    </Form.Text>
                  )}
                </Form.Group>
              </Item>
            </div>
            <div className="mt-3">
              <Item border={false} className="d-flex flex-row">
                <div>
                  <TextField
                    label="Panjang"
                    defaultValue={props?.data?.length}
                    name="length"
                    className="w-100"
                    placeholder="Input panjang"
                    suffix={() => <span>cm</span>}
                    withValidate
                    formik={formik}
                  />
                  {renderError(!formik.errors.length, errorServer?.['length'])}
                </div>
                <div>
                  <TextField
                    label="Lebar"
                    name="width"
                    defaultValue={props?.data?.width}
                    className="w-100 ml-2"
                    placeholder="Input lebar"
                    suffix={() => <span>cm</span>}
                    withValidate
                    formik={formik}
                  />
                  {renderError(!formik.errors.width, errorServer?.['width'])}
                </div>
                {formik.getFieldProps('type').value === 'Volume' && (
                  <div className="ml-2">
                    <TextField
                      label="Tinggi"
                      name="height"
                      className="w-100 ml-2"
                      placeholder="Input tinggi"
                      suffix={() => <span>cm</span>}
                      withValidate
                      formik={formik}
                    />
                    {renderError(
                      !formik.errors.height,
                      errorServer?.['height']
                    )}
                  </div>
                )}
              </Item>
            </div>
            <div className="mt-3">
              <Item border={false}>
                <Form.Label as={'h6'} className="input-label">
                  Nilai Barang
                </Form.Label>
                <div
                  className="input-group mb-3 rounded"
                  style={{
                    border: '1px solid #1C1C1C',
                    height: '40px',
                    overflow: 'hidden',
                  }}
                >
                  <span
                    className="input-group-text"
                    id="basic-addon1"
                    style={{
                      border: 'none',
                      backgroundColor: '#F2F2F2',
                      color: '#4F4F4F',
                    }}
                  >
                    Rp
                  </span>
                  <CurrencyInput
                    style={{ border: 'none' }}
                    className="form-control"
                    placeholder="Input nilai barang"
                    defaultValue={formik.getFieldProps('default_price').value}
                    decimalsLimit={2}
                    decimalSeparator=","
                    groupSeparator="."
                    onValueChange={(value, name) =>
                      value
                        ? formik.setFieldValue('default_price', value)
                        : formik.setFieldValue('default_price', '')
                    }
                  />
                </div>
                <Form.Group className="mt-3 mb-3">
                  {formik.touched.default_price &&
                    formik.errors.default_price && (
                      <Form.Text className="text-input-error">
                        {formik.errors.default_price}
                      </Form.Text>
                    )}
                  {renderError(
                    !formik.errors.default_price,
                    errorServer?.['default_price']
                  )}
                </Form.Group>
              </Item>
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              onClick={() => {
                props.onHide();
                formik.resetForm();
                setErrorServer(null);
              }}
              text="Kembali"
              styleType="lightBlueFill"
            />
            <button
              onClick={(e) => {
                formik.handleSubmit(e);
              }}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading ? 'Loading...' : props.data ? 'Perbarui' : 'Tambah'}
            </button>
          </>
        )}
        footerClassName="p-3"
      />
    </>
  );
};

export default ModalEditPackaging;
