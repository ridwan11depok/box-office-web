import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from '../../../../components/elements/Table/Table';
import { useSelector, useDispatch } from 'react-redux';
import Button from '../../../../components/elements/Button';
import { useHistory } from 'react-router-dom';
import { getPackaging } from '../reduxAction'
import { SearchField } from '../../../../components/elements/InputField';
import ModalDetailPackaging from './ModalDetailPackaging';
import ModalDeletePackaging from './ModalDeletePackaging'
import ModalEditPackaging from './ModalEditPackaging';
import ModalAddPackaging from './ModalAddPackaging'

const useStyles = makeStyles({
  label: {
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  description: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
  },
  titleTable: {
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: 'Work Sans',
  },
  inputSearch: {
    border: '1px solid #828282',
    borderRadius: '4px',
    height: '35px',
    '&>::placehoder': {
      color: '#828282',
      fontSize: '14px',
      fontWeight: '400',
    },
  },
});

const EmptyGudang = ({ onClick }) => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: '30vh' }}
    >
      <h6 className={classes.description}>Tidak ada packaging saat ini</h6>
    </div>
  );
};

const HeaderTable = (props) => {
  const { handleOpenModal, handleSearch } = props;

  return (
    <div className="d-flex flex-row justify-content-between align-items-center pt-2 pb-2 pl-3 pr-3 w-100 flex-wrap">
      <div>
        <SearchField
          placeholder="Cari packaging"
          onChange={(e) =>
            setTimeout(() => {
              handleSearch(e.target.value);
            }, 500)
          }
        />
      </div>
      <div className="d-flex flex-row flex-wrap">
        <Button
          styleType="blueFill"
          text="Tambah Packaging"
          onClick={handleOpenModal}
        />
      </div>
    </div>
  );
};



function PackagingTable(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const { listPackaging, detailPackaging } = useSelector(
    (state) => state.packagingAdministratorReducer
  );

  const { isLoading } = useSelector((state) => state.loading);

  const [state, setState] = useState({
    search: '',
    modalDetailPacking: false,
    modalDeletePackaging: false,
    modalEditPackaging: false,
    modalAddPackaging: false
  });

  const fetchListPackaging = async (payload) => {
    dispatch(getPackaging(payload));
  };

  const handleAddWarehouse = () => {
    history.push('/admin/warehouse/add');
  };

  const handleSearch = (value) => {
    setState({ ...state, search: value });
  };

  const handleOpenModal = () => {
    setState({...state, modalAddPackaging:true})
  }

  const handleDeletePacking = (data) => {
    setState({ ...state, modalDetailPacking: false, modalDeletePackaging: true })
  }

  const handleEditPacking = (data) => {
    setState({ ...state, modalDetailPacking: false, modalEditPackaging: true })
  }

  const PackagingImage = ({ data }) => {
    return (
      <img height={60} width={60} src={data?.image_full_url} alt={data?.image || 'Image'} />
    )
  }

  const Size = ({ data }) => {
    if(data?.type === 'Volume'){
      return (
        <span>{data?.length} cm x {data?.width} cm {data?.height} cm</span>
      )
    }
    else {
      return (
        <span>{data?.length} cm x {data?.width} cm</span>
      )
    }
  }

  const PackagingValue = ({ data }) => {
    return (
      <span>Rp. {data?.default_price.toLocaleString('id')}</span>
    )
  }

  const ActionField = ({ data }) => {
    return (
      <>
        <Button
          text="Detail"
          styleType="lightBlueFill"
          onClick={async function () {
            await dispatch(getPackaging({}, data?.id))
            setState({ ...state, modalDetailPacking: true })
          }}
        />
      </>
    );
  };

  return (
    <div>
      <DataTable
        action={fetchListPackaging}
        totalPage={listPackaging?.last_page || 5}
        params={{ search: state.search }}
        column={[
          {
            heading: 'Gambar Packaging',
            render: (data) => <PackagingImage data={data} />,
          },
          {
            heading: 'Nama Packaging',
            key: 'name',
          },
          {
            heading: 'Ukuran',
            render: (data) => <Size data={data} />,
          },
          {
            heading: 'Nilai Packaging',
            render: (data) => <PackagingValue data={data} />,
          },
          {
            heading: 'Aksi',
            render: (data) => <ActionField data={data} />,
          },
        ]}
        data={listPackaging || []}
        withNumber={false}
        renderHeader={() => (
          <HeaderTable
            handleOpenModal={handleOpenModal}
            handleSearch={handleSearch}
          />
        )}
        renderEmptyData={() => <EmptyGudang onClick={handleAddWarehouse} />}
      />
      <ModalDetailPackaging
        show={state.modalDetailPacking}
        onHide={() => setState({ ...state, modalDetailPacking: false })}
        data={detailPackaging ?? detailPackaging}
        handleDeletePacking={handleDeletePacking}
        handleEditPacking={handleEditPacking}
      />
      <ModalDeletePackaging
        show={state.modalDeletePackaging}
        onHide={() => setState({ ...state, modalDeletePackaging: false })}
        data={detailPackaging ?? detailPackaging}
      />
      <ModalEditPackaging
        show={state.modalEditPackaging}
        onHide={() => setState({ ...state, modalEditPackaging: false })}
        data={detailPackaging ?? detailPackaging}
      />
      <ModalAddPackaging
        show={state.modalAddPackaging}
        onHide={() => setState({ ...state, modalAddPackaging: false })}
      />
    </div>
  );
}

export default PackagingTable;
