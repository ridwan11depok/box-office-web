import React from 'react';
import Modal from '../../../../components/elements/Modal/Modal';
import Button from '../../../../components/elements/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#1c1c1c',
  },
  title: {
    fontFamily: 'Open Sans',
    fontSize: '15px',
    fontWeight: 'bold',
    color: '#1c1c1c',
  },
});

const ModalDetailPackaging = (props) => {
  const classes = useStyles();
  return (
    <>
      <Modal
        bodyClassName="pb-3"
        show={props.show}
        onHide={props.onHide}
        scrollable
        titleClassName="w-100"
        data={props.data}
        header={() => (
          <div className="d-flex flex-between w-100 align-items-center">
            Packaging Detail
            <div className="ml-auto">
              <Button
                style={{ minWidth: '120px' }}
                styleType="redOutline"
                text="Hapus"
                onClick={props.handleDeletePacking}
              />
              <Button
                style={{ minWidth: '120px' }}
                className="ml-1"
                styleType="blueOutline"
                text="Edit"
                onClick={props.handleEditPacking}
              />
            </div>
          </div>
        )}
        content={(data) => (
          <>
            <div className="mt-3">
              <h6 className={classes.title}>Gambar Packaging</h6>
              <img height={110} width={110} src={data?.image_full_url} alt={data?.image} />
            </div>
            <div className="mt-3">
              <h6 className={classes.title}>Nama Packaging</h6>
              <h6 className={classes.text}>{data?.name || 'No.Data'}</h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.title}>Ukuran</h6>
              <h6 className={classes.text}>
                {`${data?.length} cm x ${data?.width} cm ${data?.type === 'Volume' ? 'x ' + data?.height + ' cm' : ''}` || 'No.Data'}
              </h6>
            </div>
            <div className="mt-3">
              <h6 className={classes.title}>Nilai Packaging</h6>
              <h6 className={classes.text}>
                {`Rp. ${data?.default_price.toLocaleString('id')}` || 'No.Data'}
              </h6>
            </div>
          </>
        )}
        footer={(data) => (
          <>
            <Button
              onClick={props.onHide}
              text="Kembali"
              styleType="lightBlueFill"
            />
          </>
        )}
        footerClassName="p-3"
      />
    </>
  );
};

export default ModalDetailPackaging;
