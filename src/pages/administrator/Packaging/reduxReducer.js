import actionType from './reduxConstant';

const initialState = {
    listPackaging: [],
    detailPackaging: null
};

const packagingAdministratorReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case actionType.LIST_PACKAGING:
            return {
                ...prevState,
                listPackaging: action.payload,
            };
        case actionType.DETAIL_PACKING:
            return {
                ...prevState,
                detailPackaging: action.payload,
            };
        default:
            return prevState;
    }
};

export default packagingAdministratorReducer;
