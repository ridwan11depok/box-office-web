import React, { useState } from 'react';
import {
  ContentContainer,
  ContentItem,
  PageTitle,
} from '../../../components/elements/BaseContent';
import PackagingTable from './components/PackagingTable';

const GudangAdministrator = () => {
  return (
    <>
      <ContentContainer title="Packaging">
        <ContentItem border={false} col="col-12" spaceBottom={3}>
          <PackagingTable />
        </ContentItem>
      </ContentContainer>
    </>
  );
};

export default GudangAdministrator;
