import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

export const getPackaging =
    (params = {}, id = null) =>
        async (dispatch, getState) => {
            try {
                dispatch(setLoading(true));
                dispatch(setLoadingTable(true));
                const { token } = getState().login;
                let data = {};
                let headers = { token: token };
                let response;
                if (id) {
                    response = await consume.get('packaging', data, headers, id);
                } else {
                    response = await consume.getWithParams(
                        'packaging',
                        data,
                        headers,
                        params
                    );
                }
                if (response) {
                    if (id) {
                        dispatch(detailPackaging(response.result));
                    } else {
                        dispatch(listPackaging(response.result.data));
                    }
                    dispatch(setLoading(false));
                    dispatch(setLoadingTable(false));
                }
                return Promise.resolve('success');
            } catch (err) {
                
                dispatch(listPackaging([]));
                dispatch(setLoading(false));
                dispatch(setLoadingTable(false));
                return Promise.reject('failed');
            }
        };


const listPackaging = (data) => {
    return {
        type: actionType.LIST_PACKAGING,
        payload: data,
    };
};

const detailPackaging = (data) => {
    return {
        type: actionType.DETAIL_PACKING,
        payload: data
    }
}

export const addPackaging =
  (payload) => async (dispatch, getState) => {
    try {
      dispatch(setLoading(true));
      const { token } = getState().login;
      let data = payload;
      let headers = { token: token, content_type: 'multipart/form-data' };
      let response = await consume.post('packaging', data, headers);
      if (response) {
        dispatch(getPackaging({ page: 1, per_page: 10 }));
        dispatch(setLoading(false));
      }
      dispatch(
        setToast({
          isShow: true,
          messages: 'Berhasil menambah packaging.',
          type: 'success',
        })
      );
      return Promise.resolve('success');
    } catch (err) {
      dispatch(setLoading(false));
      dispatch(
        setToast({
          isShow: true,
          messages: 'Gagal menambah packaging.',
          type: 'error',
        })
      );
      return Promise.reject(err);
    }
  };

export const deletePackaging =
    (payload = {}, id) => async (dispatch, getState) => {
        try {
            dispatch(setLoading(true));
            const { token } = getState().login;
            let data = payload;
            let headers = { token: token };
            let response = await consume.delete('packaging', data, headers, id);
            if (response) {
                dispatch(detailPackaging(null))
                dispatch(
                    getPackaging({
                        page: 1,
                        per_page: 10,
                    })
                );
                dispatch(setLoading(false));
            }
            dispatch(
                setToast({
                    isShow: true,
                    messages: 'Berhasil menghapus data packaging',
                    type: 'success',
                })
            );
            return Promise.resolve('success');
        } catch (err) {
            dispatch(setLoading(false));
            dispatch(
                setToast({
                    isShow: true,
                    messages: 'Gagal menghapus data packaging',
                    type: 'error',
                })
            );
            return Promise.reject('failed');
        }
    };

export const updatePackaging = (payload, id) => async (dispatch, getState) => {
    try {
        dispatch(setLoading(true));
        const { token } = getState().login;
        let data = payload;
        let headers = { token: token, content_type: 'multipart/form-data' };
        let params = {};
        const response = await consume.post('packaging', data, headers, id);
        dispatch(
            getPackaging({
                page: 1,
                per_page: 10,
            })
        );
        dispatch(setLoading(false));
        dispatch(
            setToast({
                isShow: true,
                messages: 'Berhasil memperbarui data packaging.',
                type: 'success',
            })
        );
        return Promise.resolve('success');
    } catch (err) {
        dispatch(setLoading(false));
        dispatch(
            setToast({
                isShow: true,
                messages: 'Gagal memperbarui data packaging.',
                type: 'error',
            })
        );
        return Promise.reject(err);
    }
};