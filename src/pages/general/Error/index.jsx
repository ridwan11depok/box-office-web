import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { Link } from 'react-router-dom';
import { setErrorData } from '../../../redux/actions';

const Index = (props) => {
    const dispatch = useDispatch()
    const { data } = props;

    useEffect(() => {
        return () => {
            dispatch(setErrorData({}))
        }
    })
    return (
        <div className="container-404 d-flex justify-content-center flex-column align-items-center text-center">
            <h1>{data?.code || 500}</h1>
            <h1>{data?.error?.message || 'Terjadi kesalahan'}</h1>
        </div>
    );
};

export default Index;
