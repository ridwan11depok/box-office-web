import { Row, Col, Form, Button } from 'react-bootstrap';
// import { Row, Col, Form, Select } from 'react-bootstrap';
import React, { useState, useEffect, useRef } from 'react';
import Toggle from 'react-toggle';
import { toast, ToastContainer } from 'react-toastify';
import ButtonComponent from '../../../components/elements/Button/index';
import { ReactSelect } from '../../../components/elements/InputField'
import MapModal from '../../../components/elements/Map/MapModal';

import { connect } from 'react-redux';
import { handleRegister } from '../Login/reduxAction';
import { Link, useHistory } from 'react-router-dom';

import 'react-toastify/dist/ReactToastify.css';
import './style/IndexRegister.css';
// import 'react-toggle/style.css';
import show from '../../../assets/icons/show.svg';
import hide from '../../../assets/icons/hide.svg';
import checklist from '../../../assets/icons/checklist.svg';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import oneActive from '../../../assets/icons/1active.svg';
import twoNotActive from '../../../assets/icons/2notactive.svg';
import CheckDone from '../../../assets/icons/checkdone.svg';
import twoActive from '../../../assets/icons/twoactive.svg';
import Avatar from '../../../assets/icons/Avatar.svg';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { getArea } from '../../administrator/Gudang/reduxAction';
import { hex_md5 } from '../../../utils/md5';


const Register = (props) => {
  const { handleRegister, errorRegister } = props;
  const history = useHistory();
  const [state, setState] = useState({
    email: '',
    emailError: '',
    name: '',
    nameError: '',
    storeName: '',
    storeNameError: '',
    storeAddress: '',
    storeAddressError: '',
    countryCode: '62',
    phone_number: '',
    phone_number_error: '',
    address: '',
    addressError: '',
    password: '',
    passwordError: '',
    page: 'daftar',
    image: '',
    isOwnAddress: false,
    province_id: 0,
    city_id: 0,
    district_id:0,
    showMap: false,
    longitude: null,
    latitude: null,
    loading: false
  });

  const dispatch = useDispatch();
  const { listProvince, listCity, listDistrict } = useSelector(
    (state) => state.gudangAdministratorReducer
  );

  useEffect(() => {
    if (listProvince.length === 0) {
      dispatch(getArea());
    }
  }, []);

  useEffect(() => {
    console.log('listDistrict :>> ', listDistrict);
  }, [listDistrict]);



  const [supportState, setSupportState] = useState({
    isPasswordSecure: true,
    error: errorRegister,
  });

  const inputFile = useRef(null);

  const handleChangeProvince = (e) => {
    setState({ ...state, province_id: e.target.value});
    dispatch(getArea(Number(e.target.value)));
  };

  const handleChangeCity = (e) => {
    setState({ ...state, city_id: Number(e.target.value) });
    dispatch(getArea(state.province_id, Number(e.target.value)));
  }
  
  const handleChangeDistrict = (e) => {
    setState({ ...state, district_id: Number(e.target.value) });
  };

  //function handle email

  function validateEmail(email) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const onBlurEmail = () => {
    if (!state.email) {
      setState({ ...state, emailError: 'Email Tidak Boleh Kosong' });
    } else if (!validateEmail(state.email)) {
      setState({ ...state, emailError: 'Format Email Tidak Sesuai' });
    } else {
      setState({ ...state, emailError: '' });
    }
  };

  const onChangeEmailText = (e) => {
    e.preventDefault();
    setState({ ...state, email: e.target.value });
  };

  //end function handle email

  //function handle name

  const onBlurName = () => {
    console.log(state.name);
    if (!state.name) {
      setState({ ...state, nameError: 'Nama Tidak Boleh Kosong' });
    } else {
      setState({ ...state, nameError: '' });
    }
  };

  const onChangeNameText = (e) => {
    e.preventDefault();
    setState({ ...state, name: e.target.value });
  };

  //end function handle nama

  //function handle nomor telepon

  useEffect(() => {
    if (state.phone_number) {
      if (!validatePhoneNumber(state.phone_number))
        setState({
          ...state,
          phone_number_error: 'Format Nomor Telepon Harus Angka',
        });
    } else {
      setState({ ...state, phone_number_error: '' });
    }
  }, [state.phone_number]);

  const validatePhoneNumber = (phoneNumber) => {
    var reg = /^\d+$/;
    return reg.test(String(phoneNumber));
  };

  const onChangeSelectPhoneNumber = (e) => {
    e.preventDefault();
    setState({ ...state, countryCode: e.target.value });
  };

  const onChangePhoneNumber = (e) => {
    e.preventDefault();
    setState({ ...state, phone_number: e.target.value });
  };

  const onBlurPhoneNumber = (e) => {
    if (!state.phone_number) {
      setState({
        ...state,
        phone_number_error: 'Nomor Telepon Tidak Boleh Kosong',
      });
    } else if (!validatePhoneNumber(state.phone_number)) {
      setState({
        ...state,
        phone_number_error: 'Format Nomor Telepon Harus Angka',
      });
    } else {
      setState({ ...state, phone_number_error: '' });
    }
  };
  //end function handle nomor telepon

  //function handle address

  const onBlurAddress = (e) => {
    e.preventDefault();
    if (!state.address) {
      setState({ ...state, addressError: 'Address Tidak Boleh Kosong' });
    }
  };

  const onChangeAddress = (e) => {
    e.preventDefault();
    if (e.target.value.length <= 180) {
      setState({ ...state, address: e.target.value });
    }
  };

  useEffect(() => {
    if (state.address.length >= 180) {
      setState({
        ...state,
        addressError: 'Karakter Sudah Mencapai Batas Maksimal',
      });
    } else {
      setState({ ...state, addressError: '' });
    }
  }, [state.address]);



  //end function handle address

  //function password

  const onChangePassword = (e) => {
    e.preventDefault();
    setState({ ...state, password: e.target.value });
  };

  const onBlurPassword = (e) => {
    e.preventDefault();
    if (!state.password) {
      setState({ ...state, passwordError: 'Password Tidak Boleh Kosong' });
    } else if (state.password.length < 8) {
      setState({ ...state, passwordError: 'Password Tidak Sesuai Format' });
    } else {
      setState({ ...state, passwordError: '' });
    }
  };

  const showHidePassword = (e) => {
    e.preventDefault();
    setSupportState({
      ...supportState,
      isPasswordSecure: !supportState.isPasswordSecure,
    });
  };

  //end function password

  //function upload file

  const onButtonClick = () => {
    inputFile.current.click();
  };

  function validateFile(files){
    const allowedExtensions =  ['jpg','png','svg'],
          sizeLimit = 1_000_000; // 1 megabyte
  
    // destructuring file name and size from file object
    const { name:fileName, size:fileSize } = files[0];
  
    /*
    * if filename is apple.png, we split the string to get ["apple","png"]
    * then apply the pop() method to return the file extension
    *
    */
    const fileExtension = fileName.split(".").pop();
  
    /* 
      check if the extension of the uploaded file is included 
      in our array of allowed file extensions
    */
    if(!allowedExtensions.includes(fileExtension)){
      toast.error('File type not allowed', {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // value = null;
    }else if(fileSize > sizeLimit){
      toast.error('File size too large', {
        position: 'top-center',
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      // value = null;
    } else {
      var reader = new FileReader();
      var url = reader.readAsDataURL(files[0]);

      reader.onloadend = function (e) {
        setState({ ...state, image: [reader.result] });
      };
    }
  }
  

  const onChangeFile = (e) => {
    var file = e.target.files[0];
    document.getElementById("file").addEventListener("change", validateFile(e.target.files))
    
  };

  //end function upload file

  //function store name

  const onChangeStoreName = (e) => {
    e.preventDefault();
    setState({ ...state, storeName: e.target.value });
  };

  const onBlurStoreName = (e) => {
    e.preventDefault();
    if (!state.storeName) {
      setState({ ...state, storeNameError: 'Nama Toko Tidak Boleh Kosong' });
    } else {
      setState({ ...state, storeNameError: '' });
    }
  };

  //end function store name

  //function store address

  const onChangeStoreAddress = (e) => {
    e.preventDefault();
    setState({ ...state, storeAddress: e.target.value });
  };

  const onBlurStoreAddress = (e) => {
    e.preventDefault();
    if (!state.storeAddress) {
      setState({
        ...state,
        storeAddressError: 'Alamat Toko Tidak Boleh Kosong',
      });
    } else {
      setState({ ...state, storeAddressError: '' });
    }
  };

  //end store address

  const checkIsNoError = () => {
    return new Promise((resolve) => {
      let errorArray = [];
      if (!state.email) {
        errorArray.push('Error Email');
      }
      if (!state.name) {
        errorArray.push('Error Name');
      }
      if (!state.phone_number) {
        errorArray.push('Error Phone Number');
      }
      if (!state.address) {
        errorArray.push('Error Address');
      }
      if (!state.password) {
        errorArray.push('Error Password');
      }
      resolve(errorArray);
    });
  };

  // useEffect(()=> {
  //     console.log('support state =>', supportState.error)
  // },[supportState])

  const handleNext = (e) => {
    e.preventDefault();
    let error = {};

    if (!state.email) {
      error.email = ['Bidang isian email wajib diisi'];
    }
    if (!state.name) {
      error.name = ['Bidang isian name wajib diisi'];
    }
    if (!state.phone_number) {
      error.phone_number = ['Bidang isian phone number wajib diisi'];
    }
    if (!state.address) {
      error.address = ['Bidang isian alamat wajib diisi'];
    }
    if (!state.password) {
      error.password = ['Bidang isian password wajib diisi'];
    }

    if (Object.keys(error).length === 0) {
      setState({ ...state, page: 'infotoko' });
    } else {
      setSupportState({ ...supportState, error: error });
    }
  };

  //variable for error handling

  const removeError = (errorParams) => {
    const deleted = Object.keys(supportState.error)
      .filter((key) => key !== errorParams)
      .reduce((obj, key) => {
        obj[key] = supportState.error[key];
        return obj;
      }, {});
    return deleted;
  };

  useEffect(() => {
    if (state.storeName && supportState.error?.store_name) {
      setSupportState({ ...supportState, error: removeError('store_name') });
    }

    if (state.storeAddress && supportState.error?.store_address) {
      setSupportState({ ...supportState, error: removeError('store_address') });
    }

    if (state.image && supportState.error?.store_logo) {
      setSupportState({ ...supportState, error: removeError('store_logo') });
    }

    if (state.password.length === 8 && supportState.error?.password) {
      setSupportState({ ...supportState, error: removeError('password') });
    }

    if (state.address && supportState.error?.address) {
      setSupportState({ ...state, error: removeError('address') });
    }

    if (state.phone_number && supportState.error?.phone_number) {
      setSupportState({ ...state, error: removeError('phone_number') });
    }

    if (state.name && supportState.error?.name) {
      setSupportState({ ...state, error: removeError('name') });
    }

    if (state.province_id && supportState.error?.province_id) {
      setSupportState({ ...state, error: removeError('province_id') });
    }

    if (state.city_id && supportState.error?.city_id) {
      setSupportState({ ...state, error: removeError('city_id') });
    }

    if (state.district_id && supportState.error?.district_id) {
      setSupportState({ ...state, error: removeError('district_id') });
    }

    // if (state.email && supportState.error?.email && validateEmail(state.email)) {
    //     setSupportState({ ...state, error: removeError('email') })
    // }
  }, [state]);

  useEffect(() => {
    if (
      state.email &&
      supportState.error?.email &&
      validateEmail(state.email)
    ) {
      setSupportState({ ...state, error: removeError('email') });
    }
  }, [state.email]);

  const selectCoordinate = (coordinate) => {
    setState({ ...state, longitude: coordinate.lng, latitude: coordinate.lat, showMap: false });
  };

  const register = async (e) => {
    try {
      e.preventDefault();
      setState({...state, loading:true})
      // const result = await checkIsNoErrorInfoToko()
      //consume api register
      const url = state.image[0];
      const res = await fetch(url);
      const blob = await res.blob();
      const file = new File([blob], 'Store_Logo', { type: 'image/png' });

      // console.log('image', file)
      const data = new FormData();
      data.append('email', state.email);
      data.append('password', hex_md5(state.password));
      data.append('password_confirmation', hex_md5(state.password));
      data.append('country_code', state.countryCode);
      data.append('status', state.isOwnAddress ? 1 : 0);
      data.append('name', state.name);
      data.append('phone_number', state.phone_number);
      data.append('address', state.address);
      data.append('store_name', state.storeName);
      data.append(
        'store_address',
        state.isOwnAddress ? state.address : state.storeAddress
      );
      data.append('store_logo', file, 'StoreLogo.png');
      data.append('latitude', state.latitude)
      data.append('longitude', state.longitude)
      data.append('province_id', state.province_id)
      data.append('city_id', state.city_id)
      data.append('district_id', state.district_id)
      
      const result = await handleRegister(data);
      if (result.description === 'OK') {
        setState({...state, loading:false})
        toast.success('Registration Successfully', {
          position: 'top-center',
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        history.push('/login');
      }
      setState({...state, loading:false})
      // console.log('result nya:', result)
    } catch (err) {
      setState({...state, loading:false})
      if (err.code == 422) {
        const { error } = err;
        const { errors } = error;
        setSupportState({...state, error: errors})
        for (const property in errors) {
          if (
            property === 'email' ||
            property === 'name' ||
            property === 'phone_number' ||
            property === 'address' ||
            property === 'password'
          ) {
            setState({ ...state, page: 'daftar' });
            break;
          }
        }
      }
    }
  };

  return (
    <div>
      {/* <ToastContainer /> */}
      <div className="background-image"></div>
      <div className="overlay-register">
        <Row className="align-items-center">
          <Col className="d-flex justify-content-center">
            <img src={logoboxoffice} width="400px" height="400px" className="img-fluid" alt="Logo BoxOffice" />
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-5 col-12">
            <div className="row">
              {/* <div className="col-xs-8 col-sm-8 col-md col-lg-12"> */}
              <div className="col">
                <div className="wrapper1">
                  <div className="register-box mx-5">
                    {state.page === 'daftar' ? (
                      <Form
                        className="form-register"
                        style={{ paddingRight: '15px' }}
                      >
                        <Form.Group>
                          <div className="row">
                            <div className="col-6">
                              <div className="step1">
                                <text className="daftar">
                                  <img src={oneActive} alt="number" />
                                  Daftar
                                </text>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="step2">
                                <text className="info">
                                  <img src={twoNotActive} alt="number" />
                                  Info Toko
                                </text>
                              </div>
                            </div>
                          </div>
                        </Form.Group>
                        <text className="register-text">Daftar</text>
                        <Form.Group controlId="formGroupEmail">
                          <Form.Label className="text-form">Email</Form.Label>
                          <Form.Control
                            value={state.email}
                            onBlur={onBlurEmail}
                            onChange={onChangeEmailText}
                            className="text-placeholder"
                            type="text"
                            placeholder="Masukan Email"
                          />
                          {supportState.error &&
                            supportState.error?.email &&
                            supportState.error.email.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </Form.Group>
                        <Form.Group controlId="formGroupNama">
                          <Form.Label className="text-form">Nama</Form.Label>
                          <Form.Control
                            value={state.name}
                            className="text-placeholder"
                            onBlur={onBlurName}
                            onChange={onChangeNameText}
                            type="text"
                            placeholder="Masukan Nama"
                          />
                          {supportState.error &&
                            supportState.error?.name &&
                            supportState.error.name.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </Form.Group>

                        <Form.Group controlId="formBasicSelect">
                          <Form.Label className="text-form">
                            Nomor Telepon
                          </Form.Label>
                          <div className="row">
                            <div className="col-sm-4">
                              <Form.Control
                                onChange={onChangeSelectPhoneNumber}
                                className="text-placeholder-telp"
                                as="select"
                              >
                                <option value="62">+62</option>
                              </Form.Control>
                            </div>
                            <div className="col-sm-8">
                              <Form.Control
                                value={state.phone_number}
                                className="text-placeholder-input"
                                type="number"
                                onBlur={onBlurPhoneNumber}
                                onChange={onChangePhoneNumber}
                                placeholder="Masukan nomor telpon"
                              />
                              {state.phone_number_error &&

                                <text
                                  style={{ color: 'red' }}
                                >
                                  {state.phone_number_error}
                                </text>
                              }
                              {supportState.error &&
                                supportState.error?.phone_number &&
                                supportState.error.phone_number.map(
                                  (el, index) => {
                                    return (
                                      <text
                                        key={index}
                                        style={{ color: 'red' }}
                                      >
                                        {el}
                                      </text>
                                    );
                                  }
                                )}
                            </div>
                          </div>
                        </Form.Group>

                        <Form.Group controlId="formGroupNama">
                          <Form.Label className="text-form">Alamat</Form.Label>
                          <Form.Control
                            value={state.address}
                            className="text-placeholder"
                            type="text"
                            as="textarea"
                            rows={3}
                            onChange={onChangeAddress}
                            onBlur={onBlurAddress}
                            placeholder="Masukan Alamat"
                          />
                          <text className="length-text">
                            {!state.address
                              ? '0'
                              : String(state.address.length)}
                            /180
                          </text>
                          {supportState.error &&
                            supportState.error?.address &&
                            supportState.error.address.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </Form.Group>
                        <Form.Group className="" controlId="formGroupPassword">
                          <Form.Label className="text-form">
                            Password
                          </Form.Label>
                          <Form.Control
                            value={state.password}
                            className="text-placeholder"
                            type={
                              supportState.isPasswordSecure
                                ? 'password'
                                : 'text'
                            }
                            placeholder="Masukkan password"
                            onChange={onChangePassword}
                            onBlur={onBlurPassword}
                          />
                          <img
                            src={supportState.isPasswordSecure ? show : hide}
                            style={{ cursor: 'pointer' }}
                            onClick={showHidePassword}
                            alt="hide password"
                            className="password-show"
                          />
                          <div style={{ paddingTop: '10px' }}>
                            {supportState.error &&
                              supportState.error?.password &&
                              supportState.error.password.map((el, index) => {
                                return (
                                  <text key={index} style={{ color: 'red' }}>
                                    {el}
                                  </text>
                                );
                              })}
                          </div>
                        </Form.Group>
                        <Form.Group className="" controlId="formGroupPassword">
                          <Form.Label className="text-form">
                            Buatlah password yang:
                          </Form.Label>

                          <Form.Label className="form-checklist">
                            <img src={checklist} alt="checklist" /> Minimal 8
                            karakter
                          </Form.Label>
                          <Form.Label className="form-checklist">
                            <img src={checklist} alt="checklist" /> Tidak berisi
                            nama kamu atau alamat email
                          </Form.Label>
                          <Form.Label className="form-checklist">
                            <img src={checklist} alt="checklist" /> Memilki
                            huruf kecil (a-z) dan huruf besar (A-Z)
                          </Form.Label>
                        </Form.Group>
                        <Button
                          className="tombol login"
                          type="submit"
                          onClick={handleNext}
                        >
                          Selanjutnya
                        </Button>
                        <Form.Label className="text1">
                          Dengan menggunakan aplikasi ini anda menyetujui{' '}
                          <Link
                            to=""
                            className="a"
                            style={{ textDecoration: 'none' }}
                          >
                            {' '}
                            syarat dan ketentuan{' '}
                          </Link>{' '}
                        </Form.Label>
                        <Form.Label className="text2">
                          Sudah punya akun?{' '}
                          <Link
                            to="/login"
                            className="a"
                            style={{ textDecoration: 'none' }}
                          >
                            {' '}
                            Login{' '}
                          </Link>{' '}
                        </Form.Label>
                      </Form>
                    ) : (
                      <Form className="form-register" style={{ paddingRight: '15px' }}>
                        <Form.Group>
                          <div className="row">
                            <div
                              style={{ cursor: 'pointer' }}
                              onClick={(e) => {
                                e.preventDefault();
                                setState({ ...state, page: 'daftar' });
                              }}
                              className="col-6"
                            >
                              <div className="step2">
                                <text className="info">
                                  <img src={CheckDone} alt="checklist" />
                                  Daftar
                                </text>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="step1-infotoko">
                                <text className="daftar">
                                  <img src={twoActive} alt="number" />
                                  Info Store
                                </text>
                              </div>
                            </div>
                          </div>
                        </Form.Group>
                        <text className="register-text">Info Store</text>
                        <Form.Group>
                          <div className="upload">
                            <div className="col-12">
                              {state.image === '' ? (
                                <img src={Avatar} alt="avatar" />
                              ) : (
                                <img
                                  className="avatar-info"
                                  src={state.image}
                                />
                              )}
                            </div>
                            <div className="col-12 upload">
                              <Button
                                onClick={onButtonClick}
                                className="uploadfile"
                                onMouseEnter={(e) => console.log('aaaa', e)}
                              >
                                <input
                                  onChange={onChangeFile}
                                  accept="image/*"
                                  type="file"
                                  className="custom-file-input"
                                  id="file"
                                  ref={inputFile}
                                  hidden
                                />{' '}
                                {state.image ? 'Ganti Logo' : 'Upload Logo'}
                              </Button>
                            </div>
                          </div>
                          {supportState.error &&
                            supportState.error?.store_logo &&
                            supportState.error.store_logo.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </Form.Group>
                        <Form.Group controlId="formGroupData">
                          <Form.Label className="text-form">
                            Nama Store{' '}
                            <text className="keterangan">(Wajib diisi)</text>
                          </Form.Label>
                          <Form.Control
                            value={state.storeName}
                            className="text-placeholder"
                            onChange={onChangeStoreName}
                            onBlur={onBlurStoreName}
                            type="text"
                            placeholder="Masukan Nama Toko"
                          />
                          {/* {state.storeNameError !== '' || state.errorInfoToko.includes('Nama Toko Tidak Boleh Kosong') &&
                                                        <text style={{ color: 'red' }}>{state.storeNameError ? state.storeNameError : state.errorInfoToko.find(el=> el === 'Nama Toko Tidak Boleh Kosong')}</text>
                                                    } */}
                          {supportState.error &&
                            supportState.error?.store_name &&
                            supportState.error.store_name.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </Form.Group>
                        <Form.Group>
                          <Form.Label className="text-form">
                            Pilih Koordinat
                          </Form.Label>
                          <div className="background-map mb-3">
                            <div
                              className="d-flex flex-column flex-lg-row justify-content-between align-items-center w-100 p-3"
                              style={{ backgroundColor: 'rgba(28,28,28,0.7)', height: '100px' }}
                            >
                              <span
                                style={{
                                  fontSize: '14px',
                                  fontWeight: '600',
                                  fontFamily: 'Open Sans',
                                }}
                              >
                                Tandai lokasi warehouse Anda.
                              </span>
                              <ButtonComponent
                                style={{ width: '200px', backgroundColor: '#FAFAFF' }}
                                styleType="blueNoFill"
                                text="Tandai Lokasi"
                                onClick={() => setState({ ...state, showMap: true })}
                              />
                            </div>
                          </div>
                        </Form.Group>
                        <Form.Group controlId="formGroupNama">
                          <div className="row">
                            <div className="col-3">
                              <Form.Label className="text-form">
                                Alamat
                              </Form.Label>
                            </div>
                            <div className="col-9 alamat d-flex align-items-center justify-content-end">
                              <Toggle
                                id="is-own-address-status"
                                defaultChecked={state.isOwnAddress}
                                icons={{
                                  unchecked: null,
                                }}
                                // className='custom-classname'
                                onChange={() =>
                                  setState({
                                    ...state,
                                    isOwnAddress: !state.isOwnAddress,
                                  })
                                }
                              />
                              <text className="text-alamat ml-1">
                                Gunakan alamat pribadi
                              </text>
                            </div>
                          </div>
                          <Form.Control
                            value={
                              state.isOwnAddress
                                ? state.address
                                : state.storeAddress
                            }
                            className="text-placeholder"
                            type="text"
                            as="textarea"
                            rows={3}
                            placeholder="Masukan Alamat"
                            onChange={onChangeStoreAddress}
                            onBlur={onBlurStoreAddress}
                          />
                          {state.isOwnAddress ? (
                            <text className="length-text">
                              {String(state.address.length)}
                              /180
                            </text>
                          ) : (
                            <text className="length-text">
                              {String(state.storeAddress.length)}
                              /180
                            </text>
                          )}
                          {supportState.error &&
                            supportState.error?.store_address &&
                            supportState.error.store_address.map(
                              (el, index) => {
                                return (
                                  <text key={index} style={{ color: 'red' }}>
                                    {el}
                                  </text>
                                );
                              }
                            )}
                        </Form.Group>
                        <Form.Group>
                          <Row>
                            <Col xs={12} md={6} lg={4}>
                              <ReactSelect
                                onChange={handleChangeProvince}
                                label="Provinsi"
                                options={listProvince}
                                name="province_id"
                                placeholder="Pilih Provinsi"
                              />
                              {supportState.error &&
                                supportState.error?.province_id &&
                                supportState.error.province_id.map(
                                  (el, index) => {
                                    return (
                                      <text key={index} style={{ color: 'red' }}>
                                        {el}
                                      </text>
                                    );
                                  }
                                )}
                            </Col>
                            <Col xs={12} md={6} lg={4}>
                              <ReactSelect
                                onChange={handleChangeCity}
                                label="Kota"
                                name="city_id"
                                options={listCity}
                                placeholder="Pilih Kota"
                              />
                              {supportState.error &&
                                supportState.error?.city_id &&
                                supportState.error.city_id.map(
                                  (el, index) => {
                                    return (
                                      <text key={index} style={{ color: 'red' }}>
                                        {el}
                                      </text>
                                    );
                                  }
                                )}
                            </Col>
                            <Col xs={12} md={6} lg={4}>
                              <ReactSelect
                                onChange={handleChangeDistrict}
                                label="Kecamatan"
                                name="district_id"
                                options={listDistrict}
                                placeholder="Pilih Kecamatan"
                              />
                              {supportState.error &&
                                supportState.error?.district_id &&
                                supportState.error.district_id.map(
                                  (el, index) => {
                                    return (
                                      <text key={index} style={{ color: 'red' }}>
                                        {el}
                                      </text>
                                    );
                                  }
                                )}
                            </Col>
                          </Row>
                        </Form.Group>
                        <Button
                          className="tombol login mt-4"
                          type="submit"
                          onClick={register}
                          disabled={state.loading ? true : false}
                        >
                          Daftar Sekarang
                        </Button>
                        <Form.Label className="text1">
                          Dengan menggunakan aplikasi ini anda menyetujui{' '}
                          <Link
                            to=""
                            className="a"
                            style={{ textDecoration: 'none' }}
                          >
                            {' '}
                            syarat dan ketentuan{' '}
                          </Link>{' '}
                        </Form.Label>
                        <Form.Label className="text2">
                          Sudah punya akun?{' '}
                          <Link
                            to="/login"
                            className="a"
                            style={{ textDecoration: 'none' }}
                          >
                            {' '}
                            Login{' '}
                          </Link>{' '}
                        </Form.Label>
                      </Form>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-1 col-12"></Col>
        </Row>
      </div>
      <ToastContainer />
      <MapModal
        show={state.showMap}
        onHide={() => setState({ ...state, showMap: false })}
        onAgree={selectCoordinate}
        initialCoordinate={{
          lng: state.longitude,
          lat: state.latitude,
        }}
      />
    </div>
  );
};

const reduxState = (state) => ({
  errorRegister: state.login.errorRegister,
});

const reduxDispatch = (dispatch) => ({
  handleRegister: (payload) => dispatch(handleRegister(payload)),
});

export default connect(reduxState, reduxDispatch)(Register);
