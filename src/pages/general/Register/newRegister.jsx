import './style/newRegister.css';
import show from '../../../assets/icons/show.svg';
import checklist from '../../../assets/icons/checklist.svg';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import oneActive from '../../../assets/icons/1active.svg';
import twoNotActive from '../../../assets/icons/2notactive.svg';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Button, Select } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Register = () => {
  const [state, setState] = useState({
    email: '',
    name: '',
    phone_number: '',
    address: '',
    password: '',
  });

  // useEffect(() => {
  //     axios.get('https://54.254.190.25/api/v1/register')
  //     .then(res => {
  //         console.log(res)
  //         setState(res.data)
  //     })
  //     .catch(err => {
  //         console.log(err)
  //     })
  // }, [])
  // useEffect(() => {
  //     fetch('http://54.254.190.25/api/v1/register')
  //         .then(res => {
  //             return res.json();
  //         })
  //         .then(data => {
  //             console.log(data);
  //         })
  // }, [])

  return (
    <div>
      <div className="background-image"></div>
      <div className="overlay-register">
        <Row className="content">
          <Col className="col-xs-12 col-sm-12 col-lg-8 col-12">
            <div className="logo">
              <img src={logoboxoffice} alt="Logo BoxOffice" />
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-3 col-12">
            <div className="row">
              <div className="col-xs-2 col-sm-2 col-md-3 col-lg-12"></div>
              <div className="col-xs-8 col-sm-8 col-md-6 col-lg-12">
                <div className="wrapper1">
                  <div className="register-box">
                    <Form className="form-register">
                      <Form.Group>
                        <div className="row header-btn">
                          <div className="col-6">
                            <div className="step1">
                              <text className="daftar">
                                <img src={oneActive} alt="number" />
                                Daftar
                              </text>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="step2">
                              <text className="info">
                                <img src={twoNotActive} alt="number" />
                                Info Toko
                              </text>
                            </div>
                          </div>
                        </div>
                      </Form.Group>
                      <text className="register-text">Daftar</text>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label className="text-form">Email</Form.Label>
                        <Form.Control
                          className="text-placeholder"
                          type="text"
                          placeholder="Masukan Email"
                        />
                      </Form.Group>
                      <Form.Group controlId="formGroupNama">
                        <Form.Label className="text-form">Nama</Form.Label>
                        <Form.Control
                          className="text-placeholder"
                          type="text"
                          placeholder="Masukan Nama"
                        />
                      </Form.Group>

                      <Form.Group controlId="formBasicSelect">
                        <Form.Label className="text-form">
                          Nomor Telpon
                        </Form.Label>
                        <div className="row">
                          <div className="col-3">
                            <Form.Control
                              className="text-placeholder-telp"
                              as="select"
                            >
                              <option value="+62">+62</option>
                              <option value="+32">+32</option>
                            </Form.Control>
                          </div>
                          <div className="col-9">
                            <Form.Control
                              className="text-placeholder-input"
                              type="text"
                              placeholder="Masukan nomor telpon"
                            />
                          </div>
                        </div>
                      </Form.Group>

                      {/* <Form.Group controlId="formBasicSelect">
                                                <Form.Label className="text-form">Nomor Telpon</Form.Label>
                                                <div className="row">
                                                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                        <Form.Control className="text-placeholder-telp" as="select">
                                                            <option value="+62">+62</option>
                                                            <option value="+32">+32</option>
                                                        </Form.Control>
                                                    </div>
                                                    <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                                        <Form.Control className="text-placeholder-input" type="text" placeholder="Masukan nomor telpon" />
                                                    </div>
                                                </div>
                                            </Form.Group> */}

                      <Form.Group controlId="formGroupNama">
                        <Form.Label className="text-form">Alamat</Form.Label>
                        <Form.Control
                          className="text-placeholder"
                          type="text"
                          as="textarea"
                          rows={3}
                          placeholder="Masukan Alamat"
                        />
                        <text className="length-text">0/40</text>
                      </Form.Group>
                      <Form.Group className="" controlId="formGroupPassword">
                        <Form.Label className="text-form">Password</Form.Label>
                        <Form.Control
                          className="text-placeholder"
                          type="password"
                          placeholder="Masukkan password"
                        />
                        <img
                          src={show}
                          alt="hide password"
                          className="password-show"
                        />
                      </Form.Group>
                      <Form.Group className="" controlId="formGroupPassword">
                        <Form.Label className="text-form">
                          Buatlah password yang:
                        </Form.Label>

                        <Form.Label className="form-checklist">
                          <img src={checklist} alt="checklist" /> Minimal 8
                          karakter
                        </Form.Label>
                        <Form.Label className="form-checklist">
                          <img src={checklist} alt="checklist" /> Tidak berisi
                          nama kamu atau alamat email
                        </Form.Label>
                        <Form.Label className="form-checklist">
                          <img src={checklist} alt="checklist" /> Memilki huruf
                          kecil (a-z) dan huruf besar (A-Z)
                        </Form.Label>
                      </Form.Group>

                      {/* <Form.Label className="text-forget"><Link to="" className="a"> Lupa Password ? </Link></Form.Label> */}
                      <Button className="tombol login" type="submit">
                        Selanjutnya
                      </Button>
                      <Form.Label className="text1">
                        Dengan menggunakan aplikasi ini anda menyetujui{' '}
                        <Link
                          to=""
                          className="a"
                          style={{ textDecoration: 'none' }}
                        >
                          {' '}
                          syarat dan ketentuan{' '}
                        </Link>{' '}
                      </Form.Label>
                      <Form.Label className="text2">
                        Sudah punya akun?{' '}
                        <Link
                          to="/auth/login"
                          className="a"
                          style={{ textDecoration: 'none' }}
                        >
                          {' '}
                          Login{' '}
                        </Link>{' '}
                      </Form.Label>
                    </Form>
                  </div>
                </div>
              </div>
              <div className="col-xs-2 col-sm-2 col-md-3 col-lg-12"></div>
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-1 col-12"></Col>
        </Row>
      </div>
    </div>
  );
};

export default Register;
