import { useEffect, useState, useRef } from 'react';
import { Row, Col, Grid, Container } from 'react-bootstrap';

import logoBoxOfficeBlack from '../../../assets/images/logoBoxOfficeBlack.svg';
import humanWithBoxLogo from '../../../assets/images/humanWithBoxLogo.svg';
import halfTriangleHome from '../../../assets/images/halfTriangleHome.svg';
import Button from '../../../components/atoms/Button/Button';
import DownloadIcon from '../../../assets/icons/download.svg';
import speedometerIcon from '../../../assets/icons/speedometer.svg';
import padlockIcon from '../../../assets/icons/padlock.svg';
import shieldIcon from '../../../assets/icons/shield.svg';
import truckCarIcon from '../../../assets/icons/truckCar.svg';
import checklistHandIcon from '../../../assets/icons/checklistHand.svg';
import pcIcon from '../../../assets/icons/pc.svg';
import humanImage from '../../../assets/images/homepage-human-call.png';
import facebook from '../../../assets/icons/facebook.svg';
import twitter from '../../../assets/icons/twitter.svg';
import instagram from '../../../assets/icons/instagram.svg';
import ellipse1 from '../../../assets/icons/ellipse1.svg';
import ellipse2 from '../../../assets/icons/ellipse2.svg';
import gudang from '../../../assets/icons/gudang.svg';
import './index.css';
import { Link, useHistory } from 'react-router-dom';

const Home = () => {
  const [state, setState] = useState({
    page: 'seller',
  });
  const history = useHistory();

  // const renderItem = (props) => {
  //     const { color } = props
  //     let data = []
  //     for (let i = 1; i <= 4; i++) {
  //         data.push(
  //             <div className="card background-card" style={{ paddingTop: '200px', marginTop: '20px', marginRight: '20px', paddingLeft: '20px', paddingRight: '20px' }}>
  //                 <div className="">
  //                     <text style={{ color: color }}>Article</text>
  //                 </div>
  //                 <div className="">
  //                     <text style={{ color: color }}>5 Aplikasi mengenai pergudangan</text>
  //                 </div>
  //                 <div style={{ backgroundColor: color, border: '1px solid rgba(0, 0, 0, 0.05)', borderColor: 'black', borderRadius: '4px', marginBottom: '20px', maxWidth: '223px', maxHeight: '30px' }}>
  //                     <text>Lihat Selengkapnya  {`>`}</text>
  //                 </div>
  //             </div>
  //         )
  //     }
  //     return data
  // }

  return (
    <div>
      {state.page === 'seller' ? (
        <div>
          <section
            className="container-fluid background1"
            style={{ height: '720px' }}
          >
            <div className="container">
              <div className="d-flex justify-content-between align-items-center row">
                <div className="col">
                  <img className="img-fluid" src={logoBoxOfficeBlack} />
                </div>
                <div
                  className="d-flex flex-row align-items-center"
                  style={{ marginTop: '24px' }}
                >
                  <text style={{ fontWeight: 'bold', color: '#1C1C1C' }}>
                    Bergabung Sebagai
                  </text>
                  <div style={{ marginLeft: '16px' }}>
                    <Button
                      text={'Seller'}
                      width="78px"
                      height="48px"
                      color="#192A55"
                      textColor="#ffffff"
                    />
                    <Button
                      text={'Gudang'}
                      width="95px"
                      height="48px"
                      color="#CFCFCF"
                      textColor="#1C1C1C"
                      onClick={(e) => {
                        e.preventDefault();
                        setState({ ...state, page: 'gudang' });
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6 col-left">
                  <div
                    className="d-flex justify-content-center align-items-center"
                    style={{
                      width: '300px',
                      height: '40px',
                      backgroundColor: '#FDE7E8',
                      borderRadius: '40px',
                      marginBottom: '16px',
                    }}
                  >
                    <text className="wsm">Warehouse System Management</text>
                  </div>
                  <text className="font-work-sans">
                    Monitor bisnismu dengan terintegrasi oleh gudang <br></br>
                    kami
                  </text>
                  <p className="font-open-sans">
                    Boxxoffice menjadi solusi yang berkualitas, intuitif, dan
                    komprehensif untuk kebutuhan Fullfilment yang terkontrol
                    dalam model yang berkelanjutan dalam sebuah cloud system
                  </p>
                  <div className="jarak-btn">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        borderWidth: 2,
                        borderColor: '#192A55',
                      }}
                      onClick={(e) => {
                        e.preventDefault();
                        history.push('/login');
                      }}
                    >
                      <text className="btn-left">Masuk sebagai Seller</text>
                    </button>
                    <button
                      type="button"
                      className="btn"
                      style={{
                        marginLeft: '10px',
                        backgroundColor: '#192A55',
                        borderRadius: '4px',
                        borderWidth: 2,
                        borderColor: '#192A55',
                      }}
                      onClick={(e) => {
                        e.preventDefault();
                        history.push('/register');
                      }}
                    >
                      <text className="btn-right">Daftar Sekarang</text>
                    </button>
                  </div>
                </div>
                <div className="col-lg-6 col-right">
                  <img className="img-fluid" src={humanWithBoxLogo} />
                  <div className="selengkapnya">
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <text className="text">
                        Selengkapnya
                        <img
                          style={{ marginLeft: '10px' }}
                          src={DownloadIcon}
                        />
                      </text>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* section 2 */}
          <section className="section2" style={{ height: '624px' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="d-flex justify-content-center services-content row">
                    <div
                      style={{
                        backgroundColor: '#FDE7E8',
                        borderRadius: '40px',
                        paddingLeft: '16px',
                        paddingRight: '16px',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                      }}
                    >
                      <text
                        className="font-open-sans"
                        style={{ color: '#EE1B25', fontWeight: '700' }}
                      >
                        Services
                      </text>
                    </div>
                  </div>
                  <div
                    className="d-flex justify-content-center"
                    style={{ marginTop: '10px' }}
                  >
                    <text
                      className="font-work-sans"
                      style={{ color: '#1C1C1C', fontSize: '26px' }}
                    >
                      Kenapa sistem fullfilment di Boxxoffice merupakan yang
                      terbaik?
                    </text>
                  </div>
                </div>
              </div>
              <div className="row services-jarak">
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={speedometerIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Perfomance terbaik
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={padlockIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Kerahasiaan yang terjamin
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={shieldIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Asuransi dan klaim
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row services-jarak">
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={truckCarIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Tersedia banyak pilihan kurir
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={checklistHandIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Mudah dioperasikan
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={pcIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Bisa dipakai di banyak device
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* testimonni */}
          <section
            className="justify-content-center container-fluid background2"
            style={{ height: '504px' }}
          >
            <div className="container">
              <div className="row">
                <div className="col-lg-7">
                  <div>
                    <div
                      className="d-flex justify-content-center"
                      style={{
                        marginTop: '64px',
                        maxWidth: '121px',
                        maxHeight: '40px',
                        backgroundColor: '#FDE7E8',
                        borderRadius: '40px',
                        paddingLeft: '16px',
                        paddingRight: '16px',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                      }}
                    >
                      <text className="testimoni">Testimoni</text>
                    </div>
                  </div>

                  <div className="testimoni-jarak">
                    <p className="testimoni-desc">
                      “Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem <br></br>Ipsum has been the
                      industry's standard dummy text”
                    </p>
                    <p className="testimoni-jabatan">Owner ABCD Company</p>
                    <p className="testimoni-name">Jacob Jones</p>
                  </div>
                  <div className="testimoni-ellipse">
                    <img src={ellipse1} />
                    <img src={ellipse2} className="testimoni-ellipse-jarak" />
                    <img src={ellipse2} />
                  </div>
                </div>
                <div className="col-lg-5">
                  <div className="testimoni-img">
                    <img
                      style={{ maxHeight: '336px', maxWidth: '445px' }}
                      src={humanImage}
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* blog */}
          <section className="" style={{ height: '636px' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="row justify-content-center">
                    <div
                      style={{
                        backgroundColor: '#FDE7E8',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                        paddingRight: '16px',
                        paddingLeft: '16px',
                        borderRadius: '40px',
                      }}
                    >
                      <text
                        className="font-open-sans"
                        style={{ color: '#FF383A', fontWeight: 'bold' }}
                      >
                        Blog
                      </text>
                    </div>
                  </div>
                  <div className="row justify-content-center">
                    <text className="blog">
                      Lihat berita terbaru mengenai perusahaan
                    </text>
                    <p className="blog-desc">
                      The world's first software-only autonomous inventory
                      management platform for modern warehouses. Want to give it
                      a try?
                    </p>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <footer style={{ height: '344px', backgroundColor: '#192A55' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-4 footer-top">
                  <div>
                    <p className="footer-bold">Logo</p>
                  </div>
                  <div>
                    <p className="footer-normal">
                      We have been helping our user since 2021.
                    </p>
                  </div>
                  <div className="footer-img">
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={facebook} />
                    </Link>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={twitter} className="jarak" />
                    </Link>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={instagram} />
                    </Link>
                  </div>
                </div>
                <div className="col-lg-2 footer-top">
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Home</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Services</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Blog</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Testimoni</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">FAQ</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Syarat {`&`} Ketentuan</p>
                    </Link>
                  </div>
                </div>
                <div className="col-lg-3 footer-top">
                  <div>
                    <p className="footer-header">Headquarter</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                  <div>
                    <p className="footer-header sales">Sales</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 footer-top">
                  <div>
                    <p className="footer-header">Support</p>
                  </div>
                  <div>
                    <p className="footer-600">trungkienspktnd@gmail.com</p>
                  </div>
                  <div>
                    <p className="footer-header development">Development</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      ) : (
        <div>
          <section
            className="container-fluid background1"
            style={{ height: '720px' }}
          >
            <div className="container">
              <div className="d-flex justify-content-between align-items-center row">
                <div className="col">
                  <img className="img-fluid" src={logoBoxOfficeBlack} />
                </div>
                <div
                  className="d-flex flex-row align-items-center"
                  style={{ marginTop: '24px' }}
                >
                  <text style={{ fontWeight: 'bold', color: '#1C1C1C' }}>
                    Bergabung Sebagai
                  </text>
                  <div style={{ marginLeft: '16px' }}>
                    <Button
                      text={'Seller'}
                      width="78px"
                      height="48px"
                      color="#CFCFCF"
                      textColor="#1C1C1C"
                      onClick={(e) => {
                        e.preventDefault();
                        setState({ ...state, page: 'seller' });
                      }}
                    />
                    <Button
                      text={'Gudang'}
                      width="95px"
                      height="48px"
                      color="#192A55"
                      textColor="#ffffff"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6 col-left">
                  <div
                    className="d-flex justify-content-center align-items-center"
                    style={{
                      width: '300px',
                      height: '40px',
                      backgroundColor: '#FDE7E8',
                      borderRadius: '40px',
                      marginBottom: '16px',
                    }}
                  >
                    <text className="wsm">Warehouse System Management</text>
                  </div>
                  <text className="font-work-sans">
                    Dapatkan penghasilan tambahan dengan mendaftarkan gudangmu
                  </text>
                  <p className="font-open-sans">
                    Boxxoffice menjadi solusi yang berkualitas, intuitif, dan
                    komprehensif untuk kebutuhan Fullfilment yang terkontrol
                    dalam model yang berkelanjutan dalam sebuah cloud system
                  </p>
                  <div className="jarak-btn">
                    <button
                      type="button"
                      className="btn"
                      style={{
                        borderRadius: '4px',
                        borderWidth: 2,
                        borderColor: '#192A55',
                      }}
                    >
                      <text className="btn-left">Masuk sebagai Seller</text>
                    </button>
                    <button
                      type="button"
                      className="btn"
                      style={{
                        marginLeft: '10px',
                        backgroundColor: '#192A55',
                        borderRadius: '4px',
                        borderWidth: 2,
                        borderColor: '#192A55',
                      }}
                    >
                      <text className="btn-right">Daftar Sekarang</text>
                    </button>
                  </div>
                </div>
                <div className="col-lg-6 col-right">
                  <img className="img-fluid gudang-img" src={gudang} />
                  <div className="selengkapnya">
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <text className="text">
                        Selengkapnya
                        <img
                          style={{ marginLeft: '10px' }}
                          src={DownloadIcon}
                        />
                      </text>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* section 2 */}
          <section className="section2" style={{ height: '624px' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="d-flex justify-content-center services-content row">
                    <div
                      style={{
                        backgroundColor: '#FDE7E8',
                        borderRadius: '40px',
                        paddingLeft: '16px',
                        paddingRight: '16px',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                      }}
                    >
                      <text
                        className="font-open-sans"
                        style={{ color: '#EE1B25', fontWeight: '700' }}
                      >
                        Services
                      </text>
                    </div>
                  </div>
                  <div
                    className="d-flex justify-content-center"
                    style={{ marginTop: '10px' }}
                  >
                    <text
                      className="font-work-sans"
                      style={{ color: '#1C1C1C', fontSize: '26px' }}
                    >
                      Kenapa sistem fullfilment di Boxxoffice merupakan yang
                      terbaik?
                    </text>
                  </div>
                </div>
              </div>
              <div className="row services-jarak">
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={speedometerIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Perfomance terbaik
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={padlockIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Kerahasiaan yang terjamin
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={shieldIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Asuransi dan klaim
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row services-jarak">
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={truckCarIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Tersedia banyak pilihan kurir
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={checklistHandIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Mudah dioperasikan
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div className="">
                    <div>
                      <img src={pcIcon} />
                    </div>
                    <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                      <text
                        className="services"
                        style={{ color: '#1C1C1C', fontSize: '24px' }}
                      >
                        Bisa dipakai di banyak device
                      </text>
                    </div>
                    <div>
                      <p className="services-desc">
                        The world's first software-only autonomous inventory
                        management platform for modern warehouses
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* testimonni */}
          <section
            className="justify-content-center container-fluid background2"
            style={{ height: '504px' }}
          >
            <div className="container">
              <div className="row">
                <div className="col-lg-7">
                  <div>
                    <div
                      className="d-flex justify-content-center"
                      style={{
                        marginTop: '64px',
                        maxWidth: '121px',
                        maxHeight: '40px',
                        backgroundColor: '#FDE7E8',
                        borderRadius: '40px',
                        paddingLeft: '16px',
                        paddingRight: '16px',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                      }}
                    >
                      <text className="testimoni">Testimoni</text>
                    </div>
                  </div>

                  <div className="testimoni-jarak">
                    <p className="testimoni-desc">
                      “Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem <br></br>Ipsum has been the
                      industry's standard dummy text”
                    </p>
                    <p className="testimoni-jabatan">Owner ABCD Company</p>
                    <p className="testimoni-name">Jacob Jones</p>
                  </div>
                  <div className="testimoni-ellipse">
                    <img src={ellipse1} />
                    <img src={ellipse2} className="testimoni-ellipse-jarak" />
                    <img src={ellipse2} />
                  </div>
                </div>
                <div className="col-lg-5">
                  <div className="testimoni-img">
                    <img
                      style={{ maxHeight: '336px', maxWidth: '445px' }}
                      src={humanImage}
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* blog */}
          <section className="" style={{ height: '636px' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="row justify-content-center">
                    <div
                      style={{
                        backgroundColor: '#FDE7E8',
                        paddingTop: '10px',
                        paddingBottom: '10px',
                        paddingRight: '16px',
                        paddingLeft: '16px',
                        borderRadius: '40px',
                      }}
                    >
                      <text
                        className="font-open-sans"
                        style={{ color: '#FF383A', fontWeight: 'bold' }}
                      >
                        Blog
                      </text>
                    </div>
                  </div>
                  <div className="row justify-content-center">
                    <text className="blog">
                      Lihat berita terbaru mengenai perusahaan
                    </text>
                    <p className="blog-desc">
                      The world's first software-only autonomous inventory
                      management platform for modern warehouses. Want to give it
                      a try?
                    </p>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div
                    className="card background-card"
                    style={{
                      paddingTop: '200px',
                      marginTop: '20px',
                      marginRight: '20px',
                      paddingLeft: '20px',
                      paddingRight: '20px',
                    }}
                  >
                    <div className="">
                      <text className="blog-article">Article</text>
                    </div>
                    <div className="">
                      <text className="blog-aplikasi">
                        5 Aplikasi mengenai pergudangan
                      </text>
                    </div>
                    <div
                      style={{
                        backgroundColor: 'transparant',
                        border: '1px solid rgba(0, 0, 0, 0.05)',
                        borderColor: '#FAFAFF',
                        borderRadius: '4px',
                        marginBottom: '20px',
                        maxWidth: '223px',
                        maxHeight: '30px',
                      }}
                    >
                      <text className="blog-selengkapnya">
                        Lihat Selengkapnya {`>`}
                      </text>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <footer style={{ height: '344px', backgroundColor: '#192A55' }}>
            <div className="container">
              <div className="row">
                <div className="col-lg-4 footer-top">
                  <div>
                    <p className="footer-bold">Logo</p>
                  </div>
                  <div>
                    <p className="footer-normal">
                      We have been helping our user since 2021.
                    </p>
                  </div>
                  <div className="footer-img">
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={facebook} />
                    </Link>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={twitter} className="jarak" />
                    </Link>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <img src={instagram} />
                    </Link>
                  </div>
                </div>
                <div className="col-lg-2 footer-top">
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Home</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Services</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Blog</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Testimoni</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">FAQ</p>
                    </Link>
                  </div>
                  <div>
                    <Link to="" style={{ textDecoration: 'none' }}>
                      <p className="footer-bold">Syarat {`&`} Ketentuan</p>
                    </Link>
                  </div>
                </div>
                <div className="col-lg-3 footer-top">
                  <div>
                    <p className="footer-header">Headquarter</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                  <div>
                    <p className="footer-header sales">Sales</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                </div>
                <div className="col-lg-3 footer-top">
                  <div>
                    <p className="footer-header">Support</p>
                  </div>
                  <div>
                    <p className="footer-600">trungkienspktnd@gmail.com</p>
                  </div>
                  <div>
                    <p className="footer-header development">Development</p>
                  </div>
                  <div>
                    <p className="footer-600">
                      8502 Preston Rd. Inglewood, Maine 98380
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      )}
    </div>
  );
};

export default Home;
