import React from "react";
import Navbar from '../../../components/elements/Navbar/Navbar'
import SectionOne from "./components/sectionOne";
import SectionTwo from "./components/sectionTwo";
import SectionThree from "./components/sectionThree";
import SectionFour from "./components/sectionFour";
import Footer from "./components/Footer";

const Homepage = () => {

  return(
    <>
      <Navbar/>
      <SectionOne/>
      <SectionTwo/>
      <SectionThree/>
      <SectionFour/>
      <Footer/>
    </>
  )
}

export default Homepage  