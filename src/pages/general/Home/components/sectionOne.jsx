import React, {useEffect, useState} from "react";
import image from '../../../../assets/images/hero.svg'
import Button from '../components/Button'
import '../components/style.css'
import '../components/theme.css'
import { useDispatch, useSelector } from 'react-redux';
import { getListSetting } from '../reduxAction';
import { useHistory } from 'react-router-dom';
import { Link } from "react-scroll";
import DownloadIcon from '../../../../assets/icons/download.svg';

const SectionOne = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { listSetting } = useSelector((state) => state.home);
  // console.log('here=>', listSetting[0].value)
  useEffect(()=>{
     dispatch(getListSetting());
  },[])
  
  return(   
    // <section id="home" className="py-0" style={{position: "relative"}}>
    //   <div className="bg-holder d-none d-md-block bg-none" style={{backgroundImage: `url("${image}")`, backgroundPosition: "right bottom", backgroundSize: "contain"}}>
    //   </div>

    //   <div className="container position-relative">
    //     <div className="row align-items-center min-vh-75">
    //       <div className="col-md-12 col-lg-6 text-md-start py-8">
    //         <h1 className="mb-4 hero-title">
    //           {
    //             listSetting.length &&
    //             <span>{listSetting[0].value}</span>
    //           }
    //         </h1>
    //         <div>
    //           <div className="text-white mt-5 mb-5">
    //           {
    //             listSetting.length &&
    //             <span>{listSetting[1].value}</span>
    //           }                  
    //           </div>
    //           <div className="d-flex mt-3 mb-2">
    //             <div className="mr-2">
    //               <Button
    //                 title = "Masuk sebagai Seller"
    //                 backgroundColor = "transparent"
    //                 border = "1px solid #ffffff"
    //                 color = "#ffffff"
    //                 onClick={(e) => {
    //                   e.preventDefault();
    //                   history.push('/login');
    //                 }}
    //               />
    //             </div>
    //             <div>
    //               <Button
    //                 title = "Daftar Sekarang"
    //                 backgroundColor = "#ffffff"
    //                 color = "#192A55"
    //                 border = "none"
    //                 onClick={(e) => {
    //                   e.preventDefault();
    //                   history.push('/register');
    //                 }}
    //               />
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //     <div className="mt-4 float-right">
    //       <Link to="service" smooth={true} className="more">
    //         <b>Selengkapnya</b>
    //         <img className="ml-2" src={DownloadIcon}/>  
    //       </Link>
    //     </div>
    //   </div>
    // </section>



    <section id="home">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 col-lg-7 d-flex align-items-center justify-content-end">
            <div className="text-width">
              <h1 className="text-white">
                {
                  listSetting.length &&
                  <span className="hero-title">{listSetting[0].value}</span>
                }
              </h1>
              <div>
                <div className="text-white">
                {
                  listSetting.length &&
                  <span>{listSetting[1].value}</span>
                }                  
                </div>
                <div className="d-flex mt-3 mb-4">
                  <div className="mr-2">
                    <Button
                      title = "Masuk sebagai Seller"
                      backgroundColor = "transparent"
                      border = "1px solid #ffffff"
                      color = "#ffffff"
                      onClick={(e) => {
                        e.preventDefault();
                        history.push('/login');
                      }}
                    />
                  </div>
                  <div>
                    <Button
                      title = "Daftar Sekarang"
                      backgroundColor = "#ffffff"
                      color = "#192A55"
                      border = "none"
                      onClick={(e) => {
                        e.preventDefault();
                        history.push('/register');
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-5 p-0 hide">
            <div className="image-right">
              <img className="img-fluid" src={image} />
            </div>
          </div>
        </div>
        <div className="row bg">
          <div className="container-fluid ml-0">          
            <div className="container">
              <div className="col">
                <div className="mt-4 float-right">
                  <Link to="service" smooth={true} className="more">
                    <b>Selengkapnya</b>
                    <img className="ml-2" src={DownloadIcon}/>          

                  </Link>
                </div>
              </div>
            </div>       
          </div>
        </div>
      </div>
    </section> 
    
    // <section id="section1">
    //   <div className="container">
    //     <div className="row">
    //       <div className="col-lg-6 d-flex align-items-center">
    //         <div>
    //           <h1 className="text-white">
    //             {
    //               listSetting.length &&
    //               <span>{listSetting[0].value}</span>
    //             }
    //           </h1>
    //           <div>
    //             <div className="text-white">
    //             {
    //               listSetting.length &&
    //               <span>{listSetting[1].value}</span>
    //             }                  
    //             </div>
    //             <div className="d-flex mt-3">
    //               <div className="mr-2">
    //                 <Button
    //                   title = "Masuk sebagai Seller"
    //                   backgroundColor = "transparent"
    //                   border = "1px solid #ffffff"
    //                   color = "#ffffff"
    //                 />
    //               </div>
    //               <div>
    //                 <Button
    //                   title = "Daftar Sekarang"
    //                   backgroundColor = "#ffffff"
    //                   color = "#192A55"
    //                   border = "none"
    //                 />
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //       <div className="col-lg-6 mt-4 mb-4">
    //         <div>
    //           <img className="img-fluid" src={image} />
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // </section>
  )
}

export default SectionOne