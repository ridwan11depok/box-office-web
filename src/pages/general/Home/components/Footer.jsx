import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { getListSetting } from '../reduxAction';
import facebook from '../../../../assets/icons/facebook.svg';
import twitter from '../../../../assets/icons/twitter.svg';
import instagram from '../../../../assets/icons/instagram.svg';
import logoBoxOfficeWhite from '../../../../assets/images/logoBoxOfficeWhite.svg';

const Footer = () => {

  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { listSetting } = useSelector((state) => state.home);
  // console.log('here=>', listSetting[0].value)
  useEffect(()=>{
     dispatch(getListSetting());
  },[])

  return(
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <div className="mt-4">
              <img className="img-fluid" src={logoBoxOfficeWhite} />
            </div>
            <div className="mt-2">
              <div className="text-white">
                {
                  listSetting.length &&
                  <span>{listSetting[5].value}</span>
                }
              </div>
            </div>
            <div className="d-flex mt-5 mb-3">
              <div className="mr-3">
                {
                  listSetting.length &&
                  <a href={listSetting[6].value}>
                    <img src={facebook}/>
                  </a>
                }
              </div>
              <div className="mr-3">
                {
                  listSetting.length &&
                  <a href={listSetting[7].value}>
                    <img src={twitter}/>
                  </a>
                }
              </div>
              <div className="mr-3">
                {
                  listSetting.length &&
                  <a href={listSetting[8].value}>
                    <img src={instagram}/>
                  </a>
                }
              </div>
            </div>
          </div>
          <div className="col-lg-4 text-white">
            <div className="mt-4">
              Headquarter
            </div>
            <div className="mt-3">
              {
                listSetting.length &&
                <span>{listSetting[9].value}</span>
              }
            </div>
            <div className="mt-4">
              Support
            </div>
            <div className="mt-3 mb-3">
              {
                listSetting.length &&
                <span>{listSetting[11].value}</span>
              }
            </div>
          </div>
          <div className="col-lg-4 text-white">
            <div className="mt-4">
              Sales
            </div>
            <div className="mt-3 mb-3">
              {
                listSetting.length &&
                <span>{listSetting[10].value}</span>
              }
            </div>
          </div>
        </div>
      </div>

    </footer>
  )
}

export default Footer