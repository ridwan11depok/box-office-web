import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { getListSetting, getListService } from '../reduxAction';
import '../components/style.css'
import DownloadIcon from '../../../../assets/icons/download.svg';

const SectionTwo = () => {

  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { listSetting } = useSelector((state) => state.home);
  const { listService } = useSelector((state) => state.home);
  // console.log('here=>', listService)
  useEffect(()=>{
     dispatch(getListSetting());
     dispatch(getListService());
  },[])

  return(
    <section id="service">
      <div className="container">
        <div className="p-2 title">
          <b className="title-color">Services</b>
        </div>
        <div className="mt-1">
          <h2 className="desc-title">
            {
              listSetting.length &&
              <span>{listSetting[2].value}</span>
            }
          </h2>
        </div>
        <div className="row">
          {listService?.map(service=>(
            <div className="col-sm-6 col-md-6 col-lg-4">
              <div className="border rounded p-3 mt-4 mb-3">
                <img src={service.icon_full_url} className="service-image" />
                <div className="mt-2">
                  <b>{service.title}</b>
                </div>
                <div className="mt-2 mb-4">
                  {service.description}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
    )
}

export default SectionTwo