import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { getListSetting, getListBlog } from '../reduxAction';
import '../components/style.css'

const SectionFour = () => {

  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { listSetting } = useSelector((state) => state.home);
  const { listBlog } = useSelector((state) => state.home);
  // console.log('here=>', listBlog[0].thumbnail_full_url)
  useEffect(()=>{
     dispatch(getListSetting());
     dispatch(getListBlog());
  },[])

  return(
    <section id="blog" className="">
      <div className="container">
        <div className="mt-5 p-2 title">
          <b className="title-color">Blog</b>
        </div>
        <div className="mt-2">
          <h2 className="mb-3 desc-title">
            {
              listSetting.length &&
              <span>{listSetting[3].value}</span>
            }
          </h2>
          <div className="mt-2 mb-4">
            {
              listSetting.length &&
              <span>{listSetting[4].value}</span>
            }            
          </div>
        </div>
        <div className="row mt-2">
          {listBlog?.map(blog=>(
            <div className="col-sm-6 col-md-6 col-lg-3 mb-5">
              <div className="thumbnail rounded p-2" style={{ backgroundImage: `url(${blog.thumbnail_full_url})` }}>
                <div className="text-white">
                  {/* <div className="row mt-5"></div> */}
                  <div className="top-article">{blog.type}</div>
                  <div>
                    <b>{blog.title}</b>
                  </div>
                  <div role="button" className="border rounded text-center p-2 mt-3">
                    Lihat Selengkapnya {`>`}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}

export default SectionFour