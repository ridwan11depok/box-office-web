import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { getListTestimoni } from '../reduxAction';
import '../components/style.css'
import {Carousel} from 'react-bootstrap'

const SectionThree = () => {
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.loading);
  const { listTestimoni } = useSelector((state) => state.home);
  // console.log('here=>', listTestimoni)
  useEffect(()=>{
     dispatch(getListTestimoni());
  },[])

  return(
    <section id="testimoni">
      {/* <div className="container-fluid bg top-testimoni">
        <div className="row ml-1">
          <div className="container">
            <div className="mt-5 mb-3 p-2 title">
              <b className="title-color">Testimoni</b>
            </div>
          </div>
        </div>
      </div> */}
      <div className="container">
        <div className="mt-5 mb-3 p-2 title">
          <b className="title-color">Testimoni</b>
        </div>
        <Carousel controls={false} fade={false}>
          {listTestimoni?.map((testimoni, index)=>(
            <Carousel.Item interval={14000}>
              <div className="row">
                <div className="col-lg-6">
                  <h2>
                    {testimoni.description}
                  </h2>
                  <div className="mt-3">
                    {testimoni.position}
                  </div>
                  <div>
                    {testimoni.name}
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="d-flex justify-content-end">
                    <img src={testimoni.image_full_url} className="rounded"/>   
                  </div>
                </div>
              </div>
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
    </section>
  )
}

export default SectionThree