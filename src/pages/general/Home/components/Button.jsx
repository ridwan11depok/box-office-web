import React from "react";

const Button = (props) => {
  const {title, backgroundColor, color, border, borderRadius, onClick} = props
  return(
    <>
      <button 
        onClick={onClick}
        style={{
        backgroundColor: backgroundColor, 
        color: color, 
        border: border, 
        borderRadius: "8px",
        height: "48px"
        }}>
        <b>{title}</b>
      </button>
    </>
  )
}

export default Button