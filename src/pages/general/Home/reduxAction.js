import actionType from './reduxConstant';
import consume from '../../../api/consume';
import { setLoading, setToast, setLoadingTable } from '../../../redux/actions';

const setListSetting = (data) => {
  return {
    type: actionType.LIST_SETTING, 
    payload: data
  }
}
const setListService = (data) => {
  return {
    type: actionType.LIST_SERVICE, 
    payload: data
  }
}
const setListTestimoni = (data) => {
  return {
    type: actionType.LIST_TESTIMONI, 
    payload: data
  }
}
const setListBlog = (data) => {
  return {
    type: actionType.LIST_BLOG, 
    payload: data
  }
}

export const getListSetting =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      // console.log('params gudang', params)
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      // const token = localStorage.getItem('access-token-jwt');
      let data = {};
      let headers = {  };
      let response;

        response = await consume.get(
          'listSetting'
        );
      if (response) {
       
          dispatch(setListSetting(response.result));
        
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
      return Promise.resolve('success');
    } catch (err) {
      if (id) {
        dispatch(setListSetting({}));
      } 
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
      return Promise.reject('failed');
    }
  };

  export const getListService =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      // console.log('params gudang', params)
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      // const token = localStorage.getItem('access-token-jwt');
      let data = {};
      let headers = {  };
      let response;

        response = await consume.get(
          'listService'
        );
      if (response) {
       
          dispatch(setListService(response.result));
        
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
      return Promise.resolve('success');
    } catch (err) {
      if (id) {
        dispatch(setListService({}));
      } 
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
      return Promise.reject('failed');
    }
  };

  export const getListTestimoni =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      // console.log('params gudang', params)
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      // const token = localStorage.getItem('access-token-jwt');
      let data = {};
      let headers = {  };
      let response;

        response = await consume.get(
          'listTestimoni'
        );
      if (response) {
       
          dispatch(setListTestimoni(response.result));
        
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
      return Promise.resolve('success');
    } catch (err) {
      if (id) {
        dispatch(setListTestimoni({}));
      } 
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
      return Promise.reject('failed');
    }
  };

  export const getListBlog =
  (params = {}, id = null) =>
  async (dispatch, getState) => {
    try {
      // console.log('params gudang', params)
      dispatch(setLoading(true));
      dispatch(setLoadingTable(true));
      const { token } = getState().login;
      // const token = localStorage.getItem('access-token-jwt');
      let data = {};
      let headers = {  };
      let response;

        response = await consume.get(
          'listBlog'
        );
      if (response) {
       
          dispatch(setListBlog(response.result));
        
        dispatch(setLoading(false));
        dispatch(setLoadingTable(false));
      }
      return Promise.resolve('success');
    } catch (err) {
      if (id) {
        dispatch(setListBlog({}));
      } 
      dispatch(setLoading(false));
      dispatch(setLoadingTable(false));
      return Promise.reject('failed');
    }
  };