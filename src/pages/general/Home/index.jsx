import { useEffect, useState } from 'react';
import { Row, Col, Grid, Container } from 'react-bootstrap';

import logoBoxOfficeBlack from '../../../assets/images/logoBoxOfficeBlack.svg';
import humanWithBoxLogo from '../../../assets/images/humanWithBoxLogo.svg';
import halfTriangleHome from '../../../assets/images/halfTriangleHome.svg';
import Button from '../../../components/atoms/Button/Button';
import DownloadIcon from '../../../assets/icons/download.svg';
import speedometerIcon from '../../../assets/icons/speedometer.svg';
import padlockIcon from '../../../assets/icons/padlock.svg';
import shieldIcon from '../../../assets/icons/shield.svg';
import truckCarIcon from '../../../assets/icons/truckCar.svg';
import checklistHandIcon from '../../../assets/icons/checklistHand.svg';
import pcIcon from '../../../assets/icons/pc.svg';
import humanImage from '../../../assets/images/homepage-human-call.svg';

import './index.css';
import { Link } from 'react-router-dom';

const Home = () => {
  const renderItem = (props) => {
    const { color } = props;
    let data = [];
    for (let i = 1; i <= 5; i++) {
      data.push(
        <div
          className="card background-card"
          style={{
            paddingTop: '200px',
            marginTop: '20px',
            marginRight: '20px',
            paddingLeft: '20px',
            paddingRight: '20px',
          }}
        >
          <div className="">
            <text style={{ color: color }}>Article</text>
          </div>
          <div className="">
            <text style={{ color: color }}>
              5 Aplikasi mengenai pergudangan
            </text>
          </div>
          <div
            style={{
              backgroundColor: color,
              border: '1px solid rgba(0, 0, 0, 0.05)',
              borderColor: 'black',
              borderRadius: '4px',
              marginBottom: '20px',
              maxWidth: '223px',
              maxHeight: '30px',
            }}
          >
            <text>Lihat Selengkapnya {`>`}</text>
          </div>
        </div>
      );
    }
    return data;
  };

  return (
    <div>
      <section className="halfTriangel1 container-fluid">
        <div
          className="container-xxl"
          style={{ paddingRight: '50px', paddingLeft: '50px' }}
        >
          <div className="d-flex justify-content-between align-items-center row">
            <div className="col">
              <img className="img-fluid" src={logoBoxOfficeBlack} />
            </div>
            <div
              className="d-flex flex-row align-items-center"
              style={{ marginTop: '24px' }}
            >
              <text style={{ fontWeight: 'bold', color: '#1C1C1C' }}>
                Bergabung Sebagai
              </text>
              <div style={{ marginLeft: '16px' }}>
                <Button
                  text={'Seller'}
                  width="78px"
                  height="48px"
                  color="#192A55"
                  textColor="#ffffff"
                />
                <Button
                  text={'Gudang'}
                  width="95px"
                  height="48px"
                  color="#CFCFCF"
                  textColor="#1C1C1C"
                />
              </div>
            </div>
          </div>
          <div
            className="d-flex flex-row align-items-center row"
            style={{ marginTop: '100px' }}
          >
            <div className="col" style={{ marginTop: '10px' }}>
              <div
                className="d-flex justify-content-center align-items-center"
                style={{
                  width: '300px',
                  height: '40px',
                  backgroundColor: '#FDE7E8',
                  borderRadius: '40px',
                }}
              >
                <text
                  style={{
                    color: '#EE1B25',
                    fontWeight: 'bold',
                    fontFamily: '',
                  }}
                >
                  Warehouse System Management
                </text>
              </div>
              <div>
                <text
                  className="font-work-sans"
                  style={{
                    fontWeight: 'bold',
                    fontSize: '40px',
                    color: '#1C1C1C',
                  }}
                >
                  Monitor bisnismu dengan
                </text>
              </div>
              <div>
                <text
                  className="font-work-sans"
                  style={{
                    fontWeight: 'bold',
                    fontSize: '40px',
                    color: '#1C1C1C',
                  }}
                >
                  terintegrasi oleh gudang
                </text>
              </div>
              <div>
                <text
                  className="font-work-sans"
                  style={{
                    fontWeight: 'bold',
                    fontSize: '40px',
                    color: '#1C1C1C',
                  }}
                >
                  kami{' '}
                </text>
              </div>
              <div>
                <p className="font-open-sans">
                  Boxxoffice menjadi solusi yang berkualitas, intuitif, dan
                  komprehensif untuk kebutuhan Fullfilment yang terkontrol dalam
                  model yang berkelanjutan dalam sebuah cloud system
                </p>
              </div>
            </div>
            <div className="col">
              <img
                className="img-fluid"
                style={{ marginTop: '20px', marginLeft: '100px' }}
                src={humanWithBoxLogo}
              />
            </div>
          </div>
          <div className="d-flex flex-row col">
            <button
              type="button"
              className="btn"
              style={{
                borderRadius: '4px',
                borderWidth: 2,
                borderColor: '#192A55',
              }}
            >
              <text style={{ color: '#192A55' }}>Masuk sebagai Seller</text>
            </button>
            <button
              type="button"
              className="btn"
              style={{
                marginLeft: '10px',
                backgroundColor: '#192A55',
                borderRadius: '4px',
                borderWidth: 2,
                borderColor: '#192A55',
              }}
            >
              <text style={{ color: 'white' }}>Daftar Sekarang</text>
            </button>
          </div>
          <div className="d-flex justify-content-end flex-row row">
            <Link>
              <text style={{ color: '#192A55', fontWeight: '700' }}>
                Selengkapnya
              </text>
              <img style={{ marginLeft: '10px' }} src={DownloadIcon} />
            </Link>
          </div>
          {/* <img src={halfTriangleHome} style={{width:'100%', height:'100%'}}></img> */}
        </div>
      </section>
      <section
        className="container-fluid section2"
        style={{ paddingTop: '100px' }}
      >
        <div
          className="container-xxl col"
          style={{ paddingRight: '50px', paddingLeft: '50px' }}
        >
          <div className="d-flex justify-content-center row">
            <div
              style={{
                backgroundColor: '#FDE7E8',
                borderRadius: '40px',
                paddingLeft: '16px',
                paddingRight: '16px',
                paddingTop: '10px',
                paddingBottom: '10px',
              }}
            >
              <text
                className="font-open-sans"
                style={{ color: '#EE1B25', fontWeight: '700' }}
              >
                Services
              </text>
            </div>
          </div>
          <div
            className="d-flex justify-content-center"
            style={{ marginTop: '10px' }}
          >
            <text
              className="font-work-sans"
              style={{ color: '#1C1C1C', fontSize: '26px' }}
            >
              Kenapa sistem fullfilment di Boxxoffice merupakan yang terbaik?
            </text>
          </div>
          {/* <div className="d-flex justify-content-around"> */}
          <div
            className="row d-flex justify-content-between"
            style={{ marginTop: '40px' }}
          >
            <div
              className="col d-flex flex-column "
              style={{ marginRight: '20px' }}
            >
              <div>
                <img src={speedometerIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Perfomance terbaik
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
            <div className="col" style={{ marginRight: '20px' }}>
              <div>
                <img src={padlockIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Kerahasiaan yang terjamin
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
            <div className="col">
              <div>
                <img src={shieldIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Asuransi dan klaim
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
          </div>
          <div
            className="row d-flex justify-content-between"
            style={{ marginTop: '40px' }}
          >
            <div
              className="col d-flex flex-column "
              style={{ marginRight: '20px' }}
            >
              <div>
                <img src={truckCarIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Perfomance terbaik
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
            <div className="col" style={{ marginRight: '20px' }}>
              <div>
                <img src={checklistHandIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Kerahasiaan yang terjamin
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
            <div className="col">
              <div>
                <img src={pcIcon} />
              </div>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <text
                  className="font-work-sans"
                  style={{ color: '#1C1C1C', fontSize: '24px' }}
                >
                  Asuransi dan klaim
                </text>
              </div>
              <div>
                <p>
                  The world's first software-only autonomous inventory
                  management platform for modern warehouses
                </p>
              </div>
            </div>
          </div>

          {/* </div> */}
        </div>
      </section>
      <section
        className="halfTriangel2 container-fluid justify-content-center"
        style={{
          paddingRight: '50px',
          paddingLeft: '50px',
          paddingTop: '200px',
        }}
      >
        <div className="container-xxl">
          <div className="row">
            <div className="col">
              <div>
                <div
                  className="d-flex justify-content-center"
                  style={{
                    maxWidth: '121px',
                    maxHeight: '40px',
                    backgroundColor: '#FDE7E8',
                    borderRadius: '40px',
                    paddingLeft: '16px',
                    paddingRight: '16px',
                    paddingTop: '10px',
                    paddingBottom: '10px',
                  }}
                >
                  <text
                    className="font-open-sans"
                    style={{ color: '#EE1B25', fontWeight: '700' }}
                  >
                    Testimoni
                  </text>
                </div>
              </div>
              <div style={{ marginTop: '50px' }}>
                <p
                  className="font-work-sans"
                  style={{ fontSize: '26px', color: '#1C1C1C' }}
                >
                  “Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text”
                </p>
              </div>
              <div>
                <span className="font-open-sans" style={{ color: '#4F4F4F' }}>
                  Owner ABCD Company
                </span>
              </div>
              <div>
                <span className="font-open-sans" style={{ color: '#4F4F4F' }}>
                  Jacob Jones
                </span>
              </div>
            </div>
            <div
              className="col justify-content-center align-items-end"
              style={{ paddingTop: '60px' }}
            >
              <div className="d-flex justify-content-center align-self-end">
                <img
                  style={{ maxHeight: '336px', maxWidth: '445px' }}
                  src={humanImage}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section
        className="container-xxl"
        style={{ paddingRight: '50px', paddingLeft: '50px' }}
      >
        <div className="row justify-content-center">
          <div
            style={{
              backgroundColor: '#FDE7E8',
              paddingTop: '10px',
              paddingBottom: '10px',
              paddingRight: '16px',
              paddingLeft: '16px',
              borderRadius: '40px',
            }}
          >
            <text
              className="font-open-sans"
              style={{ color: '#FF383A', fontWeight: 'bold' }}
            >
              Blog
            </text>
          </div>
        </div>
        <div className="row justify-content-center">
          <text
            className="font-work-sans"
            style={{
              color: '#1C1C1C',
              fontWeight: 'bold',
              fontSize: '32px',
              marginTop: '20px',
            }}
          >
            Lihat berita terbaru mengenai perusahaan
          </text>
        </div>
        <div className="row justify-content-center">
          <text
            className="font-open-sans"
            style={{
              color: '#4F4F4F',
              fontWeight: 'normal',
              fontSize: '16px',
              marginTop: '20px',
            }}
          >
            The world's first software-only autonomous inventory management
            platform for modern warehouses. Want to give it a try?
          </text>
        </div>
        <div className="row justify-content-center">
          {renderItem({ color: 'red' })}
        </div>
      </section>
    </div>
  );
};

export default Home;
