const actionType = {
  LIST_SETTING: 'LIST_SETTING',
  LIST_SERVICE: 'LIST_SERVICE',
  LIST_TESTIMONI: 'LIST_TESTIMONI',
  LIST_BLOG: 'LIST_BLOG',
};
export default actionType;