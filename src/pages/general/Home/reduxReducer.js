import actionType from './reduxConstant';

const initialState = {
  listSetting: {},
  listService: [],
  listTestimoni: [],
  listBlog: [],

};

const homeReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.LIST_SETTING:
      return {
        ...prevState,
        listSetting: action.payload,
      };
    case actionType.LIST_SERVICE:
      return {
        ...prevState,
        listService: action.payload,
      };
    case actionType.LIST_TESTIMONI:
      return {
        ...prevState,
        listTestimoni: action.payload,
      };
    case actionType.LIST_BLOG:
      return {
        ...prevState,
        listBlog: action.payload,
      };
    default:
      return prevState;
  }
};

export default homeReducer;
