import {
  LOGIN,
  ERROR_REGISTER,
  ROLE,
  DATA_USER,
  LOGOUT_ACCOUNT,
} from './reduxConstant';

const initialState = {
  token: '',
  errorRegister: null,
  role: '',
  dataUser: {},
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payload,
      };
    case ERROR_REGISTER:
      return {
        ...state,
        errorRegister: action.payload,
      };
    case ROLE:
      return {
        ...state,
        role: action.payload,
      };
    case DATA_USER:
      return {
        ...state,
        dataUser: action.payload,
      };
    case LOGOUT_ACCOUNT:
      return {
        ...state,
        dataUser: {},
        token: '',
        role: '',
      };
    default:
      return state;
  }
};

export default loginReducer;
