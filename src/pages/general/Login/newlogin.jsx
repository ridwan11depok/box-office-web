import './style/IndexLogin.css';
import hide from '../../../assets/icons/hide.svg';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Button } from 'react-bootstrap';
import React, { useState } from 'react';
import { useEffect } from 'react';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [emailErr, setEmailErr] = useState({});
  const [passwordErr, setPasswordErr] = useState({});

  // const login = (async() => {
  //     console.warn(email,password)
  //     let item={email,password};
  //     let result = await fetch("https://54.254.190.25/api/v1/login",{
  //         method: 'POST',
  //         headers: {
  //             "Content-Type":"application/json",
  //             "Accept":'application/json'
  //         },
  //         body: JSON.stringify(item)
  //     });
  //     result = await result.json();
  // },[]);

  // useEffect(async() => {
  //    const response = await fetch('https://54.254.190.25/api/v1/login');
  //    const data = await response.json();
  //    console.log(data);
  //    const [item] = data.results;
  //    setEmail(item);
  //    setPassword(item);
  // },[]);

  useEffect(() => {
    if (email !== '') {
      setEmailErr({});
    }
  }, [email]);

  useEffect(() => {
    if (password !== '') {
      setPasswordErr({});
    }
  }, [password]);

  const onSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
    if (isValid) {
      //API in here
      setEmail('');
      setPassword('');
    }
  };

  const formValidation = () => {
    const emailErr = {};
    const passwordErr = {};
    let isValid = true;

    if (email === '') {
      // console.log('here');
      // const emailErr = {};
      emailErr.errors = 'Alamat email tidak boleh kosong.';
      setEmailErr(emailErr);
    }
    if (email !== '') {
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(email)) {
        isValid = false;
        emailErr.errors = 'Alamat email tidak ditemukan.';
      }
    }

    if (password === '') {
      // const passwordErr = {};
      passwordErr.errors = 'Password tidak boleh kosong.';
      setPasswordErr(passwordErr);
    }

    if (password !== '') {
      isValid = false;
      passwordErr.errors = 'Password salah';
    }

    setEmailErr(emailErr);
    setPasswordErr(passwordErr);
    return isValid;
  };

  return (
    <div className="background-image-login">
      <div className="overlay-login">
        <Row className="content-login">
          <Col className="col-xs-12 col-sm-12 col-lg-8 col-12">
            <div className="logo">
              <img
                src={logoboxoffice}
                alt="Logo BoxOffice"
                className="img-fluid"
              />
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-3 col-12">
            <div className="row">
              <div className="col-xs-2 col-sm-2 col-md-3 col-lg-12"></div>
              <div className="col-xs-8 col-sm-8 col-md-6 col-lg-12">
                <div className="wrapper1-login">
                  <div className="login-box">
                    <Form onSubmit={onSubmit} className="form-login">
                      <text className="login-text">New Login</text>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label className="text-form-login">
                          Email
                        </Form.Label>
                        <Form.Control
                          className="text-placeholder-login"
                          type="text"
                          value={email}
                          onChange={(e) => {
                            setEmail(e.target.value);
                          }}
                          placeholder="Masukan Email"
                        />
                        {Object.keys(emailErr).map((key) => {
                          return (
                            <div style={{ color: 'red' }}>{emailErr[key]}</div>
                          );
                        })}
                      </Form.Group>
                      <Form.Group className="" controlId="formGroupPassword">
                        <Form.Label className="text-form-login">
                          Password
                        </Form.Label>
                        <Form.Control
                          className="text-placeholder-login"
                          type="password"
                          value={password}
                          onChange={(e) => {
                            setPassword(e.target.value);
                          }}
                          placeholder="Masukkan password"
                        />
                        <img
                          src={hide}
                          alt="hide password"
                          className="password-hide"
                        />
                        {Object.keys(passwordErr).map((key) => {
                          return (
                            <div style={{ color: 'red' }}>
                              {passwordErr[key]}
                            </div>
                          );
                        })}
                      </Form.Group>
                      <Form.Label className="text-forget form-group text-right">
                        <Link to="" className="a-login">
                          {' '}
                          Lupa Password ?{' '}
                        </Link>
                      </Form.Label>
                      <Button className="tombol-login login" type="submit">
                        Login
                      </Button>
                      <Form.Label className="text1-login">
                        Dengan menggunakan apl ikasi ini anda menyetujui{' '}
                        <Link to="" className="a-login">
                          {' '}
                          syarat dan ketentuan{' '}
                        </Link>{' '}
                      </Form.Label>
                      <Form.Label className="text2-login">
                        Belum punya akun?{' '}
                        <Link to="/register" className="a-login">
                          {' '}
                          Daftar Disini{' '}
                        </Link>{' '}
                      </Form.Label>
                    </Form>
                  </div>
                </div>
              </div>

              <div className="col-xs-2 col-sm-2 col-md-3 col-lg-12"></div>
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-1 col-12"></Col>
        </Row>
      </div>
    </div>
  );
};

export default Login;
