import './style/IndexLogin.css';
import hide from '../../../assets/icons/hide.svg';
import show from '../../../assets/icons/show.svg';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import { Link, useHistory } from 'react-router-dom';
import { Row, Col, Form, Button } from 'react-bootstrap';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { loginAction, setRole } from './reduxAction';
import { toast } from 'react-toastify';
import { hex_md5 } from '../../../utils/md5';

const Login = (props) => {
  const { loginAction, setRole } = props;
  const history = useHistory();
  const [state, setState] = useState({
    email: '',
    password: '',
    error: null,
    isSecurePassword: true,
    loading: false,
  });

  useEffect(() => {
    if (state.password && state.error?.password) {
      setState({ ...state, error: removeError('password') });
    }
  }, [state]);

  useEffect(() => {
    if (state.email && state.error?.email) {
      setState({ ...state, error: removeError('email') });
    }
  }, [state.email]);

  const removeError = (errorParams) => {
    const deleted = Object.keys(state.error)
      .filter((key) => key !== errorParams)
      .reduce((obj, key) => {
        obj[key] = state.error[key];
        return obj;
      }, {});
    return deleted;
  };

  const onSubmit = async (e) => {
    try {
      e.preventDefault();
      setState({ ...state, loading: true });
      const payload = {
        email: state.email,
        password: hex_md5(state.password),
      };

      const result = await loginAction(payload);
      // console.log('result =>', result);
      if (!result.result) {
        setState({ ...state, loading: false });
        toast.error(result.error.message, {
          position: 'top-center',
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        // let customer = 'Customer';
        // console.log('test', result.result.user_type);
        setState({ ...state, loading: false });
        localStorage.setItem('access-token-jwt', result.result.token);
        localStorage.setItem('role', result.result.user_type);
        let role = result.result.user_type;
        setRole(role);
        if (role === 'Customer') {
          history.replace('/dashboard');
        } else if (role === 'Administrator') {
          history.replace('/admin/dashboard');
        } else if (role === 'Warehouse') {
          history.replace('/warehouse/dashboard');
        }
      }
    } catch (err) {
      setState({ ...state, loading: false });
      if (err?.error?.message) {
        toast.error(err.error.message, {
          position: 'top-center',
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      if (err.code === 422) {
        setState({ ...state, error: err.error.errors });
      }
    }
  };

  return (
    <div className="background-image-login">
      <div className="overlay-login">
        <Row className="content-login">
          <Col className="col-xs-12 col-sm-12 col-lg-6 col-12">
            <div className="logo">
              <img
                src={logoboxoffice}
                alt="Logo BoxOffice"
                width="400px"
                height="400px"
                className="img-fluid"
              />
            </div>
          </Col>
          <Col className="col-xs-12 col-sm-12 col-lg-3 col-12">
            <div className="row">
              <div className="col">
                <div className="wrapper1-login">
                  <div className="login-box">
                    <Form
                      onSubmit={onSubmit}
                      className="form-login"
                      style={{ paddingRight: '15px' }}
                    >
                      <text className="login-text">Masuk</text>
                      <Form.Group controlId="formGroupEmail">
                        <Form.Label className="text-form-login">
                          Email
                        </Form.Label>
                        <Form.Control
                          className="text-placeholder-login"
                          type="text"
                          value={state.email}
                          onChange={(e) => {
                            setState({ ...state, email: e.target.value });
                          }}
                          placeholder="Masukan Email"
                        />
                        {state.error &&
                          state.error?.email &&
                          state.error.email.map((el, index) => {
                            return (
                              <text key={index} style={{ color: 'red' }}>
                                {el}
                              </text>
                            );
                          })}
                      </Form.Group>
                      <Form.Group className="" controlId="formGroupPassword">
                        <Form.Label className="text-form-login">
                          Password
                        </Form.Label>
                        <Form.Control
                          className="text-placeholder-login"
                          type={state.isSecurePassword ? 'password' : 'text'}
                          value={state.password}
                          onChange={(e) => {
                            setState({ ...state, password: e.target.value });
                          }}
                          placeholder="Masukkan password"
                        />
                        <img
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            setState({
                              ...state,
                              isSecurePassword: !state.isSecurePassword,
                            });
                          }}
                          src={state.isSecurePassword ? hide : show}
                          alt="hide password"
                          className="password-hide"
                        />
                        <div style={{ paddingTop: '10px' }}>
                          {state.error &&
                            state.error?.password &&
                            state.error.password.map((el, index) => {
                              return (
                                <text key={index} style={{ color: 'red' }}>
                                  {el}
                                </text>
                              );
                            })}
                        </div>
                      </Form.Group>
                      <Form.Label className="text-forget form-group text-right">
                        <Link
                          to=""
                          className="a-login"
                          style={{ textDecoration: 'none' }}
                        >
                          {' '}
                          Lupa Password ?{' '}
                        </Link>
                      </Form.Label>
                      <Button
                        disabled={state.loading ? true : false}
                        className="tombol-login login"
                        type="submit"
                      >
                        Login
                      </Button>
                      <Form.Label className="text1-login">
                        Dengan menggunakan aplikasi ini anda menyetujui{' '}
                        <Link
                          to=""
                          className="a-login"
                          style={{ textDecoration: 'none' }}
                        >
                          {' '}
                          syarat dan ketentuan{' '}
                        </Link>{' '}
                      </Form.Label>
                      <Form.Label className="text2-login">
                        Belum punya akun?{' '}
                        <Link
                          to="/register"
                          className="a-login"
                          style={{ textDecoration: 'none' }}
                        >
                          {' '}
                          Daftar Disini{' '}
                        </Link>{' '}
                      </Form.Label>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

const reduxState = (state) => ({});

const reduxDispatch = (dispatch) => ({
  loginAction: (payload) => dispatch(loginAction(payload)),
  setRole: (payload) => dispatch(setRole(payload)),
});

export default connect(reduxState, reduxDispatch)(Login);
