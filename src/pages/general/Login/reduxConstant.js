export const REGISTER = 'REGISTER';
export const ERROR_REGISTER = 'ERROR_REGISTER';
export const LOGIN = 'LOGIN';
export const ROLE = 'ROLE';
export const DATA_USER = 'DATA_USER';
export const LOGOUT_ACCOUNT = 'LOGOUT_ACCOUNT';
