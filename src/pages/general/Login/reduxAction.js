import {
  LOGIN,
  ERROR_REGISTER,
  ROLE,
  DATA_USER,
  LOGOUT_ACCOUNT,
} from './reduxConstant';
import API from '../../../api/index';
import consume from '../../../api/consume';

export const handleRegister = (payload) => async (dispatch) => {
  try {
    const result = await consume.post('register', payload, {
      content_type: 'multipart/form-data',
    });
    dispatch({
      type: ERROR_REGISTER,
      payload: null,
    });
    return Promise.resolve(result);
  } catch (err) {
    if (err.code === 422) {
      // console.log('errorr', err)
      dispatch({
        type: ERROR_REGISTER,
        payload: err.error.errors,
      });
    }
    return Promise.reject(err);
  }
};

export const setRole = (payload) => async (dispatch) => {
  dispatch({
    type: ROLE,
    payload: payload,
  });
  return Promise.resolve(payload);
};

export const loginAction = (payload) => async (dispatch) => {
  try {
    const result = await consume.post('login', payload);
    if (result.result) {
      dispatch({
        type: LOGIN,
        payload: result.result.token,
      });
      dispatch({
        type: DATA_USER,
        payload: result.result,
      });
    }
    if (result.result.store) {
    }
    localStorage.setItem('store', JSON.stringify(result.result.store || {}));
    localStorage.setItem(
      'roleUser',
      JSON.stringify(result.result.role[0] || {})
    );
    return Promise.resolve(result);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const logout = () => {
  return {
    type: LOGOUT_ACCOUNT,
  };
};
