import { toast } from 'react-toastify';
import baseUrl from '../api/url';

export const validateFile = (obj, callback) =>
  new Promise((resolve, reject) => {
    const arg = Object.assign(
      {
        files: '',
        allowedExtensions: ['jpg', 'png', 'svg'],
        sizeLimit: 1_000_000, // 1 megabyte,
        type: 'general', //image
      },
      obj
    );
    const { files, allowedExtensions, sizeLimit, type } = arg;

    if (files) {
      const { name: fileName, size: fileSize } = files[0];
      const fileExtension = fileName.split('.').pop();
      if (!allowedExtensions.includes(fileExtension)) {
        toast.error('Tipe file tidak diizinkan', {
          position: 'top-center',
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        // return null;
      } else if (fileSize > sizeLimit) {
        toast.error('Ukuran file terlalu besar', {
          position: 'top-center',
          autoClose: 2000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        // return null;
      } else {
        let file = files[0];

        if (type === 'image') {
          const image = new Image();
          let width = '';
          let height = '';

          let url = URL.createObjectURL(files[0]);
          image.onload = function () {
            width = image.width;
            height = image.height;
            resolve({
              file,
              width,
              height,
              url,
            });
            URL.revokeObjectURL(image.src);
          };
          image.src = URL.createObjectURL(files[0]);
        } else {
          var reader = new FileReader();
          // let base64File = ''
          reader.onloadend = function (e) {
            resolve({ file })
            if(callback)callback(e.target.result)
          }
          reader.readAsDataURL(file);
        }
      }
    }
  });
