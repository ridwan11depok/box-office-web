import { useRef, useState } from 'react';

//source : https://www.techprescient.com/react-custom-hook-typescript-to-download-a-file-through-api/
export const useDownloadFile = ({
  apiDefinition = async () => {},
  preDownloading = () => {},
  postDownloading = () => {},
  onError = () => {},
  getFileName = () => {},
}) => {
  const ref = useRef();
  const [url, setFileUrl] = useState();
  const [name, setFileName] = useState();
  const download = async () => {
    try {
      preDownloading();
      const { data } = await apiDefinition();
      const url = URL.createObjectURL(new Blob([data]));
      setFileUrl(url);
      setFileName(getFileName());
      ref.current?.click();
      postDownloading();
      URL.revokeObjectURL(url);
      console.log(data);
    } catch (error) {
      onError();
      console.log(error);
    }
  };
  return { download, ref, url, name };
};
