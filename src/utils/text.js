export const formatRupiah = (value) => {
  const formated = new Intl.NumberFormat('id', {
    style: 'currency',
    currency: 'IDR',
  }).format(value);
  return formated.substring(0, formated.length - 3);
};

export function get_url_extension( url ) {
  return url.split(/[#?]/)[0].split('.').pop().trim();
}

export const statusRegularTransaction = (status = 0) => {
  const options = [
    'Pending',
    'Pickup',
    'At Warehouse',
    'Shipping',
    'Manifested',
    'Sent',
    'Done',
    'Cancel',
    'Reject',
  ];
  return options[status];
};

export const statusDispatchOrder = (status = 0) => {
  const options = [
    'Pending',
    'Picking',
    'Packing',
    'Shipping',
    'Manifested',
    'Sent',
    'Done',
    'Return',
    'Cancel',
    'Reject',
  ];
  return options[status];
};
