import actionType from '../constant';

export const setLoading = (isLoading) => {
  return {
    type: actionType.SET_LOADING,
    payload: isLoading,
  };
};

export const setToast = (payload) => {
  return {
    type: actionType.SET_TOAST,
    payload,
  };
};

export const setLoadingTable = (isLoading) => {
  return {
    type: actionType.SET_LOADING_TABLE,
    payload: isLoading,
  };
};

export const setErrorData = (data) => {
  return {
    type: actionType.ERROR_DATA,
    payload: data,
  };
};
