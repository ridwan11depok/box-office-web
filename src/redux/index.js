import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer/index';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { encryptTransform } from 'redux-persist-transform-encrypt';
import expireIn from 'redux-persist-transform-expire-in';

//config redux-persist
const expireTime = 24 * 60 * 60 * 1000; // expire in 24h
const expirationKey = 'expirationKey';
const rootPersistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['login'],
  transforms: [
    encryptTransform({
      secretKey:
        'boxx_office-super-secret-key-13142fgvahsnbsgskdmclldodmnc bbdhdngdgdgdxxhblablabla',
      onError: function (error) {
        // Handle the error.
      },
    }),
    expireIn(expireTime, expirationKey),
  ],
};

const persistedReducer = persistReducer(rootPersistConfig, reducer);
const store = createStore(persistedReducer, applyMiddleware(thunk));
const persistor = persistStore(store);
export { store, persistor };
