const actionType = {
  SET_LOADING: 'SET_LOADING',
  SET_ROLE: 'SET_ROLE',
  SET_TOAST: 'TOAST',
  SET_LOADING_TABLE: 'SET_LOADING_TABLE',
  ERROR_DATA: 'ERROR_DATA'
};

export default actionType;
