import actionType from '../constant';
const initialState = {
  errorData: {},
};

const errorReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.ERROR_DATA:
      return {
        ...prevState,
        errorData: action.payload,
      };
    default:
      return prevState;
  }
};

export default errorReducer;
