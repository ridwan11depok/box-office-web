import { combineReducers } from 'redux';

//import reducers
import login from '../../pages/general/Login/reduxReducer';
import prosidebar from '../../components/admin/prosidebar/reduxReducer';
import loading from './loading';
import inbound from '../../pages/customer/Inbound/reduxReducer';
import gudang from '../../pages/customer/Gudang/reduxReducer';
import transactions from '../../pages/customer/Transactions/reduxReducer';
import toast from './toast';
import gudangAdministratorReducer from '../../pages/administrator/Gudang/reduxReducer';
import categoryAdministratorReducer from '../../pages/administrator/Kategori/reduxReducer';
import storeAdministrator from '../../pages/administrator/Store/reduxReducer';
import packagingAdministratorReducer from '../../pages/administrator/Packaging/reduxReducer';
import systemSettingCustomer from '../../pages/customer/System Settings/reduxReducer';
import inboundWarehouse from '../../pages/warehouse/Inbound/reduxReducer';
import dispatchOrderWarehouse from '../../pages/warehouse/DispatchOrder/reduxReducer';
import cashlessWarehouse from '../../pages/warehouse/Cashless/reduxReducer';
import storeWarehouse from '../../pages/warehouse/Store/reduxReducer';
import manifestWarehouse from '../../pages/warehouse/Manifest/reduxReducer';
import outboundCustomer from '../../pages/customer/Outbound/reduxReducer';
import home from '../../pages/general/Home/reduxReducer';
import outboundWarehouse from '../../pages/warehouse/Outbound/reduxReducer';
import packagingWarehouseReducer from '../../pages/warehouse/Packaging/reduxReducer';
import report from '../../pages/warehouse/Report/reduxReducer';
import reportUser from '../../pages/customer/Report/reduxReducer';
import dashboardCustomer from '../../pages/customer/Dashboard/reduxReducer';
import errorReducer from './errorReducer';

//reducers
const reducer = combineReducers({
  login,
  prosidebar,
  loading,
  inbound,
  gudang,
  transactions,
  gudangAdministratorReducer,
  categoryAdministratorReducer,
  packagingAdministratorReducer,
  toast,
  storeAdministrator,
  systemSettingCustomer,
  inboundWarehouse,
  storeWarehouse,
  outboundCustomer,
  manifestWarehouse,
  home,
  outboundWarehouse,
  packagingWarehouseReducer,
  report,
  reportUser,
  dashboardCustomer,
  dispatchOrderWarehouse,
  cashlessWarehouse,
  errorReducer
});

export default reducer;
