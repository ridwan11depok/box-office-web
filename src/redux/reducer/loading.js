import actionType from '../constant';
const initialState = {
  isLoading: false,
  isLoadingTable: false,
};

const loadingReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.SET_LOADING:
      return {
        ...prevState,
        isLoading: action.payload,
      };
    case actionType.SET_LOADING_TABLE:
      return {
        ...prevState,
        isLoadingTable: action.payload,
      };
    default:
      return prevState;
  }
};

export default loadingReducer;
