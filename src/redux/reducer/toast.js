import actionType from '../constant';
const initialState = {
  isShow: false,
  message: '',
  type: '',
};

const toastReducer = (prevState = initialState, action) => {
  switch (action.type) {
    case actionType.SET_TOAST:
      return {
        ...prevState,
        isShow: action.payload.isShow,
        messages: action.payload?.messages || '',
        type: action.payload.type || '',
      };
    default:
      return prevState;
  }
};

export default toastReducer;
