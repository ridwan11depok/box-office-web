const Button = (props) => {
  const {
    text,
    onClick,
    color,
    width,
    height,
    textColor,
    buttonStyle,
    textStyle,
  } = props;
  return (
    <button
      type="button"
      style={{
        backgroundColor: color,
        height,
        width,
        borderRadius: '4px',
        borderWidth: 0,
        ...buttonStyle,
      }}
      onClick={onClick}
    >
      <text style={{ color: textColor, ...textStyle }}>{text}</text>
    </button>
  );
};

export default Button;
