import { useState } from 'react';
import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import iconHamburger from '../../../assets/icons/hamburger.svg';
// import { FaBars } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setSidebar } from './reduxAction';
// import './custom.scss'
// import 'react-pro-sidebar/dist/css/styles.css';

const ReactSidebar = () => {
  const [state, setState] = useState({
    toggled: false,
  });
  const { isCollapsed } = useSelector((state) => state.prosidebar);
  const dispatch = useDispatch();

  const DashboardIcon = () => {
    return (
      <div>
        <svg
          width="24"
          height="25"
          viewBox="0 0 24 25"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M21 21.3406H13V15.3406H21V21.3406ZM11 21.3406H3V11.3406H11V21.3406ZM21 13.3406H13V3.34058H21V13.3406ZM11 9.34058H3V3.34058H11V9.34058Z"
            fill="#EBEBFF"
          />
        </svg>
      </div>
    );
  };

  const TransactionIcon = () => {
    return (
      <div>
        <svg
          width="20"
          height="17"
          viewBox="0 0 20 17"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M7 16.3408L2 12.3408L7 8.34082V11.3408H20V13.3408H7V16.3408ZM13 8.34082V5.34082H0V3.34082H13V0.34082L18 4.34082L13 8.34082Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  const InventoryIcon = () => {
    return (
      <div>
        <svg
          width="20"
          height="19"
          viewBox="0 0 20 19"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect y="16.3408" width="20" height="2" fill="#FAFAFF" />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M8 10.3408H4V14.3408H8V10.3408ZM2 8.34082V16.3408H10V8.34082H2Z"
            fill="#FAFAFF"
          />
          <rect x="5" y="10.3408" width="2" height="1" fill="#FAFAFF" />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M16 10.3408H12V14.3408H16V10.3408ZM10 8.34082V16.3408H18V8.34082H10Z"
            fill="#FAFAFF"
          />
          <rect x="13" y="10.3408" width="2" height="1" fill="#FAFAFF" />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M12 2.34082H8V6.34082H12V2.34082ZM6 0.34082V8.34082H14V0.34082H6Z"
            fill="#FAFAFF"
          />
          <rect x="9" y="2.34082" width="2" height="1" fill="#FAFAFF" />
        </svg>
      </div>
    );
  };

  const SystemSettingIcon = () => {
    return (
      <div>
        <svg
          width="24"
          height="25"
          viewBox="0 0 24 25"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M13.8199 22.3406H10.1799C9.71003 22.3406 9.30347 22.0136 9.20292 21.5546L8.79592 19.6706C8.25297 19.4327 7.73814 19.1352 7.26092 18.7836L5.42392 19.3686C4.97592 19.5114 4.4889 19.3229 4.25392 18.9156L2.42992 15.7646C2.19751 15.3571 2.27758 14.8431 2.62292 14.5256L4.04792 13.2256C3.98312 12.6367 3.98312 12.0425 4.04792 11.4536L2.62292 10.1566C2.27707 9.83894 2.19697 9.3243 2.42992 8.91658L4.24992 5.76358C4.48491 5.35627 4.97192 5.16771 5.41992 5.31058L7.25692 5.89558C7.50098 5.71473 7.75505 5.5478 8.01792 5.39558C8.27026 5.25327 8.52995 5.12442 8.79592 5.00958L9.20392 3.12758C9.30399 2.66854 9.71011 2.34107 10.1799 2.34058H13.8199C14.2897 2.34107 14.6958 2.66854 14.7959 3.12758L15.2079 5.01058C15.4887 5.13409 15.7622 5.27366 16.0269 5.42858C16.2739 5.57139 16.5126 5.72797 16.7419 5.89758L18.5799 5.31258C19.0276 5.17025 19.514 5.35874 19.7489 5.76558L21.5689 8.91858C21.8013 9.32606 21.7213 9.84009 21.3759 10.1576L19.9509 11.4576C20.0157 12.0465 20.0157 12.6407 19.9509 13.2296L21.3759 14.5296C21.7213 14.8471 21.8013 15.3611 21.5689 15.7686L19.7489 18.9216C19.514 19.3284 19.0276 19.5169 18.5799 19.3746L16.7419 18.7896C16.5093 18.9609 16.2677 19.1194 16.0179 19.2646C15.7557 19.4165 15.4853 19.5537 15.2079 19.6756L14.7959 21.5546C14.6954 22.0132 14.2894 22.3402 13.8199 22.3406ZM7.61992 16.5696L8.43992 17.1696C8.62477 17.3057 8.81743 17.4309 9.01692 17.5446C9.20462 17.6533 9.39788 17.7521 9.59592 17.8406L10.5289 18.2496L10.9859 20.3406H13.0159L13.4729 18.2486L14.4059 17.8396C14.8132 17.66 15.1998 17.4367 15.5589 17.1736L16.3799 16.5736L18.4209 17.2236L19.4359 15.4656L17.8529 14.0226L17.9649 13.0106C18.0141 12.5679 18.0141 12.1212 17.9649 11.6786L17.8529 10.6666L19.4369 9.22058L18.4209 7.46158L16.3799 8.11158L15.5589 7.51158C15.1997 7.24729 14.8132 7.02233 14.4059 6.84058L13.4729 6.43158L13.0159 4.34058H10.9859L10.5269 6.43258L9.59592 6.84058C9.39772 6.92762 9.20444 7.02543 9.01692 7.13358C8.81866 7.24691 8.62701 7.37144 8.44292 7.50658L7.62192 8.10658L5.58192 7.45658L4.56492 9.22058L6.14792 10.6616L6.03592 11.6746C5.98672 12.1172 5.98672 12.5639 6.03592 13.0066L6.14792 14.0186L4.56492 15.4616L5.57992 17.2196L7.61992 16.5696ZM11.9959 16.3406C9.78678 16.3406 7.99592 14.5497 7.99592 12.3406C7.99592 10.1314 9.78678 8.34058 11.9959 8.34058C14.2051 8.34058 15.9959 10.1314 15.9959 12.3406C15.9932 14.5486 14.2039 16.3378 11.9959 16.3406ZM11.9959 10.3406C10.9033 10.3417 10.0138 11.2194 9.99815 12.3119C9.98249 13.4044 10.8465 14.3073 11.9386 14.3397C13.0307 14.3721 13.9468 13.5221 13.9959 12.4306V12.8306V12.3406C13.9959 11.236 13.1005 10.3406 11.9959 10.3406Z"
            fill="#EBEBFF"
          />
        </svg>
      </div>
    );
  };

  const StocksIcon = () => {
    return (
      <div>
        <svg
          width="20"
          height="21"
          viewBox="0 0 20 21"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M2 2.34082H18V5.34082H2V2.34082ZM1 7.34082H0V0.34082H20V7.34082H19V20.3408H1V7.34082ZM3 7.34082V18.3408H17V7.34082H3Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  const WarehouseIcon = () => {
    return (
      <div>
        <svg
          width="20"
          height="20"
          viewBox="0 0 20 20"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M12 10H8V14H12V10ZM6 8V16H14V8H6Z"
            fill="#FAFAFF"
          />
          <path d="M9 8H11V12H9V8Z" fill="#FAFAFF" />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M18 7.13238L10 2.33238L2 7.13238V18H18V7.13238ZM10 0L20 6V20H0V6L10 0Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  const HelpIcon = () => {
    return (
      <div>
        <svg
          width="24"
          height="25"
          viewBox="0 0 24 25"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M12 22.3399C6.47967 22.3338 2.00606 17.8602 2 12.3399V12.1399C2.10993 6.64439 6.63459 2.26782 12.1307 2.34074C17.6268 2.41367 22.0337 6.90875 21.9978 12.4052C21.9619 17.9017 17.4966 22.3388 12 22.3399ZM11.984 20.3399H12C16.4167 20.3354 19.9942 16.7526 19.992 12.3359C19.9898 7.91915 16.4087 4.33987 11.992 4.33987C7.57528 4.33987 3.99421 7.91915 3.992 12.3359C3.98979 16.7526 7.56729 20.3354 11.984 20.3399ZM13 18.3399H11V16.3399H13V18.3399ZM13 15.3399H11C10.9684 14.0375 11.6461 12.8207 12.77 12.1619C13.43 11.6559 14 11.2199 14 10.3399C14 9.23529 13.1046 8.33986 12 8.33986C10.8954 8.33986 10 9.23529 10 10.3399H8V10.2499C8.01608 8.8208 8.79333 7.50887 10.039 6.80826C11.2846 6.10765 12.8094 6.12481 14.039 6.85326C15.2685 7.58171 16.0161 8.9108 16 10.3399C15.9284 11.4189 15.3497 12.4001 14.44 12.9849C13.6177 13.5011 13.0847 14.3727 13 15.3399Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  const ChangeWarehouseIcon = () => {
    return (
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M20 9.13238L12 4.33238L4 9.13238V14H2V8L12 2L22 8V9.13238H20Z"
          fill="#FAFAFF"
        />
        <path d="M9 20H20V17.0078H22V22H9V20Z" fill="#FAFAFF" />
        <path
          d="M18.1695 14L16.5895 15.59L17.9995 17L21.9995 13L17.9995 9L16.5895 10.41L18.1695 12H12.0068V14H18.1695Z"
          fill="#FAFAFF"
        />
        <path
          d="M5.82958 19L7.40958 20.59L5.99958 22L1.99958 18L5.99958 14L7.40958 15.41L5.82958 17H12.0068V19H5.82958Z"
          fill="#FAFAFF"
        />
      </svg>
    );
  };

  const ReportIcon = () => {
    return (
      <div>
        <svg
          width="24"
          height="25"
          viewBox="0 0 24 25"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M18 22.3398H6C4.89543 22.3398 4 21.4444 4 20.3398V4.33984C4 3.23527 4.89543 2.33984 6 2.33984H13C13.0109 2.34032 13.0217 2.34234 13.032 2.34584C13.0418 2.34886 13.0518 2.35087 13.062 2.35184C13.1502 2.3575 13.2373 2.37464 13.321 2.40284L13.349 2.41184C13.3717 2.41953 13.3937 2.42888 13.415 2.43984C13.5239 2.48827 13.6232 2.55603 13.708 2.63984L19.708 8.63984C19.7918 8.72463 19.8596 8.8239 19.908 8.93284C19.918 8.95484 19.925 8.97784 19.933 9.00084L19.942 9.02684C19.9699 9.11023 19.9864 9.19702 19.991 9.28484C19.9926 9.29403 19.9949 9.30306 19.998 9.31184C19.9998 9.32107 20.0004 9.33046 20.0001 9.33984V20.3398C20.0001 21.4444 19.1046 22.3398 18 22.3398ZM6 4.33984V20.3398H18V10.3398H13C12.4477 10.3398 12 9.89213 12 9.33984V4.33984H6ZM14 5.75384V8.33984H16.586L14 5.75384Z"
            fill="#FAFAFF"
          />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M16.6727 13.0795L10.4672 18.7209L7.29297 15.5467L8.70718 14.1324L10.5329 15.9582L15.3274 11.5996L16.6727 13.0795Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  const ShippingIcon = () => {
    return (
      <div>
        <svg
          width="24"
          height="15"
          viewBox="0 0 24 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M13 2H2V11H2.17071C2.58254 9.83481 3.69378 9 5 9C6.30622 9 7.41746 9.83481 7.82929 11H13V2ZM15 2V0H0V13H2.17071C2.58254 14.1652 3.69378 15 5 15C6.30622 15 7.41746 14.1652 7.82929 13H13H15H16.1707C16.5825 14.1652 17.6938 15 19 15C20.3062 15 21.4175 14.1652 21.8293 13H24V7.5L21 2H15ZM15 4V11H16.1707C16.5825 9.83481 17.6938 9 19 9C20.3062 9 21.4175 9.83481 21.8293 11H22V8.00999L21.4491 7H18V5H20.3582L19.8127 4H15ZM6 12C6 12.5523 5.55228 13 5 13C4.44772 13 4 12.5523 4 12C4 11.4477 4.44772 11 5 11C5.55228 11 6 11.4477 6 12ZM20 12C20 12.5523 19.5523 13 19 13C18.4477 13 18 12.5523 18 12C18 11.4477 18.4477 11 19 11C19.5523 11 20 11.4477 20 12ZM6 5H4V7H6V5Z"
            fill="#FAFAFF"
          />
        </svg>
      </div>
    );
  };

  return (
    <div>
      <ProSidebar
        collapsed={isCollapsed}
        toggled={state.toggled}
        // onToggle={() => { setState({ ...state, toggled: !state.toggled }) }}
        breakPoint="xl"
        width={'250px'}
        color="#192A55"
        style={{ position: 'absolute', height: '100%' }}
      >
        <div
          style={{
            height: '50px',
            justifyContent: isCollapsed ? 'center' : 'space-between',
            alignContent: 'center',
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
          }}
        >
          {!isCollapsed && <img src={logoboxoffice} />}
          <img
            style={{ cursor: 'pointer' }}
            onClick={(e) => {
              e.preventDefault();
              dispatch(setSidebar(!isCollapsed));
            }}
            src={iconHamburger}
          />
        </div>
        <Menu iconShape="square">
          {/* <SubMenu icon={<DashboardIcon />} title="Dashboard"> */}
          <MenuItem icon={<DashboardIcon />}>
            Dashboard
            <Link to="/dashboard" />
          </MenuItem>
          <MenuItem icon={<TransactionIcon />}>
            Transaksi
            <Link to="/transactions" />
          </MenuItem>
          <SubMenu title="Inventory" icon={<InventoryIcon />}>
            <MenuItem>
              Stocks
              <Link to="/stock" />
            </MenuItem>
            <MenuItem>
              Inbound
              <Link to="/inbound" />
            </MenuItem>
            <MenuItem>
              Request Print Barcode
              <Link to="/req-print-barcode" />
            </MenuItem>
            <MenuItem>
              Mutasi Stock
              <Link to="/mutasistock" />
            </MenuItem>
            <MenuItem>
              QC Middle
              <Link to="/qcm-empty" />
            </MenuItem>
            <MenuItem>Stock Opname</MenuItem>
          </SubMenu>
          <MenuItem icon={<WarehouseIcon />}>
            Gudang
            <Link to="/dashboard" />
          </MenuItem>
          <MenuItem icon={<ChangeWarehouseIcon />}>
            Pindah Gudang
            <Link to="/mutasistock/pilih-gudang" />
          </MenuItem>
          <SubMenu title="System Settings" icon={<SystemSettingIcon />}>
            <MenuItem>
              Akun
              <Link to="/system-settings/akun" />
            </MenuItem>
            <MenuItem>
              eCommerce
              <Link to="/system-settings/ecommerce" />
            </MenuItem>
            <MenuItem>Marketplace</MenuItem>
          </SubMenu>
          <SubMenu title="Laporan" icon={<ReportIcon />}>
            <MenuItem>Cari Transaksi</MenuItem>
            <MenuItem>History Reimburse</MenuItem>
            <MenuItem>Logistic</MenuItem>
          </SubMenu>
          <MenuItem
            icon={<HelpIcon />}
            style={{ borderTop: 2, borderTopColor: 'white' }}
          >
            Help
          </MenuItem>
        </Menu>
      </ProSidebar>
    </div>
  );
};

export default ReactSidebar;
