import action from './reduxConstant';
export const setSidebar = (condition) => {
  return {
    type: action.setSidebar,
    payload: condition,
  };
};
