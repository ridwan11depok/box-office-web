import actions from './reduxConstant';

const initialState = {
  isCollapsed: false,
};

const prosidebarReducer = (state = initialState, action) => {
  if (action.type === actions.setSidebar) {
    return { ...state, isCollapsed: action.payload };
  }
  return initialState;
};

export default prosidebarReducer;
