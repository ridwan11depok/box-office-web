import React, { Component } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logout } from '../../../pages/general/Login/reduxAction';

const UserDropdown = (props) => {
  const history = useHistory();
  const { userDetail } = props;
  const dispatch = useDispatch();
  return (
    <div className="dropdown">
      {/* <a
          href="#"
          data-toggle="dropdown"
          className="nav-link dropdown-toggle nav-link-lg nav-link-user"
        > */}
      <a
        href="#"
        data-toggle="dropdown"
        className="nav-link nav-link-lg nav-link-user"
      >
        <img
          alt="image"
          src={userDetail.userImg}
          className="rounded-circle mr-1"
        />
        <div
          className="d-sm-none d-lg-inline-block "
          style={{
            fontSize: '14px',
            fontWeight: '700',
            fontFamily: 'Open Sans',
            color: '#FAFAFF',
          }}
        >
          {userDetail.logoutTitle}
        </div>
      </a>
      <div className="dropdown-menu dropdown-menu-right">
        <div className="dropdown-title">Logged in {userDetail.logTime} ago</div>

        {userDetail.datas.map((data, idata) => {
          return (
            <NavLink
              key={idata}
              to={data.link}
              activeStyle={{
                color: '#6777ef',
              }}
              exact
              className="dropdown-item has-icon"
            >
              <i className={data.icode} /> {data.title}
            </NavLink>
          );
        })}

        <div className="dropdown-divider" />
        <a
          href="#"
          className="dropdown-item has-icon text-danger"
          onClick={(e) => {
            e.preventDefault();
            // localStorage.removeItem('role');
            // localStorage.removeItem('access-token-jwt');
            dispatch(logout());
            history.replace('/login');
          }}
        >
          <i className={userDetail.logoutIcon} /> {userDetail.logoutTitle}
        </a>
      </div>
    </div>
  );
};

export default UserDropdown;
