import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import { useHistory } from 'react-router-dom';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: '#192A55',
    boxShadow: 'none',
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
    zIndex: theme.zIndex.drawer + 1,
    overflow: 'hidden',
  },
  menuItemText: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    marginLeft: '-20px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuItemTextClose: {
    display: 'none',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  title: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginBottom: 0,
    borderBottom: '1px solid #E8E8E8',
    textAlign: 'center',
    height: '55px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  listItem: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    '&:hover': {
      backgroundColor: 'rgba(28, 28, 28,0.5)',
    },
  },
  activeMenu: {
    backgroundColor: 'rgba(28, 28, 28,0.5)',
  },
}));

const PopperMenu = ({ anchorEl, open, menus, onClick, activeMenu }) => {
  const classes = useStyles();
  const history = useHistory();

  const renderList = () => (
    <List
      component="nav"
      aria-label="submenu"
      subheader={
        <ListSubheader
          component="div"
          id="nested-list-subheader"
          className={classes.title}
        >
          {menus.title}
        </ListSubheader>
      }
    >
      {menus.subMenu.map((menu) => {
        return (
          <ListItem
            button
            className={clsx(classes.listItem, {
              [classes.activeMenu]: Boolean(activeMenu === menu.route),
            })}
            onClick={() => {
              onClick();
              history.push(menu.route);
            }}
          >
            {
              menu?.icon !== '' &&
              <ListItemIcon
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                {menu.icon()}
              </ListItemIcon>
            }
            <ListItemText
              primary={menu.title}
              className={clsx({
                [classes.menuItemText]: open,
              })}
            />
          </ListItem>
        )
      })}
    </List>
  );
  return (
    <Popper anchorEl={anchorEl} open={open} placement="right-start">
      <Paper className={classes.root}>{renderList()}</Paper>
    </Popper>
  );
};

export default PopperMenu;
