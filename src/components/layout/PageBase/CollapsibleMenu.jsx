import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import { useHistory } from 'react-router-dom';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import PopperMenu from './PopperMenu';

const useStyles = makeStyles((theme) => ({
  menuItemText: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    // marginLeft: '-20px',
  },
  menuItemTextClose: {
    // display: 'none',
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  arrowIconOpen: {
    color: '#FAFAFF',
    marginRight: '10px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  arrowIconClose: {
    display: 'none',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  listItem: {
    paddingRight: 0,
    display: 'flex',
    flexDirection: 'row',
    height: '55px',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: 'rgba(28, 28, 28,0.5)',
    },
  },
  listItemOpen: {
    justifyContent: 'start',
    paddingLeft: '24px',
  },
  listItemClose: {
    // paddingLeft: 0,
    justifyContent: 'start',
    paddingLeft: '17px',
    [theme.breakpoints.up('sm')]: {
      paddingLeft: '24px',
    },
  },
  activeMenu: {
    backgroundColor: 'rgba(28, 28, 28,0.5)',
  },
  activeSubMenu: {
    backgroundColor: 'rgba(28, 28, 28, 1)',
  },
  list: {
    backgroundColor: 'rgba(28, 28, 28,0.5)',
  },
}));

const checkRoute = (path, routes = []) => {
  const match = routes.filter((item) => item.route === path);
  if (match.length > 0) {
    return true;
  } else {
    return false;
  }
};

export default function CollapsibleMenu(props) {
  const { open, menus, activeMenu } = props;
  const classes = useStyles();
  const [openSubMenu, setOpenSubMenu] = React.useState(false);
  const history = useHistory();
  const handleClick = () => {
    open && setOpenSubMenu(!openSubMenu);
  };
  useEffect(() => {
    if (!open) {
      setOpenSubMenu(false);
    }
  }, [open]);
  const [state, setState] = useState({ anchorEl: null, openPopper: false });
  const handlePopperOpen = (event) => {
    setState({
      anchorEl: event.currentTarget,
      openPopper: true,
    });
  };

  const handlePopperClose = () => {
    setState({
      anchorEl: null,
      openPopper: false,
    });
  };


  return (
    <div
      onMouseEnter={!open ? handlePopperOpen : () => { }}
      onMouseLeave={!open ? handlePopperClose : () => { }}
    >
      <PopperMenu
        open={state.openPopper}
        anchorEl={state.anchorEl}
        menus={menus}
        onClick={handlePopperClose}
        activeMenu={activeMenu}
      />
      <ListItem
        button
        className={clsx(classes.listItem, {
          [classes.listItemOpen]: open,
          [classes.listItemClose]: !open,
          [classes.activeMenu]:
            !openSubMenu && checkRoute(activeMenu, menus.subMenu),
        })}
        onClick={handleClick}
      >
          <ListItemIcon
            style={{
              display: 'flex',
              flexDirection: 'row',
              width: 'min-content',
            }}
          >
            {menus?.icon()}
          </ListItemIcon>
        <ListItemText
          primary={menus.title}
          className={clsx({
            [classes.menuItemText]: open,
            [classes.menuItemTextClose]: !open,
          })}
        />
        {openSubMenu ? (
          <ArrowDropUpIcon
            className={clsx({
              [classes.arrowIconOpen]: open,
              [classes.arrowIconClose]: !open,
            })}
          />
        ) : (
          <ArrowDropDownIcon
            className={clsx({
              [classes.arrowIconOpen]: open,
              [classes.arrowIconClose]: !open,
            })}
          />
        )}
      </ListItem>
      <Collapse in={openSubMenu} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={classes.list}>
          {menus.subMenu.map((menu, idx) => (
            <ListItem
              key={idx.toString()}
              button
              className={clsx(classes.listItem, {
                [classes.listItemOpen]: open,
                [classes.listItemClose]: !open,
                [classes.activeSubMenu]: Boolean(activeMenu === menu.route),
              })}
              onClick={() => history.push(menu.route)}
            >
              {/* {menu?.icon &&
                <ListItemIcon
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: open ? 'start' : 'center',
                    width: 'min-content',
                  }}
                >
                  {menu?.icon()}
                </ListItemIcon>
              } */}
              <ListItemText
                primary={menu.title}
                className={clsx({
                  [classes.menuItemText]: open,
                  [classes.menuItemTextClose]: !open,
                })}
              />
            </ListItem>
          ))}
        </List>
      </Collapse>
    </div>
  );
}
