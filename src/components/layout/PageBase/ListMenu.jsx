import React, { useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useHistory } from 'react-router-dom';
import CollapsibleMenu from './CollapsibleMenu';
import BottomMenu from './BottomMenu';


const useStyles = makeStyles((theme) => ({
  menuItemText: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    // marginLeft: '-20px',
  },
  menuItemTextClose: {
    // display: 'none',
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    // marginLeft: '-20px',
  },
  listItem: {
    paddingRight: 0,
    display: 'flex',
    flexDirection: 'row',
    height: '55px',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: 'rgba(28, 28, 28,0.5)',
    },
  },
  listItemOpen: {
    justifyContent: 'start',
    paddingLeft: '24px',
  },
  listItemClose: {
    // paddingLeft: 0,
    justifyContent: 'start',
    paddingLeft: '17px',
    [theme.breakpoints.up('sm')]: {
      paddingLeft: '24px',
    },
  },
  activeMenu: {
    backgroundColor: 'rgba(28, 28, 28,0.5)',
  },
  list: {
    paddingTop: 0,
  },
}));

export default function ListMenu(props) {
  const history = useHistory();
  const classes = useStyles();
  const { menus, open, activeMenu } = props;
  useEffect(() => {
    console.log('menus', menus);
  }, [menus])
  return (
    <>
      <List className={classes.list}>
        {menus.topMenu.map((menu, index) => {
          if (menu.subMenu) {
            return (
              <CollapsibleMenu
                open={open}
                menus={menu}
                activeMenu={activeMenu}
              />
            );
          } else {
            return (
              <ListItem
                button
                key={index.toString()}
                className={clsx(classes.listItem, {
                  [classes.listItemOpen]: open,
                  [classes.listItemClose]: !open,
                  [classes.activeMenu]: Boolean(menu.route === activeMenu),
                })}
                onClick={() => history.push(menu.route)}
              >
                {menu?.icon() &&
                  <ListItemIcon
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      width: 'min-content',
                    }}
                  >
                    {menu?.icon()}
                  </ListItemIcon>
                }
                <ListItemText
                  primary={menu.title}
                  className={clsx({
                    [classes.menuItemText]: open,
                    [classes.menuItemTextClose]: !open,
                  })}
                />
              </ListItem>
            );
          }
        })}
      </List>
      {menus.bottomMenu && (
        <BottomMenu
          open={open}
          activeMenu={activeMenu}
          menus={menus.bottomMenu}
        />
      )}
    </>
  );
}
