import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import logoboxoffice from '../../../assets/images/logoBoxOffice.svg';
import { menus } from './dataMenu';
import ListMenu from './ListMenu';
import { NotifyData, userDetail } from '../../admin/header/Data';
import UserDropdown from './UserDropdown';
import ToggleData from './ToggleData';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { logout } from '../../../pages/general/Login/reduxAction';
import { useDispatch } from 'react-redux';

const drawerWidth = 255;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: '#192A55',
    boxShadow: 'none',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: '#192A55',
    boxShadow: 'none',
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    position: 'relative',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: '#192A55',
    boxShadow: 'none',
    paddingBottom: '50px',
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
    backgroundColor: '#192A55',
    boxShadow: 'none',
    paddingBottom: '50px',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    width: `calc(100% - ${drawerWidth}px)`,
  },
  contentClose: {
    flexGrow: 1,
    padding: theme.spacing(3),
    width: `calc(100% - ${theme.spacing(7) + 1}px)`,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${theme.spacing(9) + 1}px)`,
    },
  },
}));

export default function PageBase(props) {
  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory()
  const dispatch = useDispatch()
  const [open, setOpen] = React.useState(true);
  // const { role } = useSelector((state) => state.loginReducer);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar style={{ justifyContent: 'between' }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <div
            style={{
              marginLeft: 'auto',
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <ToggleData data={NotifyData} />
            <div className="nav-link nav-link-lg nav-link-user" style={{ cursor: 'pointer', marginLeft: 15 }}
              onClick={(e) => {
                e.preventDefault()
                console.log('heree');
                localStorage.removeItem('role')
                localStorage.removeItem('access-token-jwt')
                dispatch(logout())
                history.replace('/login')
              }}>
              <img
                alt="image"
                src={userDetail.userImg}
                className="rounded-circle mr-1"
              />
              <div className="d-sm-none d-lg-inline-block fw-bold">
                {/* Hi, {userDetail.userName} */}
                <span style={{ fontWeight: 'bold' }}>{userDetail.logoutTitle}</span>
              </div>
            </div>
            {/* <UserDropdown userDetail={userDetail} /> */}
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div
          className={classes.toolbar}
          style={{
            boxShadow: open && '0 2px 0 0 rgba(250, 250, 255, 0.2)',
          }}
        >
          <img src={logoboxoffice} alt="logo" />
          <IconButton onClick={handleDrawerClose}>
            <MenuIcon style={{ color: '#FFFFFF' }} />
          </IconButton>
        </div>
        <ListMenu
          menus={menus[props.role]}
          open={open}
          activeMenu={props.activeMenu}
        />
      </Drawer>
      <main
        className={clsx({
          [classes.content]: open,
          [classes.contentClose]: !open,
        })}
      >
        <div className={classes.toolbar} />
        {props.children}
      </main>
    </div>
  );
}
