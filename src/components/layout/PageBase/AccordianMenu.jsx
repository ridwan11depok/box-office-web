import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import { useHistory } from 'react-router-dom';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: '#192A55',
    boxShadow: 'none',
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
    zIndex: theme.zIndex.drawer + 1,
    overflow: 'hidden',
  },
  menuItemText: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    marginLeft: '-20px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuItemTextClose: {
    display: 'none',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
}));

const PopperMenu = ({ anchorEl, open, menus, onClick }) => {
  const classes = useStyles();
  const history = useHistory();
  const renderList = () => (
    <List component="nav" aria-label="main mailbox folders">
      {menus.map((menu) => (
        <ListItem
          button
          style={{
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
          }}
          onClick={() => {
            onClick();
            history.push(menu.route);
          }}
        >
          <ListItemIcon
            style={{
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            {menu?.icon()}
          </ListItemIcon>
          <ListItemText
            primary={menu.title}
            className={clsx({
              [classes.menuItemText]: open,
              // [classes.menuItemTextClose]: !open,
            })}
          />
        </ListItem>
      ))}
    </List>
  );
  return (
    <Popper anchorEl={anchorEl} open={open} placement="right">
      <Paper className={classes.root}>{renderList()}</Paper>
    </Popper>
  );
};

export default function SimpleAccordion(props) {
  const [state, setState] = useState({ anchorEl: null, openPopper: false });
  const { open, menus } = props;
  const classes = useStyles();
  const history = useHistory();

  const handlePopperOpen = (event) => {
    setState({
      anchorEl: event.currentTarget,
      openPopper: true,
    });
  };

  const handlePopperClose = () => {
    setTimeout(() => {
      setState({
        anchorEl: null,
        openPopper: false,
      });
    }, 100);
  };

  return (
    <div
      className={classes.root}
      onMouseEnter={!open ? handlePopperOpen : () => {}}
      onMouseLeave={!open ? handlePopperClose : () => {}}
    >
      {!open && (
        <PopperMenu
          open={state.openPopper}
          anchorEl={state.anchorEl}
          menus={menus.subMenu}
          onClick={handlePopperClose}
        />
      )}
      <ListItem
        button
        style={{
          paddingLeft: !open && 0,
          paddingRight: 0,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: open ? 'start' : 'center',
          width: '100%',
          height: '72px',
          display: open && 'none',
        }}
      >
        <ListItemIcon
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: open ? 'start' : 'center',
          }}
        >
          {menus.icon()}
        </ListItemIcon>
        <ListItemText
          primary={menus.title}
          className={clsx({
            [classes.menuItemText]: open,
            [classes.menuItemTextClose]: !open,
          })}
        />
      </ListItem>
      <Accordion
        TransitionProps={{ unmountOnExit: true }}
        style={{
          backgroundColor: '#192A55',
          boxShadow: 'none',
          margin: 0,
          paddingRight: '5px',
          display: !open && 'none',
        }}
      >
        <AccordionSummary
          style={{
            padding: 0,
            minHeight: 'min-content',
            margin: 0,
            height: 'min-content',
          }}
          expandIcon={
            open && (
              <ArrowDropDownIcon
                style={{ color: '#FAFAFF', padding: 0, margin: 0 }}
              />
            )
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <ListItem
            button
            style={{
              paddingLeft: !open && 0,
              paddingRight: 0,
              display: 'flex',
              flexDirection: 'row',
              justifyContent: open ? 'start' : 'center',
              width: '100%',
            }}
          >
            <ListItemIcon
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: open ? 'start' : 'center',
                width: 'min-content',
              }}
            >
              {menus.icon()}
            </ListItemIcon>
            <ListItemText
              primary={menus.title}
              className={clsx({
                [classes.menuItemText]: open,
                [classes.menuItemTextClose]: !open,
              })}
            />
          </ListItem>
        </AccordionSummary>
        <AccordionDetails
          style={{
            display: 'flex',
            flexDirection: 'column',
            display: !open && 'none',
            marginTop: 0,
          }}
        >
          {menus.subMenu.map((menu, idx) => (
            <ListItem button onClick={() => history.push(menu.route)}>
              <ListItemIcon>{menu?.icon()}</ListItemIcon>
              <ListItemText
                primary={menu.title}
                className={classes.menuItemText}
              />
            </ListItem>
          ))}
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
