import React, { Component } from 'react';
import ToggleContent from './ToggleContent';
import iconImg from '../../../assets/icons/notif.svg';

export class ToggleData extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="dropdown dropdown-list-toggle">
        {/* <a
          href="#"
          data-toggle="dropdown"
          className={`nav-link nav-link-lg ${data.toggleName} beep`}
        > */}
        <a
          href="#"
          data-toggle="dropdown"
          className={`nav-link nav-link-lg ${data.toggleName}`}
        >
          {/* <i className={data.iconName} /> */}
          <img src={iconImg} alt="iconNotif" />
        </a>
        <div className="dropdown-menu dropdown-list dropdown-menu-right">
          <div className="dropdown-header">
            {data.headerLeft}
            <div className="float-right">
              <a href="#">{data.headerRight}</a>
            </div>
          </div>
          <ToggleContent data={data} />
          <div className="dropdown-footer text-center">
            <a href="#">
              {data.bottomMsg}
              <i className={data.bottomMsgCls} />
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default ToggleData;
