import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useHistory } from 'react-router-dom';

const drawerWidth = 255;

const useStyles = makeStyles((theme) => ({
  menuItemText: {
    color: '#FAFAFF',
    fontWeight: 'Open Sans',
    fontSize: '14px',
    fontWeight: '700',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: '-20px',
  },
  menuItemTextClose: {
    display: 'none',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  list: {
    position: 'fixed',
    bottom: 0,
    paddingBottom: 0,
    paddingTop: 0,
    borderTop: '1px solid #E8E8E8',
  },
  listItem: {
    paddingRight: 0,
    display: 'flex',
    flexDirection: 'row',
    height: '55px',
    alignItems: 'center',
    backgroundColor: '#192A55',
    overflowX: 'hidden',
    '&:hover': {
      backgroundColor: 'rgba(28, 28, 28,0.5)',
    },
  },
  listItemOpen: {
    justifyContent: 'start',
    width: `calc(${drawerWidth}px)`,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  listItemClose: {
    justifyContent: 'center',
    paddingLeft: 0,
    width: `calc(${theme.spacing(7) + 1}px)`,
    [theme.breakpoints.up('sm')]: {
      width: `calc(${theme.spacing(9) + 1}px)`,
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  activeMenu: {
    backgroundColor: 'rgba(28, 28, 28,0.5)',
  },
}));

export default function BottomMenu({ menus, activeMenu, open }) {
  const classes = useStyles();
  const history = useHistory();
  return (
    <List className={classes.list}>
      {menus.map((menu, idx) => (
        <ListItem
          key={idx.toString()}
          button
          className={clsx(classes.listItem, {
            [classes.listItemOpen]: open,
            [classes.listItemClose]: !open,
            [classes.activeMenu]: Boolean(menu.route === activeMenu),
          })}
          onClick={() => history.push(menu.route)}
        >
          <ListItemIcon
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: open ? 'start' : 'center',
              width: 'min-content',
            }}
          >
            {menu?.icon()}
          </ListItemIcon>
          <ListItemText
            primary={menu.title}
            className={clsx({
              [classes.menuItemText]: open,
              [classes.menuItemTextClose]: !open,
            })}
          />
        </ListItem>
      ))}
    </List>
  );
}
