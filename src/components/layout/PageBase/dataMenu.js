import React from 'react'
import inbound from '../../../assets/icons/menus/inbound.svg';
import outbound from '../../../assets/icons/menus/outbound.svg';
import * as Icons from '../../elements/Icons';

const DashboardIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M21 21.3406H13V15.3406H21V21.3406ZM11 21.3406H3V11.3406H11V21.3406ZM21 13.3406H13V3.34058H21V13.3406ZM11 9.34058H3V3.34058H11V9.34058Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const TransactionIcon = () => {
  return (
    <div>
      <svg
        width="20"
        height="17"
        viewBox="0 0 20 17"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M7 16.3408L2 12.3408L7 8.34082V11.3408H20V13.3408H7V16.3408ZM13 8.34082V5.34082H0V3.34082H13V0.34082L18 4.34082L13 8.34082Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};
const InventoryIcon = () => {
  return (
    <div>
      <svg
        width="20"
        height="19"
        viewBox="0 0 20 19"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect y="16.3408" width="20" height="2" fill="#FAFAFF" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M8 10.3408H4V14.3408H8V10.3408ZM2 8.34082V16.3408H10V8.34082H2Z"
          fill="#FAFAFF"
        />
        <rect x="5" y="10.3408" width="2" height="1" fill="#FAFAFF" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M16 10.3408H12V14.3408H16V10.3408ZM10 8.34082V16.3408H18V8.34082H10Z"
          fill="#FAFAFF"
        />
        <rect x="13" y="10.3408" width="2" height="1" fill="#FAFAFF" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M12 2.34082H8V6.34082H12V2.34082ZM6 0.34082V8.34082H14V0.34082H6Z"
          fill="#FAFAFF"
        />
        <rect x="9" y="2.34082" width="2" height="1" fill="#FAFAFF" />
      </svg>
    </div>
  );
};

const WarehouseIcon = () => {
  return (
    <div>
      <svg
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M12 10H8V14H12V10ZM6 8V16H14V8H6Z"
          fill="#FAFAFF"
        />
        <path d="M9 8H11V12H9V8Z" fill="#FAFAFF" />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M18 7.13238L10 2.33238L2 7.13238V18H18V7.13238ZM10 0L20 6V20H0V6L10 0Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const ChangeWarehouseIcon = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M20 9.13238L12 4.33238L4 9.13238V14H2V8L12 2L22 8V9.13238H20Z"
        fill="#FAFAFF"
      />
      <path d="M9 20H20V17.0078H22V22H9V20Z" fill="#FAFAFF" />
      <path
        d="M18.1695 14L16.5895 15.59L17.9995 17L21.9995 13L17.9995 9L16.5895 10.41L18.1695 12H12.0068V14H18.1695Z"
        fill="#FAFAFF"
      />
      <path
        d="M5.82958 19L7.40958 20.59L5.99958 22L1.99958 18L5.99958 14L7.40958 15.41L5.82958 17H12.0068V19H5.82958Z"
        fill="#FAFAFF"
      />
    </svg>
  );
};

function PackageIcon(props) {
  return (
    <svg
      width={20}
      height={21}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10 .946L20 4.28V16.453l-10 3.61-10-3.61V4.28L10 .946zm0 2.108l7.391 2.464-1.771.64-7.283-2.55L10 3.055zM5.236 4.642l-2.627.876L10 8.188l2.64-.954-7.404-2.592zM14 8.87l-3 1.083v7.624l7-2.528V7.425l-2 .722V10l-2 1V8.87zM2 14.5V7.425l7 2.527v7.624l-7-2.528V14.5z"
        fill="#FAFAFF"
      />
    </svg>
  );
}

const SystemSettingIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M13.8199 22.3406H10.1799C9.71003 22.3406 9.30347 22.0136 9.20292 21.5546L8.79592 19.6706C8.25297 19.4327 7.73814 19.1352 7.26092 18.7836L5.42392 19.3686C4.97592 19.5114 4.4889 19.3229 4.25392 18.9156L2.42992 15.7646C2.19751 15.3571 2.27758 14.8431 2.62292 14.5256L4.04792 13.2256C3.98312 12.6367 3.98312 12.0425 4.04792 11.4536L2.62292 10.1566C2.27707 9.83894 2.19697 9.3243 2.42992 8.91658L4.24992 5.76358C4.48491 5.35627 4.97192 5.16771 5.41992 5.31058L7.25692 5.89558C7.50098 5.71473 7.75505 5.5478 8.01792 5.39558C8.27026 5.25327 8.52995 5.12442 8.79592 5.00958L9.20392 3.12758C9.30399 2.66854 9.71011 2.34107 10.1799 2.34058H13.8199C14.2897 2.34107 14.6958 2.66854 14.7959 3.12758L15.2079 5.01058C15.4887 5.13409 15.7622 5.27366 16.0269 5.42858C16.2739 5.57139 16.5126 5.72797 16.7419 5.89758L18.5799 5.31258C19.0276 5.17025 19.514 5.35874 19.7489 5.76558L21.5689 8.91858C21.8013 9.32606 21.7213 9.84009 21.3759 10.1576L19.9509 11.4576C20.0157 12.0465 20.0157 12.6407 19.9509 13.2296L21.3759 14.5296C21.7213 14.8471 21.8013 15.3611 21.5689 15.7686L19.7489 18.9216C19.514 19.3284 19.0276 19.5169 18.5799 19.3746L16.7419 18.7896C16.5093 18.9609 16.2677 19.1194 16.0179 19.2646C15.7557 19.4165 15.4853 19.5537 15.2079 19.6756L14.7959 21.5546C14.6954 22.0132 14.2894 22.3402 13.8199 22.3406ZM7.61992 16.5696L8.43992 17.1696C8.62477 17.3057 8.81743 17.4309 9.01692 17.5446C9.20462 17.6533 9.39788 17.7521 9.59592 17.8406L10.5289 18.2496L10.9859 20.3406H13.0159L13.4729 18.2486L14.4059 17.8396C14.8132 17.66 15.1998 17.4367 15.5589 17.1736L16.3799 16.5736L18.4209 17.2236L19.4359 15.4656L17.8529 14.0226L17.9649 13.0106C18.0141 12.5679 18.0141 12.1212 17.9649 11.6786L17.8529 10.6666L19.4369 9.22058L18.4209 7.46158L16.3799 8.11158L15.5589 7.51158C15.1997 7.24729 14.8132 7.02233 14.4059 6.84058L13.4729 6.43158L13.0159 4.34058H10.9859L10.5269 6.43258L9.59592 6.84058C9.39772 6.92762 9.20444 7.02543 9.01692 7.13358C8.81866 7.24691 8.62701 7.37144 8.44292 7.50658L7.62192 8.10658L5.58192 7.45658L4.56492 9.22058L6.14792 10.6616L6.03592 11.6746C5.98672 12.1172 5.98672 12.5639 6.03592 13.0066L6.14792 14.0186L4.56492 15.4616L5.57992 17.2196L7.61992 16.5696ZM11.9959 16.3406C9.78678 16.3406 7.99592 14.5497 7.99592 12.3406C7.99592 10.1314 9.78678 8.34058 11.9959 8.34058C14.2051 8.34058 15.9959 10.1314 15.9959 12.3406C15.9932 14.5486 14.2039 16.3378 11.9959 16.3406ZM11.9959 10.3406C10.9033 10.3417 10.0138 11.2194 9.99815 12.3119C9.98249 13.4044 10.8465 14.3073 11.9386 14.3397C13.0307 14.3721 13.9468 13.5221 13.9959 12.4306V12.8306V12.3406C13.9959 11.236 13.1005 10.3406 11.9959 10.3406Z"
          fill="#EBEBFF"
        />
      </svg>
    </div>
  );
};

const ReportIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M18 22.3398H6C4.89543 22.3398 4 21.4444 4 20.3398V4.33984C4 3.23527 4.89543 2.33984 6 2.33984H13C13.0109 2.34032 13.0217 2.34234 13.032 2.34584C13.0418 2.34886 13.0518 2.35087 13.062 2.35184C13.1502 2.3575 13.2373 2.37464 13.321 2.40284L13.349 2.41184C13.3717 2.41953 13.3937 2.42888 13.415 2.43984C13.5239 2.48827 13.6232 2.55603 13.708 2.63984L19.708 8.63984C19.7918 8.72463 19.8596 8.8239 19.908 8.93284C19.918 8.95484 19.925 8.97784 19.933 9.00084L19.942 9.02684C19.9699 9.11023 19.9864 9.19702 19.991 9.28484C19.9926 9.29403 19.9949 9.30306 19.998 9.31184C19.9998 9.32107 20.0004 9.33046 20.0001 9.33984V20.3398C20.0001 21.4444 19.1046 22.3398 18 22.3398ZM6 4.33984V20.3398H18V10.3398H13C12.4477 10.3398 12 9.89213 12 9.33984V4.33984H6ZM14 5.75384V8.33984H16.586L14 5.75384Z"
          fill="#FAFAFF"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M16.6727 13.0795L10.4672 18.7209L7.29297 15.5467L8.70718 14.1324L10.5329 15.9582L15.3274 11.5996L16.6727 13.0795Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const HelpIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M12 22.3399C6.47967 22.3338 2.00606 17.8602 2 12.3399V12.1399C2.10993 6.64439 6.63459 2.26782 12.1307 2.34074C17.6268 2.41367 22.0337 6.90875 21.9978 12.4052C21.9619 17.9017 17.4966 22.3388 12 22.3399ZM11.984 20.3399H12C16.4167 20.3354 19.9942 16.7526 19.992 12.3359C19.9898 7.91915 16.4087 4.33987 11.992 4.33987C7.57528 4.33987 3.99421 7.91915 3.992 12.3359C3.98979 16.7526 7.56729 20.3354 11.984 20.3399ZM13 18.3399H11V16.3399H13V18.3399ZM13 15.3399H11C10.9684 14.0375 11.6461 12.8207 12.77 12.1619C13.43 11.6559 14 11.2199 14 10.3399C14 9.23529 13.1046 8.33986 12 8.33986C10.8954 8.33986 10 9.23529 10 10.3399H8V10.2499C8.01608 8.8208 8.79333 7.50887 10.039 6.80826C11.2846 6.10765 12.8094 6.12481 14.039 6.85326C15.2685 7.58171 16.0161 8.9108 16 10.3399C15.9284 11.4189 15.3497 12.4001 14.44 12.9849C13.6177 13.5011 13.0847 14.3727 13 15.3399Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const StockIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M9 13.3398H15V15.3398H9V13.3398Z" fill="#FAFAFF" />
        <path
          d="M13 11.3398L13 17.3398L11 17.3398L11 11.3398L13 11.3398Z"
          fill="#FAFAFF"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M4 4.33984H20V7.33984H4V4.33984ZM3 9.33984H2V2.33984H22V9.33984H21V22.3398H3V9.33984ZM5 9.33984V20.3398H19V9.33984H5Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const InboundIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M16 20.3398H5C4.44772 20.3398 4 19.8921 4 19.3398V11.4798C4 11.2205 4.10076 10.9713 4.281 10.7848L9.781 5.0848C9.96945 4.88957 10.2292 4.7793 10.5005 4.7793C10.7719 4.7793 11.0315 4.88957 11.22 5.0848L12.871 6.7978L11.438 8.1918L10.5 7.2198L6 11.8798V18.3348H17V19.3348C17.0013 19.6009 16.8966 19.8565 16.7089 20.0451C16.5212 20.2338 16.2661 20.3398 16 20.3398ZM17 16.3398H15V13.3398H12V11.3398H15V8.3398H17V11.3398H20V13.3398H17V16.3398Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const RequestPrintBarcode = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M2 4.33984H5V20.3398H2V4.33984Z" fill="#FAFAFF" />
        <path d="M6 4.33984H7V20.3398H6V4.33984Z" fill="#FAFAFF" />
        <path d="M11 4.33984H9V20.3398H11V4.33984Z" fill="#FAFAFF" />
        <path d="M12 4.33984H14V20.3398H12V4.33984Z" fill="#FAFAFF" />
        <path d="M15 4.33984H16V20.3398H15V4.33984Z" fill="#FAFAFF" />
        <path d="M20 4.33984H17V20.3398H20V4.33984Z" fill="#FAFAFF" />
        <path d="M21 4.33984H22V20.3398H21V4.33984Z" fill="#FAFAFF" />
      </svg>
    </div>
  );
};

const MutasiStockIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M4 4.33984H20V7.33984H4V4.33984ZM3 9.33984H2V2.33984H22V9.33984H21H19H5V13.3398H3V9.33984ZM19 20.3398H8V22.3398H21V19.3398H19V20.3398Z"
          fill="#FAFAFF"
        />
        <path
          d="M5.82958 19.3398L7.40958 20.9298L5.99958 22.3398L1.99958 18.3398L5.99958 14.3398L7.40958 15.7498L5.82958 17.3398H12.0068V19.3398H5.82958Z"
          fill="#FAFAFF"
        />
        <path
          d="M19.1773 13.3398L17.5973 11.7498L19.0073 10.3398L23.0073 14.3398L19.0073 18.3398L17.5973 16.9298L19.1773 15.3398H13V13.3398H19.1773Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const StockOpname = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M18 22.3399H6C4.89543 22.3399 4 21.4444 4 20.3399V4.33985C4 3.23529 4.89543 2.33985 6 2.33985H13C13.2654 2.33891 13.5201 2.44446 13.707 2.63286L19.707 8.63286C19.8954 8.81978 20.0009 9.07446 20 9.33985V20.3399C20 21.4444 19.1046 22.3399 18 22.3399ZM6 4.33986V20.3399H16.586L14.02 17.7739C13.4101 18.1415 12.7121 18.3371 12 18.3399C10.1612 18.3598 8.54049 17.1365 8.05545 15.3628C7.57041 13.589 8.34318 11.7113 9.93625 10.7927C11.5293 9.87419 13.5415 10.1461 14.8337 11.4545C16.1258 12.763 16.3724 14.7784 15.434 16.3599L18 18.9279V9.75386L12.586 4.33986H6ZM12 12.3399C10.8954 12.3399 10 13.2353 10 14.3399C10 15.4444 10.8954 16.3399 12 16.3399C13.1046 16.3399 14 15.4444 14 14.3399C14 13.2353 13.1046 12.3399 12 12.3399Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const AccountIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M7 8.33984C7 5.57842 9.23858 3.33984 12 3.33984C14.7614 3.33984 17 5.57842 17 8.33984C17 11.1013 14.7614 13.3398 12 13.3398C9.23858 13.3398 7 11.1013 7 8.33984ZM12 11.3398C13.6569 11.3398 15 9.9967 15 8.33984C15 6.68299 13.6569 5.33984 12 5.33984C10.3431 5.33984 9 6.68299 9 8.33984C9 9.9967 10.3431 11.3398 12 11.3398Z"
          fill="#FAFAFF"
        />
        <path
          d="M6.34315 16.683C4.84285 18.1833 4 20.2181 4 22.3398H6C6 20.7485 6.63214 19.2224 7.75736 18.0972C8.88258 16.972 10.4087 16.3398 12 16.3398C13.5913 16.3398 15.1174 16.972 16.2426 18.0972C17.3679 19.2224 18 20.7485 18 22.3398H20C20 20.2181 19.1571 18.1833 17.6569 16.683C16.1566 15.1827 14.1217 14.3398 12 14.3398C9.87827 14.3398 7.84344 15.1827 6.34315 16.683Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

// const Marketplace = () => {
//   return (
//     <div>
//       <svg
//         width="24"
//         height="25"
//         viewBox="0 0 24 25"
//         fill="none"
//         xmlns="http://www.w3.org/2000/svg"
//       >
//         <g clip-path="url(#clip0)">
//           <path
//             d="M5.88889 21.3398H19.1111C20.1528 21.3398 21 20.4877 21 19.4398V8.98984C21 8.73789 20.9005 8.49625 20.7234 8.31809C20.5463 8.13993 20.306 8.03984 20.0556 8.03984H17.2222V7.08984C17.2222 4.47069 15.1038 2.33984 12.5 2.33984C9.89617 2.33984 7.77778 4.47069 7.77778 7.08984V8.03984H4.94444C4.69396 8.03984 4.45374 8.13993 4.27662 8.31809C4.0995 8.49625 4 8.73789 4 8.98984V19.4398C4 20.4877 4.84717 21.3398 5.88889 21.3398ZM9.66667 7.08984C9.66667 5.51854 10.9379 4.23984 12.5 4.23984C14.0621 4.23984 15.3333 5.51854 15.3333 7.08984V8.03984H9.66667V7.08984ZM5.88889 9.93984H7.77778V11.8398H9.66667V9.93984H15.3333V11.8398H17.2222V9.93984H19.1111L19.113 19.4398H5.88889V9.93984Z"
//             fill="#FAFAFF"
//           />
//         </g>
//         <defs>
//           <clipPath id="clip0">
//             <rect
//               width="24"
//               height="24"
//               fill="white"
//               transform="translate(0 0.339844)"
//             />
//           </clipPath>
//         </defs>
//       </svg>
//     </div>
//   );
// };

const CategoryIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M11 4H21V6H11V4ZM11 8H17V10H11V8ZM11 14H21V16H11V14ZM11 18H17V20H11V18ZM3 4H9V10H3V4ZM5 6V8H7V6H5ZM3 14H9V20H3V14ZM5 16V18H7V16H5Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const MapIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M9.108 22.092L2 19.721V3.61297L8.892 5.91297L15.923 1.89197L22 4.32297V20.477L16.077 18.107L9.109 22.092H9.108ZM4 6.39197V18.279L8 19.612V7.71997L4 6.39197ZM14 5.29197L10 7.57997V19.28L14 16.992V5.29197ZM16.077 4.10597L16 4.15197V15.923L20 17.523V5.67597L16.077 4.10597Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};
const StoreIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M9 1.88281H19C20.103 1.88281 21 2.78509 21 3.89458V20.9946C21 21.2614 20.8946 21.5172 20.7071 21.7058C20.5196 21.8945 20.2652 22.0005 20 22.0005H4C3.73478 22.0005 3.48043 21.8945 3.29289 21.7058C3.10536 21.5172 3 21.2614 3 20.9946V11.9416C3 10.8321 3.897 9.92987 5 9.92987H7V3.89458C7 2.78509 7.897 1.88281 9 1.88281ZM11 11.9416H5V19.9887H11V11.9416ZM13 19.9887H19V3.89458H9V9.92987H11C12.103 9.92987 13 10.8321 13 11.9416V19.9887ZM12.9529 6.11811H11.0471V8.0238H12.9529V6.11811ZM16.7647 6.11811H14.8588V8.0238H16.7647V6.11811ZM16.7647 9.95903H14.8588V11.8352H16.7647V9.95903ZM16.7647 13.7409H14.8588V15.6466H16.7647V13.7409ZM9.14118 13.7418H7.23529V15.6475H9.14118V13.7418Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const InboundwarehouseIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M16 20H5C4.44772 20 4 19.5522 4 19V11.14C4 10.8806 4.10076 10.6314 4.281 10.445L9.781 4.74496C9.96945 4.54972 10.2292 4.43945 10.5005 4.43945C10.7719 4.43945 11.0315 4.54972 11.22 4.74496L13.356 6.95996L11.922 8.35396L10.5 6.87996L6 11.54V17.995H17V18.995C17.0013 19.261 16.8966 19.5167 16.7089 19.7053C16.5212 19.8939 16.2661 20 16 20Z"
          fill="#FAFAFF"
        />
        <path
          d="M15 17L10 12L15 7L16.41 8.41L13.83 11H20V13H13.83L16.41 15.59L15 17Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const OutboundwarehouseIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M16 20H5C4.44772 20 4 19.5522 4 19V11.14C4 10.8806 4.10076 10.6314 4.281 10.445L9.781 4.74496C9.96945 4.54972 10.2292 4.43945 10.5005 4.43945C10.7719 4.43945 11.0315 4.54972 11.22 4.74496L13.356 6.95996L11.922 8.35396L10.5 6.87996L6 11.54V17.995H17V18.995C17.0013 19.261 16.8966 19.5167 16.7089 19.7053C16.5212 19.8939 16.2661 20 16 20Z"
          fill="#EBEBFF"
        />
        <path
          d="M15 17L20 12L15 7L13.59 8.41L16.17 11H10V13H16.17L13.59 15.59L15 17Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const SingleTransaction = () => (
  <div>
    <svg
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16 12.3408V9.34082H3V7.34082H16V4.34082L21 8.34082L16 12.3408Z"
        fill="#FAFAFF"
      />
      <path
        d="M16 21.3408V18.3408H3V16.3408H16V13.3408L21 17.3408L16 21.3408Z"
        fill="#FAFAFF"
      />
    </svg>
  </div>
);

const ReportIconOne = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M18 22.3398H6C4.89543 22.3398 4 21.4444 4 20.3398V4.33984C4 3.23527 4.89543 2.33984 6 2.33984H13C13.0109 2.34032 13.0217 2.34234 13.032 2.34584C13.0418 2.34886 13.0518 2.35087 13.062 2.35184C13.1502 2.3575 13.2373 2.37464 13.321 2.40284L13.349 2.41184C13.3717 2.41953 13.3937 2.42888 13.415 2.43984C13.5239 2.48827 13.6232 2.55603 13.708 2.63984L19.708 8.63984C19.7918 8.72463 19.8596 8.8239 19.908 8.93284C19.918 8.95484 19.925 8.97784 19.933 9.00084L19.942 9.02684C19.9699 9.11023 19.9864 9.19702 19.991 9.28484C19.9926 9.29403 19.9949 9.30306 19.998 9.31184C19.9998 9.32107 20.0004 9.33046 20.0001 9.33984V20.3398C20.0001 21.4444 19.1046 22.3398 18 22.3398ZM6 4.33984V20.3398H18V10.3398H13C12.4477 10.3398 12 9.89213 12 9.33984V4.33984H6ZM14 5.75384V8.33984H16.586L14 5.75384Z"
          fill="#FAFAFF"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M16.6727 13.0795L10.4672 18.7209L7.29297 15.5467L8.70718 14.1324L10.5329 15.9582L15.3274 11.5996L16.6727 13.0795Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const ReportIconTwo = () => {
  return (
    <div>
      <svg
        width="24"
        height="25"
        viewBox="0 0 24 25"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M18 22.3398H6C4.89543 22.3398 4 21.4444 4 20.3398V4.33984C4 3.23527 4.89543 2.33984 6 2.33984H13C13.0109 2.34032 13.0217 2.34234 13.032 2.34584C13.0418 2.34886 13.0518 2.35087 13.062 2.35184C13.1502 2.3575 13.2373 2.37464 13.321 2.40284L13.349 2.41184C13.3717 2.41953 13.3937 2.42888 13.415 2.43984C13.5239 2.48827 13.6232 2.55603 13.708 2.63984L19.708 8.63984C19.7918 8.72463 19.8596 8.8239 19.908 8.93284C19.918 8.95484 19.925 8.97784 19.933 9.00084L19.942 9.02684C19.9699 9.11023 19.9864 9.19702 19.991 9.28484C19.9926 9.29403 19.9949 9.30306 19.998 9.31184C19.9998 9.32107 20.0004 9.33046 20.0001 9.33984V20.3398C20.0001 21.4444 19.1046 22.3398 18 22.3398ZM6 4.33984V20.3398H18V10.3398H13C12.4477 10.3398 12 9.89213 12 9.33984V4.33984H6ZM14 5.75384V8.33984H16.586L14 5.75384Z"
          fill="#FAFAFF"
        />
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M16.6727 13.0795L10.4672 18.7209L7.29297 15.5467L8.70718 14.1324L10.5329 15.9582L15.3274 11.5996L16.6727 13.0795Z"
          fill="#FAFAFF"
        />
      </svg>
    </div>
  );
};

const menus = {
  customer: {
    topMenu: [
      {
        title: 'Dashboard',
        route: '/dashboard',
        icon: DashboardIcon,
      },
      // {
      //   title: 'Transaksi',
      //   route: '/transactions',
      //   icon: TransactionIcon,
      // },
      {
        title: 'Inbound',
        route: '/inbound',
        icon: InboundIcon,
      },
      // {
      //   title: 'Inventory',
      //   icon: InventoryIcon,
      //   subMenu: [
      //     {
      //       title: 'Stocks',
      //       route: '/inventory/stock',
      //       icon: StockIcon,
      //     },
      //     {
      //       title: 'Inbound',
      //       route: '/inventory/inbound',
      //       icon: InboundIcon,
      //     },
      //     {
      //       title: 'Request Print Barcode',
      //       route: '/inventory/req-print-barcode',
      //       icon: RequestPrintBarcode,
      //     },
      //     {
      //       title: 'Mutasi Stock',
      //       route: '/inventory/mutasistock',
      //       icon: MutasiStockIcon,
      //     },
      //     {
      //       title: 'QC Middle',
      //       route: '/inventory/qcm',
      //       icon: ReportIcon,
      //     },
      //     {
      //       title: 'Stock Opname',
      //       route: '/inventory/stock-opname',
      //       icon: StockOpname,
      //     },
      //   ],
      // },
      {
        title: 'Outbound',
        icon: OutboundwarehouseIcon,
        subMenu: [
          {
            title: 'Dispatch Order',
            route: '/outbound/dispatch-order',
            icon: '',
          },
          {
            title: 'Transaksi Regular',
            route: '/outbound/regular-transaction',
            icon: '',
          },
          {
            title: 'Cashless',
            route: '/outbound/cashless',
            icon: '',
          },
        ],
      },
      {
        title: 'Gudang',
        route: '/gudang',
        icon: WarehouseIcon,
      },
      {
        title: 'Pindah Gudang',
        route: '/pindah-gudang',
        icon: ChangeWarehouseIcon,
      },
      {
        title: 'System Settings',
        route: '/system-settings/akun',
        icon: SystemSettingIcon,
        subMenu: [
          {
            title: 'Akun',
            route: '/system-settings/akun',
            icon: AccountIcon,
          },
          {
            title: 'Store',
            route: '/system-settings/store',
            icon: StoreIcon,
          },
        ],
      },
      {
        title: 'Laporan',
        route: '',
        icon: ReportIcon,
        subMenu: [
          {
            title: 'Transaksi',
            route: '/report/transaction',
            icon: ReportIconOne,
          },
          {
            title: 'Invoice',
            route: '/report/invoice',
            icon: ReportIconOne,
          },
          // {
          //   title: 'Cari Transaksi',
          //   route: '/report/cari-transaksi',
          //   icon: StockOpname,
          // },
          // {
          //   title: 'History Reimburse',
          //   route: '/report/history-reimburse',
          //   icon: StoreIcon,
          // },
        ],
      },
    ],
    bottomMenu: [
      {
        title: 'Help',
        route: '/help',
        icon: HelpIcon,
      },
    ],
  },

  administrator: {
    topMenu: [
      {
        title: 'Dashboard',
        route: '/admin/dashboard',
        icon: DashboardIcon,
      },
      {
        title: 'Warehouse',
        route: '/admin/warehouse',
        icon: WarehouseIcon,
      },
      {
        title: 'Category',
        route: '/admin/category',
        icon: CategoryIcon,
      },
      {
        title: 'Store',
        route: '/admin/store',
        icon: StoreIcon,
      },
      {
        title: 'Packaging',
        route: '/admin/packaging',
        icon: PackageIcon,
      },
    ],
    bottomMenu: [
      {
        title: 'Help',
        route: '/help',
        icon: HelpIcon,
      },
    ],
  },
  warehouse: {
    topMenu: [
      {
        title: 'Dashboard',
        route: '/warehouse/dashboard',
        icon: DashboardIcon,
      },
      {
        title: 'Inbound',
        route: '/warehouse/inbound',
        icon: InboundwarehouseIcon,
      },
      {
        title: 'Outbound',
        icon: OutboundwarehouseIcon,
        subMenu: [
          {
            title: 'Dispatch Order',
            route: '/warehouse/outbound/dispatch-order',
            icon: TransactionIcon,
          },
          {
            title: 'Transaksi Regular',
            route: '/warehouse/outbound/regular-transaction',
            icon: SingleTransaction,
          },
          {
            title: 'Cashless',
            route: '/warehouse/outbound/cashless',
            icon: TransactionIcon,
          },
        ],
      },
      {
        title: 'Manifest',
        route: '/warehouse/manifest',
        icon: WarehouseIcon,
      },
      {
        title: 'Store',
        route: '/warehouse/store',
        icon: StoreIcon,
      },
      {
        title: 'Laporan',
        icon: ReportIcon,
        subMenu: [
          {
            title: 'Transaksi',
            route: '/warehouse/report/transaction',
            icon: ReportIconOne,
          },
          {
            title: 'Invoice',
            route: '/warehouse/report/invoice',
            icon: ReportIconTwo,
          },
        ],
      },
    ],
    bottomMenu: [
      {
        title: 'Help',
        route: '/help',
        icon: HelpIcon,
      },
    ],
  },
};

export { menus };
