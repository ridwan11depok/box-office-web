import React from 'react';
import { ListGroup } from 'react-bootstrap';

const TabHeader = (props) => {
  const {
    activeTab,
    inActiveColor,
    activeColor,
    activeBackground,
    listHeader,
    styleContainer,
    styleItem,
    border,
    onChange,
    inActiveBackground,
    isClickable,
  } = props;
  return (
    <ListGroup
      horizontal="md"
      style={{
        cursor: isClickable && 'pointer',
        width: '100%',
        ...styleContainer,
      }}
    >
      {listHeader.map((item, idx) => (
        <ListGroup.Item
          onClick={() => onChange(item.tab)}
          className="p-1"
          style={{
            height: '48px',
            flex: 1 / listHeader.length,
            borderRight: idx + 1 !== listHeader.length && 'none',
            ...styleItem,
            border: !border && 'none',
          }}
        >
          <div
            className="text-center h-100 d-flex align-items-center justify-content-center w-100 rounded"
            style={{
              backgroundColor:
                activeTab === item.tab ? activeBackground : inActiveBackground,
            }}
          >
            {item.render ? (
              item.render(props, item)
            ) : (
              <span
                style={{
                  color: activeTab === item.tab ? activeColor : inActiveColor,
                  fontWeight: '700',
                }}
              >
                {item.title}
              </span>
            )}
          </div>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
};

TabHeader.defaultProps = {
  styleContainer: { backgroundColor: 'transparent' },
  styleItem: { backgroundColor: 'transparent' },
  activeTab: 2,
  border: true,
  activeBackground: '#EBF0FA',
  inActiveBackground: 'transparent',
  activeColor: '#192a55',
  inActiveColor: '#828282',
  listHeader: [
    { title: 'Title 1', tab: 1, renderItem: null },
    { title: 'Title 2', tab: 2, renderItem: null },
    { title: 'Title 3', tab: 3, renderItem: null },
  ],
  onChange: () => {},
  isClickable: true,
};

export default TabHeader;
