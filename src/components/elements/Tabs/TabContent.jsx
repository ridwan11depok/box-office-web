import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Card, Col, Tab, ListGroup } from 'react-bootstrap';

export default function TabContent(props) {
  const { tab, activeTab, children } = props;
  return (
    tab === activeTab && <>{children}</>
    // <Tab.Content>
    //   <Tab.Pane eventKey={1}>
    //     <h1>Tab 1</h1>
    //   </Tab.Pane>
    //   <Tab.Pane eventKey={2}>
    //     <h1>Tab 1</h1>
    //   </Tab.Pane>
    // </Tab.Content>
  );
}
