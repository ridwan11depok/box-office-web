import { toast } from 'react-toastify';

function Toastify({ type, message }, config = {}) {
  return toast[type](message, {
    position: 'bottom-right',
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    ...config,
  });
}

export default Toastify;
