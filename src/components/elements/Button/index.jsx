import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  button: {
    // margin: theme.spacing(1),
    textTransform: 'none',
  },
  blueFill: {
    backgroundColor: '#192A55',
    color: '#FFFFFF',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: '#192A55',
    },
  },
  blueFillDisabled: {
    backgroundColor: 'rgba(25, 42, 85,0.5)',
    color: '#FAFAFF !important',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'rgba(25, 42, 85,0.5)',
    },
  },
  blueOutline: {
    backgroundColor: 'transparent',
    color: '#192A55',
    width: 'auto',
    height: '40px',
    border: '2px solid #192A55',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  blueOutlineDisabled: {
    backgroundColor: 'transparent',
    color: 'rgba(25, 42, 85,0.5) !important',
    width: 'auto',
    height: '40px',
    border: '2px solid rgba(25, 42, 85,0.5)',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  blueNoFill: {
    backgroundColor: 'transparent',
    color: '#192A55',
    width: 'auto',
    height: '40px',
    border: 'none',
    fontSize: '16px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  lightBlueFill: {
    backgroundColor: '#E8E8E8',
    color: '#192A55',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: '#E8E8E8',
    },
  },
  lightBlueFillDisabled: {
    backgroundColor: 'rgba(232, 232, 232,0.5)',
    color: 'rgba(25, 42, 85,0.5) !important',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'rgba(232, 232, 232,0.5)',
    },
  },
  lightBlueFillOutline: {
    backgroundColor: '#E8E8E8',
    color: '#192A55',
    width: 'auto',
    height: '40px',
    border: '2px solid #192A55',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: '#E8E8E8',
    },
  },
  redFill: {
    backgroundColor: '#DA101A',
    color: '#FFFFFF',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: '#DA101A',
    },
  },
  redFillDisabled: {
    backgroundColor: 'rgba(218, 16, 26,0.5)',
    color: '#FFFFFF !important',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'rgba(218, 16, 26,0.5)',
    },
  },
  redOutline: {
    backgroundColor: 'transparent',
    color: '#DA101A',
    width: 'auto',
    height: '40px',
    border: '2px solid #DA101A',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  redOutlineDisabled: {
    backgroundColor: 'transparent',
    color: 'rgba(218, 16, 26,0.5) !important',
    width: 'auto',
    height: '40px',
    border: '2px solid rgba(218, 16, 26,0.5)',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  redNoFill: {
    backgroundColor: 'transparent',
    color: '#DA101A',
    width: 'auto',
    height: '40px',
    border: 'none',
    fontSize: '16px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  whiteOutline: {
    backgroundColor: 'transparent',
    color: '#FAFAFF',
    width: 'auto',
    height: '40px',
    border: '2px solid #FAFAFF',
    fontSize: '16px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  greyOutline: {
    backgroundColor: 'transparent',
    color: '#828282',
    width: 'auto',
    height: '40px',
    border: '2px solid #CFCFCF',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  greenFill: {
    backgroundColor: '#1B5E20',
    color: '#FFFFFF',
    width: 'auto',
    height: '40px',
    fontSize: '14px',
    fontWeight: '700',
    fontFamily: 'Open Sans',
    '&:hover': {
      backgroundColor: '#1B5E20',
    },
  },
}));

export default function CustomButton(props) {
  const classes = useStyles();
  const {
    variant,
    className,
    startIcon,
    text,
    endIcon,
    onClick,
    styleType,
    style,
    disabled,
    isOptionButton,
    renderText,
  } = props;
  return (
    <>
      <Button
        disabled={disabled}
        variant={variant}
        className={`${classes.button} ${className} ${
          styleType && classes[styleType]
        }`}
        style={style}
        startIcon={startIcon()}
        endIcon={endIcon()}
        onClick={onClick}
      >
        {isOptionButton ? (
          <i class="fas fa-ellipsis-v"></i>
        ) : text ? (
          text
        ) : (
          renderText()
        )}
      </Button>
    </>
  );
}
CustomButton.defaultProps = {
  variant: '',
  className: '',
  startIcon: () => {},
  text: '',
  endIcon: () => {},
  onClick: () => {},
  styleType: '',
  style: {},
  disabled: false,
  renderText: () => {},
  isOptionButton: false,
};

CustomButton.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  variant: PropTypes.string,
  endIcon: PropTypes.func,
  startIcon: PropTypes.func,
  onClick: PropTypes.func,
  styleType: PropTypes.oneOf([
    'blueFill',
    'blueOutline',
    'blueNoFill',
    'redFill',
    'redOutline',
    'redNoFill',
    'lightBlueFill',
    'whiteOutline',
  ]),
  style: PropTypes.object,
  disabled: PropTypes.bool,
  renderText: PropTypes.func,
  isOptionButton: PropTypes.bool,
};
