import React from 'react';
import { Modal } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../Button';

const isEmtyFunction = (func) => {
  const comparator = () => {};
  if (func.toString() === comparator.toString()) {
    return true;
  } else {
    return false;
  }
};

const useStyles = makeStyles({
  title: {
    fontWeight: '700',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    color: '#1c1c1c',
    marginLeft: '10px',
  },
  body: {
    fontWeight: '400',
    fontFamily: 'Work Sans',
    fontSize: '14px',
    color: '#1c1c1c',
  },
});

function CustomModal(props) {
  const classes = useStyles();
  const listButton = () => {
    const action = {
      agree: props.onAgree,
      disagree: props.onHide,
    };
    return props.defaultFooter.map((item, idx) => (
      <Button
        key={idx.toString()}
        text={item.text}
        styleType={item.buttonStyle}
        onClick={action[item.buttonType]}
        style={{ marginRight: idx === 0 && '5px' }}
      />
    ));
  };
  return (
    <Modal
      {...props}
      size={props.sizeModal}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      style={{ zIndex: 1500, ...props.modalStyle }}
      className={props.modalClassName}
    >
      {props.showHeader && (
        <Modal.Header
          style={props.headerStyle}
          className={`d-flex align-items-center p-3 ${
            props.closeButtonPosition === 'left'
              ? 'justify-content-start'
              : 'justify-content-between'
          } ${props.headerClassName}`}
        >
          {props.closeButtonPosition === 'left' && (
            <>
              {props.closeButton && (
                <i
                  onClick={props.onHide}
                  className="fas fa-times"
                  style={{ fontSize: '19px', cursor: 'pointer' }}
                ></i>
              )}
              <Modal.Title
                className={`ml-4 ${classes.title} ${props.titleClassName}`}
                id="contained-modal-title-vcenter"
              >
                {props.titleModal || props.header(props.data)}
              </Modal.Title>
            </>
          )}
          {props.closeButtonPosition === 'right' && (
            <>
              <Modal.Title
                className={`${classes.title} ${props.titleClassName}`}
                style={props.titleStyle}
                id="contained-modal-title-vcenter"
              >
                {props.title || props.header(props.data)}
              </Modal.Title>
              {props.closeButton && (
                <i
                  onClick={props.onHide}
                  className="fas fa-times"
                  style={{ fontSize: '19px', cursor: 'pointer' }}
                ></i>
              )}
            </>
          )}
        </Modal.Header>
      )}
      <Modal.Body
        style={{ paddingBottom: 0, paddingTop: 0, ...props.bodyStyle }}
        className={`${classes.body} ${props.bodyClassName}`}
      >
        {props.content(props.data)}
      </Modal.Body>
      {props.showFooter && (
        <Modal.Footer
          className={props.footerClassName}
          style={{
            ...props.footerStyle,
          }}
        >
          {isEmtyFunction(props.footer)
            ? listButton()
            : props.footer(props.data)}
        </Modal.Footer>
      )}
    </Modal>
  );
}

CustomModal.defaultProps = {
  sizeModal: 'md',
  show: false,
  titleModal: '',
  onHide: () => {},
  onAgree: () => {},
  header: () => {},
  content: () => {},
  footer: () => {},
  data: [],
  closeButton: true,
  closeButtonPosition: 'left',
  showHeader: true,
  showFooter: true,
  modalStyle: {},
  modalClassName: '',
  headerStyle: { borderBottom: '1px solid #E8E8E8' },
  headerClassName: '',
  titleStyle: {},
  titleClassName: '',
  bodyStyle: { borderBottom: '1px solid #E8E8E8' },
  bodyClassName: '',
  footerStyle: {},
  footerClassName: '',
  defaultFooter: [
    {
      buttonType: 'disagree',
      text: 'Batal',
      buttonStyle: 'lightBlueFill', //See props styleType of Button element
    },
    {
      buttonType: 'agree',
      text: 'Ya',
      buttonStyle: 'blueFill', //See props styleType of Button element
    },
  ],
};

export default CustomModal;
