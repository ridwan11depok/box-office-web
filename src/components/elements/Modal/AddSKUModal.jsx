import React, { useEffect, useState } from 'react';
import Modal from './Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Form, InputGroup, Row, Col } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import pictureIcon from '../../../assets/icons/picture_icon.svg';
import SwitchToggle from '../SwitchToggle';
import { SelectField, TextField, CurrencyInput } from '../InputField';
import { useSelector, useDispatch } from 'react-redux';
import { getListCategory } from '../../../pages/customer/System Settings/reduxAction';
import { validateFile } from '../../../utils/validate';

const allowedExtentions = ['image/png', 'image/jpg', 'image/jpeg'];

const useStyles = makeStyles((theme) => ({
  btnDisagree: {
    backgroundColor: '#E8E8E8',
    width: '120px',
    height: '40px',
    borderRadius: '4px',
    border: 'none',
    outline: 'none',
    color: '#192A55',
    fontWeight: '700',
    fontFamily: 'Open Sans',
  },
  descImage: {
    color: '#4F4F4F',
    fontWeight: '400',
    fontSize: '14px',
    fontFamily: 'Open Sans',
    marginBottom: '3px',
  },
  imageContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContent: {
    width: '150px',
    height: '150px',
    border: '2px dashed #CFCFCF',
    backgroundColor: '#F2F2F2',
    marginTop: '7px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '4px',
    overflow: 'hidden',
    cursor: 'pointer',
    position: 'relative',
  },
  imageSelected: {
    width: '150px',
    height: 'auto',
  },
  deleteIconContainer: {
    position: 'absolute',
    right: '10px',
    bottom: '10px',
    padding: '3px',
    backgroundColor: '#FFFFFF',
    paddingLeft: '6px',
    paddingRight: '6px',
    zIndex: 5,
    borderRadius: '4px',
  },
  deleteIcon: {
    color: '#192A55',
    fontSize: '14px',
  },
}));

const validationSchema = Yup.object({
  sku: Yup.string().required('Harap untuk mengisi SKU produk'),
  name: Yup.string().required('Harap untuk mengisi nama produk'),
  sell_price: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .required('Harap untuk mengisi harga jual produk'),
  category_product_id: Yup.string().required(
    'Harap untuk mengisi kategori produk'
  ),
  sub_category_product_id: Yup.string().required(
    'Harap untuk mengisi sub kategori produk'
  ),
  width: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .min(0.1, 'Lebar minimum 0.1')
    .required('Lebar minimum 0.1'),
  length: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .min(0.1, 'Panjang minimum 0.1')
    .required('Panjang minimum 0.1'),
  height: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .min(0.1, 'Tinggi minimum 0.1')
    .required('Tinggi minimum 0.1'),
  weight: Yup.number()
    .typeError('Input Harus Berupa Angka')
    .min(0.1, 'Berat minimum 0.1')
    .required('Berat minimum 0.1'),
  images: Yup.array().min(3, 'Minimal harus menyertakan 1 foto'),
});

const validationSchemaEdit = Yup.object({});

const AddSKUModal = (props) => {
  const dispatch = useDispatch();
  const { listCategoryProduct, listSubCategoryProduct } = useSelector(
    (state) => state.systemSettingCustomer
  );
  const { isLoading } = useSelector((state) => state.loading);
  const classes = useStyles();
  const [state, setState] = useState({
    defaultQC: false,
    firstImage: '',
    secondImage: '',
    thirdImage: '',
    isFefo: false,
  });
  const firstImageRef = React.createRef();
  const secondImageRef = React.createRef();
  const thirdImageRef = React.createRef();
  const formik = useFormik({
    initialValues: {
      sku: '',
      name: '',
      sell_price: '',
      category_product_id: '',
      sub_category_product_id: '',
      width: '',
      length: '',
      height: '',
      weight: '',
      images: [],
    },
    validationSchema:
      props.initialValues && props.show
        ? validationSchemaEdit
        : validationSchema,
    onSubmit: (values) => {
      const payload = {
        ...values,
        // qc_step: state.defaultQC,
        is_fefo: state.isFefo,
      };
      props.onAgree(payload);
    },
  });
  const handleChangeFile = async (e, num) => {
    const files = e.target.files;
    const response = await validateFile({
      files,
      type: 'image',
      allowedExtensions: ['jpg', 'png', 'jpeg'],
    });
    if (response) {
      if (num === 1 && state.firstImage === '') {
        setState({
          ...state,
          firstImage: response?.file,
        });
        formik.setFieldValue('images', [
          response?.file,
          state.secondImage,
          state.thirdImage,
        ]);
      } else if (num === 2 && state.secondImage === '') {
        setState({
          ...state,
          secondImage: response?.file,
        });
        formik.setFieldValue('images', [
          state.firstImage,
          response?.file,
          state.thirdImage,
        ]);
      } else if (num === 3 && state.thirdImage === '') {
        setState({
          ...state,
          thirdImage: response?.file,
        });
        formik.setFieldValue('images', [
          state.firstImage,
          state.secondImage,
          response?.file,
        ]);
      }
    }
  };
  const handleDeleteFile = (num) => {
    if (num === 1) {
      setState({
        ...state,
        firstImage: '',
      });
      formik.setFieldValue('images', ['', state.secondImage, state.thirdImage]);
    } else if (num === 2) {
      setState({
        ...state,
        secondImage: '',
      });
      formik.setFieldValue('images', [state.firstImage, '', state.thirdImage]);
    } else {
      setState({
        ...state,
        thirdImage: '',
      });
      formik.setFieldValue('images', [state.firstImage, state.secondImage, '']);
    }
  };

  useEffect(() => {
    if (!props.show) {
      formik.resetForm();
      setState({
        ...state,
        defaultQC: false,
        isFefo: false,
        firstImage: '',
        secondImage: '',
        thirdImage: '',
      });
    }
    if (props.initialValues && props.show) {
      const init = { ...props.initialValues };
      formik.setFieldValue('sku', init.sku);
      formik.setFieldValue('name', init.name);
      formik.setFieldValue('sell_price', init.sell_price);
      formik.setFieldValue('category_product_id', init.category_product_id);
      formik.setFieldValue(
        'sub_category_product_id',
        init.sub_category_product_id
      );
      formik.setFieldValue('width', init.width);
      formik.setFieldValue('length', init.length);
      formik.setFieldValue('height', init.height);
      formik.setFieldValue('weight', init.weight);
      const qc_step = init.qc_step == 1 ? true : false;
      const is_fefo = init.is_fefo == 1 ? true : false;
      dispatch(getListCategory(init.category_product_id));
      setState({
        ...state,
        defaultQC: qc_step,
        isFefo: is_fefo,
      });
    }
  }, [props.show, props.initialValues]);
  const handleCheckDefaultQC = (checked) => {
    setState({ ...state, defaultQC: checked });
  };
  const handleCheckIsFefo = (checked) => {
    setState({ ...state, isFefo: checked });
  };
  useEffect(() => {
    dispatch(getListCategory());
  }, []);

  const handleChangeCategory = (e) => {
    dispatch(getListCategory(e.target.value));
  };

  useEffect(() => {
    console.log('props :>> ', props);
  }, [props]);

  return (
    <>
      <input
        onChange={(e) => handleChangeFile(e, 1)}
        ref={firstImageRef}
        type="file"
        className="d-none"
        value={state.firstImage?.filename || ''}
        accept={allowedExtentions.join(',')}
      />
      <input
        onChange={(e) => handleChangeFile(e, 2)}
        ref={secondImageRef}
        type="file"
        className="d-none"
        value={state.secondImage?.filename || ''}
        accept={allowedExtentions.join(',')}
      />
      <input
        onChange={(e) => handleChangeFile(e, 3)}
        ref={thirdImageRef}
        type="file"
        className="d-none"
        value={state.thirdImage?.filename || ''}
        accept={allowedExtentions.join(',')}
      />
      <Modal
        titleModal={props.initialValues ? 'Edit SKU' : 'Tambah SKU'}
        titleClassName="title-modal"
        bodyClassName="body-modal pb-4 pt-4"
        sizeModal="xl"
        modalStyle={{ zIndex: 1500 }}
        show={props.show}
        onHide={props.onHide}
        scrollable
        content={(data) => (
          <>
            <Grid
              container
              justifyContent="space-between"
              direction="row"
              spacing={1}
            >
              <Grid
                item
                // xs={6}
                style={{
                  border: '1px solid #E8E8E8',
                  borderRadius: '4px',
                  padding: '10px',
                  width: '49%',
                }}
              >
                <Form.Group className="mb-1 mt-3" controlId="formBasicEmail">
                  <Form.Label as={'h6'} className="input-label">
                    Foto Produk
                  </Form.Label>
                  <Form.Text className={classes.descImage}>
                    Format gambar .jpg .jpeg .png dan ukuran minimum 300 x 300px
                    (Untuk gambar optimal gunakan ukuran minimum 700 x 700 px)
                  </Form.Text>
                  <div className={classes.imageContainer}>
                    <div
                      className={classes.imageContent}
                      onClick={() => {
                        state.firstImage === '' &&
                          firstImageRef.current.click();
                      }}
                    >
                      {state.firstImage ||
                        props.initialValues?.file_documents?.[0] ? (
                        <>
                          <img
                            className={classes.imageSelected}
                            // src={URL.createObjectURL(state.firstImage)}
                            alt=""
                            src={
                              state.firstImage
                                ? URL.createObjectURL(state.firstImage)
                                : props.initialValues?.file_documents?.[0]
                                  ?.url || ''
                            }
                          />
                          <div
                            className={classes.deleteIconContainer}
                            onClick={() => handleDeleteFile(1)}
                          >
                            <i
                              className={
                                'far fa-trash-alt ' + classes.deleteIcon
                              }
                            ></i>
                          </div>
                        </>
                      ) : (
                        <img src={pictureIcon} alt="" />
                      )}
                    </div>
                    <div
                      className={classes.imageContent}
                      onClick={() => {
                        state.secondImage === '' &&
                          secondImageRef.current.click();
                      }}
                    >
                      {state.secondImage ||
                        props.initialValues?.file_documents?.[1] ? (
                        <>
                          <img
                            className={classes.imageSelected}
                            // src={URL.createObjectURL(state.secondImage)}
                            alt=""
                            src={
                              state.secondImage
                                ? URL.createObjectURL(state.secondImage)
                                : props.initialValues?.file_documents?.[1]
                                  ?.url || ''
                            }
                          />
                          <div
                            className={classes.deleteIconContainer}
                            onClick={() => handleDeleteFile(2)}
                          >
                            <i
                              className={
                                'far fa-trash-alt ' + classes.deleteIcon
                              }
                            ></i>
                          </div>
                        </>
                      ) : (
                        <img src={pictureIcon} alt="" />
                      )}
                    </div>
                    <div
                      className={classes.imageContent}
                      onClick={() => {
                        state.thirdImage === '' &&
                          thirdImageRef.current.click();
                      }}
                    >
                      {state.thirdImage ||
                        props.initialValues?.file_documents?.[2] ? (
                        <>
                          <img
                            className={classes.imageSelected}
                            // src={URL.createObjectURL(state.thirdImage)}
                            alt=""
                            src={
                              state.thirdImage
                                ? URL.createObjectURL(state.thirdImage)
                                : props.initialValues?.file_documents?.[2]
                                  ?.url || ''
                            }
                          />
                          <div
                            className={classes.deleteIconContainer}
                            onClick={() => handleDeleteFile(3)}
                          >
                            <i
                              className={
                                'far fa-trash-alt ' + classes.deleteIcon
                              }
                            ></i>
                          </div>
                        </>
                      ) : (
                        <img src={pictureIcon} alt="" />
                      )}
                    </div>
                  </div>
                  {formik.touched.images && formik.errors.images && (
                    <Form.Text className="text-input-error">
                      {formik.errors.images}
                    </Form.Text>
                  )}
                </Form.Group>
                <TextField
                  label="SKU"
                  formik={formik}
                  withValidate
                  name="sku"
                  placeholder="Input SKU"
                />
                <TextField
                  label="Nama Produk"
                  formik={formik}
                  withValidate
                  name="name"
                  placeholder="Input nama produk"
                />
                <CurrencyInput
                  label="Harga Jual"
                  formik={formik}
                  withValidate
                  name="sell_price"
                  placeholder="Input harga jual"
                />
              </Grid>
              <Grid
                item
                // xs={6}
                style={{
                  border: '1px solid #E8E8E8',
                  borderRadius: '4px',
                  padding: '10px',
                  width: '49%',
                }}
              >
                <SelectField
                  label="Kategori Produk"
                  options={listCategoryProduct}
                  withValidate
                  formik={formik}
                  name="category_product_id"
                  onChange={handleChangeCategory}
                />
                <SelectField
                  label="Sub Kategori Produk"
                  options={listSubCategoryProduct}
                  withValidate
                  formik={formik}
                  name="sub_category_product_id"
                />
                <Row>
                  <Col xs={6}>
                    <TextField
                      label="Lebar"
                      formik={formik}
                      withValidate
                      name="width"
                      placeholder="Input lebar"
                      suffix={() => 'cm'}
                    />
                  </Col>
                  <Col xs={6}>
                    <TextField
                      label="Tinggi"
                      formik={formik}
                      withValidate
                      name="height"
                      placeholder="Input tinggi"
                      suffix={() => 'cm'}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={6}>
                    <TextField
                      label="Panjang"
                      formik={formik}
                      withValidate
                      name="length"
                      placeholder="Input tinggi"
                      suffix={() => 'cm'}
                    />
                  </Col>
                  <Col xs={6}>
                    <TextField
                      label="Berat"
                      formik={formik}
                      withValidate
                      name="weight"
                      placeholder="Input berat"
                      suffix={() => 'kg'}
                    />
                  </Col>
                </Row>

                {/* // Default QC */}
                {/* <Form.Group
                  className="mb-1 mt-3"
                  controlId="formPlaintextEmail"
                >
                  <Row>
                    <Col xs={6}>
                      <Form.Label as={'h6'} className="input-label">
                        Default QC
                      </Form.Label>
                    </Col>
                    <Col
                      xs={6}
                      style={{ display: 'flex', justifyContent: 'end' }}
                    >
                      <SwitchToggle
                        onChange={handleCheckDefaultQC}
                        name="qc"
                        defaultValue={
                          props.initialValues?.qc_step == 1 ? true : false
                        }
                      />
                    </Col>
                  </Row>
                  {state.defaultQC && (
                    <>
                      <h6 className="input-label">Step QC</h6>
                      <Form.Control
                        className="text-placeholder col-md-12"
                        type="text"
                        as="textarea"
                        style={{height: 10}}
                        placeholder="1. Contoh QC step yang di ketik 1&#10;2. Contoh QC step yang di ketik 2"
                      />
                    </>
                  )}
                </Form.Group> */}
                <Form.Group
                  className="mb-1 mt-3"
                  controlId="formPlaintextEmail"
                >
                  <Row>
                    <Col xs={6}>
                      <Form.Label as={'h6'} className="input-label">
                        FEFO{' '}
                        <i
                          className="fas fa-info-circle"
                          style={{ color: '#192A55' }}
                        ></i>
                      </Form.Label>
                    </Col>
                    <Col
                      xs={6}
                      style={{ display: 'flex', justifyContent: 'end' }}
                    >
                      <SwitchToggle
                        onChange={handleCheckIsFefo}
                        name="fefo"
                        // defaultValue={state.isFefo}
                        defaultValue={
                          props.initialValues?.is_fefo == 1 ? true : false
                        }
                      />
                    </Col>
                  </Row>
                </Form.Group>
              </Grid>
            </Grid>
          </>
        )}
        footer={(data) => (
          <>
            <button
              className={`${classes.btnDisagree} mr-2`}
              onClick={props.onHide}
            >
              Batal
            </button>
            <button
              onClick={(e) => {
                // if (!Boolean(formik.values.role)) {
                //   formik.setFieldTouched('role', true);
                // }
                formik.handleSubmit(e);
              }}
              // disabled={formik.isSubmitting}
              style={{
                backgroundColor: '#192A55',
                width: '120px',
                height: '40px',
                borderRadius: '4px',
                border: 'none',
                outline: 'none',
                color: '#FFFFFF',
                fontWeight: '700',
                fontFamily: 'Open Sans',
              }}
            >
              {isLoading
                ? 'Loading...'
                : props.initialValues
                  ? 'Perbarui'
                  : 'Tambah'}
            </button>
          </>
        )}
      />
    </>
  );
};

AddSKUModal.defaultProps = {
  initialValues: null,
  category: [],
  subCategory: [],
};
export default AddSKUModal;
