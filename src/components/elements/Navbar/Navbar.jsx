import React from "react";
import { Link } from "react-scroll";
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import logoBoxOfficeWhite from '../../../assets/images/logoBoxOfficeWhite.svg';

const CustomNavbar = () => {
  return(
    <Navbar style={{background: "#192A55"}} expand="lg" className="p-4">
      <Container>
        <Navbar.Brand href="#home">
          <img className="img-fluid" src={logoBoxOfficeWhite} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" style={{}}/>
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav className="me-auto" className="mr-3 ml-3" style={{color: "#ffffff"}}>
            {/* <Nav.Link href="#home" smooth className="mr-3 ml-3" style={{color: "#ffffff"}}>Home</Nav.Link>
            <Nav.Link href="#service" smooth className="mr-3 ml-3" style={{color: "#ffffff"}}>Service</Nav.Link>
            <Nav.Link href="#testimoni" smooth className="mr-3 ml-3" style={{color: "#ffffff"}}>Testimoni</Nav.Link>
            <Nav.Link href="#blog" smooth className="mr-3 ml-3" style={{color: "#ffffff"}}>Blog</Nav.Link> */}
            <Link to="home" smooth={true} className="mr-3 ml-3" style={{color: "#ffffff", cursor: "pointer", textDecoration: "none"}}>Home</Link>
            <Link to="service" smooth={true} className="mr-3 ml-3" style={{color: "#ffffff", cursor: "pointer", textDecoration: "none"}}>Service</Link>
            <Link to="testimoni" smooth={true} className="mr-3 ml-3" style={{color: "#ffffff", cursor: "pointer", textDecoration: "none"}}>Testimoni</Link>
            <Link to="blog" smooth={true} className="mr-3 ml-3" style={{color: "#ffffff", cursor: "pointer", textDecoration: "none"}}>Blog</Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default CustomNavbar