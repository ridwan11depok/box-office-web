import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    color: '#1c1c1c',
    fontFamily: 'Work Sans',
    fontSize: '18px',
    fontWeight: '700',
    display: ' flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 0,
  },
  arrowLeft: {
    color: '#1c1c1c',
    borderRadius: '4px',
    backgroundColor: '#f2f2f2',
    marginRight: ' 10px',
    width: '40px',
    height: ' 40px',
    display: ' flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  },
});
function BackButton(props) {
  const classes = useStyles();
  return (
    <h1 className={classes.root}>
      <span onClick={props.onClick} className={classes.arrowLeft}>
        <i className="fas fa-arrow-left" style={{ fontSize: '20px' }}></i>
      </span>
      {props.label}
    </h1>
  );
}

BackButton.defaultProps = {
  label: '',
  onClick: () => {},
};

BackButton.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
};

export default BackButton;
