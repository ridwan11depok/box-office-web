import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '../Button';

const useStyles = makeStyles({
  root: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#1c1c1c',
    fontWeight: '400',
  },
  blue: {
    borderLeft: '10px solid #8DA4DD',
    backgroundColor: '#EBF0FA',
  },
  green: {
    borderLeft: '10px solid #1B5E20',
    backgroundColor: '#EDF7EE',
  },
  yellow: {
    borderLeft: '10px solid #F57F17',
    backgroundColor: '#FFFDE7',
  },
  red: {
    borderLeft: '10px solid #DA101A',
    backgroundColor: '#FDE7E8',
  },
  grey: {
    borderLeft: '10px solid #828282',
    backgroundColor: '#F2F2F2',
  },
  text: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    color: '#1c1c1c',
    fontWeight: '400',
    marginBottom: '0px',
  },
});

const InfoIcon = () => {
  return (
    <div>
      <svg
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12C21.9939 17.5203 17.5203 21.9939 12 22ZM4 12.172C4.04732 16.5732 7.64111 20.1095 12.0425 20.086C16.444 20.0622 19.9995 16.4875 19.9995 12.086C19.9995 7.68451 16.444 4.10977 12.0425 4.086C7.64111 4.06246 4.04732 7.59876 4 12V12.172ZM14 17H11V13H10V11H13V15H14V17ZM13 9H11V7H13V9Z"
          fill="#4F4F4F"
        />
      </svg>
    </div>
  );
};

function Banner(props) {
  const { styleType, className, style, description, button, infoIcon } = props;
  const classes = useStyles();
  return (
    <div
      className={`rounded p-3 ${classes.root} ${classes[styleType]} ${className}`}
      style={style}
    >
      {props.children ? (
        props.children
      ) : (
        <div className="d-flex flex-row">
          {infoIcon && (
            <div className="mr-2">
              <InfoIcon />
            </div>
          )}
          <div>
            <h6 className={classes.text}>{description}</h6>
            {button.isShow && (
              <Button style={{ marginTop: '20px' }} {...button} />
            )}
          </div>
        </div>
      )}
    </div>
  );
}

Banner.defaultProps = {
  styleType: 'blue',
  description: 'description',
  className: '',
  style: {},
  button: {
    isShow: true,
    styleType: 'blueFill',
    text: 'text button',
  },
  infoIcon: false,
};

export default Banner;
