import React from 'react';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';

const exampleMapStyles = [
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        color: 'transparent',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
];

const label = (params) => {
  return (
    <div>labelll</div>
  )
}


const MapComponent = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `480px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  return (
    <GoogleMap
      // options={{ styles: exampleMapStyles }}
      // defaultOptions={{
      //   styles: exampleMapStyles,
      // }}
      defaultZoom={8}
      defaultCenter={
        props.defaultCenter ? props.defaultCenter : { lat: -6.2, lng: 106.816666 }
      } //coordinate Jakarta =>{ lat: -6.2, lng: 106.816666 }
      onClick={(e) =>
        props.onMapClick({ lat: e.latLng.lat(), lng: e.latLng.lng() })
      }
    >
      {props.isMarkerShown && (
        <>
          {props.listCoordinate.map((item) => {
            let selectedWarehouse = null
            if (props?.selectedCoordinate && props?.data) {
              const found = props?.data?.find((warehouse) => {
                return (warehouse?.latitude === props?.selectedCoordinate?.lat && warehouse?.longitude === props?.selectedCoordinate?.lng)
              })
              selectedWarehouse = found
            }
            return (
              <Marker onClick={(e) => props.onMarkerClick({ lat: e.latLng.lat(), lng: e.latLng.lng() })} defaultLabel={label} position={item} >
                {props?.selectedCoordinate && item.lng === props?.selectedCoordinate?.lng && item.lat === props?.selectedCoordinate?.lat && props?.data &&
                  <InfoWindow
                    onCloseClick={props?.onInfoWindowClose}

                  >
                    <div className='d-flex flex-col align-items-center justify-content-center'>
                      <span>{selectedWarehouse?.name || 'No Data'}</span>
                      <span className='mt-1'>{selectedWarehouse?.address || 'No Data'}</span>
                      <span className='mt-1'>{`
                    ${selectedWarehouse?.district?.name ? selectedWarehouse?.district?.name + ', ' : ''} 
                    ${selectedWarehouse?.city?.name ? selectedWarehouse?.city?.name + ', ' : ''} 
                    ${selectedWarehouse?.province?.name ? selectedWarehouse?.province?.name : ''}`}</span>
                    </div>
                  </InfoWindow>
                }
              </Marker>
            )
          })}
        </>
      )}
    </GoogleMap>
  )
});

class Maps extends React.PureComponent {
  state = {
    isMarkerShown: true,
    selectedCoordinate: this.props.initialCoordinate,
  };

  componentDidMount() {
    this.delayedShowMarker();
  }

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true });
    }, 3000);
  };

  handleMarkerClick = (data) => {
    if (this.props.mode === 'viewer') {
      this.setState({ selectedCoordinate: data });
    } else {

      this.setState({ isMarkerShown: false });
      this.delayedShowMarker();
    }
  };

  handleInfoWindowClose = () => {
    this.setState({ selectedCoordinate: null })
  }

  handleMapClick = (coordinate) => {
    this.props.onClickMap({
      lat: coordinate.lat,
      lng: coordinate.lng,
    });
    if (this.props.mode === 'select') {
      this.setState({
        selectedCoordinate: coordinate,
      });
    }

    // const newCoordinateList = this.state.listCoordinate.filter(
    //   (item) => item.lat === coordinate.lat && item.lng === coordinate.lng
    // );
    // if (newCoordinateList.length) {
    //   this.setState({
    //     listCoordinate: this.state.listCoordinate.filter(
    //       (item) => item.lat !== coordinate.lat && item.lng !== coordinate.lng
    //     ),
    //   });
    // } else {
    //   this.setState({
    //     listCoordinate: [...this.state.listCoordinate, coordinate],
    //   });
    // }
  };

  render() {
    return (
        <MapComponent
          isMarkerShown={this.state.isMarkerShown}
          selectedCoordinate={this.state.selectedCoordinate}
          mode={this?.props?.mode}
          onMarkerClick={this.handleMarkerClick}
          onMapClick={this.handleMapClick}
          onInfoWindowClose={this.handleInfoWindowClose}
          listCoordinate={[
            this.state.selectedCoordinate,
            ...this.props.listCoordinate,
          ]}
          className={this.props?.className}
          data={this.props?.data}
          defaultCenter={this.state.selectedCoordinate}
        />
    );
  }
}

Maps.defaultProps = {
  onClickMap: (coordinate) => { },
  listCoordinate: [],
  initialCoordinate: null,
  mode: 'select', //or viewer
};

export default Maps;
