import React, { useState, useEffect } from 'react';
import { Modal } from '../Modal';
import Button from '../Button';
import { useSelector, useDispatch } from 'react-redux';
import Map from './index';

const MapModal = (props) => {
  const [coordinate, setCoordinate] = useState({});
  // const { isLoading } = useSelector((state) => state.loading);
  // const dispatch = useDispatch();
  // const handleSubmit = async () => {
  //   try {
  //     await dispatch(deleteUser(props?.data?.id));
  //     props.onHide();
  //   } catch (err) {
  //     props.onHide();
  //   }
  // };
  const onClickMap = (coordinate) => {
    if (props.mode === 'select') {
      setCoordinate(coordinate);
    }
  };
  useEffect(() => {
    if (!props.show) {
      props.onHide();
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal={props.mode === 'viewer' ? 'Lokasi' : 'Tandai Lokasi'}
        bodyClassName="pb-3 pt-3"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        data={props.data}
        bodyStyle={{ border: 'none' }}
        content={() => (
          <>
            <Map
              onClickMap={onClickMap}
              initialCoordinate={
                props.initialCoordinate && props.initialCoordinate?.lng
                  ? {
                      lng: Number(props.initialCoordinate.lng),
                      lat: Number(props.initialCoordinate.lat),
                    }
                  : null
              }
              mode={props.mode}
            />
          </>
        )}
        footer={() => (
          <div>
            <Button
              style={{ minWidth: '140px' }}
              styleType="lightBlueFill"
              text={props.mode === 'select' ? 'Batal' : 'Kembali'}
              onClick={props.onHide}
            />
            {props.mode === 'select' && (
              <Button
                style={{ minWidth: '140px' }}
                className="ml-3"
                styleType="blueFill"
                text="Pilih"
                onClick={() => props.onAgree(coordinate)}
              />
            )}
          </div>
        )}
      />
    </>
  );
};

MapModal.defaultProps = {
  mode: 'select', //or 'viewer'
};

export default MapModal;
