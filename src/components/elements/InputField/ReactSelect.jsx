import React, { useEffect, useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Select from 'react-select';

const useStyles = makeStyles({
  inputLabel: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  textInputError: {
    color: '#ee1b25',
    fontSize: '12px',
  },
});


const ReactSelect = (props) => {
  const [isFocus, setFocus] = useState(false);
  const {
    label,
    placeholder,
    name,
    formik,
    className,
    withValidate,
    value,
    onChange,
    type,
    options,
    styleSelect,
    errorServer,
    ...rest
  } = props;
  const [valueInput, setValueInput] = useState(value);
  const propsField = withValidate ? formik.getFieldProps(name) : {};
  const classes = useStyles();
  const setBorder = () => {
    if (withValidate) {
      if (isFocus) {
        if (!formik.touched[name]) {
          return '1px solid #6777ef';
        } else if (
          formik.touched[name] &&
          !formik.errors[name] &&
          !errorServer
        ) {
          return '1px solid #6777ef';
        } else {
          return '1px solid #ee1b25';
        }
      } else if ((formik.touched[name] && formik.errors[name]) || errorServer) {
        return '1px solid #ee1b25';
      } else {
        return '1px solid #1C1C1C';
      }
    } else {
      if (isFocus) {
        return '1px solid #6777ef';
      } else {
        return '1px solid #1C1C1C';
      }
    }
  };
  const styles = {
    control: (css) => ({
      ...css,
      backgroundColor: 'transparent',
      border:
        withValidate && formik.touched[name] && formik.errors[name]
          ? '1px solid #ee1b25'
          : '1px solid #1C1C1C',
      ...styleSelect,
    }),
  };
  return (
    <>
      <div className={`${className}`} controlId={name}>
        <Form.Group controlId={name} className="mb-0">
          {label && (
            <Form.Label as={'h6'} className={classes.inputLabel}>
              {label}
            </Form.Label>
          )}
          <Select
            placeholder={placeholder}
            options={options}
            styles={styles}
            onChange={(val) => {
              const e = { target: { value: val.value } };
              withValidate && formik.setFieldValue(`${name}`, val.value);
              console.log('val', val);
              setValueInput(val);
              onChange(e);
            }}
            value={valueInput}
            onBlur={(e) => {
              setFocus(false);
              withValidate && formik.setFieldTouched(`${name}`, true);
            }}
            noOptionsMessage={({inputValue}) => !inputValue ? 'No Options' : "No results found"} 
            // onFocus={() => setFocus(true)}
          />

          {withValidate && formik.touched[name] && formik.errors[name] ? (
            <Form.Text className={classes.textInputError}>
              {formik.errors[name]}
            </Form.Text>
          ) : errorServer ? (
            <Form.Text className={classes.textInputError}>
              {errorServer}
            </Form.Text>
          ) : null}
        </Form.Group>
      </div>
    </>
  );
};

ReactSelect.defaultProps = {
  label: '',
  placeholder: '',
  name: '',
  formik: {},
  className: '',
  withValidate: false, //if true, you must insert formik props
  value: '',
  onChange: () => {},
  type: 'text',
  styleSelect: {},
  errorServer: '',
};

ReactSelect.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  formik: PropTypes.object,
  className: PropTypes.string,
  withValidate: PropTypes.bool, //if true, you must insert formik props
  value: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  options: PropTypes.array,
  styleSelect: PropTypes.object,
  errorServer: PropTypes.string,
};
export default ReactSelect;
