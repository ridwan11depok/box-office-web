import React, { useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  inputLabel: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  textInputError: {
    color: '#ee1b25',
    fontSize: '12px',
  },
});

const SelectField = (props) => {
  const [isFocus, setFocus] = useState(false);
  const {
    label,
    placeholder,
    name,
    formik,
    className,
    withValidate,
    value,
    onChange,
    type,
    options,
    noMarginBottom,
    errorServer,
    ...rest
  } = props;
  const [valueInput, setValueInput] = useState(value);
  const propsField = withValidate ? formik.getFieldProps(name) : {};
  const classes = useStyles();
  const setBorder = () => {
    if (withValidate) {
      if (isFocus) {
        if (!formik.touched[name]) {
          return '1px solid #6777ef';
        } else if (
          formik.touched[name] &&
          !formik.errors[name] &&
          errorServer
        ) {
          return '1px solid #6777ef';
        } else {
          return '1px solid #ee1b25';
        }
      } else if ((formik.touched[name] && formik.errors[name]) || errorServer) {
        return '1px solid #ee1b25';
      } else {
        return '1px solid #1C1C1C';
      }
    } else {
      if (isFocus) {
        return '1px solid #6777ef';
      } else {
        return '1px solid #1C1C1C';
      }
    }
  };
  return (
    <>
      <div className={`${className}`} controlId={name}>
        <Form.Group controlId={name} className={noMarginBottom && 'mb-0'}>
          {label && (
            <Form.Label as={'h6'} className={classes.inputLabel}>
              {label}
            </Form.Label>
          )}
          <InputGroup
            className="rounded"
            style={{
              border: setBorder(),
              height: '40px',
            }}
          >
            <Form.Control
              as="select"
              type={type}
              placeholder={placeholder}
              onFocus={() => setFocus(true)}
              {...propsField}
              onBlur={(e) => {
                setFocus(false);
                withValidate && formik.getFieldProps(name).onBlur(e);
              }}
              style={{
                border: 'none',
                backgroundColor: 'transparent',
                height: '40px',
              }}
              value={
                withValidate ? formik.getFieldProps(name).value : valueInput
              }
              onChange={(e) => {
                withValidate && formik.getFieldProps(name).onChange(e);
                setValueInput(e.target.value);
                onChange(e);
              }}
              {...rest}
            >
              {options.length &&
                options.map((item) => (
                  <option value={item.value}>{item.description}</option>
                ))}
            </Form.Control>
          </InputGroup>

          {withValidate && formik.touched[name] && formik.errors[name] ? (
            <Form.Text className={classes.textInputError}>
              {formik.errors[name]}
            </Form.Text>
          ) : errorServer ? (
            <Form.Text className={classes.textInputError}>
              {errorServer}
            </Form.Text>
          ) : null}
        </Form.Group>
      </div>
    </>
  );
};

SelectField.defaultProps = {
  label: '',
  placeholder: '',
  name: '',
  formik: {},
  className: '',
  withValidate: false, //if true, you must insert formik props
  value: '',
  onChange: () => {},
  type: 'text',
  noMarginBottom: false,
  errorServer: '',
};

SelectField.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  formik: PropTypes.object,
  className: PropTypes.string,
  withValidate: PropTypes.bool, //if true, you must insert formik props
  value: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.string,
  options: PropTypes.array,
  noMarginBottom: PropTypes.bool,
  errorServer: PropTypes.string,
};
export default SelectField;
