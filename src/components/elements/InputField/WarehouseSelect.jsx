import React, { useEffect, useState } from 'react';
import gudang from '../../../assets/icons/profilegudang.svg';
import gudangdropdown from '../../../assets/icons/gudangdropdown.svg';
import OptionWarehouse from './OptionWarehouse';

const dataDummy = [
  { id: 1, name: 'Gudang A', address: 'Bandung, Jawa Barat', status: 'open' },
  { id: 2, name: 'Gudang B', address: 'Cirebon, Jawa Barat', status: 'open' },
  {
    id: 3,
    name: 'Gudang C',
    address: 'Majalengka, Jawa Barat',
    status: 'tutup',
  },
  { id: 4, name: 'Gudang D', address: 'Subang, Jawa Barat', status: 'open' },
  { id: 5, name: 'Gudang E', address: 'Bekasi, Jawa Barat', status: 'tutup' },
];

function WarehouseSelect(props) {
  const { defaultValue, onChange, options, style, placeholder } = props;
  const [state, setState] = useState({
    showModal: false,
  });
  const [selected, setSelected] = useState(defaultValue);
  const onAgree = (data) => {
    setSelected(data);
    onChange(data);
  };

  useEffect(() => {
    if(defaultValue){
      setSelected(defaultValue)
    }
  }, [defaultValue]);


  return (
    <>
      <div
        style={{
          height: '48px',
          border: '1px solid #CFCFCF',
          borderRadius: '4px',
          padding: '3px',
          cursor: 'pointer',
          minWidth: '200px',
          overflow: 'hidden',
          ...style,
        }}
        onClick={() => setState({ ...state, showModal: true })}
      >
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div>
            {selected?.warehouse?.logo_full_url &&
            <img alt={selected?.warehouse?.logo} src={selected?.warehouse?.logo_full_url} width={'40px'} height={'40px'} />
            }
          </div>
          <div
            style={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: selected?.address ? 'space-between' : 'center',
              marginLeft: '3px',
              marginRight: '3px',
            }}
          >
            <h6
              style={{
                fontWeight: '600',
                fontSize: '14px',
                fontFamily: 'Open Sans',
                color: '#1c1c1c',
                marginBottom: '0px',
              }}
            >
              {selected?.name || placeholder.name}
            </h6>
            {selected?.address && (
              <h6
                style={{
                  fontWeight: '600',
                  fontSize: '10px',
                  fontFamily: 'Open Sans',
                  color: '#1c1c1c',
                }}
              >
                {selected?.address}
              </h6>
            )}
          </div>
          <div>
            <img alt="" src={gudangdropdown} />
          </div>
        </div>
      </div>
      <OptionWarehouse
        onHide={() => setState({ ...state, showModal: false })}
        onAgree={onAgree}
        show={state.showModal}
        options={options}
      />
    </>
  );
}

WarehouseSelect.defaultProps = {
  defaultValue: {},
  onChange: (data) => {},
  options: dataDummy,
  style: {},
  placeholder: { id: '', name: 'Pilih gudang', address: '' },
};
export default WarehouseSelect;
