import React, { useState, useEffect } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import CurrencyInput from 'react-currency-input-field';

const useStyles = makeStyles({
  inputLabel: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  textInputError: {
    color: '#ee1b25',
    fontSize: '12px',
  },
});

const TextField = (props) => {
  const [isFocus, setFocus] = useState(false);
  const {
    label,
    placeholder,
    name,
    formik,
    className,
    withValidate,
    value,
    defaultValue,
    onChange,
    onClickPrefix,
    onClickSuffix,
    withError,
    noMarginBottom,
    labelCurrency,
    positionLabelCurrency,
    ...rest
  } = props;
  const [valueInput, setValueInput] = useState(defaultValue);
  // const propsField = withValidate ? formik.getFieldProps(name) : {};
  const classes = useStyles();
  const setBorder = () => {
    if (withValidate) {
      if (isFocus) {
        if (!formik.touched[name]) {
          return '1px solid #6777ef';
        } else if (formik.touched[name] && !formik.errors[name]) {
          return '1px solid #6777ef';
        } else {
          return '1px solid #ee1b25';
        }
      } else if (formik.touched[name] && formik.errors[name]) {
        return '1px solid #ee1b25';
      } else {
        return '1px solid #1C1C1C';
      }
    } else {
      if (isFocus) {
        return '1px solid #6777ef';
      } else {
        return '1px solid #1C1C1C';
      }
    }
  };

  useEffect(() => {
    setValueInput(value);
  }, [value]);

  return (
    <>
      <div className={`${className}`} controlId={name}>
        <Form.Group controlId={name} className={noMarginBottom && 'mb-0'}>
          {label && (
            <Form.Label as={'h6'} className={classes.inputLabel}>
              {label}
            </Form.Label>
          )}
          <InputGroup
            className="rounded"
            style={{
              border: setBorder(),
              height: '40px',
              overflow: 'hidden',
            }}
          >
            {positionLabelCurrency === 'left' && (
              <InputGroup.Append onClick={onClickPrefix}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: '#F2F2F2',
                    color: '#4F4F4F',
                  }}
                  id={name}
                >
                  {labelCurrency}
                </InputGroup.Text>
              </InputGroup.Append>
            )}
            <CurrencyInput
              style={{ border: 'none', backgroundColor: 'transparent' }}
              className="form-control"
              placeholder={placeholder}
              defaultValue={
                // formik?.getFieldProps
                //   ? formik?.getFieldProps[name]?.value
                //   : defaultValue
                defaultValue
              }
              decimalsLimit={2}
              decimalSeparator=","
              groupSeparator="."
              onValueChange={(val) => {
                let currentValue;
                if (val) {
                  currentValue = val.includes(',') ? val.split(',')[0] : val;
                } else {
                  currentValue = '';
                }
                formik?.getFieldProps &&
                  formik?.setFieldValue(name, currentValue);

                setValueInput(currentValue);
                onChange({ target: { value: currentValue } });
              }}
              onBlur={(e) => {
                setFocus(false);
                formik?.getFieldProps && formik?.setFieldTouched(name, true);
              }}
              onFocus={() => setFocus(true)}
              value={
                formik?.getFieldProps
                  ? formik?.getFieldProps(name)?.value
                  : valueInput
              }
              {...rest}
            />
            {positionLabelCurrency === 'right' && (
              <InputGroup.Append onClick={onClickSuffix}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: '#F2F2F2',
                    color: '#4F4F4F',
                  }}
                  id={name}
                >
                  {labelCurrency}
                </InputGroup.Text>
              </InputGroup.Append>
            )}
          </InputGroup>

          {withValidate &&
            withError &&
            formik.touched[name] &&
            formik.errors[name] && (
              <Form.Text className={classes.textInputError}>
                {formik.errors[name]}
              </Form.Text>
            )}
        </Form.Group>
      </div>
    </>
  );
};

TextField.defaultProps = {
  label: '',
  placeholder: '',
  name: '',
  formik: {},
  className: '',
  withValidate: false, //if true, you must insert formik props
  value: '',
  defaultValue: '',
  onChange: () => {},
  withError: true,
  noMarginBottom: false,
  labelCurrency: 'Rp',
  positionLabelCurrency: 'left',
};

TextField.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  formik: PropTypes.object,
  className: PropTypes.string,
  withValidate: PropTypes.bool, //if true, you must insert formik props
  value: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  onClickPrefix: PropTypes.func,
  onClickSuffix: PropTypes.func,
  withError: PropTypes.bool,
  noMarginBottom: PropTypes.bool,
  labelCurrency: PropTypes.string,
  positionLabelCurrency: PropTypes.string,
};
export default TextField;
