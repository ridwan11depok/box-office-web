import React, { useState } from 'react';
import DateMomentUtils from '@date-io/moment'; // choose your lib
import moment from 'moment';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { TextField } from './index';
import localization from 'moment/locale/id';
import { CalendarIcon } from '../Icons';
import CloseIcon from '@material-ui/icons/Close';
import Button from '../Button';

moment.locale('id', localization);

export default function DatePickerField(props) {
  const {
    format,
    onDateChange,
    value,
    prefix,
    prefixIcon,
    suffix,
    suffixIcon,
    views,
    placeholder,
    ...otherProps
  } = props;
  const [selectedDate, handleDateChange] = useState(value);

  const handleSelectDate = (date) => {
    handleDateChange(date);
    onDateChange(date);
  };

  const icons = {
    arrowDown: () => <i className="fa fa-chevron-down" aria-hidden="true"></i>,
    calendar: () => <CalendarIcon />,
  };

  const closeIcon = () => <CloseIcon />;

  const handleClickSuffix = (textFieldProps) => {
    if (selectedDate) {
      handleDateChange(null);
      onDateChange('');
    } else {
      textFieldProps.onClick();
    }
  };
  return (
    <MuiPickersUtilsProvider
      utils={DateMomentUtils}
      libInstance={moment}
      locale="id"
    >
      <DatePicker
        clearable
        value={selectedDate}
        onChange={handleSelectDate}
        format={format}
        views={views}
        style={{ backgroundColor: 'transparent' }}
        TextFieldComponent={(textFieldProps) => {
          return (
            <TextField
              {...textFieldProps}
              {...otherProps}
              prefix={prefix && icons[prefixIcon]}
              placeholder={placeholder}
              suffix={selectedDate ? closeIcon : suffix && icons[suffixIcon]}
              onClickPrefix={textFieldProps.onClick}
              onClickSuffix={() => handleClickSuffix(textFieldProps)}
            />
          );
        }}
        okLabel={
          <h6 style={{ textTransform: 'none', margin: '5px' }}>Pilih</h6>
        }
        cancelLabel={
          <h6 style={{ textTransform: 'none', margin: '5px' }}>Batal</h6>
        }
        clearLabel={
          <h6 style={{ textTransform: 'none', margin: '5px' }}>Hapus</h6>
        }
      />
    </MuiPickersUtilsProvider>
  );
}

DatePickerField.defaultProps = {
  format: 'DD/MM/yyyy',
  value: null,
  onDateChange: (date) => {},
  views: ['date'],
  prefix: false,
  prefixIcon: 'calendar', //arrowDown
  suffix: true,
  suffixIcon: 'arrowDown', //calendar,
  placeholder: 'Pilih tanggal',
};
