import React, { useState, useEffect } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  inputLabel: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
  textInputError: {
    color: '#ee1b25',
    fontSize: '12px',
  },
});

const TextField = (props) => {
  const [isFocus, setFocus] = useState(false);
  const {
    label,
    placeholder,
    name,
    formik,
    className,
    withValidate,
    value,
    defaultValue,
    onChange,
    suffix,
    prefix,
    type,
    onClickPrefix,
    onClickSuffix,
    withError,
    noMarginBottom,
    errorServer,
    ...rest
  } = props;
  const [valueInput, setValueInput] = useState(defaultValue);
  const propsField = withValidate ? formik.getFieldProps(name) : {};
  const classes = useStyles();
  const setBorder = () => {
    if (withValidate) {
      if (isFocus) {
        if (!formik.touched[name]) {
          return '1px solid #6777ef';
        } else if (
          formik.touched[name] &&
          !formik.errors[name] &&
          !errorServer
        ) {
          return '1px solid #6777ef';
        } else {
          return '1px solid #ee1b25';
        }
      } else if ((formik.touched[name] && formik.errors[name]) || errorServer) {
        return '1px solid #ee1b25';
      } else {
        return '1px solid #1C1C1C';
      }
    } else {
      if (isFocus) {
        return '1px solid #6777ef';
      } else {
        return '1px solid #1C1C1C';
      }
    }
  };

  useEffect(() => {
    setValueInput(value);
  }, [value]);

  return (
    <>
      <div className={`${className}`} controlId={name}>
        <Form.Group controlId={name} className={noMarginBottom && 'mb-0'}>
          {label && (
            <Form.Label as={'h6'} className={classes.inputLabel}>
              {label}
            </Form.Label>
          )}
          <InputGroup
            className="rounded"
            style={{
              border: setBorder(),
              height: '40px',
              overflow: 'hidden',
            }}
          >
            {prefix && (
              <InputGroup.Append onClick={onClickPrefix}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: '#F2F2F2',
                    color: '#4F4F4F',
                  }}
                  id={name}
                >
                  {prefix()}
                </InputGroup.Text>
              </InputGroup.Append>
            )}
            <Form.Control
              type={type}
              placeholder={placeholder}
              onFocus={() => setFocus(true)}
              {...propsField}
              onBlur={(e) => {
                setFocus(false);
                formik.getFieldProps && formik.getFieldProps(name).onBlur(e);
              }}
              style={{
                border: 'none',
                backgroundColor: 'transparent',
                height: '40px',
                paddingLeft: prefix && 0,
                paddingRight: suffix && 0,
                paddingLeft: prefix && '3px',
              }}
              value={
                formik?.getFieldProps
                  ? formik?.getFieldProps(name)?.value
                  : valueInput
              }
              onChange={(e) => {
                let value = e.target.value;
                formik.getFieldProps && formik.getFieldProps(name).onChange(e);

                setValueInput(value);
                onChange(e);
              }}
              {...rest}
            />
            {suffix && (
              <InputGroup.Append onClick={onClickSuffix}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: '#F2F2F2',
                    color: '#4F4F4F',
                  }}
                  id={name}
                >
                  {suffix()}
                </InputGroup.Text>
              </InputGroup.Append>
            )}
          </InputGroup>

          {withValidate &&
          withError &&
          formik.touched[name] &&
          formik.errors[name] ? (
            <Form.Text className={classes.textInputError}>
              {formik.errors[name]}
            </Form.Text>
          ) : errorServer ? (
            <Form.Text className={classes.textInputError}>
              {errorServer}
            </Form.Text>
          ) : null}
        </Form.Group>
      </div>
    </>
  );
};

TextField.defaultProps = {
  label: '',
  placeholder: '',
  name: '',
  formik: {},
  className: '',
  withValidate: false, //if true, you must insert formik props
  value: '',
  defaultValue: '',
  onChange: () => {},
  suffix: null,
  prefix: null,
  type: 'text',
  onClickPrefix: () => {},
  onClickSuffix: () => {},
  withError: true,
  noMarginBottom: false,
  errorServer: '',
};

TextField.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  formik: PropTypes.object,
  className: PropTypes.string,
  withValidate: PropTypes.bool, //if true, you must insert formik props
  value: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  suffix: PropTypes.func,
  prefix: PropTypes.func,
  type: PropTypes.string,
  onClickPrefix: PropTypes.func,
  onClickSuffix: PropTypes.func,
  withError: PropTypes.bool,
  noMarginBottom: PropTypes.bool,
  errorServer: PropTypes.string,
};
export default TextField;
