import React, { useState, useEffect } from 'react';
import Modal from '../Modal/Modal';
import Button from '../Button';
import gudang from '../../../assets/icons/profilegudang.svg';
import { ContentContainer, ContentItem } from '../BaseContent';
import Badge from '../Badge';

const OptionWarehouseModal = (props) => {
  const [state, setState] = useState({ selected: null });
  const handleSelected = (selected) => {
    setState({ ...state, selected });
  };
  const handleClickBtn = () => {
    if (state.selected) {
      props.onAgree(state.selected);
    }
    props.onHide();
  };
  useEffect(() => {
    if (!props.show) {
      setState({ ...state, selected: null });
    }
  }, [props.show]);
  return (
    <>
      <Modal
        titleModal="Pilih Gudang"
        bodyClassName="p-2"
        show={props.show}
        onHide={props.onHide}
        sizeModal="md"
        content={(data) => (
          <>
            <h6
              style={{
                fontSize: '14px',
                fontWeight: '400',
                color: '#1c1c1c',
                fontFamily: 'Open Sans',
              }}
            >
              Silahkan piih gudang yang akan dikirim stock barang.
            </h6>
            <ContentContainer withHeader={false}>
              {props.options.map((item) => (
                <ContentItem border={false} col="col-12" spaceBottom={1}>
                  <div
                    style={{
                      height: '48px',
                      border: state.selected
                        ? item.id === state.selected.id
                          ? '2px solid #192A55'
                          : '1px solid #CFCFCF'
                        : '1px solid #CFCFCF',
                      borderRadius: '4px',
                      padding: '3px',
                      cursor: 'pointer',
                    }}
                    onClick={() => handleSelected(item)}
                  >
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                      <div>
                        {item?.warehouse?.logo_full_url ?
                        <img alt={item?.warehouse?.logo} src={item?.warehouse?.logo_full_url} width={'40px'} height={'40px'} />
                        :
                        <img style={{ width: '40px', height: '40px' }} src={'https://my-goodlife.com/img.php?imgsrc=&size=400x400'} alt='Image not availabe' />
                        }
                      </div>
                      <div
                        style={{
                          flex: 1,
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'space-between',
                          marginLeft: '3px',
                          marginRight: '3px',
                        }}
                      >
                        <h6
                          style={{
                            fontWeight: '600',
                            fontSize: '14px',
                            fontFamily: 'Open Sans',
                            color: '#1c1c1c',
                          }}
                        >
                          {item.name.length <= 30
                            ? item.name
                            : `${item.name.substring(0, 30)}...`}
                        </h6>
                        <h6
                          style={{
                            fontWeight: '600',
                            fontSize: '10px',
                            fontFamily: 'Open Sans',
                            color: '#1c1c1c',
                          }}
                        >
                          {item.address.length <= 30
                            ? item.address
                            : `${item.address.substring(0, 30)}...`}
                        </h6>
                      </div>
                      <div className="d-flex align-items-center justify-content-center">
                        <Badge
                          label={
                            item.status === 'open'
                              ? 'Gudang Buka'
                              : 'Sedang Tutup'
                          }
                          styleType={item.status === 'open' ? 'green' : 'red'}
                        />
                      </div>
                    </div>
                  </div>
                </ContentItem>
              ))}
              <ContentItem spaceTop={3} col="col-12" border={false}>
                <Button
                  onClick={handleClickBtn}
                  text="Ganti Gudang"
                  styleType="blueFill"
                  className="w-100"
                />
              </ContentItem>
            </ContentContainer>
          </>
        )}
        showFooter={false}
      />
    </>
  );
};

export default OptionWarehouseModal;
