import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import OptionLogistic from './OptionLogistic';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  value: {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Open Sans',
    color: '#363636',
  },
  containerSelectLogistic: {
    width: '100%',
    height: '48px',
    border: '1px solid #363636',
    borderRadius: '5px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 15px',
    cursor: 'pointer',
    userSelect: 'none',
    msUserSelect: 'none',
    MozUserSelect: 'none',
  },
});

function LogisticSelect(props) {
  const { onSelect, defaultValue, options } = props;
  const [state, setState] = useState({
    showSelectLogistic: false,
  });
  const [selectedLogistic, setSelectedLogistic] = useState(null);
  const classes = useStyles();
  const handleSelect = (selected) => {
    setSelectedLogistic(selected);
    onSelect(selected);
  };
  useEffect(() => {
    setSelectedLogistic(defaultValue);
  }, []);
  // useEffect(() => {
  //   setSelectedLogistic(value);
  // }, [value]);
  return (
    <>
      <div
        onClick={() => setState({ ...state, showSelectLogistic: true })}
        className={classes.containerSelectLogistic}
      >
        {selectedLogistic ? (
          <div>
            <img
              alt="Logo"
              src={selectedLogistic?.logo}
              style={{ height: '15px', margin: '0 10px 0' }}
            />
            <span className={classes.value}>{selectedLogistic?.name}</span>
          </div>
        ) : (
          <span className={classes.value}>Pilih logistik pengiriman</span>
        )}

        <i className="fa fa-chevron-down" aria-hidden="true" />
      </div>
      <OptionLogistic
        show={state.showSelectLogistic}
        onHide={() => setState({ ...state, showSelectLogistic: false })}
        initialValues={selectedLogistic}
        onAgree={handleSelect}
        options={options}
      />
    </>
  );
}

LogisticSelect.defaultProps = {
  onSelect: (selected) => selected,
  defaultValue: null,
  // value: null,
  options: [],
};
LogisticSelect.propTypes = {
  onSelect: PropTypes.func,
  defaultValue: PropTypes.object,
  options: PropTypes.array,
  // value: PropTypes.object,
};
export default LogisticSelect;
