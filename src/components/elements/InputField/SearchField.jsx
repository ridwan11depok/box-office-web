import React, { useState } from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles({
  inputLabel: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '600',
    color: '#1c1c1c',
  },
});

const SearchField = (props) => {
  const [isFocus, setFocus] = useState(false);
  const {
    label,
    placeholder,
    name,
    className,
    value,
    onChange,
    onClickIcon,
    iconPosition,
    onEnter,
    ...rest
  } = props;
  const [valueInput, setValueInput] = useState(value);
  const classes = useStyles();
  const setBorder = () => {
    if (isFocus) {
      return '1px solid #6777ef';
    } else {
      return '1px solid #1C1C1C';
    }
  };
  const handleSearch = (event) => {
    let target = event.target;
    let value = target.value;
    if (event.key === 'Enter') {
      onEnter(value);
    }
  };
  return (
    <>
      <div className={`${className}`} controlId={name}>
        <Form.Group controlId={name} className="mb-0">
          {label && (
            <Form.Label as={'h6'} className={classes.inputLabel}>
              {label}
            </Form.Label>
          )}
          <InputGroup
            className="rounded"
            style={{
              border: setBorder(),
              height: '40px',
              padding: 0,
            }}
          >
            {iconPosition === 'start' && (
              <InputGroup.Append onClick={onClickIcon}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: 'transparent',
                  }}
                  id={name}
                >
                  <SearchIcon style={{ color: '#4F4F4F' }} />
                </InputGroup.Text>
              </InputGroup.Append>
            )}
            <Form.Control
              type="text"
              placeholder={placeholder}
              onFocus={() => setFocus(true)}
              onBlur={(e) => {
                setFocus(false);
              }}
              style={{
                border: 'none',
                backgroundColor: 'transparent',
                height: '40px',
                paddingLeft: iconPosition === 'start' && 0,
                paddingRight: iconPosition === 'end' && 0,
              }}
              value={valueInput}
              onChange={(e) => {
                setValueInput(e.target.value);
                onChange(e);
                if (e.target.value === '') {
                  onEnter(e.target.value);
                }
              }}
              onKeyPress={(e) => handleSearch(e)}
              {...rest}
            />
            {iconPosition === 'end' && (
              <InputGroup.Append onClick={onClickIcon}>
                <InputGroup.Text
                  style={{
                    border: 'none',
                    backgroundColor: 'transparent',
                  }}
                  id={name}
                >
                  <SearchIcon style={{ color: '#4F4F4F' }} />
                </InputGroup.Text>
              </InputGroup.Append>
            )}
          </InputGroup>
        </Form.Group>
      </div>
    </>
  );
};

SearchField.defaultProps = {
  label: '',
  placeholder: '',
  name: '',
  className: '',
  value: '',
  onChange: (e) => {},
  onClickIcon: () => {},
  iconPosition: 'start',
  onEnter: () => {},
};

SearchField.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onClickIcon: PropTypes.func,
  iconPosition: PropTypes.string,
  onEnter: PropTypes.func,
};
export default SearchField;
