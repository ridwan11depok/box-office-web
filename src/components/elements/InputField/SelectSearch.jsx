import React, { useState, useEffect } from 'react';
import { Dropdown, FormControl } from 'react-bootstrap';
import SearchField from './SearchField';

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <div
    // href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
    style={{
      height: '40px',
      backgroundColor: 'transparent',
      color: '#1c1c1c',
      border: '1px solid #1C1C1C',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '10px',
      cursor: 'pointer',
      borderRadius: '4px',
    }}
  >
    <span className="mr-5">{children}</span>
    <span>&#x25bc;</span>
  </div>
));

export default function SelectSearch(props) {
  const { options, onChange, placeholderSearch, placeholderSelect } = props;
  const [selected, setSelected] = useState({
    key: '0',
    text: placeholderSelect,
  });
  const [optionList, setOption] = React.useState(options);
  useEffect(() => {
    onChange(selected);
  }, [selected]);
  return (
    <Dropdown style={{ height: '40px', backgroundColor: 'transparent' }}>
      <Dropdown.Toggle
        as={CustomToggle}
        style={{
          height: '40px',
          backgroundColor: 'transparent',
          color: '#1c1c1c',
        }}
        id="dropdown-custom-components"
      >
        {selected.text}
      </Dropdown.Toggle>

      <Dropdown.Menu style={{ backgroundColor: '#FAFAFF', width: '100%' }}>
        <div className="p-1">
          <SearchField
            className="w-100"
            onChange={(e) => {
              let newOptions = options.filter((item) =>
                item.text.toLowerCase().includes(e.target.value.toLowerCase())
              );
              setOption(newOptions);
            }}
            placeholder={placeholderSearch}
          />
        </div>
        {optionList.map((item, idx) => (
          <>
            <Dropdown.Divider
              style={{ margin: 0, borderTopColor: '#E8E8E8' }}
            />
            <Dropdown.Item
              onClick={() => setSelected(item)}
              eventKey={item.key}
              style={{
                color: '#192A55',
                fontSize: '14px',
                fontFamily: 'Open Sans',
                fontWeight: '700',
              }}
            >
              {item.text}
            </Dropdown.Item>
          </>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
}

SelectSearch.defaultProps = {
  options: [
    { key: '1', text: 'Hijau' },
    { key: '2', text: 'Biru' },
    { key: '3', text: 'Merah' },
  ],
  placeholderSelect: 'Pilih',
  placeholderSearch: 'Search',
  onChange: () => {},
};
