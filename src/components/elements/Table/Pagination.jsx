import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

function Pagination(props) {
  const { activePage, totalPage, changePage } = props;
  const renderPageNumber = () => {
    let startPage = activePage < 5 ? 1 : activePage - 4;
    const endPage = startPage + 4 < totalPage ? startPage + 4 : totalPage;
    const diff = startPage - endPage + 4;
    startPage -= startPage - diff > 0 ? diff : 0;
    const pages = [];
    for (let i = startPage; i <= endPage; i++) {
      pages.push(i);
    }

    return pages.map((page, i) => (
      <button
        className={
          activePage === page
            ? 'pagination-number-active'
            : 'pagination-number-inactive'
        }
        key={i}
        onClick={() => changePage(page)}
      >
        {page}
      </button>
    ));
  };
  return (
    <div className="" elevation={0}>
      <button
        aria-label="Previous Page"
        className={`mr-1 ${
          activePage === 1 ? 'pagination-btn-inactive' : 'pagination-btn-active'
        }`}
        disabled={activePage === 1}
        onClick={() => changePage(activePage - 1)}
        size="small"
      >
        <i class="fas fa-chevron-left"></i>
        <span className="ml-2">Prev</span>
      </button>
      {renderPageNumber()}
      <button
        aria-label="Next Page"
        className={`ml-1 ${
          activePage === totalPage
            ? 'pagination-btn-inactive'
            : 'pagination-btn-active'
        }`}
        disabled={activePage === totalPage}
        onClick={() => changePage(activePage + 1)}
        size="small"
      >
        <span className="mr-2">Next</span>
        <i class="fas fa-chevron-right"></i>
      </button>
    </div>
  );
}

Pagination.propTypes = {
  activePage: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  totalPage: PropTypes.number.isRequired,
};

export default Pagination;
