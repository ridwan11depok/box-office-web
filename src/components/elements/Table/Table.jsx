import React, { useState, useEffect } from 'react';
import Pagination from './Pagination';
import Banner from '../Banner';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useSelector } from 'react-redux';

function Table(props) {
  const {
    data,
    column,
    transformData,
    size: view,
    renderHeader,
    renderFooter,
    sizeOptions,
    withNumber,
    renderEmptyData,
    banner,
    totalPage,
    action,
    params,
    variableTriggerAction,
    showFooter,
    showBorderContainer,
    customHeader = false
  } = props;
  const [state, setState] = useState({ page: 1, size: view });
  const handleChangePage = (page) => {
    setState({ ...state, page });
  };

  useEffect(() => {
    handleChangePage(1);
    action({ ...params, page: 1, per_page: state.size });
  }, [
    state.size,
    ...Object.keys(params).map((key) => params[key]),
    ...variableTriggerAction,
  ]);

  useEffect(() => {
    action({ ...params, page: state.page, per_page: state.size });
  }, [state.page]);

  const { isLoadingTable } = useSelector((state) => state.loading);
  return (
    <div
      className="w-100 rounded mb-3 "
      style={{ border: showBorderContainer && '1px solid #E8E8E8' }}
    >
      {customHeader === true && renderHeader ? renderHeader() : <div
        className="w-100 d-flex flex-row align-items-center mb-0 p-0 w-100"
        style={{ borderBottom: renderHeader && '1px solid #E8E8E8' }}
      >
        {renderHeader && renderHeader()}
      </div>}
      
      <div className="table-responsive mb-5">
        <div className="w-100">{isLoadingTable && <LinearProgress />}</div>
        <table
          className={`table table-striped mt-0 ${banner.isShow && 'mb-0'}`}
        >
          <tr
            className="pt-0 pb-0"
            style={{ borderBottom: '1px solid #E8E8E8' }}
          >
            {withNumber && <th scope="col">No.</th>}
            {column.map((item, idx) => (
              <th key={idx.toString()} scope="col">
                {item.heading}
              </th>
            ))}
          </tr>
          <tbody>
            {data.map(transformData).map((row, idx) => {
              return (
                <tr
                  key={idx.toString()}
                  style={{ borderBottom: '1px solid #E8E8E8' }}
                >
                  {withNumber && (
                    <td scope="row">
                      {(state.page - 1) * state.size + (idx + 1)}
                    </td>
                  )}
                  {column.map((col, idx) => {
                    return col.render ? (
                      <td>{col.render(row, idx)}</td>
                    ) : (
                      <td key={idx.toString()}>{row[col.key]}</td>
                    )
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        {banner.isShow && (
          <Banner
            description={banner?.info || 'No information available'}
            button={{ isShow: false }}
            className="w-100"
            styleType="yellow"
          />
        )}
        {data.length === 0 && renderEmptyData()}
      </div>
      <div
        className="w-100 d-flex flex-row align-items-center mb-0 p-0 w-100"
        style={{ borderTop: renderFooter && '1px solid #E8E8E8' }}
      >
        {renderFooter && renderFooter()}
      </div>
      {showFooter && (
        <div
          className="d-flex flex-md-row flex-column justify-content-between p-3 align-items-md-center align-items-start w-100"
          style={{
            borderTop: '1px solid #E8E8E8',
            width: '100%',
          }}
        >
          <div className="d-flex flex-row justify-content-start align-items-center w-70 mb-md-0 mb-1">
            <span className="mr-2 view">View</span>
            <span style={{ width: 'max-content' }}>
              <select
                className="form-control text-center"
                id="exampleFormControlSelect1"
                defaultValue={state.size}
                onChange={(e) =>
                  setState({ ...state, size: Number(e.target.value), page: 1 })
                }
              >
                {sizeOptions.map((sizeItem, idx) => (
                  <option key={idx.toString()} className="text-center">
                    {sizeItem}
                  </option>
                ))}
              </select>
            </span>
            <span className="ml-2 view">data per page</span>
          </div>
          <div className="w-30">
            <Pagination
              activePage={state.page}
              changePage={handleChangePage}
              totalPage={totalPage}
            />
          </div>
        </div>
      )}
    </div>
  );
}

Table.defaultProps = {
  data: [],
  column: [],
  transformData: (item) => item,
  size: 10,
  sizeOptions: [5, 10, 20],
  renderHeader: null,
  renderFooter: null,
  withNumber: true,
  renderEmptyData: () => { },
  banner: { isShow: false, info: '' },
  totalPage: 5,
  action: () => { },
  params: { search: '', name: '' },
  variableTriggerAction: [],
  showFooter: true,
  showBorderContainer: true,
};

export default Table;
