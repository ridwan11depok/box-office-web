import React from 'react';
import Chip from '@material-ui/core/Chip';

const styles = {
  root: {
    fontSize: '10px',
    fontFamily: 'Open Sans',
    margin: 0,
  },
  green: {
    backgroundColor: '#EDF7EE',
    color: '#1B5E20',
  },
  red: {
    backgroundColor: '#FDE7E8',
    color: '#EE1B25',
  },
  black: {
    backgroundColor: '#E8E8E8',
    color: '#1C1C1C',
  },
  yellow: {
    backgroundColor: '#FFFDE7',
    color: '#F57F17',
  },
  blue: {
    backgroundColor: '#EBF0FA',
    color: '#192A55',
  },
};
const Badge = (props) => {
  const { size, label, styleType, style, ...rest } = props;
  return (
    <>
      <Chip
        size={size}
        label={label}
        color=""
        style={{ ...styles.root, ...styles[styleType], ...style }}
        {...rest}
      />
    </>
  );
};

Badge.defaultProps = {
  size: 'small',
  label: 'label',
  styleType: 'green',
  style: {},
};

export default Badge;
