import React from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PropTypes from 'prop-types';
import Button from '../Button';

function Counter(props) {
  const renderTextMin = () => {
    return (
      <span style={{ fontSize: '25px', color: props.value === 1? 'gray' : '' }}>-</span>
    )
  }

  const renderTextPlus = () => {
    return (
      <span style={{ fontSize: '20px', color: props.value === props.max ? 'gray' : '' }}>+</span>
    )
  }

  return (
    <ButtonGroup
      style={{ backgroundColor: 'transparent' }}
      aria-label="outlined primary button group"
    >
      <Button
        onClick={() => {
          if (props.value !== 1 && props.value !== 0) {
            props.onDecrease()
          }
        }
        }
        style={{ backgroundColor: 'transparent' }}
        styleType="blueOutline"
        renderText={renderTextMin}
      />
      <Button
        style={{ backgroundColor: '#FFFFFF' }}
        styleType="blueOutline"
        text={`${props.value}`}
      />
      <Button
        onClick={() => {
          if (props.value !== props.max) {
            props.onIncrease()
          }
        }}
        style={{ backgroundColor: 'transparent' }}
        styleType="blueOutline"
        renderText={renderTextPlus}
      />
    </ButtonGroup>
  );
}

Counter.defaultProps = {
  onDecrease: () => { },
  onIncrease: () => { },
  value: 0,
};

Counter.propTypes = {
  onDecrease: PropTypes.func,
  onIncrease: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default Counter;
