import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  titlePage: {
    color: '#1c1c1c',
    fontFamily: 'Work Sans',
    fontSize: '24px',
    fontWeight: '700',
  },
});
const PageTitle = (props) => {
  const classes = useStyles();
  return (
    <>
      <h1 className={classes.titlePage}>{props.title}</h1>
    </>
  );
};

PageTitle.defaultProps = {
  title: '',
};

PageTitle.propTypes = {
  title: PropTypes.string,
};

export default PageTitle;
