import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx'

const useStyles = makeStyles({
  titlePage: {
    color: '#1c1c1c',
    fontFamily: 'Work Sans',
    fontSize: '24px',
    fontWeight: '700',
  },
});
const ContentContainer = (props) => {
  const { classname } = props 
  const classes = useStyles();
  return (
    <>
      {props.withHeader ? (
        <>
          <div className="row no-gutters" >
            <div className="col-12 p-0">
              {props.renderHeader ? (
                props.renderHeader()
              ) : (
                <h1 className={classes.titlePage}>{props.title}</h1>
              )}
            </div>
          </div>
          <div className={clsx("row no-gutters mt-3", classname)} >{props.children}</div>
        </>
      ) : (
        <div className="row no-gutters" >{props.children}</div>
      )}
    </>
  );
};

ContentContainer.defaultProps = {
  title: '',
  renderHeader: null,
  withHeader: true,
};

ContentContainer.propTypes = {
  title: PropTypes.string,
  renderHeader: PropTypes.func,
  withHeader: PropTypes.bool,
};

export default ContentContainer;
