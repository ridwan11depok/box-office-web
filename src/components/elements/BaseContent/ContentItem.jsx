import React from 'react';
import PropTypes from 'prop-types';

const ContentItem = (props) => {
  const {
    col,
    className,
    style,
    children,
    spaceLeft,
    spaceRight,
    spaceTop,
    spaceBottom,
    border,
    container,
    item,
  } = props;
  return (
    <>
      {item ? (
        <div
          className={`rounded w-100 ${className}`}
          style={{
            border: border && '1px solid #E8E8E8',
            width: '100%',
            ...style,
          }}
        >
          {children}
        </div>
      ) : (
        <div
          className={`${col} pl-${spaceLeft} pr-${spaceRight} pt-${spaceTop} pb-${spaceBottom}`}
        >
          <div
            className={`rounded w-100 ${className} ${
              container ? 'd-flex flex-row' : ''
            }`}
            style={{
              border: border && '1px solid #E8E8E8',
              width: '100%',
              wordWrap: 'break-word',
              ...style,
            }}
          >
            {children}
          </div>
        </div>
      )}
    </>
  );
};

ContentItem.defaultProps = {
  col: 'col-12',
  className: '',
  style: {},
  spaceLeft: 0, //number or (sm-1, md-0 and other)
  spaceRight: 0, //number or (sm-1, md-0 and other)
  spaceTop: 0, //number or (sm-1, md-0 and other)
  spaceBottom: 0, //number or (sm-1, md-0 and other)
  border: true,
  container: false,
  item: false,
};

ContentItem.propTypes = {
  col: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node.isRequired,
  spaceLeft: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  spaceRight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  spaceTop: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  spaceBottom: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  border: PropTypes.bool,
  container: PropTypes.bool,
  item: PropTypes.bool,
};

export default ContentItem;
