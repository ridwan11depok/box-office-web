import React from 'react';
import { ContentContainer, ContentItem } from '../BaseContent';
import pickupOrder from '../../../assets/images/pickupOrder.svg';
import Button from '../Button';
import BackButton from '../BackButton';
import { useHistory } from 'react-router-dom';

function FeedbackPage(props) {
  const { feedbackType, onClick } = props;
  const title = {
    pickupStock: 'Stock dalam penjemputan',
    sendStock: 'Berhasil mengirim stock',
    warehouse: 'Anda belum memiliki gudang',
  };
  const description = {
    pickupStock:
      'Kurir akan menjemput barang sesuai dengan tanggal pickup order yang anda kirimkan',
    sendStock:
      'Stock barang harus dikirim dalam waktu 2x24 jam sehingga pihak gudang dapat mengonfirmasi dan menyimpan di gudang',
    warehouse:
      'Silahkan memilih gudang agar bisa menggunakan fitur "Mutasi Stock"',
  };
  const history = useHistory();
  return (
    <ContentContainer
      withHeader={Boolean(feedbackType !== 'warehouse')}
      renderHeader={() => (
        <div className="d-flex flex-row justify-content-between align-items-center">
          <BackButton
            label="Kembali ke Inbound"
            onClick={() => {
              onClick();
              history.push('/inventory/inbound');
            }}
          />
        </div>
      )}
    >
      <ContentItem
        border={false}
        style={{
          height: '70vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
        }}
      >
        <img src={pickupOrder} alt="" />
        <h6
          style={{
            fontFamily: 'Work Sans',
            fontWeight: '700',
            fontSize: '18px',
            color: '#1c1c1c',
            marginTop: '15px',
            marginBottom: '10px',
          }}
        >
          {title[feedbackType]}
        </h6>
        <h6
          style={{
            fontFamily: 'Open Sans',
            fontWeight: '400',
            fontSize: '14px',
            color: '#4f4f4f',
            marginBottom: '15px',
          }}
        >
          {description[feedbackType]}
        </h6>
        {feedbackType !== 'warehouse' ? (
          <Button
            onClick={() => {
              onClick();
              history.push('/inventory/inbound');
            }}
            styleType="blueFill"
            text="Kembali ke Inbound"
          />
        ) : (
          <Button
            onClick={() => {
              onClick();
              history.push('/inventory/inbound');
            }}
            styleType="blueFill"
            text="Pilih Gudang"
          />
        )}
      </ContentItem>
    </ContentContainer>
  );
}

export default FeedbackPage;
