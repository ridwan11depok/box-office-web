import React from 'react';

function Puzzle(props) {
  const { fill, ...rest } = props;
  return (
    <svg
      width="20"
      height="17"
      viewBox="0 0 20 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...rest}
    >
      <path
        d="M7 16.3408L2 12.3408L7 8.34082V11.3408H20V13.3408H7V16.3408ZM13 8.34082V5.34082H0V3.34082H13V0.34082L18 4.34082L13 8.34082Z"
        fill={fill}
      />
    </svg>
  );
}

Puzzle.defaultProps = {
  fill: '#FAFAFF',
};
export default Puzzle;
