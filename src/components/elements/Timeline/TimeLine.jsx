import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import clsx from 'clsx';
import './style.css';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  contentInactive: {
    width: '100%',
    position: 'relative',
    borderLeft: '2px solid #9C9C9C',
    padding: '0px 20px 10px',
  },
  contentInactiveLast: {
    width: '100%',
    position: 'relative',
    padding: '0px 20px 10px',
  },
  contentActive: {
    width: '100%',
    position: 'relative',
    borderLeft: '2px solid #B4C3E9',
    padding: '0px 20px 10px',
  },
  contentActiveLast: {
    width: '100%',
    position: 'relative',
    padding: '0px 20px 10px',
  },
  containerDescription: {
    display: 'flex',
    flexDirection: 'column',
  },
  dotInactive: {
    height: '16px',
    width: '16px',
    borderRadius: '50%',
    backgroundColor: '#9C9C9C',
    position: 'absolute',
    top: '0px',
    left: '0px',
    transform: 'translateX(-9px)',
  },
  dotInactiveLast: {
    height: '16px',
    width: '16px',
    borderRadius: '50%',
    backgroundColor: '#9C9C9C',
    position: 'absolute',
    top: '0px',
    left: '2px',
    transform: 'translateX(-9px)',
  },
  timeInactive: {
    fontFamily: 'Open Sans',
    fontSize: '12px',
    fontWeight: '400',
    color: '#4F4F4F',
  },
  timeActive: {
    fontFamily: 'Open Sans',
    fontSize: '12px',
    fontWeight: '400',
    color: '#233D7B',
  },
  description: {
    fontFamily: 'Open Sans',
    fontSize: '14px',
    fontWeight: '400',
    color: '#363636',
    margin: '3px 0px',
  },
});

function TimeLine(props) {
  const { data } = props;
  
  const classes = useStyles();
  const renderItem = (item, idx) => (
    <div
      className={idx !== (data.length-1) ? clsx({
        [classes.contentInactive]: idx !== 0,
        [classes.contentActive]: idx === 0,
      }) : clsx({
        [classes.contentInactiveLast]: idx !== 0,
        [classes.contentActiveLast]: idx === 0,
      })}
    >
      <div
        className={idx !== (data.length-1) ? clsx({
          [classes.dotInactive]: idx !== 0,
          'dotActive-timeline': idx === 0,
        }) : clsx({
          [classes.dotInactiveLast]: idx !== 0,
          'dotActive-timeline': idx === 0,
        })}
      />
      <div className={classes.containerDescription}>
        <span
          className={clsx({
            [classes.timeInactive]: idx !== 0,
            [classes.timeActive]: idx === 0,
          })}
        >
          {moment(item.date_time).format('DD MMMM YYYY  HH:MM')}
        </span>
        <span className={classes.description}>
          {item.description ? item.description : item.status}
        </span>
      </div>
    </div>
  );
  return (
    <div className={classes.container}>
      {data.map(renderItem)}
    </div>
  );
}

export default TimeLine;
