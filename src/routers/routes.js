//general
import Login from '../pages/general/Login/index';
import Register from '../pages/general/Register/index';
// import Home from '../pages/general/Home/homepage';
import Home from '../pages/general/Home/new';

//customer
import Transactions from '../pages/customer/Transactions/Transactions';
import Dashboard from '../pages/customer/Dashboard/Dashboard';
import WalletDashboard from '../pages/customer/Dashboard/Wallet';
import TopUpDashboard from '../pages/customer/Dashboard/TopUp';
import Stock from '../pages/customer/Stocks/Stock';
// import Mutasistock from '../pages/customer/Mutasistock/Mutasistock';
import Mutasistockfill from '../pages/customer/Mutasistock/Mutasistockfill';
// import Pilihgudang from '../pages/customer/Mutasistock/Pilihgudang';
import QCMEmpty from '../pages/customer/QC Middle/QCMEmpty';
import QCMFill from '../pages/customer/QC Middle/QCMFill';
import Inbound from '../pages/customer/Inbound/Inbound';
import InboundSend from '../pages/customer/Inbound/InboundSend';
import InboundDetail from '../pages/customer/Inbound/InboundDetail';
import OutboundDispatchOrder from '../pages/customer/Outbound/Cashless/Cashless';
import OutboundCreateDispatchOrder from '../pages/customer/Outbound/Cashless/CreateCashless';
import OutboundDetailDispatchOrder from '../pages/customer/Outbound/Cashless/DetailCashless';
import OutboundRegularTransaction from '../pages/customer/Outbound/TransactionReguler/RegularTransaction';
import OutboundCreateRegularTransaction from '../pages/customer/Outbound/TransactionReguler/CreateRegularTransaction';
import OutboundDetailRegularTransaction from '../pages/customer/Outbound/TransactionReguler/DetailRegularTransaction';
import OutboundDispatchOrderRegular from '../pages/customer/Outbound/DispatchOrder/DispatchOrder';
import OutboundDetailDispatchOrderRegular from '../pages/customer/Outbound/DispatchOrder/DetailDispatchOrder';
import OutboundCreateDispatchOrderRegular from '../pages/customer/Outbound/DispatchOrder/CreateDispatchOrder';

import ReqPrintBarcode from '../pages/customer/Req Print Barcode/ReqPrintBarcode';
import ReqPrintBarcodeFill from '../pages/customer/Req Print Barcode/ReqPrintBarcodeFill';
// import PengajuanReimburse from '../pages/customer/History Reimburse/PengajuanReimburse';
// import HistoryReimburse from '../pages/customer/History Reimburse/HistoryReimburse';
// import CariTransaksi from '../pages/customer/Cari Transaksi/CariTransaksi';
// import CariTransaksiFill from '../pages/customer/Cari Transaksi/CariTransaksiFill';
// import TransaksiDetail from '../pages/customer/Cari Transaksi/TransaksiDetail';
// import SemuaTransaksi from '../pages/customer/PindahGudang/SemuaTransaksi';
// import TransaksiPending from '../pages/customer/PindahGudang/TransaksiPending';
// import TransaksiSent from '../pages/customer/PindahGudang/TransaksiSent';
// import TransaksiDone from '../pages/customer/PindahGudang/TransaksiDone';
import SemuaTransaksiFill from '../pages/customer/PindahGudang/SemuaTransaksiFill';
import Account from '../pages/customer/System Settings/Account';
import Store from '../pages/customer/System Settings/Store';
import Gudang from '../pages/customer/Gudang/Gudang';
import RequestGudang from '../pages/customer/Gudang/RequestGudang';
import RequestPenggunaanGudang from '../pages/customer/Gudang/RequestPenggunaanGudang';
import DispatchOrder from '../pages/customer/Transactions/DispatchOrder';
import Dropship from '../pages/customer/Transactions/Dropship';
import TransaksiRegular from '../pages/customer/Transactions/TransaksiRegular';
import DropshipDetail from '../pages/customer/Transactions/DropshipDetail';
import StoreEdit from '../pages/customer/System Settings/StoreEdit';
import ReportTransactionUser from '../pages/customer/Report/Transaction';
import ReportInvoiceUser from '../pages/customer/Report/Invoice';
import ReportDetailInvoice from '../pages/customer/Report/components/DetailInvoice';

//warehouse
import DashboardWarehouse from '../pages/warehouse/Dashboard/Dashboard';
import ReimburseDashboardWarehouse from '../pages/warehouse/Dashboard/Reimburse';
import InboundWarehouse from '../pages/warehouse/Inbound/Inbound';
import DispatchOrderWarehouse from '../pages/warehouse/DispatchOrder';
import StoreWarehouse from '../pages/warehouse/Store/Store';
import StoreDetailWarehouse from '../pages/warehouse/Store/StoreDetail';
import ManifestWarehouse from '../pages/warehouse/Manifest/Manifest';
import ManifestDetailWarehouse from '../pages/warehouse/Manifest/ManifestDetail';
import TransactionReguler from '../pages/warehouse/TransactionReguler';
import CashlessWarehouse from '../pages/warehouse/Cashless';
import ReportTransaction from '../pages/warehouse/Report/Transaction';
import ReportInvoice from '../pages/warehouse/Report/Invoice';
import InvoiceDetail from '../pages/warehouse/Report/components/InvoiceDetail';

//administrator
import DashboardAdmin from '../pages/administrator/Dashboard/Dashboard';
import GudangAdministrator from '../pages/administrator/Gudang/index';
import AddGudangAdministrator from '../pages/administrator/Gudang/AddWarehouse';
import WarehouseDetailAdministrator from '../pages/administrator/Gudang/WarehouseDetail';
import WarehouseEditAdministrator from '../pages/administrator/Gudang/WarehouseEdit';
import CategoryAdministrator from '../pages/administrator/Kategori/index';
import CategoryDetail from '../pages/administrator/Kategori/CategoryDetail';
import StoreAdministrator from '../pages/administrator/Store';
import AddStoreAdministrator from '../pages/administrator/Store/AddStore';
import StoreDetailAdmininstrator from '../pages/administrator/Store/StoreDetail';
import StoreEditAdmininstrator from '../pages/administrator/Store/StoreEdit';
import Packaging from '../pages/administrator/Packaging/index';

const routes = {
  public: [
    {
      path: '/',
      page: 'Home',
      component: Home,
    },
    {
      path: '/login',
      page: 'Login',
      component: Login,
    },
    {
      path: '/register',
      page: 'Register',
      component: Register,
    },
  ],
  administrator: [
    //Dashboard
    {
      path: '/admin/dashboard',
      page: 'Dashboard',
      component: DashboardAdmin,
      activeMenu: '/admin/dashboard',
    },
    {
      path: '/admin/warehouse',
      page: 'Gudang Administrator',
      component: GudangAdministrator,
      activeMenu: '/admin/warehouse',
    },
    {
      path: '/admin/warehouse/add',
      page: 'Gudang Administrator',
      component: AddGudangAdministrator,
      activeMenu: '/admin/warehouse',
    },
    {
      path: '/admin/warehouse/detail/:id',
      page: 'Detail Gudang',
      component: WarehouseDetailAdministrator,
      activeMenu: '/admin/warehouse',
    },
    {
      path: '/admin/warehouse/edit/:id',
      page: 'Edit Gudang',
      component: WarehouseEditAdministrator,
      activeMenu: '/admin/warehouse',
    },

    {
      path: '/admin/category',
      page: 'Category Administrator',
      component: CategoryAdministrator,
      activeMenu: '/admin/category',
    },
    {
      path: '/admin/category/detail/:id',
      page: 'Detail Category',
      component: CategoryDetail,
      activeMenu: '/admin/category',
    },
    {
      path: '/admin/store',
      page: 'Store',
      component: StoreAdministrator,
      activeMenu: '/admin/store',
    },
    {
      path: '/admin/store/add',
      page: 'Tambah Store',
      component: AddStoreAdministrator,
      activeMenu: '/admin/store',
    },
    {
      path: `/admin/store/detail/:id`,
      page: 'Detail Store',
      component: StoreDetailAdmininstrator,
      activeMenu: '/admin/store',
    },
    {
      path: `/admin/store/edit/:id`,
      page: 'Edit Store',
      component: StoreEditAdmininstrator,
      activeMenu: '/admin/store',
    },

    {
      path: `/admin/packaging`,
      page: 'Packaging',
      component: Packaging,
      activeMenu: '/admin/packaging',
    },
  ],
  warehouse: [
    {
      path: '/warehouse/dashboard',
      page: 'Dashboard',
      component: DashboardWarehouse,
      activeMenu: '/warehouse/dashboard',
    },
    {
      path: '/warehouse/dashboard/reimburse',
      page: 'Reimburse',
      component: ReimburseDashboardWarehouse,
      activeMenu: '/warehouse/dashboard',
    },

    {
      path: '/warehouse/inbound',
      page: 'Inbound',
      component: InboundWarehouse,
      activeMenu: '/warehouse/inbound',
    },
    {
      path: '/warehouse/outbound/regular-transaction',
      page: 'Transaction Reguler',
      component: TransactionReguler,
      activeMenu: '/warehouse/outbound/regular-transaction',
    },
    {
      path: '/warehouse/outbound/regular-transaction/:process/:id',
      page: 'Transaction Reguler',
      component: TransactionReguler,
      activeMenu: '/warehouse/outbound/regular-transaction',
    },
    {
      path: '/warehouse/outbound/dispatch-order',
      page: 'Dispatch Order',
      component: DispatchOrderWarehouse,
      activeMenu: '/warehouse/outbound/dispatch-order',
    },
    {
      path: '/warehouse/outbound/dispatch-order/:process/:id',
      page: 'Dispatch Order',
      component: DispatchOrderWarehouse,
      activeMenu: '/warehouse/outbound/dispatch-order',
    },
    {
      path: '/warehouse/outbound/cashless',
      page: 'Cashless',
      component: CashlessWarehouse,
      activeMenu: '/warehouse/outbound/cashless',
    },
    {
      path: '/warehouse/outbound/cashless/:process/:id',
      page: 'Cashless',
      component: CashlessWarehouse,
      activeMenu: '/warehouse/outbound/cashless',
    },
    {
      path: '/warehouse/store',
      page: 'Store',
      component: StoreWarehouse,
      activeMenu: '/warehouse/store',
    },
    {
      path: '/warehouse/store/:id',
      page: 'Store Detail',
      component: StoreDetailWarehouse,
      activeMenu: '/warehouse/store',
    },
    {
      path: '/warehouse/manifest',
      page: 'Manifest',
      component: ManifestWarehouse,
      activeMenu: '/warehouse/manifest',
    },
    {
      path: '/warehouse/manifest/:id',
      page: 'Manifest Detail',
      component: ManifestDetailWarehouse,
      activeMenu: '/warehouse/manifest',
    },
    {
      path: '/warehouse/report/transaction',
      page: 'Transaction',
      component: ReportTransaction,
      activeMenu: '/warehouse/report/transaction',
    },
    {
      path: '/warehouse/report/invoice',
      page: 'Invoice',
      component: ReportInvoice,
      activeMenu: '/warehouse/report/invoice',
    },
    {
      path: `/warehouse/report/invoice/detail/:id`,
      page: 'Detail Store',
      component: InvoiceDetail,
      activeMenu: '/warehouse/report/invoice',
    },
  ],
  customer: [
    //Dashboard
    {
      path: '/dashboard',
      page: 'Dashboard',
      component: Dashboard,
      activeMenu: '/dashboard',
    },
    {
      path: '/dashboard/wallet',
      page: 'Dompet',
      component: WalletDashboard,
      activeMenu: '/dashboard',
    },
    {
      path: '/dashboard/topup',
      page: 'Top Up',
      component: TopUpDashboard,
      activeMenu: '/dashboard',
    },

    //Transactions
    {
      path: '/transactions',
      page: 'Transaksi',
      component: Transactions,
      activeMenu: '/transactions',
    },
    {
      path: '/transactions/dispatch-order',
      page: 'Dispatch Order',
      component: DispatchOrder,
      activeMenu: '/transactions',
    },
    {
      path: '/transactions/dropship',
      page: 'Dropship',
      component: Dropship,
      activeMenu: '/transactions',
    },
    {
      path: '/transactions/transaksi-regular',
      page: 'Transaksi Regular',
      component: TransaksiRegular,
      activeMenu: '/transactions',
    },
    {
      path: '/transactions/dropship-detail',
      page: 'Dropship Detail',
      component: DropshipDetail,
      activeMenu: '/transactions',
    },
    //Inventory
    {
      path: '/inventory/stock',
      page: 'Stock',
      component: Stock,
      activeMenu: '/inventory/stock',
    },
    {
      path: '/inbound',
      page: 'Inbound',
      component: Inbound,
      activeMenu: '/inbound',
    },
    {
      path: '/inbound/send',
      page: 'Kirim Stock',
      component: InboundSend,
      activeMenu: '/inbound',
    },
    {
      path: '/inbound/detail/:id',
      page: 'Detail Inbound',
      component: InboundDetail,
      activeMenu: '/inbound',
    },
    {
      path: '/inventory/req-print-barcode',
      page: 'Request Print Barcode',
      component: ReqPrintBarcodeFill,
      activeMenu: '/inventory/req-print-barcode',
    },
    {
      path: '/inventory/mutasistock',
      page: 'Mutasi Stock',
      component: Mutasistockfill,
      activeMenu: '/inventory/mutasistock',
    },
    {
      path: '/inventory/qcm',
      page: 'QCM Middle',
      component: QCMFill,
      activeMenu: '/inventory/qcm',
    },
    //Outbound
    {
      path: '/outbound/cashless',
      page: 'Cashless',
      component: OutboundDispatchOrder,
      activeMenu: '/outbound/cashless',
    },
    {
      path: '/outbound/cashless/add',
      page: 'Add Cashless',
      component: OutboundCreateDispatchOrder,
      activeMenu: '/outbound/cashless',
    },
    {
      path: '/outbound/cashless/detail/:id',
      page: 'Detail Cashless',
      component: OutboundDetailDispatchOrder,
      activeMenu: '/outbound/cashless',
    },
    {
      path: '/outbound/regular-transaction',
      page: 'Transaksi Regular',
      component: OutboundRegularTransaction,
      activeMenu: '/outbound/regular-transaction',
    },
    {
      path: '/outbound/regular-transaction/add',
      page: 'Transaksi Regular',
      component: OutboundCreateRegularTransaction,
      activeMenu: '/outbound/regular-transaction',
    },
    {
      path: '/outbound/regular-transaction/detail/:id',
      page: 'Detail Transaksi Regular',
      component: OutboundDetailRegularTransaction,
      activeMenu: '/outbound/regular-transaction',
    },
    {
      path: '/outbound/dispatch-order',
      page: 'Dispatch Order',
      component: OutboundDispatchOrderRegular,
      activeMenu: '/outbound/dispatch-order',
    },
    {
      path: '/outbound/dispatch-order/detail/:id',
      page: 'Detail Dispatch Order',
      component: OutboundDetailDispatchOrderRegular,
      activeMenu: '/outbound/dispatch-order',
    },
    {
      path: '/outbound/dispatch-order/add',
      page: 'Add Dispatch Order',
      component: OutboundCreateDispatchOrderRegular,
      activeMenu: '/outbound/dispatch-order',
    },
    {
      path: '/gudang',
      page: 'Gudang',
      component: Gudang,
      activeMenu: '/gudang',
    },
    {
      path: '/gudang/request-gudang/:id',
      page: 'Request Gudang',
      component: RequestGudang,
      activeMenu: '/gudang',
    },
    {
      path: '/gudang/request-penggunaan-gudang',
      page: 'Request Penggunaan Gudang',
      component: RequestPenggunaanGudang,
      activeMenu: '/gudang',
    },
    //Pindah Gudang
    {
      path: '/pindah-gudang',
      page: 'Pindah Gudang',
      component: SemuaTransaksiFill,
      activeMenu: '/pindah-gudang',
    },
    //System Settings
    {
      path: '/system-settings/akun',
      page: 'Akun',
      component: Account,
      activeMenu: '/system-settings/akun',
    },
    {
      path: '/system-settings/store',
      page: 'Store',
      component: Store,
      activeMenu: '/system-settings/store',
    },
    {
      path: '/system-settings/store/edit',
      page: 'Edit Store',
      component: StoreEdit,
      activeMenu: '/system-settings/store',
    },
    //Report
    {
      path: '/report/transaction',
      page: 'Transaksi',
      component: ReportTransactionUser,
      activeMenu: '/report/transaction',
    },
    {
      path: '/report/invoice',
      page: 'Invoice',
      component: ReportInvoiceUser,
      activeMenu: '/report/invoice',
    },
    {
      path: '/report/invoice/detail/:id',
      page: 'Invoice',
      component: ReportDetailInvoice,
      activeMenu: '/report/invoice',
    },
    // {
    //   path: '/report/cari-transaksi',
    //   page: 'Cari Transaksi',
    //   component: CariTransaksi,
    //   activeMenu: '/report/cari-transaksi',
    // },
    // {
    //   path: '/report/history-reimburse',
    //   page: 'History Reimburse',
    //   component: HistoryReimburse,
    //   activeMenu: '/report/history-reimburse',
    // },
    //others
    // {
    //   path: '/mutasistock',
    //   page: 'Mutasi Stock',
    //   component: Mutasistock,
    // },

    // {
    //   path: '/mutasistock/pilih-gudang',
    //   page: 'Mutasi Stock Pilih Gudang',
    //   component: Pilihgudang,
    // },
    // {
    //   path: '/qcm-empty',
    //   page: 'QCM Empty',
    //   component: QCMEmpty,
    // },

    // {
    //   path: '/req-print-barcode',
    //   page: 'Request Print Barcode',
    //   component: ReqPrintBarcodeFill,
    // },
    // {
    //   path: '/pengajuan-reimburse',
    //   page: 'Pengajuan Reimburse',
    //   component: PengajuanReimburse,
    // },
    // {
    //   path: '/cari-transaksi-fill',
    //   page: 'Cari Transaksi Fill',
    //   component: CariTransaksiFill,
    // },
    // {
    //   path: '/transaksi-detail',
    //   page: 'Transaksi Detail',
    //   component: TransaksiDetail,
    // },
    // {
    //   path: '/semua-transaksi',
    //   page: 'Semua Transaksi',
    //   component: SemuaTransaksi,
    // },
    // {
    //   path: '/transaksi-pending',
    //   page: 'Transaksi Pending',
    //   component: TransaksiPending,
    // },
    // {
    //   path: '/transaksi-sent',
    //   page: 'Transaksi Sent',
    //   component: TransaksiSent,
    // },
    // {
    //   path: '/transaksi-done',
    //   page: 'Transaksi Done',
    //   component: TransaksiDone,
    // },

    // {
    //   path: '/dashboard/ecommerce',
    //   page: 'Dashboard Ecommerce',
    //   component: EcommerceDashboard,
    // },
    // {
    //   path: '/dashboard/general',
    //   page: 'Dashboard General',
    //   component: GeneralDashboard,
    //   activeMenu:'/gudang',
    // },
  ],
};

let publicRoute = routes.public.map((item) => ({ [item.path]: item.page }));
let administratorRoute = routes.administrator.map((item) => ({
  [item.path]: item.page,
}));
let warehouseRoute = routes.warehouse.map((item) => ({
  [item.path]: item.page,
}));
let customerRoute = routes.customer.map((item) => ({ [item.path]: item.page }));

let combineRoute = [
  ...publicRoute,
  ...administratorRoute,
  ...warehouseRoute,
  ...customerRoute,
];
let titlePage = {};
for (let i = 0; i < combineRoute.length; i++) {
  titlePage = { ...titlePage, ...combineRoute[i] };
}
export { routes, titlePage };
