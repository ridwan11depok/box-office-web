import React, { useEffect } from 'react';
import { Route, useLocation } from 'react-router-dom';

export default function PublicRoute(props) {
  let location = useLocation().pathname;

  const { component: Component, titlePage, ...rest } = props;

  useEffect(() => {
    document.title = `${titlePage} | Boxxoffice`;
  }, [location, titlePage]);
  return <Route {...rest} render={(props) => <Component {...props} />} />;
}
