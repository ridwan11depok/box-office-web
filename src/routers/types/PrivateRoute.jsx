import React, { useEffect } from 'react';
import { Redirect, Route, useLocation } from 'react-router-dom';
import { PageBase } from '../../components/layout/PageBase';
import { useSelector } from 'react-redux';

export default function PrivateRoute(props) {
  let location = useLocation().pathname;
  const stateLogin = useSelector((state) => state?.login) || null
  let token = null
  let role = null
  if(stateLogin) {
    token = stateLogin?.token
    role = stateLogin?.role
  } 

  const {
    component: Component,
    role: activeRole,
    activeMenu,
    titlePage,
    ...rest
  } = props;

  useEffect(() => {
    document.title = `${titlePage} | Boxxoffice`;
  }, [location, titlePage]);
  return (
    <Route
      {...rest}
      render={(props) => {
        if (token && role === activeRole) {
          return (
            <>
              <PageBase role={role.toLowerCase()} activeMenu={activeMenu}>
                <Component {...props} />
              </PageBase>
            </>
          );
        } else {
          return (
            <Redirect
              to={{
                pathname: '/login',
              }}
            />
          );
        }
      }}
    />
  );
}

PrivateRoute.defaultProps = {
  role: '',
};
