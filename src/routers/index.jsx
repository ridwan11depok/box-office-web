import React, { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { routes } from './routes';
import './styles.css';
import { useSelector, useDispatch } from 'react-redux';
import Toast from '../components/elements/Toast';
import { setToast } from '../redux/actions';

import PrivateRoute from './types/PrivateRoute';
import PublicRoute from './types/PublicRoute';
import Error from '../pages/general/Error';

const history = React.lazy(() => import('../history'));

const AppRouter = () => {
  let location = useLocation().pathname;

  const loginState = useSelector((state) => state.login);
  const dispatch = useDispatch();
  const { isShow, type, messages } = useSelector((state) => state.toast);
  const { errorData } = useSelector((state) => state.errorReducer);

  const [state, setState] = useState({
    errorData: {}
  });

  const router = useHistory();
  useEffect(() => {
    if (
      loginState?.token &&
      loginState?.role === 'Customer' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/dashboard');
    } else if (
      loginState?.token &&
      loginState?.role === 'Administrator' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/admin/dashboard');
    } else if (
      loginState?.token &&
      loginState?.role === 'Warehouse' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/warehouse/dashboard');
    }
    console.log('errorData', errorData)
    if(errorData){
      if(errorData.code === 401){
        router.push('/login');
      }else{
        setState({ ...state, errorData: errorData });
      }
    }
  }, [location, errorData]);

  useEffect(() => {
    if (
      loginState?.token &&
      loginState?.role === 'Customer' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/dashboard');
    } else if (
      loginState?.token &&
      loginState?.role === 'Administrator' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/admin/dashboard');
    } else if (
      loginState?.token &&
      loginState?.role === 'Warehouse' &&
      (location === '/login' || location === '/register')
    ) {
      router.push('/warehouse/dashboard');
    }
  }, []);

  
  
  // useEffect(() => {
  //   console.log('errorData', errorData)
  //   if(errorData){
  //     if(errorData.code === 401){
  //       router.push('/login');
  //     }else{
  //       setState({ ...state, errorData: errorData });
  //     }
  //   }
  // }, [errorData]);
  
  useEffect(() => {
    if (isShow) {
      Toast({ type, message: messages });
      setTimeout(() => {
        dispatch(setToast({ isShow: false }));
      }, 3000);
    }
  }, [isShow]);
  return (
    <>
      {state.errorData?.code ? 
        <Error data={state.errorData} />
      : 
        <div>
          <ToastContainer />
          <React.Suspense fallback={<h1>Still Loading…</h1>}>
            <Switch history={history}>
              {/* Public route */}
              {routes.public.length &&
                routes.public.map((route) => (
                  <PublicRoute
                    exact
                    path={route.path}
                    component={route.component}
                    titlePage={route.page}
                  />
                ))}

              {/* Administrator route*/}
              {routes.administrator.length &&
                routes.administrator.map((route) => (
                  <PrivateRoute
                    role="Administrator"
                    path={route.path}
                    exact
                    component={route.component}
                    activeMenu={route.activeMenu}
                    titlePage={route.page}
                  />
                ))}

              {/* Warehouse route*/}
              {routes.warehouse.length &&
                routes.warehouse.map((route) => (
                  <PrivateRoute
                    role="Warehouse"
                    path={route.path}
                    exact
                    component={route.component}
                    activeMenu={route.activeMenu}
                    titlePage={route.page}
                  />
                ))}

              {/* Customer route*/}
              {routes.customer.length &&
                routes.customer.map((route) => (
                  <PrivateRoute
                    role="Customer"
                    path={route.path}
                    exact
                    component={route.component}
                    activeMenu={route.activeMenu}
                    titlePage={route.page}
                  />
                ))}

              {/* Not Found Route */}
              <Route path="*" component={() => (<Error data={{code: 404, error: {message: 'Page not found'}}} />)} />
            </Switch>
          </React.Suspense>  
        </div>
      }
      
    </>
  );
};

export default AppRouter;
