const dev = {
  api: 'https://apidev.boxxoffice.id/api/',
};

const prod = {
  api: 'https://api.boxxoffice.id/api/',
};

const local = {
  api: 'http://localhost:8000/api/',
};

const config = local; // change the value (dev for development, prod for production)

const baseUrl = {
  //general
  register: `${config.api}v1/register`,
  login: `${config.api}v1/login`,
  area: `${config.api}v1/area/province/`,
  listSetting: `${config.api}v1/setting/list-setting`,
  listService: `${config.api}v1/setting/list-service`,
  listTestimoni: `${config.api}v1/setting/list-testimoni`,
  listBlog: `${config.api}v1/setting/list-blog`,

  //administrator
  warehouse: `${config.api}v1/warehouse/`,
  staffWarehouse: `${config.api}v1/staff-warehouse/`,
  category: `${config.api}v1/category-product/`,
  subCategory: `${config.api}v1/sub-category-product/`,
  storeAdministrator: `${config.api}v1/administrator/stores/`,
  rateHandlingStock: `${config.api}v1/administrator/rate-handling-stores/`,
  warehouseStore: `${config.api}v1/administrator/warehouse-stores/`,
  userStore: `${config.api}v1/administrator/user-stores/`,
  listRoleUserStore: `${config.api}v1/administrator/user-stores/list-roles/`,
  packaging: `${config.api}v1/administrator/packaging/`,

  //customer
  dashboardCustomer: `${config.api}v1/customer/dashboard/`,
  userAccountTeam: `${config.api}v1/customer/team/`,
  userAccountDetail: `${config.api}v1/user/account/detail`,
  userAccountChangePassword: `${config.api}v1/user/account/change_password/`,
  userAccountUpdateProfile: `${config.api}v1/user/account/update_profile/`,
  listRoleTeam: `${config.api}v1/customer/team/list-roles/`,
  productSku: `${config.api}v1/customer/product/`,
  listAllProductSku: `${config.api}v1/customer/product/list`,
  categoryProductCustomer: `${config.api}v1/category-product/list/`,
  subCategoryProductCustomer: `${config.api}v1/sub-category-product/list/`,
  storeDetailCustomer: `${config.api}v1/customer/store/detail/`,
  updateStoreCustomer: `${config.api}v1/customer/store/update/`,
  deleteProductBatchCustomer: `${config.api}v1/customer/product/batch`,
  updateProfileCustomer: `${config.api}v1/user/account/update_profile/`,
  listWarehouseCustomer: `${config.api}v1/customer-warehouse/list/`,
  detailWarehouseCustomer: `${config.api}v1/customer-warehouse/detail/`,
  requestWarehouseCustomer: `${config.api}v1/customer/storage/request/`,
  inboundCustomer: `${config.api}v1/customer/inbound/`,
  listStorageCustomer: `${config.api}v1/customer/storage/list/`,
  listProductStorageCustomer: `${config.api}v1/customer/storage/list-product/`,
  dispatchOrderCustomer: `${config.api}v1/customer/dispatch-order/`,
  listVendor: `${config.api}v1/vendor/list/`,
  listServiceVendor: `${config.api}v1/vendor/list-service/`,
  listPackagingCustomer: `${config.api}v1/customer/dispatch-order/packaging-list/`,
  checkAWBDispatchOrderCustomer: `${config.api}v1/customer/dispatch-order/check-awb/`,
  transactionRegularCustomer: `${config.api}v1/customer/transaction-reguler/`,
  listCourierCustomer: `${config.api}v1/customer/transaction-reguler/list-courier/`,
  exportLogsDispatchOrderCustomer: `${config.api}v1/customer/dispatch-order/export/`,
  exportLogsRegularTransactionCustomer: `${config.api}v1/customer/transaction-reguler/export/`,
  cashlessCustomer: `${config.api}v1/customer/cashless/`,
  checkAWBCashlessCustomer: `${config.api}v1/customer/cashless/check-awb/`,
  listReportTransactionUser: `${config.api}v1/customer/report/transaction`,
  exportLogs: `${config.api}v1/customer/report/transaction`,
  listReportInvoiceUser: `${config.api}v1/customer/report/invoice`,
  exportLogsReportInvoice: `${config.api}v1/customer/report/invoice`,

  //warehouse
  inboundWarehouse: `${config.api}v1/admin-warehouse/inbound/`,
  inboundWarehouseConfirmation: `${config.api}v1/admin-warehouse/inbound/confirmation/`,
  storeWarehouse: `${config.api}v1/admin-warehouse/store/`,
  listStoreAdminWarehouse: `${config.api}v1/admin-warehouse/store/list/`,
  dispatchOrderAdminWarehouse: `${config.api}v1/admin-warehouse/dispatch-order/`,
  cashlessWarehouse: `${config.api}v1/admin-warehouse/cashless/`,
  packagingAdminWarehouse : `${config.api}v1/admin-warehouse/packaging/`,
  transactionRegulerAdminWarehouse: `${config.api}v1/admin-warehouse/transaction-reguler/`,
  manifestWarehouse: `${config.api}v1/admin-warehouse/manifest/`,
  listReportTransaction: `${config.api}v1/admin-warehouse/report/transaction`,
  listReportInvoice: `${config.api}v1/admin-warehouse/report/invoice/`,
  exportLogsTransaction: `${config.api}v1/admin-warehouse/report/transaction`,
  exportLogsInvoice: `${config.api}v1/admin-warehouse/report/invoice/`,
  cashlessAdminWarehouse: `${config.api}v1/admin-warehouse/cashless/`,
};

export default baseUrl;
