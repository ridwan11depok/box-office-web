import baseUrl from './url';
import ApiRequest from './config';

const API = {};

API.register = ApiRequest.post(baseUrl.register);

export default API;
