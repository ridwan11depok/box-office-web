import Axios from 'axios';


class ApiRequest {
  static get(route, token = false) {
    return payload => this.request('GET', route, payload, token);
  }
  static put(route, token = false) {
    return payload => this.request('PUT', route, payload, token);
  }
  static post(route, token = false) {
    return payload => this.request('POST', route, payload, token);
  }
  static delete(route, token = false) {
    return payload => this.request('DELETE', route, payload, token);
  }
  static resolveParams(params) {
    const paramsResult = [];
    Object.keys(params).map(e => paramsResult.push(`${e}=${params[e]}`));
    return paramsResult.join('&');
  }

  static request(method, route, payload = {}, token) {
    return new Promise((resolve, reject) => {
      const path = payload.path ? `/${payload.path}` : '';
      const params = payload.params
        ? `?${this.resolveParams(payload.params)}`
        : '';
      const customUrl = payload.url ? payload.url : '';
      const baseHeaders = {
        'Content-Type':
          payload.type === 'form-data'
            ? 'multipart/form-data'
            : 'application/json',
        'Accept': payload.accept ? payload.accept : 'application/json'
        // 'api-key': 'Balance@apikey',
      };
      if (token) {
        const userToken = token; // get local token
        baseHeaders.Authorization = `${userToken || ''}`;
      }
      let bodyForPost = {
        url: customUrl.length > 0 ? customUrl : route + path + params,
        method,
        headers: payload.headers
          ? { ...baseHeaders, ...payload.headers }
          : baseHeaders,
        data: payload.body ? payload.body : {},
      }

      let bodyForGet = {
        url: customUrl.length > 0 ? customUrl : route + path + params,
        method,
        headers: payload.headers
          ? { ...baseHeaders, ...payload.headers }
          : baseHeaders,
      }

      
      Axios.request(method === "POST" ? bodyForPost : bodyForGet)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          if (err && err.response && err.response.data) {
            reject(err.response.data);
          } else if (err && err.response) {
            const errs = {
              statusCode: err.response.status,
              ...err.response,
            };
            reject(errs);
          } else if (err) {
            reject(err);
          }
        });
    });
  }
}

export default ApiRequest;
