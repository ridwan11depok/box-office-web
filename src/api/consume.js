import React from 'react';
import axios from 'axios';
import baseUrl from './url';

class ConsumeAPI extends React.Component {
  header(payload, url) {
    const baseHeaders = {
      'Content-Type': payload.content_type ?? 'application/json',
      Accept: payload.acceptType ?? 'application/prs.boxxoffice.v1+json',
    };

    if (payload.token) {
      baseHeaders.Authorization = `Bearer ${payload.token}`;
    }

    return baseHeaders;
  }

  post(url, data, payload = {}, id , extendedUrl) {
    return this.request('POST', url, data, payload, id, extendedUrl);
  }

  get(url, data, payload = {}, id, order_id, extendedUrl) {
    return this.request('GET', url, data, payload, id, order_id, extendedUrl);
  }

  getWithParams(url, data, payload = {}, params, id, extendedUrl) {
    return this.requestWithParams('GET', url, data, payload, params, id, extendedUrl);
  }

  delete(url, data, payload = {}, id, extendedUrl) {
    return this.request('DELETE', url, data, payload, id, extendedUrl);
  }

  put(url, data, payload = {}, params = {}, id = null) {
    return this.requestWithParams('PUT', url, data, payload, params, id);
  }

  request(method, url, data, payload, id, order_id, extendedUrl = '') {
    return new Promise((resolve, reject) => {
      if (id) {
        axios
          .request({
            url: baseUrl[url] + id,
            method,
            data: data,
            headers: this.header(payload, baseUrl[url]),
          })
          .then((res) => {
            resolve(res.data);
          })
          .catch((err) => {
            if (err && err.response && err.response.data) {
              reject(err.response.data);
            } else if (err && err.response) {
              const errs = {
                statusCode: err.response.status,
                ...err.response,
              };
              reject(errs);
            } else if (err) {
              reject(err);
            }
          });
      } else if (order_id) {
        // //console.log('masuk sini', order_id)
        axios
          .request({
            url: baseUrl[url] + order_id,
            method,
            data: data,
            headers: this.header(payload, baseUrl[url]),
          })
          .then((res) => {
            // //console.log('res', res)
            resolve(res.data);
          })
          .catch((err) => {
            if (err && err.response && err.response.data) {
              reject(err.response.data);
            } else if (err && err.response) {
              const errs = {
                statusCode: err.response.status,
                ...err.response,
              };
              reject(errs);
            } else if (err) {
              reject(err);
            }
          });
      } else {
        let requestForPost = {
          url: `${baseUrl[url]}${extendedUrl ?? extendedUrl}` ,
          method,
          data,
          headers: this.header(payload, baseUrl[url]),
        };
        let requestForGet = {
          url: `${baseUrl[url]}${extendedUrl ?? extendedUrl}`,
          method,
          headers: this.header(payload, baseUrl[url]),
        };
        let requestForDelete = {
          url: `${baseUrl[url]}${extendedUrl ? extendedUrl : ''}`,
          method,
          data: data,
          headers: this.header(payload, baseUrl[url]),
        };
        let request;
        if (method === 'POST') {
          request = requestForPost;
        } else if (method === 'GET') {
          request = requestForGet;
        } else if (method === 'DELETE') {
          request = requestForDelete;
        }
        axios
          .request(request)
          .then((res) => {
            resolve(res.data);
          })
          .catch((err) => {
            if (err && err.response && err.response.data) {
              reject(err.response.data);
            } else if (err && err.response) {
              const errs = {
                statusCode: err.response.status,
                ...err.response,
              };
              reject(errs);
            } else if (err) {
              reject(err);
            }
          });
      }
    });
  }

  requestWithParams(method, url, data, payload, params, id, extendedUrl = '') {
    return new Promise((resolve, reject) => {
      let requestForPost = {
        url: baseUrl[url],
        method,
        data,
        headers: this.header(payload, baseUrl[url]),
      };

      let requestForGet = {
        
        url: id ? `${baseUrl[url]}${extendedUrl ?? extendedUrl}` + id : `${baseUrl[url]}${extendedUrl ?? extendedUrl}`,
        method,
        params,
        headers: this.header(payload, baseUrl[url]),
      };
      let requestForPut = {
        url: id ? baseUrl[url] + Number(id) : baseUrl[url],
        method,
        params,
        headers: this.header(payload, baseUrl[url]),
        data,
      };
      let typeRequest;
      if (method === 'POST') {
        typeRequest = requestForPost;
      } else if (method === 'GET') {
        typeRequest = requestForGet;
      } else if (method === 'PUT') {
        typeRequest = requestForPut;
      }

      axios
        .request(typeRequest)
        .then((res) => {
          // //console.log('res', res)
          resolve(res.data);
        })
        .catch((err) => {
          if (err && err.response && err.response.data) {
            reject(err.response.data);
          } else if (err && err.response) {
            const errs = {
              statusCode: err.response.status,
              ...err.response,
            };
            reject(errs);
          } else if (err) {
            reject(err);
          }
        });
    });
  }
}

export default new ConsumeAPI();
